# WebHare Platform

This project contains:
- The HareScript engine, modules and tests
- Webserver
- The system, publisher, consilio, wrd and tollium modules
- The testsuite for the above modules

# License
WebHare is licensed under the [MIT License](LICENSE.md). Some of the libraries
used by WebHare may be licensed differently. Modules you write and distribute
yourself (ie those that aren't part of the WebHare Platform project) can be
released under any license you wish.

# Documentation
See https://www.webhare.dev/

## How to submit a change/fix
Fork our repository and send in a merge request. See the [contribution guidelines](CONTRIBUTING.md)
