// This reverts the effects of https://github.com/emscripten-core/emscripten/pull/21775 for now
var Module = typeof moduleArg !== "undefined" ? moduleArg : {};
