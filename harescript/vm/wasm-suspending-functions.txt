# This is the list of the HareScript WASM exports that can (indirectly) call
# an EM_ASYNC_JS function (like registered async functions). These functions
# all return Promises. If an EM_ASYNC_JS call returns an 'invalid suspender'
# error, it needs to be added here.

HSVM_ObjectMemberCopy
HSVM_ObjectMemberSet
HSVM_CallFunctionPtr
HSVM_CallFunction
HSVM_CallFunctionAutoDetect
HSVM_CallObjectMethod
HSVM_ThrowException
HSVM_ThrowExceptionObject
HSVM_ExecuteScript
HSVM_LoadScript
HSVM_MakeFunctionPtrInternal
HSVM_MakeFunctionPtrAutoDetect
HSVM_PrelinkLibraryLeakRef
