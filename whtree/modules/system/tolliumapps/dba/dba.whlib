<?wh
/* Direct link
   https://webhare.moe.sf.webhare.nl/?app=system:config/dba
*/

LOADLIB "wh::internal/tabledefs.whlib";
LOADLIB "mod::tollium/lib/screenbase.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/dialogs.whlib";
LOADLIB "mod::system/lib/resources.whlib";
LOADLIB "mod::system/lib/internal/resourcemanager.whlib";
LOADLIB "mod::system/lib/internal/modulemanager.whlib";
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";
LOADLIB "mod::system/lib/internal/dbase/updatecommands.whlib";

STRING FUNCTION GetDatabaseWHLIB(OBJECT trans, STRING schemaname)
{
  STRING bindfuncname := 'Bind';
  FOREVERY(STRING tok FROM Tokenize(schemaname,"_"))
    bindfuncname := bindfuncname || ToUppercase(Left(tok,1)) || ToLowercase(substring(tok,1));
  bindfuncname := bindfuncname || 'Tables';

  STRING hs_schemadef := '<?wh\nLOADLIB "mod::system/lib/database.whlib";\n\n'
                  || GetHarescriptSchemaDefinition(trans, schemaname)
                  || '\n'
                  || 'MACRO ' || bindfuncname || '(INTEGER transactionid)\n'
                  || '{\n'
                  || '  ' || ToLowercase(schemaname) || ' := BindTransactionToSchema(transactionid, "' || ToLowercase(schemaname) || '");\n'
                  || '}\n'
                  || '\n'
                  || 'SetupPrimaryTransactionBinder(PTR ' || bindfuncname || ');\n';

  RETURN hs_schemadef;
}

STRING FUNCTION GetInterfaceName(STRING tablename)
{
  //TODO allow tabledefinitions to explicitly suggest the interface names ?
  STRING finalname; //snakecase to initialcaps:
  FOREVERY(STRING tok FROM Tokenize(tablename,"_"))
    finalname := finalname || ToUppercase(Left(tok,1)) || Substring(tok,1);

  IF(finalname LIKE "*s")
    finalname := Left(finalname, Length(finalname)-1); //eliminate plural
  RETURN finalname || "Row";
}

STRING FUNCTION GetTSType(RECORD col)
{
  IF(col.data_type LIKE "varchar(*")
    RETURN "string";

  SWITCH(col.data_type) //TODO timestamp & blob
  {
    CASE "int4", "int8"
    {
      RETURN "number";
    }
    CASE "bool"
    {
      RETURN "boolean";
    }
    DEFAULT
    {
      RETURN col.data_type;
    }
  }
}

STRING FUNCTION GetTSInterfaces(OBJECT trans, STRING schemaname)
{
  STRING interfaces;
  FOREVERY(RECORD tablerec FROM SELECT * FROM trans->GetTableListing(schemaname) ORDER BY table_name)
  {
    interfaces := interfaces || `interface ${GetInterfaceName(tablerec.table_name)} {\n`;
    RECORD ARRAY columns := trans->GetColumnListing(schemaname, tablerec.table_name);
    FOREVERY(RECORD col FROM columns)
      interfaces := interfaces || `  ${col.column_name}: ${GetTSType(col)};\n`;
    interfaces := interfaces || `}\n\n`;
  }

  RETURN interfaces;
}

STRING FUNCTION ExplainConstraints(RECORD tabledef, RECORD columnrec)
{
  STRING ARRAY constraints;
  IF(tabledef.primary_key_name = columnrec.column_name)
  {
    INSERT GetTid("system:tolliumapps.dba.main.primarykey") INTO constraints AT END;
  }
  ELSE //no point in stating the obvious
  {
    IF(columnrec."is_unique")
      INSERT GetTid("system:tolliumapps.dba.main.unique") INTO constraints AT END;
    IF(columnrec.is_nullable)
      INSERT GetTid("system:tolliumapps.dba.main.not_null") INTO constraints AT END;
    IF(ToUppercase(columnrec.on_delete) = "SET DEFAULT")
      INSERT GetTid("system:tolliumapps.dba.main.on_delete_set_default") INTO constraints AT END;
    IF(ToUppercase(columnrec.on_delete) = "CASCADE")
      INSERT GetTid("system:tolliumapps.dba.main.on_delete_cascade") INTO constraints AT END;
  }
  IF(columnrec.autonumber_start!=0)
    INSERT GetTid("system:tolliumapps.dba.main.autonumber", ToString(columnrec.autonumber_start)) INTO constraints AT END;

  RETURN Detokenize(constraints,", ");
}

PUBLIC STATIC OBJECTTYPE Main EXTEND TolliumScreenBase
<
  RECORD ARRAY dbs;
  RECORD ARRAY databaseschemas;
  RECORD ARRAY modlist;

  MACRO Init(RECORD data)
  {
    this->AddDatabases([[ title := GetTid("system:tolliumapps.dba.webharedb.database")
                        , gettrans := PTR GetPrimary()
                        , rowkey := "webharedb"
                       ]]);
    this->RefreshDatabaseSchemas();
  }

  MACRo RefreshDatabaseSchemas()
  {
    this->modlist := SELECT *
                          , databaseschemas := GetModuleDatabaseSchema(name)
                       FROM GetWebHareModules();

    this->databaseschemas := RECORD[];
    FOREVERY(RECORD mod FROM this->modlist)
      this->databaseschemas := this->databaseschemas CONCAT mod.databaseschemas;
  }

  MACRO DoUpdateSchemas()
  {
    RECORD result := [ commitmessages := DEFAULT RECORD ARRAY, commands := DEFAULT RECORD ARRAY ];
    this->RefreshDatabaseSchemas();
    FOREVERY(RECORD module FROM this->modlist)
    {
      RECORD output := InitModuleTables(module);
      result.commitmessages := result.commitmessages CONCAT output.commitmessages;
      result.commands := result.commands CONCAT output.commands;
    }

    this->InvalidateAll();
    this->OnSelectSchema();

    STRING ARRAY sqlcommands := SELECT AS STRING ARRAY cmd FROM result.commands;

    //FIXME nicer output, but what to do with a mix of errors and commands?
    REFLECT(CELL[result, sqlcommands]);

  }

  MACRO AddDatabases(RECORD ARRAY dbs)
  {
    this->dbs := this->dbs CONCAT (SELECT *, triedtrans := FALSE, trans := DEFAULT OBJECT FROM dbs);
  }

  OBJECT FUNCTION GetTransactionForDatabase(STRING rowkey)
  {
    RECORD db := SELECT * FROM this->dbs WHERE dbs.rowkey = VAR rowkey;
    IF(db.triedtrans)
      RETURN db.trans;

    OBJECT trans := db.gettrans();
    UPDATE this->dbs SET trans := VAR trans, triedtrans := TRUE WHERE dbs.rowkey = VAR rowkey;
    RETURN trans;
  }

  MACRO InvalidateAll()
  {
    ^databases->Invalidate();
    this->OnSelectSchema();
  }

  PUBLIC MACRO OnSelectSchema()
  {
    RECORD curitem := ^databases->selection;
    IF(NOT RecordExists(curitem))
    {
      ^columns->rows := DEFAULT RECORD ARRAY;
      ^indices->rows := DEFAULT RECORD ARRAY;
      RETURN;
    }

    IF(curitem.isdatabase)
    {
      ^columns->rows := DEFAULT RECORD ARRAY;
      ^indices->rows := DEFAULT RECORD ARRAY;
      RETURN;
    }
    IF(curitem.isschema)
    {
      ^columns->rows := DEFAULT RECORD ARRAY;
      ^indices->rows := DEFAULT RECORD ARRAY;
      ^databasewhlib->value := GetDatabaseWHLIB(GetPrimary(), curitem.schema_name);
      ^interfaces->value := GetTSInterfaces(GetPrimary(), curitem.schema_name);
      RETURN;
    }

    RECORD ARRAY rows, indices;

    IF(curitem.istable)
    {
      BOOLEAN allclaimed := ToLowercase(curitem.schema_name) IN [ "information_schema", "system_rights" ];
      STRING ARRAY claimedcolumns;
      IF(NOT allclaimed)
      {
        RECORD schemadef := SELECT * FROM this->databaseschemas WHERE ToUppercase(name) = ToUppercase(curitem.schema_name);
        IF(RecordExists(schemadef))
        {
          RECORD tabledef := SELECT * FROM schemadef.tables WHERE ToUppercase(name) = ToUppercase(curitem.table_name);
          IF(RecordExists(tabledef))
          {
            claimedcolumns := SELECT AS STRING ARRAY ToLowercase(name) FROM tabledef.cols;
          }
        }
      }

      RECORD tabledef :=
          SELECT *
            FROM GetPrimary()->GetTableListing(curitem.schema_name)
           WHERE ToUppercase(table_name) = ToUppercase(curitem.table_name);

      rows := SELECT columnname := ToLowercase(column_name)
                   , columntype := data_type = "VARCHAR" ? data_type || "(" || character_maximum_length || ")" : data_type
                   , references := referenced_table_schema!="" ? ToLowercase(referenced_table_schema || "." || referenced_table_name) : ""
                   , constraints := ExplainConstraints(tabledef, columns)
                   , rowkey := column_name
                   , icon := column_name = tabledef.primary_key_name ? 1 : 0
                   , listrowclasses := allclaimed OR ToLowercase(column_name) IN claimedcolumns ? STRING[] : ["grayedout"]
                   , candelete := NOT (allclaimed OR ToLowercase(column_name) IN claimedcolumns)
                FROM GetPrimary()->GetColumnListing(curitem.schema_name, curitem.table_name) AS columns;

      IF (IsValueSet(claimedcolumns))
      {
        RECORD moduledef := GetSchemaModuleDefinition(GetPrimary(), curitem.schema_name, [ withimplicitindices := TRUE ]);
        IF (RecordExists(moduledef))
        {
          RECORD mod_tbldef := SELECT * FROM moduledef.tables WHERE name = curitem.table_name;
          IF (RecordExists(mod_tbldef))
          {
            indices :=
                SELECT name
                     , listrowclasses :=  is_implicit ? ["grayedout"] : STRING[]
                     , is_unique :=       is_unique ? GetTid("~yes") : GetTid("~no")
                     , is_uppercase :=    is_uppercase ? GetTid("~yes") : GetTid("~no")
                     , cols :=            Detokenize((SELECT AS STRING ARRAY name FROM cols), ", ")
                  FROM mod_tbldef.indices;
          }
        }
      }
    }

    ^columns->rows := rows;
    ^indices->rows := indices;
  }

  STRING ARRAY FUNCTION OnDatabasesTreePath(STRING item)
  {
    IF(item="")
      RETURN DEFAULT STRING ARRAY;

    STRING ARRAY pathitems;
    STRING ARRAY intoks := Tokenize(item, '\t');
    FOREVERY(STRING tok FROM intoks)
      INSERT Detokenize(ArraySlice(intoks, 0, #tok+1), '\t') INTO pathitems AT END;
    RETURN pathitems;
  }

  RECORD ARRAY FUNCTION OnDatabasesLoadItems(RECORD item)
  {
    IF(NOT RecordExists(item))
    {
      RETURN SELECT name := title
                  , rowkey
                  , isdatabase := TRUE
                  , isschema := FALSE
                  , istable := FALSE
                  , expandable := TRUE
                  , expanded := TRUE
                  , candelete := FALSE
                  , icon := 1
                  , database := rowkey
               FROM this->dbs
           ORDER BY ToUppercase(title);
    }

    IF (item.isdatabase)
    {
      OBJECT trans := this->GetTransactionForDatabase(item.database);
      IF(NOT ObjectExists(trans))
        RETURN DEFAULT RECORD ARRAY;

      STRING ARRAY claimed_schemas := GetInstalledModuleNames(); //ADDME only if they register a <databaseschema>
      RETURN SELECT TEMPORARY isbuiltin := ToLowercase(schema_name) IN whconstant_builtin_schemas
                  , TEMPORARY isclaimed := isbuiltin OR ToLowercase(schema_name) IN STRING["public", ...claimed_schemas]
                  , *
                  , name := ToLowercase(schema_name)
                  , rowkey := item.rowkey || "\t" || schema_name
                  , isdatabase := FALSE
                  , isschema := TRUE
                  , istable := FALSE
                  , icon := 2
                  , expandable := Length(trans->GetTableListing(schema_name)) > 0
                  , expanded := FALSE
                  , candelete := isbuiltin = FALSE AND (isclaimed = FALSE OR NOT IsDtapLive())
                  , database := item.database
                  , schema_name := schema_name
                  , listrowclasses := isclaimed ? STRING[] : ["grayedout"]
               FROM trans->GetSchemaListing()
              WHERE NOT is_system_schema
           ORDER BY ToUppercase(schema_name); //ADDME mark 'is_system_schema' tables
    }

    IF(item.isschema)
    {
      OBJECT trans := this->GetTransactionForDatabase(item.database);
      IF(NOT ObjectExists(trans))
        RETURN DEFAULT RECORD ARRAY;

      BOOLEAN allclaimed := ToLowercase(item.schema_name) IN whconstant_builtin_schemas;
      STRING ARRAY tables;
      IF(NOT allclaimed)
      {
        RECORD schemadef := SELECT * FROM this->databaseschemas WHERE name = item.name;
        IF(RecordExists(schemadef))
          tables := SELECT AS STRING ARRAY ToLowercase(name) FROM schemadef.tables;
      }

      RETURN SELECT *
                  , name := ToLowercase(table_name)
                  , rowkey := item.rowkey || "\t" || table_name
                  , isdatabase := FALSE
                  , isschema := FALSE
                  , istable := TRUE
                  , icon := isview ? 4 : 3
                  , candelete := VAR item.candelete //FIXME allow deletion of tables not mentioned in schemadef
                  , expanded := FALSE
                  , expandable := FALSE
                  , database := item.database
                  , schema_name := item.schema_name
                  , listrowclasses := allclaimed OR ToLowercase(table_name) IN tables ? STRING[] : ["grayedout"]
               FROM trans->GetTableListing(item.schema_name)
           ORDER BY ToUppercase(table_name);
    }
    ABORT("Unrecognized item");
  }

  PUBLIC MACRO DoAddItem()
  {
  }

  PUBLIC MACRO DoDelete()
  {
    RECORD curitem := ^databases->selection;
    STRING question := curitem.isschema ? this->GetTid(".verifydeleteschema", curitem.schema_name)
                                        : this->GetTid(".verifydeletetable", curitem.name);

    //ADDME check and inform if there would be cascades? just not allowing CASCADE for now
    IF(this->RunSimpleScreen("confirm", question) != "yes")
      RETURN;

    OBJECT work := this->BeginWork();
    IF(curitem.isschema)
      GetPrimary()->DropSchema(curitem.schema_name, [ cascade := TRUE ]);
    ELSE
      GetPrimary()->DropTable(curitem.schema_name, curitem.name);
    work->Finish();
    this->InvalidateAll();
  }

  PUBLIC MACRO DoDeleteColumn()
  {
    RECORD databaseitem := ^databases->selection;
    RECORD curitem := ^columns->selection;
    //ADDME check and inform if there would be cascades? just not allowing CASCADE for now
    IF(this->RunSimpleScreen("confirm", this->GetTid(".verifydeletecolumn", curitem.columnname)) != "yes")
      RETURN;

    OBJECT work := this->BeginWork();
    GetPrimary()->DropColumn(databaseitem.schema_name, databaseitem.table_name, curitem.columnname);
    work->Finish();
    this->InvalidateAll();
  }

  PUBLIC MACRO DoRunSQLClient()
  {
    RunModuleScriptDialog(this, "mod::system/scripts/whcommands/sql.whscr", STRING[], [ inputenabled := TRUE ]);
  }
>;
