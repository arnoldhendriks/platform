Read about how to install, upgrade and maintain WebHare, including backing up your installation.

Check the changelogs to find out about incompatibilities and deprecations introduced with each version of WebHare, things you should do when upgrading to that version and other things that are nice to know about that version.

TODO: A way to introduce subtopics for documents (like it's already possible for libraries).
