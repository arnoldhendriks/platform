# 4.06
## Website and module development
- Source code is no longer indexed by default. If you want to use /docs/, go to Module & Configuration, Menu, WebHare backend, and re-enable it.
- certbot integration. Use `wh ssl certbot <domainname> [additionalnames...]` to request a domain. Fair usage restrictions apply!
- explicit aliases. The with-'www.' and without-'www.' aliases names are no longer implicitly generated. an upgrade script will add missing implicit aliases as follows:
  - primary webservers bound to an explicit port (not a virtualhosted) are ignored
  - if the primary webserver url is verified to connect to the webserver generating aliases, all implicit aliases are tested. any alias that connects to the same webserver is added
  - if the primary webserver url cannot be verified, all implicit aliases are added without testing.
  - if this breaks anything, you can get the old behavior back by clearing the registry key 'webserver.global.explicitaliases' until the problem is fixed
- the webserver will continously verify whether it can connect to its own webservers and generate system messages for broken webservers
- the columns shown in the publisher can be overriden on a per-folder basis. You can add custom columns, remove existing columns, and control whether the website ordering options are available

## Publisher
- Modules can add custom actions to the publisher using `<filemgrextension>`

## Applications
- An application can list supportedlanguages= and will only be opened in one of those languages. Note that you will still need to properly use limitlanguages= in the language file

