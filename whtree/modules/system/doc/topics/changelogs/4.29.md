# 4.29 - 2020-07-28

## Incompatibilities and deprecations
- The (partly broken, if the key wast too short) `Encrypt_Blowfish` and `Decrypt_Blowfish` funtions have been removed.
  The algorithm is still accessible through %Encrypt and %Decrypt (if you use algorithm `bf-ecb`) but you should generally avoid ECB in crypto.
- A HareScript Tollium test will now fail if it found missing tids during execution. If you are explicitly testing for missing tids, you should
  clear those tids from `controller->missingtids`.
- The default TLS cipher suite has been updated. If you encounter connection difficulties, this was the previous OpenSSL cipher list:
  `ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:ECDHE-ECDSA-DES-CBC3-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:DES-CBC3-SHA:!DSS`
- `<list rowselect>` (ignored since 4.03) will now be deprecated and return warnings/throw if changed
- `<button isdefault>` was already deprecated and will now return warnings if used
- `EnsureEntity`'s automatic key determination was found to be confusing, so its syntax has been reworked to require explicit
  specification of the key. `EnsureEntity(required, ifnew)` should be rewritten as `EnsureEntity(required, [ entitykey := STRING[ ... ], ifnew := ... ])`
  where `entitykey` is the list of fields which serve as the unique key. This will be backported to 4.28.2 and future versions of WebHare may
  throw an exception when using the 'old' syntax.
- The deprecated libraries in `system/js/dom/` now start warning more aggressively about their impending removal
- `<acceptdrops>` in `<xs:appinfo>` blocks should be replaced with `<acceptdropsparser>` in 4.29. Accepting `acceptdropsparser`
  has also been backported to 4.28.2.
- `system:openas` no longer implies `system:manageunits`. If you intend for users with 'open as' rights to have full rights to
  manage that unit (and its users and roles) you should explicitly assign it
- The escape hatch `__wrdauth_allow_requestless_login` for using WRDAauth webdesign plugins outside a request has been removed
- The `usefromlanguage` feature in language files has been removed

## Things that are nice to know
- The testframework added %TTLaunchStartedApp as a clean way to wait for applications to start
- QueueMailInWork has an option `ignorerecipientwhitelist` to ignore the email recipient whitelist
- WRD Password resets (including WebHare itself) will ignore the email recipient whitelist
- %Job::GetConsoleExitCode now returns -1 if the job crashed
- `RunScreen` and screen references with a modern namespace (`mod::`, `inline::`, `inline-base64::`) whose filenames end in `.xml`
  can now omit the `#hash` part to reference the first screen in the file (just like WebHare 3 allowed, but less ambiguous)
- The 'placeholder' option in a `<select>` is now marked with `data-wh-placeholder` by the form rendering (backported to 4.28.1)
- Forms now always scroll to the formtop (the `<form>` node) when jumping between pages. Or more specifically, they jump to the
  first `.wh-anchor` node which is generally inserted during rendering or at form construction. We now recommend starting custom
  forms with `[form.formprologue]`. (backported to 4.28.2)
- Standard forms (and updated custom forms) will now have `<a class="wh-anchhor"></a><div class="wh-form__prologue"></div>` injected
  into their top. We recommend using the `wh-form__prologue` for injecting any elements that should be at the top of the form. (backported to 4.28.2)
- `<sitesettings>` can match sites limited by their output URL by settings a `webrootregex` (backported to 4.28.2)
- `LimitUTF8Bytes` now accepts an optional third argument containing a string to add if it had to truncate. If you supply an ellipsis as
  the third parameter ("…", thats `ALT-:` for Mac folks) this makes this function work like UCTruncate but counting bytes rather than codepoints.
- `WidgetBase::EmbedComponent` allows you to override the Witty component to use
- The RTE now supports keyboard combos to select styles. Eg Cmd+Alt+1 to select Heading 1, (Ctrl+Alt+1 on Windows), Cmd+Alt+7 to select a list or Cmd+Alt+0 for "Normal",
  or more precisely, the first style with containertag "P". If you have multiple styles with the same container tag, the keyboard shortcut will cycle through them.

## Things you should do
- Creating foreign folders in the WebHare Backend site for CI tests is not recommended. Please switch to SetupSiteTestOutput
  for your CI tests (this API has been backported to 4.28.1)
