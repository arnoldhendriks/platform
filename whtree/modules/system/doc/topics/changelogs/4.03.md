# 4.03
## Incompatible changes
- AddDesignRequire now requires you to prefix loads from your design folder with the webdesign '->designfolder'. 4.02 assumed all requires were relative to the design folder, unless they started with '@'

## Website and module development
- .witty files in a module now automatically bind their gettid calls to the current module, eg `[gettid x.test]` in a witty inside module 'y' will now always be interpreted as `[gettid y:x.test]`.
- The languaged editor understands this autobinding for all .witty files in a module.
- WebDesignBase now adds a 'designfolder' property, which gives you the site::a/b/ or moduleroot::c/d/ path referred by the current `<webdesign designfolder=".." />`
- The new sitemgr ('wh sitemgr') allows to easily ship sites with modules
- Introduced `<moduleregistry>` and ReadModuleRegistryKey/WriteModuleRegistryKey to easier manage module registry keys
- `<textgroup>`s now support a limitlanguages attribute, limiting which languages require translation of the contained texts
- Designfiles: combined JS and CSS files now refer to a shared instance, instead of a unique one. Modifications to JS/CSS now no longer require a full republish to pick up the new URLs.
- `<apply> <to type="file" typeneedstemplate="true" />` applies to all filetypes which are marked as require a template.
- The imagecache and filecache have been merged, and the filecache now also stores data on disk

## Core APIs
- ReadDiskDirectory now returns a 'PATH' variable containing the full path to each entry
- DeleteDiskWildcardsRecursive supports deleting multiple using a path with wildcards

## WebPack
- Newly created webpacks now record who created them, and when

## WRD relations database
- GetEntiesFields(At) added as a quick GetEntityFields for multiple entities

## Buildsystem and addons
- Removed separate PCH for shared & executable
- Additional fonts can now be placed in a 'fonts' folder in your 'var' or WebHare data folder, so you don't need to clutter up the WebHare's installation dirs

## Deprecated and removed features
- Secondary outputs ('outputslaves') have been removed.
- GetPublishedDBFile no longer requires the 'outputtag' parameter.
- The <filesystem> node in moduledefinition.xsd has been deprecated. Move all contents to a <publisher> node.
- runmodel (for hooks, apps, etc) and integratedonly (for hook targets) are no longer required or used
- <list rowselect> no longer does anything and can be removed
- The old email queue has been deprecated as email is now handled by the managed task queue. To be able to see/deal with old email, the old mailqueue is still visible in the monitor
- You shouldn't append filenames to the end of cached image/file link anymore, but instead pass a [ filename := 'frogger.jpg' ] option to the call (it's still supported but will perform worse)
- MakeBlobFromDiskFile has been deprecated. GetDiskResource is now recommended, which accepts relative paths and throws when failing to open a file.
