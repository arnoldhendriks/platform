# 5.1 - 10 October 2022

PLEASE NOTE! YOU NEED TO UPGRADE TO 4.35 BEFORE UPGRADING TO 5.0 or 5.1

## Incompatibilities and deprecations
- The `needsprofile` attribute is no longer allowed (was already ignored and deprecated, and removed by dev:rewrite)
- The `<wrdauth api2017=` attribute has been dropped. (was already ignored and deprecated, and removed by dev:rewrite)
- Support for MD5 and SHA1 RSA signatures has been dropped
- The `Create_RSA_Hash`, `Verify_RSA_SHA1`, `Create_RSA_SHA1` and `Create_RSA_Hash` APIs have been removed. You should
  use the %MakeCryptoKey API with `Verify` and `Sign`.
- The global `OpenForm` API has been removed. You should always open forms through a webcontext.
- `PrepareForFrontend` is/must now also invoked for non-webtool forms. %OpenWebtoolForm and %OpenFormByTarget have added
  a `prepareforfrontend` option which you can set to FALSE if you don't need frontend preparation.
  - This eliminates a subtle difference between custom and webtool forms but might require you to check for `IsRequest()`
    in PrepareForFrontend.
- Support for WRD Stored queries and the `wrd:queryresults` component has been dropped
- You can no longer change the aspect ratio of an embedded RTD image (this checkbox has been removed). For existing images
  we will only consider the width (which is what effectively happened anyway with the default 'max-width:100%; heigh:auto' styling)
- External images in RTDs (which were already barely supported) are now completely ignored in the output
- The RTD editor no longer uses the CSS class `wh-rtd-table` - rtd.css files should switch to `wh-rtd__table` (just like the sites already did)
- The SEO `headertitle` field has been dropped. The SEO title remains.
- The `openmultipleinstances` option has been deprecated and is now ignored. All applications support multiple instances (you
  could work around a missing openmultipleinstances by opening a new browser anyway). If your application does not support
  multiple instances you should actually enforce it by using the exclusive access APIs.
- The dashboard now adopts the spacers set on the body of loaded panels. You may need to explicitly set `<body spacers="none"`
  on custom dashboard panels.

## Things that are nice to know
- The `formref` option is now also passed to webtool forms
- Images in the RTD do not receive a forced width/height after opening the dialog for the first time, it'll be an explicit
  checkbox "Override dimensions" now.
- An `opacity` property has been added to the Tollium `<image>`
- Adds `limitnetworks=` to `<videowidget/>` alllowing you to limit video network selection in the RTD
- Any user can now enable 'side tabs' in the Publisher object properties dialog through a Publisher preference. This feature
  is experimental
- Core applications are starting to support embedded remote documentation. You'll still need to explicitly opt in on the
  Services tab in the WebHare config application.
- There is no longer a difference between updating or rebuilding a catalog - we always assume a rebuild. (Consilio APIs will
  drop the `rebuild` option completely in a future update)
- You should add the `consilio:sitecontent` fieldgroup to catalogs that will receive Publisher frontend sites to prevent 
  races during setup of the catalogs. (a future version of WebHare may even require this)
- A new form debug flag `fnh` disables component/page hiding for easier form debugging
- Adds the 'publisher:advanced' access right which controls access to Formeditor and Publisher objecteditor Advanced tabs.

## Things you should do
