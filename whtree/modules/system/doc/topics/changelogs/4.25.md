# 4.25 - 2019-08-12

## Incompatibilities and deprecations
- WebHare will not run when /opt/whdata is an overlayfs volume unless you explicitly set WEBHARE_ALLOWEPHEMERAL=1
- Support for `<member publish=true />` will be removed in a future version as it causes excessive database traffic when
  calling GetInstanceData. If used, it will generate a warning in siteprofiles. On development servers all `link`
  fields (which are normally a `STRING`) will be replaced by a `CELL[ linkisobsolete := FALSE ]` to ensure any code accessing
  it as a string will fail
  - we could have removed it or made it a default value, but we need `[if link][link][/if]` to fail
  - note that simply wrapping an image record in `WrapCachedImage(img, [method := "none"])` will satisfy a lot of old
    code that only expected `width`, `height` and `link` to exist
- Support for `<dynamicexecution/>` in `<apply>` rules has been removed. It was confused when dealing with content links
  and never worked right if you didn't also set the filetype to dynamicexecution.
- The deprecation scanner has been removed
- `<vmrights>` is no longer allowed in XML files (it hasn't been supported for years)
- EncryptForCluster and DecryptForCluster have been removed

## Things you should do
- The margins and positions of widgets have been updated, this may cause issues if you've overridden the margins for
  `html` and/or `body` for the RTD editor. If you need, you can test for the `wh-rtd--margins-wide`, `wh-rtd--margins-compact`
  , `wh-rtd--margins-none` and/or `wh-rtd--margins-active` classes on the pseudo-html element to see which margin mode is in
  effect. You can also use this to test for a pre-4.25 WebHare (just check for either `wh-rtd--margins-active` or `wh-rtd--margins-none`)

## Things that are nice to know
- `<richdocument>` now supports a `margins=none|compact|wide` to give more control over the horizontal margins used
- `<rtddoc>` supports `margins=none|compact|wide` too. This setting will be used by the Publisher documenteditor
- Reflect now has a return value so it can be used in an expression. It will simply return whatever was passed to it
- You can now use domain tags as a value in WRD query filters (you can replace `domainvaluetag` with `value`)
- `capturesubpaths=` is now compatible with `isacceptableindex=`. If you start using this, make sure your subpath handler
  properly returns a 404 code for all unknown URLs or you will break the URL History for that folder. Your users will
  also need to understand that anything they create in the same folder will override your capturing file.
- Setting a form `<select>` pulldown placeholder from HareScript fixed for statically created selects.
- API added to send SMS messages using AWS.
- `<template>.content` has been added to the global polyfill, so you can use .cloneNode on it in IE11.
- 'Run from sublime' now accepts defaults arguments for scripts by putting them in an '.args' file
- Added support for [Elasticsearch index definitions in module defintions](topic:consilio/module-indices).
