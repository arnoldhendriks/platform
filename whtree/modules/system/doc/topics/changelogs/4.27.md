# 4.27 - 2020-01-22

## Incompatibilities and deprecations
- *PLEASE NOTE:* WebHare 4.26 is NOT skippable. You must upgrade to 4.26 before upgrading to 4.27
- We no longer create `/webhare-moduledata/` in WHFS - modules should use `/webhare-private/`.
  - Note that 4.26 added %OpenWHFSPrivateFolder to find your module's private folder and ensures that this folder is always
    created.
- Support for mixed-case webserver output has been removed. Lowercase output paths are enforced for all servers
- Support for `editcomponent=` on widget types has been removed.
- We no longer generate `wh-rtd__anchor` classes, only `wh-anchor` (a few previous versions generated both for the transition phase)
- The `google` substitution module has been removed. If you had an old or empty version of it, you may still need to `wh module rm google`
  to completely remove it. You may also need to drop the `google` schema in the database
- `tolliumcontroller->debug` and `ScreenBase::debug` have been removed
- Support for bower has been dropped.
- `StartPreciseTimer`, `StopPreciseTimer` and `GetUnixExtendedTimestamp` have been removed. Use %GetUnixTimestampMsecs
  - `GetUnixTimestampMsecs(GetCurrentDatetime())` gives you an INTEGER64 contaning the current number of msecs.
- Accessing registry keys starting with `system.modules.` or `modules.` will now trigger an error on development servers.
- Old registry APIs have been removed:
  - The freestanding `SetRegistryKey` and `GetRegistryKey` (which accepted a role id) have been removed. This does not affect the `TolliumUser` registry APIs.
  - `ReadModuleRegistryKey` and `WriteModuleRegistryKey` have been removed.
- Potential WHDB/Postgres compatibility issues:
  - Role APIs `GetWHDBRoleId` and `IsWHDBRoleEnabled` have been removed.
  - `MakeWHDBAutoNumber` was already deprecated, and is now removed. Switch to %MakeAutonumber.
  - All other WHDB APIs have been marked for deprecation as relying on anything from `wh::dbase/whdb.whlib` is unsafe now.
  - Old SSHA1-hashed passwords with binary data in WRD are converted to base64. These passwords cannot be checked anymore by older
    WebHare versions (eg if you migrate WRD data to an older version)
- %IsValidUTF8 has been aligned more closely with PostgreSQL and now always rejects NUL (`\0`) bytes and surrogate pairs.
- The legacy image cache (GetCachedRemoteDataURL and GetCachedRemoteImgURL) has been removed.
- wkhtmltopdf and qpdf have been removed
- `WRDLegacyType::GetDomVals` now throws on test and development servers and should be replaced with `ListDomVals`
- The `WebHare usermanagement` schema has been renamed to `system:usermgmt`
- %EncodeHTML and %EncodeTextNodeValue will now filter out characters in the 128-159 range (ie the [UNICODE C1 controls](https://en.wikipedia.org/wiki/C0_and_C1_control_codes#C1_controls))
- Error handlers now look at the exact file/folder's `<webdesign>` settings to determine `supportserrors` and `supportsaccessdenied`
  instead of the site's root. This shouldn't affect you unless you apply `<webdesign>` settings very specifically.
- Siteprofile `<webdesign>` nodes no longer accept `supportedlanguages=` - you should set it on the `<assetpack>` in the module definition.
- `WRDType::GetBulkFields` has been removed - use `WRDType::Enrich`.
- All APIs for setting and getting entity values at a specified point in time have been removed. These are among others
  `GetEntitiesFieldsAt`, `GetEntityFieldAt`, `GetEntityModificationDates` in the legacy WRD type object, `UpdateFieldsAt`,
  `UpdateFieldsAt2`, `GetModificationDates`, `GetEntityFieldsAt` in legacy WRD entity object and `CreateEntityAt`,
  `UpdateEntityAt` and `StoreEntityAt` in the tollium WRD entity composition.
- Some experimental and obsolete flash-related filetypes have been removed.
- `<sitelanguage getlanguagefunction>` introduced in 4.20 didn't seem that useful after all and has been removed.
- widgettypes using `editfragment=` are not properly initialized and this may break some components (such as the rewritten `<p:intextlink>`).
  you should switch to `editextension=` (available since 4.23)
- `WebdesignBase::GetcachableSiteconfig` has been removed, use `WebdesignBase::GetCacheableSiteConfig`
- It's no longer accepted to specify a `<webdesign objectname=>` without a `library=` (unless the objectname is a (relative) path, ie it contains a `#`)
- User management no longer allows users in the root unit. This has the following consequences:
  - The `WHUSER_UNIT` in the system usermgmt schema site now required.
  - The testramework will not setup a userapi for the testschema unless `wrdauth := TRUE` is explicitly set as a test option.
  - `wrdauth := TRUE` is automatically set if the test created testusers or testroles.
  - You'll need to pass a `WHUSER_UNIT` in tests if wrdauth is enabled for the test schema. Use `testfw->testunit` (testunit will be backported to 4.26)
  - Any users, units and roles in deleted units will be moved to an automatically created "Lost & found" unit.
- `runmodel`, deprecated since 4.03, has finally been removed
- `TolliumWRDEntity::fields` was broken since the 2017 API but hardly ever used (as it wasn't noticed yet). It has been removed, replace its use
  with `GetComposedCellnames()`

## Things that are nice to know
- Fixes a bug causing images to be deleted if they were added from the publisher and their original was deleted
- WebDesignBase and WidetBase now offer GetRTDBody which wraps OpenRTD and RenderAllObjects in a witty friendly way (by returning
  a default value when the document is empty). This allows you to replace the pattern
  `RecordExists(richdoc) ? PTR webdesign->OpenRTD(richdoc)->RenderAllObjects : DEFAULT MACRO PTR` with just `webdesign->GetRTDBody(richdoc)`
- %RequireHTTPLogin adds a simple way to protect pages with basic HTTP authentication. `wh registry set` now hash a `--password` option to directly set
  a safe password
- A builtin webdesign `No webdesign` has been added for if you just want to upload a few HTML files without any design into the publisher
- `none` is now an acceptable SameSite value in UpdateWebCookie. See also https://blog.chromium.org/2019/10/developers-get-ready-for-new.html
- Added %ConvertContentType to convert between content types (eg to migrate old data or merge similar types)
- New LetsEncrypt certificates can now be requested through the webserver application
- HareScript error pages are only shown in the webdesign when the 'etr' debugflag is unset. Once you've enabled error tracing HareScript
  errors are hosted by the Tollium webinterface design, preventing truncation/overflow hiding (parts of) error messages.
- Sites using `<wrdauth>` plugin automatically receive a `wrdauth` cell in their page witty data containing the WRDAuthPlugin::GetWittyData() information
- RTD tables now support table cell level styling through `<cellstyle>`
- Adds a standard `[sitelanguage]` to the pageconfig with a boolean for each language code supported in the assetpack, to determine
  which language code is currently active
- `<intextlink>` now supports `<linkhandlers>`. You can pass these links through `WebDesignBase::GetIntextlinkTarget` to apply the same rewrites
  that you would apply to custom links in RTDs
- `/.px/` urls are now logged to a separate log file for easier analysis
- Renamed 'Raw HTML code' to 'HTML block' which is more common name
- WRD type members `Enrich`, `GetEntityFields` and `GetEntityField`, and WRD entity members `GetField` and `GetFields` now also support the `cachettl` option
- Tabsextensions now receive their provided extended components as hatted properties, ie a tabsextension can now do `^contentdata->...`
- Compositions now support 'virtual cells' using %TolliumCompositionBase::RegisterVirtualCell - this allows eg. tabsextensions to capture
  set/gets to a composition (`^contentdata->RegisterVirtualCell("datacell", PTR this->GetDataCell, PTR this->SetDataCell)`)
- Adds `wh rewrite` which reformats XML files (and does some automatic conversions, such as `editfragment=` to `editextension=`)
- HS error pages now have links leading you directly towards the proper editor location (assuming you have dev-agent properly set up)
- `requireversion=` was unclear and had conflicting definitions when used on a module dependency. Switch to `webhareversion=`
   to match the WebHare version and `moduleversion=` to match a module version. Both expect a [semver range](https://www.npmjs.com/package/semver#ranges)

## Things you should do
- Set the flag `webdesign->__anchor_experiment` to opt into a simplified anchor model which doesn't require you to hook GatherSuggestedAnchors
  to get anchors working in eg. subwidgets and custom filetypes. It also sets up anchors for form grouptitles.
  Feedback is welcome, if this new model works better than the current setup it might replace it completely.
- WebHare allows you to configure automatic permanent deletion of files which should be used for eg. caches and backups containing potential
  sensitive data. See https://www.webhare.dev/manuals/security/sensitive-data/
- Replace `editfragment=` with `editextension=`, especially for widgets that contain an intextlink.
