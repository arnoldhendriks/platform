# Appendix 1: Listing of reserved words
The following keywords are reserved, and may not be used as a function, macro, variable
or cell name. Some already have a defined use in the HareScript language, others are
reserved for future extensions:

| Keyword | | | |
| --- | --- | --- | --- |
| \_\_Attributes__ | Continue | Inner | Ref |
| __WithAsyncContext | Cross | Insert | Return |
| Aggregate | DateTime | Integer | Schema |
| All | Default | Integer64 | Sealed |
| And | DefaultsTo | Intersect | Select |
| Array | Delete | Into | Set |
| As | Desc | Join | Static |
| Asc | Distinct | Key | String |
| Async | Else | Like | Switch |
| At | End | Limit | Table |
| Await | Except | Loadlib | Temporary |
| BitAnd | Export | Macro | This |
| BitLShift | Extend | Member | Throw |
| BitNeg | ExtendsFrom | Money | True |
| BitOr | False | New | Try |
| BitRShift | Finally | Not | TypeId |
| BitXor | FixedPoint | Null | Unique |
| Blob | Float | Nvl | Update |
| Boolean | For | Object | Using |
| Break | ForEach | ObjectType | Values |
| By | Forevery | Offset | Var |
| Case | From | Only | Variant |
| Catch | Full | Or | VarType |
| Cell | Function | Order | WeakObject |
| ClassType | Goto | Outer | Where |
| Column | Group | Private | While |
| Concat | Having | Property | Xor |
| Const | If | Ptr | Yield |
| Constant | In | Public |  |
| Constexpr | Index | Record |  |
