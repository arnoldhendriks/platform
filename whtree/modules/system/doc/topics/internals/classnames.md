# Class names used by WebHare

Use this to create consistent new class names. We try to follow (more or less) the
BEM philosophy for new classes.

## Widgets and RTD
`a.wh-anchor` - Anchors inserted by widgets, forms and RTDs. The `id` contains the actual anchor (we never use the `name` attribute)

## Removed class names
`.wh-rtd__anchor` - Too RTD specific, forms create anchors now too. Use `wh-anchor` instead

