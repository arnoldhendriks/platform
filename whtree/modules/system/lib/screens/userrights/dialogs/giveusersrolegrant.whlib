﻿<?wh

LOADLIB "mod::system/lib/screens/userrights/support.whlib";
LOADLIB "mod::tollium/lib/screenbase.whlib";


/** This dialog adds a pre-chosen role to selectable users
*/
PUBLIC STATIC OBJECTTYPE GiveUsersRoleGrant EXTEND TolliumScreenBase
< // ---------------------------------------------------------------------------
  //
  // Variables
  //

  /** Role to grant
  */
  OBJECT pvt_roleobj;

  /// Select user/role panel
  OBJECT selectuserrolepanel;

  // ---------------------------------------------------------------------------
  //
  // Properties
  //

  /// Currently selected unit
  PUBLIC PROPERTY unit(GetUnit, SetUnit);

  /// Current selected users
  PUBLIC PROPERTY grantees(GetGrantees, SetGrantees);

  /// Role to grant
  PUBLIC PROPERTY roleobj(pvt_roleobj, -);

  // ---------------------------------------------------------------------------
  //
  // Constructor & tollium admin
  //

  /** @param data
      @cell data.grantee
  */
  MACRO Init(RECORD data)
  {
    this->pvt_roleobj := data.roleobj;

    ^selectuserrole->InitForCommonDialog();
    ^selectuserrole->filterontype := 1;
    ^selectuserrole->selectmode := "multiple";
    ^selectuserrole->unit := this->contexts->userapi->GetUnitOf(this->pvt_roleobj);

    this->frame->title := GetTid('system:userrights.commondialogs.granttitles.titlespecificroletousers', this->pvt_roleobj->GetUserRightsName());
  }

  // ---------------------------------------------------------------------------
  //
  // Other tollium stuff
  //

  MACRO UserRoleClicked()
  {
    IF (this->Submit())
      this->tolliumresult := "ok";
  }

  BOOLEAN FUNCTION Submit()
  {
    OBJECT work := this->BeginWork();
    IF (work->HasFailed())
      RETURN work->Finish();

    IF (LENGTH(^selectuserrole->value) = 0)
    {
      work->AddError(GetTid("system:userrights.commondialogs.errors.needselectuser"));
      RETURN work->Finish();
    }

    TolliumUpdateRoleGrant(this->contexts, work, "grant", this->pvt_roleobj, this->grantees, [ comment := ^comment->value ]);
    RETURN work->Finish();
  }

  // ---------------------------------------------------------------------------
  //
  // Getters/setters
  //

  OBJECT ARRAY FUNCTION GetGrantees()
  {
    RETURN ^selectuserrole->value;
  }

  MACRO SetGrantees(OBJECT ARRAY grantees)
  {
    ^selectuserrole->value := grantees;
  }

  OBJECT FUNCTION GetUnit()
  {
    RETURN ^selectuserrole->unit;
  }

  MACRO SetUnit(OBJECT unit)
  {
    ^selectuserrole->unit := unit;
  }
>;
