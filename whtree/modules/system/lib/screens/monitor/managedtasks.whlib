<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::ipc.whlib";
LOADLIB "wh::promise.whlib";
LOADLIB "wh::util/algorithms.whlib";

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/screenbase.whlib";
LOADLIB "mod::system/lib/services.whlib";
LOADLIB "mod::system/lib/tasks.whlib";
LOADLIB "mod::tollium/lib/screenbase.whlib";

/* Direct URLs:

   To prefill mask & filter pulldown (note: task list is populated automatically):
   https://my.webhare.dev/?app=system:dashboard(system%3Amanagedtasks,*,all)
*/

PUBLIC INTEGER FUNCTION GetTaskStatusIcon(RECORD task)
{
  RETURN task.isscheduled ? 7 : task.isrunning ? 6 : task.iscancelled ? 4 : task.lasterrors != "" ? 2 : task.finished != DEFAULT DATETIME ? 3 : 1;
}

PUBLIC STATIC OBJECTTYPE Panel EXTEND DashboardPanelBase
<
  INTEGER ARRAY runningtasks;
  BOOLEAN queryloopactive;

  MACRO Init()
  {
    IF (MemberExists(this->tolliumparent, "screenparams")
        AND Length(this->tolliumparent->screenparams) > 1
        AND this->tolliumparent->screenparams[0] = "system:managedtasks")
    {
      ^taskmask->value := this->tolliumparent->screenparams[1];
      IF (Length(this->tolliumparent->screenparams) > 2)
        ^taskfilter->SetValueIfValid(this->tolliumparent->screenparams[2]);
    }

    this->DoUpdateFilter();
  }

  PUBLIC MACRO OnMessage(RECORD message)
  {
    IF (CellExists(message, "TAG"))
    {
      ^taskmask->value := message.tag;
      ^taskfilter->value := "pending";
      this->DoUpdateFilter();
    }
  }

  MACRO DoUpdateFilter()
  {
    IF(^taskmask->value = "")
      ^managedtasks->empty := this->GetTid(".notaskmask");
    ELSE
      ^managedtasks->empty := this->GetTid(".notaskmatching", ^taskmask->value);

    this->RefreshDashboardPanel();
  }

  UPDATE PUBLIC INTEGER FUNCTION GetSuggestedRefreshFrequency()
  {
    RETURN 0;
  }

  /** @short This monitor has been activated, start refreshing */
  UPDATE PUBLIC MACRO EnableRefresh()
  {
    ^listener->enabled := TRUE;
    this->RunQueryActiveTaskListLoop();
  }
  /** @short This monitor has been deactivated, stop refreshing */
  UPDATE PUBLIC MACRO DisableRefresh()
  {
    ^listener->enabled := FALSE;
  }

  ASYNC MACRO RunQueryActiveTaskListLoop()
  {
    // Update list of running task every second while the listener is active, refresh list when it changes
    IF (this->queryloopactive OR NOT ^listener->enabled)
      RETURN;

    this->queryloopactive := TRUE;
    TRY
    {
      OBJECT taskservice := AWAIT OpenWebHareService("system:managedqueuemgr", [ arguments := VARIANT[ -1 ] ]);
      TRY
      {
        WHILE (^listener->enabled)
        {
          RECORD ARRAY runningtasks := AWAIT taskservice->GetRunningTasks();

          INTEGER ARRAY nowrunning :=
              SELECT AS INTEGER ARRAY id
                FROM runningtasks
               WHERE NOT isephemeral;

          IF (NOT ArrayIsSetEqual(this->runningtasks, nowrunning))
          {
            this->runningtasks := nowrunning;
            this->RefreshDashboardPanel();
          }

          RECORD p := CreateDeferredPromise();
          RegisterTimedCallback(AddTimeToDate(1000, GetCurrentDateTime()), PTR p.resolve(TRUE));
          AWAIT p.promise;
        }
      }
      FINALLY
        taskservice->CloseService();
    }
    CATCH (OBJECT e) // monitoring app shouldn't fill the logs
    {
      PRINT(`Error in managed task monitor: ${e->what}\n`);
    }

    this->queryloopactive := FALSE;
    this->runningtasks := INTEGER[];
  }

  UPDATE PUBLIC MACRO RefreshDashboardPanel()
  {
    IF(^taskmask->value = "")
    {
      ^managedtasks->rows := RECORD[];
      RETURN;
    }

    DATETIME now := GetCurrentDateTime();
    BOOLEAN filter := ^taskfilter->value != "all";
    BOOLEAN showcompleted := ^taskfilter->value IN ["completed"];
    BOOLEAN showscheduled := ^taskfilter->value IN ["future"];

    RECORD ARRAY rows := SELECT rowkey := managedtasks.id
                              , nextattempt := finished = DEFAULT DATETIME ? nextattempt : DEFAULT DATETIME
                              , creationdate
                              , lasterrors
                              , tasktype
                              , finished
                              , iscancelled
                              , isrunning := id IN this->runningtasks
                              , isscheduled := notbefore > now
                              , canreschedule := finished != DEFAULT DATETIME OR nextattempt != DEFAULT DATETIME
                           FROM system.managedtasks
                          WHERE tasktype LIKE ^taskmask->value
                                AND (filter AND showcompleted ? managedtasks.finished != DEFAULT DATETIME : TRUE)
                                AND (filter AND NOT showcompleted ? managedtasks.finished = DEFAULT DATETIME : TRUE)
                                AND (filter AND showscheduled ? managedtasks.notbefore > now : TRUE)
                                AND (filter AND NOT showscheduled ? managedtasks.notbefore <= now : TRUE);

    rows := SELECT *
                 , status := GetTaskStatusIcon(rows)
                 , hasfailed := finished != DEFAULT DATETIME AND lasterrors != ""
              FROM rows;

    // ADDME: refresh when next scheduled item becomes runnable (refresh won't be triggered
    // when executing a long-running task)

    ^managedtasks->rows := rows;
  }

  MACRO OnManagedTaskEvent(RECORD ARRAY events)
  {
    this->RunQueryActiveTaskListLoop();
    this->RefreshDashboardPanel();
  }

  MACRO DoFlush()
  {
    OBJECT work := this->BeginWork();
    RetryPendingManagedTasks("*");
    work->Finish();
    //this->RefreshDashboardPanel();
  }

  MACRO DoView()
  {
    OBJECT diag := this->LoadScreen(".managedtask", [ id := ^managedtasks->value[0] ]);
    diag->RunModal();
  }
  MACRO DoReschedule()
  {
    IF(this->RunSimpleScreen("confirm", this->GetTid(".confirmrescheduletasks")) != "yes")
      RETURN;

    OBJECT work := this->BeginWork();

    RECORD ARRAY tasks :=
        SELECT id, finished, nextattempt
          FROM system.managedtasks
         WHERE id IN ^managedtasks->value;

    INTEGER ARRAY finished_ids := SELECT AS INTEGER ARRAY id FROM tasks WHERE finished != DEFAULT DATETIME;
    INTEGER ARRAY pending_ids := SELECT AS INTEGER ARRAY id FROM tasks WHERE finished = DEFAULT DATETIME AND nextattempt != DEFAULT DATETIME;

    RescheduleManagedTasks(finished_ids);
    RetryPendingManagedTasksByIds(pending_ids);

    work->Finish();
  }
  MACRO DoDelete()
  {
    IF(this->RunSimpleScreen("confirm", this->GetTid(".confirmdeletetasks")) != "yes")
      RETURN;

    OBJECT work := this->BeginWork();
    DeleteManagedTasks(^managedtasks->value);
    work->Finish();
  }
>;

PUBLIC STATIC OBJECTTYPE ManagedTask EXTEND TolliumScreenBase
<
  INTEGER taskid;

  MACRO Init(RECORD data)
  {
    this->taskid := data.id;
    ^listener->masks := [ `system:managedtasks.any.${this->taskid}` ];
    this->Refresh();
  }

  MACRO OnChangeEvents(RECORD ARRAY events)
  {
    this->Refresh();
  }

  MACRO Refresh()
  {
    RECORD task := DescribeManagedTask(this->taskid);
    IF(NOT RecordExists(task))
    {
      this->tolliumresult := "cancel";
      RETURN;
    }

    RECORD showdata := task.data;
    IF(task.tasktype = "system:outgoingmail" AND CellExists(showdata,'sensitive') AND showdata.sensitive AND NOT this->contexts->user->HasRight("system:sysop"))
    {
      //TODO generalize for all task types?
      //we assume a subject is never sensitive (as that would be too easily shoulder surfed anyway?) but mailcontents can be
      //TODO at some point we should only grant sysops access to 'raw' task data and use the normal mailviewer for other task users
      DELETE CELL toppart FROM showdata;
    }

    ^id->value := ToString(this->taskid);
    ^tasktype->value := task.tasktype;
    ^creationdate->value := task.creationdate;
    ^nextattempt->value := task.nextattempt;
    ^finished->value := task.finished;
    ^failures->value := ToString(task.failures);
    ^iterations->value := ToString(task.iterations);
    ^notbefore->value := task.notbefore = DEFAULT DATETIME ? "" : this->GetTid(".notbefore", this->contexts->user->FormatDateTime(task.notbefore, "seconds", TRUE, TRUE));
    ^lasterrorsbox->visible := task.lasterrors != "";
    ^lasterrors->value := task.lasterrors;
    ^data->value := showdata;
    ^retval->value := task.retval;
    ^manualcall->value := "wh tasks run m" || this->taskid;
    ^metadata->value := task.metadata;
    ^timeout->value := task.timeout = 0 ? GetTid("~none") : (task.timeout % 1000) = 0 ? `${task.timeout / 1000} s` : `${task.timeout} ms`;

    ^frame->flags.canreschedule := task.finished = DEFAULT DATETIME AND task.nextattempt > GetCurrentDatetime();
  }

  MACRO DoReschedule()
  {
    OBJECT work := this->BeginWork();
    RetryPendingManagedTasksByIds([ this->taskid ]);
    work->Finish();
    this->Refresh();
  }
>;
