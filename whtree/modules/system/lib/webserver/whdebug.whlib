﻿<?wh
/** @topic sitedev/dynamic
*/

LOADLIB "wh::crypto.whlib";
LOADLIB "wh::devsupport.whlib";
LOADLIB "wh::internal/debug.whlib";
LOADLIB "mod::system/lib/webserver.whlib";
LOADLIB "mod::system/lib/internal/cluster/secrets.whlib";
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";

BOOLEAN outputtoolsactive;
BOOLEAN didsetupprofiling;

MACRO ProcessWHDebugVariables(BOOLEAN skipchecks)
{
  STRING debugcookie := GetWebCookie("wh-debug");
  IF(debugcookie != "")
  {
    STRING ARRAY toks := Tokenize(debugcookie,'.');
    STRING hash := toks[END-1];
    IF(skipchecks OR ValidateSignatureForThisServer("publisher:wh-debug", Left(debugcookie, Length(debugcookie) - Length(hash) - 1), Substring(hash,4)))
      __webdebugsettings := ArraySlice(toks, 0, Length(toks)-1);
  }

  STRING debugvars := GetWebVariable("wh-debug");
  iF(debugvars != "")
  {
    FOREVERY(STRING opt FROM Tokenize(debugvars,','))
      IF(skipchecks OR opt IN whconstant_whdebug_publicflags) //we permit -some- URL flags, most specifically 'apr'
        INSERT opt INTO __webdebugsettings AT END;
  }

  IF("apr" IN __webdebugsettings)
  {
    IF(NOT didsetupprofiling)
    {
      didsetupprofiling := TRUE;
      SetupFunctionProfiling("webserver", GetRequestMethod() || " " || GetRequestUrl());
    }
  }
}

/** Generate a signed debug setting string, suitable for the wh-debug cookie
    @param opts Debug options
    @return Signed cookie data
*/
PUBLIC STRING FUNCTION GetSignedWHDebugOptions(STRING ARRAY opts)
{
  IF(Length(opts) = 0)
    RETURN "";

  STRING debugcookie := Detokenize(opts,'.');
  RETURN debugcookie|| ".sig=" || GetSignatureForThisServer("publisher:wh-debug",debugcookie); //adding .sig is backwards compatible with old dompacks, as it tokenizes on '.'
}

/** Generate the debug cookie for the webserver
    @param opts Debug options
    @return wh-webserverdebug cookie contents
*/
PUBLIC STRING FUNCTION GetWebserverDebugCookie(STRING ARRAY opts)
{
  STRING ARRAY webserver_hashtokens;
  STRING debugsecret := GetClusterKeyBase("debugsecret");

  FOREVERY (STRING opt FROM opts)
  {
    IF (opt = "wud")
      INSERT Left(EncodeUFS(GetHashForString("system:webserver-urldebugging" || debugsecret, "SHA-256")), 10) INTO webserver_hashtokens AT END;
    ELSE IF (opt LIKE "wst=*")
    {
      FOREVERY (STRING s FROM Tokenize(SubString(opt, 4), ","))
        INSERT Left(EncodeUFS(GetHashForString("system:webserver-skiptry." || s || debugsecret, "SHA-256")), 10) INTO webserver_hashtokens AT END;
    }
  }
  RETURN Detokenize(webserver_hashtokens, ".");
}

/** Explicitly accept wh-debug settings without checking. Should only be used for testing, as this can cause eg information leaks and captcha avoidance! */
PUBLIC MACRO ProcessWHDebugVariablesUnchecked()
{
  ProcessWHDebugVariables(TRUE);
}

/** Test whether the specific debug option is set */
PUBLIC BOOLEAN FUNCTION IsWHDebugOptionSet(STRING opt)
{
  RETURN IsDebugTagEnabled(ToLowercase(opt));
}

IF(IsRequest())
  ProcessWHDebugVariables(FALSE);
