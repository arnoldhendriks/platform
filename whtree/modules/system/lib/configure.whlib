﻿<?wh
/** @short Configuration
    @long Functions used to manage the WebHare system configuration
    @topic modules/config */

LOADLIB "wh::adhoccache.whlib";
LOADLIB "wh::os.whlib";
LOADLIB "wh::promise.whlib";
LOADLIB "wh::util/semver.whlib";
LOADLIB "wh::internal/callbacks.whlib";
LOADLIB "wh::internal/transbase.whlib";

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/internal/registry.whlib" EXPORT ReadRegistryNode, DeleteRegistryKey, ReadRegistryKey, DeleteRegistryNode, WriteRegistryKey, GetRegistryKeyEventMasks, ReadRegistryKeysByMask;
LOADLIB "mod::system/lib/internal/services.whlib";
LOADLIB "mod::system/lib/internal/modules/defreader.whlib";
LOADLIB "mod::system/lib/internal/whcore_interface.whlib" EXPORT GetModuleInstallationRoot;
LOADLIB "mod::system/lib/internal/whconfig.whlib" EXPORT GetDTAPStage, IsNodeApplicableToThisWebHare, IsRestoredWebHare;
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";
LOADLIB "mod::system/lib/internal/modules/version.whlib" EXPORT GetWebhareVersionNumber;

/** Base object for custom WebHare checks
*/
PUBLIC OBJECTTYPE CustomCheckBase
<
  /** Return list of issues (readable text)
      @return List of issues
  */
  PUBLIC STRING ARRAY FUNCTION GetIssues()
  {
    THROW NEW Exception(`CustomCheck does not override GetIssues or GetIssueList`);
  }

  /** Returns extended list of issues
      @return List of issues
      @cell return.msg Tid with message text (prefix with ':' to pass fixed texts)
      @cell return.jumpto Information about where to jump to where more information can be found (message to send to a tollium application)
      @cell return.jumpto.app Tollium application to start
  */
  PUBLIC RECORD ARRAY FUNCTION GetIssueList()
  {
    RETURN SELECT msg := ":" || msg FROM ToRecordArray(this->GetIssues(),'msg');
  }
>;

/** @short Get WebHare version and branding information
    @return A record describing the version
    @cell(string) return.version Semantic version (eg 4.26.3)
    @cell(integer) return.versionnum Version as number (eg 42603) - the IFVERSION number which should generally be used for version checks
    @cell(string) return.builddate Build date (not set for developer's builds)
    @cell(string) return.buildtime Build time (not set for developer's builds)
    @cell(string) return.committag Git commit tag (not set for developer's builds)
    @cell(string) return.branch Git branch of this build (not set for developer's builds)
    @cell(boolean) return.docker True if we're running inside docker
*/
PUBLIC RECORD FUNCTION GetWebhareVersionInfo()
{
  RECORD versioninfo := CELL[ ...__SYSTEM_WEBHAREVERSION()
                            , builddate := ""
                            , buildtime := ""
                            , branch := ""
                            , committag := ""
                            , ...GetWebhareBuildInfo()
                            , docker := GetEnvironmentVariable("WEBHARE_IN_DOCKER") != ""
                            ];

  RETURN versioninfo;
}

/** @short Is a specific module installed ?
    @param modulename Name of the module to test
    @return TRUE if the module is installed
*/
PUBLIC BOOLEAN FUNCTION IsModuleInstalled(STRING modulename)
{
  RETURN GetModuleInstallationRoot(modulename) != "";
}

/** @short Get the disk storage root for module specific data
    @param modulename Module name
    @return Absolute disk path to use as base for module specific storage, always ends with a slash */
PUBLIC STRING FUNCTION GetModuleStorageRoot(STRING modulename)
{
  //ensure valid module name - this especially helps countering random folder names when people make typos and pass it straight to CreateDiskDirectoryRecursive
  IF(modulename != ToLowercase(modulename) OR NOT IsModuleInstalled(modulename))
    THROW NEW Exception(`No such module '${modulename}'`);
  RETURN GetWebHareConfiguration().basedataroot || "storage/" || modulename || "/";
}

/** @short Get the version of this WebHare installation
    @return The WebHare version (eg "WebHare Platform 4.26.3") */
PUBLIC STRING FUNCTION GetWebhareVersion()
{
  RETURN "WebHare Platform " || GetWebhareVersionInfo().version;
}
/** @short Get the name for this server
    @return The name for this server */
PUBLIC STRING FUNCTION GetServerName()
{
  RETURN GetcachedWebHareConfig().servername;
}

/** @short Get the name for this process
    @return Process info
    @cell(integer) return.pid Process PID
    @cell(integer64) return.processcode Process code (from whmanager registration)
    @cell(string) return.clientname Client name of this process
*/
PUBLIC RECORD FUNCTION GetProcessInfo()
{
  RETURN __SYSTEM_GETPROCESSINFO();
}

/** @short Get the installation directory
    @long Retrieve the WebHare installation directory
    @return The WebHare installation directory (always ending in a slash, eg "/opt/webhare/") */
PUBLIC STRING FUNCTION GetInstallationRoot()
{
  RETURN GetWebHareConfiguration().installationroot;
}

/** @short IS this a 'considered live' environment
    @return True if this server's DTAP type is production or acceptance*/
PUBLIC BOOLEAN FUNCTION IsDTAPLive()
{
  RETURN GetDTAPStage() IN ["production","acceptance"];
}

/** @short Is true if we're either on production, or acting as-if
    @long Like GetDTAPStage() == "production" but can be explicitly enabled by using `wh debug setconfig asprod`. This may
          be used to test production-only workflows or dialogs
    @return True if we should function as on production */
PUBLIC BOOLEAN FUNCTION IsProductionOrAsIf()
{
  RETURN GetDTAPStage() = "production" OR IsDebugTagEnabled("asprod");
}

/** @short Get the WebHare installation/configuration parameters
    @long This function returns the various path locations and database settings as configured in the webhare-(default-)config.xml file.
    @return Configuration parameters
    @cell(string) return.installationroot The WebHare installation root directory (eg /opt/webhare)
    @cell(string) return.basedataroot The base directory for the WebHare data files (dbase, etc). Custom and modulespecific folders should use the varroot or the ephemeralroot
    @cell(string) return.config Path to the configuration file
    @cell(string) return.node Current server's node name in a cluster
    @cell(string array) return.moduledirs Module search paths
    @cell(string) return.varroot The directory for the WebHare on-disk data (eg /var/)
    @cell(string) return.ephemeralroot The directory for the WebHare on-disk data that is relatively easily recovered after loss (eg /var/ephemeral, used for image- and compilecache)
    @cell(string) return.logroot The directory where log files are stored (eg /log/).
    @cell(string) return.consilio ip:port used for consilios
    @cell(integer) return.baseport Baseport for local services (eg 13679)
    @cell(integer) return.trustedport Port for incoming LB connections where we trust X-Forwarded-For etc headers. baseport + 5, eg 13684
    @cell(integer) return.hstrustedport Like trustedport, but opened for the JS webserver. baseport + 3, eg 13682.
    */
PUBLIC RECORD FUNCTION GetWebHareConfiguration()
{
  INTEGER baseport := ToInteger(GetEnvironmentVariable("WEBHARE_BASEPORT"),13679);
  RECORD params := __SYSTEM_WHCOREPARAMETERS();
  params := CELL[ ...params
                , baseport
                , trustedhost := GetEnvironmentVariable("WEBHARE_SECUREPORT_BINDIP") ?? "127.0.0.1"
                , trustedport := baseport + whconstant_webserver_trustedportoffset
                , hstrustedport := baseport + whconstant_webserver_hstrustedportoffset
                ];
  RETURN params;
}

/** @short Get the primary WebHare interface URL
    @return The interface url, of the form 'http[s]://server/' */
PUBLIC STRING FUNCTION GetPrimaryWebhareInterfaceURL()
{
  RETURN GetcachedWebHareConfig().primaryinterfaceurl;
}

RECORD FUNCTION GetCacheableDebuggerConfig()
{
  RECORD config :=
      [ dtapstage :=                    GetDTAPStage()
      , keep_errorterminated_msecs :=   0

      , eventmasks := [ "system:debugger.configchange"
                      , "system:config.servertype"
                      ]
      ];

  // Debugger config matters only for alpha machines
  IF (config.dtapstage = "development")
  {
    OBJECT trans;
    IF (NOT HavePrimaryTransaction())
      trans := OpenPrimary();

    config.keep_errorterminated_msecs := ReadRegistryKey("system.backend.development.zombietimeout");

    IF (Objectexists(trans))
      trans->Close();
  }

  RETURN
      [ ttl :=        60 * 60 * 1000 // 1 hour
      , value :=      config
      , eventmasks := config.eventmasks
      ];
}

/** Return the configuration for the debugger
    @return Debugger configuration
    @cell(string) return.dtapstage Current dtapstage Current dtap stage
    @cell(integer) return.keep_errorterminated_msecs Time (in msecs) to keep terminated processes with errors around for the debugger to attach to
    @cell(string array) return.eventmasks Masks for events that invalidate this configuration
*/
PUBLIC RECORD FUNCTION GetDebuggerConfig()
{
  RETURN GetAdhocCached([ type := "debugconfig" ], PTR GetCacheableDebuggerConfig);
}

/** Returns the current system configuration
    @return System configuration record
*/
PUBLIC RECORD FUNCTION GetSystemConfigurationRecord()
{
  RETURN __SYSTEM_GETSYSTEMCONFIG();
}

/** Test if the current WebHare semantic version matches the specified version range
    @param semverrange Semver range to test against
    @return True if this WebHare matches */
PUBLIC BOOLEAN FUNCTION IsWebhareVersionMatch(STRING semverrange)
{
  RETURN VersionSatisfiesRange(GetWebhareVersionInfo().version, semverrange);
}

/** Test if the specified module's semantic version matches the specified version range
    @param modulename Module to test
    @param semverrange Semver range to test against
    @return True if the module is installed and matches the version range */
PUBLIC BOOLEAN FUNCTION IsModuleVersionMatch(STRING modulename, STRING semverrange)
{
  IF(modulename IN whconstant_builtinmodules)
    RETURN IsWebhareVersionMatch(semverrange);

  RECORD modinfo := GetWebhareModuleInfo(modulename);
  IF(NOT RecordExists(modinfo))
    RETURN FALSE;

  RETURN VersionSatisfiesRange(modinfo.version, semverrange);
}

RECORD FUNCTION ValidateApplyOptions(RECORD inoptions)
{
  RETURN ValidateOptions(CELL
    [ modules := STRING[]
    , subsystems := STRING[]
    , force := FALSE
    , verbose := FALSE
    , source := ""
    ], inoptions, [ required := [ "subsystems", "source"]]);
}

/** Ensure changes to WebHare's configuration are applied */
PUBLIC MACRO ApplyWebHareConfiguration(RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateApplyOptions(options);

  OBJECT service := WaitForPromise(OpenWebHareService("platform:configuration"));
  TRY
  {
    WaitForPromise(service->applyConfiguration(options));
    BroadcastLocalEvent("system:configupdate"); //ensure our process synchronously knows about any new module paths
  }
  FINALLY
  {
    service->CloseService();
  }
}

STATIC OBJECTTYPE ApplyFinishHandler EXTEND TransactionFinishHandlerBase
<
  STRING ARRAY subsystems;
  STRING ARRAY sources;
  RECORD defer;
  BOOLEAN applying;

  MACRO NEW()
  {
    this->defer := CreateDeferredPromise();
  }

  PUBLIC MACRO Add(STRING subsystem, STRING source)
  {
    IF(subsystem NOT IN this->subsystems)
      INSERT subsystem INTO this->subsystems AT END;
    IF(source NOT IN this->sources)
      INSERT source INTO this->sources AT END;
  }

  UPDATE PUBLIC MACRO OnCommit()
  {
    this->applying := TRUE;
    this->AsyncOnCommit(); //don't wait for it
  }

  ASYNC MACRO AsyncOnCommit()
  {
    OBJECT service;
    TRY
    {
      IF(Length(this->subsystems) > 0)
      {
        service := WaitForPromise(OpenWebHareService("platform:configuration"));
        WaitForPromise(service->applyConfiguration([ subsystems := this->subsystems, source := Detokenize(this->sources, ", ") ]));
      }
      this->defer.resolve(TRUE);
    }
    CATCH(OBJECT e)
    {
      this->defer.reject(e);
    }
    FINALLY
    {
      IF(ObjectExists(service))
        service->CloseService();
    }
  }

  UPDATE PUBLIC MACRO OnRollback()
  {
    this->applying := true;
    this->defer.reject(NEW Exception("Configuration changes have been rolled back"));
  }

  PUBLIC OBJECT FUNCTION GetPromise()
  {
    IF (NOT this->applying)
      THROW NEW Exception("Configuration changes are not being applied yet - wait for the work to commit");
    RETURN this->defer.promise;
  }
>;

PUBLIC FUNCTION PTR FUNCTION CreateAppliedPromise(RECORD toApply DEFAULTSTO DEFAULT RECORD)
{
  toapply := ValidateApplyOptions(toapply);

  IF(NOT GetPrimary()->IsWorkOpen())
    THROW NEW Exception(`Work must be open to use createAppliedPromise`);

  OBJECT handler := GetPrimary()->GetFinishHandler("system:applyfinishhandler");
  IF (NOT ObjectExists(handler))
  {
    handler := NEW ApplyFinishHandler;
    GetPrimary()->SetFinishHandler("system:applyfinishhandler", handler);
  }

  FOREVERY(STRING subsystem FROM toapply.subsystems)
    handler->Add(subsystem, toApply.source);

  RETURN PTR handler->GetPromise();
}
