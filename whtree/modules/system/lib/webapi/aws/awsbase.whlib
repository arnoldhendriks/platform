<?wh

LOADLIB "wh::crypto.whlib";
LOADLIB "wh::datetime.whlib";
LOADLIB "wh::internet/urls.whlib";
LOADLIB "wh::internet/webbrowser.whlib";

LOADLIB "mod::system/lib/logging.whlib";

PUBLIC STRING FUNCTION EncodeURL3986(STRING indata) //http://tools.ietf.org/html/rfc3986
{
  indata := EncodeURL(indata);
  indata := Substitute(indata,'/',"%2F");
  indata := Substitute(indata,'!',"%21");
  indata := Substitute(indata,'$',"%24");
  indata := Substitute(indata,'(',"%28");
  indata := Substitute(indata,')',"%29");
  indata := Substitute(indata,'%7E','~');
  RETURN indata;
}

STRING FUNCTION CalculateSigningKey(STRING accesssecret, STRING xamzdate, STRING region, STRING service)
{
  STRING signingkey := GetHashForString(Left(xamzdate,8), "HMAC:SHA-256", "AWS4" || accesssecret);
  signingkey := GetHashForString(region, "HMAC:SHA-256", signingkey);
  signingkey := GetHashForString(service, "HMAC:SHA-256", signingkey);
  signingkey := GetHashForString("aws4_request", "HMAC:SHA-256", signingkey);
  RETURN signingkey;
}

PUBLIC STATIC OBJECTTYPE AmazonAWSException EXTEND Exception
<
  PUBLIC INTEGER statuscode;
  PUBLIC STRING errorcode;
  PUBLIC STRING errormessage;

  MACRO NEW(INTEGER statuscode, STRING errorcode, STRING errormessage)
  : Exception(statuscode || ": " || errorcode || " "|| errormessage)
  {
    this->statuscode := statuscode;
    this->errorcode := errorcode;
    this->errormessage := errormessage;
  }
>;

PUBLIC STATIC OBJECTTYPE AmazonAWSConfig
<
  OBJECT browser;
  STRING accesskey;
  STRING accesssecret;

  PUBLIC PROPERTY debug(this->browser->debug, this->browser->debug);
  PUBLIC PROPERTY __browser(this->browser,-);//needed for legacy awsinterface

  MACRO NEW(STRING accesskey, STRING accesssecret)
  {
    IF(accesskey = "")
      THROW NEW Exception("No AWS acccess key set");
    IF(accesssecret = "")
      THROW NEW Exception("No AWS acccess secret set");

    this->accesskey := accesskey;
    this->accesssecret := accesssecret;
    this->browser := NEW WEBBrowser;
  }

  PUBLIC MACRO LogRPC(STRING target)
  {
    LogRPCforWebbrowser(target, "", this->browser);
  }

  PUBLIC RECORD FUNCTION BuildAWS4Request(STRING service, STRING region, STRING method, STRING url, RECORD options DEFAULTSTO DEFAULT RECORD )
  {
    options := ValidateOptions([ signedrequest := FALSE
                               , urlparameters := RECORD[]
                               , headers := RECORD[]
                               , body := DEFAULT BLOB
                               , accept404 := FALSE
                               , expires := 0 //expiry in seconds
                               , unsignedpayload := FALSE
                               ], options);

    IF(SearchSubstring(url,"?") != -1)
      THROW NEW Exception("Don't try to add query variables to the URL");
    IF(region = "")
      THROW NEW Exception("AWS4 requests require a region");

    STRING xamzdate := FormatDatetime("%Y%m%dT%H%M%SZ", GetCurrentDatetime());
    STRING bodyhash := options.unsignedpayload ? "UNSIGNED-PAYLOAD" : ToLowercase(EncodeBase16(GetHashForBlob(options.body, "SHA-256")));
    STRING credential_scope := Left(xamzdate,8) || "/" || region || "/" || service || "/aws4_request";
    STRING credential := this->accesskey || "/" || credential_scope;
    STRING algorithm := "AWS4-HMAC-SHA256";

    RECORD ARRAY headers := options.headers;
    INSERT [ field := "Host", value := UnpackURL(url).host ] INTO headers AT END;

    IF(NOT options.signedrequest)
    {
      INSERT [ field := "X-Amz-Date", value := xamzdate ] INTO headers AT END;
      IF(NOT options.unsignedpayload)
        INSERT [ field := "X-Amz-Content-SHA256", value := bodyhash ] INTO headers AT END;
    }

    RECORD ARRAY signedheaders := SELECT *
                                    FROM headers
                                  WHERE ToUppercase(field) IN ["HOST","CONTENT-MD5"] OR ToUppercase(field) LIKE "X-AMZ-*"
                                  ORDER BY Touppercase(field);
    STRING signedheadernames := Detokenize( (SELECT AS STRING ARRAY ToLowercase(field) FROM signedheaders), ';');

    RECORD ARRAY urlparameters := options.urlparameters;
    IF(options.signedrequest)
    {
      urlparameters := urlparameters CONCAT
         [ [ name := "X-Amz-Algorithm", value := algorithm ]
         , [ name := "X-Amz-Credential", value := credential ]
         , [ name := "X-Amz-Date", value := xamzdate ]
         , [ name := "X-Amz-Expires", value := ToString(options.expires) ]
         , [ name := "X-Amz-SignedHeaders", value := signedheadernames ]
         ];
    }
    ELSE IF(options.expires != 0)
    {
      THROW NEW Exception(`Expires only supported for signedrequests`);
    }


    STRING querystring;

    FOREVERY(RECORD param FROM SELECT * FROM urlparameters ORDER BY name)
      querystring := (querystring=""?"":querystring||"&") || EncodeURL3986(param.name) || "=" || EncodeURL3986(param.value);

    //http://docs.aws.amazon.com/general/latest/gr/sigv4-create-canonical-request.html
    STRING canonicalrequest :=
      method || "\n"
      || "/" || UnpackURL(url).urlpath || "\n"
      || querystring || "\n";

    FOREVERY(RECORD header FROM signedheaders)
      canonicalrequest := canonicalrequest || ToLowercase(header.field) || ":" || TrimWhitespace(header.value) || "\n";

    canonicalrequest := canonicalrequest
      || "\n"
      || signedheadernames || "\n"
      || bodyhash;

    IF(this->debug)
      PRINT(`Canonicalrequest=[${canonicalrequest}]\n`);

    STRING canonicalhash := ToLowercase(EncodeBase16(GetHashForString(canonicalrequest, "SHA-256")));

    //http://docs.aws.amazon.com/general/latest/gr/sigv4-create-string-to-sign.html
    STRING stringtosign :=
      algorithm || "\n"
      || xamzdate || "\n"
      || credential_scope || "\n"
      || canonicalhash;
    IF(this->debug)
      PRINT(`StringToSign=[${stringtosign}]\n`);

    STRING signingkey := CalculateSigningKey(this->accesssecret, xamzdate, region, service);
    STRING signature := ToLowercase(EncodeBase16(GetHashForString(stringtosign, "HMAC:SHA-256", signingkey)));

    IF(options.signedrequest)
    {
      url := url || "?" || querystring || "&X-Amz-Signature=" || signature;
    }
    ELSE
    {
      IF(querystring != "")
        url := url || "?" || querystring;

      INSERT [ field := "Authorization"
             , value := algorithm || " Credential=" || this->accesskey || "/" || credential_scope || ", SignedHeaders=" || signedheadernames || ", Signature=" || signature
             ] INTO headers AT END;
    }

    RETURN CELL [ method, url, headers, options ];
  }

  PUBLIC RECORD FUNCTION RequestXML(STRING service, STRING region, STRING method, STRING url, RECORD options DEFAULTSTO DEFAULT RECORD )
  {
    RECORD prep := this->BuildAWS4Request(service, region, method, url, options);
    IF(NOT this->browser->SendRawRequest(prep.method, prep.url, prep.headers, prep.options.body))
    {
      IF(NOT (prep.options.accept404 AND this->browser->GetHTTPStatusCode()=404))
      {
        this->__ProcessAWSError();
        THROW NEW Exception("Request failure and error not handled by ProcessAWSError");
      }
      RETURN DEFAULT RECORD;
    }
    RETURN [ doc := this->browser->document
           , etag := this->browser->GetResponseHeader("ETag")
           ];
  }

  PUBLIC RECORD FUNCTION RequestJSON(STRING service, STRING region, STRING method, STRING url, RECORD options DEFAULTSTO DEFAULT RECORD )
  {
    BLOB body;

    IF(CellExists(options,'rawbody'))
      body := options.rawbody;
    ELSE IF(CellExists(options,'body'))
      IF(CellExists(options,'translations'))
        body := EncodeJSONBLob(options.body, options.translations);
      ELSE
        body := EncodeJSONBLob(options.body);

    RECORD prep := this->BuildAWS4Request(service, region, method, url, CELL[...options, body, DELETE translations, DELETE rawbody ]);
    IF(NOT this->browser->SendRawRequest(prep.method, prep.url, prep.headers, body))
    {
      IF(NOT (prep.options.accept404 AND this->browser->GetHTTPStatusCode()=404))
      {
        this->__ProcessAWSError();
        THROW NEW Exception("Request failure and error not handled by ProcessAWSError");
      }
      RETURN DEFAULT RECORD;
    }
    RETURN DecodeJSONBlob(this->browser->content);
  }

  PUBLIC MACRO __ProcessAWSError()
  {
    IF(this->browser->getresponseheader("content-type") = "application/json")
    {
      RECORD error := EnforceStructure([ message := "" ], DecodeJSONBlob(this->browser->content));
      IF(error.message != "")
        THROW NEW AmazonAWSException(this->browser->GetHTTPStatusCode(), "", error.message);
    }
    ELSE IF(ObjectExists(this->browser->document))
    {
      OBJECT errorcodenode := this->browser->QS("Error > Code");
      OBJECT errormessagenode := this->browser->QS("Error > Message");
      IF(ObjectExists(errorcodenode) AND ObjectExists(errormessagenode))
        THROW NEW AmazonAWSException(this->browser->GetHTTPStatusCode(), errorcodenode->textcontent, errormessagenode->textcontent);
    }
    THROW NEW Exception(`Request failure: ${this->browser->GetHTTPStatusText()}`);
  }
>;

PUBLIC STATIC OBJECTTYPE AmazonAWSService
<
  OBJECT __config;
  STRING __region;
  STRING __service;

  PUBLIC PROPERTY region(__region, -);
  PUBLIC PROPERTY service(__service, -);
  PUBLIC PROPERTY config(__config, -);
  PUBLIC PROPERTY debug(this->__config->debug, this->__config->debug);

  MACRO NEW(OBJECT config, STRING region, STRING service)
  {
    this->__config := config;
    IF(region NOT LIKE "??-*-*")
      THROW NEW Exception(`${region} does not look like a valid AWS region`);
    this->__region := region;
    this->__service := service;
  }

  PUBLIC RECORD FUNCTION BuildAWS4Request(STRING method, STRING url, RECORD options DEFAULTSTO DEFAULT RECORD )
  {
    RETURN this->__config->BuildAWS4Request(this->__service, this->__region, url, options);
  }

  PUBLIC RECORD FUNCTION RequestXML(STRING method, STRING url, RECORD options DEFAULTSTO DEFAULT RECORD )
  {
    RETURN this->__config->RequestXML(this->__service, this->__region, method, url, options);
  }

  PUBLIC RECORD FUNCTION RequestJSON(STRING service, STRING region, STRING method, STRING url, RECORD options DEFAULTSTO DEFAULT RECORD )
  {
    RETURN this->__config->RequestJSON(this->__service, this->__region, method, url, options);
  }
>;

PUBLIC STATIC OBJECTTYPE AmazonAWSInterface EXTEND AmazonAWSService
<
  PROPERTY browser(this->__config->__browser,-);

  MACRO NEW(STRING region, STRING accesskey, STRING accesssecret)
  : AmazonAWSService(NEW AmazonAWSConfig(accesskey, accesssecret), region, "")
  {
  }

  PUBLIC RECORD FUNCTION PrepareAWS4Request(STRING service, STRING method, STRING url, RECORD options DEFAULTSTO DEFAULT RECORD )
  {
    STRING region := CellExists(options,"region") ? options.region : this->region;
    DELETE CELL region FROM options;
    RETURN this->config->BuildAWS4Request(service, region, method, url, options);
  }

  PUBLIC OBJECT FUNCTION RawAWS4Request(STRING service, STRING method, STRING url, RECORD options DEFAULTSTO DEFAULT RECORD )
  {
    STRING region := CellExists(options,"region") ? options.region : this->region;
    DELETE CELL region FROM options;
    RECORD result := this->config->RequestXML(service, region, method, url, options);
    RETURN RecordExists(result) ? result.doc : DEFAULT OBJECT;
  }

  PUBLIC RECORD FUNCTION RawAWS4JSONRequest(STRING service, STRING method, STRING url, RECORD options DEFAULTSTO DEFAULT RECORD)
  {
    RECORD prep := this->PrepareAWS4Request(service, method, url, options);
    IF(NOT this->browser->SendRawRequest(prep.method, prep.url, prep.headers, prep.options.body))
    {
      IF(NOT (prep.options.accept404 AND this->browser->GetHTTPStatusCode()=404))
      {
        this->config->__ProcessAWSError();
        THROW NEW Exception("Request failure and error not handled by ProcessAWSError");
      }
      RETURN DEFAULT RECORD;
    }
    RETURN DecodeJSONBlob(this->browser->content);
  }
>;


