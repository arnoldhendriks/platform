<?wh
LOADLIB "wh::datetime.whlib";
LOADLIB "wh::ipc.whlib";
LOADLIB "wh::internet/mime.whlib";

LOADLIB "mod::wrd/lib/api.whlib";

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/testfw/emails.whlib";
LOADLIB "mod::system/lib/internal/testfw/networking.whlib";


STATIC OBJECTTYPE SMTPReceiver
< OBJECT pvt_job;
  INTEGER pvt_callback;

  RECORD ARRAY pvt_messages;
  INTEGER seqnr;

  INTEGER pvt_connectioncount;

  /** Received messages
      @cell(record) sender Sender data
      @cell(string) sender.mailbox_domain Mailbox domain part
      @cell(string) sender.mailbox_local Mailbox local part
      @cell(record array) sender.route Mailbox route
      @cell(record array) recipients Recipients data
      @cell(string) recipients.mailbox_domain Mailbox domain part
      @cell(string) recipients.mailbox_local Mailbox local part
      @cell(record array) recipients.route Mailbox route
      @cell(blob) data Raw message data
  */
  PUBLIC PROPERTY messages(GetMessages, -);

  /// Number of connections made
  PUBLIC PROPERTY connectioncount(GetConnectionCount, -);

  MACRO NEW(OBJECT job)
  {
    this->pvt_job := job;
    this->pvt_callback := RegisterHandleReadCallback(job->ipclink->handle, PTR this->ReceiveMessage);
  }

  RECORD ARRAY FUNCTION GetMessages()
  {
    WHILE (this->pvt_job->ipclink->IsSignalled())
      IF (NOT this->ReceiveMessage()) //abort loop if the job is in error
        BREAK;

    RETURN this->pvt_messages;
  }

  INTEGER FUNCTION GetConnectionCount()
  {
    this->GetMessages();
    RETURN this->pvt_connectioncount;
  }

  PUBLIC BOOLEAN FUNCTION ReceiveMessage(DATETIME waituntil DEFAULTSTO DEFAULT DATETIME)
  {
    RECORD rec := this->pvt_job->ipclink->ReceiveMessage(waituntil);
    IF (rec.status = "gone")
    {
      IF (this->pvt_callback != 0)
        UnregisterCallback(this->pvt_callback);
      this->pvt_callback := 0;
      IF (LENGTH(this->pvt_job->GetErrors()) != 0)
      {
        PrintTo(2,"Job errors\n");
        DumpValue(this->pvt_job->GetErrors(), "boxed");
      }
    }
    ELSE IF (rec.status = "ok")
    {
      IF (rec.msg.type = "message")
      {
        this->seqnr := this->seqnr + 1;
        INSERT CELL[id := this->seqnr, ...rec.msg.msg] INTO this->pvt_messages AT END;
      }
      IF (rec.msg.type = "newconnection")
        this->pvt_connectioncount := this->pvt_connectioncount + 1;
      RETURN TRUE;
    }
    RETURN FALSE;
  }

  PUBLIC MACRO DeleteAllMessages()
  {
    this->pvt_messages := DEFAULT RECORD ARRAY;
  }

  /** Shutdown the server and retrieve all messages from the server
  */
  PUBLIC MACRO ShutdownServer()
  {
    this->Close();
  }

  /** Close all used resources
  */
  PUBLIC MACRO Close()
  {
    IF (NOT ObjectExists(this->pvt_job))
      RETURN;
    WHILE (this->ReceiveMessage())
      0;
    IF (this->pvt_callback != 0)
    {
      UnregisterCallback(this->pvt_callback);
      this->pvt_callback := 0;
    }
    UnregisterCallback(this->pvt_job->userdata.callback);

    // Wait 50ms for job to close and to see errors when they have occurred
    IF (this->pvt_job->Wait(AddTimeToDate(50, GetCurrentDateTime())) AND LENGTH(this->pvt_job->GetErrors()) != 0)
      PRINT("Job errors:\n" || AnyToString(this->pvt_job->GetErrors(), "tree"));
    this->pvt_job->Close();
    this->pvt_job := DEFAULT OBJECT;
  }

  RECORD ARRAY FUNCTION __TryGetEmails(STRING emailaddress, BOOLEAN peekonly, INTEGER limitmails)
  {
    RECORD ARRAY emails := SELECT *
                                //, maildata := DescribeManagedTask(id)
                             FROM this->messages
                            WHERE (emailaddress = "*" OR (Length(recipients) = 1 AND recipients[0].mailbox_local || "@" || recipients[0].mailbox_domain LIKE emailaddress))
                            LIMIT limitmails;

    IF(NOT peekonly)
      DELETE FROM this->pvt_messages WHERE id IN (SELECT AS INTEGER ARRAY id FROM emails);

    RECORD ARRAY outmails;
    FOREVERY(RECORD inemail FROM emails)
    {
      RECORD email := DecodeMIMEMessage(inemail.data);
      email := CELL[ ...email
                   , receiver := email.tos
                   , sender := `<${inemail.sender.mailbox_local}@${inemail.sender.mailbox_domain}>`
                   ];
      INSERT ProcessExtractedMail(email) INTO outmails AT END;
    }

    RETURN outmails;
  }
  RECORD ARRAY FUNCTION DoRetrieveEmails(STRING emailaddress, INTEGER timeout, INTEGER count, BOOLEAN peekonly, BOOLEAN returnallmail)
  {
    IF(timeout < 0 OR timeout > 60000)
      THROW NEW Exception("Illegal timeout");
    DATETIME waituntil := AddTimeToDate(timeout, GetCurrentDatetime());

    RECORD ARRAY outmails;
    WHILE(TRUE)
    {
      outmails := outmails CONCAT this->__TryGetEmails(emailaddress, peekonly, returnallmail ? 99999999 : count - Length(outmails));
      IF(Length(outmails) >= count)
        RETURN outmails;

      IF(waituntil <= GetCurrentDatetime())
      {
        IF(count > 0)
          THROW NEW Exception(`Timeout retrieving emails - expected ${returnallmail?"at least ":""}${count} emails, got ${Length(outmails)}`);

        RETURN RECORD[];
      }

      Sleep(100);
    }
  }

  PUBLIC RECORD ARRAY FUNCTION ExtractAllMailFor(STRING recipient, RECORD options DEFAULTSTO DEFAULT RECORD)
  {
    options := ValidateOptions([ peekonly := FALSE
                               , timeout := 0
                               , count := 1
                               , returnallmail := FALSE
                               ], options);
    RETURN this->DoRetrieveEmails(recipient, options.timeout, options.count, options.peekonly, options.returnallmail);
  }
>;

MACRO OnCrash(OBJECT job)
{
  ABORT("SMTP job crashed:\n" || AnyToString(job->GetErrors(), "tree"));
}

/** Create a new SMTP receiver
    @param host Host to listen on
    @param port Port to listen on
    @cell(boolean) options.debug Dump the client/server interaction
    @cell(string array) options.directbouncemasks Masks for direct bouncing
    @cell(string) options.bouncehandling
    @cell(integer) options.connectdelay Connection delay
    @cell(record) options.handleroptions @includecelldef smtpsimulatorjob.whlib#DummyMailHandler::NEW.options
    @return Object of type SMTPReceiver
*/
PUBLIC OBJECT FUNCTION GetSMTPReceiver(STRING host, INTEGER port, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  RECORD handleroptions := CellExists(options, "handleroptions") ? options.handleroptions : DEFAULT RECORD;
  DELETE CELL handleroptions FROM options;

  RECORD jobrec := CreateJob("mod::system/lib/testfw/smtpsimulatorjob.whlib");
  IF(NOT ObjectExists(jobrec.job))
  {
    DumpValue(jobrec.errors,'boxed');
  }
  jobrec.job->ipclink->SendMessage(CELL[ host, port, options, handleroptions ]);
  jobrec.job->Start();
  jobrec.job->ipclink->Wait(MAX_DATETIME);

  jobrec.job->userdata := [ callback := RegisterHandleReadCallback(jobrec.job->handle, PTR OnCrash(jobrec.job)) ];

  RETURN NEW SMTPReceiver(jobrec.job);
}

PUBLIC OBJECT FUNCTION SetupTestSMTPReceiver(STRING recipientmask, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions([ bouncehandling := "SES"
                             , directbouncemasks := STRING[]
                             , connectdelay := 0
                             ], options);
  INTEGER mailserverport := GetAvailableServerPort();
  OBJECT mailserverobject := GetSMTPReceiver("127.0.0.1", mailserverport, options);

  GetPrimary()->BeginWork();
  OpenWRDSchema("system:config")->^mailroute->CreateEntity(
    [ wrd_tag := "TESTFW_TESTSMTPRECEIVER_" || mailserverport
    , sendermasks := [[ mask := "*" ]]
    , recipientmasks := [[ mask := recipientmask ]]
    , wrd_ordering := -9999
    , wrd_title := "Created by SetupTestSMTPReceiver"
    , serverurl := `smtp://127.0.0.1:${mailserverport}`
    ]);
  GetPrimary()->CommitWork();

  RETURN mailserverobject;
}

