﻿<?wh
/** @short WebHare Database access
    @long Table definitions and binding functions for the system module. If a primary
          transaction is available, this library will automatically bind all tables
          to this transaction.
    @topic database/webharedb
*/
LOADLIB "wh::dbase/transaction.whlib" EXPORT
    GetPrimaryWebhareTransaction,
    GetPrimaryWebhareTransactionObject,
    SetPrimaryWebhareTransaction,
    SetupPrimaryTransactionBinder,
    DatabaseException;
LOADLIB "wh::dbase/postgresql.whlib";
LOADLIB "wh::datetime.whlib";
LOADLIB "wh::dbase/transaction.whlib";
LOADLIB "wh::internal/transbase.whlib" EXPORT IsDatabaseWritable, IsDatabaseOnline;
LOADLIB "wh::internal/transservices.whlib";
LOADLIB "wh::internal/wasm.whlib";

LOADLIB "mod::system/lib/internal/dbase/base.whlib" EXPORT WebhareDBException;
LOADLIB "mod::system/lib/internal/dbase/postgresql-blobhandling.whlib";
LOADLIB "mod::system/lib/internal/whcore_interface.whlib";

/** System database schema
    @schemadef system
*/
PUBLIC SCHEMA
  < TABLE
    < INTEGER "id" NULL := 0
    , STRING "name"
    , INTEGER "parent" NULL := 0
    , DATETIME "creationdate"
    , DATETIME "deletiondate"
    , INTEGER "type"
    , STRING "guid"
    ; KEY id
    > authobjects
  , TABLE
    < INTEGER "id" NULL := 0
    , INTEGER "role" NULL := 0
    , INTEGER "grantee" NULL := 0
    , INTEGER "grantor" NULL := 0
    , DATETIME "creationdate"
    , STRING "comment"
    , STRING "grantordata"
    ; KEY id
    > rolegrants
  , TABLE
    < INTEGER "id" NULL := 0
    , STRING "mimetype"
    , STRING "extension"
    , INTEGER "parsetype"
    , BOOLEAN "forcedispositionattachment"
    ; KEY id
    > mimetypes
  , TABLE
    < INTEGER "id" NULL := 0
    , STRING "ip"
    , INTEGER "port"
    , BOOLEAN "virtualhost"
    , INTEGER "keypair" NULL := 0
    , STRING "description"
    ; KEY id
    > ports
  , TABLE
    < INTEGER "id" NULL := 0
    , INTEGER "type"
    , STRING "diskfolder"
    , STRING "baseurl"
    , STRING "outputextension"
    , INTEGER "port" NULL := 0
    , INTEGER parent NULL := 0
    , STRING title
    , DATETIME "lastseen"
    , DATETIME "lastcheck"
    , STRING "lastcheckerror"
    , INTEGER "stricttransportsecurity"
    ; KEY id
    > webservers
  , TABLE
    < INTEGER "id" NULL := 0
    , INTEGER "webserver" NULL := 0
    , STRING "path"
    , STRING "description"
    , BOOLEAN "disablecaching"
    , INTEGER "matchtype"
    , STRING "errorpath"
    , INTEGER "authtype"
    , BOOLEAN "authrequirement"
    , INTEGER "hostingsrc"
    , STRING "hostingpath"
    , BOOLEAN "authlist"
    , STRING "authscript"
    , INTEGER 'redirectcode'
    , BOOLEAN fixcase
    , INTEGER maxage
    , BOOLEAN disabled
    ; KEY id
    > access
  , TABLE
    < INTEGER "accessid" NULL := 0
    , STRING "mask"
    , BOOLEAN "is_allow"
    > access_ips
  , TABLE
    < INTEGER "id" NULL := 0
    , INTEGER "webserver" NULL := 0
    , STRING "hostname"
    , BOOLEAN "explicit"
    , DATETIME "lastseen"
    , DATETIME "lastcheck"
    , STRING "lastcheckerror"
    ; KEY id
    > webservers_aliases
  , TABLE
    < INTEGER "accessid" NULL := 0
    , STRING "username"
    , STRING "userpassword"
    , STRING "description"
    > access_externalusers
  , TABLE
    < INTEGER 'id' NULL := 0
    , DATETIME creationdate
    , DATETIME nextattempt
    , DATETIME finished
    , STRING tasktype
    , STRING taskdata
    , BLOB auxdata
    , STRING lasterrors
    , STRING stacktrace
    , STRING shortretval
    , BLOB longretval
    , INTEGER iterations
    , BOOLEAN "iscancelled"
    , INTEGER "priority"
    , INTEGER "timeout"
    , DATETIME notbefore
    ; KEY id
    > managedtasks
  , TABLE
    < INTEGER id NULL := 0
    , INTEGER task NULL := 0
    , STRING metakey
    , STRING metavalue
    ; KEY id
    > managedtasksmeta
  , TABLE
    < INTEGER "id" NULL := 0
    , STRING "foldername"
    , STRING "diskfolder"
    , BOOLEAN "forcelowercase"
    ; KEY id
    > webdav_datafolders

  , TABLE
    < INTEGER "id" NULL := 0
    , STRING "name"
    , STRING "namehash" __ATTRIBUTES__(BINARY)
    , STRING "nodehash" __ATTRIBUTES__(BINARY)
    , STRING "data"
    , BLOB "blobdata"
    , DATETIME "modificationdate"
    ; KEY id
    > flatregistry

  , TABLE
    < INTEGER "id" NULL := 0
    , STRING "name"
    , DATETIME "modificationdate"
    ; KEY id
    > modules

  , TABLE
    < INTEGER "id" NULL := 0
    , STRING "name"
    , INTEGER "module" NULL := 0
    ; KEY id
    > module_rights

  , TABLE
    < INTEGER "id" NULL := 0
    , INTEGER "right" NULL := 0
    , INTEGER "impliedby" NULL := 0
    , STRING "fieldname"
    ; KEY id
    > module_impliedbys

   , TABLE
    < BLOB "data"
    , BOOLEAN "publish" __ATTRIBUTES__(READONLY)
    , DATETIME "creationdate"
    , DATETIME "modificationdate"
    , INTEGER "modifiedby" NULL := 0
    , INTEGER "filelink" NULL := 0
    , INTEGER "highestparent" AS "parentsite" __ATTRIBUTES__(READONLY)
    , INTEGER "indexdoc" NULL := 0
    , DATETIME "firstpublishdate"
    , DATETIME "contentmodificationdate"
    , DATETIME "lastpublishdate"
    , INTEGER "lastpublishsize"
    , INTEGER "lastpublishtime"
    , INTEGER "ordering"
    , INTEGER "parent" NULL := 0
    , INTEGER "highestparent" __ATTRIBUTES__(READONLY)
    , INTEGER "published"
    , INTEGER "type" NULL := 0
    , BOOLEAN "isfolder"
    //, INTEGER "parent_inside_site" __ATTRIBUTES__(READONLY) //TODO cleanup in moduledefinition.xml and C++ code
    , STRING "description"
    , STRING "errordata"
    , STRING "externallink"
    , STRING "fullpath" __ATTRIBUTES__(READONLY)
    , STRING "whfspath" __ATTRIBUTES__(READONLY)
    , STRING "indexurl" __ATTRIBUTES__(READONLY)
    , STRING "indexurl" AS "link" __ATTRIBUTES__(READONLY)
    , STRING "keywords"
    , STRING "name"
    , STRING "title"
    , STRING "url" __ATTRIBUTES__(READONLY)
    , STRING "url" AS "objecturl" __ATTRIBUTES__(READONLY)
    , INTEGER "id" NULL := 0
    , BOOLEAN "isactive" __ATTRIBUTES__(READONLY)
    , BOOLEAN "ispinned"
    , STRING "scandata"
    ; KEY id
    > fs_objects

    , TABLE
    < INTEGER "id" NULL := 0
    , STRING "name"
    , STRING "description"
    , INTEGER "outputweb" NULL := 0
    , STRING "outputfolder"
    , STRING "lockreason"
    , BOOLEAN "locked"
    , INTEGER "id" AS "root" NULL := 0 __ATTRIBUTES__(READONLY)
    , STRING "cdnbaseurl"
    , STRING "webroot" __ATTRIBUTES__(READONLY)
    ; KEY id
    > sites

  , TABLE
    < INTEGER "id" NULL := 0
    , STRING "namespace"
    , BOOLEAN "isfoldertype"
    , BOOLEAN "isfiletype"
    , BOOLEAN "cloneoncopy"
    , BOOLEAN "orphan"
    , BOOLEAN "isacceptableindex"
    , BOOLEAN "needstemplate"
    , BOOLEAN "ispublishedassubdir"
    , BOOLEAN "isdynamicexecution"
    , BOOLEAN "ispublishable"
    , BOOLEAN "generatepreview"
    , BOOLEAN "capturesubpaths"
    , STRING "scopedtype"
    ; KEY id
    > fs_types
  , TABLE
    < INTEGER "id" NULL := 0
    , STRING "name"
    , INTEGER "fs_type" NULL := 0
    , INTEGER "type"
    , BOOLEAN "orphan"
    , INTEGER "parent" NULL := 0
    ; KEY id
    > fs_members
  , TABLE
    < INTEGER64 "id" NULL := 0
    , INTEGER64 "fs_instance" NULL := 0
    , INTEGER "fs_member" NULL := 0
    , STRING "setting"
    , BLOB "blobdata"
    , INTEGER "fs_object" NULL := 0
    , INTEGER64 "parent" NULL := 0
    , INTEGER "instancetype" NULL := 0
    , INTEGER "ordering"
    ; KEY id
    > fs_settings
  , TABLE
    < INTEGER64 "id" NULL := 0
    , INTEGER "fs_type" NULL := 0
    , INTEGER "fs_object" NULL := 0
    ; KEY id
    > fs_instances
  , TABLE
    < INTEGER id NULL := 0
    , INTEGER fs_object NULL := 0
    , INTEGER user NULL := 0
    , INTEGER type
    , DATETIME when
    , INTEGER currentparent NULL := 0
    , STRING currentname
    ; KEY id
    > fs_history

  , TABLE
    < INTEGER "id" NULL := 0
    , INTEGER "module" NULL := 0
    , STRING "name"
    , STRING "descriptiontid"
    , BOOLEAN "defaultenabled"
    ; KEY id
    > towl_notifications

  , TABLE
    < INTEGER id NULL := 0
    , INTEGER owner NULL := 0
    , STRING pathhash __ATTRIBUTES__(BINARY)
    , STRING path
    , DATETIME created
    , INTEGER type
    , BLOB data
    ; KEY id
    > webdav_diversions

  , TABLE
    < INTEGER "id" NULL := 0
    , INTEGER "webserver" NULL := 0
    , DATETIME "time"
    , INTEGER "hits"
    , INTEGER "serverfails"
    , INTEGER "highpagetime"
    , INTEGER "failips"
    , INTEGER64 "download"
    , INTEGER64 "upload"
    , INTEGER64 "sumpagetime"
    ; KEY id
    > "webservers_usage"

  , TABLE
    < INTEGER "id" NULL := 0
    , STRING "hash"
    , STRING "func"
    , STRING "state"
    , STRING "keydatahsonstr"
    , BLOB "keydatahsonblob"
    , STRING "valuehsonstr"
    , BLOB "valuehsonblob"
    , STRING "settingshsonstr"
    , DATETIME "lastuse"
    ; KEY id
    > precalccache

  > system;

// Generation of system table bindings, for rightsmgmt.
PUBLIC INTEGER __system_binding_gen;

OBJECT __system_binding_trans;

/** @short Bind system tables
    @long Bind all the tables owned by the system module (eg users, system_units, and all other system_ modules)
    @param transaction ID of the transaction to which the tables should be bound */
MACRO BindSystemTables(INTEGER transaction)
{
  system := BindTransactionToSchema(transaction, "system");
  IF(transaction=0)
  {
    __system_binding_trans := DEFAULT OBJECT;
    RETURN;
  }
  __system_binding_gen := __system_binding_gen + 1;
  __system_binding_trans := GetTransactionObjectById(transaction);
}

SetupPrimaryTransactionBinder(PTR BindSystemTables);

///@private Setting effective user is pretty low-level stuff, normal code shouldn't do it
PUBLIC MACRO __SetEffectiveUser(OBJECT userobj)
{
  IF(ObjectExists(userobj) AND userobj->authobjectid < 0)
    THROW NEW Exception("Invalid effective user (has authobject #" || userobj->authobjectid || ")");

  effectiveuser := userobj;
}

PUBLIC OBJECT FUNCTION __INTERNAL_GetSystemSchemaBinding()
{
  RETURN __system_binding_trans;
}


/** Opens the primary database connection
    @long The primary database connection (or transaction) is used to bind all global database schemas to
    @return Primary transction object. Throws upon error.
*/
PUBLIC OBJECT FUNCTION OpenPrimary(RECORD options DEFAULTSTO CELL[])
{
  options := ValidateOptions( [ clientname := ""
                              , waituntil := DEFAULT DATETIME
                              ], options);

  IF (GetPrimaryWebhareTransaction() != 0)
    THROW NEW Exception("A primary transaction has already been opened");

  RECORD transopts :=
      [ auto := TRUE
      , isprimary := TRUE
      , blobhandler := GetDefaultPostgreSQLBlobHandler()
      ];

  IF(options.clientname != "")
    INSERT CELL clientname := options.clientname INTO transopts;

  OBJECT trans;
  WHILE(TRUE)
  {
    trans := __StartWHPostgreSQLTransaction(transopts);
    IF (ObjectExists(trans))
      BREAK;

    IF (NOT ObjectExists(trans) AND options.waituntil < GetCurrentDatetime())
      THROW NEW Exception("The primary transaction could not be opened - is the database available?");

    Sleep(100);
  }

  SetPrimaryWebhareTransaction(trans->id);
  trans->throwoncommiterror := TRUE;
  RETURN trans;
}


/** Do we have a primary transaction? */
PUBLIC BOOLEAN FUNCTION HavePrimaryTransaction()
{
  RETURN GetPrimaryWebhareTransaction() != 0;
}

/** @short Run a function in a separate transaction
    @param func Function to invoke
    @cell options.work If true, open and commit work (unless the function throws)
    @cell options.openprimary If false, doesn't open a new primary right away, only closes it if opened inside the callback (default: true)
    @return Return value of the function */
PUBLIC VARIANT FUNCTION RunInSeparatePrimary(FUNCTION PTR func, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions([ work := FALSE
                             , openprimary := TRUE
                             ], options);

  OBJECT savetrans;
  IF(HavePrimaryTransaction()) //even in WASM we need to go through these motions to fix subtle table autobinding issues
  {
    savetrans := GetPrimary();
    SetPrimaryWebhareTransaction(0);
  }

  OBJECT mytrans;
  TRY
  {
    IF(options.openprimary)
      mytrans := OpenPrimary();

    IF(IsWASM()) //in WASM openprimary doesn't need to have an effect, as it's no extra overhead anyway
      EM_Syscall("startSeparatePrimary");

    IF(options.work)
      mytrans->BeginWork();

    VARIANT res;
    IF(__INTERNAL_DEBUGGETFUNCTIONPTRRETURNTYPE(func) NOT IN [0,2])
    {
      res := func();
    }
    ELSE
    {
      func();
      res := TRUE;
    }
    IF(options.work)
      mytrans->CommitWork();
    RETURN res;
  }
  FINALLY
  {
    IF(IsWASM())
      EM_Syscall("stopSeparatePrimary");
    IF(HavePrimaryTransaction())
      GetPrimary()->Close();
    IF(ObjectExists(savetrans))
      SetPrimaryWebhareTransaction(savetrans->id);
  }
}

/** Returns current primary transaction
    @return Current primary transaction. Throws if there is no primary transaction.
*/
PUBLIC OBJECT FUNCTION GetPrimary()
{
  OBJECT primary := GetPrimaryWebhareTransactionObject();
  IF (NOT ObjectExists(primary))
    THROW NEW Exception("No primary transaction is currently opened");
  RETURN primary;
}
/** Return the WebHare user id under which we're accessing the system
    @return User authobject id, or 0 if no effective user set*/
PUBLIC INTEGER FUNCTION GetEffectiveUserId()
{
  RETURN ObjectExists(effectiveuser) ? effectiveuser->authobjectid : 0;
}
/** Return the WebHare user object under which we're accessing the system
    @return User object, or DEFAULT OBJECT if not set */
PUBLIC OBJECT FUNCTION GetEffectiveUser()
{
  RETURN effectiveuser;
}

PUBLIC STRING FUNCTION __GetSiteNameForObjectId(INTEGER objid)
{
  OBJECT trans;
  IF(NOT HavePrimaryTransaction())
    trans := OpenPrimary();

  STRING name := SELECT AS STRING sites.name
                   FROM system.sites, system.fs_objects
                  WHERE sites.id = fs_objects.parentsite
                          AND fs_objects.id = objid;
  IF(ObjectExists(trans))
    trans->Close();
  RETURN name;
}
