﻿<?wh
LOADLIB "wh::util/algorithms.whlib";
LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/virtualfs/base.whlib";
LOADLIB "mod::system/lib/virtualfs/modular.whlib";
LOADLIB "mod::system/lib/virtualfs/diskfolder.whlib";
LOADLIB "mod::system/lib/internal/whcore_interface.whlib";
LOADLIB "mod::system/lib/resources.whlib";

PUBLIC OBJECTTYPE ModuleFS EXTEND VirtualModularFSBase
<
  STRING systemmoduleroot;

  MACRO NEW()
  {
    this->systemmoduleroot := GetInstallationRoot() || "modules/";
  }

  UPDATE OBJECT FUNCTION GetSubfolder(STRING subfolder)
  {
    STRING root := GetModuleInstallationRoot(subfolder);
    IF (root = "" OR NOT GetEffectiveUser()->HasRight("system:sysop"))
      THROW NEW VirtualFSException("BADPATH", "No such data path");

    OBJECT fs := NEW DiskFolderFS(root, root NOT LIKE this->systemmoduleroot || "*", FALSE);
    fs->readonlymasks := [ '/history', '/history/*' ];
    RETURN fs;
  }

  UPDATE RECORD ARRAY FUNCTION GetSubfolderListing()
  {
    BOOLEAN has_sysop := GetEffectiveUser()->HasRight("system:sysop");
    IF (NOT has_sysop)
      THROW NEW VirtualFSException("BADPATH","No data paths");

    RECORD ARRAY modules :=
        SELECT foldername
             , modificationdate :=    DEFAULT DATETIME
             , root :=                GetModuleInstallationRoot(foldername)
          FROM ToRecordArray(GetInstalledModuleNames(), "FOLDERNAME")
      ORDER BY foldername;

    RECORD ARRAY dbdata :=
        SELECT *
          FROM system.modules;

    FOREVERY (RECORD rec FROM dbdata)
    {
      RECORD lookup := RecordLowerBound(modules, [ foldername := rec.name ] , [ "FOLDERNAME" ]);
      IF (lookup.found)
        modules[lookup.position].modificationdate := rec.modificationdate;
    }

    RECORD ARRAY result :=
        SELECT name := foldername
             , type := 1
             , size := 0
             , read := TRUE
             , write := has_sysop AND root NOT LIKE this->systemmoduleroot || "*"
             , modificationdate
             , creationdate := modificationdate
          FROM modules;

    RETURN result;
  }

>;
