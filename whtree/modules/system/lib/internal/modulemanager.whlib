﻿<?wh
LOADLIB "wh::datetime.whlib";
LOADLIB "wh::dbase/transaction.whlib";
LOADLIB "wh::files.whlib";
LOADLIB "wh::util/algorithms.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/logging.whlib";
LOADLIB "mod::system/lib/internal/registry.whlib";
LOADLIB "mod::system/lib/internal/dbase/updatecommands.whlib";
LOADLIB "mod::system/lib/internal/scripts.whlib";
LOADLIB "mod::system/lib/internal/dbase/parser.whlib";
LOADLIB "mod::system/lib/internal/userrights/rightsparser.whlib";
LOADLIB "mod::system/lib/resources.whlib";


PUBLIC RECORD ARRAY FUNCTION GetModuleDatabaseSchema(STRING modname)
{
  OBJECT xmldoc := GetModuleDefinitionXML(modname);
  OBJECT specs := xmldoc->documentelement->GetChildElementsByTagNameNS("http://www.webhare.net/xmlns/system/moduledefinition", "databaseschema");
  RECORD ARRAY retval;
  FOREVERY(OBJECT spec FROM specs->GetCurrentElements())
    INSERT ParseWHDBSchemaSpec(modname, spec) INTO retval AT END;

  RETURN retval;
}

/** @short
    @param pre If true: pre-init phase: Create schemas, roles, and migrate public tables to their schemas
               If false: post-init phase: Remove obsoleted grants
*/
PUBLIC RECORD FUNCTION InitModuleIndependents(RECORD ARRAY modules, BOOLEAN list_complete, BOOLEAN pre)
{
  //ADDME: If we could execute failing SQL commands without destroying the transaction, we could do with less commits ?
  RECORD result := [ commitmessages := DEFAULT RECORD ARRAY, commands := DEFAULT RECORD ARRAY ];

  GetPrimary()->BeginWork([ mutex := "system:modulemanager.initmoduleindependents" ]);

//  IF (NOT pre) //FIXME reuse data from GRSD
  {
    RECORD rightsschemaspec := GetRightsSchemaDefinition(pre);
    INSERT rightsschemaspec INTO modules[0].databaseschemas AT 0;
  }

  FOREVERY(RECORD module FROM modules)
  {
    RECORD cmds;
    IF(pre)
      cmds := GenerateIndependentSQLCommands(module.databaseschemas, GetPrimary());
    ELSE
      cmds := GeneratePostupdateSQLCommands(module.databaseschemas, GetPrimary());

    IF(NOT RecordExists(SELECT FROM module.databaseschemas WHERE name = module.name)
       AND GetPrimary()->SchemaExists(module.name)
       AND Length(GetPrimary()->GetTableListing(module.name)) = 0)
    {
      //Remove empty schema not claimed by this module
      GetPrimary()->DropSchema(module.name);
    }

    IF(Length(cmds.errors)>0) //ADDME: Decide what to do with non critical modules? Mark as disabled?
    {
      FOREVERY(STRING err FROM cmds.errors)
        INSERT INTO result.commitmessages(module,text) VALUES(module.name, err) AT END;
      CONTINUE;
    }

    result.commands := result.commands CONCAT SELECT *, module:=module.name FROM cmds.commands;
  }
  ExecuteSQLUpdates(GetPrimary(), result.commands);

  //ADDME: Blame them to specific modules
  result.commitmessages := result.commitmessages CONCAT CheckedCommitWork();

  RETURN result;
}

PUBLIC RECORD ARRAY FUNCTION CheckedCommitWork()
{
  RECORD ARRAY msgs;
  TRY
  {
    GetPrimary()->CommitWork();
  }
  CATCH(OBJECT<DatabaseException> e)
  {
    LogHarescriptException(e);
    FOREVERY(RECORD err FROM e->errors)
      INSERT INTO msgs(module, text) VALUES("", err.message) AT END;
  }
  RETURN msgs;
}

/* This function is invoked per module, in their calculated dependency order

*/
PUBLIC RECORD FUNCTION InitModuleTables(RECORD module)
{
  RECORD result := [ commitmessages := DEFAULT RECORD ARRAY, commands := DEFAULT RECORD ARRAY ];

  GetPrimary()->BeginWork([ mutex := "system:modulemanager.initmoduletables" ]);

  RECORD cmds;
  IF(Length(module.databaseschemas)>0)
    cmds := GenerateDependentSQLCommands(module.databaseschemas, GetPrimary());
  ELSE
    cmds := [ commands := DEFAULT RECORD ARRAY
            , errors := DEFAULT STRING ARRAY
            ];

  IF(Length(cmds.errors)>0) //ADDME: Decide what to do with non critical modules? Mark as disabled?
  {
    FOREVERY(STRING err FROM cmds.errors)
      INSERT INTO result.commitmessages(module,text) VALUES(module.name, err) AT END;
    RETURN result;
  }

  result.commands := SELECT *, module := module.name FROM cmds.commands;
  TRY
  {
    ExecuteSQLUpdates(GetPrimary(), result.commands);
  }
  CATCH(OBJECT e) //explicitly catching as we don't have errordelay currently... get at least ONE error
  {
    GetPrimary()->RollbackWork();
    INSERT [ module := module.name, text := e->what ] INTO result.commitmessages AT END;
    RETURN result;
  }

  result.commitmessages := result.commitmessages CONCAT CheckedCommitWork();
  RETURN result;
}

PUBLIC RECORD FUNCTION InitModuleRights(RECORD ARRAY modules, BOOLEAN update_module_modtime)
{
  //TODO it's most likely that any issues here are reported through exceptions, not through 'errors' lists, so we need to actually support *that* ...
  GetPrimary()->BeginWork([ mutex := "system:modulemanager.initmodulerights" ]);

  RECORD result := [ commitmessages := DEFAULT RECORD ARRAY, commands := DEFAULT RECORD ARRAY ];

  FOREVERY(RECORD mod FROM modules)
  {
    RECORD rightres := CreateModuleRights(mod, update_module_modtime);
    IF(Length(rightres.errors)>0) //ADDME: Decide what to do with non critical modules? Mark as disabled?
    {
      FOREVERY(RECORD err FROM rightres.errors)
        INSERT INTO result.commitmessages(module,text) VALUES(mod.name, err.message) AT END;
      GetPrimary()->RollbackWork();
      RETURN result;
    }
  }

  RECORD rightschemadef := GetRightsSchemaDefinition(FALSE);

  RECORD right_cmds := GenerateDependentSQLCommands([rightschemadef], GetPrimary());
  IF(Length(right_cmds.errors)>0) //ADDME: Decide what to do with non critical modules? Mark as disabled?
  {
    FOREVERY(STRING err FROM right_cmds.errors)
      INSERT INTO result.commitmessages(module,text) VALUES("system_rights", err) AT END;
    GetPrimary()->RollbackWork();
    RETURN result;
  }

  result.commands := SELECT *, module := "system_rights" FROM right_cmds.commands;
  ExecuteSQLUpdates(GetPrimary(), result.commands);

  //ADDME: Blame them to specific modules
  result.commitmessages := result.commitmessages CONCAT CheckedCommitWork();

  GetPrimary()->BroadcastOnCommit("system:config.rights", DEFAULT RECORD);

  RETURN result;
}

/** Given a list of partilly filled in colummn definitions, complete them with the defaults
    @param coldefs List of column definitions
    @param primarykeyname Case sensitive name of primary column (affects defaults for nullable, noupdate and isunique)
    @return List of Column definitions that can be fed to whdb schema builder
*/
RECORD ARRAY FUNCTION CompleteRightsColdefs(RECORD ARRAY coldefs, STRING primarykeyname)
{
  RECORD defaultcol :=
      [ dbtype := ""
      , defval := ""
      , name   := ""
      , autonumberstart := 0
      , internalcolumnname := ""
      , nullable := TRUE
      , noupdate := FALSE
      , isunique := FALSE
      , ondelete := ""
      , references_schema := ""
      , references_table := ""
      , maxlength := 0
      ];

  RECORD ARRAY results;
  FOREVERY (RECORD coldef FROM coldefs)
  {
    coldef := MakeUpdatedRecord(defaultcol, coldef);
    IF (coldef.name = primarykeyname)
    {
      coldef.nullable := FALSE;
      coldef.noupdate := TRUE;
      coldef.isunique := TRUE;
    }
    INSERT coldef INTO results AT END;
  }

  RETURN results;
}

/** Returns (part of) the schema definition for the system_rights schema
    @param dofilter If true, return only the tables for objecttypes with the if in @a filteron
    @param filteron If filtering, id's of the objecttypes to look at
    @param delete_obsolete If TRUE, delete objecttype entries that aren't referenced
        anymore (and don't have grants in rights for that type), and drop unreferenced rights tables
    @param allmodules List of all modules, used when deleting obsolete rights
    @return Schema definition
*/
RECORD FUNCTION GetRightsSchemaDefinition(BOOLEAN pre)
{
  RECORD retval := [ msgs := DEFAULT RECORD ARRAY
                   , success := TRUE
                   , tables := DEFAULT RECORD ARRAY
                   , obsoletetables := DEFAULT RECORD ARRAY
                   , name := "system_rights"
                   ];

  IF(pre)
    RETURN retval;

  STRING ARRAY currentrightstables := SELECT AS STRING ARRAY ToLowercase(table_name) FROM GetPrimary()->GetTableListing("system_rights");

  STRING ARRAY missingtablesfor := __GetCacheableModuleObjectTypes().value.missingrightstables;
  IF("global_rights" NOT IN currentrightstables)
    INSERT "#global" INTO missingtablesfor AT END;

  FOREVERY(STRING missing FROM missingtablesfor)
  {
    STRING tablename, t_schema, t_table;
    IF(missing = "#global")
    {
      tablename := "global_rights";

      /* For global rights, use a self-reference to make sure the column will
         be nullable. The object value stored is always 0, so no problems with
         the contents of that field.
      */
      t_schema := "system_rights";
      t_table := "global_rights";
    }
    ELSE
    {
      t_schema := Tokenize(missing,'.')[0];
      t_table := Tokenize(missing,'.')[1];

      FOR(INTEGER trynum := 1;; trynum := trynum + 1)
      {
        //for backwards compatibility, simply desire that these tables are named "system_rights.o_<some number>". we used to follow tableid/oid
        tablename := "o_" || trynum;
        IF(tablename NOT IN currentrightstables)
          BREAK;
      }
    }

    INSERT tablename INTO currentrightstables AT END;
    RECORD tablerec :=
        [ name := tablename
        , primarykey := "id"
        , indices := DEFAULT RECORD ARRAY
        , obsoletecols := [[ name := "description" ]]
        , obsoleteindices := DEFAULT RECORD ARRAY
        , legacy_readaccessmgr := ""
        , legacy_writeaccessmgr := ""
        , cols := CompleteRightsColdefs(
              [ [ name := "id"
                , dbtype := "INTEGER"
                , autonumberstart := 1
                ]
              , [ name := "grantee"
                , dbtype := "INTEGER"
                , references_schema := "system"
                , references_table := "authobjects"
                , ondelete := "cascade"
                ]
              , [ name := "grantor"
                , dbtype := "INTEGER"
                , references_schema := "system"
                , references_table := "authobjects"
                , ondelete := "set default"
                ]
              , [ name := "object"
                , dbtype := "INTEGER"
                , references_schema := t_schema
                , references_table := t_table
                , ondelete := "cascade"
                ]
              , [ name := "right"
                , dbtype := "INTEGER"
                , references_schema := "system"
                , references_table := "module_rights"
                , ondelete := "cascade"
                ]
              , [ name := "withgrantoption"
                , dbtype := "BOOLEAN"
                ]
              , [ name := "creationdate"
                , dbtype := "DATETIME"
                ]
              , [ name := "comment"
                , dbtype := "VARCHAR"
                , maxlength := 2048
                ]
              , [ name := "grantordata"
                , dbtype := "VARCHAR"
                , maxlength := 2048
                ]
              ], "id")
        ];

    INSERT tablerec INTO retval.tables AT END;
  }
  RETURN retval;
}

RECORD FUNCTION CreateModuleAuthobjects(RECORD module, BOOLEAN update_module_modtime)
{
  RECORD retval :=
      [ error :=        TRUE
      , messages :=     DEFAULT STRING ARRAY
      , id :=           0
      ];

  INTEGER transid := GetBindingFromSchema(system).transaction;
  OBJECT trans := GetTransactionObjectById(transid);

  //Insert module in system.modules list
  INTEGER module_id :=
      SELECT AS INTEGER id
        FROM system.modules
       WHERE ToLowercase(name) = module.name;

  IF (module_id = 0)
  {
    module_id := MakeAutoNumber(system.modules, "ID");

    IF (update_module_modtime)
      INSERT INTO system.modules(id, name, modificationdate) VALUES (module_id, module.name, GetCurrentDateTime());
    ELSE
      INSERT INTO system.modules(id, name) VALUES (module_id, module.name);
  }
  ELSE IF (update_module_modtime)
    UPDATE system.modules SET modificationdate := GetCurrentDateTime() WHERE id = module_id;


  retval.id := module_id;
  retval.error := FALSE;
  RETURN retval;
}


RECORD FUNCTION CreateModuleRights(RECORD module, BOOLEAN update_module_modtime)
{
  RECORD ARRAY errors;
  INTEGER ARRAY objecttypes;

  INTEGER transid := GetBindingFromSchema(system).transaction;
  OBJECT trans := GetTransactionObjectById(transid);

  RECORD cmares := CreateModuleAuthobjects(module, update_module_modtime);
  IF (cmares.error)
  {
    FOREVERY (STRING msg FROM cmares.messages)
      INSERT
          [ module := module.name
          , message := msg
          ] INTO errors AT END;
  }

  INSERT CELL id := cmares.id INTO module;
  INTEGER module_id := cmares.id;

  RECORD ARRAY db_rights;
  STRING ARRAY dependencies; //TODO this should be a checkmodule or wh check thing, and not something to report during softreset ?

  FOREVERY (RECORD rec FROM module.modulerights)
  {
    STRING localname := SubString(rec.name, SearchSubString(rec.name, ":") + 1);
    INTEGER right_id;

    // Search the existing rights record, in the rights for the current module.
    RECORD existing_rec :=
        SELECT id
             , name
          FROM system.module_rights
         WHERE name = localname
           AND COLUMN module = module_id;

    // Go after the object type
    STRING objtype_name := rec.objtype;
    IF (objtype_name = "")
      objtype_name := "system:#global";

    INTEGER objtype_cpos := SearchSubString(objtype_name, ":");

    STRING objtype_module := LEFT(objtype_name, objtype_cpos);

    IF (objtype_module NOT IN dependencies)
      INSERT objtype_module INTO dependencies AT END;

    // Re-insert the right if not present or incompatible.
    IF (NOT RecordExists(existing_rec))
    {
      right_id := MakeAutoNumber(system.module_rights, "ID");

      INSERT INTO system.module_rights(id, name, module)
             VALUES(right_id, localname, module_id);
    }
    ELSE
    {
      right_id := existing_rec.id;
    }

    RECORD ARRAY new_impliedbys;
    FOREVERY (RECORD ib FROM rec.impliedbys)
    {
      STRING ibrightname := ib.right;
      INTEGER ibmodule := module_id;
      INTEGER pos := SearchSubString(ibrightname, ":");
      IF (pos != -1)
      {
        STRING modulename := ToLowercase(LEFT(ibrightname, pos));
        ibrightname := SubString(ibrightname, pos + 1);
        ibmodule :=
            SELECT AS INTEGER id
              FROM system.modules
             WHERE name = modulename;

        IF (ibmodule = 0)
        {
          INSERT
              [ module := module.name
              , message := "Can't find module " || modulename
              ] INTO errors AT END;
          CONTINUE;
        }
        IF (modulename NOT IN dependencies)
          INSERT modulename INTO dependencies AT END;
      }

      INTEGER ibrightid :=
          SELECT AS INTEGER id
            FROM system.module_rights
           WHERE name = ibrightname
             AND COLUMN module = ibmodule;

      IF (ibrightid = 0)
      {
        INSERT
            [ module := module.name
            , message := "Can't find right '" || ib.right || "' implied by '" || rec.name || "'"
            ] INTO errors AT END;
        CONTINUE;
      }

      IF (ibrightid = right_id)
      {
        IF (NOT rec.isrootright)
        {
          INSERT
              [ module := module.name
              , message := "Only root rights may be implied by themselves"
              ] INTO errors AT END;
          // FIXME: when we have by field impliedbys, this becomes acceptable.
        }
        ELSE
          CONTINUE; // Don't put it in the DB.
      }

      INSERT
        [ impliedby := ibrightid
        ] INTO new_impliedbys AT END;
    }

    // Check all impliedbys
    new_impliedbys :=
      SELECT *
        FROM new_impliedbys
       ORDER BY impliedby;

    RECORD ARRAY current_impliedbys :=
      SELECT id
           , impliedby
        FROM system.module_impliedbys
       WHERE COLUMN right = right_id
    ORDER BY impliedby;

    BOOLEAN is_thesame := LENGTH(current_impliedbys) = LENGTH(new_impliedbys);
    IF (is_thesame)
    {
      FOREVERY (RECORD r FROM current_impliedbys)
      {
        RECORD n := new_impliedbys[#r];
        is_thesame := is_thesame AND (r.impliedby = n.impliedby);
      }
    }
    IF (NOT is_thesame)
    {
      DELETE FROM system.module_impliedbys WHERE COLUMN right = right_id;
      FOREVERY (RECORD impliedbyrec FROM new_impliedbys)
        INSERT INTO system.module_impliedbys(right, impliedby)
               VALUES (right_id, impliedbyrec.impliedby);
    }
    INSERT
        [ right_id      := right_id
        , modright      := rec
        , is_thesame    := is_thesame
        , new_impliedbys:= new_impliedbys
        ] INTO db_rights AT END;
  }

  FOREVERY (STRING modulename FROM dependencies)
  {
    IF (modulename NOT IN module.dependencies AND modulename != module.name)
    {
      INSERT
          [ module := module.name
          , message := "Module " || modulename || " is referenced, but not listed as dependency"
          ] INTO errors AT END;
    }
  }

  FOREVERY (RECORD rec FROM db_rights)
  {
    // All rights are now in the DB.
    // - check the impliedwith for cycles

    RECORD db_rec :=
        SELECT *
          FROM system.module_rights
         WHERE id = rec.right_id;

    // Check impliedbys
    IF (NOT rec.is_thesame)
    {
      // check for implied-by-cycles
      INTEGER ARRAY worklist := SELECT AS INTEGER ARRAY impliedby FROM rec.new_impliedbys;
      INTEGER ARRAY seen := [ INTEGER(rec.right_id) ];
      WHILE (LENGTH(worklist) != 0)
      {
        INTEGER ARRAY new_worklist;
        FOREVERY (INTEGER i FROM worklist)
        {
          IF (i IN seen)
          {
            INSERT
              [ module := module.name
              , message := "Right " || rec.modright.name || " is implied by itself"
              ] INTO errors AT END;
          }
          ELSE
          {
            new_worklist := new_worklist CONCAT
                SELECT AS INTEGER ARRAY impliedby
                  FROM system.module_impliedbys
                 WHERE COLUMN right = i;
//                   AND impliedby != i AND fieldname != ""; // FIXME: check for self-recursive stuffs?
          }
        }
        worklist := new_worklist;
      }
    }
  }

  RETURN
    [ errors := errors
    , objecttypes := objecttypes
    ];
}

/** Run all runonce scripts
    @param module Parsed module data
    @param phase Phase to run
    @param showinfo IF TRUE, print debug data to stdout
    @return Return result
    @cell return.error Whether an error has occurred
    @cell return.output Output of all scripts
*/
PUBLIC RECORD FUNCTION RunRunOnceScripts(RECORD module, STRING phase, BOOLEAN showinfo)
{
  RECORD retval :=
    [ error     := FALSE
    , messages  := DEFAULT STRING ARRAY
    ];

  STRING ARRAY addkeys;

  FOREVERY (RECORD rec FROM module.runoncescripts)
  {
    // See if we need to run a relevant script
    IF(rec.when != phase)
      CONTINUE;

    STRING regkey := "system.servicemanager.runonce." || rec.tag; //tag = modulename:scripttag

    // Check the registry if this script needs to be run
    BOOLEAN must_run := ReadRegistryKey(regkey, [ fallback := DEFAULT DATETIME ]) = DEFAULT DATETIME;
    IF (NOT must_run)
      CONTINUE;

    LogDebug("system:modulemanager", CELL[ type := "runonce-start", module := module.name, phase, rec.script ]);
    DATETIME start := GetCurrentDateTime();
    IF(showinfo)
      Print(`>> ${module.name}: ${phase}: ${rec.script}\n`);

    // Go and run.
    RECORD res := LaunchScript(rec.script, DEFAULT STRING ARRAY, showinfo);
    INTEGER runtime_ms := GetMsecsDifference(start, GetCurrentDateTime());

    retval.error := retval.error OR res.error;
    IF (res.output != "")
    {
      INSERT `Runonce script '${rec.tag}' (${rec.script}):` INTO retval.messages AT END;
      retval.messages := retval.messages CONCAT Tokenize(res.output, "\n");
    }

    LogDebug("system:modulemanager", CELL[ type := "runonce-finish", module := module.name, phase, rec.script, start, runtime_ms, retval, res]);
    IF(showinfo)
      Print(`>> ${module.name}: ${phase}: ${rec.script} finished (${runtime_ms} ms)\n`);
    ClearAllSchemaCaches();

    IF(NOT res.error)
    {
      GetPrimary()->BeginWork([ mutex := "system:modulemanager.setrunoncefinishtime" ]);
      WriteRegistryKey(regkey, GetCurrentDateTime(), [createifneeded := TRUE]);
      GetPrimary()->CommitWork();
    }
  }

  RETURN retval;
}
