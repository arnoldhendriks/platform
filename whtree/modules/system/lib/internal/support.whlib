﻿<?wh
LOADLIB "wh::os.whlib";

LOADLIB "mod::publisher/lib/internal/siteprofiles/cache.whlib";

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/internal/rightsmgmt.whlib";
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";

LOADLIB "mod::tollium/lib/dragdrop.whlib";
LOADLIB "mod::tollium/lib/gettid.whlib";


PUBLIC INTEGER ttl_updownsession := 720; //minutes: how long an upload/download session persists (used to be 30, extended as long as RTE/upload fields don't autoextend lifetimes)


PUBLIC STRING FUNCTION GetFSObjectTolliumIcon(RECORD obj, RECORD typeinfo)
{
  IF(obj.id = 1)
    RETURN "tollium:folders/repository";
  IF(obj.issite)
    RETURN "tollium:folders/site";
  IF(RecordExists(typeinfo) AND typeinfo.tolliumicon != "")
    RETURN typeinfo.tolliumicon;
  RETURN obj.isfolder ? whconstant_publisher_foldericonfallback : whconstant_publisher_fileiconfallback;
}

PUBLIC RECORD FUNCTION EnrichFSObject(RECORD obj)
{
  RECORD typeinfo := DescribeContentTypeById(obj.type, [ isfolder := obj.isfolder, mockifmissing := TRUE ]);
  RETURN CELL [ ...obj
              , candownload := typeinfo.blobiscontent
              , ispublishable := typeinfo.ispublishable
              , pvt_iconname := GetFSObjectTolliumIcon(obj, typeinfo)
              ];
}

/** Returns the ids of all the units that the user may see, but may not see their parents (or don't have parents)
    @param user User to check
    @return List of ids of all root viewable units
*/
PUBLIC INTEGER ARRAY FUNCTION GetRootViewableUnits(OBJECT user)
{
  RETURN GetRootObjectsForRights([ "system:browseunits" ], user);
}




STRING ARRAY typeicons :=
    [ ""                        // Type 0: n/a
    , "tollium:users/user"      // Type 1: user
    , "tollium:users/users"     // Type 2: unit
    , "tollium:users/mask"    // Type 3: role
    , "tollium:system/module"   // Type 4: module
    , "tollium:placeholders/na" // Type 5: database role
    ];

PUBLIC OBJECTTYPE UnitObjectTypeDescriber EXTEND ObjectTypeDescriber
< UPDATE PUBLIC RECORD FUNCTION DescribeObject(INTEGER objectid)
  {
    RECORD data :=
      SELECT id
           , name
           , icon     := type > 0 AND type <= 5 ? typeicons[type] : "tollium:placeholders/na"
           , parent
        FROM system.authobjects
       WHERE id = objectid
         AND type < 4; // Don't present modules and roles

    RETURN data;
  }

  UPDATE PUBLIC INTEGER ARRAY FUNCTION GetRootObjects(OBJECT user)
  {
    RETURN GetRootViewableUnits(user);
  }

  PUBLIC UPDATE INTEGER ARRAY FUNCTION GetObjectChildren(INTEGER id, OBJECT user)
  {
    // Explore only units
    RETURN
        SELECT AS INTEGER ARRAY COLUMN id
          FROM system.authobjects
         WHERE COLUMN parent = VAR id
           AND type = 2;
  }


  UPDATE PUBLIC BOOLEAN FUNCTION IsObjectVisibleForUser(INTEGER id, OBJECT user)
  {
    RETURN user->HasRightOn("system:browseunits", id);
  }
>;

PUBLIC OBJECTTYPE FSObjectTypeDescriber EXTEND ObjectTypeDescriber
<
  INTEGER ARRAY tohide;

  MACRO NEW()
  {
    this->tohide := GetWHFSObjectsToHide(GetEffectiveUser());
  }

  UPDATE PUBLIC RECORD FUNCTION DescribeObject(INTEGER objectid)
  {
    IF (objectid IN this->tohide)
      RETURN DEFAULT RECORD;

    RECORD objinfo := SELECT id
                           , issite := id=parentsite
                           , name
                           , sortkey  := (isfolder ? "A" : "B") || ToUppercase(name)
                           , isfolder
                           , type
                        FROM system.fs_objects
                       WHERE id = objectid AND (VAR whconstant_whfsid_private IN this->tohide ? isactive : TRUE);

    IF(NOT RecordExists(objinfo))
      RETURN DEFAULT RECORD;

    RECORD enriched := EnrichFSObject(objinfo);
    INSERT CELL icon := enriched.pvt_iconname INTO enriched;
    RETURN enriched;
  }

  UPDATE PUBLIC BOOLEAN FUNCTION IsObjectVisibleForUser(INTEGER id, OBJECT user)
  {
    //if we're hiding webhare-private (10), don't show inactive objects
    IF(whconstant_whfsid_private IN this->tohide AND RecordExists(SELECT FROM system.fs_objects WHERE fs_objects.id = VAR id AND fs_objects.isactive = FALSE))
      RETURN FALSE;

    RETURN ObjectTypeDescriber::IsObjectVisibleForUser(id, user);
  }
>;


PUBLIC OBJECTTYPE webdav_datafoldersdescriber EXTEND ObjectTypeDescriber
< PUBLIC UPDATE RECORD FUNCTION DescribeObject(INTEGER objectid)
  {
    RETURN
      SELECT id
           , name     := foldername
           , icon     := "tollium:folders/shared"
        FROM system.webdav_datafolders
       WHERE id = objectid;
  }
>;

PUBLIC OBJECTTYPE AccessRulesDescriber EXTEND ObjectTypeDescriber
< PUBLIC UPDATE RECORD FUNCTION DescribeObject(INTEGER objectid)
  {
    RECORD rec :=
        SELECT *
          FROM system.access
         WHERE id = objectid;

    IF (NOT RecordExists(rec))
      RETURN DEFAULT RECORD;

    // For now, only allow seeing rules where the 'urlaccess' right  makes sense.
    IF (rec.authtype != 2)
      RETURN DEFAULT RECORD;

    STRING webserverbaseurl;
    IF (rec.webserver != 0)
      webserverbaseurl := SELECT AS STRING baseurl FROM system.webservers WHERE id = rec.webserver;
    ELSE
      webserverbaseurl := GetTid("system:rights.access.allwebservers");

    RETURN
        [ id :=         objectid
        , name :=       webserverbaseurl || " - " || rec.path
        , icon :=       "tollium:security/access" // FIXME: better icon
        ];
  }
>;


PUBLIC OBJECTTYPE UnitDrag EXTEND DragTypeDescriber
< UPDATE PUBLIC RECORD FUNCTION GetBaseRecord()
  {
    RETURN
        [ candelete :=  FALSE
        , wrd_id :=     0
        ];
  }
>;

PUBLIC OBJECTTYPE UserDrag EXTEND DragTypeDescriber
< UPDATE PUBLIC RECORD FUNCTION GetBaseRecord()
  {
    RETURN
        [ candelete :=  FALSE
        , wrd_id :=     0
        ];
  }
>;

PUBLIC OBJECTTYPE RoleDrag EXTEND DragTypeDescriber
< UPDATE PUBLIC RECORD FUNCTION GetBaseRecord()
  {
    RETURN
        [ candelete :=  FALSE
        , wrd_id :=     0
        ];
  }
>;

PUBLIC RECORD ARRAY FUNCTION GetSystemAppRunnerConfig()
{
  OBJECT trans := OpenPrimary();
  RECORD ARRAY apps :=
      [ [ name :=     "system:watchsourcefiles"
        , cmdline :=  "runscript --workerthreads 3 mod::system/scripts/internal/watchsourcefiles.whscr"
        ]
      , [ name :=     "system:managedqueue"
        , cmdline :=  "runscript mod::system/scripts/internal/managedqueuemgr.whscr"
        ]
      , [ name :=     "system:executetasks"
        , cmdline :=  "runscript mod::system/scripts/internal/executetasks.whscr"
        , databasemodes := [ "online" ]
        ]
      , [ name :=     "system:poststart"
        , cmdline :=  "runscript mod::system/scripts/internal/post-start.whscr"
        , runatsoftreset := TRUE
        ]
      , [ name :=     "system:precalc"
        , cmdline :=  `runscript --workerthreads ${ReadRegistryKey("system.precalc.maxjobs") + 2} mod::system/lib/internal/precalc/service.whscr`
        , databasemodes := [ "online" ]
        ]
      , [ name :=     "system:chromeheadlessrunner"
        , cmdline :=  "runscript mod::system/scripts/internal/chromeheadlessrunner.whscr"
        ]
      , [ name :=     "system:thebridge"
        , cmdline :=  "runscript --workerthreads 16 mod::system/scripts/internal/thebridge.whscr"
        ]
      ];

  IF (NOT ReadRegistryKey("system.backend.development.manualdebugmgr"))
  {
    INSERT
        [ name :=       "system:debugmgr"
        , cmdline :=    "runscript mod::system/scripts/internal/debugmgr.whscr"
        ] INTO apps AT END;
    INSERT
        [ name :=       "system:debugmgr-ts"
        , cmdline :=    "wh run mod::system/js/internal/whmanager/debugmgr.ts"
        ] INTO apps AT END;
  }

  IF(GetEnvironmentVariable("WEBHARE_WEBSERVER") = "node") //servicemanager will have to manage the HareScript/C++ webserver?
    INSERT
        [ name :=       "system:hswebserver"
        , cmdline :=    "webserver"
        ] INTO apps AT END;

  trans->Close();

  RETURN apps;
}

PUBLIC INTEGER ARRAY FUNCTION GetWHFSObjectsToHide(OBJECT user)
{
  INTEGER ARRAY tohide;
  IF(NOT ObjectExists(user)) //no effective user here
    RETURN DEFAULT INTEGER ARRAY;

  RECORD tweaks := [ showwhfsprivate := user->GetRegistryKey("publisher.filemgr.showwhfsprivate", FALSE)
                   , showwebharebackend := user->GetRegistryKey("publisher.filemgr.showwebharebackend", FALSE)
                   ];

  IF(NOT tweaks.showwhfsprivate)
    tohide := [10,12];
 IF(NOT tweaks.showwebharebackend)
   INSERT 16 INTO tohide AT END;
  RETURN tohide;
}
