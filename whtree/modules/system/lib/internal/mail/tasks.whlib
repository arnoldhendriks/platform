<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::internet/mime.whlib";
LOADLIB "wh::internet/smtp.whlib";
LOADLIB "wh::internet/urls.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/services.whlib";
LOADLIB "mod::system/lib/tasks.whlib";
LOADLIB "mod::system/lib/internal/smtpmgr.whlib";
LOADLIB "mod::system/lib/internal/mail/routemgr.whlib";

OBJECT smtpmgr; //Cache between tasks... the smtpmgr by default keeps connections open for 30 seconds

PUBLIC STRING FUNCTION GenerateTaskMessageId(INTEGER taskid)
{
  RETURN `<${EncryptForThisServer("system:messageid", taskid)}@smtp.webhare.net>`;
}

PUBLIC RECORD FUNCTION CreateMailFromTask(RECORD mailtask)
{
  //Set fields which may not be set in earlier WebHare versions but still on the queue
  mailtask := EnforceStructure([ replytofallsbacktorecipient := FALSE
                               , messageid := ""
                               ], mailtask);

  STRING sender := mailtask.sender;
  // Get defaults for some header fields
  RECORD ARRAY mimeheaders := mailtask.mimeheaders;
  DELETE FROM mimeheaders WHERE ToUppercase(field) IN ["X-WEBHARE-ORIGIN","MESSAGE-ID"];

  STRING fromheader := SELECT AS STRING value FROM mimeheaders WHERE ToUppercase(field)="FROM";
  IF(fromheader="")
  {
    DELETE FROM mimeheaders WHERE ToUppercase(field)="FROM";
    INSERT INTO mimeheaders(field, value) VALUES("From", sender) AT END;
  }

  IF(mailtask.replytofallsbacktorecipient AND (SELECT AS STRING value FROM mimeheaders WHERE ToUppercase(field) = "REPLY-TO") = "")
  {
    DELETE FROM mimeheaders WHERE ToUppercase(field) = "REPLY-TO";
    INSERT [ field := "Reply-To", value := PrettyFormatEmailAddress(mailtask.receiver) ] INTO mimeheaders AT END;
  }

  STRING ARRAY u_mimeheaders_fields := SELECT AS STRING ARRAY ToUppercase(field) FROM mimeheaders;

  RECORD ARRAY baseheaders := GetSMTPEmailHeader(sender, "", "");

  IF(mailtask.messageid != "")
    INSERT [ field := "Message-Id", value := mailtask.messageid ] INTO mimeheaders AT END;

  // Add the missing headers with their default values (when missing)
  FOREVERY(RECORD hdr FROM baseheaders)
  {
    IF (ToUppercase(hdr.field) NOT IN u_mimeheaders_fields)
      INSERT hdr INTO mimeheaders AT END;
  }
  RETURN [ mimeheaders := mimeheaders
         , toppart := mailtask.toppart
         , sender := sender
         , receiver := mailtask.receiver
         ];
}

PUBLIC BLOB FUNCTION GetEMLFromMaiLTask(RECORD mailtask)
{
  RECORD data := CreateMailFromTask(mailtask);
  RETURN EncodeMIMEMessage(data.mimeheaders, data.toppart, [ rawheaders := CellExists(mailtask,"rawheaders") AND mailtask.rawheaders ]);
}

//Send a mail using Webhare's routing rules
PUBLIC RECORD FUNCTION SendWebHareMail(RECORD mail, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions( [ ignorerecipientwhitelist := FALSE
                              , origin := ""
                              , messageid := ""
                              , rawheaders := TRUE
                              ], options, [ required := ["messageid"] ]);

  RECORD baseretval := CELL[ serverip := ""
                           , route := ""
                           , response := ""
                           , success := FALSE
                           , sender := mail.sender
                           , finalsender := mail.sender
                           , finalreceiver := mail.receiver
                           ];

  IF(NOT ObjectExists(smtpmgr))
    smtpmgr := NEW WebHareSMTPManager;

  STRING senderemail := SplitEmailName(mail.sender).email;
  INTEGER sendstatus := IsAllowedEmailSender(senderemail);
  IF(sendstatus = 2) //usemailfromfornonwhitelisted
  {
    STRING mailfrom := ReadRegistryKey("system.services.smtp.mailfrom");
    IF(IsAllowedEmailSender(mailfrom) != 1) //if this one is not whitelisted, we're not allowed to send
      RETURN CELL[ ...baseretval, nextaction := "retry", response := `Fallback sender '${mailfrom}' is not whitelisted` ];

    RECORD split_mailfrom := SplitEmailName(mailfrom);
    RECORD split_origsender := SplitEmailName(senderemail);
    STRING use_name := (split_origsender.name != "" ? split_origsender.name || " " : "")
                       || Substitute(split_origsender.name, '@', " at ");
    senderemail := split_mailfrom.email;
    mail.sender := MakeEmailAddress(use_name, split_mailfrom.email, FALSE);

    UPDATE mail.mimeheaders SET value := mail.sender WHERE ToUppercase(field) = "FROM";
    UPDATE mail.mimeheaders SET value := "<" || senderemail || ">" WHERE ToUppercase(field) IN ["ERRORS-TO","RETURN-PATH"];
  }
  ELSE IF(sendstatus = 0)
  {
    RETURN CELL[ ...baseretval, nextaction := "retry", response := `Sender '${senderemail}' is not whitelisted` ];
  }

  baseretval.finalsender := mail.sender;

  RECORD route := smtpmgr->GetMailroute(senderemail, mail.receiver, options.origin);
  IF(NOT RecordExists(route)) //this used to be 'Cancel' but tempfail makes more sense.. just add a final explicit null-route if you wanted this
    RETURN CELL[ ...baseretval, nextaction := "retry", response := `No mailroute available` ];

  IF(route.serverurl = "infinitedelay") //test destination, simply delay, it will be cancelled at some point - these tasks are meant to be picked up and cancelled by the tests
    RETURN CELL[ ...baseretval, nextaction := "hold" ];

  IF(route.serverurl = "null") //explicit blackhole
    RETURN CELL[ ...baseretval, nextaction := "cancel", success := TRUE, response := `This mailroute is blackholed` ];

  RECORD serverurlunpacked := UnpackURL(route.serverurl);
  IF(serverurlunpacked.password != "")
    serverurlunpacked.password := "xxxxx";
  baseretval.route := RepackURL(serverurlunpacked);

  STRING receiver := smtpmgr->RewriteEmailTo(route, mail.receiver);
  baseretval.finalreceiver := receiver;

  IF(NOT options.ignorerecipientwhitelist AND NOT IsAllowedEmailReceiver(baseretval.finalreceiver))
    RETURN CELL[ ...baseretval, nextaction := "retry", response := `Receiver '${baseretval.finalreceiver}' is not whitelisted` ];

  RECORD smtpconn := smtpmgr->GetSMTPConnection(route.serverurl);
  IF(NOT ObjectExists(smtpconn.conn))
    THROW NEW Exception(smtpconn.errors != "" ? smtpconn.errors : "Unable to connect with any mailserver");

  DELETE FROM mail.mimeheaders WHERE TOUppercase(field)="MESSAGE-ID";
  INSERT [ field := "Message-ID", value := options.messageid ] INTO mail.mimeheaders AT END;

  RECORD sendresult := smtpconn.conn->SendMessage([receiver], mail.sender, mail.mimeheaders, mail.toppart, [ rawheaders := options.rawheaders ]);
  baseretval.serverip := sendresult.smtpserverip;

  IF(NOT sendresult.success OR Length(sendresult.failures)>0) //all recipients were successful
  {
    RECORD error := Length(sendresult.failures) > 0 ? sendresult.failures[0] : smtpconn.conn->lasterror;
    STRING errortext := Length(sendresult.failures) > 0 ? sendresult.failures[0].text : smtpconn.conn->lasterror.message;

    BOOLEAN isfatal := (error.code / 100) = 5;
    STRING message := LimitUTF8Bytes(error.code || " " || errortext, 512);
    RETURN CELL[ ...baseretval, nextaction := isfatal ? "bounce" : "retry", response := message ];
  }
  RETURN CELL[ ...baseretval, nextaction := "sent", success := TRUE, response := sendresult.message ];
}

PUBLIC OBJECTTYPE OutgoingMailTask EXTEND ManagedTaskBase
<
  UPDATE PUBLIC MACRO RunTask(RECORD taskdata)
  {
    RECORD data := CreateMailFromTask(taskdata);

    taskdata := EnforceStructure([ ignorerecipientwhitelist := FALSE //pre 4.28 tasks won't have this flag
                                 , rawheaders := FALSE //and something prior to 4.35 apparently missed this too ?
                                 ], taskdata);

    RECORD sendresult := SendWebHareMail(data, CELL[ taskdata.ignorerecipientwhitelist
                                                   , taskdata.origin
                                                   , messageid := GenerateTaskMessageId(this->GetTaskMetadata().taskid)
                                                   , taskdata.rawheaders
                                                   ]);
    SWITCH(sendresult.nextaction)
    {
      CASE "retry"
      {
        this->ResolveByTemporaryFailure(sendresult, sendresult.response);
      }
      CASE "hold" //test(framework() mail. hold forever, should be manually cleared (or automatically on RunTestframework startup)
      {
        this->ResolveByRestart(AddDaysToDate(1,GetCurrentDatetime()));
      }
      CASE "cancel"
      {
        this->ResolveByCancellation(sendresult, sendresult.response);
      }
      CASE "bounce"
      {
        this->ResolveByFailure(sendresult, sendresult.response);
      }
      CASE "sent"
      {
        this->ResolveByCompletion(sendresult);
      }
      DEFAULT
      {
        THROW NEW Exception(`Unrecognized nextacton '${sendresult.nextaction}'`);
      }
    }
  }
>;

PUBLIC OBJECTTYPE MailStatusCallbackTask EXTEND ManagedTaskBase
<
  UPDATE PUBLIC MACRO RunTask(RECORD taskdata)
  {
    this->ResolveByCompletion(MakeFunctionPtr(taskdata.func)(taskdata.data, taskdata.type, taskdata.status));
  }
>;
