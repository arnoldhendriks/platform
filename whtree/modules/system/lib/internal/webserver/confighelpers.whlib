<?wh
LOADLIB "wh::adhoccache.whlib";
LOADLIB "wh::internet/urls.whlib";
LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";

//an interface server generally running on http://127.0.0.1:13678/
PUBLIC CONSTANT INTEGER whwebserverconfig_rescueportoffset := 0;
PUBLIC CONSTANT INTEGER whwebserverconfig_trustedportid := -2;
PUBLIC CONSTANT INTEGER whwebserverconfig_trustedportid_ipv6 := -3;
PUBLIC CONSTANT INTEGER whwebserverconfig_hstrustedportid := -6;
PUBLIC CONSTANT INTEGER whwebserverconfig_rescueportid := -4;
PUBLIC CONSTANT INTEGER whwebserverconfig_rescuewebserverid := -5;

PUBLIC CONSTANT INTEGER whwebserverconfig_virtualportid := 0;

PUBLIC CONSTANT INTEGER whwebserverconfig_proxywebserverid := -1000; //catches any usage of webhare as a proxy

PUBLIC CONSTANT INTEGER whwebserverconfig_redirecthostoffset := -2000; //all redirecting hosts that mirror a real webserver, are offset by this index

BOOLEAN FUNCTION IsLikeIn(STRING name, STRING ARRAY list)
{
  FOREVERY(STRING item FROM list)
    IF(name LIKE item)
      RETURN TRUE;
  RETURN FALSE;
}


PUBLIC RECORD ARRAY FUNCTION EnumerateAllWebservers(BOOLEAN minimalconfig)
{
  RECORD ARRAY hosts;
  INTEGER rescueport := GetWebHareConfiguration().baseport + whwebserverconfig_rescueportoffset;
  INSERT CELL[ id := whwebserverconfig_rescuewebserverid
             , defaultpages := whconstant_webserver_indexpages
             , is_interface_webserver := TRUE
             , baseurl := `http://127.0.0.1:${rescueport}/`
             , port := whwebserverconfig_rescueportid
             , lowercasemode := TRUE
             , forcehttps := FALSE
             , forcehttpsport := 0
             , type := whconstant_webservertype_interface
             , diskfolder := ""
             , stricttransportsecurity := 0
             ] INTO hosts AT END;

  IF(NOT minimalconfig)
  {
    hosts := hosts CONCAT
             SELECT TEMPORARY forcehttps := port = 0 AND baseurl LIKE "https:*"
                  , TEMPORARY unpackedbaseurl := UnpackURL(baseurl)
                  , id
                  , defaultpages := whconstant_webserver_indexpages
                  , is_interface_webserver := type=1
                  , baseurl
                  , port := webservers.port
                  , lowercasemode := TRUE
                  , forcehttps := forcehttps
                  , forcehttpsport := forcehttps ? unpackedbaseurl.port : 0
                  , type
                  , diskfolder
                  , stricttransportsecurity
               FROM system.webservers
              WHERE type IN [0,whconstant_webservertype_interface];
  }
  RETURN hosts;
}

PUBLIC RECORD FUNCTION GetCacheableHostedSites()
{
  RECORD ARRAY hosts := SELECT id
                             , baseurl
                             , port
                             , isinterface := type = whconstant_webservertype_interface
                          FROM EnumerateAllWebservers(FALSE);
  RECORD ARRAY ports := SELECT id
                             , virtualhost
                             , port
                          FROM system.ports;

  //Also mark our trusted port as a virtualhosted port
  INSERT [ id := whwebserverconfig_trustedportid, virtualhost := TRUE, port := GetWebHareConfiguration().baseport + whconstant_webserver_trustedportoffset ] INTO ports AT END;
  INSERT [ id := whwebserverconfig_hstrustedportid, virtualhost := TRUE, port := GetWebHareConfiguration().baseport + whconstant_webserver_hstrustedportoffset ] INTO ports AT END;
  RETURN [ ttl := 15 * 60 * 1000
         , value := [ hosts := EnrichWithListenHosts(hosts, FALSE)
                    , ports := ports
                    ]
         , eventmasks := [ "system:internal.webserver.didconfigreload" ]
         ];
}
INTEGER FUNCTION GetGlobalOrdering(STRING hostname)
{
  IF(SearchSUbstring(hostname,'*') = -1 AND SearchSUbstring(hostname,'?')=-1) //plain hostname
    RETURN -1;
  IF(hostname="*")
    RETURN 2;
  RETURN hostname != "" AND Right(hostname,1)='*' ? 0 : 1;
}
PUBLIC RECORD ARRAY FUNCTION EnrichWithListenHosts(RECORD ARRAY hosts, BOOLEAN minimalconfig)
{
  RECORD ARRAY aliaslist;
  IF(NOT minimalconfig)
    aliaslist := SELECT webserver, hostname FROM system.webservers_aliases WHERE explicit;
  FOREVERY(RECORD host FROM hosts)
  {
    RECORD unp := UnpackURL(host.baseurl);
    STRING hostname := unp.host LIKE "*::*" ? "[" || ToUppercase(unp.host) || "]" : ToUppercase(unp.host); //wrap IPv6 adresses
    STRING ARRAY listenhosts;

    IF(host.port=0)
    {
      listenhosts := [STRING(hostname)]
                     CONCAT
                     (SELECT AS STRING ARRAY ToUppercase(aliaslist.hostname) FROM aliaslist WHERE aliaslist.webserver = host.id);
    }
    ELSE
    {
      listenhosts := [STRING(hostname) || (unp.isdefaultport?"":":"||unp.port)];
      IF(NOT unp.isdefaultport)
        INSERT "*:" || unp.port INTO listenhosts AT END;
    }

    INSERT CELL hostname := ToLowercase(hostname)
              , listenhosts := listenhosts
           INTO host;
    hosts[#host]:=host;
  }

  FOREVERY(RECORD website FROM hosts)
    hosts[#website].listenhosts := SELECT AS STRING ARRAY host
                                     FROM ToRecordArray(website.listenhosts,'host')
                                 ORDER BY GetGlobalOrdering(host)
                                        , Length(host)
                                        , ToUppercase(host);
  RETURN hosts;
}
PUBLIC RECORD FUNCTION GetHostedSites()
{
  RETURN GetAdhocCached( [ type := "hostedsites" ], PTR GetCacheableHostedSites);
}

PUBLIC RECORD FUNCTION LookupWebserver(RECORD hostinginfo, STRING findhostname, INTEGER findport)
{
  //enumerate all our ports.. bit of a workaround as we may not know whether the host is IPv4 or IPv6
  findhostname := ToUppercase(findhostname);

  RECORD ARRAY ports := SELECT id, virtualhost FROM hostinginfo.ports WHERE ports.port = findport;

  IF(RecordExists(SELECT FROM ports WHERE virtualhost) OR (LENGTH(ports) = 0 AND findport IN [ 80, 443 ]))
  {
    RECORD matchserver;
    /* do we need this?
    IF(findport NOT IN [80,443])
       matchserver := SELECT id,baseurl, isinterface FROM hostinginfo.hosts WHERE findhostname || ":" || findport IN Listenhosts;
       */
    IF(NOT RecordExists(matchserver))
      matchserver := SELECT id,baseurl, isinterface FROM hostinginfo.hosts WHERE findhostname IN Listenhosts;
      /*
    IF(NOT RecordExists(matchserver) AND findport NOT IN [80,443])
      matchserver := SELECT id,baseurl, isinterface FROM hostinginfo.hosts WHERE IsLikeIn(findhostname||":"||findport,Listenhosts);
      */
    IF(NOT RecordExists(matchserver))
      matchserver := SELECT id, baseurl, isinterface FROM hostinginfo.hosts WHERE IsLikeIn(findhostname,Listenhosts);
    IF(RecordExists(matchserver))
      RETURN matchserver;
  }

  FOREVERY(RECORD port FROM ports)
  {
    IF(port.virtualhost)
      CONTINUE;

    RECORD matchserver := SELECT id, baseurl, isinterface := type=1 FROM system.webservers WHERE webservers.port = VAR port.id;
    IF(RecordExists(matchserver))
      RETURN matchserver;
  }
  RETURN DEFAULT RECORD;
}
