﻿<?wh
LOADLIB "wh::adhoccache.whlib";
LOADLIB "wh::datetime.whlib";
LOADLIB "wh::ipc.whlib";
LOADLIB "wh::util/semver.whlib";

LOADLIB "mod::tollium/lib/dragdrop.whlib";
LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/logging.whlib";
LOADLIB "mod::system/lib/resources.whlib";
LOADLIB "mod::system/lib/webserver/management.whlib";
LOADLIB "mod::system/lib/internal/modules/defreader.whlib" EXPORT GetWebhareModules, GetWebhareModuleInfo;
LOADLIB "mod::system/lib/internal/rightsmgmt.whlib";
LOADLIB "mod::system/lib/internal/whconfig.whlib";
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";

///Update the server type. Assumes an available primary transaction.
PUBLIC MACRO SetSystemServerType(STRING newservertype)
{
  IF(newservertype = ReadRegistryKey("system.global.servertype"))
    RETURN;
  IF(newservertype NOT IN systemservertypes)
    THROW NEW Exception(`Invalid system server type ${newservertype}`);

  GetPrimary()->BeginWork();
  WriteRegistryKey("system.global.servertype", newservertype);
  GetPrimary()->CommitWork();

  // TODO This very likely needs to be replaced by a full soft reset
  UpdateSystemConfigurationRecord();

  // Rebuild the webhare config file
  ApplyWebHareConfiguration([ subsytems := ["config"], source := "SetSystemServerType" ]);

  BroadcastEvent("system:config.servertype", DEFAULT RECORD);
  //ADDME access rules should listen to servertype themselves?
}

/** @short Refresh the webserver configuration
    @long Ask the webserver to refresh its configuration read from the database and module definitions. The relevant changes need to be already committed. Returns when the webserver is reconfigured
    @return The configuration load status as reported by ConfigureWebserver. */
PUBLIC RECORD FUNCTION RefreshGlobalWebserverConfig(RECORD options DEFAULTSTO DEFAULT RECORD)
{
  DATETIME now := GetCurrentDatetime();
  options := ValidateOptions( [ nowait := FALSE ], options);
  LogDebug("system:refreshglobalwebserverconfig-start", CELL[ options.nowait ], GetstackTrace());

  OBJECT getconfig := ConnectToManagedPort("system:webserver.getconfig", "webserver");
  IF (NOT ObjectExists(getconfig))
  {
    LogDebug("system:refreshglobalwebserverconfig", CELL[ options.nowait, error := "Webserver configuration port not yet available!" ]);
    RETURN [ error := "Webserver configuration port not yet available!" ];
  }

  RECORD reloadstatus;
  IF(options.nowait)
  {
    getconfig->SendMessage([ task := "rescan" ]);
    LogDebug("system:refreshglobalwebserverconfig", CELL[ options.nowait ]);
  }
  ELSE
  {
    RECORD res := getconfig->DoRequest([ task := "rescan" ]);
    IF (res.status != "ok" OR res.msg.status != "ok")
    {
      LogDebug("system:refreshglobalwebserverconfig-error", CELL[ options.nowait, error := res ]);
      THROW NEW Exception("Reloading webserver configuration failed");
    }
    reloadstatus := res.msg.reloadstatus;
    LogDebug("system:refreshglobalwebserverconfig-done", CELL[ options.nowait, reloadstatus, time := GetDatetimeDifference(now, GetCurrentDatetime()).totalmsecs ]);
  }
  getconfig->Close();
  RETURN reloadstatus;
}

///Reload authentication data and/or modules
PUBLIC MACRO ReloadWebhareConfig2(STRING toreload, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions( [ isdelete := FALSE, nowait := FALSE ], options);

  STRING ARRAY parts := toreload = "" ? DEFAULT STRING ARRAY : Tokenize(toreload, ' ');
  IF("AUTHDATA" IN parts OR "ALL" IN parts)
  {
    TRY
    {
      RefreshGlobalWebserverConfig([ nowait := options.nowait ]);
    }
    CATCH(OBJECT e)
    {
      LogHarescriptException(e);
      /* Not sending the softreset isn't an option, and the "gone" is often triggered by modules disappearing triggering
         a RetrieveResourceException. We then need to rely on the config handler restarting and reloading config
         anyway. So we'll log and ignore */
    }
  }
  BroadcastEvent("system:softreset", [ toreload := parts, isdelete := options.isdelete ]);
}

PUBLIC MACRO ReloadWebhareConfig(BOOLEAN authdata, BOOLEAN modules, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  STRING ARRAY toks;
  IF(authdata)
    INSERT "AUTHDATA" INTO toks AT END;
  IF(modules)
  {
    INSERT "MODULES" INTO toks AT END;
    INSERT "MODULEINIT" INTO toks AT END;
  }
  ReloadWebhareConfig2(Detokenize(toks,' '), options);
}

PUBLIC MACRO SetApplicationAndUserInAuthRecord(RECORD app, RECORD data, OBJECT user, RECORD options)
{
  options := ValidateOptions(
      [ sessionexpires :=   MAX_DATETIME
      ], options);

  RECORD authrec := GetAuthenticationRecord();
  IF (CellExists(authrec, "TOLLIUM"))
    DELETE CELL tollium FROM authrec;

  INSERT CELL tollium :=
      [ app :=
            [ name :=           app.name
            , title :=          app.title
            , data :=           data
            ]
      , user :=
            [ realname :=       user->realname
            , emailaddress :=   user->emailaddress
            , language :=       user->language
            , login :=          user->login
            , eventidentifier := user->GetUserEventIdentifier()
            , wrdentityid :=    user->entityid
            , authobjectid :=   user->authobjectid
            , expires :=        options.sessionexpires
            ]
      ] INTO authrec;

  SetAuthenticationRecord(authrec);
}

PUBLIC RECORD FUNCTION ApplyAccessCheck(RECORD accesscheck, BOOLEAN makeautotrans, OBJECT user)
{
  RECORD authrec := GetAuthenticationRecord();
  IF (NOT CellExists(authrec, "DATABASE"))
    INSERT CELL database := DEFAULT RECORD INTO authrec;

  // Check rights
  IF (NOT DoAccessCheckFor(accesscheck, user))
    RETURN [ success := FALSE, errorcode:="ACCESCHECK" ]; //(ADDME unclean? if we fail, we'll still have modified global stae through initializerightsweblet)

  OBJECT transobj := GetPrimaryWebhareTransactionObject();
  INTEGER ARRAY roleids;
  authrec.database :=
      [ userid :=       ObjectExists(user) ? user->authobjectid : 0
      , readonly :=     FALSE
      , autotrans :=    TRUE
      ];

  SetAuthenticationRecord(authrec);
  RETURN [ success := TRUE ];
}



// ---------------------------------------------------------------------------
//
// Public API
//


PUBLIC RECORD FUNCTION GetObjectEditor(STRING objeditorname)
{
  STRING modname := Left(objeditorname, SearchSubstring(objeditorname, ':'));
  STRING editorname := Substring(objeditorname, Length(modname)+1);

  RECORD modinfo := GetWebhareModuleInfo(modname);
  IF(RecordExists(modinfo))
  {
    RECORD objeditor := SELECT * FROM modinfo.fs_objecteditors WHERE ToUppercase(name)=ToUppercase(modname || ":" || editorname);
    IF(RecordExists(objeditor))
      RETURN objeditor;
  }
  RETURN DEFAULT RECORD;
}

PUBLIC STRING ARRAY FUNCTION GetModuleDependencyIssues()
{
  STRING ARRAY errors;
  RECORD ARRAY allmods := GetWebhareModules();
  RECORD myapplicability := GetMyApplicabilityInfo();
  FOREVERY(RECORD mod FROM allmods)
  {
    IF(RecordExists(mod.packagingapplicability))
    {
      STRING applicabilityerror := GetApplicabilityError(myapplicability, mod.packagingapplicability);
      IF(applicabilityerror != "")
        INSERT `Module ${mod.name} is incompatible with this WebHare version: ${applicabilityerror}` INTO errors AT END;
    }

    IF(mod.circulairdependency)
    {
      INSERT "Module " || mod.name || " is involved in a circular dependency (requires " || Detokenize(mod.dependencies,",") INTO errors AT END;
    }
    FOREVERY(RECORD dep FROM mod.requiredmodules)
    {
      RECORD match := SELECT * FROM allmods WHERE allmods.name = dep.name;
      IF(NOT RecordExists(match))
      {
        INSERT `Module '${mod.name}' requires module '${dep.name}' but it is not installed` INTO errors AT END;
        CONTINUE;
      }
      IF(dep.moduleversion != "" AND NOT VersionSatisfiesRange(match.version, dep.moduleversion))
      {
        INSERT `Module '${mod.name}' requires module '${dep.name}' version ${dep.moduleversion}, got ${match.version}` INTO errors AT END;
        CONTINUE;
      }
    }
  }
  RETURN errors;
}

RECORD FUNCTION GetCacheableGlobalDragTypes()
{
  RECORD ARRAY results;

  STRING ARRAY allmodules := GetInstalledModuleNames();

  FOREVERY(STRING module FROM allmodules)
    allmodules[#module] := ToLowercase(module);

  FOREVERY(STRING module FROM allmodules)
  {
    OBJECT xmldoc;
    TRY
      xmldoc := GetModuleDefinitionXML(module);
    CATCH
      CONTINUE;


    OBJECT ARRAY elts := xmldoc->GetElementsByTagNameNS(whconstant_xmlns_moduledef, "dragtype")->GetCurrentElements();

    FOREVERY (OBJECT elt FROM elts)
    {
      STRING ARRAY flags := Tokenize(elt->GetAttribute("flags"), " ");
      flags := SELECT AS STRING ARRAY flag FROM ToRecordArray(flags, "FLAG") WHERE flag != "";

      STRING objectname := MakeAbsoluteResourcePath(GetModuleDefinitionXMLResourceName(module), elt->GetAttribute("objectname"));

      OBJECT dragtypeobj;
      TRY
      {
        dragtypeobj := MakeObject(objectname);
      }
      CATCH (OBJECT e) {}
      IF (NOT ObjectExists(dragtypeobj))
        THROW NEW Exception(`Dragtype object '${objectname}' could not be found`);
      ELSE IF (dragtypeobj NOT EXTENDSFROM DragTypeDescriber)
        THROW NEW Exception(`Dragtype object '${objectname}' not extending DragTypeDescriber`);

      INSERT
          [ name := module || ":" || elt->GetAttribute("name")
          , candownloadflags := elt->GetAttribute("candownloadflags")
          , objectname := objectname
          , flags := flags
          ] INTO results AT END;
    }
  }

  RETURN
      [ ttl :=   24 * 60 * 60 * 1000
      , eventmasks := ["system:modulesupdate"] //flush if any moduledef changes.
      , value := SELECT * FROM results ORDER BY name
      ];
}

PUBLIC RECORD ARRAY FUNCTION GetGlobalDragTypes()
{
  RETURN GetAdhocCached(
      [ type := "globaldragtypes"
      ], PTR GetCacheableGlobalDragTypes);
}

PUBLIC BOOLEAN FUNCTION IsApplicationAccessible(OBJECT user, RECORD apprec)
{
  RETURN DoAccessCheckFor(apprec.accesscheck, user);
}

/** Executes access check
    @return Returns whether the current user doesn't fail any access check
    @param accesscheck Describes the accesscheck to be done
    @cell accesscheck.type Type of the acccesscheck, valid: right, combine
    @cell accesscheck.value Name of right to check for (only valid when @a type = 'right')
    @cell accesscheck.combine Type of combining, valid: 'and', 'or' (only valid when @a type = 'combine')
    @cell accesscheck.checks List of access check records to combine (only valid when @a type = 'combine')
*/
PUBLIC BOOLEAN FUNCTION DoAccessCheckFor(RECORD accesscheck, OBJECT user)
{
  IF (NOT RecordExists(accesscheck) OR (accesscheck.type="combine" AND accesscheck.combine="and" AND Length(accesscheck.checks)=0))
    RETURN TRUE;
  IF (NOT ObjectExists(user))
    RETURN FALSE;

  TRY
  {
    SWITCH (accesscheck.type)
    {
    CASE "right"
      {
        IF (NOT IsRightGlobal(accesscheck.value))
        {
          RETURN user->HasRightOnAny(accesscheck.value);
        }
        ELSE
        {
          RETURN user->HasRight(accesscheck.value);
        }
      }
    CASE "combine"
      {
        SWITCH (accesscheck.combine)
        {
        CASE "and"
          {
            FOREVERY (RECORD check FROM accesscheck.checks)
              IF (NOT DoAccessCheckFor(check, user))
                RETURN FALSE;
            RETURN TRUE;
          }
        CASE "or"
          {
            FOREVERY (RECORD check FROM accesscheck.checks)
              IF (DoAccessCheckFor(check, user))
                RETURN TRUE;
            RETURN FALSE;
          }
        }
        THROW NEW Exception("Unknown combine type " || accesscheck.combine);
      }
    }
    THROW NEW Exception("Unknown access check type " || accesscheck.type);
  }
  CATCH (OBJECT e)
  {
    //ADDME log the error!?
    RETURN FALSE;
  }
}
