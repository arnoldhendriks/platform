<?wh
LOADLIB "wh::datetime.whlib";
LOADLIB "wh::devsupport.whlib";
LOADLIB "wh::ipc.whlib";
LOADLIB "wh::util/algorithms.whlib";
LOADLIB "wh::internal/wasm.whlib";

/** Connect to the mutex manager, wait for it to come online if needed
    @return(object wh::internal/jobs.whlib#IPCLinkEndPoint) IPC link to the module manager
*/
PUBLIC OBJECT FUNCTION ConnectToMutexManager()
{
  //Wait up to 60 seconds (perhaps a bit more) for the mutexmanager to be reachable
  //it might be unreachable for a few seconds after a crash or during webhare startup
  FOR(INTEGER attempt := 0; attempt < 600; attempt := attempt + 1)
  {
    OBJECT link := ConnectToIPCPort("system:whmanager");
    IF(ObjectExists(link)) //whmanager reached... that gives us contact with the lock
    {
      RECORD global_res := link->DoRequest([ type := "connect", port := "system:mutexmanager" ]);
      IF (global_res.status = "ok" AND global_res.msg.status = "ok")
        RETURN link; //success!

      // Connect to remote port failed, destroy link to whmanager
      link->Close();
    }
    Sleep(100);
  }
  THROW NEW Exception("Unable to connect to the mutex manager");
}

/// Lockmanager for this session
OBJECT lockmgr;

/** Lock object
    @topic modules/services
    @public
*/
OBJECTTYPE MutexLock
< // ---------------------------------------------------------------------------
  //
  // Variables
  //

  /// Name of the mutex
  STRING pvt_name;


  // ---------------------------------------------------------------------------
  //
  // Properties
  //

  /// Name of the mutex
  PUBLIC PROPERTY name(pvt_name, -);

  // ---------------------------------------------------------------------------
  //
  // Constructor
  //

  MACRO NEW(STRING name)
  {
    this->pvt_name := name;
  }

  // ---------------------------------------------------------------------------
  //
  // Public interface
  //

  /// Release the lock of the mutex
  PUBLIC MACRO Release()
  {
    this->Close();
  }

  PUBLIC MACRO Close()
  {
    //override!
  }
>;

STATIC OBJECTTYPE NativeMutexLock EXTEND MutexLock
<
  /// Private ptr to mutex manager
  OBJECT mutexmgr;

  /// Local mutex
  OBJECT pvt_localmutex;

  MACRO NEW(OBJECT mutexmgr, STRING name, OBJECT localmutex) : MutexLock(name)
  {
    this->mutexmgr := mutexmgr;
    this->pvt_localmutex := localmutex;
  }

  UPDATE PUBLIC MACRO Close()
  {
    IF (ObjectExists(this->pvt_localmutex))
    {
      this->pvt_localmutex->Close();
      this->pvt_localmutex := DEFAULT OBJECT;
    }
    this->mutexmgr->ReleaseMutex(this->name);
  }

>;

STATIC OBJECTTYPE WasmMutexLock EXTEND MutexLock
<
  INTEGER mutexid;

  MACRO NEW(STRING name, INTEGER mutexid) : MutexLock(name)
  {
    this->mutexid := mutexid;
  }
  UPDATE PUBLIC MACRO Close()
  {
    EM_SYSCALL("unlockMutex", CELL[this->mutexid]);
  }
>;



/** Mutual exclusion manager
    @topic modules/services
    @public
*/
OBJECTTYPE LockManager
< // ---------------------------------------------------------------------------
  //
  // Variables
  //

  /// Link to webserver mutexmanager script
  OBJECT link;


  /// List of currently locked mutexes
  STRING ARRAY pvt_lockedmutexes;

  /// Whether to send stack traces with lock requests
  BOOLEAN logtraces;

  // ---------------------------------------------------------------------------
  //
  // Properties
  //

  // ---------------------------------------------------------------------------
  //
  // Constructor
  //

  MACRO NEW()
  {
    this->RequireConnection();
  }

  // ---------------------------------------------------------------------------
  //
  // Helper functions
  //

  MACRO RequireConnection()
  {
    IF (NOT ObjectExists(this->link) AND NOT IsWasm())
    {
      this->link := ConnectToMutexManager();
      STRING clientname := GetHarescriptLoadedLibraries()[0].liburi;
      RECORD rec := this->DoRequest([ task := "init", clientname := clientname, groupid := GetCurrentGroupId() ]);
      IF (rec.status = "ok")
        this->logtraces := rec.msg.logtraces;
    }
  }

  MACRO ReleaseMutex(STRING mutexname)
  {
    this->RequireConnection();

    // Send an unlock request, don't care about the result (it is ignored by our dorequests)
    this->link->SendMessage([ task := "unlock", mutexname := mutexname ]);
    RECORD pos := LowerBound(this->pvt_lockedmutexes, mutexname);
    IF (pos.found)
      DELETE FROM this->pvt_lockedmutexes AT pos.position;
  }

  RECORD FUNCTION DoRequest(RECORD message)
  {
    this->RequireConnection();

    RECORD res := this->link->SendMessage(message);
    IF (res.status != "ok")
      RETURN res;

    INTEGER64 msgid := res.msgid;

    INTEGER handle;

    WHILE (TRUE)
    {
      RECORD rec := this->link->ReceiveMessage(MAX_DATETIME);
      IF (rec.status != "ok")
        RETURN rec;

      IF (rec.replyto = 0)
      {
        this->HandleRemoteCommand(rec.msg);
        CONTINUE;
      }
      ELSE IF (rec.replyto != msgid)
        THROW NEW Exception("Received a reply to message " || rec.replyto || " but did DoRequest for " || msgid || ": " || AnyToString(rec, "tree"));

      RETURN rec;
    }
  }

  MACRO ProcessRemoteCommands()
  {
    WHILE (TRUE)
    {
      RECORD res := this->link->ReceiveMessage(DEFAULT DATETIME);
      IF (res.status = "ok")
        this->HandleRemoteCommand(res.msg);
      ELSE
        BREAK;
    }
  }

  MACRO HandleRemoteCommand(RECORD message)
  {
    SWITCH (message.task)
    {
      CASE "setdebugging"
      {
        this->logtraces := message.logtraces;
      }
    }
  }

  // ---------------------------------------------------------------------------
  //
  // Public interface
  //

  /** Locks a mutex
      @param mutexname Name of mutex to lock
      @return(object #MutexLock) Lock
  */
  PUBLIC OBJECT FUNCTION LockMutex(STRING mutexname)
  {
    RETURN this->TryLockMutex(mutexname, MAX_DATETIME);
  }


  /** Tries to locks a mutex
      @param mutexname Name of mutex to lock
      @param wait_until Wait max until this time for the lock.
      @return(object #MutexLock) Lock (DEFAULT OBJECT if not available before wait_until)
  */
  PUBLIC OBJECT FUNCTION TryLockMutex(STRING mutexname, DATETIME wait_until)
  {
    IF(IsWasm())
    {
      RECORD lockresult := WaitForPromise(EM_Syscall("lockMutex", CELL[mutexname, wait_until]));
      IF (lockresult.status = "ok")
        RETURN NEW WasmMutexLock(mutexname, lockresult.mutex);
      ELSE
        RETURN DEFAULT OBJECT;
    }

    BOOLEAN trylock;

    IF (wait_until = DEFAULT DATETIME)
    {
      trylock := TRUE;
      wait_until := MAX_DATETIME;
    }

    RECORD pos := LowerBound(this->pvt_lockedmutexes, mutexname);
    IF (pos.found)
      THROW NEW Exception("Mutex '" || mutexname || "' has already been locked by this session");

    // First lock the local mutex counterpart
    OBJECT localmutex := OpenLocalLockManager()->TryLockLocalMutex(mutexname, trylock ? DEFAULT DATETIME : wait_until);
    IF (NOT ObjectExists(localmutex))
      RETURN DEFAULT OBJECT;

    this->ProcessRemoteCommands();
    TRY
    {
      RECORD ARRAY trace;
      IF (this->logtraces)
        trace := GetAsyncStackTrace();

      RECORD res := this->DoRequest(
          [ task := "lock"
          , mutexname := mutexname
          , trylock := trylock
          , wait_until := wait_until
          , trace := trace
          ]);

      IF (res.status != "ok")
      {
        // mutex manager is gone, remove the link and get a new one the next time
        this->link->Close();
        this->link := DEFAULT OBJECT;
        THROW NEW Exception("The mutual exclusion manager could not be contacted");
      }

      SWITCH (res.msg.status)
      {
      CASE "ok"
        {
          // New mutex locked
          INSERT mutexname INTO this->pvt_lockedmutexes AT pos.position;

          RETURN NEW NativeMutexLock(PRIVATE this, mutexname, localmutex);
        }
      CASE "no"
        {
          // trylock = true, and mutex was already locked
          localmutex->Close();
          RETURN DEFAULT OBJECT;
        }
      CASE "error"
        {
          localmutex->Close();
          THROW NEW Exception("Could not lock mutex: " || res.msg.msg);
        }
      CASE "timeout"
        {
          localmutex->Close();
          RETURN DEFAULT OBJECT;
        }
      }

      localmutex->Close();
      RETURN DEFAULT OBJECT;
    }
    CATCH (OBJECT e)
    {
      localmutex->Close();
      THROW e;
    }
  }


  PUBLIC RECORD FUNCTION GetStatus()
  {
    RECORD res := this->DoRequest([ task := "status" ]);
    IF (res.status != "ok" OR res.msg.status != "ok")
      RETURN DEFAULT RECORD;

    RETURN res.msg;
  }


  /** Return whether the current session has locked mutex 'mutexname'
      @param mutexname Name of mutex
      @return Returns whether the current session has this mutex locked
  */
  PUBLIC BOOLEAN FUNCTION HasLockedMutex(STRING mutexname)
  {
    RETURN LowerBound(this->pvt_lockedmutexes, mutexname).found;
  }

  PUBLIC MACRO SetDebugging(RECORD options)
  {
    options := ValidateOptions(
        [ logtraces :=  FALSE
        , eventlog :=   FALSE
        ], options);

    this->DoRequest(MakeMergedRecord([ task := "setdebugging" ], options));
  }

  /** Noop, kept for legacy purposes.
  */
  PUBLIC MACRO Close()
  {
  }

>;


/** Creates a new mutexmanager object
    @return(object %LockManager) Lock manager
    @topic modules/services
    @public
    @loadlib mod::system/lib/services.whlib
    @example

OBJECT lockmgr := OpenLockManager();
OBJECT lock := lockmgr->LockMutex("mutex:name");

// mutual exclusive stuff

lock->Close();
*/
PUBLIC OBJECT FUNCTION OpenLockManager()
{
  /* The lock manager is currently dependent on the webserver mutexmanager
     script, so locks only function correctly when a single webserver is
     running in the cluster.

     Deadlock detection is not implemented yet.
  */
  IF (NOT ObjectExists(lockmgr))
    lockmgr := NEW LockManager;

  RETURN lockmgr;
}
