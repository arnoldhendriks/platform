<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::dbase/postgresql.whlib";
LOADLIB "wh::util/algorithms.whlib";

LOADLIB "mod::system/lib/services.whlib";


/* The blob cleanup has two different steps

   - analyze which blobs are not referenced / abandoned uploads, save this in a manifest
   - removing database records for unreferenced records, deleting blob files that
     haven't been in the database for some time
*/

PUBLIC STATIC OBJECTTYPE PostgreSQLBlobAnalyzer
<
  OBJECT trans;

  /** How old a blob file should be before it flagged as abandoned upload when
      no database entry exists for it. Defaults to 4 hours.
  */
  PUBLIC INTEGER abandoned_upload_secs;

  /** How long a blob should be unreferenced before being removed from the
      database table. Defaults to 2 hours.
  */
  PUBLIC INTEGER unreferenced_period_secs;

  /** How long a blob file should be kept after removing its database record.
      0 for immediate removal, defaults to 1 hour.
  */
  PUBLIC INTEGER keep_removed_files_secs;

  /** Filter seen blobs by this ids, for testing purposes
  */
  PUBLIC STRING ARRAY __filterids;

  /// Function ptr to override manifest access. Signature: RECORD FUNCTION func(RECORD newmanifest)
  PUBLIC FUNCTION PTR __debugmanifestaccess;

  /// Function ptr to override getting the current datetime. Signature: DATETIME FUNCTION func()
  PUBLIC FUNCTION PTR __debuggetcurrentdatetime;

  /** Creates a new blob analyzer for a PostgreSQL database
      @param trans PostgreSQL transaction
  */
  MACRO NEW(OBJECT trans)
  {
    this->trans := trans;
    this->abandoned_upload_secs := 4 * 3600; // 4 hour
    this->unreferenced_period_secs := 2 * 3600; // 2 hour
    this->keep_removed_files_secs := 1 * 3600; // 1 hour
  }

  DATETIME FUNCTION GetCurrentDateTime()
  {
    RETURN IsValueSet(this->__debuggetcurrentdatetime) ? this->__debuggetcurrentdatetime() : GetCurrentDatetime();
  }

  /** Lists all blob parts in the tree
      @return All found blob parts
      @cell(string array) return.new_disk_blobs Ids of blob parts newer than new_cutoff
      @cell(string array) return.old_disk_blobs Ids of blob parts older than new_cutoff
  */
  RECORD FUNCTION GetDiskBlobInfo()
  {
    STRING ARRAY new_disk_blobs;
    STRING ARRAY old_disk_blobs;

    DATETIME new_cutoff := AddTimeToDate(-this->abandoned_upload_secs * 1000, this->GetCurrentDateTime());

    BOOLEAN have_filter := IsValueSet(this->__filterids);
    STRING ARRAY filterblobpartids;
    IF (have_filter)
      filterblobpartids := GetSortedSet(this->trans->blobhandler->GetBlobPartIdsFromBlobIds(this->__filterids));

    RECORD ARRAY blobs := this->trans->blobhandler->ListBlobParts([ filterids := filterblobpartids ]);

    FOREVERY (RECORD rec FROM blobs)
    {

      IF (rec.modified < new_cutoff)
      {
        RECORD pos := LowerBound(old_disk_blobs, rec.blobpartid);
        IF (NOT pos.found)
          INSERT rec.blobpartid INTO old_disk_blobs AT END;
      }
      ELSE
      {
        RECORD pos := LowerBound(new_disk_blobs, rec.blobpartid);
        IF (NOT pos.found)
          INSERT rec.blobpartid INTO new_disk_blobs AT END;
      }
    }

    RETURN CELL[ old_disk_blobs, new_disk_blobs ];
  }

  /** List all blobs in the database tables
      @return All blobs in the DB
      @cell(string array) return.existing_blobs List of all blobs in the blob table (full blob ids)
      @cell(string array) return.unreferenced_blobs Active blobs that have no reference (full blob ids)
  */
  RECORD FUNCTION GetDatabaseBlobInfo()
  {
    IF (this->trans->type != "postgresql")
      ABORT("Not a PostgreSQL transaction");

    STRING old_isolationlevel := this->trans->transactionisolationlevel;
    this->trans->transactionisolationlevel := "repeatable read";
    this->trans->BeginWork();

    STRING ARRAY referenced_blobs;
    FOREVERY (RECORD schemarec FROM this->trans->GetSchemaListing())
    {
      IF (schemarec.is_system_schema)
        CONTINUE;

      FOREVERY (RECORD tablerec FROM this->trans->GetTableListing(schemarec.schema_name))
      {
        FOREVERY (RECORD columnrec FROM this->trans->GetColumnListing(schemarec.schema_name, tablerec.table_name))
        {
          IF (columnrec.data_type != "webhare_internal.webhare_blob" OR (schemarec.schema_name = "webhare_internal" AND tablerec.table_name = "blob"))
            CONTINUE;

          // Select via raw query so we won't get itnto problems with missing blobs, and avoid blob lookup
          STRING ARRAY blobids :=
              SELECT AS STRING ARRAY DISTINCT id
                FROM this->trans->__ExecSQL(
                        `SELECT (${PostgreSQLEscapeIdentifier(columnrec.column_name)}).id
                           FROM ${PostgreSQLEscapeIdentifier(schemarec.schema_name)}.${PostgreSQLEscapeIdentifier(tablerec.table_name)}
                          WHERE ${PostgreSQLEscapeIdentifier(columnrec.column_name)} IS NOT NULL`);

          referenced_blobs := ArrayUnion(referenced_blobs, blobids);
        }
      }
    }

    // Get the blobs from the blob table via a direct query, missing blobs might cause throws
    STRING ARRAY existing_blobs :=
        SELECT AS STRING ARRAY id
          FROM this->trans->__ExecSQL(`SELECT (id).id FROM webhare_internal.blob`) ORDER BY id;

    IF (IsValueSet(this->__filterids))
    {
      referenced_blobs := ArrayIntersection(referenced_blobs, this->__filterids);
      existing_blobs := ArrayIntersection(existing_blobs, this->__filterids);
    }

    this->trans->RollbackWork();
    this->trans->transactionisolationlevel := old_isolationlevel;

    STRING ARRAY unreferenced_blobs := ArrayDelete(existing_blobs, referenced_blobs);

    RETURN CELL
        [ existing_blobs
        , referenced_blobs
        , unreferenced_blobs :=     ArrayDelete(existing_blobs, referenced_blobs)
        ];
  }

  MACRO DeleteBlobParts(STRING ARRAY ids)
  {
    this->trans->blobhandler->DeleteBlobParts(ids);
  }

  /** Reads the blob manifest from disk
      @return Blob manifest contents
      @cell return.unreferenced List of unreferenced blobs
      @cell(string) return.unreferenced.date Date when the unreferencedness was detected first
      @cell return.unreferenced.checked Date when the unreferencedness was last confirmed (used because the delete
         stage isn't integrated with the analyze state)
      @cell(string array) return.unreferenced.ids Database ids of the unferenced blobs
      @cell return.unlinked List of unlinked blobs (unreferenced blob whose database entry
         was deleted by blob cleanup)
      @cell(string) return.unlinked.date Date when the database entry was deleted
      @cell(string array) return.unlinked.blobpartids Blob part ids of the unlinked blobs
      @cell(string array) return.abandoned List of blob part ids of abandoned blobs (old blobs with no database entry)
  */
  PUBLIC RECORD FUNCTION ReadBlobManifest()
  {
    RECORD manifest;
    IF (IsValueSet(this->__debugmanifestaccess))
      manifest := this->__debugmanifestaccess(DEFAULT RECORD);
    ELSE
      manifest := DecodeJSONBlob(this->trans->blobhandler->GetBlobManifest());

    RETURN EnforceStructure(
        [ unreferenced :=       [ [ date := "", checked := "", ids := STRING[] ] ]
        , unlinked :=           [ [ date := "", blobpartids := STRING[] ] ]
        , abandoned :=          STRING[]
        ], manifest,
        [ removeunexpected := TRUE ]);
  }

  MACRO StoreBlobManifest(RECORD manifest)
  {
    IF (IsValueSet(this->__debugmanifestaccess))
      this->__debugmanifestaccess(manifest);
    ELSE
      this->trans->blobhandler->StoreBlobManifest(EncodeJSONBlob(manifest));
  }

  /** Analyze the disk and database blobs, update the blob manifest with that info
  */
  PUBLIC MACRO UpdateBlobManifest(RECORD options DEFAULTSTO DEFAULT RECORD)
  {
    options := ValidateOptions([debug := FALSE], options);
    OBJECT lock := OpenLockManager()->LockMutex("system:blobcleanup");
    TRY
    {
      STRING curdate := FormatISO8601DateTime(this->GetCurrentDateTime());

      IF(options.debug)
        Print(`Gathering blob information from database\n`);
      RECORD dbinfo := this->GetDatabaseBlobInfo();

      IF(options.debug)
        Print(`Gathering blob information from disk\n`);
      RECORD diskinfo := this->GetDiskBlobInfo();

      STRING ARRAY expected_blobpartids := GetSortedSet(this->trans->blobhandler->GetBlobPartIdsFromBlobIds(dbinfo.existing_blobs));
      STRING ARRAY unreferenced_blobpartids := GetSortedSet(this->trans->blobhandler->GetBlobPartIdsFromBlobIds(dbinfo.unreferenced_blobs));

      // Remove the expected blobpartids from the unreferenced_blobpartids, for the case that a blob can be composed from multiple storage ids
      unreferenced_blobpartids := ArrayDelete(unreferenced_blobpartids, expected_blobpartids);

      STRING ARRAY all_missing_blob_files := ArrayDelete(expected_blobpartids, diskinfo.old_disk_blobs CONCAT diskinfo.new_disk_blobs);
      IF (IsValueSet(all_missing_blob_files))
      {
        STRING ARRAY referenced_missing_blob_files := ArrayDelete(all_missing_blob_files, unreferenced_blobpartids);
        IF (IsValueSet(referenced_missing_blob_files))
        {
          PrintTo(2, `Could not locate all referenced blobs!\n`);
          DumpValue(CELL[ all_missing_blob_files, referenced_missing_blob_files ]);
        }
        ELSE
        {
          PrintTo(2, `Could not locate some unreferenced blobs\n`);
          DumpValue(Cell[ all_missing_blob_files ]);
        }
      }

      RECORD manifest := this->ReadBlobManifest();

      STRING ARRAY new_unreferenced := dbinfo.unreferenced_blobs;

      // Remove the referenced blobs from the list of previously unreferenced blobs
      // and remove previously unreferenced blobs from our list of new unrefs.
      FOREVERY (RECORD rec FROM manifest.unreferenced)
      {
        STRING ARRAY filtered_ids := ArrayDelete(rec.ids, dbinfo.referenced_blobs);
        manifest.unreferenced[#rec].ids := filtered_ids;
        manifest.unreferenced[#rec].checked := curdate;

        new_unreferenced := ArrayDelete(new_unreferenced, filtered_ids);
      }

      IF (IsValueSet(new_unreferenced))
      {
        INSERT [ date := curdate, checked := curdate, ids := new_unreferenced ]
          INTO manifest.unreferenced AT END;
      }

      // The abandoned uploads are all 'old' diskblobs without a dbentry, of which the
      // db entry wasn't deleted for being unreferenced
      STRING ARRAY abandoned := ArrayDelete(diskinfo.old_disk_blobs, expected_blobpartids);
      FOREVERY (RECORD rec FROM manifest.unlinked)
      {
        // Remove now registered blobs from list of pending deletes
        manifest.unlinked[#rec].blobpartids := ArrayDelete(rec.blobpartids, expected_blobpartids);
        abandoned := ArrayDelete(abandoned, rec.blobpartids);
      }

      manifest.abandoned := abandoned;
      DELETE FROM manifest.unreferenced WHERE IsDefaultValue(ids);
      DELETE FROM manifest.unlinked WHERE IsDefaultValue(blobpartids);

      this->StoreBlobManifest(manifest);
    }
    FINALLY
      lock->Close();
  }

  STRING ARRAY FUNCTION TryDeleteUnreferencedBlobs(STRING ARRAY ids)
  {
    STRING ARRAY deleted;

    STRING ARRAY worklist := ids, nextround;
    FOR (INTEGER blocksize := 64; blocksize >= 1; blocksize := blocksize / 2)
    {
      IF (blocksize >= LENGTH(ids) * 2) // Skip blocksizes much bigger than deletion set
        CONTINUE;

      ids := ShuffleArray(ids);
      nextround := STRING[];

      FOR (INTEGER i := 0; i < LENGTH(ids); i := i + blocksize)
      {
        STRING ARRAY todelete := ArraySlice(ids, i, blocksize);
        TRY
        {
          this->trans->BeginWork();
          STRING ARRAY returned_deleted :=
              SELECT AS STRING ARRAY id
                FROM this->trans->__ExecSQL(
            `DELETE FROM webhare_internal.blob AS x
              WHERE id = Any(SELECT id
                               FROM webhare_internal.blob
                              WHERE (id).id = ANY($1)
                                FOR UPDATE SKIP LOCKED)
          RETURNING (id).id`, [ args := VARIANT[ todelete ]]);
          this->trans->CommitWork();
          deleted := deleted CONCAT returned_deleted;
        }
        CATCH
        {
          IF (this->trans->IsWorkOpen())
            this->trans->RollbackWork();

          nextround := nextround CONCAT todelete;
        }
      }

      ids := nextround;
    }

    RETURN deleted;
  }

  /** Removes all blobs that have been unreferenced dusing checks twice, at least
      'minunrefmins' minutes apart. Also deletes all blobs files unlinked by these removals
      after 'minunlinkedmins'
      @cell(boolean) options.skipdeletes If TRUE, don't do real deletes (from the DB or blob part storage)
      @return Results
      @cell(string array) return.scheduled_abandoned Blob parts resulting from abandoned uploads
      @cell(string array) return.removed_unreferenced Database blob ids that were removed from the blob table
      @cell(string array) return.unlinked Blob parts of the blobs that were removed from the blob table
      @cell(string array) return.removed Blob parts that were removed
  */
  PUBLIC RECORD FUNCTION RemoveUnreferencedBlobs(RECORD options DEFAULTSTO DEFAULT RECORD)
  {
    options := ValidateOptions(
        [ skipdeletes :=    FALSE
        ], options);

    RECORD retval :=
        [ scheduled_abandoned :=  STRING[]
        , removed_unreferenced := STRING[]
        , unlinked :=             STRING[]
        , removed :=              STRING[]
        , removed_old_archived := STRING[]
        ];

    OBJECT lock := OpenLockManager()->LockMutex("system:blobcleanup");
    TRY
    {
      RECORD manifest := this->ReadBlobManifest();
      DATETIME now := this->GetCurrentDateTime();

      STRING curdate := FormatISO8601DateTime(now);

      // Move abandoned blobs to the unlinked list, so they will be deleted after a grace period
      IF (IsValueSet(manifest.abandoned))
      {
        IF (IsDefaultValue(manifest.unlinked) OR manifest.unlinked[END-1].date != curdate)
          INSERT [ date := curdate, blobpartids := STRING[] ] INTO manifest.unlinked AT END;

        retval.scheduled_abandoned := manifest.abandoned;

        manifest.unlinked[END-1].blobpartids := ArrayUnion(manifest.unlinked[END-1].blobpartids, manifest.abandoned);
        manifest.abandoned := STRING[];

        this->StoreBlobManifest(manifest);
      }

      WHILE (IsValueSet(manifest.unreferenced))
      {
        RECORD rec := manifest.unreferenced[0];
        RECORD diff := GetDateTimeDifference(MakeDateFromText(rec.date), MakeDateFromText(rec.checked));
        IF (diff.days * 24 * 3600 * 60 + diff.msecs / 1000 < this->unreferenced_period_secs)
          BREAK;

        // Try to delete unreferenced blobs. Any blobs not deleted (and still unreferenced)
        // will be deleted in the following round
        STRING ARRAY deleted;
        IF (NOT options.skipdeletes)
          deleted := this->TryDeleteUnreferencedBlobs(rec.ids);
        ELSE
          deleted := rec.ids;
        DELETE FROM manifest.unreferenced AT 0;

        retval.removed_unreferenced := retval.removed_unreferenced CONCAT deleted;

        /// This is dangerous when we start using composed blobs, the parts might still be referenced by other blobs
        STRING ARRAY deleted_blobpartids := this->trans->blobhandler->GetBlobPartIdsFromBlobIds(deleted);
        retval.unlinked := retval.unlinked CONCAT deleted_blobpartids;

        IF (IsDefaultValue(manifest.unlinked) OR manifest.unlinked[END-1].date != curdate)
          INSERT [ date := curdate, blobpartids := STRING[] ] INTO manifest.unlinked AT END;

        manifest.unlinked[END-1].blobpartids := ArrayUnion(manifest.unlinked[END-1].blobpartids, deleted_blobpartids);
        this->StoreBlobManifest(manifest);
      }

      DATETIME deletecutoff := AddTimeToDate(-this->keep_removed_files_secs * 1000, now);
      WHILE (IsValueSet(manifest.unlinked))
      {
        RECORD rec := manifest.unlinked[0];
        IF (MakeDateFromText(rec.date) >= deletecutoff)
          BREAK;

        retval.removed := retval.removed CONCAT rec.blobpartids;

        IF (NOT options.skipdeletes)
          this->DeleteBlobParts(rec.blobpartids);
        DELETE FROM manifest.unlinked AT 0;
        this->StoreBlobManifest(manifest);
      }

      RECORD removeoldoptions := [ dryrun := options.skipdeletes ];
      IF (IsValueSet(this->__filterids))
        INSERT CELL filterids := GetSortedSet(this->trans->blobhandler->GetBlobPartIdsFromBlobIds(this->__filterids)) INTO removeoldoptions;
      retval.removed_old_archived := this->trans->blobhandler->RemoveOldArchivedBlobParts(removeoldoptions);
    }
    FINALLY
      lock->Close();

    // Sort stuff
    retval.scheduled_abandoned := GetSortedSet(retval.scheduled_abandoned);
    retval.removed_unreferenced := GetSortedSet(retval.removed_unreferenced);
    retval.unlinked := GetSortedSet(retval.unlinked);
    retval.removed := GetSortedSet(retval.removed);
    retval.removed_old_archived := GetSortedSet(retval.removed_old_archived);

    RETURN retval;
  }
>;
