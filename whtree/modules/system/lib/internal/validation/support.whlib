<?wh

LOADLIB "wh::devsupport.whlib";

LOADLIB "mod::system/lib/resources.whlib";
LOADLIB "mod::system/lib/internal/resources.whlib";
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";
LOADLIB "mod::system/lib/internal/validation/witty.whlib";
LOADLIB "mod::system/lib/internal/modules/althooks.whlib";
LOADLIB "mod::system/lib/internal/modules/version.whlib";

LOADLIB "mod::tollium/lib/internal/icons.whlib";

STRING FUNCTION GetNodeType(OBJECT node)
{
  IF(node->namespaceuri = whconstant_xmlns_screens AND node->localname IN ["tabsextension","screen","fragment"])
    RETURN node->localname;

  RETURN "";
}


/** Base objecttype for resource validators
*/
PUBLIC STATIC OBJECTTYPE XMLValidatorBase
< /// Last described library resource
  STRING lastlib;

  /// Last described library description @includecelldef DescribeCompiledLibrary.return
  RECORD lastlibdescr;

  /// Whether only tids should be processed
  PUBLIC BOOLEAN onlytids;

  /// @type(object %XMLDocument) XML document
  PUBLIC OBJECT xml;

  /// Name of the module of the current document (if applicable)
  PUBLIC STRING modulename;

  /// Resource name of the current document
  PUBLIC STRING respath;

  //TODO merge hint,warning,errors into one big list? but we'll need to remove the public errors/warnings/hints then
  /** List of errors
      @cell resourcename Resource name
      @cell line Line number
      @cell col Column number
      @cell message Message
  */
  PUBLIC RECORD ARRAY errors;

  /** List of warnings @includecelldef #errors */
  PUBLIC RECORD ARRAY warnings;

  /** List of warnings @includecelldef #hints */
  PUBLIC RECORD ARRAY hints;

  /** List of icons
      @cell resourcename Resource name
      @cell line Line number
      @cell col Column number
      @cell icon Icon reference
  */
  PUBLIC RECORD ARRAY icons;

  /** Create a new XML validatior base object
      @param(object %XMLDocument) xmldoc XML document to validate
      @param respath Resource path of the document
  */
  MACRO NEW(OBJECT xmldoc, STRING respath)
  {
    this->modulename := GetModuleNameFromResourcePath(respath);
    this->respath := respath;
    this->xml := xmldoc;
  }

  /** Add a hint. informational message that will never turn into an error, but usually adds details to warnings and errors
      @param node Node that caused the hint
      @param hint Hint message
  */
  PUBLIC MACRO AddHint(OBJECT node, STRING hint)
  {
    this->AddHintForLine(ObjectExists(node) ? node->linenum : 0, hint);
  }

  /** Add a hint. informational message that will never turn into an error, but usually adds details to warnings and errors
      @param linenum Line in the document relevant to the hint
      @param hint Hint message
  */
  PUBLIC MACRO AddHintForLine(INTEGER linenum, STRING hint)
  {
    INSERT [ resourcename :=  this->respath
           , line :=      linenum
           , col :=       0
           , message :=   hint
           ] INTO this->hints AT END;
  }

  /** Adds a list of hints
      @param hints List of hints
      @cell hints.resourcename Resource name
      @cell hints.line Line number
      @cell hints.col Column number
      @cell hints.message Hint message
  */
  PUBLIC MACRO AddHints(RECORD ARRAY hints)
  {
    this->hints := this->hints CONCAT hints;
  }

  /** Add a warning. Depending on validation settings for a module these may turn into errors
      @param node Node that cause the warning
      @param warning Warning message
      @param options @includecelldef #AddWarningForLine.options
  */
  PUBLIC MACRO AddWarning(OBJECT node, STRING warning, RECORD options DEFAULTSTO DEFAULT RECORD)
  {
    this->AddWarningForLine(ObjectExists(node) ? node->linenum : 0, warning, options);
  }

  /** Add a warning. Depending on validation settings for a module these may turn into errors
      @param linenum Line in the document relevant to the warning
      @param warning Warning message
  */
  PUBLIC MACRO AddWarningForLine(INTEGER linenum, STRING warning, RECORD options DEFAULTSTO DEFAULT RECORD)
  {
    options := ValidateOptions( CELL[], options);
    INSERT [ resourcename :=  this->respath
           , line :=      linenum
           , col :=       0
           , message :=   warning
           ] INTO this->warnings AT END;
  }

  /** Adds a list of warnings
      @param warnings List of warnings
      @cell warnings.resourcename Resource name
      @cell warnings.line Line number
      @cell warnings.col Column number
      @cell warnings.message Warning message
  */
  PUBLIC MACRO AddWarnings(RECORD ARRAY warnings)
  {
    this->warnings := this->warnings CONCAT warnings;
  }

  /** Add an error.
      @param node Node that cause the error
      @param error Error message
  */
  PUBLIC MACRO AddError(OBJECT node, STRING error)
  {
    this->AddErrorForLine(ObjectExists(node) ? node->linenum : 0, error);
  }

  /** Add an error.
      @param linenum Line in the document relevant to the error
      @param error Error message
  */
  PUBLIC MACRO AddErrorForLine(INTEGER linenum, STRING error)
  {
    INSERT [ resourcename :=  this->respath
           , line :=      linenum
           , col :=       0
           , message :=   error
           ] INTO this->errors AT END;
  }

  /** Add an error using scope information
      @param linenum Line in the document relevant to the error
      @param error Error message
  */
  PUBLIC MACRO AddErrorForScope(RECORD scope, STRING error)
  {
    scope := CELL[ resourcename := this->respath
                 , col := 0
                 , line := 0  //defaults
                 , ...scope
                 ];

    INSERT CELL[ scope.resourcename
               , scope.line
               , scope.col
               , message :=   error
               ] INTO this->errors AT END;
  }

  /** Adds a list of errors
      @param errors List of errors
      @cell errors.resourcename Resource name
      @cell errors.line Line number
      @cell errors.col Column number
      @cell errors.message Error message
  */
  PUBLIC MACRO AddErrors(RECORD ARRAY errors)
  {
    this->errors := this->errors CONCAT errors;
  }

  /** Returns the module name of the current document
      @return Module name
  */
  PUBLIC STRING FUNCTION GetModuleName()
  {
    RETURN GetModuleNameFromResourcePath(this->respath);
  }

  /** Are we working for a builtin module? (higher compliance requirements)
      @return Whether the current document is from a builtin module
  */
  PUBLIC BOOLEAN FUNCTION IsBuiltinModule()
  {
    RETURN this->GetModuleName() IN whconstant_builtinmodules;
  }
  /** @param node Node to get the path from
      @param attrname Attribute to read
      @param legacybasepath Alternative lookup path (often `MakeAbsoluteResourcePath(this->respath,"scripts/")`)
      @cell(boolean) options.warnversion Warn after this version if the file was found in the legacy path. We recommend
           choosing at least 3 minor versions above the current version. Set to 0 to disable warnings entirely, even for builtin modules (backports)
      @cell(boolean) options.allowwittytags Allow witty tags in the path (disables verification if they are found)
      @return Absolute path
  */
  PUBLIC STRING FUNCTION GetVerifyPath(OBJECT node, STRING attrname, STRING legacybasepath, RECORD options DEFAULTSTO DEFAULT RECORD)
  {
    options := ValidateOptions([ warnversion := 0
                               , allowwittytags := FALSE
                               ], options);

    STRING path := node->GetAttribute(attrname);
    IF(path = "")
      RETURN "";

    STRING directpath := MakeAbsoluteResourcePath(this->respath, path);
    IF(options.allowwittytags AND path LIKE '*[*')
      RETURN directpath; //no further checking possible

    IF(RecordExists(RetrieveWebHareResource(directpath, [ allowmissing := TRUE ])))
      RETURN directpath;

    //Direct path not failed.
    IF(legacybasepath != "")
    {
      STRING legacypath := MakeAbsoluteResourcePath(legacybasepath, path);
      IF(RecordExists(RetrieveWebHareResource(legacypath, [ allowmissing := TRUE ])))
      {
        STRING realpath := Substring(legacypath, Length(MakeAbsoluteResourcePath(this->respath,"./")));
        STRING message := `Attribute '${attrname}' required the use of an implicit path to find, please specify it as '${realpath}'`;
        IF(options.warnversion > 0 AND (this->IsBuiltinModule() OR GetWebhareVersionNumber() >= options.warnversion)) //builtin modules need to set a good example, so ignore the version check
          this->AddWarning(node, message);
        ELSE
          this->AddHint(node, message);

        RETURN legacypath;
      }
    }
    this->AddError(node, `Unable to find '${path}'`);
    RETURN "";
  }

  /** Describe a library
      @param libname Library resource name
      @return Library description @includecelldef #DescribeCompiledLibrary.return
  */
  RECORD FUNCTION GetLibDescription(STRING libname)
  {
    IF(this->lastlib != libname)
    {
      this->lastlib := libname;
      this->lastlibdescr := HookableDescribeCompiledLibrary(libname);

      IF(NOT RecordExists(this->lastlibdescr))
      {
        BOOLEAN exists := RecordExists(RetrieveWebHareResource(libname, [ allowmissing := TRUE ]));
        IF(exists)
        {
          RECORD fileinfo := GetHarescriptLibraryInfo(libname);
          IF(Length(fileinfo.errors) > 0)
            this->lastlibdescr := CELL[ error := `At ${fileinfo.errors[0].filename}:${fileinfo.errors[0].line}:${fileinfo.errors[0].col}: ${fileinfo.errors[0].message}` ];
          ELSE
            this->lastlibdescr := CELL[ error := `Cannot compile '${libname}'` ];
        }
        ELSE
        {
          this->lastlibdescr := CELL[ error := `No such library '${libname}'` ];
        }
      }
    }
    RETURN this->lastlibdescr;
  }

  /** Verifies the existence of a reference to a witty component
      @param linenum Line number of the reference
      @param wittyref Reference to the witty component
      @return TRUE if the witty component exists
  */
  BOOLEAN FUNCTION VerifyWittyComponentExistence(INTEGER linenum, STRING wittyref)
  {
    STRING message := VerifyWittyComponentExistence(this->respath, wittyref);
    IF(message != "")
    {
      this->AddErrorForLine(linenum, message);
      RETURN FALSE;
    }
    RETURN TRUE;
  }

  /** Verifies the existence of a reference to a function
      @param linenum Line number of the reference
      @param objtype Reference to the objecttype
      @return Object description if it exists
  */
  RECORD FUNCTION VerifyObjectExistence(INTEGER linenum, STRING objtype)
  {
    STRING ARRAY objtoks := Tokenize(objtype,'#');
    RECORD lib := this->GetLibDescription(objtoks[0]);
    IF(CellExists(lib,'error'))
    {
      this->AddErrorForLine(linenum, `Cannot find '${objtoks[1]}': ${lib.error}`);
      RETURN DEFAULT RECORD;
    }

    RECORD match := SELECT * FROM lib.objecttypes WHERE name = ToUppercase(objtoks[1]);
    IF(NOT RecordExists(match))
    {
      this->AddErrorForLine(linenum, `Object '${objtoks[1]}' not found in '${objtoks[0]}'`);
      RETURN DEFAULT RECORD;
    }

    RETURN match;
  }

  /** Verify the existence of a reference to a screens file thingy
      @param linenum Line number of the reference
      @param screenref Reference to the screen
      @param type Screen type (eg 'tabsextension')
      @return TRUE if the screen exists
  */
  OBJECT FUNCTION VerifyScreenReferenceExistence(INTEGER linenum, STRING screenref, STRING findtype)
  {
    STRING absscreenref := MakeAbsoluteScreenReference(this->respath, screenref);
    IF(absscreenref = "")
      RETURN DEFAULT OBJECT; //we are not a required check..

    STRING ARRAY reftoks := Tokenize(absscreenref,'#');
    IF(Length(reftoks) > 2)
    {
      this->AddErrorForLine(linenum, `Invalid reference for '${findtype}' target '${screenref}': expecting <resource>[#<name>]`);
      RETURN DEFAULT OBJECT;
    }

    TRY
    {
      RECORD target := RetrieveCachedXMLResource(reftoks[0]);

      //Look for things under the documentnode
      FOREVERY(OBJECT potentialtarget FROM target.doc->documentelement->ListChildren("*","*"))
      {
        STRING targettype := GetNodeType(potentialtarget);
        IF(Length(reftoks) = 1)
        {
          IF(targettype = findtype) //first hit is okay if we're not searching by name
            RETURN potentialtarget;

          CONTINUE;
        }

        IF(potentialtarget->GetAttribute("name") = reftoks[1])
        {
          IF(targettype = findtype)
            RETURN potentialtarget; //Found it!

          this->AddErrorForLine(linenum, `Invalid reference for '${findtype}' target '${screenref}': '${reftoks[1]}' is a '${targettype}', not a '${findtype}'`);
          RETURN DEFAULT OBJECT;
        }
      }
    }
    CATCH(OBJECT<RetrieveResourceException> e)
    {
      this->AddErrorForLine(linenum, `Can't find '${findtype}' target '${screenref}': ${e->what}`);
      RETURN DEFAULT OBJECT;
    }
    this->AddErrorForLine(linenum, `Can't find '${findtype}' target '${reftoks[1]}' in ${reftoks[0]}`);
    RETURN DEFAULT OBJECT;
  }

  /** Verifies the existence of a reference to a function
      @param node Node for the reference
      @param func Reference to the function
      @return Function definition if it exists
  */
  RECORD FUNCTION VerifyFunctionExistence(OBJECT node, STRING func)
  {
    STRING ARRAY functoks := Tokenize(func,'#');
    IF(Length(functoks) != 2)
    {
      this->AddError(node, `Invalid function reference '${func}')`);
      RETURN DEFAULT RECORD;
    }
    RECORD lib := this->GetLibDescription(functoks[0]);
    IF(CellExists(lib,'error'))
    {
      this->AddError(node, `Cannot find '${functoks[1]}': ${lib.error}`);
      RETURN DEFAULT RECORD;
    }

    RECORD match := SELECT * FROM lib.functions WHERE lib.casesensitive ? name = functoks[1] : name = ToUppercase(functoks[1]);
    IF(NOT RecordExists(match))
    {
      this->AddError(node, `Function '${functoks[1]}' not found in '${functoks[0]}'`);
      RETURN DEFAULT RECORD;
    }

    RETURN match;
  }

  /** Verifies the existence of a reference to a function
      @param tolliumscope Scope for the reference
      @param func Reference to the function
      @return Function definition if it exists
  */
  RECORD FUNCTION VerifyFunctionExistenceForScope(RECORD tolliumscope, STRING func)
  {
    STRING ARRAY functoks := Tokenize(func,'#');
    IF(Length(functoks) != 2)
    {
      this->AddErrorForScope(tolliumscope, `Invalid function reference '${func}')`);
      RETURN DEFAULT RECORD;
    }
    RECORD lib := this->GetLibDescription(functoks[0]);
    IF(CellExists(lib,'error'))
    {
      this->AddErrorForScope(tolliumscope, `${lib.error} (looking for '${functoks[1]}')`);
      RETURN DEFAULT RECORD;
    }

    RECORD match := SELECT * FROM lib.functions WHERE lib.casesensitive ? name = functoks[1] : name = ToUppercase(functoks[1]);
    IF(NOT RecordExists(match))
    {
      this->AddErrorForScope(tolliumscope, `Function '${functoks[1]}' not found in '${functoks[0]}'`);
      RETURN DEFAULT RECORD;
    }

    RETURN match;
  }

  RECORD FUNCTION FindObjectMember(STRING objtype, STRING membername)
  {
    WHILE(objtype != "")
    {
      STRING ARRAY objtoks := Tokenize(objtype,'#');
      RECORD lib := this->GetLibDescription(objtoks[0]);
      IF(NOT RecordExists(lib))
        RETURN DEFAULT RECORD;

      RECORD matchobj := SELECT * FROM lib.objecttypes WHERE name = ToUppercase(objtoks[1]);
      IF(NOT RecordExists(matchobj))
        RETURN DEFAULT RECORD;

      RECORD match := SELECT * FROM matchobj.members WHERE ToUppercase(name) = ToUppercase(membername);
      IF(RecordExists(match))
        RETURN match;

      //chase the extend. if it starts with '#', stay in this lib
      objtype := matchobj.objectextend LIKE "#*" ? objtoks[0] || matchobj.objectextend : matchobj.objectextend;
    }
    RETURN DEFAULT RECORD;
  }

  PUBLIC MACRO ValidateIcon(RECORD scope, STRING iconname, STRING colors, RECORD options DEFAULTSTO DEFAULT RECORD)
  {
    options := ValidateOptions(CELL[], options);
    IF(iconname = "")
      RETURN;

    FOREVERY(STRING icon FROM Tokenize(iconname,'+'))
      IF(NOT RecordExists(GetImage(icon, 24, 24, "", colors, [ nobroken := TRUE ])))
        this->AddErrorForScope(scope, `No such icon '${icon}'`);
  }

  PUBLIC MACRO ValidateLocalHandler(RECORD handlerrec, STRING attributename, RECORD tolliumscope, STRING objtype)
  {
    IF(handlerrec.name LIKE "*!*" OR ToUppercase(handlerrec.name) IN ["CANCEL","SUBMIT","@NOTIMPLEMENTED"])
      RETURN; //no support for scoped names (yet?)

    RECORD match := this->FindObjectMember(objtype, handlerrec.name);
    IF(NOT RecordExists(match) OR NOT match.ismethod)
    {
      this->AddErrorForScope(tolliumscope, `No such method '${handlerrec.name}' in objecttype ${objtype}`);
      RETURN;
    }

    IF(NOT __ValidateFunctionPtrSignature([ excessargstype := 0
                                          , returntype := match.resulttype
                                          , parameters := SELECT source := (#parameters + 1) * (hasdefault ? -1 : 1)
                                                               , type
                                                            FROM match.parameters
                                          ], handlerrec.req_retval, handlerrec.req_args))
    {
      this->AddErrorForScope(tolliumscope,
              `The '${attributename}' handler points to function '${handlerrec.name}'`
              ||" which has the wrong function signature. Expected " ||
              ExplainFunctionSignature(handlerrec.req_retval, handlerrec.req_args) || ", got " || ExplainFunctionSignature(match.resulttype, SELECT AS INTEGER ARRAY type FROM match.parameters));
    }
  }
  PUBLIC MACRO ValidateGlobalHandler(RECORD handlerrec, STRING attributename, RECORD tolliumscope)
  {
    RECORD match := this->VerifyFunctionExistenceForScope(tolliumscope, handlerrec.name);
    IF(NOT RecordExists(match))
      RETURN; //already handled

    INTEGER ARRAY expectargs := [TYPEID(OBJECT)] CONCAT handlerrec.req_args;
    IF(NOT __ValidateFunctionPtrSignature([ excessargstype := 0
                                          , returntype := match.resulttype
                                          , parameters := SELECT source := (#parameters + 1) * (hasdefault ? -1 : 1)
                                                               , type
                                                            FROM match.parameters
                                          ], handlerrec.req_retval, expectargs))
    {
      this->AddErrorForScope(tolliumscope,
              `The '${attributename}' handler points to function '${handlerrec.name}'`
              ||" which has the wrong function signature. Expected " ||
              ExplainFunctionSignature(handlerrec.req_retval, expectargs) || ", got " || ExplainFunctionSignature(match.resulttype, SELECT AS INTEGER ARRAY type FROM match.parameters));
    }
  }

  /** Hook for additional resource validation for XML documents */
  PUBLIC MACRO ValidateDocument()
  {
  }
>;
