<?wh

LOADLIB "wh::adhoccache.whlib";
LOADLIB "wh::witty.whlib";

LOADLIB "mod::system/lib/resources.whlib";
LOADLIB "mod::tollium/lib/internal/gettid.whlib";

RECORD FUNCTION GetCacheableComponentsList(STRING resourceref)
{
  OBJECT templ  := LoadWittyLibrary(resourceref,"TEXT");

  RETURN [ ttl := 15 * 1000 //15 seconds is long enough to prevent a lot of reloads in a typical validation run
         , eventmasks := GetResourceEventMasks([resourceref])
         , value := (SELECT AS STRING ARRAY data FROM templ->__GetTidsRawData() WHERE type = "component")
         ];
}

PUBLIC STRING FUNCTION VerifyWittyComponentExistence(STRING basepath, STRING componentref)
{
  INTEGER comprefstart := SearchLastSubstring(componentref, ':');
  IF(comprefstart = -1) //no ':' in a witty component ref? :/
    RETURN `Invalid Witty component reference '${componentref}'`;

  STRING resource := Left(componentref, comprefstart);
  STRING component := Substring(componentref, comprefstart+1);

  resource := MakeAbsoluteResourcePath(basepath, resource);
  TRY
  {
    STRING ARRAY components := GetAdhocCached(CELL[resource], PTR GetCacheableComponentsList(resource)); //this also tests resource existence
    IF(ToUppercase(component) NOT IN components)
      RETURN `No such component '${component}' in '${resource}'`;
    RETURN "";
  }
  CATCH (OBJECT< WittyParseException > e)
  {
    RETURN e->what; //contains enough context
  }
}


PUBLIC RECORD FUNCTION RunWittyValidator(BLOB doc_blob, STRING resourcename, RECORD options)
{
  OBJECT templ;
  TRY
  {
    templ := NEW WittyTemplate("TEXT");
    templ->LoadBlob(doc_blob, "test.witty");
  }
  CATCH (OBJECT< WittyParseException > e)
  {
    RETURN [ errors := SELECT resourcename := VAR resourcename
                            , message :=  text
                            , line
                            , col
                       FROM e->errors
           ];
  }

  STRING modulename := GetModuleNameFromResourcePath(resourcename);
  IF (modulename="")
    THROW NEW Exception(`Cannot determine module for ${resourcename}`);

  RECORD ARRAY tids, errors, referredcomponents;
  STRING ARRAY internalcomponents;

  FOREVERY (RECORD part FROM templ->__GetTidsRawData())
  {
    SWITCH(part.type)
    {
      CASE "tid","htmltid"
      {
        INSERT CELL[ resourcename
                   , part.line
                   , col := part.col + LENGTH(part.type) + 3 + 1 // add 'get' and a space
                   , tid := IsAbsoluteTid(part.data) ? part.data : modulename || ":" || part.data
                   ] INTO tids AT END;
      }
      CASE "content"
      {
        STRING data := Substitute(part.data, `'`, `"`);
        INTEGER pos := 0;
        WHILE (TRUE)
        {
          INTEGER idx := SearchSubstring(data, `data-texttid=`, pos);
          IF (idx = -1)
            BREAK;

          pos := idx + 13;
          STRING res := SubString(data, pos) || `"`;
          STRING attr := TrimWhitespace(res);
          IF (attr NOT LIKE `"*`)
            CONTINUE;

          // compensate for eaten whitespace
          idx := idx + LENGTH(res) - LENGTH(attr);
          attr := Tokenize(attr, `"`)[1];

          STRING ARRAY lines := Tokenize(Substitute(Substitute(Left(data, idx), "\r\n", "\n"), "\r", ""), "\n");
          INSERT CELL[ resourcename
                     , line :=        part.line + LENGTH(lines) - 1
                     , col :=         (LENGTH(lines) = 1 ? part.col : 1) + LENGTH(lines[END - 1])
                     , tid :=         attr
                     ] INTO tids AT END;
          pos := idx + LENGTH(attr) + 2;
        }
      }
      CASE "component"
      {
        IF(options.onlytids)
          CONTINUE;

        INSERT ToUppercase(part.data) INTO internalcomponents AT END;
      }
      CASE "embed"
      {
        IF(options.onlytids)
          CONTINUE;

        INSERT part INTO referredcomponents AT END;
      }
    }
  }

  FOREVERY(RECORD embed FROM referredcomponents)
  {
    IF(embed.data NOT LIKE "*:*")
    {
      IF(ToUppercase(embed.data) NOT IN internalcomponents)
        INSERT CELL [ resourcename, embed.line, embed.col, message := `No such component '${embed.data}'` ] INTO errors AT END;
    }
    ELSE
    {
      STRING message := VerifyWittyComponentExistence(resourcename, embed.data);
      IF(message != "")
        INSERT CELL [ resourcename, embed.line, embed.col, message ] INTO errors AT END;
    }
  }

  RETURN CELL[tids, errors];
}
