<?wh

LOADLIB "wh::adhoccache.whlib";

LOADLIB "mod::system/lib/logging.whlib";
LOADLIB "mod::system/lib/resources.whlib";
LOADLIB "mod::system/lib/internal/tasks.whlib";

LOADLIB "mod::wrd/lib/api.whlib";


/** Returns all task types and taskclusters, for adhoccache
    @return Cacheable task & taskcluster data
    @cell(string array) return.eventmasks Masks for events that invalidate this data
    @cell(record) return.value All managed/ephemeral tasktypes and taskclusters
    @cell(string array) return.value.eventmasks Masks for events that invalidate this data
    @cell return.value.tasktypes @includecelldef #ParseTaskNode.return
    @cell(record array) return.value.clusters List of taskclusters
    @cell(string) return.value.clusters.tag Tag of the taskcluster
    @cell(integer) return.value.clusters.harescriptworkers Number of harescript workers
    @cell(integer) return.value.clusters.javascriptworkers Number of javascript workers
*/
RECORD FUNCTION GetCacheableAllTaskTypeInfo()
{
  RECORD ARRAY tasktypes;
  STRING ARRAY resources;

  // Default cluster
  RECORD ARRAY clusters  :=
      [ [ tag :=                    "system:default"
        , harescriptworkers :=      1
        , harescriptworkerthreads := 1
        , javascriptworkers :=      0 //DISABLED - no longer supporting runepehmeraltask.es and we may never have managed TS workers...
        , profile := FALSE
        ]
      ];

  FOREVERY (STRING modulename FROM GetInstalledModuleNames())
  {
    STRING resname := GetModuleDefinitionXMLResourceName(modulename);
    INSERT resname INTO resources AT END;
    TRY
    {
      OBJECT xmldoc := GetModuleDefinitionXML(modulename);

      RECORD parsed := ParseModuleTasks(modulename, resname, xmldoc);
      clusters := clusters CONCAT parsed.clusters;
      tasktypes := tasktypes CONCAT parsed.tasktypes;
    }
    CATCH(OBJECT e)
    {
      LogHarescriptException(e);
      CONTINUE; //Don't shut down the whole task system just because one XML file broke
    }
  }

  // Get overrides from system:config schema
  OBJECT configschema := OpenWRDSchema("system:config");
  RECORD ARRAY overrides := configschema ->^taskcluster->RunQuery(
      [ outputcolumns := CELL[ "CLUSTER", "HARESCRIPTWORKERS" ]
      , cachettl := 15 * 60 * 1000
      ]);
  RECORD ARRAY typeoverrides := configschema->^tasktype->RunQuery(
      [ outputcolumns := CELL[ "TYPE", "PROFILE" ]
      , cachettl := 15 * 60 * 1000
      ]);

  // Fix task types to remove clusters that aren't available anymore (or set the default when not provided)
  STRING ARRAY activeclusters := [ "system:default", ...SELECT AS STRING ARRAY tag FROM clusters ];
  UPDATE tasktypes
         SET cluster := "system:default"
       WHERE cluster NOT IN activeclusters;

  tasktypes := SELECT TEMPORARY override := RECORD(SELECT * FROM typeoverrides WHERE typeoverrides.type = tasktypes.tasktype)
                    , *
                    , profile := RecordExists(override) AND override.profile
                 FROM tasktypes
             ORDER BY tasktype, ephemeral;

  clusters := SELECT TEMPORARY override := RECORD(SELECT * FROM overrides WHERE overrides.cluster = clusters.tag)
                   , *
                     //take override, but only if we already desired HS workers
                   , harescriptworkers := harescriptworkers > 0 ? (RecordExists(override) ? override.harescriptworkers : 0) ?? harescriptworkers : 0
                FROM clusters
            ORDER BY tag;

  STRING ARRAY eventmasks := [ "system:modulesupdate", ...GetResourceEventMasks(resources), ...configschema->^taskcluster->GetEventMasks(), ...configschema->^tasktype->GetEventMasks() ];
  RETURN
      [ value :=      CELL[ tasktypes, clusters, eventmasks ]
      , eventmasks := eventmasks
      ];
}

/** Get all tasktype and taskclusters
    @return @includecelldef #GetCacheableAllTaskTypeInfo.return.value
*/
PUBLIC RECORD FUNCTION GetAllTaskTypeInfo()
{
  RETURN GetAdhocCached(CELL[ "tasktypes" ], PTR GetCacheableAllTaskTypeInfo());
}
