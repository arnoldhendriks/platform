#!/bin/bash
source $WEBHARE_DIR/lib/wh-functions.sh

BLOBIMPORTMODE="hardlink"

while true; do
  if [ "$1" == "--copy" ]; then
    BLOBIMPORTMODE="copy"
    shift
  elif [ "$1" == "--softlink" ]; then
    BLOBIMPORTMODE="softlink"
    shift
  elif [ "$1" == "--hardlink" ]; then
    BLOBIMPORTMODE="hardlink"
    shift
  elif [ "$1" == "--restoreto" ]; then
    shift
    RESTORETO="$1"
    shift
  elif [[ $1 =~ ^- ]]; then
    echo "Illegal option '$1'"
    exit 1
  else
    break
  fi
done

if [ -z "$1" ]; then
  echo "Syntax: wh restore [ --hardlink | --softlink | --copy ] [ --restoreto newdbasedir ] <srcdir>"
  echo "  --hardlink:     hardlink blobs. this is the default"
  echo "  --softlink:     softlink blobs"
  echo "  --copy:         copy blobs"
  exit 1
fi

TORESTORE="$1"

if [ "$(uname)" == "Darwin" ]; then
  RSYNCOPTS="--progress"
else
  RSYNCOPTS="--info=progress2"
fi


if [ ! -d "$TORESTORE" ] ; then
  echo "$TORESTORE is not a directory"
  exit 1
fi

if [ -f "$TORESTORE/backup/base.tar.gz" ] ; then
  RESTORE_DB=postgresql
  if [ -z "$RESTORETO" ]; then
    RESTORETO="$WEBHARE_DATAROOT/postgresql"
  fi
else
  echo "Cannot find $TORESTORE/backup/base.tar.gz"
  exit 1
fi



if [ ! -d "$TORESTORE/blob" ]; then
  echo "$TORESTORE/blob does not exist"
  exit 1
fi

if [ -d "$RESTORETO" ]; then
  echo "$RESTORETO already exists - did you mean to specify a different WEBHARE_DATAROOT or --restoreto for the restore?"
  exit 1
fi

if [ "$RESTORE_DB" == "postgresql" ]; then
  # Remove previous restore
  rm -rf "$WEBHARE_DATAROOT/postgresql.restore/"

  mkdir -p "$WEBHARE_DATAROOT/postgresql.restore/db/pg_wal/"
  chmod -R 700 "$WEBHARE_DATAROOT/postgresql.restore/db/"

  PV="cat"
  which pv >/dev/null 2>&1 && PV="pv"

  if ! $PV "$TORESTORE/backup/base.tar.gz" | (umask 0077 && tar zx --no-same-permissions -C "$WEBHARE_DATAROOT/postgresql.restore/db/"); then
    echo Extracting base database failed
    exit 1
  fi
  if ! $PV "$TORESTORE/backup/pg_wal.tar.gz" | (umask 0077 && tar zx --no-same-permissions -C "$WEBHARE_DATAROOT/postgresql.restore/db/pg_wal/"); then
    echo Extracting WAL segments failed
    exit 1
  fi

  if [ "$BLOBIMPORTMODE" == "softlink" ]; then
    # Note: can't use cp -rs on OSX, -s is not supported there
    for P in $( cd "$TORESTORE/blob/" ; echo */ ) ; do
      # Note $P will end with a '/'
      mkdir -p "$WEBHARE_DATAROOT/postgresql.restore/blob/$P"
      if ! ln -s "$TORESTORE/blob/$P"* "$WEBHARE_DATAROOT/postgresql.restore/blob/$P" ; then
        echo Softlinking blobs failed
        exit 1
      fi
    done
  else
    LINKARG=()
    if [ "$BLOBIMPORTMODE" == "hardlink" ]; then
      LINKARG+=(--link-dest=$TORESTORE/)
    fi

    if ! rsync -a $RSYNCOPTS "${LINKARG[@]}" "$TORESTORE/blob" "$WEBHARE_DATAROOT/postgresql.restore/"; then
      echo Extracting blobs failed
      exit 1
    fi
  fi

  mkdir -p "$RESTORETO" 2>/dev/null

  if [ -n "$WEBHARE_IN_DOCKER" ]; then
    chown postgres:root "$RESTORETO/"
    chown -R postgres:root "$WEBHARE_DATAROOT/postgresql.restore/"
  fi

  mv "$WEBHARE_DATAROOT/postgresql.restore/"* "$RESTORETO/"
  rmdir "$WEBHARE_DATAROOT/postgresql.restore"
fi

echo ""
echo "Restore complete!"
