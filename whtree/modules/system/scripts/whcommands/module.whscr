<?wh

LOADLIB "wh::files.whlib";
LOADLIB "wh::os.whlib";
LOADLIB "wh::internet/urls.whlib";
LOADLIB "wh::internet/webbrowser.whlib";
LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/internal/moduleimexport.whlib";


MACRO PrintSyntaxAndTerminate()
{
  Print("Syntax: wh module <command>\n");
  Print("  get <giturl>:            Get a module from a git server\n"); //note - actually implemented in our wrapper module.sh
  Print("  install <path|url>:      Install a module\n");
  Print("  rm <module>:             Delete a module\n");
}

MACRO CmdDelete(STRING ARRAY params)
{
  RECORD subargs := ParseArguments(params,
      [ [ name := "modulename", type := "param", required := TRUE ]
      ]);

  IF(subargs.modulename = "")
  {
    Print("Syntax: wh module rm <modulename>");
    TerminateScriptWithError("Invalid syntax");
  }

  DeleteModule(subargs.modulename);
}

/////////////////////////////////////////////////
//
// Main
//

RECORD ARRAY options := [ [ name := "command", type := "param", required := TRUE ]
                        , [ name := "params", type := "paramlist" ]
                        ];

RECORD cmdargs := ParseArguments(GetConsoleArguments(), options);

SWITCH(RecordExists(cmdargs) ? cmdargs.command : "help")
{
  CASE "create"
  {
    TerminateScriptWithError("`wh module create` is no longer supported - use wh dev:createmodule");
  }
  CASE "createwebdesign"
  {
    TerminateScriptWithError("`wh module createwebdesign` is no longer supported - use wh dev:createwebdesign");
  }
  CASE "install"
  {
    RECORD subargs := ParseArguments(cmdargs.params,
        [ [ name := "path", type := "param", required := TRUE ]
        ]);

    IF(NOT RecordExists(subargs))
    {
      Print("Syntax: wh module install <path | url>\n");
      TerminateScriptWithError("Invalid syntax");
    }

    BLOB toinstall;
    IF(IsAbsoluteURL(subargs.path, FALSE))
    {
      OBJECT browser := NEW WebBrowser;
      IF(NOT browser->GotoWebPage(subargs.path))
        THROW NEW Exception(`Failed to download from ${subargs.path}`);

      toinstall := browser->content;
    }
    ELSE
    {
      toinstall := GetDiskResource(subargs.path);
    }

    OpenPrimary();
    RECORD installres := ImportModule(toinstall);
    IF(Length(installres.errors) > 0)
    {
      Print("Installation failed:\n" || Detokenize(installres.errors,"\n") || "\n");
      SetConsoleExitCode(1);
    }
    ELSE
    {
      Print(`Module '${installres.importmodulename}' installed\n`);
      IF(Length(installres.warnings) > 0)
        Print("There were warnings:\n" || Detokenize(installres.warnings,"\n") || "\n");
    }
  }
  CASE "rm","del","delete"
  {
    OpenPrimary();
    CmdDelete(cmdargs.params);
  }
  DEFAULT
  {
    PrintSyntaxAndTerminate();
    SetConsoleExitCode(1);
  }
}
