<?wh

LOADLIB "wh::adhoccache.whlib";

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/logging.whlib";
LOADLIB "mod::system/lib/webserver.whlib";
LOADLIB "mod::system/lib/internal/harescript/preload.whlib";
LOADLIB "mod::system/lib/internal/webserver/whfsexecute.whlib";

LOADLIB "mod::publisher/lib/internal/siteprofiles/reader.whlib";

RECORD FUNCTION GetCacheableFileData(INTEGER fileid)
{
  IF(HavePrimaryTransaction())
    ABORT("A primary transaction is already open - you may have to update the access rule and disable transactions");

  OBJECT trans := OpenPrimary();
  RECORD info := GetSystemRedirectInfo(fileid);
  IF(NOT info.success)
  {
    LogError("system:redirect", "GetCacheableFileData", info);
    AbortWithHTTPError(404, "Not found - See notice log for details");
  }

  // Invalidate on recompilation of siteprofiles, or on any modification of the (parent)folder
  STRING ARRAY eventmasks := [ "publisher:internal.siteprofiles.recompiled" ];
  FOREVERY(INTEGER folderid FROM info.folders)
    INSERT "system:whfs.folder." || folderid INTO eventmasks AT END;

  trans->Close();

  RETURN
      [ ttl :=        5 * 60 * 1000
      , value :=      info
      , eventmasks := eventmasks
      ];
}


///////////////////////////////////////////////////////////////////////////
//
// Main code
//
PUBLIC MACRO __Dummy() //exists only to make systemredirect.whlib happy
{

}

INTEGER currentfileid := GetAuthenticationRecord().whfsexecute;

// Get the library data for the library
RECORD rec := GetAdhocCached([ fileid := currentfileid ], PTR GetCacheableFileData(currentfileid));
IF (NOT RecordExists(rec)) // FIXME: nicer error pages?
  THROW NEW Exception("Could not locate type for fs object #" || currentfileid);

DoWHFSExecute(currentfileid, rec);
