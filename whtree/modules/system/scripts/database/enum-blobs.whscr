﻿<?wh

LOADLIB "wh::os.whlib";
LOADLIB "wh::dbase/dynquery.whlib";
LOADLIB "wh::util/algorithms.whlib";

LOADLIB "mod::system/lib/database.whlib";

LOADLIB "mod::wrd/lib/internal/dbschema.whlib";


OBJECT trans := OpenPrimary();
RECORD ARRAY blobs;

INTEGER filethreshold_part := 1000; // enum all folders that have more than 1/X of the total size

RECORD ARRAY relevant_columns;
FOREVERY(RECORD schemarec FROM trans->GetSchemaListing())
  FOREVERY(RECORD tablerec FROM trans->GetTableListing(schemarec.schema_name))
  {
    IF(schemarec.schema_name = "webhare_internal" AND tablerec.table_name = "blob")
      CONTINUE;

    FOREVERY(RECORD columnrec FROM trans->GetColumnListing(schemarec.schema_name, tablerec.table_name))
      IF(columnrec.data_type = "webhare_internal.webhare_blob")
        INSERT CELL [ table_schema := schemarec.schema_name
                    , tablerec.table_name
                    , columnrec.column_name
                    , tablerec.primary_key_name
                    ] INTO relevant_columns AT END;
  }

//RECORD ARRAY relevant_columns := SELECT table_schema,table_name,column_name from information_schema.columns where data_type="BLOB" ORDER BY table_schema, table_name, column_name;
FOREVERY(RECORD col FROM relevant_columns)
{
  IF (col.table_schema = "WRD")
    CONTINUE;

  Print("Scanning " || col.table_schema || "." || col.table_name || "." || col.column_name || " (" || #col+1 || "/" || LENGTH(relevant_columns) || ")\n");
  FlushOutputBuffer();

  OBJECT q := NEW DynamicQuery;
  BOOLEAN have_id := ToUppercase(col.primary_key_name) = "ID";
  trans->ColumnExists(col.table_schema, col.table_name, col.primary_key_name);
  STRING ARRAY getcols := [STRING(col.column_name)];
  IF(have_id)
    INSERT col.primary_key_name INTO getcols AT END;

  STRING tablename := col.table_schema||"."||col.table_name;
  STRING colname := col.column_name;

  q->AddTable("data", trans->id, tablename, getcols);
  RECORD ARRAY colblobs :=
      SELECT tablename :=   tablename
           , size :=        LENGTH64(GetCell(data, colname))
        FROM q->Execute()
       WHERE LENGTH(GetCell(data, colname)) != 0;

  blobs := blobs CONCAT
      SELECT *
           , size_sectors :=  (size + 511) BITAND -512
           , size_pages :=    (size + 4095) BITAND -4096
        FROM colblobs;
}
PRINT("Grouping\n");

// Order for median
blobs := SELECT * FROM blobs ORDER BY tablename, size;

blobs :=
    SELECT tablename
         , blobs :=             COUNT(*)
         , avgsize :=           AVG(size)
         , totalsize :=         SUM(size)
         , totalsize_gb :=      SUM(size) / 1024 / 1024 / 1024
         , totalsize_sectors := SUM(size_sectors)
         , totalsize_pages :=   SUM(size_pages)
         , less4k :=            LENGTH(SELECT FROM ToRecordArray(GroupedValues(size), "bsize") WHERE bsize < 4096)
         , median :=            GroupedValues(size)[Count(*) / 2]
      FROM blobs
  GROUP BY tablename;

PRINT("Result:\n");
PRINT(AnyToString(blobs, "boxed"));

PRINT("Reading all folders:\n");
RECORD ARRAY folders :=
    SELECT id :=        id
         , parent  :=   parent
         , name :=      name
         , size :=      0i64
      FROM system.fs_objects
  ORDER BY id;

INSERT [ id := 0, parent := -1, name := "", size := 0i64 ] INTO folders AT 0;

PRINT("Reading all fs_settings:\n");
RECORD ARRAY fs_settings :=
    SELECT id :=              fs_instance
         , settingsize :=     SUM(LENGTH64(blobdata))
      FROM system.fs_settings
  GROUP BY fs_instance;

fs_settings := EnrichWithTable(fs_settings, "id", system.fs_instances, [ fs_object := "fs_object" ]);

PRINT("Reading all files:\n");
RECORD ARRAY folderfiles :=
    SELECT parent :=    isfolder ? id : parent
         , id
         , size :=      LENGTH64(data)
      FROM system.fs_objects AS file
  ORDER BY id;

PRINT("Join file and settings sizes:\n");
folderfiles := JoinArrays(folderfiles, "id", fs_settings, [ settingsize := 0i64 ], [ rightouterjoin := TRUE ]);

folderfiles :=
    SELECT parent
         , size :=      SUM(size + settingsize)
      FROM folderfiles
  GROUP BY parent;

INTEGER64 totalfiles := SELECT AS INTEGER64 Sum(size) FROM folderfiles;
INTEGER64 filethreshold := totalfiles / filethreshold_part;

PRINT("Adding files to folders:\n");
FOREVERY (RECORD ff FROM folderfiles)
{
  INTEGER pos := RecordLowerBound(folders, [ id := ff.parent ], [ "ID" ]).position;
  folders[pos].size := folders[pos].size + ff.size;
}

folders := SELECT * FROM folders ORDER BY parent;

INTEGER cnt;

RECORD FUNCTION GetFolders(RECORD ARRAY rawdata, STRING name, INTEGER64 localsize, INTEGER parent)
{
  IF ((cnt % 1000) = 0)
    PRINT("Folder " || cnt || " / " || LENGTH(folders) || "\r");

  RECORD pos := RecordLowerBound(rawdata, [ parent := parent ], [ "PARENT" ]);
  INTEGER ubound := RecordUpperBound(rawdata, [ parent := parent ], [ "PARENT" ]);

  INTEGER64 size := localsize;
  BOOLEAN hasthresholdchildren;

  RECORD ARRAY subs;
  FOR (INTEGER i := pos.position; i < ubound; i := i + 1)
  {
    RECORD s := rawdata[i];
    RECORD rec := GetFolders(rawdata, (name = "" OR name = "/" ? name : name || "/") || s.name, s.size, s.id);
    size := size + rec.direct.size;
    subs := subs CONCAT [ RECORD(rec.direct) ] CONCAT rec.subs;
    IF (rec.direct.thr)
      hasthresholdchildren := TRUE;
  }

  RETURN
      [ direct :=
            [ name := name
            , size := size
            , thr :=  size > filethreshold
            , newthr := size > filethreshold// AND NOT hasthresholdchildren
            ]
      , subs := subs
      ];
}

PRINT("Calculating folder total sizes\n");
RECORD final := GetFolders(folders, "/", 0, -1);

RECORD ARRAY lsubs :=
    SELECT name
         , size
         , size_mb :=       RoundToMultiple(MONEY(size / 1024 / 1024), 0.01m, "half-up")
         , size_gb :=       RoundToMultiple(MONEY(size / 1024 / 1024 / 1024), 0.01m, "half-up")
         , perc :=          RoundToMultiple(totalfiles = 0 ? 0m : MONEY(size) / (MONEY(totalfiles) / 100m), 0.01m, "half-up")
      FROM final.subs
     WHERE newthr
  ORDER BY name;/*DESC;
  ORDER BY size DESC;*/

PRINT(AnyToString(lsubs, "boxed"));


PRINT("Reading all WRD entity settings:\n");
RECORD ARRAY entity_settings :=
    SELECT attribute
         , size :=      SUM(Length64(blobdata))
      FROM wrd.entity_settings
  GROUP BY attribute;

PRINT("Enriching setting stats:\n");
entity_settings := EnrichWithTable(entity_settings, "attribute", wrd.attrs, [ type := "type" ]);
entity_settings := EnrichWithTable(entity_settings, "type", wrd.types, [ wrd_schema := "wrd_schema", tag := "tag" ]);
entity_settings := EnrichWithTable(entity_settings, "wrd_schema", wrd.schemas, [ name := "name" ]);

INTEGER64 total_wrd := SELECT AS INTEGER64(SUM(size)) FROM entity_settings;

PRINT("WRD schemas:\n");
PRINT(AnyToString((
    SELECT name
         , size_mb :=       RoundToMultiple(MONEY(SUM(size) / 1024 / 1024), 0.01m, "half-up")
         , size_gb :=       RoundToMultiple(MONEY(SUM(size) / 1024 / 1024 / 1024), 0.01m, "half-up")
         , perc :=          RoundToMultiple(total_wrd = 0 ? 0m : MONEY(SUM(size)) / (MONEY(total_wrd) / 100m), 0.01m, "half-up")
      FROM entity_settings
  GROUP BY name
  ORDER BY name), "boxed"));

PRINT("Per type (only types with > 1% of WRD total):\n");
RECORD ARRAY pertype :=
    SELECT name :=          `${name} - ${tag}`
         , size_mb :=       RoundToMultiple(MONEY(SUM(size) / 1024 / 1024), 0.01m, "half-up")
         , size_gb :=       RoundToMultiple(MONEY(SUM(size) / 1024 / 1024 / 1024), 0.01m, "half-up")
         , perc :=          RoundToMultiple(total_wrd = 0 ? 0m : MONEY(SUM(size)) / (MONEY(total_wrd) / 100m), 0.01m, "half-up")
      FROM entity_settings
  GROUP BY name, tag
  ORDER BY name, tag;

PRINT(AnyToString((SELECT * FROM pertype WHERE perc >= 1m), "boxed"));
