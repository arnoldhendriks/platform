<?wh
LOADLIB "wh::datetime.whlib";
LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/resources.whlib";
LOADLIB "mod::system/lib/whfs.whlib";
LOADLIB "mod::system/lib/internal/modules/disk.whlib";
LOADLIB "mod::system/lib/internal/userrights/cleanup.whlib";
LOADLIB "mod::system/lib/internal/userrights/resync.whlib";
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";
LOADLIB "mod::socialite/lib/database.whlib";

LOADLIB "mod::wrd/lib/api.whlib";

OBJECT trans := OpenPrimary();

MACRO CleanupTasks()
{
  trans->BeginWork();

  //these retention times are configurable in the global settings of system:config
  INTEGER finishedtaskretention := ReadRegistrykey("system.tasks.finishedretention"); //default: 3
  INTEGER finishedmailretention := ReadRegistrykey("system.services.smtp.mailretention"); //default: 7

  DELETE FROM system.managedtasks WHERE finished != DEFAULT DATETIME AND tasktype != "system:outgoingmail" AND finished <= AddDaysToDate(-finishedtaskretention, GetCurrentDateTime());
  DELETE FROM system.managedtasks WHERE finished != DEFAULT DATETIME AND tasktype = "system:outgoingmail" AND finished <= AddDaysToDate(-finishedmailretention, GetCurrentDateTime());

  trans->CommitWork();
}

MACRO CleanupUsageLogs()
{
  trans->BeginWork();

  DATETIME delete_usagestats_older_than := AddDaysToDate(-ReadRegistrykey("system.webserver.global.usagelogdays"), GetCurrentDateTime());
  DELETE FROM system.webservers_usage WHERE time < delete_usagestats_older_than;

  trans->CommitWork();
}

MACRO CleanupDiversions()
{
  trans->BeginWork();

  DATETIME cutoff := AddDaysToDate(-ReadRegistryKey("system.webserver.webdav.divertcachedays"), GetCurrentDatetime());
  DELETE FROM system.webdav_diversions WHERE created < cutoff;

  trans->CommitWork();
}

MACRO CleanupUsermgmt()
{
  OBJECT usermgmtschema := OpenWRDSchema("system:usermgmt");
  IF(NOT ObjectExists(usermgmtschema))
    RETURN;

  INTEGER retention := ReadRegistrykey("system.userrights.retention");
  IF(retention > 0)
  {
    DATETIME cutoff := AddDaysToDate(-retention, GetCurrentDatetime());
    trans->BeginWork();
    FOREVERY(RECORD type FROM usermgmtschema->ListTypes())
    {
      OBJECT wrdtype := usermgmtschema->GetTypeById(type.id);
      RECORD ARRAY tokill := wrdtype->RunQuery([ outputcolumns := [ "WRD_ID" ]
                                               , filters := [[ field := "WRD_LIMITDATE", matchtype := "<", value := cutoff ]]
                                               , historymode := "all"

                                               ]);
      IF(Length(tokill) > 0)
        wrdtype->DeleteEntities(SELECT AS INTEGER ARRAY wrd_id FROM tokill);
    }
    trans->CommitWork();
  }

  trans->BeginWork();
  FixUnconnectedItems(usermgmtschema);
  trans->CommitWork();
}

MACRO CleanupUserRights()
{
  trans->BeginWork();
  CleanupAuthObjects(); //always cleanup authobjects first, that may drop references to rights tables
  CleanupObsoleteRights();
  trans->CommitWork();
}

MACRO FixupUserMgmt()
{
  trans->BeginWork();

  ResyncAllToSystemTables();

  trans->CommitWork();
}

MACRO CleanupWebharePrivate()
{
  RECORD ARRAY candidates := SELECT id, name
                               FROM system.fs_objects
                              WHERE parent = whconstant_whfsid_private
                                    AND name NOT LIKE "whfs-*" //TODO remove this once we're satisfied no server has whfs- at the old location (4.33 should move it away)
                                    AND name NOT IN GetInstalledModuleNames();
  IF(Length(candidates) > 0)
  {
    INTEGER ARRAY notempty := SELECT AS INTEGER ARRAY DISTINCT parent FROM system.fs_objects WHERE parent IN (SELECT AS INTEGER ARRAY id FROM candidates);
    candidates := SELECT * FROM candidates WHERE id NOT IN notempty;
  }
  IF(Length(candidates) > 0)
  {
    trans->BeginWork();
    FOREVERY(RECORD candidate FROM candidates)
      OpenWHFSObject(candidate.id)->RecycleSelf();
    trans->CommitWork();
  }

  // get rid of webhare-moduledata if possible
  IF(RecordExists(SELECT FROM system.fs_objects WHERE id = 12 AND parent = 0 AND name = "webhare-moduledata")
     AND NOT RecordExists(SELECT FROM system.fs_objects WHERE parent = 12))
  {
    trans->BeginWork();
    OpenWHFSObject(12)->RecycleSelf();
    trans->CommitWork();

  }
}

MACRO CleanupSocialite()
{
  trans->BeginWork();
  //Remove unconfigured apps
  INTEGER ARRAY appswithkeys := SELECT AS INTEGER ARRAY application FROM socialite.applicationkeys WHERE value != "";
  DELETE FROM socialite.applications WHERE id NOT IN appswithkeys;
  trans->CommitWork();
}

MACRO DeleteOldIssues()
{
  //TODO should this be configurable?
  DATETIME cutoff := AddDaysToDate(-400, GetCurrentDatetime());
  trans->BeginWork();

  OBJECT wrd_type := OpenWRDSchema("system:config")->GetType("server_check");
  RECORD ARRAY todelete := wrd_type->RunQuery(
      [ outputcolumns := [ "WRD_ID" ]
      , filters := [ [ field := "WRD_LIMITDATE", matchtype := "<", value := cutoff ] ]
      , historymode := "all"
      ]);

  wrd_type->DeleteEntities(SELECT AS INTEGER ARRAY wrd_id FROM todelete);
  trans->CommitWork();
}


MACRO RunExecuteMaintenance()
{
  IF(NOT IsRestoredWebhare())
  {
    CleanupTasks();
    CleanupUsageLogs();
    CleanupDiversions();
    CleanupUserMgmt();
    CleanupUserRights();
    CleanupWebharePrivate();
    CleanupSocialite();
    EnsureModuleStorageFolders(TRUE); //cleanup obsolete existing ones too
    DeleteOldIssues();
  }
  FixupUserMgmt();
}

RunExecuteMaintenance();
