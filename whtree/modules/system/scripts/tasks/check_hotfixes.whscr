<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::files.whlib";
LOADLIB "wh::util/algorithms.whlib";
LOADLIB "wh::xml/dom.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/logging.whlib";
LOADLIB "mod::system/lib/resources.whlib";
LOADLIB "mod::system/lib/internal/moduledefparser.whlib";
LOADLIB "mod::system/lib/internal/moduleimexport.whlib";


RECORD hotfixstate :=
    [ modules :=      RECORD[]
    , calculated :=   GetCurrentDatetime()
    ];

FOREVERY (STRING module FROM GetSortedSet(GetInstalledModulenames()))
{
  RECORD rec := CELL
      [ module
      , messages :=   STRING[]
      ];

  STRING path := GetModuleInstallationRoot(module);
  IF (RecordExists(GetDiskFileProperties(MergePath(path, "history/source.zip"))))
  {
    RECORD changes := GetModuleModifiedFiles(module, path || "/", FALSE);
    IF (changes.success AND RecordExists(changes.files))
    {
      STRING ARRAY limitedfiles := SELECT AS STRING ARRAY COLUMN path FROM changes.files LIMIT 4;
      INTEGER unnamed := LENGTH(changes.files) - LENGTH(limitedfiles);
      INSERT `:Module ${module} has hotfixes: ${Detokenize(STRING ARRAY(limitedfiles), ", ")}${unnamed = 0 ? "" : ` and ${unnamed} more`}` INTO rec.messages AT END;
    }
  }

  BLOB manifestblob := GetDiskResource(MergePath(path, "history/manifest.xml"), [ allowmissing := TRUE ]);
  IF (LENGTH(manifestblob) != 0)
  {
    OBJECT manifestdoc := MakeXMLDocument(manifestblob);
    RECORD manifest := ParseModuleManifest(manifestdoc);

    IF (manifest.original_source = "git" AND manifest.source_haslocalremoterevision)
    {
      IF (manifest.source_revision != manifest.source_localremoterevision OR manifest.source_revision != manifest.source_remoterevision)
      {
        IF (manifest.source_localremoterevision = "")
        {
          INSERT `:Module ${module} was deployed without a repository in source control` INTO rec.messages AT END;
        }
        ELSE IF (manifest.source_revision != manifest.source_localremoterevision)
        {
          // Either uncommitted changes or not rebased yet
          IF (RecordExists(SELECT FROM manifest.commits WHERE id = manifest.source_localremoterevision))
          {
            // Local remote is ancestor of current commit - so current commit just hasn't been pushed yet
            INSERT `:Module ${module} was deployed while its commit had not been pushed to source control` INTO rec.messages AT END;
          }
        }
      }
    }

    IF (manifest.has_modifications)
      INSERT `:Module ${module} had uncommitted changes when it was deployed` INTO rec.messages AT END;
  }

  IF (LENGTH(rec.messages) != 0)
    INSERT rec INTO hotfixstate.modules AT END;
}

IF (LENGTH(hotfixstate.modules) != 0)
  PRINT(`The following modules have problems: ${Detokenize((SELECT AS STRING ARRAY module FROM hotfixstate.modules), ", ")}\n`);

STRING hotfixfile := GetWebHareConfiguration().ephemeralroot || "hotfixstate.json";

// Log audit events for every change + hotfixes present at audit log file switch
RECORD ARRAY curmodules;
BOOLEAN daychange;
BLOB curdata := GetDiskResource(hotfixfile, [ allowmissing := TRUE ]);

IF (IsValueSet(curdata))
{
  // Decode hotfix data
  RECORD decoded := EnforceStructure(
      [ modules :=    [ [ module := "", messages := STRING[] ] ]
      , calculated := DEFAULT DATETIME
      ], DecodeJSONBlob(curdata));

  // Audit log is logged in UTC, so we can just use daycount to determine the first check going to a new log file
  daychange := GetDayCount(decoded.calculated) != GetDayCount(hotfixstate.calculated);
  curmodules :=
      SELECT module
           , curmessages :=   messages
        FROM decoded.modules;
}

RECORD ARRAY joined := JoinArrays(hotfixstate.modules, "module", curmodules, [ curmessages := STRING[] ],
    [ rightouterjoin := TRUE
    , leftouterjoin := [ messages := STRING[] ]
    ]);

FOREVERY (RECORD rec FROM joined)
{
  // Record in audit log when messages changed or messages present at first check of the day
  IF (NOT ArrayIsSetEqual(rec.curmessages, rec.messages) OR (IsValueSet(rec.messages) AND daychange))
  {
    LogAuditEvent("system:hotfix-state", CELL
          [ rec.module
          , rec.messages
          ]);
  }
}

StoreDiskFile(hotfixfile, EncodeJSONBlob(hotfixstate), [ overwrite := TRUE ]);
