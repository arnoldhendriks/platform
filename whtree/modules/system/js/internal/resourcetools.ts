import { loadJSFunction as _loadJSFunction } from "@webhare/services";

/** @deprecated In WH5.7+, use loadJSFunction from \@webhare/services */
export const loadJSFunction = _loadJSFunction;
