export * from "./finmath";

import { warnESFile } from "@mod-system/js/internal/es-warning";
warnESFile("@mod-system/js/util/finmath.es");
