<schemadefinition xmlns="http://www.webhare.net/xmlns/wrd/schemadefinition">

  <!-- The system:config schema stores various structured settings which would be quite complex in the registry but don't
       warrant their own tables -->
  <domain tag="configapp">
    <!-- Domain for configuration apps. Used to attach rights -->
    <attributes>
      <free tag="apptag" required="true" title="Application tag, eg system:dashboard" unique="true" />
      <free tag="tid" title="TID to use" />
    </attributes>
  </domain>

  <object tag="taskcluster" keephistorydays="2600">
    <!-- overrides for module task clusters -->
    <attributes>
      <free tag="cluster" required="true" unique="true" />  <!-- must match the module:cluster tag -->
      <integer tag="harescriptworkers" />  <!-- if set, overrides harescriptworkers value for this cluster (if the cluster actually requires harescript workers) -->
    </attributes>
  </object>

  <object tag="tasktype" keephistorydays="2600">
    <!-- overrides for module task types -->
    <attributes>
      <free tag="type" required="true" unique="true" />  <!-- must match the module:cluster tag -->
      <boolean tag="profile" />
    </attributes>
  </object>

  <object tag="externalservice" keephistorydays="2600">
    <!-- list services we are allowed to connect with -->
    <attributes>
      <free tag="url" required="true" />
      <free tag="servicetype" required="true" />

      <!-- TODO: once the 'tika' users are switched, <obsolete tag="type" /> .. we introduced servicetype in 4.34 -->
      <integer tag="priority" />
    </attributes>
  </object>

  <object tag="statusupload" keephistorydays="2600">
    <!-- list URLs to which we report our status -->
    <attributes>
      <free tag="wrd_title" required="true" />
      <free tag="url" required="true" />
      <enumarray tag="reports" allowedvalues="modules sites issues" />
    </attributes>
  </object>

  <object tag="mailroute" keephistorydays="2600">
    <!-- list URLs to which we report our status -->
    <attributes>
      <free tag="wrd_title" />
      <integer tag="wrd_ordering" />
      <free tag="originmask" />
      <array tag="sendermasks">
        <free tag="mask" />
      </array>
      <array tag="recipientmasks">
        <free tag="mask" />
      </array>
      <free tag="serverurl" />
      <free tag="rewritemailaddress" />
      <boolean tag="includeoriginaladdress" />
    </attributes>
  </object>

  <object tag="mailsender" keephistorydays="2600">
    <!-- sender whitelist -->
    <attributes>
      <free tag="wrd_title" />
      <free tag="mask" />
    </attributes>
  </object>

  <object tag="mailrecipient" keephistorydays="2600">
    <!-- recipients (receivers) whitelist -->
    <attributes>
      <free tag="wrd_title" />
      <free tag="mask" />
    </attributes>
  </object>

  <object tag="mailcorrection">
    <attributes>
      <free tag="wrd_title" />
      <integer tag="wrd_ordering" />
      <array tag="masks">
        <free tag="mask" />
      </array>
      <boolean tag="block" />
      <free tag="rewriteto" />
      <integer tag="suggestionfuzz" />
    </attributes>
  </object>

  <!-- server checks -->
  <object tag="server_check">
    <attributes>
      <enum tag="type" allowedvalues="?*:?*" description="Check's type, in module:tag format" required="true" />
      <free tag="check_task" description="Timed task responsible for managing this check" />
      <json tag="metadata" description="Additional metadata to identify unique instances of this check" />
      <free tag="message_text" description="Message text" required="true" />
      <json tag="message_tid" description="Translatable message text (of the form: {tid,params})" />
      <boolean tag="is_critical" />
      <json tag="jump_to" description="Jump instruction to application which can provide more information" />
      <datetime tag="snoozed_until" description="Time until we start actively reporting this error again" />
      <enumarray tag="scopes" allowedvalues="gdpr policy" description="Special scopes for some checks" />
    </attributes>
  </object>

  <!-- history of this check -->
  <attachment tag="server_check_history" linkfrom="server_check">
    <attributes>
      <enum tag="event"
            allowedvalues="start change stop snooze dismiss"
            gid="tolliumapps.dashboard.platform.eventtypes"
            required="true" />
      <free tag="message_text" description="Message text (start and change events)" />
      <json tag="message_tid" description="Message tid (start and change events)" />
      <free tag="comment" description="Comment (snooze and dismiss)" />
      <json tag="wh_user" description="User who snoozed or dismissed this message" />
      <datetime tag="snoozed_until" description="Until when we were snoozed" />
    </attributes>
  </attachment>

  <object tag="custom_check_messages">
    <attributes>
      <free tag="wrd_title" description="Message text" required="true" />
      <json tag="conditions" typedeclaration="mod::system/data/wrdschemas/config_types.ts#ManualCheckMessagesConditions" />
    </attributes>
  </object>

</schemadefinition>
