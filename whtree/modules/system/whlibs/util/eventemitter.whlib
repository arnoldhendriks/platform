<?wh

LOADLIB "wh::util/algorithms.whlib";


/** Object on which multiple event emitter listeners can be registered, and all
    deregistered at the same time
*/
STATIC OBJECTTYPE EventEmitterListenGroup
<
  OBJECT client;
  INTEGER ARRAY ids;

  MACRO NEW(OBJECT client)
  {
    this->client := client;
  }

  /** Add an event listener
      @param event Event name (exact match!)
      @param call Event callback. Signature: MACRO func(RECORD event_data)
  */
  PUBLIC MACRO AddListener(STRING event, FUNCTION PTR call)
  {
    INSERT this->client->AddListener(event, call) INTO this->ids AT END;
  }

  /// Removes all listeners
  PUBLIC MACRO Close()
  {
    FOREVERY (INTEGER id FROM this->ids)
      this->client->RemoveListener(id);
    this->ids := INTEGER[];
  }
>;


/** Eventemitter, a bit like the one from nodejs
*/
PUBLIC STATIC OBJECTTYPE EventEmitter
<
  RECORD ARRAY listeners;
  RECORD ARRAY listenerids;
  INTEGER listenerid;

  /** Emits an event, calls callbacks for all listeners added for this event
      @param event Event name
      @param data Event data
  */
  MACRO EmitEvent(STRING event, RECORD data)
  {
    INTEGER id := 0;
    WHILE (TRUE)
    {
      RECORD pos := RecordLowerBound(this->listeners, CELL[ event ], [ "EVENT" ]);
      IF (NOT pos.found)
        RETURN;
      RECORD ARRAY callbacks := this->listeners[pos.position].callbacks;
      RECORD cpos := RecordLowerBound(callbacks, [ id := id ], [ "ID" ]);
      IF (cpos.position = LENGTH(callbacks))
        RETURN;
      RECORD cb := callbacks[cpos.position];
      id := cb.id + 1;
      cb.call(data);
    }
  }

  /** Add an event listener
      @param event Event name (exact match!)
      @param call Event callback. Signature: MACRO func(RECORD event_data)
      @return Event listener registration id.
  */
  PUBLIC INTEGER FUNCTION AddListener(STRING event, FUNCTION PTR call)
  {
    this->listenerid := this->listenerid + 1;
    RECORD pos := RecordLowerBound(this->listeners, CELL[ event ], [ "EVENT" ]);
    IF (NOT pos.found)
      INSERT CELL[ event, callbacks := RECORD[] ] INTO this->listeners AT pos.position;
    INSERT CELL[ id := this->listenerid, call ] INTO this->listeners[pos.position].callbacks AT END;
    INSERT CELL[ id := this->listenerid, event ] INTO this->listenerids AT END;
    RETURN this->listenerid;
  }

  /** Removes a listener
      @param id Id as returned by AddListener
  */
  PUBLIC MACRO RemoveListener(INTEGER id)
  {
    RECORD pos := RecordLowerBound(this->listenerids, CELL[ id ], [ "ID" ]);
    IF (NOT pos.found)
      THROW NEW Exception("No such listener");
    STRING event := this->listenerids[pos.position].event;
    DELETE FROM this->listenerids AT pos.position;
    pos := RecordLowerBound(this->listeners, CELL[ event ], [ "EVENT" ]);
    RECORD cpos := RecordLowerBound(this->listeners[pos.position].callbacks, CELL[ id ], [ "ID" ]);
    DELETE FROM this->listeners[pos.position].callbacks AT cpos.position;
  }

  /** Get an object to register listeners on that can all be removed in one call
      @return(object #EventEmitterListenGroup) Listen group object
  */
  PUBLIC OBJECT FUNCTION GetListenGroup()
  {
    RETURN NEW EventEmitterListenGroup(this);
  }
>;

