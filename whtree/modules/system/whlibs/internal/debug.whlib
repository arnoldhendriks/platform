<?wh (* ISSYSTEMLIBRARY *)
/** @topic harescript-core/builtins
    @public
*/

LOADLIB "wh::internal/interface.whlib";
LOADLIB "wh::internal/transservices.whlib";

BOOLEAN FUNCTION RecordExists(RECORD rec) __ATTRIBUTES__(EXTERNAL, CONSTANT);
BOOLEAN FUNCTION ObjectExists(OBJECT obj) __ATTRIBUTES__(EXTERNAL);
STRING FUNCTION GetExternalSessionData() __ATTRIBUTES__(EXTERNAL);
RECORD ARRAY FUNCTION GetStackTrace() __ATTRIBUTES__(EXTERNAL);
STRING FUNCTION EncodeJava(STRING text) __ATTRIBUTES__(EXTERNAL, CONSTANT);
STRING FUNCTION EncodeHSON(VARIANT arg)
{
  RETURN JSONENCODETOSTRING(arg, TRUE, DEFAULT RECORD, DEFAULT RECORD);
}
VARIANT FUNCTION GetSortedSet(VARIANT input)
{
  RETURN __HS_SQL_SortArray(input, FALSE, TRUE);
}
MACRO __HS_INTERNAL_SETDEBUGGINGTAGS(STRING ARRAY tags) __ATTRIBUTES__(EXTERNAL);

PUBLIC RECORD __debugconfig;
PUBLIC STRING ARRAY __webdebugsettings;

PUBLIC MACRO PTR __ondebuglog; //installed by preload

/** @short Log debugging data
    @param logsource Source or call of the debug message.
                     This is used for searching, determining the origin or grouping of log messages of this type.
                     Guideline: prefix the source with your module, for example "modulename:mytask.results"
    @param message Message
    @param args Optional extra debugging arguments. These will be stored HSON encoded. Arguments which push the length of the debug line over 127 KB won't be logged and will be stored as '-' */
PUBLIC MACRO LogDebug(STRING logsource, VARIANT message, VARIANT ARRAY args) __ATTRIBUTES__(VARARG)
{
  IF(logsource = "")
    logsource := GetStackTrace()[1].filename;

  IF(Length(args) > 0)
    message := VARIANT[message, ...args];

  IF(__ondebuglog != DEFAULT MACRO PTR) //This should normally be set up by the preload
    __ondebuglog(logsource, message);
  ELSE
  {
    STRING msg := TYPEID(message) = TYPEID(STRING) ? EncodeJava(message) : EncodeHSON(message);
    PrintTo(2, `[system:debug] ${logsource} ${Length(msg) > 127*1024 ? "-" : msg}\n`);
  }
}

/** Returns whether a tag is currently enabled in the current debugging configuration
    @param tag Tag to test for
    @return Whether the debug tag has been enabled
*/
PUBLIC BOOLEAN FUNCTION IsDebugTagEnabled(STRING tag)
{
  RETURN tag IN __webdebugsettings OR (RecordExists(__debugconfig) AND tag IN __debugconfig.tags);
}

PUBLIC RECORD FUNCTION ReadDebugSetting(STRING varcontent)
{
  __debugconfig :=
      [ tags :=           STRING[]
      , outputsession :=  "default"
      , context :=        ""
      ];

  STRING ARRAY cmdtokens := Tokenize(varcontent,",");
  IF("reset" IN cmdtokens)
    RETURN __debugconfig;

  FOREVERY(STRING cmd FROM cmdtokens)
  {
    IF(cmd = "")
      CONTINUE;

    IF(cmd LIKE "session=*")
      __debugconfig.outputsession := Substring(cmd,8);
    ELSE IF(cmd LIKE "context=*")
      __debugconfig.context := Substring(cmd,8);
    ELSE
      INSERT cmd INTO __debugconfig.tags AT END;
  }

  __debugconfig.tags := GetSortedset(__debugconfig.tags);
  __HS_INTERNAL_SETDEBUGGINGTAGS(__debugconfig.tags);
  IF (ObjectExists(GetPrimaryWebhareTransactionObject()))
    GetPrimaryWebhareTransactionObject()->__UpdateDebugConfig(DEFAULT RECORD);
  RETURN __debugconfig;
}

/** Returns whether profiling is enabled. If so, jobs and processes need extra care so they can
    shutdown cleanly (eg using TerminateScript) so they can send they profiles before being closed
    @return TRUE if currently profiling
*/
PUBLIC BOOLEAN FUNCTION IsProfilingEnabled()
{
  RETURN IsDebugTagEnabled("apr") OR IsDebugTagEnabled("cov");
}
