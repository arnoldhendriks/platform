<?wh (* ISSYSTEMLIBRARY *)

PUBLIC STRING FUNCTION GetCallingLibrary(BOOLEAN skip_system) __ATTRIBUTES__(EXTERNAL);
PUBLIC RECORD FUNCTION  __DoEvpCrypt(STRING algo, BOOLEAN encrypt, STRING keydata, STRING data, STRING iv, STRING tag) __ATTRIBUTES__(EXTERNAL, EXECUTESHARESCRIPT, CONSTANT);
PUBLIC         MACRO    __HS_COLLECTGARBAGE() __ATTRIBUTES__(EXTERNAL);
PUBLIC OBJECT  FUNCTION __HS_GETRESETTHROWVAR() __ATTRIBUTES__(EXTERNAL);
PUBLIC         MACRO    __HS_THROWEXCEPTION(OBJECT obj, BOOLEAN isrethrow) __ATTRIBUTES__(EXTERNAL, EXECUTESHARESCRIPT, TERMINATES);
PUBLIC INTEGER FUNCTION __HS_TYPEID(VARIANT a) __ATTRIBUTES__(EXTERNAL, CONSTANT);

//BLIC NCTION PTR ARRAY FUNCTION
PUBLIC DATETIME         FUNCTION GETBLOBMODTIME(BLOB file) __ATTRIBUTES__(EXTERNAL); //internal api, not guaranteed to return a modtime (eg dbase blob)
PUBLIC STRING           FUNCTION GETBLOBDESCRIPTION(BLOB file) __ATTRIBUTES__(EXTERNAL);


/** @short Returns the timestamp of the current date and time on the server
    @long You can use the @italic AddTimeToDate() function in conjunction with this function to
          get the time and date in different time zones.
    @return A DateTime value, representing the current date and time at the WebHare system in UTC.
    @public
    @loadlib wh::datetime.whlib
    @topic harescript-core/datetime
    @see wh::datetime.whlib#AddTimeToDate
    @example
// returns a timestamp of the current date and time
DATETIME serverTime := GetCurrentDateTime();

// returns a timestamp of the current date and time in UTC + 1
INTEGER ExtraMsecs := 60 * 60 * 1000;
DATETIME UTC1 := AddTimeToDate( ExtraMSecs, GetCurrentDateTime() );
*/
PUBLIC DATETIME         FUNCTION GetCurrentDateTime() __ATTRIBUTES__(EXTERNAL);

PUBLIC INTEGER          FUNCTION JSONDECODERALLOCATE(BOOLEAN hson, RECORD options, RECORD translations) __ATTRIBUTES__(EXTERNAL, EXECUTESHARESCRIPT);
PUBLIC RECORD           FUNCTION JSONDECODERFINISH(INTEGER id) __ATTRIBUTES__(EXTERNAL);
PUBLIC BOOLEAN          FUNCTION JSONDECODERPROCESS(INTEGER id, STRING data) __ATTRIBUTES__(EXTERNAL);
PUBLIC RECORD           FUNCTION JSONDECODERQUICK(STRING s, BOOLEAN hson, RECORD options, RECORD translations) __ATTRIBUTES__(EXTERNAL, EXECUTESHARESCRIPT);
PUBLIC BLOB             FUNCTION JSONENCODETOBLOB(VARIANT v, BOOLEAN hson, RECORD translations, RECORD options) __ATTRIBUTES__(EXTERNAL, EXECUTESHARESCRIPT);
PUBLIC STRING           FUNCTION JSONENCODETOSTRING(VARIANT v, BOOLEAN hson, RECORD translations, RECORD options) __ATTRIBUTES__(EXTERNAL, EXECUTESHARESCRIPT);

PUBLIC INTEGER          FUNCTION __HS_EVENT_CREATESTREAM() __ATTRIBUTES__(EXTERNAL);
PUBLIC                  MACRO    __HS_EVENT_CLOSESTREAM(INTEGER id) __ATTRIBUTES__(EXTERNAL);
PUBLIC                  MACRO    __HS_EVENT_STREAMMODIFYSUBSCRIPTIONS(INTEGER id, STRING ARRAY add, STRING ARRAY remove, BOOLEAN reset) __ATTRIBUTES__(EXTERNAL);
PUBLIC RECORD           FUNCTION __HS_EVENT_STREAMREAD(INTEGER id) __ATTRIBUTES__(EXTERNAL);
PUBLIC INTEGER          FUNCTION __HS_EVENT_CREATECOLLECTOR(STRING ARRAY masks) __ATTRIBUTES__(EXTERNAL);
PUBLIC                  MACRO    __HS_EVENT_CLOSECOLLECTOR(INTEGER id) __ATTRIBUTES__(EXTERNAL);
PUBLIC                  MACRO    __HS_EVENT_COLLECTORMODIFYSUBSCRIPTIONS(INTEGER id, STRING ARRAY add, STRING ARRAY remove, BOOLEAN reset) __ATTRIBUTES__(EXTERNAL);
PUBLIC STRING ARRAY     FUNCTION __HS_EVENT_COLLECTORREAD(INTEGER id) __ATTRIBUTES__(EXTERNAL);
PUBLIC                  MACRO    __HS_EVENT_BROADCAST(STRING event, RECORD data, BOOLEAN local) __ATTRIBUTES__(EXTERNAL, EXECUTESHARESCRIPT);

/** @short Get the path to the current directory
    @topic harescript-core/os
    @public
    @loadlib wh::os.whlib
    @return The current path */
PUBLIC STRING FUNCTION GetCurrentPath() __ATTRIBUTES__(EXTERNAL);

/** Determines if the console is a terminal.
    @return TRUE if the console is a terminal. Throws if console support is not available
    @topic harescript-core/os
    @public
    @loadlib wh::os.whlib
    @see #IsConsoleSupportAvailable
*/
PUBLIC BOOLEAN FUNCTION IsConsoleATerminal() __ATTRIBUTES__(EXTERNAL, CONSTANT);

/** Returns TRUE if console support is available (only using `wh run`)
    @return TRUE if console support is available
    @topic harescript-core/os
    @public
    @loadlib wh::os.whlib
*/
PUBLIC BOOLEAN FUNCTION IsConsoleSupportAvailable() __ATTRIBUTES__(EXTERNAL, CONSTANT);

/** Returns the console size
    @return The size of the console. Throws if console support is not available
    @cell(integer) return.rows Number of rows
    @cell(integer) return.cols Number of columns
    @topic harescript-core/os
    @public
    @loadlib wh::os.whlib
    @see #IsConsoleSupportAvailable
*/
PUBLIC RECORD FUNCTION GetConsoleSize() __ATTRIBUTES__(EXTERNAL);

/** @short Open a blob as file
    @topic harescript-core/os
    @public
    @loadlib wh::files.whlib
    @param blobfile Blob to open
    @return ID of open blob file
    @see %GetDiskResource,%OpenDiskFile,%CloseBlobFile
*/
PUBLIC INTEGER          FUNCTION OpenBlobAsFile(BLOB blobfile) __ATTRIBUTES__(EXTERNAL);

/** @short Does a (case sensitive) binary search within a sorted record array
    @long This function searches for an record with specific values within a record array that is sorted
          on specific cells. If the element is found, its position is returned, otherwise the insert
          position is returned (the place the element should be inserted to preserve the ordering of the
          record array)
    @topic harescript-utils/algorithms
    @public
    @loadlib wh::util/algorithms.whlib
    @param list Record array to search in
    @param element Record with values to search for
    @param cellnames Names of cells to search for (the list must be ordered on these cellnames, in the
               same order they are passed to this function)
    @return Whether the element was found, and the position of the element/positionm is should be inserted
                to preserve the ordering of list
    @cell return.found Whether the element was found
    @cell return.position Position of the element (when found, otherwise the insert position)
    @see RecordUpperBound, %LowerBound, %UpperBound
    @example

// List (sorted on 'a', then 'b')
RECORD ARRAY list :=
    [ [ a := 1, b := 10, text := "value 1" ]
    , [ a := 3, b := 1,  text := "second value" ]
    , [ a := 3, b := 3,  text := "value 3" ]
    , [ a := 5, b := 7,  text := "last value" ]
    ];

// Returns [ found := TRUE, position := 1 ]
RECORD res := RecordLowerBound(list, [ a := 3, b := 1 ], [ "A", "B" ]);

// Returns [ found := FALSE, position := 4 ] (one past the end)
RECORD res := RecordLowerBound(list, [ a := 5, b := 8 ], [ "A", "B" ]);

// Returns [ found := FALSE, position := 0 ]
RECORD res := RecordLowerBound(list, [ a := 1, b := 8 ], [ "A", "B" ]);
*/
PUBLIC RECORD FUNCTION RecordLowerBound(RECORD ARRAY list, RECORD element, STRING ARRAY cellnames) __ATTRIBUTES__(EXTERNAL, EXECUTESHARESCRIPT);


/** @short Does a (case sensitive) binary search within a sorted record array, returns the position of the first element that is greater than the searched-for element
    @long This function searches for an record with specific values within a record array that is sorted
          on specific cells. The position of the first element that is greater than the searched-for element
          is returned, or one past the last element if no such element exists.
    @topic harescript-utils/algorithms
    @public
    @loadlib wh::util/algorithms.whlib
    @param list Record array to search in
    @param element Record with values to search for
    @param cellnames Names of cells to search for (the list must be ordered on these cellnames, in the
               same order they are passed to this function)
    @return Position of the first element that is greater than the searched-for element.
    @see RecordLowerBound, %LowerBound, %UpperBound
    @example

// List (sorted on 'a', then 'b')
RECORD ARRAY list :=
    [ [ a := 1, b := 10, text := "value 1" ]
    , [ a := 3, b := 1,  text := "second value" ]
    , [ a := 3, b := 1,  text := "second value again" ]
    , [ a := 3, b := 3,  text := "value 3" ]
    , [ a := 5, b := 7,  text := "last value" ]
    ];

// Returns [ found := TRUE, position := 3 ]
INTEGER res := RecordUpperBound(list, [ a := 3, b := 1 ], [ "A", "B" ]);

// Returns [ found := FALSE, position := 5 ] (one past the end)
INTEGER res := RecordUpperBound(list, [ a := 5, b := 8 ], [ "A", "B" ]);

// Returns [ found := FALSE, position := 0 ]
INTEGER res := RecordUpperBound(list, [ a := 1, b := 8 ], [ "A", "B" ]);
*/
PUBLIC INTEGER FUNCTION RecordUpperBound(RECORD ARRAY list, RECORD element, STRING ARRAY cellnames) __ATTRIBUTES__(EXTERNAL);

/** @short Set the current file pointer
    @long Set the location (in the range 0 to the length of file) where the next read or write will take place.
          If the location is moved or beyond the end of the file, any newly written data will be appended
    @topic harescript-core/os
    @public
    @loadlib wh::files.whlib
    @param filehandle ID of file, as returned by OpenBlobAsFile, OpenDiskFile or CreateDiskFile
    @param newpos New position for the file pointer, in bytes
    @see OpenBlobAsFile,%OpenDiskFile,%CreateDiskFile*/
PUBLIC                  MACRO    SetFilePointer(INTEGER filehandle, INTEGER64 newpos) __ATTRIBUTES__(EXTERNAL, EXECUTESHARESCRIPT);

/** @short   Returns an array of strings by splitting a string into sub strings.
    @long    Tokenize splits a string into substrings, divided on the place of the seperator.
             When found, the separator is removed from the string and the substrings
             are returned in an array.

             If the seperator is not found, the original string in split into its UTF-8 characters.
    @topic   harescript-core/builtins
    @public
    @loadlib wh::system.whlib
    @param   text String to split into tokens
    @param   separator Case-sensitive separator between the tokens
    @return  A string array with the separated tokens
    @example
// returns {"a","b","c","d","e"}
STRING ARRAY example1 := Tokenize("a-b-c-d-e", "-");

// returns {"A","B","C","D","E"}
STRING ARRAY example2 := Tokenize("A&&B&&C&&D&&E", "&&");

// returns the entire string, because seperator "+" is not found
STRING ARRAY example3 := Tokenize("a-b-c-d-e", "+");

// returns an empty array because string and seperator are the same
STRING ARRAY example4 := Tokenize("a/b/c/d/e", "a/b/c/d/e");

// returns the entire string, because seperator "a" is not found
STRING ARRAY example5 := Tokenize("A-B-C-D-E-F", "a");

// Splits the string into its UTF-8 characters
STRING ARRAY example5 := Tokenize("reëel", "");
// Results in [ "r", "e", "ë", "e", "l" ]
*/
PUBLIC STRING ARRAY FUNCTION Tokenize(STRING text, STRING separator) __ATTRIBUTES__(EXTERNAL, CONSTANT);

PUBLIC INTEGER          FUNCTION __EVP_GENERATEKEY(STRING type, INTEGER bits, STRING curve) __ATTRIBUTES__(EXTERNAL);
PUBLIC INTEGER          FUNCTION __EVP_LOADPRVKEY(STRING data) __ATTRIBUTES__(EXTERNAL);
PUBLIC INTEGER          FUNCTION __EVP_LOADPUBKEY(STRING data) __ATTRIBUTES__(EXTERNAL);
PUBLIC STRING           FUNCTION __EVP_GENERATECSR(INTEGER keyhandle, RECORD ARRAY nameparts, STRING altnames) __ATTRIBUTES__(EXTERNAL);
PUBLIC STRING           FUNCTION __EVP_ENCRYPT(INTEGER handle, STRING data) __ATTRIBUTES__(EXTERNAL);
PUBLIC STRING           FUNCTION __EVP_DECRYPT(INTEGER handle, STRING data) __ATTRIBUTES__(EXTERNAL);
PUBLIC STRING           FUNCTION __EVP_SIGN(INTEGER handle, STRING data, STRING alg) __ATTRIBUTES__(EXTERNAL);
PUBLIC BOOLEAN          FUNCTION __EVP_VERIFY(INTEGER handle, STRING data, STRING signature, STRING alg) __ATTRIBUTES__(EXTERNAL);
PUBLIC STRING           FUNCTION __EVP_GETKEYTYPE(INTEGER handle) __ATTRIBUTES__(EXTERNAL);
PUBLIC INTEGER          FUNCTION __EVP_GETKEYLENGTH(INTEGER handle) __ATTRIBUTES__(EXTERNAL);
PUBLIC BOOLEAN          FUNCTION __EVP_ISKEYPUBLICONLY(INTEGER handle) __ATTRIBUTES__(EXTERNAL);
PUBLIC STRING           FUNCTION __EVP_GETPRIVATEKEY(INTEGER handle) __ATTRIBUTES__(EXTERNAL);
PUBLIC STRING           FUNCTION __EVP_GETPUBLICKEY(INTEGER handle) __ATTRIBUTES__(EXTERNAL);
PUBLIC RECORD           FUNCTION __GETHSRESOURCE(STRING path) __ATTRIBUTES__(EXTERNAL);
PUBLIC BOOLEAN          FUNCTION __HS_CloseFile(INTEGER filehandle) __ATTRIBUTES__(EXTERNAL);
PUBLIC RECORD           FUNCTION __HS_GETJOBMANAGERSTATUS(BOOLEAN with_history) __ATTRIBUTES__(EXTERNAL);

PUBLIC INTEGER          FUNCTION __HS_GetSignalIntPipe() __ATTRIBUTES__(EXTERNAL);
PUBLIC                  MACRO    __HS_MARSHALWRITETO(INTEGER stream, VARIANT data) __ATTRIBUTES__(EXTERNAL);
PUBLIC VARIANT          FUNCTION __HS_MarshalReadFromBlob(BLOB blobdata) __ATTRIBUTES__(EXTERNAL);
PUBLIC                  MACRO    __HS_MARSHALPACKETWRITETO(INTEGER stream, VARIANT v) __ATTRIBUTES__(EXTERNAL);
PUBLIC VARIANT          FUNCTION __HS_MARSHALPACKETREADFROMBLOB(BLOB data) __ATTRIBUTES__(EXTERNAL);
PUBLIC STRING           FUNCTION __HS_MoneyToString(MONEY val) __ATTRIBUTES__(EXTERNAL);
PUBLIC INTEGER          FUNCTION __HS_OpenDiskFile(STRING path, BOOLEAN writeaccess, BOOLEAN create, BOOLEAN failifexists, BOOLEAN publicfile) __ATTRIBUTES__(EXTERNAL);
PUBLIC RECORD           FUNCTION __HS_UnpackOleProps(BLOB indata) __ATTRIBUTES__(EXTERNAL, CONSTANT);
PUBLIC RECORD           FUNCTION __HS_SQL_LOWERBOUND(VARIANT list, VARIANT element) __ATTRIBUTES__(EXTERNAL, EXECUTESHARESCRIPT);
PUBLIC INTEGER          FUNCTION __HS_SQL_UPPERBOUND(VARIANT list, VARIANT element) __ATTRIBUTES__(EXTERNAL, EXECUTESHARESCRIPT);
PUBLIC VARIANT          FUNCTION __HS_SQL_SortArray(VARIANT list, BOOLEAN reverse, BOOLEAN makedistinct) __ATTRIBUTES__(EXTERNAL, CONSTANT);
PUBLIC RECORD           FUNCTION __HS_TCPIP_GETLASTERROR(INTEGER handle) __ATTRIBUTES__(EXTERNAL);
PUBLIC INTEGER          FUNCTION __HS_TOINTEGER(VARIANT val) __ATTRIBUTES__(EXTERNAL);
PUBLIC MONEY            FUNCTION __HS_TOMONEY(VARIANT val) __ATTRIBUTES__(EXTERNAL);
PUBLIC FLOAT            FUNCTION __HS_StringToFloat(STRING val, FLOAT defval) __ATTRIBUTES__(EXTERNAL);
PUBLIC RECORD           FUNCTION __HS_WAITFORMULTIPLEUNTIL(INTEGER ARRAY reads, INTEGER ARRAY writes, DATETIME until, BOOLEAN breakonasyncjscode) __ATTRIBUTES__(EXTERNAL);
PUBLIC INTEGER64        FUNCTION __HS_GETRAWMONEY(MONEY val) __ATTRIBUTES__(EXTERNAL);
PUBLIC MONEY            FUNCTION __HS_SETRAWMONEY(INTEGER64 val) __ATTRIBUTES__(EXTERNAL);
PUBLIC BOOLEAN          FUNCTION __HS_VERIFYWEBHAREPASSWORDHASH(STRING pwd, STRING hash) __ATTRIBUTES__(EXTERNAL, CONSTANT);

PUBLIC DATETIME         FUNCTION __HS_CREATEDATETIMEFROMDM(INTEGER days, INTEGER msecs) __ATTRIBUTES__(EXTERNAL, CONSTANT);


PUBLIC RECORD           FUNCTION __HS_OPENLOCALLOCK(STRING name, INTEGER maxconcurrent, BOOLEAN fail_if_queued) __ATTRIBUTES__(EXTERNAL);
PUBLIC                  MACRO    __HS_CLOSELOCALLOCK(INTEGER lockid) __ATTRIBUTES__(EXTERNAL);
PUBLIC STRING           FUNCTION __HS_GetCurrentGroupID()  __ATTRIBUTES__(EXTERNAL);
PUBLIC BOOLEAN          FUNCTION __INTERNAL_RUNASYNCJSCODE() __ATTRIBUTES__(EXECUTESHARESCRIPT, EXTERNAL);

PUBLIC RECORD           FUNCTION __INTERNAL_DEBUGFUNCTIONPTRTORECORD(FUNCTION PTR fptr) __ATTRIBUTES__(EXTERNAL);

// TCP/IP
PUBLIC INTEGER          FUNCTION __HS_TCPIP_CONNECT(INTEGER connid, STRING ip, INTEGER port, STRING hostname) __ATTRIBUTES__(EXTERNAL);
PUBLIC INTEGER          FUNCTION __HS_TCPIP_FinishConnect(INTEGER connid, BOOLEAN cancel) __ATTRIBUTES__(EXTERNAL);
PUBLIC BOOLEAN          FUNCTION __HS_TCPIP_BIND(INTEGER connid, STRING ip, INTEGER port) __ATTRIBUTES__(EXTERNAL);
PUBLIC                  MACRO    __HS_TCPIP_SETLASTERROR(INTEGER connid, INTEGER errorcode) __ATTRIBUTES__(EXTERNAL);
PUBLIC BOOLEAN          FUNCTION __HS_TCPIP_SETCERT(INTEGER connid, BLOB cert) __ATTRIBUTES__(EXTERNAL);
PUBLIC STRING           FUNCTION __HS_TCPIP_CanonicalizeIP(STRING address, INTEGER networkprefix) __ATTRIBUTES__(EXTERNAL);
PUBLIC BOOLEAN          FUNCTION __HS_TCPIP_SetSecureConnection(INTEGER connectionid, BOOLEAN initiate, STRING ciphersuites, STRING hostname, INTEGER securitylevel, STRING ARRAY ssloptions) __ATTRIBUTES__(EXTERNAL);
PUBLIC INTEGER          FUNCTION __HS_TCPIP_GetSocketTimeout(INTEGER connectionid) __ATTRIBUTES__(EXTERNAL);
PUBLIC STRING           FUNCTION __HS_TCPIP_GetPeerCertificateChain(INTEGER connectionid) __ATTRIBUTES__(EXTERNAL);

PUBLIC                  MACRO    __HS_INTERNAL_AddAsyncContext(OBJECT ctx, INTEGER skipframes) __ATTRIBUTES__(EXTERNAL);
PUBLIC                  MACRO    __HS_INTERNAL_RemoveAsyncContext() __ATTRIBUTES__(EXTERNAL);

//This API in baselibs.cpp invokes Module.emSyscall and acts as a (slow) generic bridge to javascript.
PUBLIC RECORD           FUNCTION __EM_SYSCALL(STRING call, VARIANT data) __ATTRIBUTES__(EXECUTESHARESCRIPT, EXTERNAL);
PUBLIC RECORD           FUNCTION __EM_SYNCSYSCALL(STRING call, VARIANT data) __ATTRIBUTES__(EXECUTESHARESCRIPT, EXTERNAL);

/** Returns the call stack for asynchronous functions
    @topic harescript-core/builtins
    @public
    @loadlib wh::system.whlib
    @return Stack frames, with asynchronous calls marked by adding ' (async)' after the function name
    @cell(string) return.filename Source file name
    @cell(string) return.func Function name
    @cell(integer) return.line Line number
    @cell(integer) return.col Column number
*/
PUBLIC RECORD ARRAY     FUNCTION GetAsyncStackTrace() __ATTRIBUTES__(EXTERNAL, SKIPTRACE);

PUBLIC STATIC OBJECTTYPE __HS_INTERNAL_AsyncContext
<
  MACRO NEW(INTEGER skipelts) __ATTRIBUTES__(SKIPTRACE)
  {
    this->InitState(skipelts);
  }

  MACRO InitState(INTEGER skipelts) __ATTRIBUTES__(EXTERNAL);
>;

/** @short Start coverage profile counting */
PUBLIC MACRO EnableCoverageProfile() __ATTRIBUTES__(EXTERNAL);

/** @short End coverage profile counting */
PUBLIC MACRO DisableCoverageProfile() __ATTRIBUTES__(EXTERNAL);

/** @short Resets coverage profile */
PUBLIC MACRO ResetCoverageProfile() __ATTRIBUTES__(EXTERNAL);

PUBLIC RECORD FUNCTION __INTERNAL_GETCOVERAGEPROFILEDATA() __ATTRIBUTES__(EXTERNAL);

//Callbacks and hooks
PUBLIC FUNCTION PTR __GetHSResourceCall;
PUBLIC FUNCTION PTR __LibraryLoadRemapper;
PUBLIC FUNCTION PTR __UnhandledRejection;
PUBLIC FUNCTION PTR __RejectionHandled;
PUBLIC FUNCTION PTR __TooManyUnhandledRejections;
PUBLIC FUNCTION PTR __OnBeforeTerminateScript;
PUBLIC FUNCTION PTR __OnDecodeObjectMarshalData;

/** @topic harescript-core/datetime
    @public
    @loadlib wh::datetime.whlib
    @short The maximum possible DATETIME value. There is no DATETIME value bigger than this value. */
PUBLIC __CONSTREF DATETIME MAX_DATETIME := __HS_CREATEDATETIMEFROMDM(0x7FFFFFFF, 24*60*60*1000 - 1);

/** @short Generate a random 128bit UFS-encoded ID
    @return A random UFS-encoded 128bit number. Always 22 characters/bytes in length
    @topic harescript-core/crypto
    @public
    @loadlib wh::crypto.whlib
*/
PUBLIC STRING FUNCTION GenerateUFS128BitId() __ATTRIBUTES__(EXTERNAL);

/** Returns the content of an environment variable
    @param varname Name of the variable
    @return Contents of the environment variable
    @topic harescript-core/os
    @public
    @loadlib wh::os.whlib
*/
PUBLIC STRING FUNCTION GetEnvironmentVariable(STRING varname) __ATTRIBUTES__(EXTERNAL);

/** @short Write a string to any output device
    @long This function writes a string to any output device, eg. an open blob-stream, a file or a network connection.
    @topic harescript-core/builtins
    @public
    @loadlib wh::system.whlib
    @param outputid Output device to write the data to. The value 0 writes it to the standard output (the same output as @italic Print)
    @param data The string to write
    @return True if the data was written succesfully, false if an I/O error occured.
    @see %Print, %CreateStream, MakeBlobFromStream
    @example
// Create a blob containing the text 'Hello, World'.
INTEGER newblobid := CreateStream();
PrintTo(newblobid, "Hello, World");
BLOB newblob := MakeBlobFromStream(newblobid);

// Equivalent to Print("<HTML>");
PrintTo(0, "<HTML>");
*/
PUBLIC BOOLEAN FUNCTION PrintTo(INTEGER outputid, STRING data) __ATTRIBUTES__(EXTERNAL);
PUBLIC INTEGER FUNCTION __HS_CreateStream() __ATTRIBUTES__(EXTERNAL);

/** @short Finish writing to a stream and return a blob
    @long End writing of a stream, finalize it, and return it as a blob variable
    @topic harescript-core/os
    @public
    @loadlib wh::files.whlib
    @param streamid File ID, as returned by CreateStream
    @return Blob variable
    @see %CreateStream */
PUBLIC BLOB FUNCTION MakeBlobFromStream(INTEGER streamid) __ATTRIBUTES__(EXTERNAL);

PUBLIC MACRO __HS_SILENTTERMINATE() __ATTRIBUTES__(EXTERNAL, TERMINATES);

/** @short Flush the standard output buffer
    @long If standard output buffering is enabled, flush the buffer, writing
          the buffered data to the standard output.
    @topic harescript-core/os
    @public
    @loadlib wh::os.whlib
    @see %SetOutputBuffering */
PUBLIC MACRO FlushOutputBuffer() __ATTRIBUTES__(EXTERNAL);
PUBLIC MACRO __HS_FATALERROR(INTEGER line, STRING msg1, STRING msg2) __ATTRIBUTES__(EXTERNAL);
PUBLIC STRING FUNCTION __HS_INTERNAL_RESOLVEABSOLUTELIBRARY(STRING base, STRING url) __ATTRIBUTES__(EXTERNAL, EXECUTESHARESCRIPT);
PUBLIC STRING FUNCTION __HS_INTERNAL_TRANSLATELIBRARYPATH(STRING path) __ATTRIBUTES__(EXTERNAL, EXECUTESHARESCRIPT);
PUBLIC RECORD FUNCTION __HS_INTERNAL_GetLibrariesInfo(BOOLEAN only_direct_loaded) __ATTRIBUTES__(EXTERNAL);
PUBLIC RECORD FUNCTION __INTERNAL_DescribeObjectStructure(OBJECT x, BOOLEAN also_internals DEFAULTSTO FALSE) __ATTRIBUTES__(EXTERNAL, SPECIAL);
PUBLIC BOOLEAN FUNCTION __INTERNAL_GetIsObjectTypeStatic(OBJECT x) __ATTRIBUTES__(EXTERNAL);
PUBLIC RECORD FUNCTION __HS_INTERNAL_TESTOBJECTSHAREDREFERENCE(OBJECT x) __ATTRIBUTES__(EXTERNAL);

PUBLIC INTEGER FUNCTION CreateHasher(STRING alg) __ATTRIBUTES__(EXTERNAL);
PUBLIC STRING FUNCTION FinalizeHasher(INTEGER alg) __ATTRIBUTES__(EXTERNAL);
PUBLIC STRING FUNCTION __HS_INTERNAL_CalculateVariableHash(VARIANT variable) __ATTRIBUTES__(EXTERNAL);


PUBLIC STRING FUNCTION Encrypt_Xor(STRING xor_key, STRING data) __ATTRIBUTES__(EXTERNAL, CONSTANT);

PUBLIC MACRO DoFatalError(INTEGER line, STRING msg1, STRING msg2) __ATTRIBUTES__(SKIPTRACE, TERMINATES)
{
  TRY
  {
    IF(__OnBeforeTerminateScript != DEFAULT FUNCTION PTR)
      __OnBeforeTerminateScript(); //coverage analysis hook

    FlushOutputBuffer();
    __HS_FATALERROR(line, msg1, msg2);
  }
  FINALLY
  {
    __HS_SILENTTERMINATE();
  }
}

/** @topic harescript-core/datetime
    @public
    @loadlib wh::datetime.whlib
    @short Create a datetime from a date string
    @long This function decodes various internet date formats (RFC822, RFC850, RFC1123, ISO8601)
    @param datestring Date string to decode
    @return The decoded datetime, in GMT, or a default datetime if the date could not be decoded */
PUBLIC DATETIME FUNCTION MakeDateFromText(STRING datestring) __ATTRIBUTES__(EXTERNAL, CONSTANT);


/** @short Check if a file path is safe (for use in portable WebHare scripts)
    @long This function rejects invalid UTF-8, spaces at the start of a filename, filenames
          ending in any combination of spaces/dots, and filenames containing any of the characters
          \, :, *, ?, ", <, >, |
    @param path Path to check
    @param accept_slashes Whether to accept forward slashes
    @return True if the filename was accepted */
PUBLIC BOOLEAN FUNCTION IsSafeFilePath(STRING path, BOOLEAN accept_slashes) __ATTRIBUTES__ (CONSTANT, EXTERNAL);

/** @short Open and decompress a blob
    @param indata Blob to read
    @param fileformat File format to read (either 'GZIP', 'ZLIB', 'ZLIBRAW' or 'ZLIBRAW:filelength')
    @return The stream handle, usable by ReadFrom and ReadLineFrom
    @see #CloseZlibDecompressor
    @topic file-formats/archives
    @loadlib wh::filetypes/archiving.whlib
    @public
*/
PUBLIC INTEGER FUNCTION OpenBlobAsDecompressingStream(BLOB indata, STRING fileformat) __ATTRIBUTES__(EXTERNAL);

/** Close a decompressing stream
    @param compressor Decompressing stream
    @see #OpenBlobAsDecompressingStream
    @topic file-formats/archives
    @public
    @loadlib wh::filetypes/archiving.whlib
*/
PUBLIC MACRO CloseZlibDecompressor(INTEGER compressor) __ATTRIBUTES__(EXTERNAL);

/** Create a zlib compressor (using deflate)
    @param outstream Stream to write to
    @param format Format of compressed data
       - "GZIP": Add GZIP headers and calculate CRC32
       - "RAW": Output raw data
       - "ZIP": Output raw data and calculate CRC32
    @param compressionlevel Compression level (1-9)
    @return Id of the stream the uncompressed data can be printed to
    @see #CloseZlibCompressor
    @topic file-formats/archives
    @public
    @loadlib wh::filetypes/archiving.whlib
*/
PUBLIC INTEGER FUNCTION CreateZlibCompressor(INTEGER outstream, STRING format, INTEGER compressionlevel) __ATTRIBUTES__(EXTERNAL);

/** Close a zlib compressor
    @param compressor Compressor stream id
    @return Compression result
    @cell(string) return.crc32 CRC32 hash of the uncompressed data (only when format is "GZIP" or "ZIP")
    @see #CreateZlibCompressor
    @topic file-formats/archives
    @public
    @loadlib wh::filetypes/archiving.whlib
*/
PUBLIC RECORD FUNCTION CloseZlibCompressor(INTEGER compressor) __ATTRIBUTES__(EXTERNAL);

/** @short   Returns a string representation of an integer
    @long    ToString creates a string representation of the specified integer in the specified base (radix)

             A base of 10 indicates that the string should use decimal numbers, 8 octal, 16 hexadecimal, and so on.
             Note: you don't need to use the ToString function to merge integers to strings.

    @param   val Value to convert
    @param   radix Number system used in the string, ranging from 2 to 36. This parameter is optional and defaults to 10 (decimal system)
    @return  The number converted to a string value in the requested number system
    @see     ToInteger, ToInteger64
    @topic   harescript-core/builtins
    @public
    @loadlib wh::system.whlib
    @example
// returns "9"
STRING example1 := ToString(9, 10);

// returns "11"
STRING example2 := ToString(11);

// returns "12"
STRING example3 := ToString(10, 8);

// returns "ff00"
STRING example4 := ToString(65280, 16);
*/
PUBLIC STRING FUNCTION ToString(INTEGER64 val, INTEGER radix DEFAULTSTO 10) __ATTRIBUTES__(EXTERNAL, CONSTANT);

/** @short   Returns an integer representation of a string
    @long    ToInteger parses the string, and attempts to return an integer of the specified base (radix).
             If the function failed to parse the specified string, it will return @italic defaultvalue

             A base of 10 indicates that a decimal number should be converted, 8 octal, 16 hexadecimal, and so on.
    @param   str String to convert
    @param   defaultvalue Value to return if 'str' is not a valid number in the specified number system
    @param   radix Number system used in the string, ranging from 2 to 36. If the base is out of range,
             defaultvalue is returned. This parameter is optional and defaults to 10 (decimal system)
    @return  The number in the passed string as an integer, or defaultvalue if the conversion failed.
    @see     ToString, ToInteger64
    @topic   harescript-core/builtins
    @public
    @loadlib wh::system.whlib
    @example
// The following examples all return 15
INTEGER example1 := ToInteger("15", -1);
INTEGER example2 := ToInteger("15", -1, 10);
INTEGER example3 := ToInteger("F", -1, 16);
INTEGER example4 := ToInteger("1111", -1, 2);

// The following faulty strings all return -1
INTEGER example5 := ToInteger("15,99", -1);
INTEGER example6 := ToInteger("5*3", -1, 10);
INTEGER example7 := ToInteger("FFFFFFFFFF", -1, 16);
INTEGER example8 := ToInteger("22", -1, 2);
*/
PUBLIC INTEGER FUNCTION ToInteger(STRING str, INTEGER defaultvalue, INTEGER radix DEFAULTSTO 10) __ATTRIBUTES__(EXTERNAL, CONSTANT);

/** @short   Returns an integer64 representation of a string
    @long    ToInteger64 parses the string, and attempts to return an integer of the specified base (radix).
             If the function failed to parse the specified string, it will return @italic defaultvalue

             A base of 10 indicates that a decimal number should be converted, 8 octal, 16 hexadecimal, and so on.
    @param   str String to convert
    @param   defaultvalue Value to return if 'str' is not a valid number in the specified number system
    @param   radix Number system used in the string, ranging from 2 to 36. If the base is out of range,
             defaultvalue is returned. This parameter is optional and defaults to 10 (decimal system)
    @return  The number in the passed string as an integer64, or defaultvalue if the conversion failed.
    @see     ToString, ToInteger
    @topic   harescript-core/builtins
    @public
    @loadlib wh::system.whlib
    @example
// The following examples all return 15
INTEGER64 example1 := ToInteger64("15", -1);
INTEGER64 example2 := ToInteger64("15", -1, 10);
INTEGER64 example3 := ToInteger64("F", -1, 16);
INTEGER64 example4 := ToInteger64("1111", -1, 2);

// The following faulty strings all return -1
INTEGER64 example5 := ToInteger64("15,99", -1);
INTEGER64 example6 := ToInteger64("5*3", -1, 10);
INTEGER64 example7 := ToInteger64("FFFFFFFFFFFFFFFFF", -1, 16);
INTEGER64 example8 := ToInteger64("22", -1, 2);
*/
PUBLIC INTEGER64 FUNCTION ToInteger64(STRING str, INTEGER64 defaultvalue, INTEGER radix DEFAULTSTO 10) __ATTRIBUTES__(EXTERNAL, CONSTANT);



/** @short Returns the length of an variable.
    @long This function returns the length of an variable. The variable can be of a STRING, BLOB or any ARRAY type.
          Its unit of measurement depends on the type of the variable.
    @param obj The variable whose length should be measured.
    @return The function returns the number of bytes in the STRING, number of elements in the
            ARRAY or the length of a BLOB in bytes.
    @see wh::system.whlib#UCLength
    @topic   harescript-core/builtins
    @public
    @loadlib wh::system.whlib
    @example
// returns 6
INTEGER example1 := Length("abcdef");

// returns 8 (the euro character consists of three bytes)
INTEGER example2 := Length("€ 2,50");

// returns 6 (the euro character is one character in UNICODE(UTF-8))
INTEGER example2 := UCLength("€ 2,50");

// returns 4
INTEGER ARRAY intarray := [1, 2, 3, 4];
INTEGER example3 := Length(intarray);
*/
PUBLIC INTEGER Function Length(VARIANT obj) __ATTRIBUTES__(EXTERNAL, CONSTANT);


/** @short Returns the length of an variable as an integer64.
    @long This function returns the length of an variable. It supports all the types the Length function does, but is mostly useful for Blobs larger than 2GB
    @param obj The variable whose length should be measured.
    @return The function returns the number of bytes in the STRING, number of elements in the ARRAY or the length of a BLOB in bytes.
    @topic   harescript-core/builtins
    @public
    @loadlib wh::system.whlib
    @see wh::system.whlib#UCLength
    @example
INTEGER64 dvdlength := Length64(GetDiskResource("/tmp/centos-6.iso"));
*/
PUBLIC INTEGER64 Function Length64(VARIANT obj) __ATTRIBUTES__(EXTERNAL, CONSTANT);

/** @short   Returns the left part of a string.
    @long    Left returns the first 'n' bytes from a string.
             If a string contains less than the requested number of bytes, the entire string is returned.
             When manipulating UTF-8 strings it is recommended that you use the function UCLeft because UTF-8
             characters can be made up from more bytes and the result is therefore unpredictable.
    @param   text String to return a part from
    @param   numbytes Number of bytes to return
    @return  The requested part of the string
    @see     wh::system.whlib#UCLeft Substring Right wh::system.whlib#UCRight
    @topic   harescript-core/builtins
    @public
    @loadlib wh::system.whlib
    @example
// returns 'ab'
STRING example1 := Left("abcdef", 2);

// returns '€u'
STRING example2 := Left("€uro", 4);

// returns '€uro'
STRING example3 := UCLeft("€uro", 4)

// returns the entire string, e.g 'abcdef'
STRING example4 := Left("abcdef", 100);

// returns an empty string
STRING example5 := Left("abcdef", -10);
*/
PUBLIC STRING FUNCTION Left(STRING text, INTEGER numbytes) __ATTRIBUTES__(EXTERNAL, CONSTANT);

/** @short   Returns the right part of a string.
    @long    Right returns the last 'n' bytes from a string.
             If a string contains less than the requested number of bytes, the entire string is returned.
             When manipulating UTF-8 strings it is recommended that you use the function UCRight because UTF-8
             characters can be made up from more bytes and the result is therefore unpredictable.
    @param   text String to return a part from
    @param   numbytes Number of bytes to return
    @return  The requested part of the string
    @see     wh::system.whlib#UCRight Substring Left wh::system.whlib#UCLeft
    @topic   harescript-core/builtins
    @public
    @loadlib wh::system.whlib
    @example
// returns 'ef'
STRING example1 := Right("abcdef", 2);

// returns 'ro€'
STRING example2 := Right("euro€", 5);

// returns 'euro€'
STRING example3 := UCRight("euro€", 5);

// returns the entire string, e.g 'abcdef'
STRING example4 := Right("abcdef", 100);

// returns an empty string
STRING example5 := Right("abcdef", -10);
*/
PUBLIC STRING FUNCTION Right(STRING text, INTEGER numbytes) __ATTRIBUTES__(EXTERNAL, CONSTANT);

/** @short   Returns part of a string.
    @long    Substring returns the part of string specified by the start and length parameters.
             If the string after the start position contains less than the requested number of bytes, the whole
             string after the start position is returned.
             If the length parameter is omitted, the portion of string beginning at start and ending at the
             end of the string is returned
             When manipulating UTF-8 strings it is recommended that you use the function UCSubstring because
             UTF-8 characters can be made up from more bytes and the result is therefore unpredictable.
    @param   text String to return a part from
    @param   start Starting byte position
    @param   numbytes Number of bytes to return. When omitted, the portion from start until the end of the string is returned.
    @return  The requested part of the string
    @see     wh::system/whlib#UCSubstring Left Right wh::system/whlib#UCLeft wh::system/whlib#UCRight
    @topic   harescript-core/builtins
    @public
    @loadlib wh::system.whlib
    @example
// returns 'bcde'
STRING example1 := Substring("abcdef", 1, 4);

// returns '€u'
STRING example2 := Substring("euro€uro", 4, 4);

// returns '€uro'
STRING example3 := UCSubstring("euro€uro", 4, 4);

// returns an empty string
STRING example4 := Substring("abcdef", 100, 100);

// returns an empty string
STRING example5 := Substring("abcdef", -10, -20);
*/
PUBLIC STRING FUNCTION Substring(STRING text, INTEGER start, INTEGER numbytes DEFAULTSTO 2147483647) __ATTRIBUTES__(EXTERNAL, CONSTANT);

/** @short   Find the first occurrence of a substring
    @long    Searchsubstring does a case-sensitive search in a string for the first occurence of a substring.
    @param   text String to search in
    @param   search String to search for
    @param   start The byte position to start searching. Note that characters in UNICODE(UTF-8) are sometimes
             made up from more bytes.
    @return  Starting byte position of the first occurence, or -1 if the string was not found
    @see     SearchLastSubstring wh::system/whlib#UCSearchSubstring wh::system/whlib#UCSearchLastSubstring
    @topic   harescript-core/builtins
    @public
    @loadlib wh::system.whlib
    @example
// returns 2
INTEGER example1 := SearchSubstring("abcdef", "c");

// returns 0
INTEGER example2 := SearchSubstring("ababab", "ab");

// returns -1
INTEGER example3 := SearchSubstring("abcdef", "g");

// returns -1
INTEGER example4 := SearchSubstring("abcdef", "A");

// returns 2
INTEGER example5 := SearchSubstring("ababab", "ab", 1);

// returns -1
INTEGER example6 := SearchSubstring("ababab", "ab", 5);
*/
PUBLIC INTEGER FUNCTION SearchSubstring(STRING text, STRING search, INTEGER start DEFAULTSTO 0) __ATTRIBUTES__(EXTERNAL, CONSTANT);

/** @short   Find the last occurrence of a substring
    @long    Searchsubstring does a case-sensitive search in a string for the last occurence of a substring.
    @param   text String to search in
    @param   search String to search for
    @param   start The byte position to start searching. Note that characters in UNICODE(UTF-8) are sometimes
             made up from more bytes.
    @return  Starting byte position of the last occurence, or -1 if the string was not found
    @see     SearchSubstring wh::system/whlib#UCSearchSubstring wh::system/whlib#UCSearchLastSubString
    @topic   harescript-core/builtins
    @public
    @loadlib wh::system.whlib
    @example
// returns 3
INTEGER example1 := SearchLastSubstring("aaaa", "a");

// returns 4
INTEGER example2 := SearchLastSubstring("ababab", "ab");

// returns -1
INTEGER example3 := SearchLastSubstring("abcdef", "g");

// returns -1
INTEGER example4 := SearchLastSubstring("abcdef", "A");

// returns 2
INTEGER example5 := SearchLastSubstring("ababab", "ab", 4);

// returns -1
INTEGER example6 := SearchLastSubstring("ababab", "ab", 0);
*/
PUBLIC INTEGER FUNCTION SearchLastSubstring(STRING text, STRING search, INTEGER start DEFAULTSTO 2147483647) __ATTRIBUTES__(EXTERNAL, CONSTANT);
