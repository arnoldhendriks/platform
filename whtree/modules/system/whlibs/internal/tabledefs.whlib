<?wh

STRING FUNCTION GetHarescriptType(STRING schemaname, STRING tablename, STRING columnname, STRING dbase_type)
{
  IF(dbase_type LIKE "varchar(*") //postgresql 'varchar(nnn)'
    RETURN "STRING";

  SWITCH(dbase_type)
  {
    CASE "_text","name","text","char","bytea" //FIXME bytea should have the BINARY annotation
    {
      RETURN "STRING";
    }
    CASE "numeric"
    {
      RETURN "MONEY";
    }
    CASE "float4","_float4", "float8"
    {
      RETURN "FLOAT";
    }
    CASE "oid", "int2", "int4", "regproc"
    {
      RETURN "INTEGER";
    }
    CASE "int8"
    {
      RETURN "INTEGER64";
    }
    CASE "timestamp"
    {
      RETURN "DATETIME";
    }
    CASE "webhare_internal.webhare_blob"
    {
      RETURN "BLOB";
    }
    CASE "bool"
    {
      RETURN "BOOLEAN";
    }
    DEFAULT
    {
      RETURN "";
    }
  }
}

STRING FUNCTION GetDefStmt(OBJECT trans, STRING table_schema, RECORD tabledef, RECORD ARRAY columns, STRING indent)
{
  STRING hs_tabledef;

  columns :=
    SELECT *
      FROM columns
  ORDER BY column_name != tabledef.primary_key_name, column_name;

  IF (trans->type = "postgresql" AND table_schema = "system")
  {
    IF (tabledef.table_name = "sites")
      INSERT [ column_name := "webroot", data_type := "text" ] INTO columns AT END;
    ELSE IF (tabledef.table_name = "fs_objects")
    {
      columns := columns CONCAT
          [ [ column_name := "fullpath", data_type := "text" ]
          , [ column_name := "highestparent", data_type := "int4", referenced_table_name := "" ]
          , [ column_name := "indexurl", data_type := "text" ]
          , [ column_name := "isactive", data_type := "bool" ]
          , [ column_name := "publish", data_type := "bool" ]
          , [ column_name := "url", data_type := "text" ]
          , [ column_name := "whfspath", data_type := "text" ]
          ];
    }
  }
  // renames are not visible from the column definitions
  IF (table_schema = "system" AND tabledef.table_name = "sites")
  {
    tabledef.columns := columns CONCAT
        [ [ column_name := "id", data_type := "int4", rename := "root" ] ];
  }
  IF (table_schema = "system" AND tabledef.table_name = "fs_objects")
  {
    columns := columns CONCAT
        [ [ column_name := "highestparent", data_type := "int4", rename := "parentsite", referenced_table_name := ""  ]
        , [ column_name := "indexurl", data_type := "text", rename := "link" ]
        , [ column_name := "url", data_type := "text", rename := "objecturl" ]
        ];
  }

  FOREVERY (RECORD col FROM columns)
  {
    STRING hstype := GetHarescriptType(table_schema, tabledef.table_name, col.column_name, col.data_type);
    IF(hstype = "")
      CONTINUE;

    IF(hs_tabledef != "")
      hs_tabledef := hs_tabledef || "\n" || indent || ", ";

    hs_tabledef := hs_tabledef ||  (hstype || ' "' || EncodeJava(col.column_name) || '"');
    IF (CellExists(col, "rename"))
      hs_tabledef := hs_tabledef || ` AS "${EncodeJava(col.rename)}"`;

    IF ( hstype IN ["INTEGER","INTEGER64"] AND (col.referenced_table_name != "" OR col.column_name = tabledef.primary_key_name))
      hs_tabledef := hs_tabledef || " NULL := 0" ;
    IF (ToLowercase(col.data_type) = "bytea")
      hs_tabledef := hs_tabledef || " __ATTRIBUTES__(BINARY)" ;
  }
  IF (tabledef.primary_key_name != "")
    hs_tabledef := hs_tabledef || `\n${indent}; KEY "${EncodeJava(tabledef.primary_key_name)}"`;

  RETURN hs_tabledef;
}


PUBLIC STRING FUNCTION GetHarescriptSchemaDefinition(OBJECT trans, STRING schemaname)
{
  BOOLEAN ispublicschema := ToLowercase(schemaname)="public";
  STRING hs_schemadef;
  IF(NOT ispublicschema)
    hs_schemadef := 'PUBLIC SCHEMA\n  < ';

  FOREVERY(RECORD tablerec FROM SELECT * FROM trans->GetTableListing(schemaname) ORDER BY table_name)
  {
    IF(#tablerec>0)
      hs_schemadef := `${hs_schemadef}\n  ${ispublicschema ? ";" : ","} `;

    STRING code_tablename;
    IF(ispublicschema)
      code_tablename := ToLowercase(tablerec.table_name);
    ELSE
      code_tablename := '"' || EncodeJava(ToLowercase(tablerec.table_name)) || '"';

    RECORD ARRAY columns := trans->GetColumnListing(schemaname, tablerec.table_name);
    hs_schemadef := hs_schemadef || "TABLE\n    < " || GetDefStmt(trans, schemaname, tablerec, columns,"    ") || `\n    > ${code_tablename}`;
  }
  hs_schemadef := hs_schemadef || "\n";
  IF(ispublicschema)
    hs_schemadef := hs_schemadef || ";\n";
  ELSE
    hs_schemadef := hs_schemadef || "  > " || ToLowercase(schemaname) || ";\n";
  RETURN hs_schemadef;
}
