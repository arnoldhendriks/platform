<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::files.whlib";
LOADLIB "wh::witty.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/testframework.whlib";

LOADLIB "mod::tollium/lib/gettid.whlib";
//LOADLIB "mod::tollium/lib/gettid2.whlib"; //when debugging/developing you might want to copypaste gettid.whlib and develop on your local instance
LOADLIB "mod::tollium/lib/testframework.whlib";

MACRO TidCompilerTest()
{
  //Do as much tests as possible before we install the module...
  TestEq("Dit is bold\nvolgende\nregel", GetTid("webhare_testsuite:test.richtext"));
  TestEq("Dit is <b>bold</b><br />volgende<br />regel", GetHTMLTid("webhare_testsuite:test.richtext"));

  TestEq("param:P1", GetTid("webhare_testsuite:test.richtext_params", "P1", "P2", "P3", "P4"));
  TestEq("param:<i>P1</i>", GetHTMLTid("webhare_testsuite:test.richtext_params", "P1", "P2", "P3", "P4"));

  TestEq("", GetTid(""));
  TestEq("", GetTid(":"));
  TestEq("One", GetTid(":One"));
  TestEq("A&B", GetTid(":A&B"));
  TestEq("A&#38;B", GetHTMLTid(":A&B"));
  TestEq("One", GetTid("webhare_testsuite:aaa.limitlanguage_en.one"));
  TestEq("One", GetTid("webhare_testsuite:AAa.Limitlanguage_en.One"));
  TestEq("One", GetTid("Webhare_Testsuite:aaa.limitlanguage_en.one"));
  TestEq("One", GetTid("webhare_testsuite:aaa.limitlanguage en.one"), "spaces -> underscore");
  TestEq("(cannot find text: webhare testsuite:aaa.limitlanguage_en.one)", GetTid("webhare testsuite:aaa.limitlanguage en.one"), "but should NOT be rewriting module names to underscore");
  TestEq("(cannot find text: webhare_testsuite:aaa.limitlanguage_nl.isnlonly)", GetTIDForLanguage("en", "webhare_testsuite:aaa.limitlanguage_nl.isnlonly"));
  TestEq("IsNLOnly", GetTIDForLanguage("nl", "webhare_testsuite:aaa.limitlanguage_nl.isnlonly"));

  TestEq("Ifparam: p1!=a p2!=b", GetTID("webhare_testsuite:test.ifparam"));
  TestEq("Ifparam: p1=a p2!=b", GetTID("webhare_testsuite:test.ifparam","a"));
  TestEq("Ifparam: p1=a p2!=b", GetTID("webhare_testsuite:test.ifparam","A"));
  TestEq("Ifparam: p1=a p2=b", GetTID("webhare_testsuite:test.ifparam","A","B"));
  TestEq('Use this link to choose a new password', GetHTMLTID("webhare_testsuite:test.hrefparam"));
  TestEq('Use <a href="http://www.webhare.net/">this link</a> to choose a new password', GetHTMLTID("webhare_testsuite:test.hrefparam","http://www.webhare.net/"));
  TestEq('Use <a href="x-fallback:">this link</a> to choose a new password', GetHTMLTID("webhare_testsuite:test.hrefparam2"));
  TestEq('Use <a href="x-fallback:">this link</a> to choose a new password', GetHTMLTID("webhare_testsuite:test.hrefparam2", "", "unused"));
  TestEq('Use <a href="x-test:a&#38;b">this link</a> to choose a new password', GetHTMLTID("webhare_testsuite:test.hrefparam2","x-test:a&b"));

  //Builtins
  TestEq(GetLanguageDatetimeStrings("nl"), GetTidForLanguage("nl","tollium:tilde.locale.datetimestrings"));
  TestEq(GetLanguageDatetimeStrings("nl"), GetTidForLanguage("nl","~locale.datetimestrings"));

  //Common tids
  TestEq("Close", GetTid("~close"));
  TestEq("Add", GetTidForLanguage("en","~add"));
  TestEq("Toevoegen", GetTidForLanguage("nl","~add"));
  TestEq("Nederlands", GetTidForLanguage("nl","tollium:common.languages.nl")); //make sure we're not suddenly requiring nl_NL once these are bound to i18n libraries...

  TestEq(STRING[], GetTIDListForLanguage("en", "webhare_testsuite:aaa"));
  TestEq(STRING[], GetTIDListForLanguage("en", "webhare_testsuite:aaa.limitlanguage_nl"));
  TestEq(STRING['webhare_testsuite:aaa.limitlanguage_en.one', 'webhare_testsuite:aaa.limitlanguage_en.two'], GetTIDListForLanguage("en", "webhare_testsuite:aaa.limitlanguage_en"));
  TestEq(["webhare_testsuite:aaa.limitlanguage_nl.isnlonly"], GetTIDListForLanguage("nl", "webhare_testsuite:aaa.limitlanguage_nl"));
}

MACRO SetupTestModule_LanguageFiles()
{
  testfw->SetupTestModule();

  OBJECT srcwitty := LoadWittyLibrary("mod::webhare_testsuite/lib/system/testmodule.witty", "TEXT");
  StoreDiskFile(GetModuleInstallationRoot(testfw_testmodule) || "language/default.xml", srcwitty->RunComponentToBlob("language_default", CELL[]), [ overwrite := TRUE ]);
  StoreDiskFile(GetModuleInstallationRoot(testfw_testmodule) || "language/nl.xml", srcwitty->RunComponentToBlob("language_nl", CELL[]), [ overwrite := TRUE ]);
  StoreDiskFile(GetModuleInstallationRoot(testfw_testmodule) || "language/xx.xml", srcwitty->RunComponentToBlob("language_xx", CELL[]), [ overwrite := TRUE ]);
  StoreDiskFile(GetModuleInstallationRoot(testfw_testmodule) || "language/xy.xml", srcwitty->RunComponentToBlob("language_xy", CELL[ modifiedtext := "v1" ]), [ overwrite := TRUE ]);
}

MACRO GetTidTest()
{
  SetTidLanguage("en");
  TestEq('(missing module name in tid: notifications.messagefromthefuture)', GetTid("notifications.messagefromthefuture"));
  TestEq('(missing module name in tid: notifications.messagefromthefuture)', GetHTMLTid("notifications.messagefromthefuture"));
  TestEq("A message with underscores", GetTid("webhare_testsuite_temp:notifications.message_from_the_future"));
  TestEq("A message with underscores", GetTid("webhare_testsuite_temp:notifications.message from the future"));
  TestEq('(cannot find text: webhare testsuite temp:notifications.message_from_the_future)', GetTid("webhare testsuite temp:notifications.message from the future"));

  TestEq("A message", GetTid("webhare_testsuite_temp:notifications.messagefromthefuture"));
  TestEq("A <i>message</i>", GetHTMLTid("webhare_testsuite_temp:notifications.messagefromthefuture"));
  TestEq("An <a href=\"http://www.example.org/\">example</a> link", GetHTMLTid("webhare_testsuite_temp:notifications.linkfromthefuture"));
  TestEq("A <a href=\"https://www.webhare.com/\">parametered</a> link", GetHTMLTid("webhare_testsuite_temp:notifications.linkparameter","https://www.webhare.com/"));
}

MACRO FallbackLanguageTest()
{
  //Create a temporary module
  TestEq("A message", GetTid("webhare_testsuite_temp:notifications.messagefromthefuture"));
  TestEq("A text", GetTid("webhare_testsuite_temp:testfallback.sub.text"));
  TestEq("Another text", GetTid("webhare_testsuite_temp:testfallback.anothertext"));
  TestEq("Can't find me from NL", GetTid("webhare_testsuite_temp:testfallbackdefaultrecursion.text"));
  SetTidLanguage("xxx");
  TestEq("A message", GetTid("webhare_testsuite_temp:notifications.messagefromthefuture"));
  TestEq("A text", GetTid("webhare_testsuite_temp:testfallback.sub.text"));
  TestEq("Another text", GetTid("webhare_testsuite_temp:testfallback.anothertext"));
  SetTidLanguage("nl");
  TestEq("(cannot find text: webhare_testsuite_temp:notifications.messagefromthefuture)", GetTid("webhare_testsuite_temp:notifications.messagefromthefuture"));
  TestEq("Can't find me from NL", GetTid("webhare_testsuite_temp:testfallbackdefaultrecursion.text"));

  SetTidLanguage("XY");
  TestEq("Changed this text!", GetTid("webhare_testsuite_temp:testfallback.anothertext"));
  SetTidLanguage("xy");
  TestEq("Changed this text!", GetTid("webhare_testsuite_temp:testfallback.anothertext"));
  TestEq("A message", GetTid("webhare_testsuite_temp:notifications.messagefromthefuture"));

  SetTidLanguage("en");
}

ASYNC MACRO ScreenTidTest()
{
  SetTidLanguage("debug");
  RECORD rec := AWAIT ExpectScreenChange(+1, PTR GetTestController()->RunScreen("mod::webhare_testsuite/screens/tests/tolliumtids.xml#screen"));

  TestEQ("{webhare_testsuite:testtids.screen.screen}", topscreen->Test());
  TestEQ("{webhare_testsuite:testtids.fragment.fragment}", TT("fragmentcomp")->Test());
  TestEQ("{webhare_testsuite:testtids.fragment.fragment}", topscreen->dynfrag->Test());

  RECORD ext := TT("tabs")->LoadTabsExtension("mod::webhare_testsuite/screens/tests/tolliumtids.xml#tabsextension");
  TestEQ("{webhare_testsuite:testtids.tabsextension.tabsextension}", ext.extension->Test());

  AWAIT ExpectScreenChange(-1, PTR topscreen->TolliumExecuteCancel());
  AWAIT rec.expectcallreturn();
}
RunTestFramework([ PTR TidCompilerTest
                 , PTR SetupTestModule_LanguageFiles
                 , PTR GetTidTest
                 , PTR FallbackLanguageTest
                 , PTR ScreenTidTest
                 ]);
