<?wh
LOADLIB "wh::datetime.whlib";
LOADLIB "wh::datetime.whlib";
LOADLIB "mod::system/lib/testframework.whlib";
LOADLIB "mod::tollium/lib/testframework.whlib";

MACRO InputValidations()
{
  OBJECT scr := GetTestController()->LoadScreen("webhare_testsuite:tests/basecomponents.inputvalidations");
  OBJECT work;

  FOREVERY(STRING check FROM ["url"])
  {
    scr->textedit->validationchecks := [ STRING (check) ];
    FOREVERY(RECORD mask FROM [ [ value := ":www.b-lex.nl",               acceptin := DEFAULT STRING ARRAY ]
                              , [ value := "http:www.b-lex.nl",           acceptin := DEFAULT STRING ARRAY ]
                              , [ value := "http:/www.wilminktheater.nl", acceptin := DEFAULT STRING ARRAY ]
                              , [ value := "//www.wilminktheater.nl",     acceptin := DEFAULT STRING ARRAY ]
                              , [ value := "http://www.b-lex.nl/",        acceptin := ["url"  ] ]
                              , [ value := "http://www.b-lex.nl/" || RepeatText("1", 2001-20), acceptin := DEFAULT STRING ARRAY ]
                              ])
    {
      scr->textedit->value := mask.value;
      work := scr->BeginFeedback();
      TestEq(check NOT IN mask.acceptin, work->HasFailed());

      work->Cancel();
    }
  }

  STRING testlink := "https://eur02.safelinks.protection.outlook.com/?url=https%3A%2F%2Fyoutu.be%2Fd8dABsc6jEw&data=05%7C01%7Cx.xxxx%40example.nl%7Cc3eee60137244a5c5f1508db417e04f4%7C723246a1c3f543c5acdc43adb404ac4d%7C0%7C0%7C638175781730563834%7CUnknown%7CTWFpbGZsb3d8eyJWIjoiMC4wLjAwMDAiLCJQIjoiV2luMzIiLCJBTiI6Ik1haWwiLCJXVCI6Mn0%3D%7C3000%7C%7C%7C&sdata=TJpJnhP%2FsBbHijbOXUZuF0udVt8O4Qx46G%2BdkXJ1ARs%3D&reserved=0";;
  scr->textedit->validationchecks := STRING[];
  scr->textedit->value := testlink;
  TestEq(testlink, scr->textedit->value);
  TestEq(testlink, scr->textedit->textvalue);
  scr->textedit->validationchecks := STRING["url"];
  TestEq("https://youtu.be/d8dABsc6jEw", scr->textedit->value);
  TestEq(testlink, scr->textedit->textvalue);
  scr->textedit->validationchecks := STRING["http-url"];
  TestEq("https://youtu.be/d8dABsc6jEw",scr->textedit->value);
  scr->textedit->validationchecks := STRING["email"];
  TestEq(testlink, scr->textedit->value);
}

MACRO SetValue(OBJECT prop, VARIANT toset)
{
  prop->value := toset;
}

MACRO DateTimeClamping() //test precision clamping
{
  OBJECT scr := GetTestController()->LoadScreen("webhare_testsuite:tests/basecomponents.datetimetest");

  TestEq(DEFAULT DATETIME, scr->bigdt->value, "bigdt = MAX_DATETIME"); //was never initted to max_datetime
  SetValue(scr->bigdt, MakeDatetime(1234567,8,13,23,59,57));
  TestEq(MakeDatetime(9999,8,13,23,59,57), scr->bigdt->value);

  TestThrowslike('Cannot represent*MAX_DATETIME*',PTR SetValue(scr->bigdt, MAX_DATETIME)); //only acceptable if value = max_datetime

  TestEq(MakeDate(2009,8,13), scr->da1->value);
  TestEq(MakeDateTime(2009,8,13,8,9,0), scr->dt1->value);
  TestEq(MakeDateTime(2009,8,13,8,9,18), scr->dt2->value);
  TestEq(AddTimeToDate(189,MakeDateTime(2009,8,13,8,9,18)), scr->dt3->value);

  TestEq(MakeDateTime(2009,8,13,8,9,0), scr->ti1->value);
  TestEq(MakeDateTime(2009,8,13,8,9,18), scr->ti2->value);
  TestEq(AddTimeToDate(189,MakeDateTime(2009,8,13,8,9,18)), scr->ti3->value);

  TestEq( (8*60 + 9) * 60 * 1000, scr->ti4->value);
  TestEq( ((8*60 + 9) * 60 + 18) * 1000, scr->ti5->value);
  TestEq( ((8*60 + 9) * 60 + 18) * 1000 + 189, scr->ti6->value);

  TestEq(MAX_DATETIME, scr->maxdt->value, "maxdt != MAX_DATETIME");
}

MACRO DateTimeLocalTZValue()
{
  testfw->SetTestUser("sysop");
  OBJECT scr := GetTestController()->LoadScreen("webhare_testsuite:tests/basecomponents.datetimetest");

  TestEq(DEFAULT DATETIME, scr->bigdt->value, "bigdt = MAX_DATETIME"); //was never initted to max_datetime
  SetValue(scr->bigdt, MakeDatetime(2020,5,11,9,0,0));
  TestEq(MakeDatetime(2020,5,11,9,0,0), scr->bigdt->value);
  TestEq(MakeDatetime(2020,5,11,9,0,0), scr->bigdt->localtzvalue);
  scr->bigdt->storeutc := TRUE;
  SetValue(scr->bigdt, MakeDatetime(2020,5,11,9,0,0));
  TestEq(MakeDatetime(2020,5,11,11,0,0), scr->bigdt->localtzvalue);
  scr->bigdt->localtzvalue := MakeDatetime(2020,5,11,12,0,0);
  TestEq(MakeDatetime(2020,5,11,12,0,0), scr->bigdt->localtzvalue);
  TestEq(MakeDatetime(2020,5,11,10,0,0), scr->bigdt->value);
  scr->bigdt->value := MakeDatetime(2020,5,11,9,0,0);
  TestEq(MakeDatetime(2020,5,11,11,0,0), scr->bigdt->localtzvalue);

  //test storeutc=true safety for <datetime> fields
  //note that localtzvalue is also the value transmitted to tollium, so we use that to verify what the user sees
  scr->dt1->type := "date";
  scr->dt1->storeutc := FALSE;
  scr->dt1->value := MakeDatetime(2024,5,11,12,0,0);
  TestEq(MakeDatetime(2024,5,11,0,0,0), scr->dt1->value, 'Datetime clamped back to day due to type=day');
  TestEq(MakeDatetime(2024,5,11,0,0,0), scr->dt1->localtzvalue, 'A date does not change its local value for timezones');
  scr->dt1->storeutc := TRUE;
  TestEq(MakeDatetime(2024,5,11,0,0,0), scr->dt1->value, 'Changing a type=date to storeutc=true does not affect the value');
  TestEq(MakeDatetime(2024,5,11,0,0,0), scr->dt1->localtzvalue, 'Should still match ->value');
  scr->dt1->value := MakeDatetime(2024,5,11,12,0,0);
  TestEq(MakeDatetime(2024,5,11,0,0,0), scr->dt1->value, 'Datetime clamped back to day due to type=day, storeutc=true is irrelevant');
  scr->dt1->TolliumWeb_FormUpdate("2024-06-13"); //Mocks an update from the frontend
  TestEq(MakeDatetime(2024,6,13,0,0,0), scr->dt1->value, 'No translating the value we got from the frontend');

  //Switch to datetime! As we have storeutc=true and our test account is CET, but for users this should just appear like enabling the hour/minute/second part, this should cause a 2 hour jump
  scr->dt1->type := "datetime";
  TestEq(MakeDatetime(2024,6,12,22,0,0), scr->dt1->value, 'value changes to compensate');
  TestEq(MakeDatetime(2024,6,13,0,0,0), scr->dt1->localtzvalue, 'but the frontend visible time is still the same');

  scr->dt1->type := "date";
  TestEq(MakeDatetime(2024,6,13,0,0,0), scr->dt1->value, 'all back to midnight on the 13th');
  TestEq(MakeDatetime(2024,6,13,0,0,0), scr->dt1->localtzvalue, 'all back to midnight on the 13th (2)');

  scr->dt1->storeutc := FALSE;
  TestEq(MakeDatetime(2024,6,13,0,0,0), scr->dt1->value, 'still no reason for storeutc changes in date mode to change the value - but we tested that above');
  TestEq(MakeDatetime(2024,6,13,0,0,0), scr->dt1->localtzvalue, 'still no reason for storeutc changes in date mode to change the value - but we tested that above (2)');

  scr->dt1->type := "datetime";
  TestEq(MakeDatetime(2024,6,13,0,0,0), scr->dt1->localtzvalue, 'datetime switch NEVER affects localtzvalue');
  TestEq(MakeDatetime(2024,6,13,0,0,0), scr->dt1->value, 'and value == localtzvalue with storeutc == FALSE');

  scr->dt1->storeutc := TRUE;
}

MACRO TestBasicFieldsRegressions()
{
  //detect textedit incorrectly parsing XML given value using user personal settings
  GetTestController()->tolliumuser->decimalseparator:=",";
  GetTestController()->tolliumuser->thousandseparator:=".";
  OBJECT scr := GetTestController()->LoadScreen("webhare_testsuite:tests/regressions.basicfields");
  TestEq(5.0m, scr->moneyeditpoint->value);
  TestEq(5.0f, scr->floateditpoint->value);
  TestEq(50m, scr->moneyeditcomma->value);
  TestEq(50f, scr->floateditcomma->value);
}

ASYNC MACRO TestTextField()
{
  //also test linefeed handling (although this is also more a bit of clarification: it's not us but it's XML that gets rid of any non-&#10; break - https://www.w3.org/TR/xml/#AVNormalize)
  AWAIT ExpectScreenChange(+1, PTR GetTestController()->RunScreen(InlineScreens(`
    <screen name="test" implementation="none">
      <body>
        <text name="text1" value="this &lt;b>is&lt;/b> text

now lf:&#10;next line" />
        <text name="text2" htmlvalue="this &lt;b>is&lt;/b> text &#60;p>no para&#60;/p>" />
        <text name="text3" valuetid="webhare_testsuite:test.richtext" />
      </body>
    </screen>`)));

  TestEq("this <b>is</b> text  now lf:\nnext line", TT("text1")->value);
  TestEq("this &#60;b&#62;is&#60;/b&#62; text  now lf:<br />next line", TT("text1")->htmlvalue);

  TestEq("this is text no para", TT("text2")->value);
  TestEq("this <b>is</b> text no para", TT("text2")->htmlvalue,"verify <P> was completely ignored");

  TestEq("Dit is bold\nvolgende\nregel", TT("text3")->value);
  TestEq("Dit is <b>bold</b><br />volgende<br />regel", TT("text3")->htmlvalue);

  TT("text3")->htmlvalue := `Hey <a href="a&b">click</a> this!`;
  TestEq("Hey click this!", TT("text3")->value);
  TestEq(`Hey <a href="a&#38;b">click</a> this!`, TT("text3")->htmlvalue);

  AWAIT ExpectScreenChange(-1, PTR TTEscape);
}

ASYNC MACRO TestActions()
{
  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("webhare_testsuite:demo"));
  RECORD openres := ExecuteWindowOpenAction(TT("openwebharedev"));
  TestEq([ type := "url", url := "https://www.webhare.dev/" ], openres);
  AWAIT ExpectScreenChange(-1, PTR TTEscape);
}

Runtestframework( [ PTR InputValidations
                  , PTR DateTimeClamping
                  , PTR DateTimeLocalTZValue
                  , PTR TestBasicFieldsRegressions
                  , PTR TestTextField
                  , PTR TestActions
                  ], [ testusers := [[ login := "sysop", grantrights := ["system:sysop"]]]
                  ]);
