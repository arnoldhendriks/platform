﻿<?wh
LOADLIB "wh::dbase/postgresql.whlib";

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/testframework.whlib";
LOADLIB "mod::system/lib/internal/dbase/postgresql.whlib";

LOADLIB "mod::webhare_testsuite/lib/db/tablecreate.whlib";


RECORD ARRAY FUNCTION FindPGObjectsWithName(STRING mask)
{
  INTEGER namespace_oid := SELECT AS INTEGER oid FROM postgresql_pg_catalog.pg_namespace WHERE nspname = "webhare_testsuite_temp";

  RECORD ARRAY pg_classes :=
      SELECT type :=    "pg_class"
           , name :=    relname
           , oid
           , ns_oid :=  relnamespace
        FROM postgresql_pg_catalog.pg_class
       WHERE relnamespace = namespace_oid
         AND relname LIKE mask;

  RECORD ARRAY pg_constraints :=
      SELECT type :=    "pg_constraint"
           , name :=    conname
           , oid
           , ns_oid :=  connamespace
        FROM postgresql_pg_catalog.pg_constraint
       WHERE connamespace = namespace_oid
         AND conname LIKE mask;

  RECORD ARRAY pg_proc :=
      SELECT type :=    "pg_proc"
           , name :=    proname
           , oid
           , ns_oid :=  pronamespace
        FROM postgresql_pg_catalog.pg_proc
       WHERE pronamespace = namespace_oid
         AND proname LIKE mask;

  RETURN pg_classes CONCAT pg_constraints CONCAT pg_proc;
}


MACRO TestByteaConversion()
{
  ApplyTempDatabaseSchema(`
  <databaseschema xmlns:d="http://www.webhare.net/xmlns/whdb/databaseschema">
    <d:table name="byteaconversiontest" primarykey="id">
      <d:integer name="id" autonumberstart="1"/>
      <d:varchar name="name" maxlength="256" nullable="0"/>
    </d:table>
  </databaseschema>
`);
  TABLE< INTEGER id, STRING name > tbl_text := BindTransactionToTable(GetPrimary()->id, "webhare_testsuite_temp.byteaconversiontest");
  testfw->BeginWork();
  INSERT [ id := 1, name := `E'\\000'` ] INTO tbl_text;
  testfw->CommitWork();

  ApplyTempDatabaseSchema(`
  <databaseschema xmlns:d="http://www.webhare.net/xmlns/whdb/databaseschema">
    <d:table name="byteaconversiontest" primarykey="id">
      <d:integer name="id" autonumberstart="1"/>
      <d:bytea name="name" maxlength="256" />
    </d:table>
  </databaseschema>
`);

  TestEQ(
      [ [ id := 1, name := `E'\\000'` ]
      ], SELECT * FROM tbl_text ORDER BY id);
}

MACRO TestIndexDrops()
{
  ApplyTempDatabaseSchema(`
  <databaseschema xmlns:d="http://www.webhare.net/xmlns/whdb/databaseschema">
    <d:table name="indextest1" primarykey="id">
      <d:integer name="id" autonumberstart="1"/>
      <d:varchar name="name" maxlength="256" nullable="0"/>
      <d:index name="idx1" columns="name(20)" />
    </d:table>
    <d:table name="indextest2" primarykey="id">
      <d:integer name="id" autonumberstart="1"/>
      <d:varchar name="name" maxlength="256" nullable="0"/>
      <d:index name="idx1" columns="name(20)" />
    </d:table>
  </databaseschema>
`);

  ApplyTempDatabaseSchema(`
  <databaseschema xmlns:d="http://www.webhare.net/xmlns/whdb/databaseschema">
    <d:table name="indextest1" primarykey="id">
      <d:integer name="id" autonumberstart="1"/>
      <d:varchar name="name" maxlength="256" nullable="0"/>
      <d:obsoleteindex name="idx1" columns="name(20)" />
    </d:table>
    <d:obsoletetable name="indextest2" />
  </databaseschema>
`);
}

BOOLEAN FUNCTION TextFieldHasNotNullCheck(STRING for_table_schema, STRING for_table_name, STRING for_column_name)
{
  RECORD ARRAY candidates := SELECT check_clause
                               FROM postgresql_information_schema.table_constraints, postgresql_information_schema.check_constraints
                              WHERE table_constraints.constraint_schema = for_table_schema
                                    AND check_constraints.constraint_schema = for_table_schema
                                    AND table_constraints.constraint_name = check_constraints.constraint_name
                                    AND table_constraints.table_name = for_table_name;

  RETURN RecordExists(SELECT FROM candidates WHERE check_clause = `(((${for_column_name})::text <> \'\'::text))`);
}

MACRO TestColumnCreate()
{
  // float column was modified every update
  ApplyTempDatabaseSchema(`
  <databaseschema xmlns:d="http://www.webhare.net/xmlns/whdb/databaseschema">
    <d:table name="colcreatetest" primarykey="id">
      <d:integer name="id" autonumberstart="1"/>
      <d:float name="myfloat" />
    </d:table>
  </databaseschema>
`);
}

MACRO TestColumnAttributes()
{
  ApplyTempDatabaseSchema(`
  <databaseschema xmlns:d="http://www.webhare.net/xmlns/whdb/databaseschema">
    <d:table name="attrtest" primarykey="id">
      <d:integer name="id" autonumberstart="1"/>
      <d:varchar name="name" maxlength="256"/>
    </d:table>
  </databaseschema>
`);

  TestEq(FALSE, TextFieldHasNotNullCheck(testfw_testmodule, "attrtest", "name"));

  ApplyTempDatabaseSchema(`
  <databaseschema xmlns:d="http://www.webhare.net/xmlns/whdb/databaseschema">
    <d:table name="attrtest" primarykey="id">
      <d:integer name="id" autonumberstart="1"/>
      <d:varchar name="name" maxlength="512" unique="true" nullable="false"/>
    </d:table>
  </databaseschema>
`);

  TestEq(TRUE, TextFieldHasNotNullCheck(testfw_testmodule, "attrtest", "name"));

  //and revert
  ApplyTempDatabaseSchema(`
  <databaseschema xmlns:d="http://www.webhare.net/xmlns/whdb/databaseschema">
    <d:table name="attrtest" primarykey="id">
      <d:integer name="id" autonumberstart="1"/>
      <d:varchar name="name" maxlength="256"/>
    </d:table>
  </databaseschema>
`);

  TestEq(FALSE, TextFieldHasNotNullCheck(testfw_testmodule, "attrtest", "name"));
}

MACRO TestMultiColumnUniqueIndex()
{
  ApplyTempDatabaseSchema(`
  <databaseschema xmlns:d="http://www.webhare.net/xmlns/whdb/databaseschema">
    <d:table name="mcindextest" primarykey="id">
      <d:integer name="id" autonumberstart="1"/>
      <d:integer name="parent" references=".mcindextest" />
      <d:varchar name="name" maxlength="256"/>
      <d:index name="parentnameindex" columns="parent name(32)" unique="true" uppercase="true" />
    </d:table>
  </databaseschema>
`);

  TABLE< INTEGER id, INTEGER parent NULL := 0, STRING name > tbl_mcindextext := BindTransactionToTable(GetPrimary()->id, "webhare_testsuite_temp.mcindextest");

  testfw->BeginWork();
  INSERT [ id := 1, parent := 0, name := `name` ] INTO tbl_mcindextext;
  testfw->CommitWork();

  testfw->BeginWork();
  INSERT [ id := 2, parent := 0, name := `name` ] INTO tbl_mcindextext;
  RECORD ARRAY errors := testfw->CommitWorkReturnErrors();
  TestEQLike("ERROR:*duplicate key value violates unique constraint*", (errors??[[message := "no error"]])[0].message);
}

MACRO TestRenameCleanup()
{
  ApplyTempDatabaseSchema(`
    <databaseschema xmlns:d="http://www.webhare.net/xmlns/whdb/databaseschema">
      <d:table name="basetable" primarykey="id">
        <d:integer name="id" autonumberstart="1"/>
      </d:table>
      <d:table name="renametest" primarykey="id">
        <d:integer name="id" autonumberstart="1"/>
        <d:integer name="ref" references="webhare_testsuite_temp.basetable" />
        <d:integer name="parent" references=".renametest" />
        <d:varchar name="name" maxlength="256"/>
        <d:index name="parentnameindex" columns="parent name(32)" unique="true" uppercase="true" />
        <d:index name="nameindex" columns="name(32)" />
      </d:table>
    </databaseschema>
`);

  testfw->BeginWork();
  GetPrimary()->__ExecSQL(`ALTER TABLE webhare_testsuite_temp.renametest RENAME TO othername`);
  testfw->CommitWork();

  ClearAllPostgreSQLSchemaCaches();
  // also change foreign reference of parent to basetable, that confused contraint renames
  ApplyTempDatabaseSchema(`
    <databaseschema xmlns:d="http://www.webhare.net/xmlns/whdb/databaseschema">
      <d:table name="basetable" primarykey="id">
        <d:integer name="id" autonumberstart="1"/>
      </d:table>
      <d:table name="othername" primarykey="id">
        <d:integer name="id" autonumberstart="1"/>
        <d:integer name="ref" references="webhare_testsuite_temp.basetable" />
        <d:integer name="parent" references=".basetable" />
        <d:varchar name="name" maxlength="256"/>
        <d:index name="parentnameindex" columns="parent name(32)" unique="true" uppercase="true" />
      </d:table>
    </databaseschema>
`);

  TestEQ(RECORD[], FindPGObjectsWithName("*renametest*"));

  ClearAllPostgreSQLSchemaCaches();
  ApplyTempDatabaseSchema(`
    <databaseschema xmlns:d="http://www.webhare.net/xmlns/whdb/databaseschema">
      <d:table name="basetable" primarykey="id">
        <d:integer name="id" autonumberstart="1"/>
      </d:table>
      <d:obsoletetable name="othername" />
    </databaseschema>
`);

  TestEQ(RECORD[], FindPGObjectsWithName("*othername*"));
}

MACRO TestMultiTableCleanup()
{
  ClearAllPostgreSQLSchemaCaches();
  ApplyTempDatabaseSchema(`
    <databaseschema xmlns:d="http://www.webhare.net/xmlns/whdb/databaseschema">
      <d:table name="catalogs" primarykey="id">
        <d:integer name="id" autonumberstart="1" />
      </d:table>
      <d:table name="query_results" primarykey="id">
        <d:integer name="id" autonumberstart="1" />
        <d:obsoletecolumn name="indexid" />
        <d:integer name="catalogid" references=".catalogs" ondelete="cascade" nullable="0" />
        <d:integer name="pages" />
        <d:varchar name="query" nullable="0" maxlength="1024" />
        <d:integer name="results" />
        <d:varchar name="sess" unique="1" nullable="0" noupdate="1" maxlength="32" />
        <d:integer name="site" references="system.sites" ondelete="set default" />
        <d:varchar name="tag" maxlength="64" />
        <d:datetime name="when" />
      </d:table>
      <d:table name="query_clicks" primarykey="id">
        <d:integer name="id" autonumberstart="1" />
        <d:integer name="query" references=".query_results" ondelete="cascade" nullable="0" noupdate="1" />
        <d:varchar name="url" nullable="0" maxlength="1024" />
      </d:table>
    </databaseschema>
`);

  ClearAllPostgreSQLSchemaCaches();
  ApplyTempDatabaseSchema(`
    <databaseschema xmlns:d="http://www.webhare.net/xmlns/whdb/databaseschema">
      <d:table name="catalogs" primarykey="id">
        <d:integer name="id" autonumberstart="1" />
      </d:table>
      <d:obsoletetable name="query_results" />
      <d:obsoletetable name="query_clicks" />
    </databaseschema>
`);
}

MACRO Cleanup()
{
  ApplyTempDatabaseSchema(`
  <databaseschema xmlns:d="http://www.webhare.net/xmlns/whdb/databaseschema">
    <d:obsoletetable name="basetable" />
    <d:obsoletetable name="catalogs" />
    <d:obsoletetable name="attrtest" />
    <d:obsoletetable name="byteaconversiontest" />
    <d:obsoletetable name="colcreatetest" />
    <d:obsoletetable name="indextest1" />
    <d:obsoletetable name="mcindextest" />
  </databaseschema>
`);
  ClearAllPostgreSQLSchemaCaches();
  DumpValue(GetPrimary()->GetTableListing("webhare_testsuite_temp"), "boxed");
}

RunTestframework([ PTR SetupTempSchema
                 , PTR TestByteaConversion
                 , PTR TestIndexDrops
                 , PTR TestColumnCreate()
                 , PTR TestColumnAttributes
                 , PTR TestMultiColumnUniqueIndex
                 , PTR TestRenameCleanup
                 , PTR TestMultiTableCleanup()
                 , PTR Cleanup
                 ]);
