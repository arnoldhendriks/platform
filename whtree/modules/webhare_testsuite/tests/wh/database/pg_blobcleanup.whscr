﻿<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::files.whlib";
LOADLIB "wh::dbase/postgresql.whlib";
LOADLIB "wh::util/algorithms.whlib";

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/testframework.whlib";
LOADLIB "mod::system/lib/internal/dbase/postgresql-blobcleanup.whlib";
LOADLIB "mod::system/lib/internal/dbase/updatecommands.whlib";


TABLE <INTEGER id, BLOB data> beta_blobtests;

MACRO Setup()
{
  TestEQ("postgresql", GetPrimary()->type);

  testfw->BeginWork();
  IF (GetPrimary()->SchemaExists("webhare_testsuite_testschema"))
    GetPrimary()->DropSchema("webhare_testsuite_testschema", [ cascade := TRUE ]);
  GetPrimary()->CreateSchema("webhare_testsuite_testschema", "", "");

  __LegacyCreateTable(GetPrimary(), "webhare_testsuite_testschema", "beta_blobtests",
      [ primarykey := "id"
      , cols :=       [ [ column_name := "id", data_type := "INTEGER", autonumber_start := 1 ]
                      , [ column_name := "data", data_type := "BLOB" ]
                      ]
      ]);
  testfw->CommitWork();
}

BOOLEAN FUNCTION GetBlobStorageExists(STRING dbid)
{
  STRING ARRAY storageids := GetPrimary()->blobhandler->GetBlobPartIdsFromBlobIds([ dbid ]);

  STRING ARRAY exists := SELECT AS STRING ARRAY blobpartid FROM GetPrimary()->blobhandler->ListBlobParts([ filterids := storageids ]);

  BOOLEAN any_missing, any_found;
  FOREVERY (STRING id FROM storageids)
    IF (id IN exists)
      any_found := TRUE;
    ELSE
      any_missing := TRUE;

  IF (any_found AND any_missing)
    THROW NEW Exception(`Found a subset of storage files for blob ${dbid}`);

  RETURN any_found;
}

STATIC OBJECTTYPE ManifestStore
< RECORD manifest;
  PUBLIC RECORD FUNCTION GetSetManifest(RECORD newmanifest)
  {
    IF (RecordExists(newmanifest))
      this->manifest := newmanifest;
    RETURN this->manifest;
  }
>;

DATETIME FUNCTION ReturnDateTime(DATETIME toreturn)
{
  RETURN toreturn;
}

MACRO TestBlobCleanup()
{
  OBJECT analyzer := NEW PostgreSQLBlobAnalyzer(GetPrimary());

  beta_blobtests := BindTransactionToTable(GetPrimary()->id, "webhare_testsuite_testschema.beta_blobtests");

  analyzer->__filterids := [ "none" ];
  analyzer->abandoned_upload_secs := 3600;
  analyzer->unreferenced_period_secs := 3600;
  analyzer->keep_removed_files_secs := 3600;
  analyzer->__debugmanifestaccess := PTR (NEW ManifestStore)->GetSetManifest;
  DATETIME now := GetCurrentDatetime();
  analyzer->__debuggetcurrentdatetime := PTR ReturnDateTime(now);

  analyzer->UpdateBlobManifest();
  TestEQ(
      [ unreferenced :=   RECORD[]
      , unlinked :=       RECORD[]
      , abandoned :=      STRING[]
      ], analyzer->ReadBlobManifest());

  BLOB data1 := StringToBlob("myblob1");
  BLOB data2 := StringToBlob("myblob2");
  BLOB data3 := StringToBlob("myblob3");
  BLOB data4 := StringToBlob("myblob4");

  // Insert 3 blobs
  testfw->BeginWork();
  INTEGER id1 := MakeAutonumber(beta_blobtests, "id");
  INTEGER id2 := MakeAutonumber(beta_blobtests, "id");
  INTEGER id3 := MakeAutonumber(beta_blobtests, "id");
  INSERT CELL[ id := id1, data := data1 ] INTO beta_blobtests;
  INSERT CELL[ id := id2, data := data2 ] INTO beta_blobtests;
  INSERT CELL[ id := id3, data := data3 ] INTO beta_blobtests;
  STRING blob_deleted := __GetPostgreSQLBlobRegistrationId(GetPrimary()->id, data1);
  STRING blob_deleted_storageid := GetPrimary()->blobhandler->GetBlobPartIdsFromBlobIds([ blob_deleted ])[0];
  STRING blob_deletedreinserted := __GetPostgreSQLBlobRegistrationId(GetPrimary()->id, data2);
  STRING blob_kept := __GetPostgreSQLBlobRegistrationId(GetPrimary()->id, data3);
  testfw->CommitWork();

  // Failed upload
  testfw->BeginWork();
  INTEGER id4 := MakeAutonumber(beta_blobtests, "id");
  INSERT CELL[ id := id4, data := data4 ] INTO beta_blobtests;
  STRING blob_failedupload := __GetPostgreSQLBlobRegistrationId(GetPrimary()->id, data4);
  STRING blob_failedupload_storageid := GetPrimary()->blobhandler->GetBlobPartIdsFromBlobIds([ blob_failedupload ])[0];
  testfw->RollbackWork();


  analyzer->__filterids := [ "none", blob_deleted, blob_deletedreinserted, blob_kept, blob_failedupload ];

  // Initial situation - blob_failedupload is too new to register as failed upload yet

  now := GetCurrentDateTime();
  analyzer->__debuggetcurrentdatetime := PTR ReturnDateTime(AddTimeToDate(1, now));
  analyzer->UpdateBlobManifest();

  TestEQ(
      [ unreferenced :=   RECORD[]
      , unlinked :=       RECORD[]
      , abandoned :=      STRING[]
      ], analyzer->ReadBlobManifest());

  // Remove blob_deleted and blob_deletedreinserted
  testfw->BeginWork();
  DELETE FROM beta_blobtests WHERE id = id1 OR id = id2;
  testfw->CommitWork();

  // blob_deleted, blob_deletedreinserted should be picked up as unreferenced now
  now := GetCurrentDateTime();
  analyzer->__debuggetcurrentdatetime := PTR ReturnDateTime(AddTimeToDate(2, now));
  analyzer->UpdateBlobManifest();

  TestEQMembers(
      [ unreferenced :=   [ [ ids := GetSortedSet([ blob_deleted, blob_deletedreinserted ]) ] ]
      , unlinked :=       RECORD[]
      , abandoned :=      STRING[]
      ], analyzer->ReadBlobManifest(), "*");

  // up the time a little more than 60 seconds

  // blob_failedupload is now picked up as abandoned,
  // and blob_deleted, blob_deletedreinserted are confirmed as unreferenced
  now := GetCurrentDateTime();
  analyzer->__debuggetcurrentdatetime := PTR ReturnDateTime(AddTimeToDate(65*60000, now));
  analyzer->UpdateBlobManifest();

  TestEQMembers(
      [ unreferenced :=   [ [ ids := GetSortedSet([ blob_deleted, blob_deletedreinserted ]) ] ]
      , unlinked :=       RECORD[]
      , abandoned :=      STRING[ blob_failedupload_storageid ]
      ], analyzer->ReadBlobManifest(), "*");

  // Re-insert blob_deletedreinserted just before deleting from the blob table
  testfw->BeginWork();
  INSERT CELL[ id := id2, data := data2 ] INTO beta_blobtests;
  testfw->CommitWork();

  // Execute remove, blob_deleted and blob_failedupload are scheduled for file delete
  // blob_deletedreinserted should be skipped due to being referenced again
  now := GetCurrentDateTime();
  analyzer->__debuggetcurrentdatetime := PTR ReturnDateTime(AddTimeToDate(70*60000, now));
  RECORD res := analyzer->RemoveUnreferencedBlobs();

  TestEQ(
      [ scheduled_abandoned :=  STRING[ blob_failedupload_storageid ]
      , removed_unreferenced := GetSortedSet([ blob_deleted ])
      , unlinked :=             GetSortedSet([ blob_deleted_storageid ])
      , removed :=              STRING[]
      , removed_old_archived := STRING[]
      ], res);
  TestEQMembers(
      [ unreferenced :=   RECORD[]
      , unlinked :=       [ [ blobpartids := GetSortedSet([ blob_deleted_storageid, blob_failedupload_storageid ]) ] ]
      , abandoned :=      STRING[]
      ], analyzer->ReadBlobManifest(), "*");

  // up the time past the keep_removed_files_secs (another 60 secs), the pending deletes will be done
  now := GetCurrentDateTime();
  analyzer->__debuggetcurrentdatetime := PTR ReturnDateTime(AddTimeToDate(135 * 60000, now));
  analyzer->UpdateBlobManifest();

  TestEQMembers(
      [ unreferenced :=   RECORD[]
      , unlinked :=       [ [ blobpartids := GetSortedSet([ blob_deleted_storageid, blob_failedupload_storageid ]) ] ]
      , abandoned :=      STRING[]
      ], analyzer->ReadBlobManifest(), "*");

  TestEQ(TRUE, GetBlobStorageExists(blob_deleted));
  TestEQ(TRUE, GetBlobStorageExists(blob_deletedreinserted));
  TestEQ(TRUE, GetBlobStorageExists(blob_kept));
  TestEQ(TRUE, GetBlobStorageExists(blob_failedupload));

  res := analyzer->RemoveUnreferencedBlobs();
  TestEQ(
      [ scheduled_abandoned :=  STRING[]
      , removed_unreferenced := STRING[]
      , unlinked :=             STRING[]
      , removed :=              GetSortedSet([ blob_deleted_storageid, blob_failedupload_storageid ])
      , removed_old_archived := STRING[]
      ], res);
  TestEQMembers(
      [ unreferenced :=   RECORD[]
      , unlinked :=       RECORD[]
      , abandoned :=      STRING[]
      ], analyzer->ReadBlobManifest(), "*");

  TestEQ(FALSE, GetBlobStorageExists(blob_deleted));
  TestEQ(TRUE, GetBlobStorageExists(blob_deletedreinserted));
  TestEQ(TRUE, GetBlobStorageExists(blob_kept));
  TestEQ(FALSE, GetBlobStorageExists(blob_failedupload));
}

RunTestframework([ PTR Setup
                 , PTR TestBlobCleanup
                 ]);
