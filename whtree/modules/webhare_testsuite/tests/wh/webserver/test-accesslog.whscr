<?wh

LOADLIB "wh::datetime.whlib";

LOADLIB "mod::system/lib/logging.whlib";
LOADLIB "mod::system/lib/testframework.whlib";


RECORD ARRAY FUNCTION GetAccessLogEntries(STRING logname, DATETIME rangestart, INTEGER expectlines)
{
  // Wait for access log to catch up
  DATETIME deadline := AddTimeToDate(60*1000,GetCurrentDateTime());
  RECORD ARRAY rows;
  WHILE (GetCurrentDateTime() < deadline)
  {
    Sleep(1000);

    OBJECT accesslog := OpenWebHareLogStream(logname, rangestart, GetCurrentDateTime());
    rows := RECORD[];
    WHILE (TRUE)
    {
      RECORD row := accesslog->ReadRecord();
      IF (NOT RecordExists(row))
        BREAK;

      IF (row.url NOT LIKE "*__test*")
        CONTINUE;

      INSERT row INTO rows AT END;
    }
    IF(Length(rows) >= expectlines)
      BREAK;

    accesslog->Close();
  }
  RETURN rows;
}

MACRO RunQueries()
{
  RECORD testport := testfw->GetLocalhostWebinterface();
  RECORD testvport := testfw->CreateWebserverPort([ virtualhost := TRUE ]);

  STRING longurlpath := Left("/.__test/longurl-" || RepeatText("abcdefg", 32768), 8192);
  DATETIME teststart := GetCurrentDateTime();
  testfw->browser->GotoWebPage(testport.baseurl || SubString(longurlpath, 1));
  testfw->browser->GotoWebPage(testvport.baseurl || ".__test/legalhost", [ headers := [ [ field := "Host", value := "example.com" ] ] ]);
  testfw->browser->GotoWebPage(testvport.baseurl || ".__test/illegalhost", [ headers := [ [ field := "Host", value := "example \" .com " ] ] ]);
  testfw->browser->GotoWebPage(testvport.baseurl || ".__test/illegalhost2", [ headers := [ [ field := "Host", value := "example\r.com" ] ] ]);
  testfw->browser->GotoWebPage(testvport.baseurl || ".__test/illegalhost3", [ headers := [ [ field := "Host", value := "example\n.com" ] ] ]);
  testfw->browser->GotoWebPage(testvport.baseurl || ".px/pixelhit?__test", [ headers := [ [ field := "Host", value := "example\n.com" ] ] ]);
  testfw->browser->GotoWebPage(testvport.baseurl || ".wh/ea/pxl/pixelhit2?__test", [ headers := [ [ field := "Host", value := "example\n.com" ] ] ]);

  RECORD ARRAY data := GetAccessLogEntries("access", teststart, 7);
  TestEQMembers(
      [ [ host :=       "127.0.0.1"
        , method :=     "GET"
        , url :=        longurlpath
        , useragent :=  'HareScript/2 (HareScript HTTP library)'
        ]
      , [ host :=       "example.com"
        , method :=     "GET"
        , url :=        "/.__test/legalhost"
        , useragent :=  'HareScript/2 (HareScript HTTP library)'
        ]
      , [ host :=       "example \" .com "
        , url :=        "/.__test/illegalhost"
        ]
      , [ host :=       "example"
        , url :=        "/.__test/illegalhost2"
        ]
      , [ host :=       "example"
        , url :=        "/.__test/illegalhost3"
        ]
      , [ host :=       "example"
        , url :=        "/.px/pixelhit?__test"
        ]
      , [ host :=       "example"
        , url :=        "/.wh/ea/pxl/pixelhit2?__test"
        ]
      ], data, "*");

  data := GetAccessLogEntries("pxl", teststart, 1);
  TestEQMembers(
      [ [ host :=       "example"
        , method :=     "GET"
        , url :=        "/.px/pixelhit?__test"
        , useragent :=  'HareScript/2 (HareScript HTTP library)'
        ]
      , [ host :=       "example"
        , method :=     "GET"
        , url :=        "/.wh/ea/pxl/pixelhit2?__test"
        , useragent :=  'HareScript/2 (HareScript HTTP library)'
        ]
      ], data, "*");
}


RunTestFramework(
        [ PTR RunQueries
        ]);
