<?wh
LOADLIB "wh::datetime.whlib";

LOADLIB "mod::consilio/lib/api.whlib";

LOADLIB "mod::system/lib/testframework.whlib";

LOADLIB "mod::webhare_testsuite/lib/system/tests.whlib";


MACRO TestHTMLIndexing()
{
  // The testfile has two pages, with the first page linking to the second page

  // First page has robots "index" -> it should be indexed
  testfw->BeginWork();
  OBJECT catalog := CreateConsilioCatalog("consilio:testfw_robots", [ priority := 9 ]);
  TestEq(TRUE, catalog->managed);

  OBJECT siteroot := GetTestsuiteTempFolder();
  OBJECT newsource := catalog->AddFolderToCatalog(siteroot->id, [ enabled := FALSE ]); //disable it so we can manually control the frontend (and can avoid WaitIndexDone etc) without risking conflicting/duplicate index actions
  OBJECT testfile := siteroot->CreateFile([ name := "robottest", typens := "http://www.webhare.net/xmlns/webhare_testsuite/consilio/robottest", publish := TRUE]);

  testfw->CommitWork();
  Print(catalog->GetStorageInfo() || "\n");
  TestEq(TRUE, catalog->WaitReady(MAX_DATETIME, [ forstorage := TRUE, debug := TRUE ]));

  IF (testfw->debug)
    Print(testfile->url || "\n");

  testfw->WaitForPublishCompletion(siteroot->id);
  newsource->ReindexGroup(ToString(testfile->id), [ rebuild := TRUE, foreground := TRUE ]); //run in foreground, we're not testing queues

  RECORD res := RunConsilioSearch("consilio:testfw_robots", CQParseUserQuery("page"));
  TestEQ(1, res.totalcount);
  TestEq(TRUE, RecordExists(SELECT FROM res.results WHERE objectid = testfile->url));


  testfw->BeginWork();
  testfile->SetInstanceData("http://www.webhare.net/xmlns/webhare_testsuite/consilio/robottest", [ noindex := TRUE ]);
  // First page now has robots "noindex", follow" -> nothing should be indexed as we no longer spider since webhare 5.3
  testfw->CommitWork();

  newsource->ReindexGroup(ToString(testfile->id), [ rebuild := TRUE, foreground := TRUE ]);

  res := RunConsilioSearch("consilio:testfw_robots", CQParseUserQuery("page"));
  TestEq(0, res.totalcount);
}

RunTestframework(
    [ PTR TestHTMLIndexing ],
    [ testusers :=
        [ [ login := "sysop", grantrights := [ "system:sysop" ] ]
        ]
    ]);
