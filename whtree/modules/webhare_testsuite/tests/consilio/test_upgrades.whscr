<?wh

LOADLIB "wh::crypto.whlib";
LOADLIB "wh::datetime.whlib";

LOADLIB "mod::consilio/lib/api.whlib";
LOADLIB "mod::consilio/lib/catalogs.whlib";
LOADLIB "mod::consilio/lib/internal/dbschema.whlib";
LOADLIB "mod::consilio/lib/internal/opensearch.whlib";
LOADLIB "mod::consilio/lib/internal/updateindices.whlib";

LOADLIB "mod::system/lib/testframework.whlib";

MACRO TestNofieldUnmanagedCatalog()
{
  //clear out and recreate the testcatalog metadata
  testfw->BeginWork();
  DELETE FROM consilio.catalogs WHERE name LIKE "webhare_testsuite:emptycatalog";
  testfw->CommitWork();
  FixConsilioIndices([ catalogmask := "webhare_testsuite:emptycatalog" ]);

  //Ensure a catalog with empty definitions doesn't crash on mapping updates
  testfw->BeginWork();
  OpenConsilioCatalog("webhare_testsuite:emptycatalog")->AttachIndex(0);
  testfw->CommitWork();

  FixConsilioIndices([ catalogmask := "webhare_testsuite:emptycatalog" ]);
  RECORD ARRAY indices := OpenConsilioCatalog("webhare_testsuite:emptycatalog")->ListAttachedIndices();
  RECORD mapping := SendRawJSONToOpenSearch(indices[0].indexmanager, "GET", `/${indices[0].indexname}/_mapping`, DEFAULT RECORD).result;
  TestEq("strict", GetCell(mapping,indices[0].indexname).mappings.dynamic);
}

MACRO TestOldIndexSyntax()
{
  testfw->BeginWork();

  //A catalog created using a legacy <index tag (we'll drop support soon!) should be an Unmanaged Suffixed catalog
  OBJECT legacytoolslog := OpenConsilioCatalog("webhare_testsuite:toolslog");
  TestEq(FALSE, legacytoolslog->managed);
  TestEq(TRUE, legacytoolslog->suffixed, "Catalog is not initially suffixed - you may have to `wh consilio:updateindices` first");

  FOREVERY(RECORD toremove FROM legacytoolslog->ListAttachedIndices())
    legacytoolslog->DetachIndex(toremove.id);

  legacytoolslog->AttachIndex(0);
  legacytoolslog->ApplyConfiguration([ suffixes := ["sfx1"]]);
  testfw->CommitWork();

  RECORD indexinfo := legacytoolslog->ListAttachedIndices()[0];
  RECORD mapping := SendRawJSONToOpenSearch(indexinfo.indexmanager, "GET", `/${indexinfo.indexname}-sfx1/_mapping`, DEFAULT RECORD).result;
  TestEq("keyword", GetCell(mapping,indexinfo.indexname || "-sfx1").mappings.properties.minfin.properties.theme.type, "Verify that the fieldgroup got applied");

  testfw->BeginWork();
  legacytoolslog->UpdateCatalog([ suffixed := FALSE ]);
  TestEq(FALSE, legacytoolslog->suffixed);
  testfw->CommitWork();

  TestEq(FALSE, OpenConsilioCatalog("webhare_testsuite:toolslog")->suffixed);

  FixConsilioIndices([ catalogmask := "webhare_testsuite:*" ]);

  TestEq(TRUE, OpenConsilioCatalog("webhare_testsuite:toolslog")->suffixed);
}

MACRO TestIndexConfiguration(BOOLEAN expect_proper, OBJECT catalog)
{
  RECORD indexinfo := catalog->ListAttachedIndices()[0];
  RECORD settings := SendRawJSONToOpenSearch(indexinfo.indexmanager, "GET", `/${indexinfo.indexname}/_settings`, DEFAULT RECORD).result;
  settings := Getcell(settings, indexinfo.indexname).settings;
  IF(expect_proper)
  {
    TestEq("icu_tokenizer", settings."index".analysis.analyzer.CONSILIO_ANALYZER.tokenizer);
  }
  ELSE
  {
    TestEq(FALSE, CellExists(settings."index", "analysis"));
  }

  //TestEq(expect_proper ? cell[x:=42] : CELL[], GetCell(mapping, indexinfo.indexname).mapings);
  RECORD mapping := SendRawJSONToOpenSearch(indexinfo.indexmanager, "GET", `/${indexinfo.indexname}/_mapping`, DEFAULT RECORD).result;
  IF(expect_proper)
    TestEqMembers([ properties := CELL[] ], GetCell(mapping, indexinfo.indexname).mappings, '*');
  ELSE
    TestEq(CELL[], GetCell(mapping, indexinfo.indexname).mappings);
}

MACRO TestOpeningExternalIndex()
{
  testfw->BeginWork();

  //A catalog without a consilio_analysis (eg legacy wobcovid19) crashed during adding objects
  RECORD builtin_opensearch := SELECT * FROM ListIndexManagers() WHERE isbuiltin;
  STRING updateindexname := "c_upgradetest_" || ToLowercase(EncodeBase16(Left(DecodeUFS(GenerateUFS128BitId()),8)));
  TestEq(TRUE, SendRawJSONToOpenSearch(builtin_opensearch.id, "PUT", `/${updateindexname}`, DEFAULT RECORD).result.acknowledged);

  //Now create catalog on top of this existing index
  OBJECT testcatalog := CreateConsilioCatalog("consilio:testfw_upgradetest", [ managed := FALSE
                                                                             , fieldgroups := ["webhare_testsuite:pagelistfields" ]
                                                                             ]);
  TestEq(FALSE, testcatalog->suffixed);
  INTEGER newattachment := testcatalog->AttachIndex(builtin_opensearch.id, [ indexname := updateindexname, readonly := TRUE ]);
  Print(testcatalog->GetStorageInfo()||'\n');

  //At this point consilio should *not* yet have intervened... attachindex on an unmanaged index should be a no-op
  TestIndexConfiguration(FALSE, testcatalog);
  testcatalog->DetachIndex(newattachment);

  //Normal AttachIndex will schedule an update of our configuration
  testcatalog->AttachIndex(builtin_opensearch.id, [ indexname := updateindexname, readonly := FALSE ]);
  TestIndexConfiguration(FALSE, testcatalog);
  testfw->CommitWork();

  TestEq(TRUE, testcatalog->WaitReady(MAX_DATETIME, [ forconfiguration := TRUE]));

  TestIndexConfiguration(TRUE, testcatalog);
  testcatalog->AddObjects([[ objectid := "x123", documentfields := [ myfield1 := "hi this is a test" ]]]);
  TestEqMembers([[ suffix := "" ]], testcatalog->ListSuffixes(), "*");

}

RunTestframework(
    [ PTR TestNofieldUnmanagedCatalog
    , PTR TestOldIndexSyntax
    , PTR TestOpeningExternalIndex
    ]);
