<?wh

LOADLIB "wh::util/algorithms.whlib";

LOADLIB "mod::system/lib/testframework.whlib";
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";
LOADLIB "mod::system/lib/internal/whfs/events.whlib";

LOADLIB "mod::consilio/lib/api.whlib";

LOADLIB "mod::webhare_testsuite/lib/system/tests.whlib";
LOADLIB "mod::webhare_testsuite/lib/internal/testsite.whlib";

/* NOTE changes to searchprovide.whlib currently require a
         wh services restart publisher:consiliohandler
*/
PUBLIC MACRO BuildForms()
{
  testfw->BeginWork();
  RECORD formplain := BuildWebtoolForm( [ openingrichtext := TRUE, checkboxes := TRUE, addintegerfield := TRUE, addcheckboxfield := TRUE, addaddressfield := TRUE, addtimefield := TRUE, checkboxsubs:=TRUE ]);
  RECORD formwidget := BuildWebtoolForm( [ filename := "form-widget", iswidget := TRUE, checkboxes := TRUE, addintegerfield := TRUE, addcheckboxfield := TRUE, addaddressfield := TRUE, addtimefield := TRUE, checkboxsubs := TRUE ]);
  RECORD forminfotexts := BuildWebtoolForm( [ filename := "form-pagetitles-infotexts" ]);
  RECORD formcustom := BuildCustomWebtoolForm2();

  OBJECT webtoolsfolder := OpenTestsuiteSite()->OpenByPath("webtools");
  OBJECT emptyform := webtoolsfolder->EnsureFile( [ name := "form-empty"
                                                  , typens := "http://www.webhare.net/xmlns/publisher/formwebtool"
                                                  ]);


  OBJECT promise := GetWHFSCommitHandler()->WaitForChangesIndexed();

  testfw->CommitWork();
  WaitForPromise(promise);

  RECORD result;

  result := RunConsilioSearch(whconstant_consilio_catalog_whfs, CQMatch("whfstree", "CONTAINS", webtoolsfolder->id), [ summary_length := 1000 ]);
  //dumpvalue(result,'tree');

  RECORD plainfile := SELECT * FROM result.results WHERE whfsid = formplain.id;
  TestEq(TRUE, RecordExists(plainfile));
  dumpvalue(plainfile.summary);
  TestEqLike("*Welcome to this form!*Hi!*First name*", plainfile.summary);
  TestEq(FALSE, plainfile.summary LIKE "*We humbly thank you*", "should NOT show thankyou page in form");

  RECORD emptyfile := SELECT * FROM result.results WHERE whfsid = emptyform->id;
  TestEq(TRUE, RecordExists(emptyfile));

  // Search by form component
  // All forms contain a plain, single-line text field
  result := RunConsilioSearch(whconstant_consilio_catalog_whfs, CQAnd(
      [ CQMatch("whfstree", "CONTAINS", webtoolsfolder->id)
      , CQMatch("mod_publisher.formfields", "CONTAINS", "http://www.webhare.net/xmlns/publisher/forms#textedit")
      ]), [ summary_length := 0, mapping := [ whfsid := 0 ] ]);
  TestEq(GetSortedSet(INTEGER[ formplain.id, formwidget.id, forminfotexts.id, formcustom.id ]), SELECT AS INTEGER ARRAY whfsid FROM result.results ORDER BY whfsid);
  // All forms contain the custom testsuite handler
  result := RunConsilioSearch(whconstant_consilio_catalog_whfs, CQAnd(
      [ CQMatch("whfstree", "CONTAINS", webtoolsfolder->id)
      , CQMatch("mod_publisher.formfields", "CONTAINS", "http://www.webhare.net/xmlns/webhare_testsuite/testformcomponents#testsuitehandler")
      ]), [ summary_length := 0, mapping := [ whfsid := 0 ] ]);
  TestEq(GetSortedSet(INTEGER[ formplain.id, formwidget.id, forminfotexts.id, formcustom.id ]), SELECT AS INTEGER ARRAY whfsid FROM result.results ORDER BY whfsid);
  // Only the plain and widget forms contain a number field
  result := RunConsilioSearch(whconstant_consilio_catalog_whfs, CQAnd(
      [ CQMatch("whfstree", "CONTAINS", webtoolsfolder->id)
      , CQMatch("mod_publisher.formfields", "CONTAINS", "http://www.webhare.net/xmlns/publisher/forms#textedit?valuetype=integer|money|float")
      ]), [ summary_length := 0, mapping := [ whfsid := 0 ] ]);
  TestEq(GetSortedSet(INTEGER[ formplain.id, formwidget.id ]), SELECT AS INTEGER ARRAY whfsid FROM result.results ORDER BY whfsid);
}


RunTestframework([ PTR BuildForms
                 ]
                ,[ testusers := [[ login := "sysop", grantrights := ["system:sysop"] ]
                                ]
                 ]);
