<?wh

LOADLIB "wh::files.whlib";

LOADLIB "mod::consilio/lib/api.whlib";
LOADLIB "mod::consilio/lib/whfscatalog.whlib";
LOADLIB "mod::consilio/lib/internal/whfscatalog/whfsobjectsource.whlib";

LOADLIB "mod::publisher/lib/search/richdocuments.whlib";
LOADLIB "mod::publisher/lib/siteprofiles.whlib";

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/whfs.whlib";
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";
LOADLIB "mod::system/lib/testframework.whlib";

LOADLIB "mod::webhare_testsuite/lib/system/tests.whlib";

RECORD testdoc :=  [ htmltext := StringToBlob('<h1 class="heading1">Eerste kop</h1>'  //h1s[0]
                                          || '<div class="wh-rtd-embeddedobject" data-instanceid="level2doc">noise</div>'
                                          )
                   , instances := [[ instanceid := "level2doc"
                                   , data := [ whfstype := "http://www.webhare.net/xmlns/webhare_testsuite/rtd/embedlvl2"
                                             , richdoc := [ htmltext := StringToBlob('<h2 class="heading2">Tweede kop</h2>')
                                                          ]
                                             ]
                                   ]
                                 ]
                   ];

OBJECT testrtd, testrtd_noinstances, testunknownfile;

MACRO TestIndexTypeAPIs()
{
  testfw->BeginWork();

  OBJECT testloc := GetTestsuiteTempFolder()->EnsureFolder([ name := "rtdtype" ]);
  OBJECT richdoctype := OpenWHFSType("http://www.webhare.net/xmlns/publisher/richdocumentfile");
  testrtd := testloc->CreateFile( [ name := "lvl1.rtd", type := richdoctype->id ]);
  richdoctype->SetInstanceData(testrtd->id, [data := testdoc]);

  OBJECT applytester := GetApplytesterForObject(testrtd->id);
  TestEq(DEFAULT RECORD, GetRichDocumentIndexableContent(DEFAULT RECORD, CELL[applytester]));

  RECORD indexable := GetSearchSubfiles((SELECT * FROM system.fs_objects WHERE id = testrtd->id));
  TestEqLike("*Eerste kop*Tweede kop*", BlobToString(indexable.subfiles[0].data));
  TestEq(2, Length(indexable.indextypes));
  TestEq(2, Length(indexable.indexversions));

  testunknownfile := testloc->CreateFile( [ name := "unknown", type := 0 ]);
  richdoctype->SetInstanceData(testunknownfile->id, [data := testdoc]);
  TestEq(0, testunknownfile->type);

  RECORD testdoc_noinstances := CELL[...testdoc, instances := RECORD[]];
  testrtd_noinstances := testloc->CreateFile([name := "noinstances.rtd", type := richdoctype->id ]);
  richdoctype->SetInstanceData(testrtd_noinstances->id, [ data := testdoc_noinstances ] );

  RECORD indexable_noinstances := GetSearchSubfiles((SELECT * FROM system.fs_objects WHERE id = testrtd_noinstances->id));
  //No subcontent, so no indextypes or versions other than the file itself
  TestEq(1, Length(indexable_noinstances.indextypes));
  TestEq(1, Length(indexable_noinstances.indexversions));

  RECORD indexable_unknknowntype := GetSearchSubfiles((SELECT * FROM system.fs_objects WHERE id = testunknownfile->id));
  TestEq(1, Length(indexable_unknknowntype.subfiles), "The file is its only subfile");
  TestEq(2, Length(indexable_unknknowntype.indextypes));
  TestAssert(indexable_unknknowntype.indextypes[1] != indexable.indextypes[1], "Assert different type hashes");
  TestAssert(indexable_unknknowntype.indextypes[0] = indexable.indextypes[0], "But matching widget types");
  TestEq(1, Length(indexable_unknknowntype.indexversions));
  TestAssert(indexable_unknknowntype.indexversions[0] = indexable.indexversions[0], "And matching widget versions");

  testfw->CommitWork();

  OBJECT catalog := OpenConsilioCatalog(whconstant_consilio_catalog_whfs);
  OBJECT provider := catalog->OpenContentSource("consilio:whfsobjects")->__OpenProvider();

  RECORD ARRAY objs := provider->ListObjects3(DEFAULT DATETIME, "fsobj_" || testrtd->id);

  TestEq(1, Length(objs));
  TestEq(2, Length(objs[0].data.document_fields.mod_consilio.indextypes));
  TestEq(2, Length(objs[0].data.document_fields.mod_consilio.indexversions));

  RECORD fetched := objs[0].data;
  TestEqLike("*Eerste kop*Tweede kop*", fetched.document_body);
  TestEq(FALSE, fetched.document_body LIKE "*noise*");

  RECORD ARRAY objs2 := provider->ListObjects3(DEFAULT DATETIME, "fsobj_" || testrtd_noinstances->id);
  TestEq(1, Length(objs2));
  TestEq(1, Length(objs2[0].data.document_fields.mod_consilio.indextypes));
  TestEq(1, Length(objs2[0].data.document_fields.mod_consilio.indexversions));
  TestEq(TRUE, objs2[0].data.document_fields.mod_consilio.indextypes[0] IN fetched.document_fields.mod_consilio.indextypes, "The filetype hash has to be in both files");
  TestEq(TRUE, objs2[0].data.document_fields.mod_consilio.indexversions[0] IN fetched.document_fields.mod_consilio.indexversions, "The filetype version hash has to be in both files");

  RECORD ARRAY objs_unknown := provider->ListObjects3(DEFAULT DATETIME, "fsobj_" || testunknownfile->id);
  TestEq(1, Length(objs_unknown));
  TestEq(2, Length(objs_unknown[0].data.document_fields.mod_consilio.indextypes));
  TestEq(1, Length(objs_unknown[0].data.document_fields.mod_consilio.indexversions));
}

MACRO TestPublisherIndex()
{
  ReindexWHFSChanges([[ id := testrtd->id, isfolder := FALSE, isdelete := FALSE ]]);

  RECORD result := RunConsilioSearch(whconstant_consilio_catalog_whfs, CQMatch("whfsid", "=", testrtd->id), [ mapping := [ mod_consilio := [ indextypes := STRING[], indexversions := STRING[] ]]]);
  TestEq(2, Length(result.results[0].mod_consilio.indextypes));
  TestEq(2, Length(result.results[0].mod_consilio.indexversions));
  TestEq(1, Length(result.results));
  TestEqLike("*Eerste kop*Tweede kop*", result.results[0].summary);
}

MACRO TestSearchOverride()
{
  //Test overriding the search parameter
  testfw->BeginWork();
  testrtd->UpdateMetadata([name := "override-widget-search.rtd"]);
  testfw->CommitWork();

  OBJECT applytester := GetApplytesterForObject(testrtd->id);
  RECORD indexable := GetRichDocumentIndexableContent(testdoc, CELL[applytester]);
  TestEqLike("*This is the level2 search preview*", BlobToString(indexable.htmltext));
}

RunTestframework(
    [ PTR TestIndexTypeAPIs
    , PTR TestPublisherIndex
    , PTR TestSearchOverride
    ]);
