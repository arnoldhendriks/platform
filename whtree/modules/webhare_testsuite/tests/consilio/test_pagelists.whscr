<?wh

LOADLIB "mod::consilio/lib/api.whlib";
LOADLIB "mod::consilio/lib/pagelists.whlib";

LOADLIB "mod::system/lib/testframework.whlib";

LOADLIB "mod::webhare_testsuite/lib/system/tests.whlib";


/* sitemap

we brengen eerst de mappen in kaart
per map brengen we de bestanden in kaart. id, type, link, isindex, subpagebaseurl, modificationdate
per type zoeken we op of er een pagelistprovider is. instantieren we
zo ja, dan roepen we pagelistprovider functie GetSitemapLinks aan met bovenstaand record.
zo nee, dan roepen we een default pagelistprovider aan

TODO:
  - canonical links herkennen (via seotab instelbaar toch?)
  - SEOTAB NOINDEX herkennen
  - sitemap types aanmaken en negeren in dit overzicht (misschien meer filetypes simpelweg via siteprofile volledig kunnen excluden van sitemap, maar wat betekent dit voor consilio?)
*/

MACRO TestSitemapLinks()
{
  STRING baseurl := OpenTestsuiteSite()->webroot;
  RECORD ARRAY links := GenerateSitemapLinks(OpenTestsuiteSite()->id);
//  dumpvalue(links,'boxed');

  INTEGER pagelistfile := OpenTestsuiteSite()->OpenByPath("testpages/pagelisttest")->id;
  TestEq(11, Length(SELECT FROM links WHERE fileid = pagelistfile));
  TestEq(TRUE, RecordExists(SELECT FROM links WHERE link LIKE "*/formtest/"));

  TestEq(2, Length(SELECT FROM links WHERE link LIKE "*/sub/*"));
  TestEq(TRUE, RecordExists(SELECT FROM links WHERE link LIKE "*/sub/staticpage/"));
  TestEq(TRUE, RecordExists(SELECT FROM links WHERE link LIKE "*/sub/contentlink/"));

  TestEq([[ changefreq := "",        priority := 0.0 ]], SELECT changefreq, priority FROM links WHERE link = baseurl || "TestPages/StaticPage/");
  TestEq([[ changefreq := "daily",   priority := 1.0 ]], SELECT changefreq, priority FROM links WHERE link = baseurl || "TestPages/pagelisttest/");
  TestEq([[ changefreq := "monthly", priority := 0.8 ]], SELECT changefreq, priority FROM links WHERE link = baseurl || "TestPages/pagelisttest/sub1.html");
  TestEq([[ changefreq := "yearly",  priority := 0.6 ]], SELECT changefreq, priority FROM links WHERE link = baseurl || "TestPages/pagelisttest/sub2.html");
  //sub10 only exists as hash
  TestEq([[ changefreq := "",        priority := 0.0 ]], SELECT changefreq, priority FROM links WHERE link = baseurl || "TestPages/pagelisttest/sub10.html");
  TestEq(RECORD[], SELECT * FROM links WHERE link NOT LIKE baseurl || "*");
  TestEq(RECORD[], SELECT * FROM links WHERE link LIKE "*#*");
  //of the link types (18-20) only the contentlink deserves a place in the site map
  TestEq([[ link := baseurl || "TestPages/foldertemplate/sub/contentlink/" ]
         ,[ link := baseurl || "TestPages/staticpage-contentlink/" ]
         ], SELECT link FROM links WHERE filetype IN [18,19,20] ORDER BY link);

  OBJECT foldertemplatesub := OpenTestsuiteSite()->OpenByPath("testpages/foldertemplate/sub");
  links := GenerateSitemapLinks(foldertemplatesub->id);
  TestEq(TRUE, RecordExists(SELECT FROM links WHERE link LIKE "*/sub/staticpage/"));
  TestEq(TRUE, RecordExists(SELECT FROM links WHERE link LIKE "*/sub/contentlink/"));

  links := GenerateSitemapLinks(OpenTestsuiteSite()->id, [ skipfolderswithoutindex := TRUE ]);
  TestEq(FALSE, RecordExists(SELECT FROM links WHERE link LIKE "*/sub/staticpage/"));
  TestEq(FALSE, RecordExists(SELECT FROM links WHERE link LIKE "*/sub/contentlink/"));

  OBJECT testpagessub := OpenTestsuiteSite()->OpenByPath("testpages/foldertemplate/sub");
  TestEq(2, Length(GenerateSitemapLinks(testpagessub->id)));
  TestEq(0, Length(GenerateSitemapLinks(testpagessub->id, [ skipfolderswithoutindex := TRUE ])));
}


MACRO TestSubpageIndexing()
{
  testfw->BeginWork();

  OBJECT catalog := CreateConsilioCatalog("consilio:testfw_subpages", [ fieldgroups := [ "webhare_testsuite:pagelistfields" ]
                                                                      , priority := 9
                                                                      ]);
  Print(catalog->GetStorageInfo() || "\n");

  OBJECT csource := catalog->AddFolderToCatalog(OpenTestsuiteSite()->OpenByPath("testpages")->id, [ definedby := "consilio.test_pagelists" ]);

  testfw->CommitWork();

  // Clear the index
  catalog->ReindexCatalog(); //don't rebuild := TRUE .. this destroys mappings ... https://gitlab.webhare.com/webharebv/codekloppers/-/issues/555
  csource->WaitForIndexingDone();

  INTEGER pagelistfile := OpenTestsuiteSite()->OpenByPath("testpages/pagelisttest")->id;
  RECORD result := RunConsilioSearch("consilio:testfw_subpages", CQMatch("myfield1", "=", "Seven"), [ mapping := [ myfield1 := "", myfield2 := "", title := "" ]]);
  // There was a bug where subpagebaseurl would not be calculated correctly because it was cached from the first file, resulting
  // in the second file not being properly indexed. There should be 2 results.
  TestEq(2, result.totalcount);

  RECORD sub7 := SELECT * FROM result.results WHERE title = "pagelistfile - sub7";
  TestEq("Seven", sub7.myfield1);
  TestEq("", sub7.myfield2);

  result := RunConsilioSearch("consilio:testfw_subpages", CQParseUserQuery("FP_sub10_piece6"), [ mapping := [ myfield1 := "", myfield2 := "", title := "" ]]);
  TestEq(2, result.totalcount);
  TestEqMembers([ title := "Subdocument sub10 piece6", summary := 'This is an overwritten body: <span class="consilio--highlight">FP_sub10_piece6</span>' ], result.results[0], "*");
  TestEqLike("http*sub10.html#piece6", result.results[0].objectid);

  result := RunConsilioSearch("consilio:testfw_subpages", CQParseUserQuery("FP_sub10_piece6"), [ mapping := [ myfield1 := "", myfield2 := "", title := "" ], highlightclass := "" ]);
  TestEq(2, result.totalcount);
  TestEqMembers([ title := "Subdocument sub10 piece6", summary := 'This is an overwritten body: FP_sub10_piece6' ], result.results[0], "*");
  TestEqLike("http*sub10.html#piece6", result.results[0].objectid);
}

RunTestframework([ PTR TestSitemapLinks
                 , PTR TestSubpageIndexing
                 ]);
