interface Dimension {
  width: string;
  height: string;
}

let test: Dimension = {
  width: '200px',
  height: '300px'
};
