<?wh
LOADLIB "wh::javascript.whlib";

LOADLIB "mod::publisher/lib/internal/siteprofiles/compiler.whlib";
LOADLIB "mod::publisher/lib/internal/siteprofiles/reader.whlib";
LOADLIB "mod::publisher/lib/internal/siteprofiles/parser.whlib";
LOADLIB "mod::publisher/lib/internal/siteprofiles/support.whlib";

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/testframework.whlib";
LOADLIB "mod::system/lib/whfs.whlib";

LOADLIB "mod::webhare_testsuite/lib/system/tests.whlib";

OBJECTTYPE FakeContext < > ;


MACRO TestParsing()
{
  TestEq("this_is_a_snake", ToSnakeCase("thisIsASnake"));
  TestEq("thisIsASnake", ToCamelCase("this_is_a_snake"));

  TestEq("x-webhare-scopedtype:webhare_testsuite.global.generic_test_type", ScopedTypeToNamespace("webhare_testsuite:global.genericTestType", FALSE));
  TestEq("x-webhare-scopedtype:webhare_testsuite.global.generic_test_type", ScopedTypeToNamespace("x-webhare-scopedtype:webhare_testsuite.global.generic_test_type", TRUE));

  //x-namespace enforces converting names that look like scoped types to full namespaces in case we find problems in the field with the heuristic approach
  TestEq("x-webhare-scopedtype:urn.my.type", ScopedTypeToNamespace("urn:my.type", TRUE));
  TestEq("urn:my.type", ScopedTypeToNamespace("x-namespace:urn:my.type", TRUE));

  TestThrowsLike('Invalid scoped type*',PTR ScopedTypeToNamespace("x-webhare-scopedtype:webhare_testsuite.global.generic_test_type", FALSE));

  RECORD res;

  res := GetParsedSiteProfile("mod::webhare_testsuite/tests/publisher/siteprofile/data/nosuchfile.siteprl.xml");
  TestEq(1, Length(res.errors));

  res := GetParsedSiteProfile("inline::");
  TestEq(1, Length(res.errors));

  STRING test1resname := "mod::webhare_testsuite/tests/publisher/siteprofile/data/test1.siteprl.xml";
  res := GetParsedSiteProfile(test1resname);
  TestEq(DEFAULT RECORD ARRAY, SELECT * FROM res.errors WHERE resourcename != test1resname); //nothing should sneak by....

  dumpvalue((SELECT * FROM res.errors ORDER BY line),'boxed');
  TestEq(1, Length(SELECT FROM res.errors WHERE message LIKE "*trying to match*")); //Apply <to> rule is trying to match a folder type, but is not set to apply to folders
  TestEq(1, Length(SELECT FROM res.errors WHERE message LIKE "*Duplicate*webhare_testsuite/test1*")); //Duplicate key-sequence [\'http://www.webhare.net/xmlns/webhare_testsuite/test1\'] in key identity-constraint \'{http://www.webhare.net/xmlns/publisher/
  TestEq(2, Length(SELECT FROM res.errors WHERE message LIKE "*No such resource*"));
  TestEq(2, Length(SELECT DISTINCT line FROM res.errors WHERE message LIKE "*No such resource*"));

  TestEq(4, Length(res.errors));


  STRING test3resname := "mod::webhare_testsuite/tests/publisher/siteprofile/data/test3.siteprl.xml";
  res := GetParsedSiteProfile(test3resname);

  RECORD ARRAY sitesettings := SELECT * FROM res.rules WHERE ruletype = "sitesetting";
  TestEq(2, Length(sitesettings));
  TestEq(DEFAULT RECORD, sitesettings[0].sitefilter);
  TestEq([ sitename := "SomeOtherSite" ], sitesettings[1].sitefilter);

  TestEq(1,Length(SELECT FROM res.contenttypes WHERE namespace="http://www.webhare.net/xmlns/webhare_testsuite/test3")); //even though test3 is dupe, should see only one
  RECORD test3type := SELECT * FROM res.contenttypes WHERE namespace="http://www.webhare.net/xmlns/webhare_testsuite/test3";
  TestEq(test3resname, test3type.siteprofile);
  Testeq(5, test3type.line);
  TestEq(TRUE, RecordExists(test3type.foldertype));
  TestEq(FALSE, RecordExists(test3type.filetype));

  //NOTE actual parsing is tested in test_parser.ts, this is just superficial integration testing
  res := GetParsedSiteProfile("mod::webhare_testsuite/tests/publisher/siteprofile/data/test.siteprl.yml");
  TestEqMembers([ yaml := TRUE ], RECORD(SELECT * FROM res.contenttypes WHERE namespace="x-webhare-scopedtype:webhare_testsuite.test.my_yaml_type"), '*');
  TestEqMembers([ yaml := TRUE ], res.rules[0], '*');

  //verifies the major siteprofile doesn't crash:
  res := GetParsedSiteProfile("mod::publisher/data/publisher.siteprl.xml");
}

MACRO TestCompiler()
{
  INTEGER testsiteid := OpenTestsuiteSite()->id;

  OBJECT compiler := NEW RecompileContext();
  RECORD gathersp := compiler->GatherSiteProfiles();
  INTEGER siteprl_neverused_webdesign :=  SearchElement(gathersp.siteprofiles, "mod::webhare_testsuite/webdesigns/basetest/neverused_compiletest.siteprl.xml") + 1;
  INTEGER siteprl_neverused_webfeature := SearchElement(gathersp.siteprofiles, "mod::webhare_testsuite/webfeatures/neverused_feature.siteprl.xml") + 1;
  INTEGER siteprl_module := SearchElement(gathersp.siteprofiles, "mod::webhare_testsuite/data/webhare_testsuite.siteprl.xml") + 1;

  TestEq(TRUE, siteprl_neverused_webdesign > 0);
  TestEq(TRUE, siteprl_neverused_webfeature > 0);
  TestEq(TRUE, siteprl_module > 0);
  TestEq(TRUE, siteprl_module IN gathersp.baseprofiles);
  TestEq([[ siteprofileids := [ siteprl_neverused_webdesign ] ]], SELECT siteprofileids FROM gathersp.profilerefs WHERE isfeature = FALSE AND name = "webhare_testsuite:neverused_compiletest");
  TestEq([[ siteprofileids := [ siteprl_neverused_webfeature ] ]], SELECT siteprofileids FROM gathersp.profilerefs WHERE isfeature = TRUE AND name = "webhare_testsuite:unusedfeature");

  RECORD ARRAY activeprofiles := GetSiteActiveProfileReferrals(gathersp);
  RECORD testsuitesite := SELECT * FROM activeprofiles WHERE id = OpenTestsuiteSite()->id;
  TestEq(2, Length(testsuitesite.siteprofileids));
  TestEq("mod::webhare_testsuite/webdesigns/basetest/basetest.siteprl.xml", gathersp.siteprofiles[testsuitesite.siteprofileids[0]-1]);
  TestEq("mod::webhare_testsuite/webdesigns/basetest/siteprofiles/addtobasetest.siteprl.xml", gathersp.siteprofiles[testsuitesite.siteprofileids[1]-1]);

  RECORD test1_only;
  RECORD res := compiler->CompileSiteprofiles([ siteprofiles := [ "mod::webhare_testsuite/tests/publisher/siteprofile/data/test1.siteprl.xml" ]
                                              , profilerefs := RECORD[]
                                              , baseprofiles := [ 1 ]
                                              ]);

  TestEqStructure([[ col := 0, resourcename := "", line := 0, message := ""]], compiler->GetErrors());

  //First test test3 directly
  compiler := NEW RecompileContext();
  res := compiler->CompileSiteprofiles([ siteprofiles := [ "mod::webhare_testsuite/tests/publisher/siteprofile/data/test3.siteprl.xml" ]
                                       , profilerefs := RECORD[[ isfeature := FALSE, name := "webhare_testsuite:test3", siteprofileids := [1] ]]
                                       , baseprofiles := INTEGER[]
                                       ]);

  TestEq(2, Length(res.result.webrules), "We see 2 rules now, site filtering will need to be done later now");
  TestEq([1], res.result.webrules[0].siteprofileids);

  //Test basesiteprofile against empty system
  compiler := NEW RecompileContext();
  res := compiler->CompileSiteprofiles([ siteprofiles := [ "mod::publisher/data/publisher.siteprl.xml" ]
                                       , profilerefs := RECORD[]
                                       , baseprofiles := [ 1 ]
                                       ]);

  RECORD ARRAY faked_db_contents := SELECT *
                                      FROM system.fs_types
                                     WHERE id < 20;

  UPDATE faked_db_contents SET orphan := TRUE WHERE id = 5;
  UPDATE faked_db_contents SET isfiletype := FALSE, ispublishable := FALSE WHERE id = 4;

  RECORD ARRAY against_empty_db := compiler->PreprocessContentTypes(res.allcontenttypes, faked_db_contents);
  TestEq(TRUE, Length(against_empty_db)>30, 'there should be over 30 contenttypes if basesiteprofile was parsed, we have ' || Length(against_empty_db));

  RECORD mswordfile := SELECT * FROM against_empty_db WHERE namespace="http://www.webhare.net/xmlns/publisher/mswordfile";
  //dumpvalue(mswordfile);
  TestEq(TRUE, RecordExists(mswordfile), "MSWord should be there");
  TestEq(4, mswordfile.id, "And picked up as type #4");
  TestEq(TRUE, RecordExists(mswordfile.filetype), "Parsed as a filetype");
  TestEq(TRUE, mswordfile.filetype.blobiscontent, "blob is content");
  TestEq([isfiletype := TRUE, ispublishable := TRUE ], mswordfile.toupdate, "Upgrade back to publishable filetype!");

  RECORD htmlfile := SELECT * FROM against_empty_db WHERE namespace="http://www.webhare.net/xmlns/publisher/htmlfile";
  TestEq(TRUE, RecordExists(htmlfile), "HTMLFILE should be there");
  TestEq(5, htmlfile.id, "And picked up as type #5");
  TestEq([orphan := FALSE ], htmlfile.toupdate, "Upgrade back to non-orphan type!");

  compiler := NEW RecompileContext();
  res := compiler->CompileSiteprofiles([ siteprofiles := [  "mod::publisher/data/publisher.siteprl.xml", "mod::webhare_testsuite/tests/publisher/siteprofile/data/test1.siteprl.xml" ]
                                       , profilerefs := RECORD[[ isfeature := FALSE, name := "webhare_testsuite:test1", siteprofileids := [2] ]]
                                       , baseprofiles := INTEGER[1]
                                       ]);

  res := compiler->ApplyStandardPostProcessing(res);
  TestEq(FALSE, 1 IN res.hiddenfoldertypes, "Foreign folders used to be hidden, but are now controlled by the userpreference OR siteprofile settings");
  TestEq(TRUE, 2 IN res.hiddenfoldertypes);
  TestEq(FALSE, 0 IN res.hiddenfoldertypes);

  TestEq(TRUE, Length(res.allcontenttypes)>30, 'there should be over 30 contenttypes if basesiteprofiel was parsed, we have ' || Length(res.result.contenttypes));
  TestEq([id:=0], RECORD(SELECT id FROM res.allcontenttypes WHERE namespace="http://www.webhare.net/xmlns/publisher/normalfolder"));
  TestEq([id:=4], RECORD(SELECT id FROM res.allcontenttypes WHERE namespace="http://www.webhare.net/xmlns/publisher/mswordfile"));
}

MACRO TestUnreferencedSiteprofiles()
{
  RECORD fullcompileres := __DoRecompileSiteprofiles(FALSE, FALSE, FALSE);
  //dumpvalue(GetCachedSiteProfiles());
  TestEq(TRUE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/unusedfiletype")), "This type should exist, even though its webdesign/siteprofile is not referred");
  TestEq(TRUE, ObjectExists(OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/unusedfoldertype")), "This type should exist, even though its feature is not referred");

  RECORD testsite_applicability := GetSiteApplicabilityInfo(OpenTestsuiteSite()->id);
  TestEq(2, Length(testsite_applicability.siteprofileids));

  INTEGER ARRAY affectedsites := GetSiteSettingIDs( [ siteprofileids := testsite_applicability.siteprofileids, sitefilter := DEFAULT RECORD ]);
  IF(ObjectExists(OpenTestsuiteAltSite()))
    TestEq(SortArray(INTEGER[OpenTestsuiteSite()->id, OpenTestsuiteAltSite()->id]), SortArray(affectedsites), "*only* our testsite(s) should be affected by its test siteprofiles");
  ELSE
    TestEq(INTEGER[OpenTestsuiteSite()->id], affectedsites, "*only* our testsite(s) should be affected by its test siteprofiles");

  TestEq(INTEGER[OpenTestsuiteSite()->id], GetSiteSettingIDs( [ siteprofileids := testsite_applicability.siteprofileids, sitefilter :=  [ sitename := ToLowercase(OpenTestsuiteSite()->name) ] ]));
  TestEq(INTEGER[], GetSiteSettingIDs( [ siteprofileids := testsite_applicability.siteprofileids, sitefilter :=  [ sitemask := "*backend" ] ]));
}

RunTestframework([ PTR TestParsing
                 , PTR TestCompiler
                 , PTR TestUnreferencedSiteprofiles
                 ]);
