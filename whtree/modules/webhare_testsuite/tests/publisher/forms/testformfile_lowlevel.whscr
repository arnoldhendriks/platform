<?wh
/* Test some low-level stuff oand corner cases in the form api */

LOADLIB "wh::files.whlib";

LOADLIB "mod::system/lib/resources.whlib";
LOADLIB "mod::system/lib/testframework.whlib";
LOADLIB "mod::webhare_testsuite/lib/system/tests.whlib";

LOADLIB "mod::publisher/lib/siteprofiles.whlib";
LOADLIB "mod::publisher/lib/forms/api.whlib";

LOADLIB "mod::tollium/lib/gettid.whlib";

LOADLIB "mod::webhare_testsuite/lib/internal/testsite.whlib";


PUBLIC MACRO BuildForm()
{
  BuildWebtoolForm();
}

MACRO DirectFormStorage1()
{
  OBJECT formfile := OpenTestsuiteSite()->OpenByPath("webtools/form");
  OBJECT webtoolform := OpenFormFileDefinition(formfile);
  OBJECT formresults := OpenFormFileResults(formfile);
  OBJECT liveform := OpenWebtoolForm(formfile->id);

  liveform->^firstname->value := "Pietje";

  RECORD ARRAY fields := liveform->ListFields();
  STRING emailfieldname := SELECT AS STRING name FROM fields WHERE title = ":Email";
  GetMember(liveform, "^" || emailfieldname)->value := "directsubmit1@beta.webhare.net";

  RECORD source := WrapBlob(GetWebHareResource("mod::system/web/tests/snowbeagle.jpg"),"bob.jpg");
  liveform->^upload->value := source;

  RECORD res := liveform->FormExecuteSubmit([ extradata := [x := 42] ]);
  TestEq(1, formresults->GetResultsCount());

  RECORD submitted := formresults->GetSingleResult(res.result.resultsguid);
  TestEqLike("mod::webhare_testsuite/*", submitted.useragent);
  TestEq([ x := 42 ], submitted.extradata);

  //Open in edit mode
  liveform := webtoolform->__OpenAsLiveForm("en", DEFAULT OBJECT);
  liveform->PrepareForFrontend(); //as we called a lowlevel API we're responsible for proper init
  liveform->EditExistingResult(res.result.resultsguid);

  TestEqMembers(source, liveform->^upload->value, "*,-__BLOBSOURCE,-HASH,-DOMINANTCOLOR");
  TestEq(TRUE, liveform->^upload->value.hash != "", "should have a hash after storage");
  TestEqLike("#??????", liveform->^upload->value.dominantcolor);

  //test image hosting for editing
  RECORD imghost := liveform->^upload->__GetRenderData();
  //dumpvalue(imghost);
  TestEq(TRUE, imghost.value.link != "");

  //Make sure image stays after editing something else
  liveform->^firstname->value := "Kereltje";

  liveform->FormExecuteSubmit();
  TestEq(1, formresults->GetResultsCount()); //should have been an edit

  submitted := formresults->GetSingleResult(res.result.resultsguid);
  //dumpvalue(submitted);
  TestEq("Kereltje", submitted.response.firstname);
  TestEqMembers(source, submitted.response.upload, "*,-__BLOBSOURCE,-HASH,-DOMINANTCOLOR");
}

MACRO DirectFormStorage2()
{
  OBJECT formfile := OpenTestsuiteSite()->OpenByPath("webtools/form");
  OBJECT webtoolform := OpenFormFileDefinition(formfile);
  OBJECT formresults := OpenFormFileResults(formfile);
  OBJECT liveform := webtoolform->__OpenAsLiveForm("en", DEFAULT OBJECT);
  liveform->PrepareForFrontend(); //as we called a lowlevel API we're responsible for proper init

  RECORD ARRAY fields := liveform->ListFields();
  STRING emailfieldname := SELECT AS STRING name FROM fields WHERE title = ":Email";
  GetMember(liveform, "^" || emailfieldname)->value := "directsubmit2@beta.webhare.net";

  RECORD source := WrapBlob(StringToBlob("Gimme gimme gimme"), "man(1).txt");
  liveform->^upload->value := source;

  RECORD res := liveform->FormExecuteSubmit();
  TestEq(2, formresults->GetResultsCount());

  //Open in edit mode
  liveform := webtoolform->__OpenAsLiveForm("en", DEFAULT OBJECT);
  liveform->PrepareForFrontend(); //as we called a lowlevel API we're responsible for proper init
  liveform->EditExistingResult(res.result.resultsguid);

  TestEqMembers(source, liveform->^upload->value, "*,-__BLOBSOURCE,-HASH,-DOMINANTCOLOR");

  //test file hosting for editing
  RECORD filehost := liveform->^upload->__GetRenderData();
  TestEq(TRUE, filehost.value.link != "");

  liveform->FormExecuteSubmit();
}

MACRO VerifyAndCleanupMail()
{
  //flush tasks and collect our mail
  testfw->FlushTasks(["publisher:mailresults", "publisher:mailfeedback"], [ acceptcancel := TRUE ]);
  TestEq(4, Length(testfw->ExtractAllMailFor("mailresult+jstest@beta.webhare.net", [ count := 0, returnallmail := TRUE ])));
}

MACRO WorkHandlingErrors()
{
  OBJECT liveform := GetWebContext(OpenTestsuiteSite()->id)->OpenForm("workhandlingerrorsform");

  // Test no transaction leaking, and correct handling of work errors
  liveform->^mode->value := "nofinish";
  TestThrowsLike("*returned with work still open*", PTR liveform->FormExecuteSubmit());

  liveform->^mode->value := "normal";
  TestEQ(DEFAULT RECORD, liveform->FormExecuteSubmit().result);

  liveform->^mode->value := "throws";
  TestThrowsLike("Form exception", PTR liveform->FormExecuteSubmit());

  liveform->^mode->value := "normal";
  TestEQ(DEFAULT RECORD, liveform->FormExecuteSubmit().result);

  liveform->^mode->value := "doublework";
  TestEQ(DEFAULT RECORD, liveform->FormExecuteSubmit().result);

  liveform->^mode->value := "doublebegin";
  TestThrowsLike("*another work object is still open*", PTR liveform->FormExecuteSubmit());

  liveform->^mode->value := "normal";
  TestEQ(DEFAULT RECORD, liveform->FormExecuteSubmit().result);

  liveform->^twolevel->value := [ throwsomething := TRUE ];
  TestThrowsLike("*throwsomething was present in the value record*", PTR liveform->FormExecuteSubmit());

  liveform->^mode->value := "nofinish";
  liveform->^twolevel->value := [ throwsomething := TRUE ];
  TestThrowsLike("*throwsomething was present in the value record*", PTR liveform->FormExecuteSubmit());
}

MACRO TestAttributeMatching()
{
  SetTidLanguage("nl");
  OBJECT liveform := GetWebContext(OpenTestsuiteSite()->OpenByPath("testpages/formtest")->id)->OpenForm("coretest", [ formref := "CORE-TEST-FORM" ]);

  TestEQ(TRUE, "wh-testsuite-matchattributes-type1" IN liveform->^matchattributes_type1->groupclasses);
  TestEQ(TRUE, "wh-testsuite-matchattributes-type2-true" IN liveform->^matchattributes_type2_true->groupclasses);
  TestEQ(TRUE, "wh-testsuite-matchattributes-type2-false" IN liveform->^matchattributes_type2_false->groupclasses);
}

RunTestframework([ PTR BuildForm
                 , PTR DirectFormStorage1
                 , PTR DirectFormStorage2
                 , PTR VerifyAndCleanupMail
                 , PTR WorkHandlingErrors
                 , PTR TestAttributeMatching
                 ]);
