<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::files.whlib";

LOADLIB "mod::publisher/lib/internal/contentlibraries/publishing.whlib";
LOADLIB "mod::system/lib/testframework.whlib";
LOADLIB "mod::system/lib/whfs.whlib";

LOADLIB "mod::tollium/lib/testframework.whlib";

LOADLIB "mod::webhare_testsuite/lib/system/tests.whlib";

LOADLIB "relative::libs/adaptivecontent.whlib";

RECORD settings;

MACRO PrepConnectMgr()
{
  settings := SetupAdaptiveContentTest();

  TestEq([[ namespace := "http://www.webhare.net/xmlns/webhare_testsuite/contentslot" ]
         ,[ namespace := "http://www.webhare.net/xmlns/webhare_testsuite/headerslot" ]
         ], SELECT namespace FROM settings.slotstore->ListAllowedSlotTypes() ORDER BY namespace);

  TestEq([[ namespace := "http://www.webhare.net/xmlns/webhare_testsuite/headeroption1" ]
         ,[ namespace := "http://www.webhare.net/xmlns/webhare_testsuite/headeroption2" ]
         ], SELECT namespace FROM settings.headerslot->ListAllowedWidgetTypes());

  TestEq([[ namespace := "http://www.webhare.net/xmlns/webhare_testsuite/contentoption1" ]
         ,[ namespace := "http://www.webhare.net/xmlns/webhare_testsuite/contentoption2" ]
         ], SELECT namespace FROM settings.slot1->ListAllowedWidgetTypes());

  TestThrowsLike('*not*allowed*', PTR settings.slotstore->CreateSlot([ typens := "http://www.webhare.net/xmlns/webhare_testsuite/contentoption1"
                                                                     , title := "should fail"
                                                                     ]));

  TestThrowsLike('*not*allowed*', PTR settings.headerslot->CreateWidget([ typens := "http://www.webhare.net/xmlns/webhare_testsuite/contentoption1"
                                                                        , title := "should fail"
                                                                        ]));
}

ASYNC MACRO TestDCComponents()
{
  OBJECT actestpage := OpenTestsuiteSite()->OpenByPath("actests/index", [ expect := "file" ]);
  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("publisher", [ params := [ STRING(actestpage->whfspath) ] ]));
  AWAIT ExpectScreenChange(+1, PTR TTClick("props"));
  OBJECT footerdcslot := topscreen->GetExtendedComponent("http://www.webhare.net/xmlns/webhare_testsuite/actestpage", "footerdcslot");
  TestEq(0, footerdcslot->value);
  TestEq(DEFAULT RECORD, footerdcslot->selection);
  TestEq(2, Length(footerdcslot->options));
  TestEQ("Headerslot", footerdcslot->options[1].title);

  OBJECT contentdcslot := topscreen->GetExtendedComponent("http://www.webhare.net/xmlns/webhare_testsuite/actestpage", "contentdcslot");
  TestEq(0, contentdcslot->value);
  TestEq(3, Length(contentdcslot->options));
  TestEq(1, Length(SELECT FROM contentdcslot->options WHERE title = "a slot")); //this is slot #2

  AWAIT ExpectAndAnswerMessageBox("ok", PTR TTClick(":Ok"), [ messagemask := "*Content slot*required*"]); //cannot submit, must select slot

  contentdcslot->value := SELECT AS INTEGER rowkey FROM contentdcslot->options WHERE title = "a slot";

  AWAIT ExpectScreenChange(-1, PTR TTClick(":Ok"));

  TestEq(GetTestsuiteTempFolder()->OpenByPath("slots/a-slot-2")->id, actestpage->GetInstancedata("http://www.webhare.net/xmlns/webhare_testsuite/actestpage").contentdcslot);

  AWAIT ExpectScreenChange(+1, PTR TTClick("props"));
  contentdcslot := topscreen->GetExtendedComponent("http://www.webhare.net/xmlns/webhare_testsuite/actestpage", "contentdcslot");
  TestEq("a slot", contentdcslot->selection.title);
  AWAIT ExpectScreenChange(-1, PTR TTClick(":Ok"));
  AWAIT ExpectScreenChange(-1, PTR TTEscape);
}

ASYNC MACRO TestAdaptiveContentBeacons()
{
  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("publisher", [ params := ["/webhare-tests/webhare_testsuite.testsite/tmp/beacons" ]]));
  TestEq(3, Length(TT("filelist")->rows));

  AWAIT ExpectScreenChange(+1, PTR TTClick("newfolder"));
  TestEq(RECORD[], TT("types")->rows, "All folder types should be blocked"); //and a new publisher UI should remove the need for an empty newfolder dialog
  AWAIT ExpectScreenChange(-1, PTR TTEscape);

  AWAIT ExpectScreenChange(+1, PTR TTClick("newfile"));
  TT(":Title")->value := "Beacon Delta";
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));

  TestEq("Beacon Delta",TT("filelist")->selection.title);

  TT("filelist")->selection := SELECT * FROM TT("filelist")->rows WHERE name = "is-employee";
  AWAIT ExpectScreenChange(+1, PTR TTClick("open"));
  TestEq("Is Employee", TT(":Title")->value);

  TT(":Title")->value := "Employee Beacon";
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));

  TestEq("Employee Beacon", TT("filelist")->selection.title);
  TestEq("is-employee", TT("filelist")->selection.name);

  AWAIT ExpectScreenChange(+1, PTR TTClick(":Properties"));
  TestEq(2, Length(TT("fragment2!documents")->rows)); //We lack a clean way to find Documents on the References tab, so for now fragment2 will work
  AWAIT ExpectScreenChange(-1, PTR TTEscape);

  AWAIT ExpectScreenChange(-1, PTR TTEscape);
}

ASYNC MACRO TestAdaptiveContentTriggerBeaconWidget()
{
  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("publisher:edit", [ calltype := "direct", params := [ GetTestsuiteTempFolder()->whfspath || "beacondoc" ], target := DEFAULT RECORD ]));
  AWAIT ExpectScreenChange(+1, PTR TT("doceditor")->contents->rte->ProcessInboundMessage("buttonclick", [ button := "object-insert" ]));
  TT("contenttypes")->selection := SELECT * FROM TT("contenttypes")->rows WHERE rowkey = "http://www.webhare.net/xmlns/publisher/widgets/triggerbeacon";
  TestEq(TRUE, RecordExists(TT("contenttypes")->selection));
  AWAIT ExpectScreenChange(0, PTR TTClick(":OK"));

  TestEq(TRUE, RecordExists(SELECT FROM TT(":Beacon")->options WHERE title="Employee Beacon"));
  TT(":Beacon")->selection := SELECT * FROM TT(":Beacon")->options WHERE title="Employee Beacon";

  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));
  AWAIT ExpectAndAnswerMessageBox("yes", PTR TTClick("publishbutton")); //answer to dialog: are you sure you want to publish?
  AWAIT ExpectScreenChange(-1, PTR TTEscape);
}

ASYNC MACRO TestAdaptiveContentSlots()
{
  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("publisher", [ params := ["/webhare-tests/webhare_testsuite.testsite/tmp/slots" ]]));
  TestEq(3, Length(TT("filelist")->rows));

  AWAIT ExpectScreenChange(+1, PTR TTClick("newfile"));
  TestEq(RECORD[], TT("types")->rows, "All filetypes should be blocked"); //and a new publisher UI should remove the need for an empty newfile dialog
  AWAIT ExpectScreenChange(-1, PTR TTEscape);

  AWAIT ExpectScreenChange(+1, PTR TTClick("newfolder"));
  TestEq(2, Length(TT("type")->options));
  TT("type")->value := OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/contentslot")->id;
  TT("title")->value := "Content slot alpha";

  //TODO not sure if we should be setting a name. might be more trouble than it actually adds. see also https://gitlab.webhare.com/addons/connect/-/issues/8
  AWAIT ExpectScreenChange(-1, PTR TTClick(":Ok"));

  TestEq("Content slot alpha", TT("filelist")->selection.title);
  TestEq(4, Length(TT("filelist")->rows));

  AWAIT ExpectScreenChange(+1, PTR TTClick(":Properties"));
  TestEq("Content slot alpha", TT(":Title")->value);
  TT(":Title")->value := "Content slot alpha!";

  AWAIT ExpectScreenChange(-1, PTR TTClick(":Ok"));

  TestEq("Content slot alpha!", TT("filelist")->selection.title);
  INTEGER content_slot_alpha_id := TT("filelist")->selection.rowkey;

  TestEq(4, Length(TT("filelist")->rows));
  TTClick("open");

  //create a widget
  DATETIME expect_change_after := GetCurrentDatetime();
  AWAIT ExpectScreenChange(+1, PTR TTClick(":New file"));
  TestEq(2, Length(TT("types")->rows));
  TT("types")->selection := SELECT * FROM TT("types")->rows WHERE title = "ContentOption1";
  AWAIT ExpectScreenChange(0, PTR TTClick(":Ok"));
  TT(":Widget name")->value := "Contents option 1 alpha";
  TT(":Text")->value := [ htmltext := StringToBlob("My text") ];

  TestEq("Content", TT("extensiontabs")->pages[0]->title);
  TestEq("Conditions", TT("extensiontabs")->pages[1]->title);

  //basic conditions test. TODO more testing but the API isn't that test friendly yet I think. probably deservers a separate test anyway
  TestEq("No condition", TT("widgetsettings")->^condition->GetChildComponents()[0]->^field->selection.title);
  TT("widgetsettings")->^condition->GetChildComponents()[0]->^field->value := "newvisitor";
  TestEq("New visitor", TT("widgetsettings")->^condition->GetChildComponents()[0]->^field->selection.title);

  AWAIT ExpectScreenChange(-1, PTR TTClick(":Ok"));

  TestEq("Contents option 1 alpha", TT("filelist")->selection.title);

  WHILE(RecordExists(SELECT FROM ListSlotsNeedingRepublish() WHERE id = content_slot_alpha_id))
    Sleep(100);
  TestEq(1, Length(GetPublishedSlotInfo(content_slot_alpha_id).widgets));
  TestEq([ _type := "newvisitor" ], GetPublishedSlotInfo(content_slot_alpha_id).widgets[0].condition);

  AWAIT ExpectScreenChange(+1, PTR TTClick("open"));
  TestEq("Contents option 1 alpha", TT("filetitle")->value);
  TestEqLike("*My text*", BlobToString(TT(":Text")->value.htmltext));
  TestEq("New visitor", TT("widgetsettings")->^condition->GetChildComponents()[0]->^field->selection.title);
  TT("widgetsettings")->^condition->GetChildComponents()[0]->^field->value := ":and";

  TT("widgetsettings")->^condition->GetChildComponents()[0]->GetChildComponents()[0]->^field->value := "returningvisitor";
  TT("widgetsettings")->^condition->GetChildComponents()[0]->GetChildComponents()[0]->^addcondition->TolliumClick();

  TT("widgetsettings")->^condition->GetChildComponents()[0]->GetChildComponents()[1]->^field->value := "visitbeforedate";
  AWAIT ExpectAndAnswerMessageBox("ok", PTR TTClick(":Ok"), [ messagemask := "*Visiting before*required*"]); //verify errorlabel tid bindings for the deeper components
  TT("widgetsettings")->^condition->GetChildComponents()[0]->GetChildComponents()[1]->__fragmentobjs[0]->^date->value := MakeDatetime(2021,4,13,14,3,0);
  AWAIT ExpectScreenChange(-1, PTR TTClick(":Ok"));

  TestEq("Returning visitor and Visiting before 13-04-2021 16:03", TT("filelist")->selection[0].custom_condition, "Make sure timezones are applied");

  WHILE(RecordExists(SELECT FROM ListSlotsNeedingRepublish() WHERE id = content_slot_alpha_id))
    Sleep(100);

  TestEq("and", GetPublishedSlotInfo(content_slot_alpha_id).widgets[0].condition._type);

  AWAIT ExpectAndAnswerMessageBox("yes", PTR TTClick("delete"));

  //wait for the slot json file to show '0 widgets'
  WHILE(RecordExists(SELECT FROM ListSlotsNeedingRepublish() WHERE id = content_slot_alpha_id))
    Sleep(100);
  TestEq(0, Length(GetPublishedSlotInfo(content_slot_alpha_id).widgets));

  topscreen->frame->focused := TT("foldertree");

  //wait for the slot json file to go away
  AWAIT ExpectAndAnswerMessageBox("yes", PTR TTClick("delete"));
  WHILE(RecordExists(GetPublishedSlotInfo(content_slot_alpha_id)))
    Sleep(100);

  AWAIT ExpectScreenChange(-1, PTR TTEscape);
}

ASYNC MACRO TestAdaptiveContentReordering()
{
  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("publisher", [ params := ["/webhare-tests/webhare_testsuite.testsite/tmp/slots/a-slot" ]]));

  TT("sortorderselect")->value := "ordering"; //FIXME shouldn't this be forced on us? see addons/connect#41

  TestEq(["Widget 1A","Widget 1B","Widget 1C"], SELECT AS STRING ARRAY title FROM TT("filelist")->rows);
  TestEq(["widget-1a","widget-1b","widget-1c"], SELECT AS STRING ARRAY name FROM GetPublishedSlotInfo(settings.slot1->id).widgets);
  topscreen->frame->focused := TT("filelist");
  TT("filelist")->selection := SELECT * FROM TT("filelist")->rows WHERE title = "Widget 1B";
  TTClick(":Move up", [ menuitem := TRUE ]);
  TestEq(["Widget 1B","Widget 1A","Widget 1C"], SELECT AS STRING ARRAY title FROM TT("filelist")->rows);

  WHILE(RecordExists(SELECT FROM ListSlotsNeedingRepublish() WHERE id = settings.slot1->id))
    Sleep(100);

  TestEq(["widget-1b","widget-1a","widget-1c"], SELECT AS STRING ARRAY name FROM GetPublishedSlotInfo(settings.slot1->id).widgets);

  AWAIT ExpectScreenChange(-1, PTR TTEscape);
}

ASYNC MACRO TestAdaptiveContentReferences()
{
  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("publisher", [ params := ["/webhare-tests/webhare_testsuite.testsite/tmp/slots/a-slot-2" ]]));

  AWAIT ExpectScreenChange(+1, PTR TTClick(":Properties"));
  TestEq(1, Length(TT("fragment2!documents")->rows)); //We lack a clean way to find Documents on the References tab, so for now fragment2 will work
  AWAIT ExpectScreenChange(-1, PTR TTEscape);

  AWAIT ExpectScreenChange(-1, PTR TTEscape);
}

RunTestFramework([ PTR PrepConnectMgr
                 , PTR TestDCComponents
                 , PTR TestAdaptiveContentBeacons
                 , PTR TestAdaptiveContentTriggerBeaconWidget
                 , PTR TestAdaptiveContentSlots
                 , PTR TestAdaptiveContentReordering
                 , PTR TestAdaptiveContentReferences
                 ], [ testusers := [[ login := "sysop", grantrights := ["system:sysop"] ]
                                   ,[ login := "superuser"]
                                   ]
                    ]);
