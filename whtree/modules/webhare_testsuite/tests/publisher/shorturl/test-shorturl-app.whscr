<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::ooxml/spreadsheet.whlib";

LOADLIB "mod::tollium/lib/testframework.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/testframework.whlib";

LOADLIB "mod::publisher/lib/publisher.whlib";
LOADLIB "mod::publisher/lib/shorturl.whlib";

LOADLIB "relative::shorturltests.whlib";
LOADLIB "mod::webhare_testsuite/lib/system/tests.whlib";

ASYNC MACRO TestShortURLAPP_AsSysop()
{
  AWAIT ExpectScreenChange(+2, PTR TTLaunchApp("publisher:shorturl"));

  AWAIT ExpectScreenChange(+1, PTR TTClick(":Add"));
  TT("domain")->value := "test.bea.gl";
  TT("baseurl")->value := "https://www.example.net/";
  AWAIT ExpectAndAnswerMessagebox("Ok",PTR TTClick(":Ok"), [ messagemask := "*ot*hosted *" ]);
  TT("baseurl")->value := GetPrimaryWebhareInterfaceURL() || "testoutput/webhare_testsuite.shorturl/";
  AWAIT ExpectScreenChange(-1, PTR TTClick(":Ok"));

  TestEq("test.bea.gl", TT("domains")->selection.domain, "Should autoselect the new domain");
  AWAIT ExpectScreenChange(-1, PTR TTClick(":Open"));

  TestEqLike("*test.bea.gl*", topscreen->frame->title);

  //Add a URL
  AWAIT ExpectScreenChange(+1, PTR TTClick(":Add"));
  TT("target")->value := MakeIntExtExternalLink("https://beta.webhare.net/");
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));

  TestEq("https://beta.webhare.net/", TT("shorturls")->selection.targeturl);

  //Verify preview panel
  OBJECT testdomain := OpenShortURLDomain("test.bea.gl");
  TestEqLike(testdomain->baseurl || TT("shorturls")->selection.shortname, TT("previewbrowser")->GetPreviewState().displayurl);
  testfw->browser->autofollow := FALSE;

  AWAIT ExpectScreenChange(+1, PTR TTClick(":Edit"));
  TestEq(MakeIntExtExternalLink("https://beta.webhare.net/"), TT("target")->value);
  OBJECT newtarget := OpenTestsuiteSite()->OpenByPath("testpages/staticpage-ps-af");
  TT("target")->value := MakeIntExtInternalLink(newtarget->id, "?appendstuff");

  AWAIT ExpectScreenChange(-1, PTR TTClick(":Ok"));

  TestEq(newtarget->link || "?appendstuff", TT("shorturls")->selection.targeturl);

  AWAIT ExpectScreenChange(+1, PTR TTClick(":Edit"));
  TT("shortname")->value := "short";
  TT("description")->value := "My First URL";
  TT("expirationdate")->localtzvalue := MakeDatetime(2047,5,1,12,0,0);
  AWAIT ExpectScreenChange(-1, PTR TTClick(":Ok"));

  AWAIT ExpectScreenChange(+1, PTR TTClick(":Edit"));
  TestEq("My First URL", TT("description")->value);
  TestEq(MakeDatetime(2047,5,1,12,0,0), TT("expirationdate")->localtzvalue);
  AWAIT ExpectScreenChange(-1, PTR TTEscape);

  //Bulk create
  AWAIT ExpectScreenChange(+1, PTR TTClick("bulkcreate"));
  TT("importstr")->value := "https://www.webhare.dev/link1\n/www.webhare.dev/link5";
  AWAIT ExpectAndAnswerMessagebox("ok",PTR TTClick(":Create URLs"), [ messagemask := "*Invalid*www.webhare.dev/link5*" ]);
  TT("importstr")->value := "https://www.webhare.dev/link6\n https://www.webhare.dev/link2";
  AWAIT ExpectScreenChange(-1, PTR TTClick(":Create URLs"));

  TestEqMembers([[ targeturl := "https://www.webhare.dev/link2" ]
                ,[ targeturl := "https://www.webhare.dev/link6" ]
                ], (SELECT * FROM TT("shorturls")->rows WHERE targeturl LIKE "https://www.webhare.dev/*" ORDER BY targeturl),"*");
  TestEqMembers([[ targeturl := "https://www.webhare.dev/link2" ]
                ,[ targeturl := "https://www.webhare.dev/link6" ]
                ], (SELECT * FROM TT("shorturls")->selection ORDER BY targeturl),"*");

   AWAIT ExpectScreenChange(+1, PTR TTClick("exportselection"));
  RECORD ARRAY rows := GetOOXMLSpreadsheetRows((AWAIT ExpectSentWebFile(PTR TTClick(":Download"))).file.data);
  TestEqMembers([[ "target url" := "https://www.webhare.dev/link2" ]
                ,[ "target url" := "https://www.webhare.dev/link6" ]
                ], rows, "*");
  TestEqLike(testdomain->baseurl || "??*", rows[0]."short url");
  AWAIT ExpectScreenChange(-1, DEFAULT MACRO PTR); //download self-closes

  AWAIT ExpectScreenChange(-1, PTR TTEscape); //Close app

  testfw->BeginWork();
  OBJECT testdomain2 := CreateShortURLDomain("test2.bea.gl", [ baseurl := GetPrimaryWebhareInterfaceURL() || "testoutput/webhare_testsuite.shorturl2/" ]);
  testdomain2->CreateExternalShortUrl("https://www.webhare.dev/", [ shortname := "testje" ]);
  testfw->CommitWork();

  //Launching the app should just jump straight into the last used domain
  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("publisher:shorturl"));
  TestEqLike("*test.bea.gl*", topscreen->frame->title);
  TestEqMembers([[ targeturl := "https://www.webhare.dev/link2" ]
                ,[ targeturl := "https://www.webhare.dev/link6" ]
                ], (SELECT * FROM TT("shorturls")->rows WHERE targeturl LIKE "https://www.webhare.dev/*" ORDER BY targeturl),"*");

  //Select the other domain
  AWAIT ExpectScreenChange(+1, PTR TTClick("selectdomain"));
  TT("domains")->selection := SELECT * FROM TT("domains")->rows WHERE domain = "test2.bea.gl";
  AWAIT ExpectScreenChange(-1, PTR TTClick(":Open"));

  TestEq([[ shortname := "testje" ]], SELECT shortname FROM TT("shorturls")->rows ORDER BY shortname);
  TestEqLike("*test2.bea.gl*", topscreen->frame->title);

  //Test list extension
  TestEq("WHFS Path", SELECT AS STRING title FROM TT("shorturls")->columns WHERE name = "testbeagle2_whfspath");
  TestEq(["/webhare-private/system/shorturl/test2.bea.gl/testje"], SELECT AS STRING ARRAY testbeagle2_whfspath FROM TT("shorturls")->rows);

  //Test the window extensions
  AWAIT ExpectScreenChange(+1, PTR TTClick(":Add"));
  TT("target")->value := MakeIntExtExternalLink("https://beta.webhare.net/sublink");
  TT(":StringTest")->value := "Val1";
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));

  AWAIT ExpectScreenChange(+1, PTR TTClick(":Edit"));
  TestEq("Val1", TT(":StringTest")->value);
  TT(":StringTest")->value := "Val2";
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));

  AWAIT ExpectScreenChange(-1, PTR TTEscape);
}

ASYNC MACRO TestShortURLAPP_AsUser()
{
  OBJECT testdomain := OpenShortURLDomain("test.bea.gl");
  testfw->BeginWork();
  testfw->GetUserObject("sysop")->UpdateGrant("grant", "publisher:manageshorturls", testdomain->id, testfw->GetUserObject("lisa"), [ allowselfassignment := TRUE ]);
  testfw->CommitWork();

  testfw->SetTestUser("lisa");
  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("publisher:shorturl"));
  TestEqLike("*test.bea.gl*", topscreen->frame->title, "test.bea.gl should have been autoselected");

  AWAIT ExpectScreenChange(+1, PTR TTClick("selectdomain"));
  TestEq(["test.bea.gl"],SELECT AS STRING ARRAY domain FROM TT("domains")->rows);
  TT("domains")->selection := TT("domains")->rows;
  TestEq(FALSE, TTClick(":Edit", [ allowfailure := TRUE ]));
  TestEq(FALSE, TTClick(":Delete", [ allowfailure := TRUE ]));
  AWAIT ExpectScreenChange(-1, PTR TTClick(":Open"));

  AWAIT ExpectScreenChange(+1, PTR TTClick("selectdomain"));
  AWAIT ExpectScreenChange(-1, PTR TTEscape);

  TT("shorturls")->selection := SELECT * FROM TT("shorturls")->rows WHERE shortname = "short";
  topscreen->frame->focused := TT("shorturls");

  AWAIT ExpectScreenChange(+1, PTR TTClick(":Edit"));
  TestEq("My First URL", TT("description")->value);
  TT("description")->value := "The short url";
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));

  TT("searchfor")->value := "link6";
  TTClick(":Search");
  TestEqMembers([[ targeturl := "https://www.webhare.dev/link6" ]], TT("shorturls")->rows, "*");

  AWAIT ExpectScreenChange(-1, PTR TTEscape); //close app
}

ASYNC MACRO TestShortURLAPP_AsSysop_2()
{
  testfw->SetTestUser("sysop");
  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("publisher:shorturl"));
  AWAIT ExpectScreenChange(+1, PTR TTClick("selectdomain"));

  TT("domains")->selection := SELECT * FROM TT("domains")->rows WHERE domain = "test2.bea.gl";

  AWAIT ExpectScreenChange(+1, PTR TTClick(":Edit"));
  TestEq(GetPrimaryWebhareInterfaceURL() || "testoutput/webhare_testsuite.shorturl2/", TT("baseurl")->value);

  //set a variable
  AWAIT ExpectScreenChange(+1, PTR TTClick("setvariables->add"));
  TT(":URL variable")->value := "x";
  TT(":Value")->value := "42";
  AWAIT ExpectScreenChange(-1, PTR TTClick(":Ok"));

  //set a domain whitelist
  AWAIT ExpectScreenChange(+1, PTR TTClick("allowedurls->add"));
  TT(":Domain mask")->value := "*.webhare.dev";
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));

  TT("baseurl")->value := GetPrimaryWebhareInterfaceURL() || "testoutput/webhare_testsuite.shorturl3/";
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));

  AWAIT ExpectScreenChange(-1, PTR TTClick(":Open"));

  //test domain whitelist
  AWAIT ExpectScreenChange(+1, PTR TTClick(":Add"));
  TT("target")->value := MakeIntExtExternalLink("https://gamma.webhare.net/");
  TT(":StringTest")->value := "Val1";
  AWAIT ExpectAndAnswerMessagebox("Ok",PTR TTClick(":Ok"), [ messagemask := "*domain*gamma.webhare.net*" ]);
  TT("target")->value := MakeIntExtExternalLink("https://gamma.webhare.dev/");
  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));

  testfw->browser->autofollow := FALSE;
  TestEq(TRUE, testfw->browser->GotoWebPage(GetPrimaryWebhareInterfaceURL() || "testoutput/webhare_testsuite.shorturl3/testje"));
  TestEq("https://www.webhare.dev/?x=42", testfw->browser->GetResponseHeader("Location"));

  AWAIT ExpectScreenChange(+1, PTR TTClick("selectdomain"));
  TT("domains")->selection := SELECT * FROM TT("domains")->rows WHERE domain = "test2.bea.gl";
  AWAIT ExpectAndAnswerMessagebox("yes",PTR TTClick(":Delete"), [ messagemask := "*delete*test2.bea.gl*" ]);
  TestEq(DEFAULT RECORD, TT("domains")->selection);

  AWAIT ExpectScreenChange(-1, PTR TTEscape);

  AWAIT ExpectScreenChange(-1, DEFAULT MACRO PTR); //app autocloses once last domain is deleted
}

RunTestFramework([ PTR CleanupShortURLTests
                 , PTR TestShortURLAPP_AsSysop
                 , PTR TestShortURLAPP_AsUser
                 , PTR TestShortURLAPP_AsSysop_2
                 ], [ testusers := [[ login := "sysop", grantrights := ["system:sysop"] ]
                                   ,[ login := "lisa" ]
                                   ]]);
