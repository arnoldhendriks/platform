<?wh

LOADLIB "mod::system/lib/testframework.whlib";

LOADLIB "mod::publisher/lib/publisher.whlib";

LOADLIB "mod::webhare_testsuite/lib/system/tests.whlib";

LOADLIB "mod::tollium/lib/testframework.whlib";

ASYNC MACRO TestIntExtLink()
{
  OBJECT screen := GetTestController()->LoadScreen("webhare_testsuite:tests/anycomponent.anycomponent",
                                                   [ component := "http://www.webhare.net/xmlns/publisher/components:intextlink"
                                                   , settings := DEFAULT RECORD
                                                   ]);
  AWAIT ExpectScreenChange(+1, PTR screen->RunModal());
  TestEQ(DEFAULT RECORD, topscreen->comp->value);

  // Set external link
  topscreen->comp->value := [ append := "", externallink := "http://example.com", internallink := 0 ];
  TestEQ([ append := "", externallink := "http://example.com", internallink := 0 ], topscreen->comp->value);

  // Set empty externallink link, should clear it - some users expect this
  topscreen->comp->value := [ append := "", externallink := "", internallink := 0 ];
  TestEQ(DEFAULT RECORD, topscreen->comp->value);
  TestEQ("", topscreen->comp->^external->value);

  // Set internal link
  topscreen->comp->value := [ append := "", externallink := "", internallink := GetTestsuiteTempFolder()->id ];
  TestEQ([ append := "", externallink := "", internallink := GetTestsuiteTempFolder()->id ], topscreen->comp->value);

  // Toggle required, shouldn't affect the link
  topscreen->comp->required := TRUE;
  TestEQ([ append := "", externallink := "", internallink := GetTestsuiteTempFolder()->id ], topscreen->comp->value);
  topscreen->comp->required := FALSE;
  TestEQ([ append := "", externallink := "", internallink := GetTestsuiteTempFolder()->id ], topscreen->comp->value);

  // Set default record, should clear too
  topscreen->comp->value := DEFAULT RECORD;
  TestEQ(DEFAULT RECORD, topscreen->comp->value);

  // Change to external link again (had clearing bug)
  topscreen->comp->value := [ append := "", externallink := "http://example.com", internallink := 0 ];
  TestEQ([ append := "", externallink := "http://example.com", internallink := 0 ], topscreen->comp->value);
  TestEQ("http://example.com", topscreen->comp->^external->value);

  // Swapping this to internal and back to external should not clear the field
  topscreen->comp->SetLinkType("internal");
  topscreen->comp->SetLinkType("external");
  TestEQ("http://example.com", topscreen->comp->^external->value);

  // Change to internal link again (had clearing bug)
  topscreen->comp->value := [ append := "", externallink := "", internallink := GetTestsuiteTempFolder()->id ];
  TestEQ([ append := "", externallink := "", internallink := GetTestsuiteTempFolder()->id ], topscreen->comp->value);

  // With append
  topscreen->comp->value := [ append := "#jo", externallink := "", internallink := GetTestsuiteTempFolder()->id ];
  TestEQ([ append := "#jo", externallink := "", internallink := GetTestsuiteTempFolder()->id ], topscreen->comp->value);

  // Test conversion to external link (without index doc)
  topscreen->comp->SetLinkType("external");
  TestEQ([ append := "", externallink := GetTestsuiteTempFolder()->url || "#jo", internallink := 0 ], topscreen->comp->value);

  // Test conversion to internal link (without index doc)
  topscreen->comp->SetLinkType("internal");
  TestEQ([ append := "#jo", externallink := "", internallink := GetTestsuiteTempFolder()->id ], topscreen->comp->value);

  // Now with '/' append
  topscreen->comp->value := [ append := "/jo", externallink := "", internallink := GetTestsuiteTempFolder()->id ];
  TestEQ([ append := "/jo", externallink := "", internallink := GetTestsuiteTempFolder()->id ], topscreen->comp->value);

  // Test conversion to external link
  topscreen->comp->SetLinkType("external");
  TestEQ([ append := "", externallink := GetTestsuiteTempFolder()->url || "jo", internallink := 0 ], topscreen->comp->value);

  // NOTE we don't (yet?) support conversion to internal link, unsafe if we're unsure whether the internal link supports any sort of suburl, we might just be throwing noise into the append...
  topscreen->comp->SetLinkType("internal");
  TestEQ([ append := "", externallink := "", internallink := GetTestsuiteTempFolder()->id ], topscreen->comp->value, "Append should be lost now...");

  // Verify canonicalization
  topscreen->comp->SetLinkType("external");
  topscreen->comp->^external->value := "https://www.google.com/url?q=https://us02web.zoom.us/j/12345678?pwd%3Dxxxx&sa=D&source=calendar&ust=1689417590895063&usg=AOvVaw2EoUXIyukNNpLwZ_6YHZdz";
  TestEQ([ append := "", externallink := "https://us02web.zoom.us/j/12345678", internallink := 0 ], topscreen->comp->value);

  TestEq(["No link","External link","Internal link"], SELECT AS STRING ARRAY title FROM topscreen->comp->^linktype->options);
  topscreen->comp->required := TRUE;
  TestEq(["External link","Internal link"], SELECT AS STRING ARRAY title FROM topscreen->comp->^linktype->options);
  topscreen->comp->required := FALSE;
  TestEq(["No link","External link","Internal link"], SELECT AS STRING ARRAY title FROM topscreen->comp->^linktype->options);

  // Folder with indexdoc
  testfw->BeginWork();
  OBJECT whfs_folderwithindex := GetTestsuiteTempFolder();
  OBJECT whfs_index := whfs_folderwithindex->CreateFile([ name := "index.html", publish := TRUE ]);
  whfs_folderwithindex->UpdateMetadata([ indexdoc := whfs_index->id ]);
  testfw->CommitWork();

  topscreen->comp->value := [ append := "#jo", externallink := "", internallink := whfs_folderwithindex->id ];
  TestEQ([ append := "#jo", externallink := "", internallink := whfs_folderwithindex->id ], topscreen->comp->value);
  topscreen->comp->SetLinkType("external");
  TestEQ([ append := "", externallink := whfs_folderwithindex->indexurl || "#jo", internallink := 0 ], topscreen->comp->value);

  // Preference to resolving to index files instead of folders
  topscreen->comp->SetLinkType("internal");
  TestEQ([ append := "#jo", externallink := "", internallink := whfs_folderwithindex->indexdoc ], topscreen->comp->value);
  topscreen->comp->SetLinkType("external");
  TestEQ([ append := "", externallink := whfs_folderwithindex->indexurl || "#jo", internallink := 0 ], topscreen->comp->value);

  // Folder with indexdoc
  testfw->BeginWork();
  OBJECT whfs_publishedfile := whfs_folderwithindex->CreateFile([ name := "other.html", publish := TRUE ]);
  testfw->CommitWork();

  topscreen->comp->value := [ append := "#jo", externallink := "", internallink := whfs_publishedfile->id ];
  TestEQ([ append := "#jo", externallink := "", internallink := whfs_publishedfile->id ], topscreen->comp->value);
  topscreen->comp->SetLinkType("external");
  TestEQ([ append := "", externallink := whfs_publishedfile->indexurl || "#jo", internallink := 0 ], topscreen->comp->value);
  topscreen->comp->SetLinkType("internal");
  TestEQ([ append := "#jo", externallink := "", internallink := whfs_publishedfile->id ], topscreen->comp->value);

  //Test limiting internallinkroots.
  topscreen->comp->internallinkroots := INTEGER[ whfs_folderwithindex->id ];
  AWAIT ExpectScreenChange(+1, PTR TTClick("comp->internal->browse"));
  TestEq(["tmp"], (SELECT AS STRING ARRAY name FROM TT("folders")->rows), "should only offer 'tmp' folder due to limited roots");
  AWAIT ExpectScreenChange(-1, PTR TTEscape);
  AWAIT ExpectScreenChange(-1, PTR TTEscape);
}

ASYNC MACRO TestIntExtLink_WithLinkHandlers()
{
  STRING intextlink_withlinkhandlers := `<?xml version="1.0" encoding="UTF-8"?>
  <screens xmlns="http://www.webhare.net/xmlns/tollium/screens" xmlns:p="http://www.webhare.net/xmlns/publisher/components" xmlns:test="http://www.webhare.net/xmlns/webhare_testsuite/testcomponents">
    <screen name="firstscreen" implementation="none"><body><p:intextlink name="link" required="true" internallinks="false"><linkhandlers><test:customlinkhandler/><test:customlinkhandler2/></linkhandlers></p:intextlink></body></screen>
  </screens>`;

  AWAIT ExpectScreenChange(+1, PTR GetTestController()->RunScreen(`inline::${intextlink_withlinkhandlers}`));

  TestEq(["External link","custom link handler","custom link handler2"], SELECT AS STRING ARRAY title FROM TT("link")->^linktype->options);
  TT("link")->value := MakeIntExtExternalLink("x-webharetestsuite-customlink:15");
  TestEq("custom link handler", TT("link")->^linktype->selection.title);
  TestEq(15, TT("link")->^linktype->selection.enablecomponents[0]->value);
  TestEq(MakeIntExtExternalLink("x-webharetestsuite-customlink:15"), TT("link")->value);

  TT("link")->value := MakeIntExtinternalLink(1,"#test");
  TestEq("custom link handler2", TT("link")->^linktype->selection.title);
  TestEQ(MakeIntExtinternalLink(1,"#test"), TT("link")->value);

  AWAIT ExpectScreenChange(-1, PTR TTEscape);

  // test through screen
  AWAIT ExpectScreenChange(+1, PTR GetTestController()->RunScreen(InlineScreens(`
    <screen name="firstscreen" implementation="none"><body><includefragment name="linkfragment" fragment="#firstfragment"/></body></screen>
    <fragment name="firstfragment" implementation="none"><contents><p:intextlink name="link" required="true"><linkhandlers><test:customlinkhandler/><test:customlinkhandler2/></linkhandlers></p:intextlink></contents></fragment>`)));
  TestEq(["External link","Internal link","custom link handler","custom link handler2"], SELECT AS STRING ARRAY title FROM TT("linkfragment->link")->^linktype->options);
  AWAIT ExpectScreenChange(-1, PTR TTEscape);
}

ASYNC MACRO TestIntExtLink_Tabsextension()
{
  // test through tabsextension
  STRING screenwithtabsresouce := InlineScreens(`
    <screen name="firstscreen" implementation="none"><body><tabs name="tabs" /></body></screen>
    <tabsextension name="tabs"><newtab><p:intextlink name="link" required="true"><linkhandlers><test:customlinkhandler/><test:customlinkhandler2/></linkhandlers></p:intextlink></newtab></tabsextension>
    <fragment name="firstfragment" implementation="none"><contents><p:intextlink name="link" required="true"><linkhandlers><test:customlinkhandler/><test:customlinkhandler2/></linkhandlers></p:intextlink></contents></fragment>`);
  AWAIT ExpectScreenChange(+1, PTR GetTestController()->RunScreen(screenwithtabsresouce));
  OBJECT ext := TT("tabs")->LoadTabsextension(screenwithtabsresouce || "#tabs").extension;
  TestEq(["External link","Internal link","custom link handler","custom link handler2"], SELECT AS STRING ARRAY title FROM ext->^link->^linktype->options);
  AWAIT ExpectScreenChange(-1, PTR TTEscape);

  // test through tabsextension with component references
  STRING screenwithtabsresouce2 := InlineScreens(`
    <screen name="firstscreen" implementation="none"><compositions><record name="rowcomp"/></compositions><body><tabs name="tabs" /></body></screen>
    <tabsextension name="tabs"><newtab><textedit composition="row" cellname="rich"/><p:intextlink composition="row" cellname="link" name="link" required="true"><linkhandlers><test:customlinkhandler/><test:customlinkhandler3/></linkhandlers></p:intextlink></newtab></tabsextension>
    <fragment name="firstfragment" implementation="none"><contents><p:intextlink name="link" required="true"><linkhandlers><test:customlinkhandler/><test:customlinkhandler3/></linkhandlers></p:intextlink></contents></fragment>`);

  AWAIT ExpectScreenChange(+1, PTR GetTestController()->RunScreen(screenwithtabsresouce2));
  TT("tabs")->extendcomponents := [[ name := "row", component := TT("rowcomp") ]];
  ext := TT("tabs")->LoadTabsextension(screenwithtabsresouce2 || "#tabs").extension;
  TestEq(["External link","Internal link","custom link handler","custom link handler3"], SELECT AS STRING ARRAY title FROM ext->^link->^linktype->options);
  AWAIT ExpectScreenChange(-1, PTR TTEscape);
}

RunTestframework([ PTR TestIntExtLink
                 , PTR TestIntExtLink_WithLinkHandlers
                 , PTR TestIntExtLink_Tabsextension
                 ]);
