<?wh
LOADLIB "wh::datetime.whlib";
LOADLIB "wh::ipc.whlib";
LOADLIB "wh::javascript.whlib";
LOADLIB "wh::promise.whlib";
LOADLIB "wh::util/algorithms.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/internal/dbschema.whlib";
LOADLIB "mod::system/lib/testframework.whlib";
LOADLIB "mod::system/lib/tasks.whlib";

RECORD val;

MACRO StoreValue(RECORD v)
{
  val := v;
}

ASYNC MACRO TestEphemeralTasks()
{
  OBJECT ephtask;
  TestThrowsLike("No such ephemeral task type *webhare_testsuite:nosuchtask*", PTR ScheduleEphemeralTask("webhare_testsuite:nosuchtask", DEFAULT RECORD));

  testfw->BeginWork();
  ephtask := ScheduleEphemeralTask("webhare_testsuite:ping", [ ping := 43 ]);
  testfw->RollbackWork();
  TestThrowsLike("*was queued did not commit*", PTR WaitForPromise(ephtask)); //this task can never run because the transaction rolled back

  testfw->BeginWork();
  ephtask := ScheduleEphemeralTask("webhare_testsuite:ping", [ ping := 43 ])->Then(PTR StoreValue);
  testfw->CommitWork();

  TestEq(TRUE, ObjectExists(ephtask));
  WaitForPromise(ephtask);
  TestEq([ephpong := 43, managedtaskid := 0], val);

  ephtask := ScheduleEphemeralTask("webhare_testsuite:ping", [ ping := 1 ], [ auxdata := [ ping := RepeatText("Y",10000) ] ] )->Then(PTR StoreValue);

  WaitForPromise(ephtask);
  TestEq([ephpong := RepeatText("Y",10000), managedtaskid := 0 ],val);

  ephtask := ScheduleEphemeralTask("webhare_testsuite:ping", [ ping := "ABORT" ]);
  TestThrowsLike('*PING-TASK-Abort*', PTR WaitForPromise(ephtask));
  ephtask := ScheduleEphemeralTask("webhare_testsuite:ping", [ ping := "THROW" ]);
  TestThrowsLike('*PING-TASK-Throw*', PTR WaitForPromise(ephtask));
}

ASYNC MACRO TestWaitableAction(BOOLEAN javascript)
{
  INTEGER taskid;
  STRING taskname := javascript ? "webhare_testsuite:ping_js" : "webhare_testsuite:ping";
  STRING taskretryname := javascript ? "webhare_testsuite:pingretry2_js" : "webhare_testsuite:pingretry2";

  TestThrowsLike("No such managed task type *webhare_testsuite:nosuchtask*", PTR ScheduleManagedTask("webhare_testsuite:nosuchtask", DEFAULT RECORD));

  TestEq(0, Length(LookupManagedTasks(taskname, [ createdafter := testfw->starttime ])));

  testfw->BeginWork();
  taskid := ScheduleManagedTask(taskname, CELL[ ping := 42, extraping := 44, javascript ]);

  TestEq(1, Length(LookupManagedTasks(taskname, [ createdafter := testfw->starttime ])));
  TestEq(1, Length(LookupManagedTasks(taskname, [ createdafter := testfw->starttime, unfinished := TRUE ])));
  testfw->CommitWork();

  RECORD retval := RetrieveManagedTaskResult(taskid, MAX_DATETIME);
  TestEqMembers([pong := 42, managedtaskid := taskid ], retval, '*');
  RECORD taskinfo := DescribeManagedTask(taskid);
  TestEq(TRUE, taskinfo.finished <= GetCurrentDatetime(), 'task should be marked as finished');

  RECORD retval2 := RetrieveManagedTaskResult(retval.extrataskid, MAX_DATETIME);
  TestEq([pong := 44, managedtaskid := retval.extrataskid ], retval2);

  testfw->BeginWork();
  taskid := ScheduleManagedTask(taskretryname, [ ping := "CANCEL" ]);
  testfw->CommitWork();

  TestThrowsLike("*ping=CANCEL*", PTR RetrieveManagedTaskResult(taskid, MAX_DATETIME)); //cancels because we told it to
  Sleep(200); //sleep a bit, give managedqueuemgr a chance to overwrite the reuslt
  RECORD fullres := RetrieveManagedTaskResult(taskid, MAX_DATETIME, [ acceptcancel := TRUE ]);
  TestEq("CANCEL", fullres.data.ping);

  testfw->BeginWork();
  taskid := ScheduleManagedTask(taskretryname, [ ping := "ABORT" ]);
  testfw->CommitWork();

  TestThrowsLike(javascript ? "*Task attempted to abort*" : "*PING-TASK-Abort*", PTR RetrieveManagedTaskResult(taskid, MAX_DATETIME)); //crashes because we told it to
  TestEq(DEFAULT RECORD, RetrieveManagedTaskResult(taskid, DEFAULT DATETIME, [ accepttempfailure := TRUE, accepttimeout := TRUE ])); //if we probe for temporay failures we won't get a throw
  taskinfo := DescribeManagedTask(taskid);
  TestEq(1, taskinfo.failures);
  TestEq(DEFAULT DATETIME, taskinfo.finished, 'task should still be marked as unfinished');
  TestEq(TRUE, taskinfo.nextattempt > GetCurrentDatetime() AND taskinfo.nextattempt < AddDaySToDate(1,GetCurrentDatetime()),"Should still be queued");

  testfw->BeginWork();
  RetryPendingManagedTasksByIds([taskid]);
  testfw->CommitWork();

  iF(javascript)
  {
    //wait for task to report 'failed once'
    WHILE(TRUE)
    {
      TRY
      {
        RetrieveManagedTaskResult(taskid, MAX_DATETIME);
      }
      CATCH(OBJECT e)
      {
        IF(e->what LIKE "*Failed once*")
          BREAK;
        Sleep(50);
      }
    }

    //restart immediately
    testfw->BeginWork();
    RetryPendingManagedTasksByIds([taskid]);
    testfw->CommitWork();
  }

  // wait for the final failure
  TestThrowsLike(javascript ? "*Task attempted to abort*" : "*PING-TASK-Abort*", PTR RetrieveManagedTaskResult(taskid, MAX_DATETIME, [accepttempfailure := TRUE]));

  taskinfo := DescribeManagedTask(taskid);
  TestEq(javascript ? 3 : 2, taskinfo.failures);
  TestEq(TRUE, taskinfo.finished < GetCurrentDatetime(), "task should now be ignored for further requeueing");

  TestEq(0, Length(LookupManagedTasks(taskname, [ createdafter := testfw->starttime, unfinished := TRUE ])));
  TestEq(2, Length(LookupManagedTasks(taskname, [ createdafter := testfw->starttime, unfinished := FALSE ])));
  testfw->BeginWork();
  taskid := ScheduleManagedTask(taskname, [ ping := 1 ], [ auxdata := [ ping := RepeatText("X",10000) ] ] );
  testfw->CommitWork();
  TestEq([pong := RepeatText("X",10000), managedtaskid := taskid ],RetrieveManagedTaskResult(taskid, MAX_DATETIME));

  testfw->BeginWork();
  WriteRegistryKey("webhare_testsuite.tests.taskthrownow", TRUE);
  taskid := ScheduleManagedTask(taskname, [ ping := "THROWNOW" ]);
  testfw->CommitWork();

  RECORD info;
  WHILE(TRUE)
  {
    info := DescribeManagedTask(taskid);
    IF(info.lasterrors != "")
      BREAK;

    AWAIT CreateSleepPromise(100);
  }
  TestEqLike("*Throw-Now*", info.lasterrors);
  TestEq(TRUE, GetDatetimeDifference(GetCurrentDatetime(), info.nextattempt).msecs < 5000, "Next attempt should be within 5 seconds");
  INTEGER ARRAY todelete := [ taskid ];

  testfw->BeginWork();
  WriteRegistryKey("webhare_testsuite.tests.taskthrownow", FALSE);
  testfw->CommitWork();

  STRING cancellable_task := "webhare_testsuite:cancellable" || (javascript ? "_js" : "");
  OBJECT port := CreateGlobalIPCPort("webhare_testsuite:cancellable_connectport" || (javascript ? "_js" : ""));


  // HS CancelManagedTasks
  {
    testfw->BeginWork();
    taskid := ScheduleManagedTask(cancellable_task, DEFAULT RECORD);
    testfw->CommitWork();

    // cancellable will connect to our global port
    OBJECT link := port->Accept(AddTimeToDate(60 * 1000, GetCurrentDatetime()));
    IF (NOT ObjectExists(link))
      ABORT(`Expected managed task 'cancellable' to run within a minute`);

    testfw->BeginWork();
    OBJECT cancelpromise := CancelManagedTasks([ taskid ]);
    testfw->CommitWork();

    AWAIT cancelpromise;

    RECORD msg := AWAIT link->AsyncReceiveMessage(AddTimeToDate(30 * 1000, GetCurrentDatetime()));
    TestEQ("gone", msg.status, "Task should have been killed by the cancel");
  }

  // TS CancelManagedTasks
  {
    testfw->BeginWork();
    taskid := ScheduleManagedTask(cancellable_task, DEFAULT RECORD);
    testfw->CommitWork();

    // cancellable will connect to our global port
    OBJECT link := port->Accept(AddTimeToDate(60 * 1000, GetCurrentDatetime()));
    IF (NOT ObjectExists(link))
      ABORT(`Expected managed task 'cancellable' to run within a minute`);

    RECORD rec := AWAIT CallJS("./helpers.ts#testCancelManagedTasks", [ taskid ]);

    RECORD msg := AWAIT link->AsyncReceiveMessage(AddTimeToDate(30 * 1000, GetCurrentDatetime()));
    TestEQ("gone", msg.status, "Task should have been killed by the cancel");
  }

  // test timeout handling
  STRING timelimitedtask := "webhare_testsuite:timelimitedtask" || (javascript ? "_js" : "");
  testfw->BeginWork();
  taskid := ScheduleManagedTask(cancellable_task, DEFAULT RECORD, [ timeout := 100 ]);
  INTEGER taskid3 := ScheduleManagedTask(timelimitedtask, [ sleep := 200 ], [ timeout := 100 ]);
  INTEGER taskid4 := ScheduleManagedTask(timelimitedtask, [ sleep := 400 ]);
  testfw->CommitWork();

  TestThrowsLike("Task * #* has failed: Task has timed out after 100ms", PTR RetrieveManagedTaskResult(taskid, MAX_DATETIME));
  TestThrowsLike("Task * #* has failed: Task has timed out after 100ms", PTR RetrieveManagedTaskResult(taskid3, MAX_DATETIME));
  TestThrowsLike("Task * #* has failed: Task has timed out after 300ms", PTR RetrieveManagedTaskResult(taskid4, MAX_DATETIME));

  WHILE(TRUE)
  {
    //it may take an extra attempt to get the sleep-200ms task to win from its timeout, as a sleep(200) isn't guaratneed to finish immediately after 200ms
    testfw->BeginWork();
    INTEGER taskid2 := ScheduleManagedTask(timelimitedtask, [ sleep := 200 ]);
    testfw->CommitWork();

    TRY
    {
      RECORD res := RetrieveManagedTaskResult(taskid2, MAX_DATETIME);
      TestEq([sleep := 200], res);
      BREAK;
    }
    CATCH(OBJECT e)
    {
      IF(e->what like "*timed out*")
      {
        Print("Timedout exception, it happens... " || e->What || "\n");
        Sleep(100);
        CONTINUE; //just retry
      }
      THROW;
    }
  }

  testfw->BeginWork();
  OBJECT deletepromise := DeleteManagedTasks(INTEGER[ taskid, taskid3, taskid4, ...todelete ]);
  testfw->CommitWork();

  AWAIT deletepromise;
}

RECORD FUNCTION WaitForError(INTEGER taskid)
{
  RECORD descr;

  // Wait for first error to occur
  FOR (INTEGER i := 0; i < (60 * 1000) / 100; i := i + 1)
  {
    descr := DescribeManagedTask(taskid);
    IF (descr.lasterrors != "")
      BREAK;
    Sleep(100);
  }

  IF (descr.lasterrors = "")
    THROW NEW Exception(`Timeout waiting for task #${taskid} to run`);

  RETURN descr;
}

MACRO TestMarkAsTemporaryFailure(STRING engine)
{
  DATETIME taskstart, retryat;
  INTEGER taskid;
  RECORD descr;

  IF (engine != "js" AND engine != "hs")
    THROW NEW Exception(`Invalid engine ${engine}`);
  STRING temporaryfailure_taskname := engine = "js" ? "webhare_testsuite:temporaryfailure_js" : "webhare_testsuite:temporaryfailure";

  FOR (INTEGER itr := 0; itr <= 7; itr := itr + 1)
  {
    testfw->BeginWork();
    taskstart := GetCurrentDateTime();
    taskid := ScheduleManagedTask(temporaryfailure_taskname, DEFAULT RECORD);

    IF (itr != 0)
    {
      UPDATE system_internal.managedtasks
         SET failures := itr
       WHERE id = taskid;
    }

    testfw->CommitWork();

    descr := WaitForError(taskid);

    // Test the exponential backoff
    INTEGER ARRAY exp_interval_mins := [ 15, 30, 60, 2 * 60, 4 * 60, 8 * 60, 24 * 60, 24 * 60 ];
    DATETIME expected := exp_interval_mins[itr] >= 24 * 60
        ? AddDaysToDate(exp_interval_mins[itr] / (24 * 60), taskstart)
        : AddTimeToDate(exp_interval_mins[itr] * 60000, taskstart);

    TestAssert(descr.nextattempt >= expected AND descr.nextattempt <  AddTimeToDate(60000,expected), `Iteration ${itr} expected ${EncodeJSON(expected)} got ${EncodeJSON(descr.nextattempt)}`);

    testfw->BeginWork();
    CancelManagedTasks([ taskid ]);
    testfw->CommitWork();
  }

  {
    // Test honouring specifying retryat option
    testfw->BeginWork();
    retryat := AddTimeToDate(2 * 60 * 1000, GetCurrentDateTime());
    taskstart := GetCurrentDateTime();
    taskid := ScheduleManagedTask(temporaryfailure_taskname, CELL[ nextretry := retryat ]);
    testfw->CommitWork();

    descr := WaitForError(taskid);

    // Check if retryat is honoured
    TestEQ(retryat, descr.nextattempt);

    testfw->BeginWork();
    CancelManagedTasks([ taskid ]);
    testfw->CommitWork();
  }
}

MACRO TestFailingTask(STRING engine)
{
  IF (engine != "js" AND engine != "hs")
    THROW NEW Exception(`Invalid engine ${engine}`);
  STRING temporaryfailure_taskname := engine = "js" ? "webhare_testsuite:temporaryfailure_js" : "webhare_testsuite:temporaryfailure";

  testfw->BeginWork();
  INTEGER taskid := ScheduleManagedTask(temporaryfailure_taskname, [ "temporary" := FALSE ]);
  testfw->CommitWork();
  TestThrowsLike("*permanently failed: Permanent failure", PTR RetrieveManagedTaskResult(taskid, AddTimeToDate(120*1000, GetCurrentDateTime())));
  // test if retval is recorded
  TestEQMembers(
      [ retval :=
          [ type := "failed"]
      ], DescribeManagedTask(taskid), "*");
}

ASYNC MACRO TestNotBeforeTasks(STRING engine)
{
  DATETIME now := GetCurrentDateTime();

  IF (engine != "js" AND engine != "hs")
    THROW NEW Exception(`Invalid engine ${engine}`);
  STRING temporaryfailure_taskname := engine = "js" ? "webhare_testsuite:temporaryfailure_js" : "webhare_testsuite:temporaryfailure";

  testfw->BeginWork();
  INTEGER taskid := ScheduleManagedTask("webhare_testsuite:ping", [ ping := 42 ], [ notbefore := AddTimeToDate(2000, now) ]);
  INTEGER taskid2 := ScheduleManagedTask("webhare_testsuite:ping", [ ping := 42 ], [ notbefore := AddTimeToDate(2000, now) ]);
  INTEGER taskid3 := ScheduleManagedTask(temporaryfailure_taskname, DEFAULT RECORD, [ notbefore := AddTimeToDate(60000, now) ]);
  testfw->CommitWork();

  TestEQMembers([ notbefore := AddTimeToDate(2000, now), nextattempt := AddTimeToDate(2000, now) ], DescribeManagedTask(taskid), "*");

  testfw->BeginWork();
  RetryPendingManagedTasksByIds([ taskid2 ]);
  RetryPendingManagedTasks(temporaryfailure_taskname);
  testfw->CommitWork();

  TestEQMembers([ notbefore := AddTimeToDate(2000, now), nextattempt := AddTimeToDate(2000, now) ], DescribeManagedTask(taskid2), "*");
  TestEQMembers([ notbefore := AddTimeToDate(60000, now), nextattempt := AddTimeToDate(60000, now) ], DescribeManagedTask(taskid3), "*");

  // should not resolve within the first 1.5 second
  TestEQ(DEFAULT RECORD, RetrieveManagedTaskResult(taskid, AddTimeToDate(1500, now), [ accepttimeout := TRUE ]));

  // but it should resolve when waiting for it (10 secs should be enough for CI, all other tasks are deleted)
  TestEQ([ managedtaskid := taskid, pong := 42 ], RetrieveManagedTaskResult(taskid, AddTimeToDate(10000, now)));

  testfw->BeginWork();
  OBJECT cancelpromise := CancelManagedTasks([ taskid, taskid2, taskid3 ]);
  testfw->CommitWork();

  AWAIT cancelpromise;
}

STRING FUNCTION AcceptLink(OBJECT port)
{
  OBJECT link := port->Accept(MAX_DATETIME);
  STRING retval := link->ReceiveMessage(MAX_DATETIME).msg.msg;
  link->Close();
  RETURN retval;
}

MACRO TestDoubleSchedule(STRING engine)
{
  IF (engine != "js" AND engine != "hs")
    THROW NEW Exception(`Invalid engine ${engine}`);
  STRING doublescheduletask_taskname := engine = "js" ? "webhare_testsuite:doublescheduletask_js" : "webhare_testsuite:doublescheduletask";
  STRING doublescheduletask_portname := engine = "js" ? "webhare_testsuite:doubleschedule_connectport_js" : "webhare_testsuite:doubleschedule_connectport";

  /* Tests double scheduling of tasks that have resolved by restart.
     The tasks increasingly fill the event pipe so the event signalling the
     task table update interferes with the handling of the task finish report by
     the managed worker.
  */
  INTEGER iters := 200;
  testfw->BeginWork();

  OBJECT port := CreateGlobalIPCPort(doublescheduletask_portname);
  ScheduleManagedTask(doublescheduletask_taskname, CELL[ stage := 1, iters, t := 1 ]);
  ScheduleManagedTask(doublescheduletask_taskname, CELL[ stage := 1, iters, t := 2 ]);
  ScheduleManagedTask(doublescheduletask_taskname, CELL[ stage := 1, iters, t := 3 ]);
  ScheduleManagedTask(doublescheduletask_taskname, CELL[ stage := 1, iters, t := 4 ]);
  testfw->CommitWork();

  STRING ARRAY messages;
  FOR (INTEGER i := 0; i < 4 * iters; i := i + 1)
  {
    STRING str := AcceptLink(port);
    RECORD pos := LowerBound(messages, str);
    TestEQ(FALSE, pos.found, `The task iterations with message ${EncodeJSON(str)} has been duplicated`);
    INSERT str INTO messages AT pos.position;
  }
  port->Close();
}

ASYNC MACRO TestWaitableAction_HS()
{
  AWAIT TestWaitableAction(FALSE);
}

ASYNC MACRO TestWaitableAction_JS()
{
  AWAIT TestWaitableAction(TRUE);
}

 RunTestframework([ PTR TestEphemeralTasks
                 , PTR TestFailingTask("hs")
                 , PTR TestFailingTask("js")
                 , PTR TestWaitableAction_HS
                 , PTR TestWaitableAction_JS
                 , PTR TestMarkAsTemporaryFailure("hs")
                 , PTR TestMarkAsTemporaryFailure("js")
                 , PTR TestNotBeforeTasks("hs")
                 , PTR TestNotBeforeTasks("js")
                 , PTR TestDoubleSchedule("hs")
                 , PTR TestDoubleSchedule("js")
                 ],
                 [ wrdschema := FALSE
                 ]);
