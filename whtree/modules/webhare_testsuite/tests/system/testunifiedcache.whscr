<?wh

LOADLIB "wh::crypto.whlib";
LOADLIB "wh::datetime.whlib";
LOADLIB "wh::files.whlib";
LOADLIB "wh::internet/urls.whlib";
LOADLIB "wh::graphics/canvas.whlib";


LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/resources.whlib";
LOADLIB "mod::system/lib/cache.whlib";
LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/testframework.whlib";
LOADLIB "mod::system/lib/internal/cache/imgcache.whlib";

LOADLIB "mod::wrd/lib/api.whlib";

LOADLIB "mod::webhare_testsuite/lib/system/tests.whlib";
LOADLIB "mod::webhare_testsuite/lib/internal/wrdtesthelpers.whlib";


STRING link_grayscalehomer, link_kikker, link_fs_frogger, link_unused_kikker, link_wrd_homer;
DATETIME after_kikker_hit;
STRING default_imgcache := "public, max-age=31536000, immutable";
STRING incorrect_imgcache := "public, max-age=86400"; //used when requesting incorrect urls
OBJECT homer, wrd_person;
INTEGER entityid1, entityid2;

MACRO ClearUnifiedCache()
{
  STRING cachedir := GetUnifiedCacheDiskRoot();
  IF (NOT RecordExists(GetDiskFileProperties(cachedir)))
    RETURN;

  FOREVERY (RECORD dir FROM ReadDiskDirectory(cachedir, "???"))
  {
    IF (dir.type != 1)
      CONTINUE;
    DeleteDiskDirectoryRecursive(dir.path);
  }

  TestEQ(STRING[], SELECT AS STRING ARRAY name FROM ReadDiskDirectory(cachedir, "*") WHERE name != "CACHEDIR.TAG");
}

MACRO ImgCacheTest()
{
  // Verify AVIF and WEBP support in ScanBlob
  TesteqMembers([width := 428, height:= 284, extension := ".jpg", mimetype := "image/jpeg"], ScanBlob(GetWebhareResource("mod::system/web/tests/snowbeagle.jpg")),'*');
  TesteqMembers([width := 428, height:= 284, extension := ".webp", mimetype := "image/webp"], ScanBlob(GetWebhareResource("mod::system/web/tests/snowbeagle.webp")),'*');
  TesteqMembers([width := 428, height:= 284, extension := ".avif", mimetype := "image/avif"], ScanBlob(GetWebhareResource("mod::system/web/tests/snowbeagle.avif")),'*');

  // Verify calculations
  TestEqMembers(
     [ item := [cc := 456 ]],
     AnalyzeUnifiedURLToken(`i${GetUCSubUrl([ method := "scale", setwidth:=25, setheight:=25], [ hash := "u4HI1_mWV8E0UWndfoBvwsQr4PxwK7pdZLzYjWSw_0Q", refpoint := default record ], 1, 1, 123, 456, '.png')}/image.png`), '*');

  // Insert a simple image into the publisher

  testfw->BeginWork();

  OBJECT root := GetTestsuiteTempFolder();
  STRING baseurl := ResolveToAbsoluteURL(root->objecturl,"/tollium_todd.res/webhare_testsuite/tests/adhoccache.shtml");
  BLOB toupload := GetWebhareResource("mod::webhare_testsuite/tests/system/testdata/cmyk_kikkertje.jpg");

  RECORD srckikkerinfo := ScanBlob(toupload,""); //we need to be sure filetype detection is functioning...
  TestEQ(TRUE, srckikkerinfo.width != 0, "original cmyk_kikkertje.jpg not recognized as image");

  OBJECT kikkertje := root->CreateFile( [ data := toupload, publish := FALSE, name := "kwaak.jpg" ]);
  OBJECT unused_kikkertje := root->CreateFile( [ data := toupload, publish := FALSE, name := "kwaak_unused.jpg" ]);

  BLOB toupload2 := GetWebhareResource("mod::webhare_testsuite/tests/system/testdata/homersbrain.bmp");
  homer := root->CreateFile( [ data := toupload2, publish := FALSE, name := "homersbrain.bmp" ]);

  STRING legacy_badkikkerlink := GetCachedFSImageURL(baseurl, kikkertje->id, [ method := "scale", setwidth:=25, setheight:=25]);
  STRING badkikkerlink := GetCachedImageLink(kikkertje->GetWrapped(), [ method := "scale", setwidth:=25, setheight:=25, baseurl := baseurl]);
  TestEqLike("*/kwaak.jpg", badkikkerlink);

  TestEQ(legacy_badkikkerlink, Substitute(badkikkerlink,"kwaak.jpg","data.jpg"));
  //Detect API misuse, accidentally using getcachedfilelink
  TestThrowsLike("A cached file cannot have a scale method.*", PTR GetCachedFileLink(kikkertje->GetWrapped(), [ method := "scale", setwidth:=25, setheight:=25, baseurl := baseurl]));

  // invalidate legacy_badkikkerlink and badkikkerlink by updating the creationdate
  UPDATE system.fs_objects SET creationdate:=AddTimeToDate(1,creationdate) WHERE id=kikkertje->id;

  STRING goodkikkerlink := GetCachedFSImageURL(baseurl, kikkertje->id, [ method := "scale", setwidth:=25, setheight:=25]);
  STRING bigkikkerlink := GetCachedFSImageURL(baseurl, kikkertje->id, [ method := "stretch", setwidth:=250, setheight:=250]);
  STRING poorkikkerlink := GetCachedFSImageURL(baseurl, kikkertje->id, [ method := "stretch", setwidth:=250, setheight:=250, quality := 25]);

  TestEq(FALSE, testfw->browser->GotoWebPage(goodkikkerlink), testfw->browser->url);
  TestEq("no-cache", testfw->browser->GetResponseHeader("Cache-Control")); //A valid 'but not yet' hash should return a no-cache header

  testfw->CommitWork();

  TestEq(FALSE, testfw->browser->GotoWebPage(badkikkerlink));
  TEstEq(404, testfw->browser->GetHTTPStatusCode(),  badkikkerlink);
  TestEq(FALSE, testfw->browser->GotoWebPage(badkikkerlink || "/frogger.jpg"));
  TEstEq(404, testfw->browser->GetHTTPStatusCode());

  TestEq(TRUE, testfw->browser->GotoWebPage(goodkikkerlink), testfw->browser->url);
  RECORD kikkerinfo := ScanBlob(testfw->browser->content,"");
  TestEq(21, kikkerinfo.width);
  TestEq(25, kikkerinfo.height);

  TestEq(TRUE, testfw->browser->GotoWebPage(goodkikkerlink || "/frogger.jpg"), testfw->browser->url);
  kikkerinfo := ScanBlob(testfw->browser->content,"");
  TestEq(21, kikkerinfo.width);
  TestEq(25, kikkerinfo.height);

  // make sure a hit for the unused_kikkertje file is logged
  link_unused_kikker := GetCachedFSImageURL(baseurl, unused_kikkertje->id, [ method := "scale", setwidth:=25, setheight:=25]);
  Testeq(TRUE, testfw->browser->GotoWebPage(link_unused_kikker), link_unused_kikker);
  Sleep(1000);
  after_kikker_hit := GetCurrentDatetime();

  //Should not accept odd extensions
  Testeq(FALSE, testfw->browser->GotoWebPage(Substitute(goodkikkerlink,'.jpg','.xxx')), testfw->browser->url);
  Testeq(FALSE, testfw->browser->GotoWebPage(Substitute(goodkikkerlink,'.jpg','.png')), testfw->browser->url);
  TestEq(incorrect_imgcache, testfw->browser->GetResponseHeader("Cache-Control"), testfw->browser->url || " (this url is never correct, so cacheable)");

  //'beautiful' links should be created using the 'filename' option now
  //setting a wrong extension simply causes us to postfix the correct one
  STRING renamedkikkerlink;
  renamedkikkerlink := GetCachedFSImageURL(baseurl, kikkertje->id, [ method := "scale", setwidth:=25, setheight:=25, filename := "kikker.png" ]);
  TestEqLike("*/kikker.jpg", renamedkikkerlink);

  RECORD wrappedinfo;
  wrappedinfo := GetWrappedSourceFromURL(renamedkikkerlink); //note, we get source info, not method-changed info
  TestEq('kwaak.jpg', wrappedinfo.filename);
  TestEq(122, wrappedinfo.width);
  TestEq("#090D09", wrappedinfo.dominantcolor);

  Testeq(TRUE, testfw->browser->GotoWebPage(renamedkikkerlink), testfw->browser->url);
  STRING lastmod := testfw->browser->GetResponseHeader("Last-Modified");
  TestEq(default_imgcache, testfw->browser->GetResponseHeader("Cache-Control"));
  TestEq(TRUE, lastmod != "", "Lastmodification not set by first request");

  Testeq(TRUE, testfw->browser->GotoWebPage(renamedkikkerlink), testfw->browser->url);
  TestEq(default_imgcache, testfw->browser->GetResponseHeader("Cache-Control"));
  TestEq(lastmod, testfw->browser->GetResponseHeader("Last-Modified"), "Last-modification time mismatch between first (cache generating) and second (cache filling) request");

  renamedkikkerlink := GetCachedFSImageURL(baseurl, kikkertje->id, [ method := "scale", setwidth:=25, setheight:=25, filename := "kikker" ]);
  link_kikker := renamedkikkerlink;
  TestEqLike("*/kikker.jpg", renamedkikkerlink);
  renamedkikkerlink := GetCachedFSImageURL(baseurl, kikkertje->id, [ method := "scale", setwidth:=25, setheight:=25, filename := "kikker.jpg" ]);
  TestEQLike("*/kikker.jpg", renamedkikkerlink);

  Testeq(TRUE, testfw->browser->GotoWebPage(renamedkikkerlink), testfw->browser->url);

  //Dashes shouldn't confuse us
  renamedkikkerlink := GetCachedFSImageURL(baseurl, kikkertje->id, [ method := "scale", setwidth:=25, setheight:=25, filename := "k-i-k-k-e-r.jpg" ]);
  TestEQLike("*/k-i-k-k-e-r.jpg", renamedkikkerlink);
  Testeq(TRUE, testfw->browser->GotoWebPage(renamedkikkerlink), testfw->browser->url);

  //Spaces and other odd stuff should be urlencoded
  renamedkikkerlink := GetCachedFSImageURL(baseurl, kikkertje->id, [ method := "scale", setwidth:=25, setheight:=25, filename := "k i%k&k?e;r.jpg" ]);
  TestEQLike("*/k%20i%25k%26k%3Fe%3Br.jpg", renamedkikkerlink);
  Testeq(TRUE, testfw->browser->GotoWebPage(renamedkikkerlink), testfw->browser->url);

  RECORD cacheddata := GetCachedDataFromURL(renamedkikkerlink);
  TestEq("k i%k&k?e;r.jpg", cacheddata.filename);

  kikkerinfo := ScanBlob(testfw->browser->content,"");
  TestEq(21, kikkerinfo.width);
  TestEq(25, kikkerinfo.height);

  TestEq(TRUE, testfw->browser->GotoWebPage(bigkikkerlink || "/frogger.jpg"));
  BLOB bigkikker := testfw->browser->content;
  TestEq(TRUE, testfw->browser->GotoWebPage(poorkikkerlink || "/frogger.jpg"));
  TestEq(TRUE, Length(bigkikker) > Length(testfw->browser->content)); //high quality frogger must be a bigger file

  STRING homerlink := GetCachedFSImageURL(baseurl, homer->id, [ method := "scale", setwidth:=25, setheight:=25 ]);
  TestEq(TRUE, testfw->browser->GotoWebPage(homerlink), testfw->browser->url); //succeeds, x-bmp is now not supported and converted to image/png
  TestEq("image/png", testfw->browser->contenttype);

  homerlink := GetCachedFSImageURL(baseurl, homer->id, [ method := "scale", setwidth:=25, setheight:=25, format := "image/png"]);
  TestEq(TRUE , testfw->browser->GotoWebPage(homerlink));
  TestEq("image/png", testfw->browser->contenttype);

  STRING homerlink_webp := GetCachedFSImageURL(baseurl, homer->id, [ method := "scale", setwidth:=25, setheight:=25, format := "image/webp"]);
  TestAssert(homerlink_webp != "");
  TestEq(TRUE , testfw->browser->GotoWebPage(homerlink_webp));
  TestEq("image/webp", testfw->browser->contenttype);

  STRING homerlink_avif := GetCachedFSImageURL(baseurl, homer->id, [ method := "scale", setwidth:=25, setheight:=25, format := "image/avif"]);
  TestAssert(homerlink_avif != "");
  TestEq(TRUE , testfw->browser->GotoWebPage(homerlink_avif));
  TestEq("image/avif", testfw->browser->contenttype);

  STRING homercroplink := GetCachedFSImageURL(baseurl, homer->id, [ method := "crop", setwidth:=25, setheight:=25, format := "image/png"]);

  //Modify homer base file
  testfw->BeginWork();
  homer->UpdateMetadata(DEFAULT RECORD);
  testfw->CommitWork();
  TestEq(homerlink, GetCachedFSImageURL(baseurl, homer->id, [ method := "scale", setwidth:=25, setheight:=25, format := "image/png"]), "moddate update should not affect scaled link");
  TestEq(homercroplink, GetCachedFSImageURL(baseurl, homer->id, [ method := "crop", setwidth:=25, setheight:=25, format := "image/png"]), "moddate update should not affect crop link");

  //Modify refpoint
  RECORD homerdata := homer->GetWrapped();
  TestEQ(DEFAULT RECORD, homerdata.refpoint);

  RECORD wrapped := WrapCachedImage(homer->GetWrapped(), [ method := "fill", setwidth := 80,  setheight := 80 ]);
  TestEq("#000001", wrapped.dominantcolor);
  TestEq("", wrapped.refpoint_backgroundposition);
  TestEq(`#000001 url("${wrapped.link}") center/cover`, wrapped.css);

  homerdata.refpoint := [ x := 1, y := 1];
  testfw->BeginWork();
  homer->UpdateMetadata([ wrapped := homerdata ]);
  testfw->CommitWork();

  TestEq(homerlink, GetCachedFSImageURL(baseurl, homer->id, [ method := "scale", setwidth:=25, setheight:=25, format := "image/png"]), "moddate update should not affect scaled link");
  STRING newurl := GetCachedFSImageURL(baseurl, homer->id, [ method := "crop", setwidth:=25, setheight:=25, format := "image/png"]);
  TestEq(TRUE, homercroplink != newurl, "moddate update should affect crop link");

  wrapped := WrapCachedImage(homer->GetWrapped(), [ method := "fill", setwidth := 80,  setheight := 80 ]);
  TestEq("0.0000% 1.2500%", wrapped.refpoint_backgroundposition);
  TestEq(`#000001 url("${wrapped.link}") 0.0000% 1.2500%/cover`, wrapped.css);

  wrapped := WrapCachedImage(homer->GetWrapped(), [ method := "none" ]);
  TestEq("0.1563% 0.2083%", wrapped.refpoint_backgroundposition);
  TestEq(`#000001 url("${wrapped.link}") 0.1563% 0.2083%/cover`, wrapped.css);

  //Grayscale homer
  STRING grayhomerlink := GetCachedFSImageURL(baseurl, homer->id, [ method := "none", grayscale := TRUE ]);
  TestEq(TRUE, testfw->browser->GotoWebPage(grayhomerlink)); //succeeds, x-bmp is now not supported and converted to image/png
  TestEq("image/png", testfw->browser->contenttype);
  link_grayscalehomer := grayhomerlink;

  //Make sure it was actually grayscaled
  OBJECT returned_grayhomer := CreateCanvasFromBlob(testfw->browser->content);
  OBJECT reference_grayhomer := CreateCanvasFromBlob(toupload2);
  reference_grayhomer->Grayscale();
  TestEq(0f, reference_grayhomer->CompareWithCanvas(returned_grayhomer));

  //Test accessing homer through GetCachedFileLink
  homerlink := GetCachedFileLink(homer->GetWrapped());
  TestEqLike("*/homersbrain.bmp", homerlink);
  TestEq(TRUE, testfw->browser->GotoWebPage(homerlink));

  //Test accessing homer with GetWebhareConfiguration - this crashed in 4.17
  homerlink := GetCachedFileLink(homer->GetWrapped(), [ baseurl := GetPrimaryWebhareInterfaceURL()
                                                      , filename := "homer.bmp"
                                                      ]);
  TestEqLike("*/homer.bmp", homerlink);
  TestEq(TRUE, testfw->browser->GotoWebPage(homerlink));

  // Get data directly (with use of current database transaction)
  testfw->BeginWork();
  STRING smallgrayhomerlink := GetCachedFSImageURL(baseurl, homer->id, [ method := "scale", setwidth:=25, setheight:=25, grayscale := TRUE ]);
  cacheddata := GetCachedDataFromURL(smallgrayhomerlink);
  TestEq("image/png", cacheddata.mimetype);
  //TestEq(FALSE, cacheddata.filename LIKE "*/*", `Filename ${cacheddata.filename} may not contain slashes`);
  TestEq(25, Max[]([ INTEGER(ScanBlob(cacheddata.data).height), ScanBlob(cacheddata.data).width ]), "GetCachedDataFromURL for FSImages did not resize");
  testfw->CommitWork();
}

MACRO ImgCacheSourcesTest()
{
  testfw->BeginWork();
  OBJECT testfile := GetTestsuiteTempFolder()->CreateFile( [ name := "testfile" ]);
  testfile->SetInstanceData("http://www.webhare.net/xmlns/beta/test", [ arraytest := [[ blobcell := WrapBlob(GetWebhareResource("mod::webhare_testsuite/tests/system/testdata/cmyk_kikkertje.jpg"),"kikker.jpg") ]]]);
  testfw->CommitWork();

  RECORD img := testfile->GetInstanceData("http://www.webhare.net/xmlns/beta/test").arraytest[0].blobcell;
  RECORD kikkerinstance_nofilenamegiven := WrapCachedImage(img, [ method := "scale", setwidth:=25, setheight:=25 ]);
  TestEqLike("*/kikker.jpg", kikkerinstance_nofilenamegiven.link);
  RECORD kikkerinstance := WrapCachedImage(img, [ method := "scale", setwidth:=25, setheight:=25, filename := "kikker.png" ]);
  TestEqLike("*/kikker.jpg", kikkerinstance.link);

  RECORD wrappedinfo;
  wrappedinfo := GetWrappedSourceFromURL(kikkerinstance.link); //note, we get source info, not method-changed info

  TestEq('kikker.jpg', wrappedinfo.filename);
  TestEq(122, wrappedinfo.width); //original width
}

MACRO NotTheImageTest()
{
  testfw->BeginWork();
  OBJECT testfile := GetTestsuiteTempFolder()->CreateFile( [ name := "nottheimage" ]);
  testfile->SetInstanceData("http://www.webhare.net/xmlns/beta/test", [ arraytest := [[ blobcell := WrapBlob(StringToBlob("Dit is een file"),"file.txt") ]]]);
  RECORD img := testfile->GetInstanceData("http://www.webhare.net/xmlns/beta/test").arraytest[0].blobcell;

  //shouldn't crash but return empty strings
  TestEq("", GetCachedImageLink(img, [ method := "none" ]));
  TestEq(DEFAULT RECORD, WrapCachedImage(img, [ method := "none" ]));

  testfw->RollbackWork();
}


MACRO FsCacheTest()
{
  STRING baseurl := ResolveToAbsoluteURL(GetTestsuiteTempFolder()->link, "/tollium_todd.res/webhare_testsuite/tests/adhoccache.shtml");

  OBJECT kikkertje := GetTestsuiteTempFolder()->OpenByName("kwaak.jpg");
  TestEq(TRUE, ObjectExists(kikkertje), 'previous test did not leave kwaak.jpg for us...');


  testfw->BeginWork();
  STRING badkikkerfilelink := GetCachedFSFileURL(baseurl, kikkertje->id);
  UPDATE system.fs_objects SET creationdate:=AddTimeToDate(1,creationdate) WHERE id=kikkertje->id;
  STRING goodkikkerfilelink := GetCachedFSFileURL(baseurl, kikkertje->id);
  STRING avifkikkerfilelink := GetCachedFSImageURL(baseurl, kikkertje->id, [ method := "none", format := "image/avif" ]);

  OBJECT docxje := GetTestsuiteTempFolder()->CreateFile( [ data := GetWebhareResource("mod::webhare_testsuite/tests/system/testdata/empty.docx"), publish := FALSE, name := "empty.docx" ]);
  OBJECT extensionless := GetTestsuiteTempFolder()->CreateFile([data := StringToBlob("\x00\x01\x02\x03"), name := "extensionless" ]);
  OBJECT oddity := GetTestsuiteTempFolder()->CreateFile( [ data := StringToBlob("Space?"), publish := TRUE, name := "Bowie Space!.oddity" ]);
  OBJECT oddity2 := GetTestsuiteTempFolder()->CreateFile( [ data := StringToBlob("Space?"), publish := TRUE, name := "Bowie Space!.oddity 2!" ]);
  testfw->CommitWork();

  link_fs_frogger := goodkikkerfilelink;

  TestEq(FALSE, testfw->browser->GotoWebPage(badkikkerfilelink));
  TEstEq(404, testfw->browser->GetHTTPStatusCode(),  badkikkerfilelink);
  TestEq(FALSE, testfw->browser->GotoWebPage(badkikkerfilelink || "/frogger.jpg"));
  TestEq(404, testfw->browser->GetHTTPStatusCode());

  TestEq(TRUE, testfw->browser->GotoWebPage(goodkikkerfilelink), goodkikkerfilelink);
  BLOB jpgkikker := testfw->browser->content;
  RECORD kikkerinfo := ScanBlob(testfw->browser->content,"");
  TestEq(122, kikkerinfo.width);
  TestEq(148, kikkerinfo.height);

  TestEq(TRUE, testfw->browser->GotoWebPage(avifkikkerfilelink));
  RECORD avifkikkerinfo := ScanBlob(testfw->browser->content,"");
  TestEq(122, avifkikkerinfo.width);
  TestEq(148, avifkikkerinfo.height);

  STRING docxjelink := GetCachedFileLink(docxje->GetWrapped(), [ filename := "empty.docx" ]);
  TestEqLike("*/empty.docx", docxjelink);
  TestEq(TRUE, testfw->browser->GotoWebPage(docxjelink));
  TestEq("application/vnd.openxmlformats-officedocument.wordprocessingml.document", testfw->browser->GetResponseHeader("content-type"));

  STRING odditylink := GetCachedFileLink(oddity->GetWrapped());
  TestEqLike("*/bowie-space.oddity.bin", odditylink);
  TestEq(TRUE, testfw->browser->GotoWebPage(odditylink));
  TestEq("application/octet-stream", testfw->browser->GetResponseHeader("content-type"));

  odditylink := GetCachedFileLink(oddity->GetWrapped(), [ allowanyextension := TRUE ]);
  TestEqLike("*/bowie-space.oddity", odditylink);
  TestEq(TRUE, testfw->browser->GotoWebPage(odditylink));
  TestEq("application/octet-stream", testfw->browser->GetResponseHeader("content-type"));

  STRING oddity2link := GetCachedFileLink(oddity2->GetWrapped());
  TestEqLike("*/bowie-space.oddity-2.bin", oddity2link);
  TestEq(TRUE, testfw->browser->GotoWebPage(oddity2link));
  TestEq("application/octet-stream", testfw->browser->GetResponseHeader("content-type"));

  oddity2link := GetCachedFileLink(oddity2->GetWrapped(), [ allowanyextension := TRUE ]);
  TestEqLike("*/bowie-space.oddity-2", oddity2link);
  TestEq(TRUE, testfw->browser->GotoWebPage(oddity2link));
  TestEq("application/octet-stream", testfw->browser->GetResponseHeader("content-type"));

  docxjelink := GetCachedFileLink(docxje->GetWrapped());
  TestEq(TRUE, testfw->browser->GotoWebPage(docxjelink), docxjelink);
  TestEqLike("*.docx" ,docxjelink);
  TestEq("application/vnd.openxmlformats-officedocument.wordprocessingml.document", testfw->browser->GetResponseHeader("content-type"));

  STRING extensionlesslink := GetCachedFileLink(extensionless->GetWrapped(), [allowanyextension:=TRUE]);
  TestEq(FALSE, extensionlesslink LIKE "/.wh/ea/uc/*.*", extensionlesslink);
  TestEq(TRUE, testfw->browser->GotoWebPage(extensionlesslink), extensionlesslink);
  TestEq(StringToBlob("\x00\x01\x02\x03"), testfw->browser->content, testfw->browser->href);

  RECORD wrappedinfo;
  wrappedinfo := GetWrappedSourceFromURL(docxjelink);
  TestEq("empty.docx" ,wrappedinfo.filename);
  Testeq("B70b9aY2PO-eOr35IRzq7QJ2TwI6jusE7tCS934hYK4", wrappedinfo.hash);

  wrappedinfo := GetWrappedSourceFromURL(extensionlesslink);
  TestEq("extensionless" ,wrappedinfo.filename);
  Testeq("BU7ewdAhH2JP7Qy8qdT5QAsOSRxDdCryxbCr6_DJkNg", wrappedinfo.hash);
}

MACRO FilenameTest()
{
  testfw->BeginWork();
  OBJECT testfile := GetTestsuiteTempFolder()->OpenByName("testfile");
  RECORD wrappedentry := WrapBlob(GetWebhareResource("mod::webhare_testsuite/tests/system/testdata/cmyk_kikkertje.jpg"),"kikker.jpg");

  //Long filenames
  wrappedentry.filename := RepeatText("kikker",42) || ".jpg";
  testfile->SetInstanceData("http://www.webhare.net/xmlns/beta/test", [ arraytest := [[ blobcell := wrappedentry ]]]);
  testfw->CommitWork();

  RECORD img := testfile->GetInstanceData("http://www.webhare.net/xmlns/beta/test").arraytest[0].blobcell;
  TestEq(wrappedentry.filename, img.filename, "Filename should not be truncated during storage");

  RECORD kikkerinstance := WrapCachedImage(img, [ method := "scale", setwidth:=25, setheight:=25 ]);
  TestEqLike("*/" || Left(RepeatText("kikker",42),80) || ".jpg", kikkerinstance.link);

  //Odd filenames
  testfw->BeginWork();
  wrappedentry.filename := "I am (a) happy kikker!.jpg";
  testfile->SetInstanceData("http://www.webhare.net/xmlns/beta/test", [ arraytest := [[ blobcell := wrappedentry ]]]);
  testfw->CommitWork();

  img := testfile->GetInstanceData("http://www.webhare.net/xmlns/beta/test").arraytest[0].blobcell;
  TestEq(wrappedentry.filename, img.filename, "Filename should not be modified during storage");

  kikkerinstance := WrapCachedImage(img, [ method := "scale", setwidth:=25, setheight:=25 ]);
  TestEqLike("*/i-am-a-happy-kikker.jpg", kikkerinstance.link);
}

MACRO WRDImageCache()
{
  testfw->BeginWork();

  OBJECT wrdperson := testfw->GetWRDSchema()->^wrd_person;
  STRING baseurl := OpenTestsuitesite()->webroot;

  BLOB testimage_blob := GetWebHareResource("mod::system/web/tests/goudvis.png");
   entityid1 := wrdperson->CreateEntity([ test_image := WrapBlob(testimage_blob,"")
                                        ] )->id;
   entityid2 := wrdperson->CreateEntity([ test_array := [ [ test_image := WrapBlob(testimage_blob,"") ], DEFAULT RECORD ]
                                        ] )->id;

  RECORD entity := wrdperson->RunQuery( [ outputcolumns := [ imageid := "TEST_IMAGE.IMAGEID", arrayimages := "TEST_ARRAY.TEST_IMAGE.IMAGEID" ]
                                        , filters := [[ field := "WRD_ID", value := entityid1 ]
                                                     ]
                                        ]);
  TestEq(TRUE, entity.imageid != 0);
  TestEq("INTEGER64", GetTypeName(TypeID(entity.imageid)));
  TestEQ(INTEGER64[entity.imageid], wrdperson->RunQuery( [ outputcolumn := "TEST_IMAGE.IMAGEID", filters := [[ field := "WRD_ID", value := entityid1 ]] ]));

  INTEGER64 entityid1_imageid := entity.imageid;
  TestEq(0, Length(entity.arrayimages));

  entity := wrdperson->RunQuery( [ outputcolumns := [ imageid := "TEST_IMAGE.IMAGEID", arrayimages := "TEST_ARRAY.TEST_IMAGE.IMAGEID", test_array := "TEST_ARRAY" ]
                                 , filters := [[ field := "WRD_ID", value := entityid2 ]
                                              ]
                                 ]);

  TestEq(TRUE, entity.imageid = 0);
  TestEq("INTEGER64", GetTypeName(TypeID(entity.imageid)));
  TestEq(2, Length(entity.arrayimages));
  TestEq(TRUE, entity.arrayimages[0]!=0);
  TestEq(0i64, entity.arrayimages[1]);

  testfw->CommitWork();

  STRING legacy_imagelink := GetCachedWRDImageURL(baseurl, entityid1_imageid, [method := "none"]);
  STRING imagelink := GetCachedImageLink(wrdperson->GetEntityFields(entityid1, ["TEST_IMAGE"]).test_image, [ method := "none", baseurl := baseurl ]);
  TestEq(imagelink, Substitute(legacy_imagelink, "data.png", "noname.png"));

  RECORD wrappedinfo := GetWrappedSourceFromURL(imagelink);
  TestEq('noname.png', wrappedinfo.filename);
  TestEq(385, wrappedinfo.width);

  //Test images with a WHFS source
  testfw->BeginWork();
  BLOB toupload2 := GetWebHareResource("mod::webhare_testsuite/tests/system/testdata/homersbrain.bmp");
  wrdperson->UpdateEntity(entityid1, [ test_image := CELL[...WrapBlob(toupload2, "homersbrain.bmp"), source_fsobject := homer->id ]]);
  testfw->CommitWork();

  RECORD wrappedresized := WrapCachedImage(wrdperson->GetEntityFields(entityid1, ["TEST_IMAGE"]).test_image, [ method := "fit", setwidth := 64, setheight := 52, baseurl := GetPrimaryWebhareInterfaceURL() ]);
  TestEqLike("*/homersbrain.png", wrappedresized.link, 'no bmp in the path please..');
  TestEq(TRUE, testfw->browser->GotoWebPage(wrappedresized.link));
  TestEqMembers([width:=64, mimetype:="image/png"], ScanBlob(testfw->browser->content), "MIMEWIDTH,TYPE");
  link_wrd_homer := wrappedresized.link;
}


STRING FUNCTION GetCacheDiskPathFromLink(STRING link)
{
  link := Tokenize(link, "?")[0];
  STRING urlpart := GetUCTokenFromURL(link);
  IF (urlpart = "")
    THROW NEW Exception(`Could not recognized link ${link}`);

  INTEGER lastslash := SearchLastSubString(urlpart, "/");
  STRING filename := Substring(urlpart, lastslash + 1);

  STRING extension;
  IF (filename LIKE "*.*")
    extension := SubString(filename, SearchLastSubString(filename, "."));

  STRING folder := Left(urlpart, lastslash);
  STRING hash := ToLowercase(EncodeBase16(GetHashForString(folder, "SHA-256"))) || extension;

  hash := Left(hash, 3) || "/" || SubString(hash, 3);

  RETURN GetUnifiedCacheDiskRoot() || hash;
}

MACRO CleanupTests()
{
  IF(testfw->debug)
    Print("testing cache cleanup code... giving 10 seconds for logs etc to clear\n");
  Sleep(10000); //give caches time to clear out... by default takes 5 seconds
  //FIXME need a more accurate way to flush/ensure cache clean...

  RECORD ARRAY currentcachecontents := GetCurrentUnifiedCacheContents(MAX_DATETIME);

  // Lookup via trace leaves traces in the access log, so ignore all entries before this lookup
  DATETIME tracestarttime := GetCurrentDatetime();

  STRING filename_kikker := GetCacheDiskPathFromLink(link_kikker);
  STRING filename_unused_kikker := GetCacheDiskPathFromLink(link_unused_kikker);
  STRING filename_grayscalehomer := GetCacheDiskPathFromLink(link_grayscalehomer);
  STRING filename_fs_frogger := GetCacheDiskPathFromLink(link_fs_frogger);
  STRING filename_wrd_homer := GetCacheDiskPathFromLink(link_wrd_homer);

  currentcachecontents := SELECT *
                            FROM currentcachecontents
                           WHERE fullpath IN [ filename_kikker
                                             , filename_unused_kikker
                                             , filename_grayscalehomer
                                             , filename_fs_frogger
                                             , filename_wrd_homer
                                             ];

  TestEq(5, Length(currentcachecontents));

  RECORD ARRAY cachecontents := SELECT * FROM FilterRecentHits(currentcachecontents, after_kikker_hit, tracestarttime);

  //unused kikker should be a deletion candidate
  TestEq([[ expired := TRUE ]], SELECT expired FROM cachecontents WHERE fullpath = filename_unused_kikker);
  //but grayscale and testhost were seen 'after' kikker so should not be marked as expired
  TestEq([[ expired := FALSE ]], SELECT expired FROM cachecontents WHERE fullpath = filename_kikker);
  TestEq([[ expired := FALSE ]], SELECT expired FROM cachecontents WHERE fullpath = filename_grayscalehomer, filename_grayscalehomer);
  TestEq([[ expired := FALSE ]], SELECT expired FROM cachecontents WHERE fullpath = filename_fs_frogger, filename_fs_frogger);
  TestEq([[ expired := FALSE ]], SELECT expired FROM cachecontents WHERE fullpath = filename_wrd_homer, filename_wrd_homer);

  //forcibly delete the gray homer
  testfw->BeginWork();
  homer->DeleteSelf();
  testfw->GetWRDSchema()->^wrd_person->UpdateEntity(entityid1, [ wrd_limitdate := GetCurrentDateTime() ]);
  testfw->CommitWork();

  cachecontents := ExpireForMissingSources(cachecontents);
  TestEq([[ expired := FALSE ]], SELECT expired FROM cachecontents WHERE fullpath = filename_fs_frogger, filename_fs_frogger);
  TestEq([[ expired := TRUE ]], SELECT expired FROM cachecontents WHERE fullpath = filename_grayscalehomer, filename_grayscalehomer);
  TestEq([[ expired := TRUE ]], SELECT expired FROM cachecontents WHERE fullpath = filename_wrd_homer, filename_wrd_homer);

  /* TODO should we also be preventing the links from being recreated by locations having the old link? (ie we assume the links just die out?)
  TestEq(FALSE, testfw->browser->GotoWebPage(link_grayscalehomer));
  TestEq(404, testfw->browser->GetHTTPStatusCode());

  TestEq(FALSE, testfw->browser->GotoWebPage(link_wrd_homer));
  TestEq(404, testfw->browser->GetHTTPStatusCode());
  */
}

RunTestframework([ PTR ClearUnifiedCache
                 , PTR ImgCacheTest
                 , PTR ImgCacheSourcesTest
                 , PTR NotTheImageTest
                 , PTR FsCacheTest
                 , PTR FilenameTest
                 , PTR CreateWRDTestSchema
                 , PTR WRDImageCache
                 , PTR CleanupTests
                 ]);
