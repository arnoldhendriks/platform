<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::devsupport.whlib";
LOADLIB "wh::ipc.whlib";
LOADLIB "wh::promise.whlib";
LOADLIB "wh::internal/testfuncs.whlib";
LOADLIB "wh::javascript.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/services.whlib";

LOADLIB "wh::internal/wasm.whlib" EXPORT IsWasm;

OBJECT std := ImportJs("@webhare/std");
TestEqLike("????????-????-????-????-????????????", std->GenerateRandomId("uuidv4"));
TestEqLike("????????-????-????-????-????????????", CallJS("@webhare/std#generateRandomId", "uuidv4"));

OBJECT imp := ImportJs(Resolve("data/calls.ts"));
TestEq(imp, ImportJs(Resolve("data/calls.ts")), "should simply be the same object so callers don't have to bother centrally keeping refs");

TestEq(imp, ImportJs("./data/calls.ts"), "ImportJs should not require Resolve a long as you make clear the path is relative");
TestEq(imp, ImportJs("../nodejs/data/calls.ts"), "ImportJs should not require Resolve a long as you make clear the path is relative (2)");
TestEq("testme! 42", ImportJs("testpackage")->testme());
TestEq("testme! 42", ImportJs("testpackage/testpackage.js")->testme());

OBJECT promise42 := imp->testAsync42();
TestEq(43, imp->testSync43());
TestEq(42, WaitForPromise(promise42));
TestEq(42, WaitForPromise(promise42)); //verify we can request the resolution twice

OBJECT rejected := imp->testReject();
Sleep(300);
TestThrowsLike('Rejection', PTR WaitForPromise(rejected)); //verify we still got the rejection, preventing "Promise rejection was handled asynchronously"
TestThrowsLike('Rejection', PTR WaitForPromise(rejected)); //verify we can request the rejection twice

TestEq(42, CallJS("./data/calls.ts#testAsync42"));
TestEq(43, CallJS("./data/calls.ts#testSync43"));
//We need specifically test with a TS that was *not* loaded by ImportJS yet:
TestEq(52, CallJS("./data/calls2.ts#testAsync52"));
TestEq(53, CallJS("./data/calls2.ts#testSync53"));

STRING randomonce := imp->getOnceRandom();
STRING randomtwice := imp->getOnceRandom();
TestEq(randomonce, randomtwice, "Should be equal as we expect ImportJS to cache loaded lib instances");

STRING calljs_randomonce := CallJS("./data/calls.ts#getOnceRandom");
STRING calljs_randomtwice := CallJS("./data/calls.ts#getOnceRandom");
TestEq(calljs_randomonce, calljs_randomtwice, "Should be equal as we expect CallJS to cache loaded lib instances");

IF(NOT IsWasm()) { //Running twice, the second one should not fail outside WASM as CallJS-es should all be in separate scopes
  CallJS("./data/calls.ts#leakWork");
  TestEq(FALSE, CallJS("./data/calls.ts#isWorkOpen"));
  CallJS("./data/calls.ts#leakWork");
  TestEq(FALSE, CallJS("./data/calls.ts#isWorkOpen"));
}

IF(IsWasm()) //Remove once CallAsync is eliminated
  TestEq(randomonce, calljs_randomonce, "In WASM mode ImportJS and CallJS shouldn't make a difference");
ELSE
  TestAssert(randomonce != calljs_randomonce, "It looks like CallAsync has been eliminated? Remove this test and IF IsWASM() above this test");

//verify void functions don't crash
imp->RunVoid();
WaitForPromise(imp->RunAsyncVoid());
CallJS("./data/calls.ts#runVoid");
CallJS("./data/calls.ts#runAsyncVoid");

TestEq(GetProcessInfo().pid, ImportJS("process")->_get(IsWasm() ? "pid" : "ppid"));

OBJECTTYPE ThrowingGetterSetter <
  PUBLIC PROPERTY p(ThrowingGetter, ThrowingSetter);
  INTEGER FUNCTION ThrowingGetter() {
    THROW NEW Exception("throwing getter");
  }
  MACRO ThrowingSetter(INTEGER value) {
    THROW NEW Exception("throwing setter");
  }
>;

IF(IsWasm()) { //Support opaque object marshalling
  OBJECT jsobj_11 := CallJS("./data/calls.ts#getObject", 11);
  TestEq(11, CallJS("./data/calls.ts#getObjectValue", jsobj_11));
  OBJECT jsobj_11_b := CallJS("./data/calls.ts#returnObject", jsobj_11);
  TestEq(11, CallJS("./data/calls.ts#getObjectValue", jsobj_11_b));
  TestAssert(jsobj_11_b != jsobj_11, "Objects on the HS side don't yet have the same identity. Would have been nice...");

  // Correct handling of calling throwing setters on JS objects when using calljs
  PRINT(`test throwing setter\n`);
  TestThrowsLike("throwing setter", PTR CallJS("./data/calls.ts#setThrowingProperty", NEW ThrowingGetterSetter));
  PRINT(`test throwing getter\n`);
  TestThrowsLike("throwing getter", PTR CallJS("./data/calls.ts#getThrowingProperty", NEW ThrowingGetterSetter));
}

IF(NOT IsWasm()) { //Test crash resistance
  STRING startrandom := CallJS("./data/calls.ts#getOnceRandom");

  OBJECT eventmgr := NEW EventManager;
  eventmgr->RegisterInterest("system:webhareservice.platform:calljs.start");

  //We need a low level connection to request a crash - and also because the 'CallJS' cached connection must remain in working order
  OBJECT service := WaitForPromise(OpenWebHareService("platform:calljs"));
  STRING servicerandom := WaitForPromise(service->Invoke(Resolve("./data/calls.ts"), "getOnceRandom", VARIANT[]));
  TestEq(servicerandom, startrandom, "Verify it's the same connection");
  service->Invoke(Resolve("./data/calls.ts"), "crash", VARIANT[]); //DISCARD the result as we expect a crash
  TestEqMembers([ status := "ok" ], eventmgr->ReceiveEvent(MAX_DATETIME), "*");

  TestAssert(startrandom != CallJS("./data/calls.ts#getOnceRandom"), "verify we see a new calljs instance but not a crash");
}
