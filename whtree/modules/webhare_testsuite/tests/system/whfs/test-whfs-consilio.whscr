﻿<?wh

LOADLIB "wh::files.whlib";
LOADLIB "wh::filetypes/richdocument.whlib";
LOADLIB "wh::internet/urls.whlib";

LOADLIB "mod::system/lib/testframework.whlib";

LOADLIB "mod::consilio/lib/internal/linkreports.whlib";

LOADLIB "mod::publisher/lib/publisher.whlib";

LOADLIB "mod::system/lib/internal/whfs/contenttypes.whlib";

LOADLIB "mod::webhare_testsuite/lib/system/tests.whlib";


MACRO TestCheckedLinks()
{
  OBJECT testsite := testfw->GetTestSite();
  testfw->BeginWork();
  OBJECT testdatafile := GetTestsuiteTempFolder()->CreateFile([ name := "test.txt" ]);

  OBJECT doc := NEW RichDocument;
  doc->ImportFromRecord([ htmltext := StringToBlob('<html><body>'
                                                   || '<a href="http://example.com/4">4</a><a href="http://example.com/5">5</a><a href="/test">test</a>'
                                                   || '</body></html')
                        , embedded := DEFAULT RECORD ARRAY
                       ]);

  OBJECT whfs_consiliotest := OpenWHFSType("http://www.webhare.net/xmlns/beta/consiliotest");

  whfs_consiliotest->SetInstanceData(testdatafile->id,
      [ str :=          "http://example.com/1"
      , intextlink :=   MakeIntExtExternalLink("http://example.com/2")
      , url :=          "http://example.com/3"
      , rich :=         doc->ExportAsRecord()
      ]);

  testfw->CommitWork();

  TestEQ(
      [ [ url :=    "http://example.com/2", referrer := "intextlink" ]
      , [ url :=    "http://example.com/3", referrer := "url" ]
      , [ url :=    "http://example.com/4", referrer := "rich" ]
      , [ url :=    "http://example.com/5", referrer := "rich" ]
      , [ url :=    ResolveToAbsoluteURL(testdatafile->url,"/test"), referrer := "rich" ]
      ], (SELECT url, referrer FROM GetLinksForFSObject(testdatafile->id) ORDER BY url LIKE "http://example.com/*" DESC, url), "Verify that 'oldintextlink' does not appear in this list");

  testfw->BeginWork();

//ADDME test links in embedded objects
//ADDME mailto and invalid links
//ADDME hash links
  doc->ImportFromRecord([ htmltext := StringToBlob('<html><body>'
                                                   || '<a href="http://example.com/4a">4</a><a href="http://example.com/5a">5\u00EA</a>'
                                                   || '</body></html')
                        , embedded := DEFAULT RECORD ARRAY
                       ]);


  whfs_consiliotest->SetInstanceData(testdatafile->id,
      [ str :=          "http://example.com/1a"
      , intextlink :=   MakeIntExtExternalLink("http://example.com/2a")
      , url :=          "http://example.com/3a"
      , rich :=         doc->ExportAsRecord()
      ]);

  testfw->CommitWork();

  TestEQ(
      [ [ text := "", url :=    "http://example.com/2a", referrer := "intextlink" ]
      , [ text := "", url :=    "http://example.com/3a", referrer := "url" ]
      , [ text := "4", url :=    "http://example.com/4a", referrer := "rich" ]
      , [ text := "5\u00EA", url :=    "http://example.com/5a", referrer := "rich" ]
      ], SELECT text, url, referrer FROM GetLinksForFSObject(testdatafile->id) ORDER BY url);

  testfw->BeginWork();

  whfs_consiliotest->SetInstanceData(testdatafile->id,
      [ str :=          ""
      , rich :=         DEFAULT RECORD
      , intextlink :=   DEFAULT RECORD
      , url :=          ""
      ]);

  testfw->CommitWork();

  TestEQ(DEFAULT RECORD ARRAY, SELECT url, referrer FROM GetLinksForFSObject(testdatafile->id) ORDER BY url);

  //Verify orphan members are properly ignored
  testfw->BeginWork();
  RegisterWHFSType([ namespace := "http://www.webhare.net/xmlns/beta/consiliotest"
                   , members := [[ name := "oldintextlink", type := 16/*intextlink*/ ]]
                   ]);
  whfs_consiliotest->SetInstanceData(testdatafile->id,
      [ oldintextlink :=   MakeIntExtExternalLink("http://example.com/000")
      ], [ orphans := TRUE ]);
  testfw->CommitWork();

  TestEq(RECORD[], (SELECT url, referrer FROM GetLinksForFSObject(testdatafile->id) ORDER BY url LIKE "http://example.com/*" DESC, url));

  testfw->BeginWork();
  testdatafile->DeleteSelf();
  testfw->CommitWork();
}

RunTestframework([ PTR TestCheckedLinks
                 ]);
