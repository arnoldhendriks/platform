<?wh

LOADLIB "mod::publisher/lib/siteapi.whlib";
LOADLIB "mod::publisher/lib/siteprofiles.whlib";
LOADLIB "mod::system/lib/testframework.whlib";
LOADLIB "mod::system/lib/whfs.whlib";

LOADLIB "mod::webhare_testsuite/lib/system/tests.whlib";
LOADLIB "mod::publisher/lib/testframework.whlib";

STRING FUNCTION GetMyIntercepts(INTEGER obj)
{
  RECORD data := GetApplyTesterForObject(obj)->RunSiteprofileHookTarget("webhare_testsuite:siteprofilehook", [ result := STRING[] ]);
  RETURN Detokenize(data.result, ',');
}

STRING FUNCTION GetMyCustomNodes(INTEGER obj)
{
  STRING ARRAY result;
  FOREVERY(RECORD node FROM GetCustomSiteProfileSettings("urn:xyz", "test", obj))
    INSERT node.node->getattribute("data") INTO result AT END;
  RETURN Detokenize(result,',');
}

OBJECT siteroot2;

MACRO TestBeforeSite()
{
  testfw->BeginWork();
  siteroot2 := testfw->GetWHFSTestRoot()->EnsureFolder([name := "webhare_testsuite.site2"]);

  OBJECT asystemfolder := GetTestsuiteTempFolder()->CreateFolder([name:="systemfolder", typens := "http://www.webhare.net/xmlns/publisher/systemfolder"]);
  OBJECT aslotsfolder := asystemfolder->CreateFolder([name:="slotsfolder", typens := "http://www.webhare.net/xmlns/publisher/contentlibraries/slots"]);
  OBJECT abeaconfolder := aslotsfolder->CreateFolder([name:="beaconsfolder", typens := "http://www.webhare.net/xmlns/publisher/contentlibraries/beacons"]);
  OBJECT subfile := abeaconfolder->CreateFile([name:="subfile"]);

  testfw->CommitWork();

  TestEq(0, siteroot2->parentsite);
  TestEq('/webhare-tests/webhare_testsuite.testfolder/webhare_testsuite.site2/', siteroot2->whfspath);
  TestEq("applytest-megaglobal,applytest-whfspath", GetMyCustomNodes(siteroot2->id));
  TestEq("intercept-whfspath,intercept-megaglobal", GetMyIntercepts(siteroot2->id));

  TestEq("applytest-megaglobal,parentmask-tmp,parentregex-tmp,is-system-folder", GetMyCustomNodes(asystemfolder->id));
  TestEq("applytest-megaglobal,in-system-folder,is-slots-folder", GetMyCustomNodes(aslotsfolder->id));
  TestEq("applytest-megaglobal,in-system-folder,in-slots-folder,is-beacons-folder", GetMyCustomNodes(abeaconfolder->id));
  TestEq("applytest-megaglobal,in-system-folder,in-slots-folder,in-beacons-folder", GetMyCustomNodes(subfile->id));
}

MACRO TestRules()
{
  siteroot2 := OpenWHFSObject(siteroot2->id);
  TestEq(OpenSiteByName("webhare_testsuite.site2")->id, siteroot2->parentsite);
  TestEq("applytest-megaglobal,applytest-whfspath", GetMyCustomNodes(siteroot2->id));

  testfw->BeginWork();

  OBJECT applytest1 := GetTestsuiteTempFolder()->CreateFolder([name:="applytest1"]);
  TestEq("/webhare-tests/webhare_testsuite.testsite/tmp/applytest1/", applytest1->whfspath);
  TestEq("applytest-megaglobal,applytest-whfsregex,pathmask-applytest1,parentmask-tmp,parentregex-tmp,pathregex-applytest1", GetMyCustomNodes(applytest1->id));

  applytest1->UpdateMetadata([typens:="http://www.webhare.net/xmlns/webhare_testsuite/applytestfolder"]);
  TestEq("applytest-megaglobal,applytest-whfsregex,pathmask-applytest1,folderapplytestertest,parentmask-tmp,parentregex-tmp,pathregex-applytest1", GetMyCustomNodes(applytest1->id));

  //even apply rules from OTHER websites should apply to us (ie they should escape automatisch siteid= tagging to their implicit apply rules)
  applytest1->UpdateMetadata([typens:="http://www.webhare.net/xmlns/webhare_testsuite/alttestfolder"]);
  TestEqLike("*alttestfolder*", GetMyCustomNodes(applytest1->id));

  OBJECT applytest1sub := applytest1->CreateFolder([name:="sub"]);
  TestEq("applytest-megaglobal,parentregex-tmp-applytest1,siteregex-webharetestsuitesuite", GetMyCustomNodes(applytest1sub->id));

  //Now test some rules applied through our main siteprofile
  OBJECT throughglobalsiteprofile := GetTestsuiteTempFolder()->CreateFolder([name:="applytest-throughglobal"]);
  TestEq("applytest-megaglobal,applytest-throughglobal,applytest-throughglobal-next,applytest-throughglobal-next2,parentmask-tmp,parentregex-tmp", GetMyCustomNodes(throughglobalsiteprofile->id));

  testfw->CommitWork();
}

RunTestFramework( [ PTR TestBeforeSite
                  , PTR PrepareTestModuleWebDesignWebsite("webhare_testsuite:alttest", [ istemplate := FALSE, withcontent := FALSE, sitename := "webhare_testsuite.site2" ])
                  , PTR TestRules
                  ]);
