// Note: you need two webhares to run this test. See twoharetests.sh

import * as test from "@mod-tollium/js/testframework";
import { invokeSetupForTestSetup, type TestSetupData } from "@mod-webhare_testsuite/js/wts-testhelpers";

let setupdata: TestSetupData | null = null;
let setup2data: TestSetupData | null = null;

test.runTests(
  [
    async function () {
      const setup1 = invokeSetupForTestSetup({ createsysop: true });

      //this removes the testsuite module on the second server
      const setup2 = invokeSetupForTestSetup({ onpeerserver: true });
      setupdata = await setup1;
      setup2data = await setup2;
      await test.load(test.getWrdLogoutURL(setupdata.testportalurl + "?app=publisher(/webhare-tests/webhare_testsuite.testsite/tmp)/managefoldersync/add/selectpeer&notifications=0"));
      await test.wait("ui");

      // Wait for login page to appear
      await test.sleep(200);
      test.setTodd('loginname', setupdata.sysopuser);
      test.setTodd('password', setupdata.sysoppassword);
      test.clickToddButton('Login');
      await test.wait('ui');
    },

    "Setup peering",
    async function () {
      test.clickToddButton("Add");
      await test.wait("ui");
      test.setTodd('peer', setupdata!.peerserver);

      const oauth_auth_wait = Promise.withResolvers<void>();
      //@ts-ignore yes it's an ugly hack..
      test.getWin().open = async url => {
        const overlay = test.getDoc().createElement("div");
        //@ts-ignore yes it's an ugly hack..
        window.parent.overlay = overlay;
        overlay.innerHTML =
          `<div style="position:fixed; top: 20px; left:20px; width:800px; height:640px;">
             <iframe src="${url}" style="width:800px;height:640px;border:none;display:block"></iframe>
           </div>`;
        test.getDoc().body.appendChild(overlay);

        for (; ;) {
          if (!test.compByName("peer"))
            break; //dialog is gone so peering must have completed

          /* get the peering code to progress:
             the second webhare has been configured with the feature webhare_testsuite:insecure_interface on its backend
             which disables some security and loads remotecontrol.js which will do the actual login for us when postMessage
             as there's no way to directly control the iframe of course */
          overlay!.querySelector("iframe")!.contentWindow!.postMessage(
            { dopeering: { overridetoken: new URL(setup2data!.overridetoken, location.href).searchParams.get("overridetoken") } }, "*");

          await test.sleep(100);
        }

        test.getDoc().body.removeChild(overlay);
        oauth_auth_wait.resolve();
      };

      test.clickToddButton("Connect");
      await oauth_auth_wait.promise;

      test.clickToddButton("Connect");
      await test.wait("ui");

      await test.sleep(300); //the test.focus below wasn't enough, for some reason focus doesn't get set. workaround that race..

      test.clickToddButton("Select folder");
      await test.wait("ui");
      test.clickToddButton("OK");
      await test.wait("ui");
      test.eq("/webhare-tests/webhare_testsuite.testsite/tmp", test.compByName("remotepath").querySelector("input").value);
      test.clickToddButton("OK");
    }

  ]);
