<?wh

LOADLIB "wh::datetime.whlib";

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/testframework.whlib";
LOADLIB "mod::system/lib/internal/dbschema.whlib";
LOADLIB "mod::system/lib/internal/moduleimexport.whlib";
LOADLIB "mod::system/lib/internal/rightsmgmt.whlib";
LOADLIB "mod::system/lib/internal/userrights/cleanup.whlib";
LOADLIB "mod::system/lib/internal/userrights/resync.whlib";

LOADLIB "mod::webhare_testsuite/lib/system/tests.whlib";

TABLE
  < INTEGER id NULL := 0
  , INTEGER parent NULL := 0
  , STRING name
  ; KEY id
  > my_rights_tree;

MACRO Prepare()
{
  EnsureUserMgmtStateClean(FALSE); //prevent spurious errors from earlier tests
}

MACRO CreateProblemRight()
{
  testfw->SetupTestModule( [ moduledefinition :=
    `<?xml version="1.0" encoding="UTF-8"?>
<module xmlns="http://www.webhare.net/xmlns/system/moduledefinition">
  <meta>
    <version>0.1.0</version>
    <description>Test module</description>
    <packaging download="true" webhareversion=">= 5.0.0" />
    <validation options="nowarnings" />
  </meta>

  <!-- create our own table so we can test full rights creation -->
  <databaseschema xmlns:d="http://www.webhare.net/xmlns/whdb/databaseschema">
    <d:table name="rights_tree" primarykey="id">
      <d:integer name="id" autonumberstart="1"/>
      <d:varchar name="name" maxlength="256" nullable="0"/>
      <d:integer name="parent" references=".rights_tree" ondelete="cascade"/>
    </d:table>
  </databaseschema>

  <rights>
    <objecttype name="rights_tree" table=".rights_tree" describer="mod::webhare_testsuite/lib/internal/support.whlib#unitobjecttypedescriber"/>
    <right name="danglingright">
      <impliedby right="system:sysop" />
    </right>
    <right name="danglingright2" objecttype="rights_tree">
      <impliedby right="system:sysop" />
    </right>
  </rights>
</module>
`]);

  testfw->BeginWork();
  my_rights_tree := BindTransactionToTable(GetPrimary()->id, "webhare_testsuite_temp.rights_tree");
  INSERT INTO my_rights_tree(id,name,parent) VALUES(1,"Test 1",0);
  INSERT INTO my_rights_tree(id,name,parent) VALUES(2,"Test 1.2",1);
  testfw->GetUserObject("sysop")->UpdateGrant("grant", "webhare_testsuite_temp:danglingright", 0, testfw->GetUserObject("lisa"), [ withgrantoption := TRUE ]);
  testfw->GetUserObject("sysop")->UpdateGrant("grant", "webhare_testsuite_temp:danglingright2", 2, testfw->GetUserObject("lisa"), [ withgrantoption := TRUE ]);
  testfw->CommitWork();
}

ASYNC MACRO TestProblemRights()
{
  INTEGER testsuitemoduleid := SELECT AS INTEGER id FROM system_internal.modules WHERE name = "webhare_testsuite";
  INTEGER globalrightid := SELECT AS INTEGER id FROM system_internal.module_rights WHERE name = "globalright" AND module = testsuitemoduleid;
  TestEq(TRUE, globalrightid != 0);

  INTEGER testmoduleid := SELECT AS INTEGER id FROM system_internal.modules WHERE name = testfw_testmodule;
  TestEq(TRUE, testmoduleid != 0);
  TestEq(TRUE, RecordExists(SELECT FROM system_internal.module_rights WHERE module = testmoduleid AND name = "danglingright"));
  TestEq(TRUE, RecordExists(SELECT FROM system_internal.module_rights WHERE module = testmoduleid AND name = "danglingright2"));

  DeleteModule(testfw_testmodule);

  testfw->BeginWork();
  UPDATE system_internal.module_rights SET orphansince := GetCurrentDatetime() WHERE id = globalrightid;
  CleanupObsoleteRights(); //shouldn't clean our dangling rights yet..
  testfw->CommitWork();

  TestEq([[ orphansince := DEFAULT DATETIME ]], SELECT orphansince FROM system_internal.module_rights WHERE id = globalrightid, "Orphansince should have been reset on webhare_testsuite:globalright");
  //dangling rights should not be removed yet, but they should be an orphan!
  TestEq(TRUE, RecordExists(SELECT FROM system_internal.module_rights WHERE module = testmoduleid AND name = "danglingright" AND orphansince != DEFAULT DATETIME));
  TestEq(TRUE, RecordExists(SELECT FROM system_internal.module_rights WHERE module = testmoduleid AND name = "danglingright2" AND orphansince != DEFAULT DATETIME));

  testfw->BeginWork();

  __ResetRightsCaches();
  //there's no way to revoke a broken right as we can't find its storage tables.
  TestThrowsLike("Right*does not exist*", PTR testfw->GetUserObject("sysop")->UpdateGrant("revoke", "webhare_testsuite_temp:danglingright", 0, testfw->GetUserObject("lisa")));
  DELETE FROM my_rights_tree; //this should cascade danglingright2
  CleanupObsoleteRights(); //should now clean the danglingright2

  testfw->CommitWork();

  TestEq(TRUE, RecordExists(SELECT FROM system_internal.module_rights WHERE module = testmoduleid AND name = "danglingright"));
  TestEq(FALSE, RecordExists(SELECT FROM system_internal.module_rights WHERE module = testmoduleid AND name = "danglingright2"), "danglingright2 should have been safe for immediate removal");

  testfw->BeginWork();
  UPDATE system_internal.module_rights SET orphansince := MakeDate(2021,1,1) WHERE module = testmoduleid AND name = "danglingright";
  CleanupObsoleteRights(); //should now clean the global danglingright
  TestEq(FALSE, RecordExists(SELECT FROM system_internal.module_rights WHERE module = testmoduleid AND name = "danglingright"));
  testfw->CommitWork();
}

MACRO TestFixUnconnectedItems()
{
  EnsureUserMgmtStateClean(TRUE); //make sure all is okay before we start. also verifies no loose ends left above
  testfw->BeginWork();

  OBJECT deletedunit :=      testfw->GetWRDSchema()->^whuser_unit->CreateEntity([ wrd_title := "Deletable unit" ]);
  OBJECT expiredunit :=      testfw->GetWRDSchema()->^whuser_unit->CreateEntity([ wrd_title := "Expirable unit" ]);

  OBJECT deletedchildunit := testfw->GetWRDSchema()->^whuser_unit->CreateEntity([ wrd_leftentity := deletedunit->id, wrd_title := "Deletable child unit" ]); //note, this unit is okay as units may live in the root!
  OBJECT expiredchildunit := testfw->GetWRDSchema()->^whuser_unit->CreateEntity([ wrd_leftentity := expiredunit->id, wrd_title := "Expirable child unit" ]);

  OBJECT deletedchildrole := testfw->GetWRDSchema()->^whuser_role->CreateEntity([ wrd_leftentity := deletedunit->id, wrd_title := "Deletable child role" ]);
  OBJECT expiredchildrole := testfw->GetWRDSchema()->^whuser_role->CreateEntity([ wrd_leftentity := expiredunit->id, wrd_title := "Expirable child role" ]);

  OBJECT deletedchilduser := testfw->GetWRDSchema()->^wrd_person->CreateEntity([ whuser_unit := deletedunit->id, wrd_contact_email := "deletable@beta.webhare.net" ]);
  OBJECT expiredchilduser := testfw->GetWRDSchema()->^wrd_person->CreateEntity([ whuser_unit := expiredunit->id, wrd_contact_email := "expirable@beta.webhare.net" ]);

  RECORD ARRAY resync := testfw->userapi->ResyncToSystemTables();
  IF(Length(resync) != 8)
  {
    DumpValue(resync,'boxed');
    TestEq(8, Length(resync), "Ensure all newobjects are created like real users/units would");
  }

  deletedunit->DeleteEntity();
  expiredunit->CloseEntity();

  RECORD ARRAY fixes := FixUnconnectedItems(testfw->GetWRDSchema());
  IF(Length(fixes) != 4)
  {
    DumpValue(fixes,'boxed');
    TestEq(4, Length(fixes), "Expecting 5 fixes (2 users + 1 role + 1 unit)");
  }

  INTEGER lostandfound := testfw->GetWRDSchema()->^whuser_unit->Search("WRD_TAG", "WH_LOSTANDFOUND");
  TestEq(FALSE, deletedchildunit->IsLive());
  TestEq(lostandfound, expiredchildunit->GetField("WRD_LEFTENTITY"));
  TestEq(FALSE, deletedchildrole->IsLive());
  TestEq(lostandfound, expiredchildrole->GetField("WRD_LEFTENTITY"));
  TestEq(lostandfound, deletedchilduser->GetField("WHUSER_UNIT"));
  TestEq(lostandfound, expiredchilduser->GetField("WHUSER_UNIT"));

  testfw->CommitWork();

  //FIXME: not checking for loose ends anymore, we _know_ the actions above have generated loose ends (deletions) BUT we need incremental changes to prevent that
}

RunTestFramework([ PTR Prepare
                 , PTR CreateProblemRight
                 , PTR TestProblemRights
                 , PTR TestFixUnconnectedItems
                 ], [ testusers := [[ login := "sysop", grantrights := ["system:sysop"] ]
                                   ,[ login := "lisa"]
                                   ]
                    ]
                 );
