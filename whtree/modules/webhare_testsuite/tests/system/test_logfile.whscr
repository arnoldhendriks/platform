﻿<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::files.whlib";
LOADLIB "wh::filetypes/archiving.whlib";

LOADLIB "mod::system/lib/logging.whlib";
LOADLIB "mod::system/lib/internal/cluster/logging.whlib";
LOADLIB "mod::system/lib/testframework.whlib";

MACRO TestAccessLogFileReader()
{
  STRING lines :=
    'remoteip - username [01/Aug/2011:15:06:13 +0000] "GET url HTTP/1.1" 200 300 "referrer" "useragent" hostname 81 244 7636\n' ||
    'remoteip - username [01/Aug/2011:15:06:14 +0000] "GET url HTTP/1.1" 200 300 "referrer" "useragent" hostname 81 244 trackerid 7636\n' ||
    'remoteip - - [01/Aug/2011:15:06:15 +0000] "GET url HTTP/1.1" 200 300 "referrer" "useragent" hostname 81 244 trackerid "mimetype" 7636\n' ||
    '{"@timestamp":"2024-12-03T12:59:48.297Z","ip":"10.55.55.55","method":"GET","url":"https://webhare.moe.sf.webhare.dev/testoutput/live_api/chatplane/sub?token=&visitorid=&visitordata=&visitorpermissions=&item=item%3A%7B%22channeltype%22%3A%22sub%22%2C%22id%22%3A%22BC-36z88N7c-LLks1NiAloVCw%22%7D%4030FBCB2E3752A148F7C500A4DE853B79C4AAF9BE","statusCode":200,"bodySent":129,"userAgent":"node","mimeType":"application/json","responseTime":0.002244}\n';

  RECORD ARRAY rows;
  BLOB logdata := StringToBlob(lines);
  OBJECT r := __OpenLogFileBlob(logdata, LENGTH(logdata), "access", 0);
  WHILE (TRUE)
  {
    RECORD rec := r->ReadRecord();
    IF (NOT RecordExists(rec))
      BREAK;
    INSERT rec INTO rows AT END;
  }

  TestEQ(
      [ [ stream :=         "hit"
        , remoteip :=       "remoteip"
        , time :=           MakeDateTime(2011,8,1,15,6,13)
        , time_msecs :=     FALSE
        , url :=            "url"
        , httpcode :=       200
        , download :=       300i64
        , upload :=         244i64
        , referrer :=       "referrer"
        , useragent :=      "useragent"
        , host :=           "hostname"
        , port :=           81
        , trackingstamp :=  ""
        , pagetime :=       7636i64
        , mimetype :=       ""
        , method :=         "GET"
        , username :=       "username"
        ]
      , [ stream :=         "hit"
        , remoteip :=       "remoteip"
        , time :=           MakeDateTime(2011,8,1,15,6,14)
        , time_msecs :=     FALSE
        , url :=            "url"
        , httpcode :=       200
        , download :=       300i64
        , upload :=         244i64
        , referrer :=       "referrer"
        , useragent :=      "useragent"
        , host :=           "hostname"
        , port :=           81
        , trackingstamp :=  "trackerid"
        , mimetype :=       ""
        , pagetime :=       7636i64
        , method :=         "GET"
        , username :=       "username"
        ]
      , [ stream :=         "hit"
        , remoteip :=       "remoteip"
        , time :=           MakeDateTime(2011,8,1,15,6,15)
        , time_msecs :=     FALSE
        , url :=            "url"
        , httpcode :=       200
        , download :=       300i64
        , upload :=         244i64
        , referrer :=       "referrer"
        , useragent :=      "useragent"
        , host :=           "hostname"
        , port :=           81
        , trackingstamp :=  "trackerid"
        , mimetype :=       "mimetype"
        , pagetime :=       7636i64
        , method :=         "GET"
        , username :=       ""
        ]
      , [ stream :=         "hit"
        , remoteip :=       "10.55.55.55"
        , time :=           MakeDateTime(2024,12,3,12,59,48,297)
        , time_msecs :=     TRUE
        , url :=            "/testoutput/live_api/chatplane/sub?token=&visitorid=&visitordata=&visitorpermissions=&item=item%3A%7B%22channeltype%22%3A%22sub%22%2C%22id%22%3A%22BC-36z88N7c-LLks1NiAloVCw%22%7D%4030FBCB2E3752A148F7C500A4DE853B79C4AAF9BE"
        , httpcode :=       200
        , download :=       129i64
        , upload :=         0i64
        , referrer :=       ""
        , useragent :=      "node"
        , host :=           "webhare.moe.sf.webhare.dev"
        , port :=           443
        , trackingstamp :=  ""
        , mimetype :=       "application/json"
        , pagetime :=       2244i64
        , method :=         "GET"
        , username :=       ""
        ]
      ], rows);
  r->Close();
}

MACRO TestLineReader()
{
  BLOB linesblob := StringToBlob(
      RepeatText("a", 40000) || "\n" ||
      RepeatText("b", 40000) || "\n" ||
      "c\n");

  BLOB zippedlinesblob := MakeZlibCompressedFile(linesblob, "GZIP", 9);

  OBJECT reader := NEW __LineReader([ type := "blob", data := linesblob, format := 0, startposition := 0 ]);
  TestEQ(
      [ [ position := 0i64,     newposition := 40001i64,  line := RepeatText("a", 40000) ]
      , [ position := 40001i64, newposition := 80002i64,  line := RepeatText("b", 40000) ]
      , [ position := 80002i64, newposition := 80004i64,  line := "c" ]
      ], reader->ReadLines());

  TestEQ(DEFAULT RECORD ARRAY, reader->ReadLines());

  reader->Close();

  // Test with non-0 starting position
  reader := NEW __LineReader([ type := "blob", data := linesblob, format := 0, startposition := 40001 ]);
  TestEQ(
      [ [ position := 40001i64, newposition := 80002i64,  line := RepeatText("b", 40000) ]
      , [ position := 80002i64, newposition := 80004i64,  line := "c" ]
      ], reader->ReadLines());
  TestEQ(DEFAULT RECORD ARRAY, reader->ReadLines());
  reader->Close();

  // zipped tst with non-0 starting position
  reader := NEW __LineReader([ type := "blob", data := zippedlinesblob, format := 1, startposition := 40001 ]);
  TestEQ(
      [ [ position := 40001i64, newposition := 80002i64,  line := RepeatText("b", 40000) ]
      , [ position := 80002i64, newposition := 80004i64,  line := "c" ]
      ], reader->ReadLines());
  TestEQ(DEFAULT RECORD ARRAY, reader->ReadLines());
  reader->Close();
}

MACRO TestCheckpointedLogReader()
{
  ModuleLog("webhare_testsuite:test", "--start of tests--");
  FlushModuleLog("webhare_testsuite:test");

  OBJECT logstream := OpenWebHareLogStream("betatest", DEFAULT DATETIME, MAX_DATETIME);
  WHILE(RecordExists(logstream->ReadRecord()))
    /* flush any records from earlier testruns */;

  STRING checkpoint_start := logstream->checkpoint;

  ModuleLog("webhare_testsuite:test", "Hi everybody");
  FlushModuleLog("webhare_testsuite:test");

  logstream := OpenWebHareLogStream("betatest", DEFAULT DATETIME, MAX_DATETIME);
  logstream->checkpoint := checkpoint_start;
  TestEqMembers([ parts := [ "Hi everybody" ] ], logstream->ReadRecord(), "*");
  TestEq(DEFAULT RECORD, logstream->ReadRecord());

  logstream->checkpoint := checkpoint_start;  //test re-reading
  TestEqMembers([ parts := [ "Hi everybody" ] ], logstream->ReadRecord(), "*");
  TestEq(DEFAULT RECORD, logstream->ReadRecord());

  STRING checkpoint_2nd := logstream->checkpoint;
  logstream := OpenWebHareLogStream("betatest", DEFAULT DATETIME, MAX_DATETIME);
  logstream->checkpoint := checkpoint_2nd;
  TestEq(checkpoint_2nd, logstream->checkpoint);
  TestEq(DEFAULT RECORD, logstream->ReadRecord());
  TestEq(checkpoint_2nd, logstream->checkpoint); //fix bug: opening a stream, setting a checkpoint but never succesfully reading a record breaks checkpoint progress
}

RunTestframework([ PTR TestLineReader
                 , PTR TestAccessLogFileReader
                 , PTR TestCheckpointedLogReader
                 ]);
