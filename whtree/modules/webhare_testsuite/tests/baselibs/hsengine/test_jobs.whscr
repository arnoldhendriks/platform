﻿<?wh
/// @short Ports functions test

LOADLIB "wh::devsupport.whlib";
LOADLIB "wh::promise.whlib";
LOADLIB "wh::internal/hsselftests.whlib";
LOADLIB "wh::datetime.whlib";
LOADLIB "wh::ipc.whlib";
LOADLIB "wh::os.whlib";
LOADLIB "wh::internal/testfuncs.whlib";
LOADLIB "wh::internal/jobs.whlib";
LOADLIB "wh::javascript.whlib";


MACRO TestReadSignalledStatus(INTEGER testid, BOOLEAN expect_status, INTEGER handle)
{
  BOOLEAN is_signalled := WaitForMultiple([ handle ], DEFAULT INTEGER ARRAY, 0) = handle;

  TestEqual(testid, expect_status, is_signalled);
}

MACRO BaseTest()
{
  OpenTest("TestJobs: Base");

  TestEq(0,__GetNumSubJobs());
  OBJECT promise := AsyncCallFunctionFromJob("wh::system.whlib#Left", "", 0);
  TestEq(1,__GetNumSubJobs(), "Must be 1 job here as it can't have had a chance to run yet");
  WaitForPromise(promise);
  TestEq(0,__GetNumSubJobs());

  SetExternalSessionData("test-1");

  CONSTANT RECORD ARRAY env_override := [ [ name := "test", value := "job-env-override" ], [ name := "test2", value := "job-env-override2" ] ];

  TestEQ("test-1", GetExternalSessionData());

  RECORD authrec := GetAuthenticationRecord();
  SetAuthenticationRecord([ jobtest := 1 ]);

  RECORD res := CreateJob("mod::webhare_testsuite/tests/baselibs/hsengine/testdata_jobs_a.whlib");
  OBJECT job := res.job;
  job->SetConsoleArguments(["test1","test2"]);
  TestEQ([ jobtest := 1 ], job->GetAuthenticationRecord());
  job->SetAuthenticationRecord([ jobtest := 2]);
  TestEQ([ jobtest := 2 ], job->GetAuthenticationRecord());

  // in node, '_' is set to the main script (and differs in workers)
  TestEQ((SELECT * FROM GetEnvironment() WHERE name != "_"), (SELECT * FROM job->GetEnvironment() WHERE name != "_"));
  job->SetEnvironment(env_override);
  TestEQ(env_override, job->GetEnvironment());

  TestEQ("test-1", job->GetExternalSessionData());
  job->SetExternalSessionData("test-2");
  TestEQ("test-2", job->GetExternalSessionData());

  SetAuthenticationRecord(authrec);
  TestEQ([ jobtest := 2 ], job->GetAuthenticationRecord());

  TestEq(DEFAULT RECORD ARRAY, res.errors);
  TestEq(TRUE, ObjectExists(job));

  INTEGER outputhandle := job->CaptureOutput();
  res := job->ipclink->SendMessage([ type := "testmessage" ]);

  TestEq("ok", res.status);

  job->Start();
  job->Wait(MAX_DATETIME);
  res := job->ipclink->ReceiveMessage(MAX_DATETIME);

  TestEq("ok", res.status);
  TestEq("ack", res.msg.type);
  TestEq(["test1","test2"], res.msg.args);
  TestEq([jobtest := 2 ], res.msg.authrec);
  TestEQ(env_override, res.msg.env);
  TestEQ("job-env-override", res.msg.env_test);
  TestEQ("test:job-env-override", res.msg.env_first);

  STRING output := ReadFrom(outputhandle, 4096);
  TestEq("job output", output);

  //FIXME might be racy, perhaps we should wait for the job to finish first?
  TestEq(42, job->GetConsoleExitCode());

  job->Close();
  TestEq(0, job->handle);
  TestEq(0, job->ipclink->handle);

  res := CreateJob("mod::webhare_testsuite/tests/baselibs/hsengine/testdata_jobs_a.whlib");
  job := res.job;
  job->CaptureOutput();
  job->Start();
  res := job->ipclink->DoRequest([ type := "testmessage" ]);
  job->Close();

  // in node, '_' is set to the main script (and differs in workers)
  TestEQ((SELECT * FROM GetEnvironment() WHERE name != "_"), (SELECT * FROM res.msg.env WHERE name != "_"));
  TestEQ((SELECT AS STRING `${name}:${GetEnvironmentVariable(name)}` FROM GetEnvironment() WHERE value != ""), res.msg.env_first);

  CloseTest("TestJobs: Base");
}

MACRO TestIPCLinkGoneAfterCrash()
{
  OpenTest("TestJobs: IPCLinkGoneAfterCrash");

  // Tests whether the ipclink is gone after job crashes before getting it
  OBJECT port := CreateIPCPort("jobcrashfast");

  RECORD res := CreateJob("mod::webhare_testsuite/tests/baselibs/hsengine/testdata_jobs_a.whlib");
  OBJECT job := res.job;

  job->Start();

  OBJECT link := port->Accept(MAX_DATETIME);
  WHILE (TRUE)
  {
    // wait for crash
    IF (link->ReceiveMessage(MAX_DATETIME).status = "gone")
      BREAK;
  }

  RECORD rec := job->ipclink->ReceiveMessage(AddTimeToDate(1000, GetCurrentDateTime()));
  TestEq("gone", rec.status);
  TestEQ(TRUE, job->Wait(AddTimeToDate(100, GetCurrentDateTime())), "Job should have terminated in reasonable time");
  TestEq(-1, job->GetConsoleExitCode());

  port->Close();
  link->Close();
  job->Close();

  CloseTest("TestJobs: IPCLinkGoneAfterCrash");
}

MACRO TestOverrideExecuteLibrary()
{
  OpenTest("TestJobs: OverrideExecuteLibrary");

  OBJECT port := CreateIPCPort("testoverrideexecutelibrary");

  RECORD res := CreateJob("mod::webhare_testsuite/tests/baselibs/hsengine/testdata_jobs_a.whlib");
  OBJECT job := res.job;

  job->SetConsoleArguments(["overrideexecutelibrary"]);
  job->Start();

  DATETIME wait_until := AddTimeToDate(30000, GetCurrentDateTime());

  OBJECT link := port->Accept(wait_until);
  IF (NOT ObjectExists(link))
    ABORT(AnyToString(job->GetErrors(), "boxed"));
  RECORD msg := link->ReceiveMessage(wait_until);
  TestEQ([ type := "foundme!" ], msg.msg);

  port->Close();
  link->Close();
  job->Close();

  CloseTest("TestJobs: OverrideExecuteLibrary");
}

MACRO TestInterrupt()
{
  // Interrupts are not yet implemented in wasm
  IF (IsWasm())
    RETURN;

  OBJECT job := CreateJob("mod::webhare_testsuite/tests/baselibs/hsengine/testdata_jobs_a.whlib").job;
  job->Start();
  job->ipclink->DoRequest([ type := "interruptcallback" ]);
  TestEQ(TRUE, job->SendInterrupt());
  TestEQMembers([ status := "ok", msg := [ type := "interrupt" ] ], job->ipclink->ReceiveMessage(MAX_DATETIME), "*");
  job->Close();
}

VARIANT FUNCTION ReturnValue(VARIANT x) { RETURN x; }

ASYNC MACRO EnsureNotFulfilled(OBJECT promise, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions([ timeout := 50 ], options);
  TestEQ("timeout", AWAIT CreatePromiseRace(OBJECT[ promise->Then(PTR ReturnValue("got value")), CreateSleepPromise(options.timeout)->Then(PTR ReturnValue("timeout")) ]));
}

ASYNC MACRO TestAsyncIterator()
{
  OBJECT job := CreateJob("mod::webhare_testsuite/tests/baselibs/hsengine/testdata_jobs_a.whlib").job;
  INTEGER outputhandle := job->CaptureOutput();
  job->Start();
  job->ipclink->SendMessage([ type := "control" ]);

  OBJECT itr := MakeJobAsyncIterator(job, CELL[ outputhandle ]);
  OBJECT v := itr->Next();
  AWAIT EnsureNotFulfilled(v);
  job->ipclink->SendMessage([ type := "output", data := "line1\nline2\nline3" ]);
  TestEQ([ done := FALSE, value := [ type := "output", data := "line1" ] ], AWAIT v);
  TestEQ([ done := FALSE, value := [ type := "output", data := "line2" ] ], AWAIT itr->Next());
  v := itr->Next();
  AWAIT EnsureNotFulfilled(v);
  job->ipclink->SendMessage([ type := "output", data := "\nline4" ]);
  TestEQ([ done := FALSE, value := [ type := "output", data := "line3" ] ], AWAIT v);
  v := itr->Next();
  AWAIT EnsureNotFulfilled(v);
  job->ipclink->SendMessage([ type := "terminate" ]);
  TestEQ([ done := FALSE, value := [ type := "output", data := "line4" ] ], AWAIT v);
  TestEQ([ done := FALSE, value := [ type := "close", exitcode := 0, errors := RECORD[] ] ], AWAIT itr->Next());
  TestEQ([ done := TRUE, value := [ exitcode := 0, errors := RECORD[] ] ], AWAIT itr->Next());
  TestEQ(0, job->handle); // is auto-closed

  job := CreateJob("mod::webhare_testsuite/tests/baselibs/hsengine/testdata_jobs_a.whlib").job;
  outputhandle := job->CaptureOutput();
  job->Start();
  job->ipclink->DoRequest([ type := "control" ]);

  itr := MakeJobAsyncIterator(job, CELL[ outputhandle, autoclose := FALSE, tokenizelines := FALSE ]);
  v := itr->Next();
  AWAIT EnsureNotFulfilled(v);
  job->ipclink->SendMessage([ type := "output", data := "line1\nline2\nline3" ]);
  TestEQ([ done := FALSE, value := [ type := "output", data := "line1\nline2\nline3" ] ], AWAIT v);
  v := itr->Next();
  AWAIT EnsureNotFulfilled(v);
  job->ipclink->SendMessage([ type := "output", data := "\nline4" ]);
  TestEQ([ done := FALSE, value := [ type := "output", data := "\nline4" ] ], AWAIT v);
  job->ipclink->SendMessage([ type := "terminate" ]);
  TestEQ([ done := FALSE, value := [ type := "close", exitcode := 0, errors := RECORD[] ] ], AWAIT itr->Next());
  TestEQ([ done := TRUE, value := [ exitcode := 0, errors := RECORD[] ] ], AWAIT itr->Next());
  TestEQ(TRUE, job->handle != 0); // is not auto-closed
}

MACRO TestErrorHandling()
{
  {
    OBJECT job := CreateJob("mod::webhare_testsuite/tests/baselibs/hsengine/testdata_jobs_a.whlib").job;
    job->Start();
    job->ipclink->DoRequest([ type := "control" ]);
    job->ipclink->SendMessage([ type := "crash", data := "CRASHMSG" ]);
    TestEQMembers(
      [ [ iserror := TRUE,  iswarning := FALSE, istrace := FALSE, filename := "mod::webhare_testsuite/tests/baselibs/hsengine/testdata_jobs_a.whlib", code := 182, param1 := 'CRASHMSG',      param2 := '', func := ':INITFUNCTION', message := 'Custom error message: \'CRASHMSG\'.' ]
      , [ iserror := FALSE, iswarning := FALSE, istrace := TRUE,  filename := "mod::webhare_testsuite/tests/baselibs/hsengine/testdata_jobs_a.whlib", code := -1,  param1 := ':INITFUNCTION', param2 := '', func := ':INITFUNCTION', message := '' ]
      ], job->GetErrors(), "*");
    job->Close();
  }
  {
    OBJECT job := CreateJob("mod::webhare_testsuite/tests/baselibs/hsengine/testdata_jobs_a.whlib").job;
    job->Start();
    job->ipclink->DoRequest([ type := "control" ]);
    job->ipclink->SendMessage([ type := "throw", data := "THROWMSG" ]);
    TestEQMembers(
      [ [ iserror := TRUE,  iswarning := FALSE, istrace := FALSE, filename := "mod::webhare_testsuite/tests/baselibs/hsengine/testdata_jobs_a.whlib", code := 203, param1 := 'EXCEPTION',     param2 := 'THROWMSG', func := ':INITFUNCTION', message := 'Exception EXCEPTION: THROWMSG.' ]
      , [ iserror := FALSE, iswarning := FALSE, istrace := TRUE,  filename := "mod::webhare_testsuite/tests/baselibs/hsengine/testdata_jobs_a.whlib", code := -1,  param1 := ':INITFUNCTION', param2 := '',         func := ':INITFUNCTION', message := '' ]
      ], job->GetErrors(), "*");
    job->Close();
  }
}

BaseTest();
TestIPCLinkGoneAfterCrash();
TestOverrideExecuteLibrary();
TestInterrupt();
WaitForPromise(TestAsyncIterator());
TestErrorHandling();
