﻿<?wh
/// @short Datetime tests

LOADLIB "wh::internal/hsselftests.whlib";
LOADLIB "wh::os.whlib";

RECORD arguments;
RECORD result;

MACRO ParseArgumentsTest()
{
  OpenTest("TestArguments: ParseArgumentsTest");

  // Test different kinds of arguments
  arguments := ParseArguments( [ "-m", "foo", "--path=/test/", "--option", "-m=bar", "3", "one", "two", "three" ]
                             , [ [ name := "option", type := "switch" ]
                               , [ name := "switch", type := "switch", defaultvalue := TRUE ]
                               , [ name := "check",  type := "switch" ]
                               , [ name := "m",      type := "stringlist" ]
                               , [ name := "path",   type := "stringopt" ]
                               , [ name := "count",  type := "param" ]
                               , [ name := "other",  type := "paramlist" ]
                               ]);

  TestEq(TRUE, RecordExists(arguments));
  TestEq(TRUE, CellExists(arguments, "option"));
  TestEq(TRUE, CellExists(arguments, "switch"));
  TestEq(TRUE, CellExists(arguments, "check"));
  TestEq(TRUE, CellExists(arguments, "m"));
  TestEq(TRUE, CellExists(arguments, "path"));
  TestEq(TRUE, CellExists(arguments, "count"));
  TestEq(TRUE, CellExists(arguments, "other"));

  TestEq(TRUE, arguments.option);
  TestEq(TRUE, arguments."switch");
  TestEq(FALSE, arguments.check);

  TestEq(2, Length(arguments.m));
  TestEq("foo", arguments.m[0]);
  TestEq("bar", arguments.m[1]);

  TestEq("/test/", arguments.path);
  TestEq("3", arguments.count);

  TestEq(3, Length(arguments.other));
  TestEq("one", arguments.other[0]);
  TestEq("two", arguments.other[1]);
  TestEq("three", arguments.other[2]);

  // Test empty argument list
  arguments := ParseArguments(DEFAULT STRING ARRAY, [ [ name := "option", type := "switch" ]
                                                    , [ name := "switch", type := "switch", defaultvalue := TRUE ]
                                                    , [ name := "check",  type := "switch" ]
                                                    , [ name := "m",      type := "stringlist" ]
                                                    , [ name := "path",   type := "stringopt" ]
                                                    , [ name := "count",  type := "param" ]
                                                    , [ name := "other",  type := "paramlist" ]
                                                    ]);

  TestEq(TRUE, RecordExists(arguments));
  TestEq(TRUE, CellExists(arguments, "option"));
  TestEq(TRUE, CellExists(arguments, "switch"));
  TestEq(TRUE, CellExists(arguments, "check"));
  TestEq(TRUE, CellExists(arguments, "m"));
  TestEq(TRUE, CellExists(arguments, "path"));
  TestEq(TRUE, CellExists(arguments, "count"));
  TestEq(TRUE, CellExists(arguments, "other"));

  TestEq(FALSE, arguments.option);
  TestEq(TRUE, arguments."switch");
  TestEq(FALSE, arguments.check);
  TestEq(0, Length(arguments.m));
  TestEq("", arguments.path);
  TestEq("", arguments.count);
  TestEq(0, Length(arguments.other));

  arguments := ParseArguments(DEFAULT STRING ARRAY, [ [ name := "option", type := "switch", defaultvalue := "yes" ] ]);
  TestEq(TRUE, RecordExists(arguments));
  TestEq(TRUE, CellExists(arguments, "option"));
  TestEq(FALSE, arguments.option);

  arguments := ParseArguments([ "--switch" ], [ [ name := "option", type := "switch", defaultvalue := "yes" ] ]);
  TestEq(FALSE, RecordExists(arguments));

  arguments := ParseArguments(DEFAULT STRING ARRAY, [ [ name := "count", type := "param", required := TRUE ] ]);
  TestEq(FALSE, RecordExists(arguments));

  // Test illegal parameters
  result := TestCompileAndRun('<?wh LOADLIB "wh::os.whlib"; ParseArguments(DEFAULT STRING ARRAY, [ [ name := "option", type := "check" ] ]); ?>');
  MustContainError(44, result.errors, 182, "Unknown argument type 'CHECK'");
  result := TestCompileAndRun('<?wh LOADLIB "wh::os.whlib"; ParseArguments(DEFAULT STRING ARRAY, [ [ type := "switch" ] ]); ?>');
  MustContainError(45, result.errors, 182, "Column 'NAME' does not exist in variable record");
  result := TestCompileAndRun('<?wh LOADLIB "wh::os.whlib"; ParseArguments(DEFAULT STRING ARRAY, [ [ name := TRUE, type := "switch" ] ]); ?>');
  MustContainError(46, result.errors, 182, "Column 'NAME' not of type STRING");
  result := TestCompileAndRun('<?wh LOADLIB "wh::os.whlib"; ParseArguments(DEFAULT STRING ARRAY, [ [ name := "option" ] ]); ?>');
  MustContainError(47, result.errors, 182, "Column 'TYPE' does not exist in variable record");
  result := TestCompileAndRun('<?wh LOADLIB "wh::os.whlib"; ParseArguments(DEFAULT STRING ARRAY, [ [ name := "option", type := TRUE ] ]); ?>');
  MustContainError(48, result.errors, 182, "Column 'TYPE' not of type STRING");
  result := TestCompileAndRun('<?wh LOADLIB "wh::os.whlib"; ParseArguments(DEFAULT STRING ARRAY, [ [ name := "other", type := "paramlist" ], [ name := "count", type := "param" ] ]); ?>');
  MustContainError(49, result.errors, 182, "'PARAMLIST' not the last variable record type");

  // Test ParseArguments not ignoring empty arguments (like '"" "b"')
  TestEQ(
      [ a :=  ""
      , b :=  "b"
      ], ParseArguments([ "", "b" ],
                        [ [ name := "a", type := "param" ]
                        ,  [ name := "b", type := "param" ]
                        ]));

  TestEQ(TRUE, RecordExists(ParseArguments(STRING[], RECORD[])));
  TestEQ(FALSE, RecordExists(ParseArguments([ "extra" ], RECORD[])));

  CloseTest("TestArguments: ParseArgumentsTest");
}

ParseArgumentsTest();
