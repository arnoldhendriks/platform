<?wh
/// @short Extensive test of HareScript constants implementation

LOADLIB "wh::float.whlib";
LOADLIB "wh::internal/hsselftests.whlib";

CONSTANT INTEGER globaltestval := 5;

INTEGER FUNCTION DefFunc(INTEGER i DEFAULTSTO globaltestval + INTEGER(pi) + 1)
{
  RETURN i;
}

CONSTANT INTEGER testdefaultval;

MACRO ConstantsTest()
{
  TestEQ(TRUE, testdefaultval = 0);

  TestAnnotatedCompile(`
CONSTANT VARIANT val;
//               ^ E:63 VARIANT
`);
}

MACRO SwitchTest()
{
  OpenTest("TestSyntax: EmbeddingTest");

  CONSTANT INTEGER testval := 2;

  SWITCH (11)
  {
    CASE testval + INTEGER(pi) + 6 { }
    DEFAULT
    {
      ABORT("CASE constant expression calculated wrong");
    }
  }

  TestEQ(9, DefFunc());
  TestEQ(9, (PTR DefFunc)());


  CONSTANT RECORD test := [ a := 1, b := [ 3 ] ];
  CONSTANT INTEGER v2 := 2;
  FOR (INTEGER i := 1; i <= 3; i := i + 1)
  {
    INTEGER c;
    SWITCH (i) { CASE test.a { c := 1; } CASE v2 { c := 2; } CASE INTEGER(test.b[0]) { c := 3; }}
    TestEQ(i, c);
  }

  TestAnnotatedCompile(`
CONSTANT RECORD test := [ a := 1 ];
SWITCH (1)
{
  CASE test.a.a { PRINT("fail"); }
//         ^ E:62 INTEGER RECORD
}
`);

  TestAnnotatedCompile(`
CONSTANT RECORD test := [ a := 1 ];
CONSTANT INTEGER v2 := 2;
SWITCH (1)
{
  CASE test.b { PRINT("fail"); }
//         ^ E:86 B A
}
`);
}

MACRO CompileTimeErrorTest()
{
  TestAnnotatedCompile(`
CONSTANT RECORD ARRAY test := [ [ a := 1 ] ];
PRINT(test[0].meh);
//           ^ E:92 MEH
PRINT(test[0].b);
//           ^ E:86 B A
`);

  TestAnnotatedCompile(`
CONSTANT RECORD ARRAY test := [ [ a := 1 ] ];
PRINT(test[-1].a);
//         ^ E:46 -1
PRINT(test[1].a);
//         ^ E:46 1
`);

  TestAnnotatedCompile(`
CONSTANT RECORD ARRAY test := [ [ a := [ b := [ c := 3, d := [ 1 ] ] ] ] ];
PRINT(test[0].a.b.c[3]);
//               ^ E:130
PRINT(test[0].a.b.d[1]);
//                  ^ E:46 1
`);
}


ConstantsTest();
SwitchTest();
CompileTimeErrorTest();
