﻿<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::promise.whlib";
LOADLIB "wh::internal/testfuncs.whlib";

INTEGER FUNCTION ReturnXplusY(INTEGER x, INTEGER y) { RETURN x + y; }
INTEGER FUNCTION ReturnXplusEY(INTEGER x, OBJECT y) { RETURN x + ToInteger(y->what, -1); }
MACRO DoNothin() { }
VARIANT FUNCTION ReturnId(VARIANT e) { RETURN e; }
VARIANT FUNCTION ThrowE(OBJECT e) { THROW e; }

OBJECTTYPE atype < >;


MACRO TestCreateResolvedPromise()
{
  // Resolving
  TestEQ(66, WaitForPromise(CreateResolvedPromise(66)));
  OBJECT promise := CreateResolvedPromise(123);
  TestEQ(promise, CreateResolvedPromise(promise));
}

MACRO TestCreateRejectedPromise()
{
  TestThrowsLike("throw", PTR WaitForPromise(CreateRejectedPromise(NEW Exception("throw"))));
}

MACRO TestThen()
{
  OBJECT x := NEW atype;

  OBJECT c := CreateResolvedPromise(1);

  // Different parameter formats
  TestEQ(3, WaitForPromise(c->Then(PTR ReturnXplusY(2, #1)))); // function with parameter
  TestEQ(5, WaitForPromise(c->Then(PTR ReturnXplusY(2, 3)))); // function without parameter
  TestEQ(DEFAULT RECORD, WaitForPromise(c->Then(PTR DoNothin))); // macro

  TestEQ(1, WaitForPromise(c->Then(DEFAULT FUNCTION PTR, DEFAULT FUNCTION PTR))); // no functions

  // Returning promises
  TestEQ(5, WaitForPromise(c->Then(PTR CreateResolvedPromise(5))));

  // Other objects
  TestEQ(x, WaitForPromise(c->Then(PTR ReturnId(x))));

  OBJECT e := CreateRejectedPromise(NEW Exception("1"));
  e->OnError(PTR DoNothin); // Prevent 'unhandled async exception)

  // Different parameter formats
  TestEQ(3, WaitForPromise(e->Then(DEFAULT FUNCTION PTR, PTR ReturnXplusEY(2, #1)))); // function with parameter
  TestEQ(5, WaitForPromise(e->Then(DEFAULT FUNCTION PTR, PTR ReturnXplusY(2, 3)))); // function without parameter
  TestEQ(DEFAULT RECORD, WaitForPromise(e->Then(DEFAULT FUNCTION PTR, PTR DoNothin))); // macro

  // Returning promises
  TestEQ(5, WaitForPromise(e->Then(DEFAULT FUNCTION PTR, PTR CreateResolvedPromise(5))));

  // Other objects
  TestEQ(x, WaitForPromise(e->Then(DEFAULT FUNCTION PTR, PTR ReturnId(x))));

  // Throwing stuff
  TestThrowsLike("thrown", PTR WaitForPromise(c->Then(PTR ThrowE(NEW Exception("thrown")))));
  TestThrowsLike("thrown", PTR WaitForPromise(e->Then(DEFAULT FUNCTION PTR, PTR ThrowE(NEW Exception("thrown")))));
}

MACRO TestCancel()
{
  RECORD d;

  // Cancel normally does nothing
  d := CreateDeferredPromise();
  d.promise->Cancel();
  d.resolve(1);
  TestEQ(1, WaitForPromise(d.promise));

  // With autocancel on, promise is rejected immediately
  d := CreateDeferredPromise();
  d.promise->rejectoncancel := TRUE;
  d.promise->Cancel();
  d.resolve(1);
  TestThrowsLike("*cancelled*", PTR WaitForPromise(d.promise));

  // Cancel status is remembered and applied immediately when autocancel is set to true
  d := CreateDeferredPromise();
  d.promise->Cancel();
  d.promise->rejectoncancel := TRUE;
  d.resolve(1);
  TestThrowsLike("*cancelled*", PTR WaitForPromise(d.promise));
}

MACRO Dummy(VARIANT inval)
{
}

MACRO TestUncaught()
{
  OBJECT prom := CreateRejectedPromise(NEW Exception("dead"));
  TestThrowsLike("dead", PTR WaitForPromise(prom));

  TestEq(TRUE, WaitForPromise(CreateResolvedPromise(TRUE))); //this one works

  prom := CreateRejectedPromise(NEW Exception("dead"))->Then(PTR Dummy);
  TestThrowsLike("dead", PTR WaitForPromise(prom));

  TestEq(TRUE, WaitForPromise(CreateResolvedPromise(TRUE))); //this now fails
}

MACRO TestPromiseAll()
{
  OBJECT a := CreateResolvedPromise(1), b := CreateResolvedPromise(2);
  TestEQ(VARIANT ARRAY([ 1, 2 ]), WaitForPromise(CreatePromiseAll([ a, b ])));
  TestThrowsLike("thrown", PTR WaitForPromise(CreatePromiseAll([ a, b, CreateRejectedPromise(NEW Exception("thrown")) ])));
}

MACRO TestPromiseRace()
{
  OBJECT a := CreateDeferredPromise().promise, b := CreateResolvedPromise("1");
  TestEQ(1, WaitForPromise(CreatePromiseRace([ CreateResolvedPromise(1) ])));
  TestEQ(1, WaitForPromise(CreatePromiseRace([ OBJECT(a), CreateResolvedPromise(1) ])));
  TestEQ([ source := b, value := "1" ], WaitForPromise(CreatePromiseRace([ OBJECT(a), b ], [ wrap := TRUE ])));
  TestThrowsLike("thrown", PTR WaitForPromise(CreatePromiseRace([ a, CreateRejectedPromise(NEW Exception("thrown")) ])));
}

MACRO TestPromiseAllSettled()
{
  OBJECT a := CreateResolvedPromise(1), b := CreateResolvedPromise("2"), c := NEW Exception("test"), d := CreateRejectedPromise(c);

  TestEQ([ [ status := "fulfilled", value := 1 ], [ status := "fulfilled", value := "2" ] ], WaitForPromise(CreatePromiseAllSettled([ a, b ])));
  TestEQ([ [ status := "fulfilled", value := 1 ], [ status := "rejected", reason := c ] ], WaitForPromise(CreatePromiseAllSettled([ a, d ])));
  TestEQ(RECORD[], WaitForPromise(CreatePromiseAllSettled(OBJECT[])));
}

ASYNC MACRO TestCancellation()
{
  OBJECT ts := NEW CancellationTokenSource;
  OBJECT t := ts->canceltoken;
  TestEQ(FALSE, t->IsCancelled());
  ts->Cancel();
  TestEQ(TRUE, t->IsCancelled());
  TestThrowsLike("*cancelled*", PTR t->Check());

  // test callback, registration after cancellation
  RECORD defer := CreateDeferredPromise();
  t->AddCallback(defer.reject);
  TestThrowsLike("*cancelled*", PTR WaitForPromise(defer.promise));

  // test callback, registration before cancellation
  ts := NEW CancellationTokenSource;
  t := ts->canceltoken;
  defer := CreateDeferredPromise();
  t->AddCallback(defer.reject);
  ts->Cancel();
  TestThrowsLike("*cancelled*", PTR WaitForPromise(defer.promise));

  // linktoken before cancellation, one callback
  ts := NEW CancellationTokenSource;
  t := ts->canceltoken;
  OBJECT bts := NEW CancellationTokenSource;
  ts->LinkToken(bts->canceltoken);
  TestEQ(FALSE, t->IsCancelled());
  defer := CreateDeferredPromise();
  t->AddCallback(defer.reject);
  bts->Cancel();
  TestEQ(TRUE, bts->canceltoken->IsCancelled());
  TestEQ(TRUE, t->IsCancelled());
  TestThrowsLike("*cancelled*", PTR WaitForPromise(defer.promise));

  // linktoken before cancellation, no final callback
  ts := NEW CancellationTokenSource;
  t := ts->canceltoken;
  bts := NEW CancellationTokenSource;
  ts->LinkToken(bts->canceltoken);
  TestEQ(FALSE, t->IsCancelled());
  bts->Cancel();
  TestEQ(TRUE, bts->canceltoken->IsCancelled());
  TestEQ(TRUE, t->IsCancelled());

  // linktoken after cancellation, no final callback
  ts := NEW CancellationTokenSource;
  t := ts->canceltoken;
  bts := NEW CancellationTokenSource;
  bts->Cancel();
  ts->LinkToken(bts->canceltoken);
  TestEQ(TRUE, bts->canceltoken->IsCancelled());
  TestEQ(TRUE, t->IsCancelled());

  // multilayer linktoken, no callbacks
  ts := NEW CancellationTokenSource;
  t := ts->canceltoken;
  bts := NEW CancellationTokenSource;
  OBJECT bbts := NEW CancellationTokenSource;
  bts->LinkToken(bbts->canceltoken);
  ts->LinkToken(bts->canceltoken);

  TestEQ(FALSE, t->IsCancelled());
  bbts->Cancel();
  TestEQ(TRUE, bbts->canceltoken->IsCancelled());
  TestEQ(TRUE, t->IsCancelled());

  // multilayer linktoken, no callbacks
  ts := NEW CancellationTokenSource;
  t := ts->canceltoken;
  bts := NEW CancellationTokenSource;
  bbts := NEW CancellationTokenSource;
  bts->LinkToken(bbts->canceltoken);
  ts->LinkToken(bts->canceltoken);

  TestEQ(FALSE, t->IsCancelled());
  bbts->Cancel();
  TestEQ(TRUE, bbts->canceltoken->IsCancelled());
  TestEQ(TRUE, t->IsCancelled());

  // multilayer linktoken, no callbacks
  ts := NEW CancellationTokenSource;
  t := ts->canceltoken;
  bts := NEW CancellationTokenSource;
  bbts := NEW CancellationTokenSource;
  bts->LinkToken(bbts->canceltoken);
  ts->LinkToken(bts->canceltoken);

  defer := CreateDeferredPromise();
  t->AddCallback(defer.reject);
  bbts->Cancel();
  TestThrowsLike("*cancelled*", PTR WaitForPromise(defer.promise));
}

MACRO TestPromiseTime()
{
  DATETIME now := GetCurrentDatetime();
  TestEq(100, WaitForPromise(CreateSleepPromise(100)));
  TestEq(TRUE, GetdatetimeDifference(now, GetCurrentDatetime()).msecs >= 100);

  now := GetCurrentDatetime();
  DATETIME deadline := AddTimeToDate(100, GetCurrentDatetime());
  WaitForPRomise(CreateDeadlinePromise(deadline));
  TestEQ(TRUE, GetdatetimeDifference(now, GetCurrentDatetime()).msecs >= 100);
}

TestCreateResolvedPromise();
TestCreateRejectedPromise();
TestThen();
TestCancel();
// TestUncaught();
TestPromiseAll();
TestPromiseRace();
TestPromiseAllSettled();
TestCancellation();
TestPromiseTime();
