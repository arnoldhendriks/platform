<?wh

LOADLIB "mod::system/lib/testframework.whlib";

LOADLIB "mod::webhare_testsuite/lib/system/tests.whlib";

LOADLIB "mod::publisher/lib/database.whlib";
LOADLIB "mod::publisher/lib/testframework.whlib";
LOADLIB "mod::publisher/lib/forms/api.whlib";

LOADLIB "mod::tollium/lib/testframework.whlib";

LOADLIB "mod::wrd/lib/testfw/payments.whlib";
LOADLIB "mod::webhare_testsuite/lib/internal/testsite.whlib";
LOADLIB "mod::webhare_testsuite/lib/internal/wrdtesthelpers.whlib";

OBJECT pm2;

PUBLIC MACRO BuildForm()
{
  OBJECT paymentapi := GetWRDTestPaymentAPI();
  BuildWebtoolForm([ emailfieldname := "email", addpaymentmethod := TRUE, addcustompaymenthandler := TRUE]);
  BuildWebtoolForm([ emailfieldname := "email", filename := "custompaymentfield", addpaymentmethod := TRUE, addpaymenthandler := TRUE, addtslinecomp := TRUE]);
  BuildWebtoolForm([ emailfieldname := "email", filename := "apipayment", addpaymentmethod := TRUE, addpaymenthandler := TRUE ]);

  BuildCustomWebtoolForm2([ emailfieldname := "email", addpaymentmethod := TRUE, addpaymentmethodnoise := TRUE]);
  BuildCustomWebtoolForm2([ emailfieldname := "email", addpaymentmethod := TRUE, addpaymentmethodnoise := TRUE, filename := "paymenthandlerform"]);

  pm2 := testfw->wrdschema->^payprov->GetEntity(testfw->wrdschema->^payprov->Search("wrd_title", "TestMethod"));
}

PUBLIC MACRO FillInForm()
{
  //Ensure the form is up
  OBJECT formfile := OpenTestsuiteSite()->OpenByPath("webtools/form");
  IF(testfw->debug)
    Print('Form URL: ' || formfile->url||'\n');

  RECORD rendertree := OpenWebtoolForm(formfile->id)->GetRenderTree();
  RECORD pmfield := SELECT * FROM rendertree.pages[0].fields WHERE name = "pm";
  TestEqMembers(
     [ type := "composed"
     , lines := [[ type := 'radio'
                 , options := [[ htmltitle := 'TestNoIssuer (test mode)'
                               ]
                              ,[ htmltitle := 'TestWithIssuer (test mode)'
                               , subfields := [[ type := "select"
                                               , placeholder := "Select bank"
                                               , placeholderisset := TRUE
                                               , options := [[ enabled := TRUE
                                                             , htmltitle := 'BoerenLeenBank'
                                                             ]
                                                            ,[ enabled := TRUE
                                                             , htmltitle := 'DurePakkenBank'
                                                             ]
                                                            ]
                                              ]]
                             ]]
                ]]
    ], pmfield, '*');

  TestEq(TRUE, testfw->browser->GotoWebPage(formfile->url));

  OBJECT ARRAY radios := testfw->browser->document->GetElements('input[name="pm.paymentmethod"]');
  TestEq(2, Length(radios), "should be two options now, noissuer and withissuer");

  OBJECT ARRAY verify_inside_required := testfw->browser->document->GetElements('.wh-form__fieldgroup.wh-form__fieldgroup--required input[name="pm.paymentmethod"]');
  TestEq(2, Length(verify_inside_required), "paymentmethod should be inside a wh-form__fieldgroup--required");

  OBJECT ARRAY pulldowns := testfw->browser->document->GetElements('select');
  TestEq(2, Length(pulldowns), "Can't find issuer <select>");

  OBJECT ARRAY issuer_opts := pulldowns[1]->GetElements("option");
  TestEq(3, Length(issuer_opts), "Expecting three options");

  OBJECT webtoolform := OpenFormFileDefinition(formfile);
  OBJECT webtoolresults := OpenFormFileResults(formfile);
  RECORD ARRAY fields := webtoolform->ListFields();

  TestEq(TRUE, RecordExists(SELECT FROM fields WHERE title = ":Pay using"), "payment field not in formfields!");
  TestEq(FALSE, RecordExists(SELECT FROM fields WHERE ToUppercase(name)="PM.PAYMENTMETHOD"), "PM.PAYMENTMETHOD should not leak into fields"); //it never did here, though

  OBJECT tester := OpenFormsapiFormTester(testfw->browser);
  RECORD submitrec := [ firstname := "Pietje"
                      , "pm.paymentmethod" := STRING[radios[0]->GetAttribute("value")]
                      , email := "testfw+formtest@beta.webhare.net"
                      ];

  RECORD res := tester->SubmitForm(submitrec);
  TestEq(TRUE, res.success);

  //Inspect the raw data to ensure a GUID was stored
  RECORD rawdata := SELECT * FROM publisher.formresults WHERE formresults.guid = res.result.resultsguid;
  RECORD rawresults := DecodeHSON(rawdata.results);
  RECORD rawpm := GetCEll(rawresults, SELECT AS STRING guid FROM fields WHERE ToUppercase(name)="PM");
  TestEq([ paymentprovider := pm2->guid, paymentoptiontag := "NOISSUER", issuer := "" ], rawpm);

  RECORD submitted := webtoolresults->GetSingleResult(res.result.resultsguid);
  TestEq([ paymentprovider := pm2->id, paymentoptiontag := "NOISSUER", issuer := "" ], submitted.response.pm);
  TestEq(FALSE, CellExists(submitted.response,"PM.PAYMENTMETHOD")); //should not leak

  submitrec := [ ...submitrec, "pm.paymentmethod" := STRING[radios[1]->GetAttribute("value")]];
  res := tester->SubmitForm(submitrec);
  TestEq(FALSE, res.success);

  //Select an issuer. The only pulldown should be the issuer
  submitrec := CellInsert(submitrec, pulldowns[1]->GetAttribute("name"), issuer_opts[1]->GetAttribute("value"));
  res := tester->SubmitForm(submitrec);
  //dumpvalue(res);
  TestEq(TRUE, res.success);

  submitted := webtoolresults->GetSingleResult(res.result.resultsguid);
  TestEq([ paymentprovider := pm2->id, paymentoptiontag := "WITHISSUER", issuer := "BLB" ], submitted.response.pm);
  //dumpvalue(submitted);

  RECORD witty := webtoolresults->GetWittyResultdata(submitted);
  TestEq(TRUE, CellExists(witty.field,'PM')); //we want this to exist
  TestEq(FALSE, CellExists(witty.field,'PM.PAYMENTMETHOD')); //but we DON'T want to see this subcell

  TestEq(TRUE, RecordExists(res.result.submitinstruction), "Expect a submission instruction to execute the payment");
  TestEq(TRUE, testfw->browser->ExecuteSubmitInstruction(res.result.submitinstruction));

  PushWRDTestPaymentButton("approved");
  RECORD payresult := DecodeJSONBlob(testfw->browser->content);
  TestEq("approved", payresult.payinfo.status);

  //Test an export
  OBJECT exporter := webtoolresults->CreateExporter();

  RECORD ARRAY exportrows := exporter->ExportAllResults();
  STRING col_payusing  := SELECT AS STRING name FROM exporter->columns WHERE title="Pay using";
  TestEq("TestWithIssuer (BLB)", GetCell(exportrows[1], col_payusing));
}

MACRO CustomForm()
{
  OBJECT formfile := OpenTestsuiteSite()->OpenByPath("webtools/customform2");
  IF(testfw->debug)
    Print('Form URL: ' || formfile->url||'\n');
  TestEq(TRUE, testfw->browser->GotoWebPage(formfile->url));

  OBJECT ARRAY labels := testfw->browser->document->GetElements('label.wh-form__optionlabel[for*="pm.paymentmethod"]');
  TestEq(2, Length(labels), "should be two labels");
  TestEq("Test<b>WITH</b>Issuer (test mode)", labels[1]->innerhtml);

  OBJECT ARRAY labels_pm_noschema := testfw->browser->document->GetElements('label.wh-form__optionlabel[for*="pm_noschema.paymentmethod"]');
  TestEq(0,Length(labels_pm_noschema));

  formfile := OpenTestsuiteSite()->OpenByPath("webtools/paymenthandlerform");
  TestEq(TRUE, testfw->browser->GotoWebPage(formfile->url));

  labels_pm_noschema := testfw->browser->document->GetElements('label.wh-form__optionlabel[for*="pm_noschema.paymentmethod"]');
  TestEq(0,Length(labels_pm_noschema));

  TestEq(TRUE, testfw->browser->GotoWebPage(formfile->url || "?in_pm_noschema=NOISSUER"));
  labels_pm_noschema := testfw->browser->document->GetElements('label.wh-form__optionlabel[for*="pm_noschema.paymentmethod"]');
  TestEq(1, Length(labels_pm_noschema), "should be one labels");
  TestEq("TestNoIssuer (test mode)", labels_pm_noschema[0]->innerhtml);

  OBJECT tester := OpenFormsapiFormTester(testfw->browser);
  OBJECT ARRAY radios_pm := testfw->browser->document->GetElements('input[name="pm.paymentmethod"]');
  RECORD submitrec := [ firstname := "Pietje"
                      , "pm.paymentmethod" := STRING[radios_pm[0]->GetAttribute("value")]
                      , email := "testfw+formtest@beta.webhare.net"
                      ];

  //Submit to trigger a bug when fields are added to non-issuer payment methods
  RECORD res := tester->SubmitForm(submitrec);
  TestEq(TRUE, res.success);
}

ASYNC MACRO TestEditPaymentHandler()
{
  //Ensure the form is up
  OBJECT formfile := OpenTestsuiteSite()->OpenByPath("webtools/custompaymentfield");
  IF(testfw->debug)
    Print('Form URL: ' || formfile->url||'\n');
  TestEq(TRUE, testfw->browser->GotoWebPage(formfile->url));

  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("publisher:edit", [ params := STRING[ formfile->whfspath ] ]));
  TT("doceditor->questions")->selection := SELECT * FROM TT("doceditor->questions")->rows WHERE title = "Request payment";
  AWAIT ExpectScreenChange(+1, PTR TTClick("{item}:Edit"));
  OBJECT paymentextension := TT("tabs")->ListExtensions()[0].extension;
  TestEq(3, Length(paymentextension->^field->options));
  TestEq("TSLine - Sum", paymentextension->^field->options[1].title);
  TestEq("TSLine - Difference", paymentextension->^field->selection.title);

  AWAIT ExpectScreenChange(-1, PTR TTClick(":Ok"));
  AWAIT ExpectAndAnswerMessageBox("YES", PTR TTClick("Publish"), [ awaitcall := TRUE ]); //confirm save and publish
  AWAIT ExpectScreenChange(-1, PTR TTEscape);
}

MACRO TestThroughFormValueAPI()
{
  OBJECT formfile := OpenTestsuiteSite()->OpenByPath("webtools/apipayment");
  OBJECT form := OpenWebtoolForm(formfile->id);
  OBJECT webtoolresults := OpenFormFileResults(formfile);
  OBJECT exporter := webtoolresults->CreateExporter();

  RECORD rendertree := form->GetRenderTree();
  RECORD pmfield := (SELECT * FROM rendertree.pages[0].fields WHERE name = "pm").lines[0];
  TestEq("TestWithIssuer (test mode)", pmfield.options[1].htmltitle);

  form->SetFormValues([[ name := "firstname", value := "FNTest" ]
                      ,[ name := "email", value := "testfw+apipayment@beta.webhare.net" ]
                      ,[ name := pmfield.name, value := pmfield.options[1].value ]
                      ,[ name := pmfield.options[1].subfields[0].name, value := "DPB" ]
                      ]);

  RECORD savevalue := form->^pm->value;
  form->^pm->value := DEFAULT RECORD; //Crashed at one point. shouldnt!
  form->^pm->value := savevalue;

  TestEqMembers([ issuer := "DPB"
                , paymentoptiontag := "WITHISSUER"
                ], form->^pm->value, '*');

  RECORD res := form->FormExecuteSubmit();
  TestEq(TRUE, testfw->browser->ExecuteSubmitInstruction(res.result.submitinstruction));

  TestEq(0, Length(exporter->ExportAllResults()));

  PushWRDTestPaymentButton("approved");

  RECORD ARRAY exportrows := exporter->ExportAllResults();
  STRING col_payusing  := SELECT AS STRING name FROM exporter->columns WHERE title="Pay using";
  TestEq("TestWithIssuer (DPB)", GetCell(exportrows[END-1], col_payusing));
}

RunTestframework([ PTR CreateWRDTestSchema([withpayment := STRING["noissuer","withissuer"] ])
                 , PTR BuildForm
                 , PTR FillInForm
                 , PTR CustomForm
                 , PTR TestEditPaymentHandler
                 , PTR TestThroughFOrmValueAPI
                 ], [ testusers := [[ login := "sysop", grantrights := ["system:sysop"] ]
                                   ]
                    ]);
