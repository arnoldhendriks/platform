<?wh

LOADLIB "wh::crypto.whlib";
LOADLIB "wh::datetime.whlib";
LOADLIB "wh::files.whlib";
LOADLIB "wh::internet/urls.whlib";
LOADLIB "wh::internet/webbrowser.whlib";

LOADLIB "mod::consilio/lib/api.whlib";
LOADLIB "mod::publisher/lib/testframework.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/logging.whlib";
LOADLIB "mod::tollium/lib/testframework.whlib";
LOADLIB "mod::wrd/lib/auth.whlib";
LOADLIB "mod::wrd/lib/dialogs.whlib";

LOADLIB "mod::system/lib/testframework.whlib";
LOADLIB "mod::webhare_testsuite/lib/system/tests.whlib";

LOADLIB "mod::webhare_testsuite/lib/system/webserver.whlib";
LOADLIB "mod::webhare_testsuite/lib/internal/testsite.whlib";


/*******************************************************************************
 *
 * Test WRD authentication using the webdesign plugin API
 */
//ADDME: Using the plugin seems to always make the login persistent

STRING baseurl;
RECORD thirdpartytest;
STRING thirdpartybaseurl;
STRING thirdpartyurl;
INTEGER entityid;

RECORD FUNCTION DoReq(RECORD vars, BOOLEAN thirdparty DEFAULTSTO FALSE)
{
  STRING url := UpdateURLVariables(thirdparty ? thirdpartyurl : baseurl, vars);
  IF(NOT testfw->browser->GotoWebPage(url) OR testfw->browser->contenttype!="application/x-hson")
  {
    SendBlobTo(0,testfw->browser->content);
    Abort("get failure on " || testfw->browser->url);
  }
  RETURN DecodeHSON(BlobToString(testfw->browser->content,-1));
}

MACRO InvalidateWebhareLoginSession()
{
  //Invalidate the sessionid stored in the webhare login cookie. invalidating sessions may be tricky, but just randomizing the sessionid will have the same effect
  TestEq(TRUE, testfw->browser->GetCookie("webharelogin-wrd-testschema") != "");
  testfw->browser->SetSessionCookie("webharelogin-wrd-testschema", GenerateUFS128BitId() || " " || Tokenize(testfw->browser->GetCookie("webharelogin-wrd-testschema"),' ')[1]);
}

MACRO InvalidateExternalLoginSessions()
{
  //Find the _e cookie
  STRING e_cookiename := SELECT AS STRING name FROM testfw->browser->GetAllCookies() WHERE name LIKE 'webharelogin*_e';
  //change the sessionid, simulating a lost _e session
  TestEq(TRUE, e_cookiename != "", "Cannot locate the webhare external access cookie");
  testfw->browser->SetSessionCookie(e_cookiename, "V_DFv-saZMPa5TyZ7Wi2Gg " || Tokenize(testfw->browser->GetCookie(e_cookiename),' ')[1]);
}


MACRO PrepareServer()
{
  RECORD testport := testfw->GetLocalhostWebinterface();
  thirdpartytest  := testfw->CreateWebserverPort([ outputserver := TRUE, virtualhosts := ["localhost.beta.webhare.net"]]); //note: localhost.webhare.com maps to 127.0.0.1

  testfw->BeginWork();

  baseurl := testport.baseurl || "wrd/authtest/webhare_testsuite.shtml?";
  thirdpartybaseurl := thirdpartytest.webservers[0].url || "wrd/authtest/";
  thirdpartyurl := thirdpartybaseurl || "webhare_testsuite.shtml?";

  INSERT INTO system.access(webserver, path, matchtype, hostingsrc, hostingpath)
         VALUES(testport.webserverid, "/wrd/authtest/webhare_testsuite.shtml", 0/*exact*/, 3/*exact file*/, GetModuleInstallationRoot("webhare_testsuite") || "web/tests/wrd/authdebug.shtml");

  INSERT INTO system.access(webserver,path,matchtype,hostingsrc,hostingpath)
              VALUES(thirdpartytest.webservers[0].id,"/",1,6,"webhare_testsuite:securityheaders");
  INSERT INTO system.access(webserver,path,matchtype,hostingsrc,hostingpath)
              VALUES(thirdpartytest.webservers[0].id,"/",1,6,"webhare_testsuite:thirdpartytest");

  INTEGER extusersid := MakeAutonumber(system.access,"ID");
  INSERT INTO system.access(id,webserver,path,matchtype,authtype,authrequirement,authlist, hostingsrc, hostingpath)
              VALUES(extusersid,thirdpartytest.webservers[0].id,"/wrd/authtest/extusers/",1,1,TRUE,TRUE,3,GetModuleInstallationRoot("webhare_testsuite") || "web/tests/");
  INSERT INTO system.access_externalusers(accessid,username,userpassword)
              VALUES(extusersid,"testwrdauthplugin-username","testwrdauthplugin-password");

  testfw->CommitWork();

  testfw->RefreshWebserverConfig();
}

MACRO CreateMyTestSchema()
{
  RECORD result;

  testfw->BeginWork();

  OBJECT schemaobj := testfw->GetWRDSchema();
  TestEq(TRUE, ObjectExists(schemaobj));
  schemaobj->^wrd_authdomain->DeleteSelf();

  OBJECT persontype := testfw->GetWRDSchema()->^wrd_person;
  persontype->CreateAttribute("LASTLOGIN",  "DATETIME");
//  persontype->CreateAttribute("EMAIL", "Email address", "", "WRD_CONTACT_EMAIL", "");
//  persontype->CreateAttribute("PASSWORD", "Password", "", "PASSWORD", "");

//  OBJECT domain := CreateWRDAuthDomain(persontype, "TESTDOMAIN", "WRD_CONTACT_EMAIL", "PASSWORD");

  OBJECT entity := persontype->CreateEntity(
      [ wrd_contact_email := "testaccount@test.invalid"
      , whuser_password := CreateAuthenticationSettingsFromPasswordHash("PLAIN:secret")
      , whuser_unit := testfw->testunit
      ]);

  entityid := entity->id;

  testfw->CommitWork();

  //ADDME some sort of automatic backing off/delays/even captcha? to prevent excessive login attempts ?
  DATETIME starttest := GetCurrentDatetime();
  result := DoReq( [ action := "login", username := "testaccount@test.invali", pwd := "wrongpassword", browsertriplet := "mac-chrome-01" ]);
  TestEq(FALSE, result.success);
  TestEq("UNKNOWNLOGIN", result.code);
  TestAuditEvent(
      [ entity := 0
      , type := "wrd:login:failed"
      , ip := "127.0.0.1"
      , login := "testaccount@test.invali"
      , impersonated := FALSE
      , data := EncodeHSON([failurecode := "UNKNOWNLOGIN"])
      , browsertriplet := "mac-chrome-01"
      , byentity := 0
      , bylogin := "testaccount@test.invali"
      ]);

  OBJECT stream := OpenWebHareLogStream("audit", starttest, GetCurrentDatetime());
  RECORD rec := stream->ReadRecord();
  TestEq("wrd:testschema", DecodeHSON(rec.parts[2]).wrdschema);

  result := DoReq( [ action := "login", username := "testaccount@test.invalid", pwd := "wrongpassword", browsertriplet := "mac-chrome-01"]);
  TestEq(FALSE, result.success);
  TestEq("INCORRECTPASSWORD", result.code);
  TestAuditEvent(
      [ entity := entityid
      , type := "wrd:login:failed"
      , ip := "127.0.0.1"
      , login := "testaccount@test.invalid"
      , impersonated := FALSE
      , data := EncodeHSON([failurecode := "INCORRECTPASSWORD"])
      , browsertriplet := "mac-chrome-01"
      , byentity := entityid
      , bylogin := "testaccount@test.invalid"
      ]);

  result := DoReq( [ action := "checkstatus" ]);
  TestEq(FALSE, result.isloggedin);
  TestEq("", result.loginname);
  TestEq(0, result.entityid);

  TestEq("PLAIN:secret", entity->GetField("whuser_password").passwords[0].passwordhash);
  TestEq(FALSE, IsWebHarePasswordHashStillSecure(entity->GetField("whuser_password").passwords[0].passwordhash));

  result := DoReq( [ action := "login", username := "testaccount@test.invalid", pwd := "secret", browsertriplet := "mac-chrome-01"]);
  TestEq(TRUE, IsWebHarePasswordHashStillSecure(entity->GetField("whuser_password").passwords[0].passwordhash));
  TestEqLike("WHBF:*", entity->GetField("whuser_password").passwords[0].passwordhash);
  TestEq(1,length(entity->GetField("whuser_password").passwords));
  TestEq(TRUE, result.success);
  TestAuditEvent(
      [ entity := entityid
      , type := "wrd:login:ok"
      , ip := "127.0.0.1"
      , login := "testaccount@test.invalid"
      , impersonated := FALSE
      , data := ""
      , browsertriplet := "mac-chrome-01"
      , byentity := entityid
      , bylogin := "testaccount@test.invalid"
      ]);

  result := DoReq( [ action := "checkstatus" ]);
  TestEq(TRUE, result.isloggedin);
  TestEq("testaccount@test.invalid", result.loginname);
  TestEq(entity->id, result.entityid);

  //regression test. issue #5989, early use of GetLoggedinEntity() or GetLoginName() causes a re-login
  result := DoReq( [ action := "logout-checkstatus" ]);
  TestEq(TRUE, result.oldstatus.isloggedin);
  TestEq(FALSE, result.isloggedin);
  TestEq("", result.loginname);
  TestEq(0, result.entityid);
  TestAuditEvent(
      [ entity := entityid
      , type := "wrd:logout"
      , ip := "127.0.0.1"
      , login := "testaccount@test.invalid"
      , impersonated := FALSE
      , data := ""
      , browsertriplet := ""
      , byentity := entityid
      , bylogin := "testaccount@test.invalid"
      ]);

  DATETIME logginginat := GetCurrentDatetime();

  result := DoReq( [ action := "login", username := "testaccount@test.invalid", pwd := "wrongpassword", browsertriplet := "mac-chrome-01"]);
  TestEq(FALSE, result.success);
  TestEq("INCORRECTPASSWORD", result.code);
  TestAuditEvent(
      [ entity := entityid
      , type := "wrd:login:failed"
      , ip := "127.0.0.1"
      , login := "testaccount@test.invalid"
      , impersonated := FALSE
      , data := EncodeHSON([failurecode := "INCORRECTPASSWORD"])
      , browsertriplet := "mac-chrome-01"
      , byentity := entityid
      , bylogin := "testaccount@test.invalid"
      ]);

  TestEq(TRUE, entity->GetField("LASTLOGIN") < logginginat, "Failed login shouldn't have updated LASTLOGIN");

  //neither should a 'master' (impersonated) login
  result := DoReq( [ action := "loginid", userid := ToString(entityid), impersonation := "1", browsertriplet := "mac-chrome-01" ]);
  TestEq(TRUE, result.success);
  TestAuditEvent(
      [ entity := entityid
      , type := "wrd:loginbyid:ok"
      , ip := "127.0.0.1"
      , login := "testaccount@test.invalid"
      , impersonated := TRUE
      , data := ""
      , browsertriplet := "mac-chrome-01"
      , byentity := entityid
      , bylogin := "testaccount@test.invalid"
      , impersonator_entity := entityid
      , impersonator_login := "testaccount@test.invalid"
      ]);

  TestEq(TRUE, entity->GetField("LASTLOGIN") < logginginat, "Neither should an impersonated login update LASTLOGIN");

  result := DoReq( [ action := "logout-checkstatus" ]);
  TestEq(TRUE, result.oldstatus.isloggedin);
  TestEq(FALSE, result.isloggedin);
  TestAuditEvent(
      [ entity := entityid
      , type := "wrd:logout"
      , ip := "127.0.0.1"
      , login := "testaccount@test.invalid"
      , impersonated := FALSE
      , data := ""
      , browsertriplet := ""
      , byentity := entityid
      , bylogin := "testaccount@test.invalid"
      ]);

  logginginat := GetCurrentDatetime();
  result := DoReq( [ action := "login", username := "testaccount@test.invalid", pwd := "secret", browsertriplet := "mac-chrome-01"]);
  TestEq(TRUE, result.success);

  TestEq(TRUE, entity->GetField("LASTLOGIN") >= logginginat, "succesful login, so update LASTLOGIN time");

  TestAuditEvent(
      [ entity := entityid
      , type := "wrd:login:ok"
      , ip := "127.0.0.1"
      , login := "testaccount@test.invalid"
      , impersonated := FALSE
      , data := ""
      , browsertriplet := "mac-chrome-01"
      , byentity := entityid
      , bylogin := "testaccount@test.invalid"
      ]);
}

MACRO TestWRDAuthPlugin()
{
  OBJECT plugin := GetWRDAuthPlugin(OpenTestsuiteSite()->OpenByPath("portal1-domainless")->url);
  TestEq(TRUE, IsAbsoluteURL(plugin->__GetLoginPageURL(),FALSE), "__GetLoginPageURL (or whatever function generates login urls in the future) should return absolute URL, it returned a relative path when used with currentsite:: earlier");
}

MACRO TestModuleDefIPRules()
{
  //testfw->browser->debug := TRUE;
  TestEq(FALSE, testfw->browser->GotoWebPage( OpenTestsuiteSite()->webroot || "portal1-domainless/requiresysop/"));
  TestEq(403, testfw->browser->GetHTTPStatusCode(),"should receive 403 as our current user is unprivileged");

  TestEq(FALSE, testfw->browser->GotoWebPage( OpenTestsuiteSite()->webroot || "portal1-domainless/requiresysop-or-consilio/"));
  TestEq(403, testfw->browser->GetHTTPStatusCode());

  OBJECT trustedbrowser := NEW WebBrowser;
  //trustedbrowser->debug := TRUE;
  trustedbrowser->autofollow := FALSE;
  trustedbrowser->SetupWebHareTrustedPort(GetWebhareConfiguration().trustedhost, GetWebhareConfiguration().trustedport, fetcher_trusted_ip);

  TestEq(TRUE, trustedbrowser->GotoWebPage( OpenTestsuiteSite()->webroot || "portal1-domainless/requiresysop/"));
  TestEq(303, trustedbrowser->GetHTTPStatusCode(), "should be redirect to login page as we have no session");

  TestEq(TRUE, trustedbrowser->GotoWebPage( OpenTestsuiteSite()->webroot || "portal1-domainless/requiresysop/test.txt?__whpub_clock_=1"));
  TestEq(303, trustedbrowser->GetHTTPStatusCode(), "should be redirect to login page as we have no session");

  TestEq(TRUE, trustedbrowser->GotoWebPage( OpenTestsuiteSite()->webroot || "portal1-domainless/requiresysop/test.txt?__whpub_clock_=1", [ headers := [[ field := "Accept-Encoding", value := "gzip" ]]]));
  TestEq(303, trustedbrowser->GetHTTPStatusCode(), "should be redirect to login page as we have no session");

  TestEq("", trustedbrowser->GetResponseHeader("Content-Encoding"), "Should not GZIP wrdauth pages!"); //OR ensure they're properly compressed. but we were sending uncompresed pages with a compressed header!

  TestEq(TRUE, trustedbrowser->GotoWebPage( OpenTestsuiteSite()->webroot || "portal1-domainless/requiresysop-or-consilio/"));
  TestEq(200, trustedbrowser->GetHTTPStatusCode());
}

MACRO TestPersistentLogin()
{
  OBJECT auditlogtype := testfw->GetWRDSchema()->GetType("WRD_AUDITLOG");

  RECORD result;
  result := DoReq( [ action := "login", username := "testaccount@test.invalid", pwd := "secret", browsertriplet := "mac-chrome-01"]);
  TestAuditEvent(
      [ entity := entityid
      , type := "wrd:login:ok"
      , ip := "127.0.0.1"
      , login := "testaccount@test.invalid"
      , impersonated := FALSE
      , data := ""
      , browsertriplet := "mac-chrome-01"
      , byentity := entityid
      , bylogin := "testaccount@test.invalid"
      ]);

  RECORD logincookie := SELECT * FROM testfw->browser->GetAllCookies() WHERE ToUppercase(name)="WEBHARELOGIN-WRD-TESTSCHEMA";
  TEstEq(TRUE, RecordExists(logincookie));
  TestEQ(DEFAULT DATETIME, logincookie.expires);

  STRING cursessionid := Tokenize(DecodeURL(logincookie.data),' ')[0];
  RECORD sessdata := ManipWebserverSession("get", cursessionid, "wrd:authsession");
  TestEq("testaccount@test.invalid", sessdata.login);
  ManipWebserverSession("close", cursessionid, "wrd:authsession");

  result := DoReq( [ action := "checkstatus" ]);
  TestEq(TRUE, result.isloggedin, "Session should be repaired");
  logincookie := SELECT * FROM testfw->browser->GetAllCookies() WHERE ToUppercase(name)="WEBHARELOGIN-WRD-TESTSCHEMA";
  STRING newsessionid := Tokenize(DecodeURL(logincookie.data),' ')[0];
  TestEq(cursessionid, newsessionid, "Session ID should be retained");

  TestAuditEvent(
      [ entity := entityid
      , type := "wrd:logincookie"
      , login := "testaccount@test.invalid"
      , impersonated := FALSE
      , data := ""
      , browsertriplet := ""
      , byentity := entityid
      , bylogin := "testaccount@test.invalid"
      ]);

  TestEq(TRUE, RecordExists(logincookie));
  TestEQ(DEFAULT DATETIME, logincookie.expires);

  //Invalidate the sessionid stored in the webhare login cookie
  InvalidateWebhareLoginSession();

  //Should autorestore because of the data in the cookie
  result := DoReq( [ action := "checkstatus" ]);
  TestEq(TRUE, result.isloggedin);
  TestAuditEvent(
      [ entity := entityid
      , type := "wrd:logincookie"
      , login := "testaccount@test.invalid"
      , impersonated := FALSE
      , data := ""
      , browsertriplet := ""
      , byentity := entityid
      , bylogin := "testaccount@test.invalid"
      ]);

  //Completely clear the session
  testfw->browser->DeleteCookie("webharelogin-wrd-testschema");

  //Should not recover
  result := DoReq( [ action := "checkstatus" ]);
  TestEq(FALSE, result.isloggedin);

//testfw->browser->debug:=TRUE;
  result := DoReq( [ action := "login", username := "testaccount@test.invalid", pwd := "secret", persistent := "1", browsertriplet := "mac-chrome-01" ]);
  InvalidateWebhareLoginSession();
  TestAuditEvent(
      [ entity := entityid
      , type := "wrd:login:ok"
      , ip := "127.0.0.1"
      , login := "testaccount@test.invalid"
      , impersonated := FALSE
      , data := ""
      , browsertriplet := "mac-chrome-01"
      , byentity := entityid
      , bylogin := "testaccount@test.invalid"
      ]);

  //Should still recover from this (from the persistent cookie). Browser triplet is not available
  result := DoReq( [ action := "checkstatus" ]);
  TestEq(TRUE, result.isloggedin);
  TestAuditEvent(
      [ entity := entityid
      , type := "wrd:logincookie"
      , login := "testaccount@test.invalid"
      , impersonated := FALSE
      , data := ""
      , browsertriplet := ""
      , byentity := entityid
      , bylogin := "testaccount@test.invalid"
      ]);

  InvalidateWebhareLoginSession();

  //Should now also recover from this.
  result := DoReq( [ action := "checkstatus" ]);
  TestEq(TRUE, result.isloggedin);
  TestAuditEvent(
      [ entity := entityid
      , type := "wrd:logincookie"
      , login := "testaccount@test.invalid"
      , impersonated := FALSE
      , data := ""
      , browsertriplet := ""
      , byentity := entityid
      , bylogin := "testaccount@test.invalid"
      ]);

  InvalidateWebhareLoginSession();

  //Should now also recover from this if checkstatus runs in work
  result := DoReq( [ action := "checkstatus-work" ]);
  TestEq(TRUE, result.isloggedin);
  TestAuditEvent(
      [ entity := entityid
      , type := "wrd:logincookie"
      , login := "testaccount@test.invalid"
      , impersonated := FALSE
      , data := ""
      , browsertriplet := ""
      , byentity := entityid
      , bylogin := "testaccount@test.invalid"
      ]);

  //Close the user
  testfw->BeginWork();
  testfw->GetWRDSchema()->^wrd_person->GetEntity(entityid)->CloseEntity();
  testfw->CommitWork();

  result := DoReq( [ action := "checkstatus-work" ]);
  TestEq(FALSE, result.isloggedin);

  //Clear the session again
  testfw->browser->DeleteCookie("webharelogin-wrd-testschema");

  //Close the user
  testfw->BeginWork();
  testfw->GetWRDSchema()->^wrd_person->GetEntity(entityid)->CloseEntity();
  testfw->CommitWork();

  result := DoReq( [ action := "checkstatus-work" ]);
  TestEq(FALSE, result.isloggedin);
}

VARIANT ARRAY FUNCTION MakeVA(VARIANT ARRAY args) __ATTRIBUTES__(VARARG)
{
  RETURN args;
}

MACRO TestRequireExternalLoggedinUser()
{
  OBJECT auditlogtype := testfw->GetWRDSchema()->GetType("WRD_AUDITLOG");
  testfw->browser->DeleteCookies("webharelogin-*");

  STRING loginrequiringurl := thirdpartyurl || "action=requirelogin"; //uses WRD auth plugin dynamic auth

  TestEq(TRUE, testfw->browser->GotoWebPage(loginrequiringurl));
  TesteqLike("*wh-shell*", testfw->browser->document->documentelement->GetAttribute("class")); //ensure we're talking to some sort of tollium

  //We should be seeing the tollium page above. Do a WRD login
//  testfw->browser->debug:=TRUE;
  STRING logincontrol := GetVariableFromURL(testfw->browser->url, "wrdauth_logincontrol");
  STRING loginpage := testfw->browser->url;

  RECORD rec := testfw->browser->InvokeJSONRPC("/wh_services/wrd/auth", "Login",
                  VARIANT[testfw->browser->url, testfw->GetUserLogin("sysop"), testfw->GetUserPassword("sysop"), FALSE, CELL[ browsertriplet := "mac-chrome-01", logincontrol ]]);

  TestEq(TRUE, rec.success);

  TestAuditEvent(
      [ entity := testfw->GetUserWRDId("sysop")
      , type := "wrd:login:ok"
      , login := "sysop@beta.webhare.net"
      , impersonated := FALSE
      , data := ""
      , browsertriplet := "mac-chrome-01"
      , byentity := testfw->GetUserWRDId("sysop")
      , bylogin := "sysop@beta.webhare.net"
      ]);

  TestEq('redirect', rec.result.submitinstruction.type);

  TestEq(TRUE, testfw->browser->ExecuteSubmitInstruction(rec.result.submitinstruction), "Execute the submit instruction");
  TestEq(200, testfw->browser->GetHTTPStatusCode()); // another redirect to clean the location
  TestEq(loginrequiringurl, testfw->browser->url, "Should be redirected back to the original location, but now we should see things here");
  TestEq("sysop@beta.webhare.net", DecodeHSONBLob(testfw->browser->content).login);

  InvalidateExternalLoginSessions();

  TestEq(TRUE, testfw->browser->GotoWebPage(loginrequiringurl)); //should still work
  TestEq(200, testfw->browser->GetHTTPStatusCode());

  TestEq(TRUE, testfw->browser->GotoWebPage( OpenTestsuiteSite()->webroot || "portal1-domainless/requiresysop"));
  TestEq(200, testfw->browser->GetHTTPStatusCode());

  TestEq(TRUE, testfw->browser->GotoWebPage( OpenTestsuiteSite()->webroot || "portal1-domainless/requiresysop-or-consilio"));
  TestEq(200, testfw->browser->GetHTTPStatusCode());

  // Log the user out on the main page (TODO can we invalidate the external thirdparty logins to?)
  TestAuditEvent(DEFAULT RECORD); //check log is still empty
  testEq(TRUE, testfw->browser->GotoWebPage(GetTestsuiteWRDauthPlugin()->GetLogoutLink()));
  TestAuditEvent(
      [ entity := testfw->GetUserWRDId("sysop")
      , type := "wrd:logout"
      , login := "sysop@beta.webhare.net"
      , impersonated := FALSE
      , data := ""
      , browsertriplet := ""
      , byentity := testfw->GetUserWRDId("sysop")
      , bylogin := "sysop@beta.webhare.net"
      ]);

  // FIXME: when the disk audit log is converted to JSON, test if the WRD schema is set in this log entry
}

MACRO TestCrossDomainLogin()
{
  OBJECT auditlogtype := testfw->GetWRDSchema()->GetType("WRD_AUDITLOG");
  testfw->browser->DeleteCookies("webharelogin-*");

  STRING loginrequiringurl := thirdpartyurl || "action=requirelogin"; //uses WRD auth plugin dynamic auth
  STRING accessrulelogin := thirdpartybaseurl || "accessruleaccounts/";

  TestEq(TRUE, testfw->browser->GotoWebPage(accessrulelogin));
  TesteqLike("*wh-shell*", testfw->browser->document->documentelement->GetAttribute("class")); //ensure we're talking to some sort of tollium

  STRING logincontrol := GetVariableFromURL(testfw->browser->url, "wrdauth_logincontrol");

  //We should be seeing the tollium page above. Do a WRD login
//  testfw->browser->debug:=TRUE;
  STRING loginpage := testfw->browser->url;
  RECORD rec := testfw->browser->InvokeJSONRPC("/wh_services/wrd/auth", "Login",
                  VARIANT[testfw->browser->url, testfw->GetUserLogin("sysop"), testfw->GetUserPassword("sysop"), FALSE, CELL[ browsertriplet := "mac-chrome-01", logincontrol ]]);

  TestEq(TRUE, rec.success);
  TestEq("redirect", rec.result.submitinstruction.type);
  TestEq(TRUE, GetVariableFromURL(rec.result.submitinstruction.url, "wrdauth_proof") != "");
  TestEq(accessrulelogin, DeleteVariableFromUrl(rec.result.submitinstruction.url, "wrdauth_proof"));

  // Test if GetAfterLoginSubmitInstruction returns a similar submitinstruction
  RECORD rec2 := testfw->browser->InvokeJSONRPC("/wh_services/wrd/auth", "GetAfterLoginSubmitInstruction",
      VARIANT[ loginpage, logincontrol ]);

  TestEQ("redirect", rec2.result.submitinstruction.type);
  TestEq(TRUE, GetVariableFromURL(rec2.result.submitinstruction.url, "wrdauth_proof") != "");
  TestEQ(accessrulelogin, DeleteVariableFromUrl(rec2.result.submitinstruction.url, "wrdauth_proof"));

  //TestEq('reload', rec.result.submitinstruction.type);
  TestAuditEvent(
      [ entity := testfw->GetUserWRDId("sysop")
      , type := "wrd:login:ok"
      , login := "sysop@beta.webhare.net"
      , impersonated := FALSE
      , data := ""
      , browsertriplet := "mac-chrome-01"
      , byentity := testfw->GetUserWRDId("sysop")
      , bylogin := "sysop@beta.webhare.net"
      ]);

  TestEq(TRUE, testfw->browser->ExecuteSubmitInstruction(rec.result.submitinstruction), "Execute the submitinstruction");
  TestEq(accessrulelogin, testfw->browser->url, "Should be redirected back to the original location, but now we should see things");
  TestEqLike("*WebHare testsuite /web/tests/ root*", BlobToString(testfw->browser->content));

  testfw->browser->DeleteCookies("webharelogin-*"); //force logout otherwise can't access the reset

  // Go to accessrule which allows both webhare and ext users. It should send us to the webhare login page)
  STRING extuserslogin := thirdpartybaseurl || "extusers/";
  testfw->browser->autofollow := TRUE;
  TestEq(TRUE, testfw->browser->GotoWebPage(extuserslogin));
  TestEqLike(GetPrimaryWebhareInterfaceURL() || "*wrdauth_logincontrol=*", testfw->browser->url, "verify we were redirected to WebHare");
  logincontrol := GetVariableFromURL(testfw->browser->url, "wrdauth_logincontrol");
  rec := testfw->browser->InvokeJSONRPC("/wh_services/wrd/auth", "Login",
                  VARIANT[testfw->browser->url, "testwrdauthplugin-username", "testwrdauthplugin-password", FALSE, CELL[ browsertriplet := "mac-chrome-01", logincontrol ]]);

  TestEq(TRUE, rec.success);
  TestEq("redirect", rec.result.submitinstruction.type);
  TestEq(TRUE, GetVariableFromURL(rec.result.submitinstruction.url, "wrdauth_proof") != "");
  TestEq(extuserslogin, DeleteVariableFromUrl(rec.result.submitinstruction.url, "wrdauth_proof"));

  TestEq(TRUE, testfw->browser->ExecuteSubmitInstruction(rec.result.submitinstruction), "Execute the submitinstruction");
  TestEq(extuserslogin, testfw->browser->url, "Should be redirected back to the original location, but now we should see things");
  TestEqLike("*WebHare testsuite /web/tests/ root*", BlobToString(testfw->browser->content));
}

RECORD onshowdata;
MACRO RecordOnShowData(RECORD data)
{
  onshowdata := data;
}

OBJECT ASYNC FUNCTION TestAuditDialog()
{
  AWAIT ExpectScreenChange(+1, PTR RunWRDAuditLogDialog(GetTestController(),
    [ wrdschema := testfw->GetWRDSchema()
    , accountid := testfw->GetUserWRDId("sysop")
    , onshow := PTR RecordOnShowData
    ]));
  TestEq(3, Length(topscreen->events->rows));

  topscreen->events->selection := (SELECT * FROM topscreen->events->rows ORDER BY wrd_creationdate)[0];
  TestEq(FALSE, RecordExists(onshowdata));
  topscreen->events->openaction->TolliumClick();
  TestEq(TRUE, RecordExists(onshowdata));

  //dumpvalue(onshowdata);
  TestEq(testfw->GetUserWRDId("sysop"), onshowdata.entity);
  TestEq("wrd:login:ok", onshowdata.type);

  topscreen->events->selection := (SELECT * FROM topscreen->events->rows ORDER BY wrd_creationdate)[1];
  topscreen->events->openaction->TolliumClick();
  TestEq(TRUE, RecordExists(onshowdata));

  //dumpvalue(onshowdata);
  TestEq(testfw->GetUserWRDId("sysop"), onshowdata.entity);
  TestEq("wrd:logout", onshowdata.type);

  AWAIT ExpectScreenChange(-1, PTR topscreen->TolliumExecuteCancel);
  RETURN TRUE;
}

MACRO TestLoginDenied()
{
  testfw->BeginWork();

  OBJECT schemaobj:=  testfw->GetWRDSchema();
  TestEq(TRUE, ObjectExists(schemaobj));

  OBJECT persontype := schemaobj->GetType("WRD_PERSON");
  OBJECT auditlogtype := schemaobj->GetType("WRD_AUDITLOG");

  OBJECT entity := persontype->CreateEntity(
      [ wrd_contact_email := "logindenied@test.invalid"
      , whuser_password := CreateAuthenticationSettingsFromPasswordHash(testfw->hashedpasswords.secret)
      , whuser_unit := testfw->testunit
      ]);

  entityid := entity->id;

  testfw->CommitWork();

  // Make sure the current user is logged out
  RECORD result := DoReq( [ action := "checkstatus" ]);
  TestEq(FALSE, result.isloggedin);
  TestEq("", result.loginname);
  TestEq(0, result.entityid);

  // Login is denied when logging in by username and password
  result := DoReq( [ action := "login", username := "logindenied@test.invalid", pwd := "secret", browsertriplet := "mac-chrome-01"]);
  TestEq(FALSE, result.success);
  TestEq("NOACCESS", result.code);
  TestEq(TRUE, CellExists(result, "USERINFO"));
  TestEq(FALSE, CellExists(result, "_INTERNAL"));
  TestAuditEvent(
      [ entity := entityid
      , type := "wrd:login:failed"
      , ip := "127.0.0.1"
      , login := "logindenied@test.invalid"
      , impersonated := FALSE
      , data :=
          EncodeHSON([ userinfo := [ wrd_contact_email := "logindenied@test.invalid" ]
                     , failurecode := "NOACCESS"
                     ])
      , browsertriplet := "mac-chrome-01"
      , byentity := entityid
      , bylogin := "logindenied@test.invalid"
      ]);

  // User still not logged in
  result := DoReq( [ action := "checkstatus" ]);
  TestEq(FALSE, result.isloggedin);
  TestEq("", result.loginname);
  TestEq(0, result.entityid);

  // Login is denied when logging in by id
  result := DoReq( [ action := "loginid", userid := ToString(entityid), browsertriplet := "mac-chrome-01" ]);
  TestEq(FALSE, result.success);
  TestEq("NOACCESS", result.code);
  TestEq(TRUE, CellExists(result, "USERINFO"));
  TestEq(FALSE, CellExists(result, "_INTERNAL"));
  TestAuditEvent(
      [ entity := entityid
      , type := "wrd:loginbyid:failed"
      , ip := "127.0.0.1"
      , login := "logindenied@test.invalid"
      , impersonated := FALSE
      , data :=
          EncodeHSON([ userinfo := [ wrd_contact_email := "logindenied@test.invalid" ]
                     , failurecode := "NOACCESS"
                     ])
      , browsertriplet := "mac-chrome-01"
      , byentity := entityid
      , bylogin := "logindenied@test.invalid"
      ]);

  // User still not logged in
  result := DoReq( [ action := "checkstatus" ]);
  TestEq(FALSE, result.isloggedin);
  TestEq("", result.loginname);
  TestEq(0, result.entityid);

  // Login is not denied when impersonating login
  result := DoReq( [ action := "loginid", userid := ToString(entityid), impersonation := "1", browsertriplet := "mac-chrome-01" ]);
  TestEq(TRUE, result.success);
  TestEq(FALSE, RecordExists(result.submitinstruction));
  TestAuditEvent(
      [ entity := entityid
      , type := "wrd:loginbyid:ok"
      , login := "logindenied@test.invalid"
      , impersonated := TRUE
      , data := ""
      , browsertriplet := "mac-chrome-01"
      , byentity := entityid
      , bylogin := "logindenied@test.invalid"
      , impersonator_entity := entityid
      , impersonator_login := "logindenied@test.invalid"
      ]);
}

MACRO TestWRDAuthRouter()
{
  DoSetupWRDAuth();

  testfw->BeginWork();
  testfw->GetWRDSchema()->^wrd_person->CreateEntity([ wrd_contact_email := "testwrdauthrouter@beta.webhare.net"
                                                    , password := "WHBF:$2y$10$V0b0ckLtUivNWjT/chX1OOljYgew24zn8/ynfbUNkgZO9p7eQc2dO"  //secret
                                                    , whuser_unit := testfw->testunit
                                                    ]);
  testfw->CommitWork();

  //just login first
  STRING testbaseurl := OpenTestsuiteSite()->webroot || "testpages/";
  RECORD loginres := testfw->browser->CallJSONRPC("/wh_services/wrd/auth", "Login", testbaseurl || "wrdauthtest-domainless-router"
                                                 , "testwrdauthrouter@beta.webhare.net", "secret", FALSE);
  TestEq(TRUE, loginres.success);

  OBJECT wrdplugin := GetWRDAuthPlugin(testbaseurl || "wrdauthtest-domainless-router/");
  RECORD wittydata := wrdplugin->GetWRDAuthRouterWittyData(testbaseurl || "wrdauthtest-domainless-router/");
  TestEqLike("http*", wittydata.forgotpasswordlink);
  TestEq("", wittydata.emailchangelink);

  wrdplugin := GetWRDAuthPlugin(testbaseurl || "wrdauthtest-domainless-router-extended/");
  wittydata := wrdplugin->GetWRDAuthRouterWittyData(testbaseurl || "wrdauthtest-domainless-router-extended/");
  TestEqLike("http*", wittydata.forgotpasswordlink);
  TestEqLike("http*", wittydata.emailchangelink);

  testfw->browser->autofollow := FALSE;
  TestEq(TRUE, testfw->browser->GotoWebPage(wittydata.emailchangelink));
  TestEq(200, testfw->browser->GetHTTPStatusCode());

  TestEq(FALSE, testfw->browser->GotoWebPage(Substitute(wittydata.emailchangelink, "wrdauthtest-domainless-router-extended", "wrdauthtest-domainless-router")));
  TestEq(403, testfw->browser->GetHTTPStatusCode(), "should explicitly show emailchange as forbidden");

  testfw->browser->DeleteCookies("webharelogin-*"); //force logout otherwise can't access the reset

  //Let's get to the password reset. -broken truncates password lifetime to 0 minutes, giving us an invalid link to test
  TestEq(TRUE, testfw->browser->GotoWebPage(Substitute(wittydata.forgotpasswordlink, "wrdauthtest-domainless-router-extended", "wrdauthtest-domainless-router-broken")));

  //Submit it for our test user
  OBJECT tester := OpenFormsapiFormTester(testfw->browser);
  RECORD submitrec := CELL[ email := "testwrdauthrouter@beta.webhare.net" ];
  RECORD res := tester->SubmitForm(submitrec);
  TestEq(TRUE,res.success);

  RECORD ARRAY emails := testfw->ExtractAllMailFor("testwrdauthrouter@beta.webhare.net");
  TestEq(1,length(emails));
  TestEq(1,Length(emails[0].links));

  STRING fixedlink := Substitute(emails[0].links[0].href, "wrdauthtest-domainless-router-broken", "wrdauthtest-domainless-router-extended"); //otherwise it'll just break again
  TestEq(TRUE, testfw->browser->GotoWebPage(fixedlink));

  TestEq(TRUE, ObjectExists(testfw->browser->document->GetElement(".wh-wrdauth-linkexpired")));
  //TODO the page should offer a way to re-request the password

  //Let's get to the password set.
  INTEGEr user := testfw->wrdschema->^wrd_person->Search("WRD_CONTACT_EMAIL", "testwrdauthrouter@beta.webhare.net");
  RECORD passwordsetter := wrdplugin->CreatePasswordResetLink(wittydata.forgotpasswordlink, user, [ routerpage := "setpassword" ]);
  TestEq(TRUE, testfw->browser->GotoWebPage(passwordsetter.verifiedlink));
  TestEq(TRUE, ObjectExists(testfw->browser->document->GetElement(".wh-wrdauth-setpassword")));

  RECORD passwordsetter2 := wrdplugin->CreatePasswordResetLink(wittydata.forgotpasswordlink, user, [ routerpage := "setpassword" ]);
  TestEq(TRUE, testfw->browser->GotoWebPage(passwordsetter.verifiedlink));
  TestEq(TRUE, ObjectExists(testfw->browser->document->GetElement(".wh-wrdauth-setpassword")));

  // Change the password
  OBJECT formtester := OpenFormsapiFormTester(testfw->browser);
  formtester->SubmitForm(
      [ passwordnew := "newpassword"
      , passwordrepeat := "newpassword"
      ]);

  // Password link should be expired now
  testfw->browser->DeleteCookies("webharelogin-*"); //force logout otherwise can't access the reset
  TestEq(TRUE, testfw->browser->GotoWebPage(passwordsetter.verifiedlink));
  TestEq(TRUE, ObjectExists(testfw->browser->document->GetElement(".wh-wrdauth-linkexpired")));

  // 2nd password reset link should be expired too
  TestEq(TRUE, testfw->browser->GotoWebPage(passwordsetter2.verifiedlink));
  TestEq(TRUE, ObjectExists(testfw->browser->document->GetElement(".wh-wrdauth-linkexpired")));
}

Runtestframework( [ PTR PrepareServer
                  , PTR CreateMyTestSchema
                  , PTR TestWRDAuthPlugin
                  , PTR TestModuleDefIPRules
                  , PTR TestPersistentLogin
                  , PTR TestRequireExternalLoggedinUser
                  , PTR TestCrossDomainLogin
                  , PTR TestAuditDialog
                  , PTR TestLoginDenied
                  , PTR TestWRDAuthRouter
                  ], [ testusers := [[ login := "sysop", grantrights := ["system:sysop"] ]
                                    ]
                     ]);
