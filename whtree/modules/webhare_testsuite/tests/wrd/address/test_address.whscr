<?wh
LOADLIB "mod::publisher/lib/siteprofiles.whlib";
LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/testframework.whlib";
LOADLIB "mod::webhare_testsuite/lib/system/tests.whlib";
LOADLIB "mod::tollium/lib/testframework.whlib";
LOADLIB "mod::wrd/lib/address.whlib";
LOADLIB "mod::wrd/lib/worldinfo/countries.whlib";
LOADLIB "mod::wrd/lib/internal/addressverification/support.whlib";
LOADLIB "mod::wrd/lib/internal/addressfields.whlib";

RECORD FUNCTION ParseAddress(STRING rawdata)
{
  RETURN GenerateAddress(rawdata, "", 0, FALSE);
}

MACRO AddressConversion()
{
  //we'll never set locationdetail anymore but we'll keep it for backwards compatibility
  CONSTANT RECORD baseaddress := CELL[ country := "", zip := "", street := "", nr_detail := "", city := "", state := "", locationdetail := "" ];
  TestEq(CELL[ ...baseaddress
             , country := "NL"
             , street :="Loskade"
             , nr_detail := "4"
             , zip := "7202 CZ"
             , city := "Zutphen"
             ], ParseAddress('COUNTRY=NL\nSTREET=Loskade\nNR_DETAIL=4\nZIP=7202 CZ\nCITY=Zutphen\nCOUNTRY_FULL=Nederland\nLOCATIONDETAIL=\n'));

  TestEq(CELL[ ...baseaddress
             , country := "NL"
             , street :="Hengelosestraat"
             , nr_detail := "296"
             , zip := "7521 AM"
             , city := "Enschede"
             , state := "Overijssel" //not discarding this, Hengelo might be ambiguous without
             ], ParseAddress('STREET=Hengelosestraat\nNR_DETAIL=296\nZIP=7521 AM\nCITY=Enschede\nPROVINCE=Overijssel\nCOUNTRY=NL\n'));

  TestEq(CELL[ ...baseaddress
             , country := "ES"
             , street := "Villa Azul\nCarrer del torrent"
             , nr_detail := "0"
             , zip := "07817"
             , city := "Eivissa"
             ], ParseAddress('LOCATIONDETAIL=Villa Azul\nPROVINCE=\nCOUNTRY_FULL=Spanje\nCOUNTRY=ES\nCITY=Eivissa\nNR_DETAIL=0\nSTREET=Carrer del torrent\nZIP=07817\n'));

  TestEq(CELL[ ...baseaddress
             , country := "CN"
             , street := "Ms. WU\nScience"
             , nr_detail := "1"
             , zip := "1"
             , city := "China"
             ], ParseAddress('COUNTRY=cn\nLOCATIONDETAIL=Ms. WU\nBUILDING=\nNR_DETAIL=1\nSTREET=Science\nZIP=1\nCITY=China\nCOUNTRY_FULL=China'));

  TestEq(CELL[ ...baseaddress
             , country := "GB"
             , street := "Great Barr\nAmblecote Avenue"
             , nr_detail := "0"
             , zip := "B44 9AL"
             , city := "Birmingham"
             ], ParseAddress('COUNTRY=GB\nCITY=Birmingham\nCOUNTRY=GB\nLOCALAREA=Great Barr\nNR_DETAIL=0\nSTREET=Amblecote Avenue\nZIP=B44 9AL\n'));

  TestEq(CELL[ ...baseaddress
             , country := "JP"
             , street := "0-1\nMaki"
             , zip := "514-1123"
             , city := "Tsu-shi, Mie"
             ], ParseAddress('CITY=Tsu-shi\nCOUNTRY=JP\nDISTRICT=Mie\nLOCATIONDETAIL=0-1\nLOCATIONDETAIL2=Maki\nNR_DETAIL=\nSTREET=\nZIP=514-1123\n'));

  TestEq(CELL[ ...baseaddress
             , country := "FR"
             , street := "Rue du Grand Moulin"
             , zip := "50700"
             , city := "Vol"
             , nr_detail := "0"
             ], ParseAddress('COUNTRY=fr\nLOCATIONDETAIL=-\nNR_DETAIL=0\nSTREET=Rue du Grand Moulin\nZIP=50700\nCITY=Vol\nCEDEX=-\nCOUNTRY_FULL=Frankrijk\n'));

   STRING encoded := EncodeWRDAddress(CELL[ country := "Nl"
                                          , street :="Hengelosestraat"
                                          , nr_detail := "296"
                                          , zip := "7521 AM"
                                          , city := "Enschede"
                                          , state := ""
                                          ]);
  TestEq(CELL[ country := "NL"
             , street :="Hengelosestraat"
             , nr_detail := "296"
             , zip := "7521 AM"
             , city := "Enschede"
             ], DecodeJSON(encoded));

  TestEq(CELL[ ...baseaddress
             , country := "NL"
             , street :="Hengelosestraat"
             , nr_detail := "296"
             , zip := "7521 AM"
             , city := "Enschede"
             ], ParseAddress(encoded));

  Testeq(CELL[ country := "AU" ], DecodeJSON(EncodeWRDAddress(CELL[country:="au"])));
}

MACRO GenericCodes()
{
  TestEq([nr:="13", detail:="6"], SplitHousenumber("13 6"));
  TestEq([nr:="13", detail:=""], SplitHousenumber("13"));
  TestEq([nr:="13", detail:="-6"], SplitHousenumber("13-6"));

  TestEq("13-6", JoinHousenumber("13","-6"));
  TestEq("13 6", JoinHousenumber("13","6"));
  TestEq("13a", JoinHousenumber("13","a"));
  TestEq("13a", JoinHousenumber("13"," a"));
}

MACRO NLCodes()
{
  TestEq("7522 MB", FixupZipCodeNL("7522mb"));
  TestEq("7522 MB", FixupZipCodeNL("  7522    mb "));
  TestEq("7522 MB", FixupZipCodeNL("7522MB"));
  TestEq("7522 MB", FixupZipCodeNL("752 2MB"));
  TestEq("", FixupZipCodeNL("0999MB"));
  TestEq("", FixupZipCodeNL("752 2M"));
  TestEq("", FixupZipCodeNL("752 23 M"));

  TestEq("7522 MB", FixupZipCodeNL("N L-7522mb"));
  TestEq("7522 MB", FixupZipCodeNL("NL-  7522    mb "));
  TestEq("7522 MB", FixupZipCodeNL("NL-7522MB"));
  TestEq("7522 MB", FixupZipCodeNL("NL-752 2MB"));
  TestEq("", FixupZipCodeNL("NL-0999MB"));
  TestEq("", FixupZipCodeNL("NL-752 2M"));
  TestEq("", FixupZipCodeNL("NL-752 23 M"));

  TestEq("", FixupNRDetailNL("Eh"));
  TestEq("123", FixupNRDetailNL("123"));
  TestEq("t/o 123", FixupNRDetailNL("tegenover 123"));
  TestEq("t/o 123", FixupNRDetailNL("tegenover 123"));
  TestEq("t/o 123", FixupNRDetailNL("to 123"));
  TestEq("t/o 123", FixupNRDetailNL("t.o. 123"));
  TestEq("t/o 123", FixupNRDetailNL("t/o 123"));
  TestEq("t/o 123a", FixupNRDetailNL("t/o 123a"));
  TestEq("123a", FixupNRDetailNL("123a"));
  TestEq("123-6", FixupNRDetailNL("123-6"));
  TestEq("", FixupNRDetailNL("06-123"));

  TestEq(DEFAULT RECORD, FixupAddress(DEFAULT RECORD));
  TestEq([ street := "", nr_detail := "", city := "", country := "NL", zip := "7512 AB" ], FixupAddress([ country := "nl", zip := "7512ab" ]));
  TestEq([ street := "", nr_detail := "", city := "", country := "NL", zip := "7512 AB" ],  FixupAddress([ country := "NL", zip := "7512ab" ]));
  TestEq([ street := "", nr_detail := "", city := "", country := "NL", zip := "7512 AB" ],  FixupAddress([ country := "NL", zip := "NL-7512 AB" ]));
  TestEq([ street := "", nr_detail := "", city := "", country := "CY", zip := "7512ab" ],  FixupAddress([ country := "Cy", zip := "7512ab" ]));

  //Now through a schema
  TestEq([ street := "", nr_detail := "", city := "", country := "NL", zip := "7512 AB" ], FixupAddress([ country := "nl", zip := "7512ab" ]));
}

OBJECT ASYNC FUNCTION TolliumFieldsNew()
{
  GetTestController()->contexts->wrdschema := testfw->GetWRDSchema();

  OBJECT scr := GetTestController()->LoadScreen("webhare_testsuite:tests/anycomponent.anycomponent", [ component := "http://www.webhare.net/xmlns/wrd/components:addressfield" ]);
  AWAIT ExpectScreenChange(+1, PTR scr->RunModal());

  TestEq("", topscreen->comp->addressfield->value);
  TestEq("<title:addressfield>", topscreen->comp->block->title);

  AWAIT ExpectScreenChange(+1, PTR topscreen->comp->changeaction->TolliumClick());
  TT("country")->value := "NL";
  TestEq(5, Length(TT("countryfields")->currentfields));
  TestEq(["ZIP","NR_DETAIL","STREET","CITY"], SELECT AS STRING ARRAY tag FROM TT("countryfields")->currentfields WHERE component->visible);

  (SELECT AS OBJECT component FROM TT("countryfields")->currentfields WHERE tag="ZIP")->value := "7521am";
  (SELECT AS OBJECT component FROM TT("countryfields")->currentfields WHERE tag="NR_DETAIL")->value := "296";
  TTClick(":Look up");
  TestEq("Enschede", (SELECT AS OBJECT component FROM TT("countryfields")->currentfields WHERE tag="CITY")->value);
  TestEq("Hengelosestraat", (SELECT AS OBJECT component FROM TT("countryfields")->currentfields WHERE tag="STREET")->value);
  TestEq(TRUE, (SELECT AS OBJECT component FROM TT("countryfields")->currentfields WHERE tag="CITY")->enabled);

  TT("country")->value := "CF";
  TestEq(5, Length(SELECT FROM TT("countryfields")->currentfields WHERE component->visible)); //state will become visible
  TestEq("Enschede", (SELECT AS OBJECT component FROM TT("countryfields")->currentfields WHERE tag="CITY")->value);

  TT("country")->value := "NL"; //TODO restore nr_detail etc if we had them in the current session?
  (SELECT AS OBJECT component FROM TT("countryfields")->currentfields WHERE tag="STREET")->value := "Hengelosestraat";
  (SELECT AS OBJECT component FROM TT("countryfields")->currentfields WHERE tag="NR_DETAIL")->value := "296";
  (SELECT AS OBJECT component FROM TT("countryfields")->currentfields WHERE tag="ZIP")->value := "7521am";

  AWAIT ExpectScreenChange(-1, PTR topscreen->TolliumExecuteSubmit());

  TestEq("Hengelosestraat 296\n7521 AM  Enschede\nNetherlands", topscreen->comp->addressfield->value);

  topscreen->comp->wrdschema := testfw->GetWRDSchema(); //doesn't have effect yet. should eg. enable validation?
  topscreen->comp->required := TRUE;
  topscreen->validatevalue->TolliumClick();

  AWAIT ExpectScreenChange(-1, PTR topscreen->TolliumExecuteSubmit());

  scr := GetTestController()->LoadScreen("webhare_testsuite:tests/anycomponent.anycomponent",
    [ component := "http://www.webhare.net/xmlns/wrd/components:addressfield"
    , settings := [ checks := [""] ] //nl-zip-suggest was the default, remove it
    ]);

  AWAIT ExpectScreenChange(+1, PTR scr->RunModal());
  AWAIT ExpectScreenChange(+1, PTR topscreen->comp->changeaction->TolliumClick());
  TT("country")->value := "NL";
  TestEq(["STREET", "NR_DETAIL", "ZIP", "CITY" ], SELECT AS STRING ARRAY tag FROM TT("countryfields")->currentfields WHERE component->visible);
  AWAIT ExpectScreenChange(-1, PTR TTEscape);
  AWAIT ExpectScreenChange(-1, PTR TTEscape);

  scr := GetTestController()->LoadScreen("webhare_testsuite:tests/anycomponent.anycomponent",
    [ component := "http://www.webhare.net/xmlns/wrd/components:addressfield"
    , settings := [ checks := ["nl-zip-force"] ]
    ]);

  AWAIT ExpectScreenChange(+1, PTR scr->RunModal());
  AWAIT ExpectScreenChange(+1, PTR topscreen->comp->changeaction->TolliumClick());
  TT("country")->value := "NL";
  TestEq(["ZIP","NR_DETAIL","STREET","CITY"], SELECT AS STRING ARRAY tag FROM TT("countryfields")->currentfields WHERE component->visible);

  (SELECT AS OBJECT component FROM TT("countryfields")->currentfields WHERE tag="ZIP")->value := "7521am";
  (SELECT AS OBJECT component FROM TT("countryfields")->currentfields WHERE tag="NR_DETAIL")->value := "296";
  TTClick(":Look up");
  TestEq("Enschede", (SELECT AS OBJECT component FROM TT("countryfields")->currentfields WHERE tag="CITY")->value);
  TestEq("Hengelosestraat", (SELECT AS OBJECT component FROM TT("countryfields")->currentfields WHERE tag="STREET")->value);
  TestEq(FALSE, (SELECT AS OBJECT component FROM TT("countryfields")->currentfields WHERE tag="CITY")->enabled);

  AWAIT ExpectScreenChange(-1, PTR TTEscape);
  AWAIT ExpectScreenChange(-1, PTR TTEscape);

  RETURN TRUE;
}

MACRO TestAddressInfo()
{
  TestEq(
    [[ tag := "STREET", autocomplete := "street-address",  title := "Street", hide := FALSE, required := TRUE ]
    ,[ tag := "NR_DETAIL", autocomplete := "",  title := "Number", hide := FALSE, required := TRUE ]
    ,[ tag := "ZIP", autocomplete := "postal-code",  title := "Zip code", hide := FALSE, required := TRUE ]
    ,[ tag := "CITY", autocomplete := "address-level2",  title := "City", hide := FALSE, required := TRUE ]
    ,[ tag := "STATE", autocomplete := "address-level1",  title := "State", hide := TRUE, required := FALSE ]
    ], GetAddressFormat("nl", [ language := "en" ]).fields);

  TestEq(TRUE, GetAddressFormat("nl", [ language := "en" ]).onlyStreet);

  TestEqMembers( //test translation
    [[ tag := "STREET",  title := "Straat", hide := FALSE ]
    ,[ tag := "NR_DETAIL", title :="Huisnummer", hide := FALSE ]
    ,[ tag := "ZIP" ]
    ,[ tag := "CITY" ]
    ,[ tag := "STATE" ]
    ], GetAddressFormat("nl", [ language := "nl" ]).fields, "*");

  TestEqMembers(
    [ fields := [[ tag := "STREET",    hide := FALSE, required := TRUE ]
                ,[ tag := "NR_DETAIL", hide := TRUE, required := FALSE ]
                ,[ tag := "ZIP",       hide := FALSE, required := TRUE ]
                ,[ tag := "CITY",      hide := FALSE, required := TRUE ]
                ,[ tag := "STATE",     hide := FALSE, required := TRUE ]
                ]
    , onlystreet := FALSE
    ], GetAddressFormat("us"), "*");

  //Note that 'Afghanisatan' is currently a 'we honestly don't know at all' country and is used to test the defaults
  TestEqMembers(
    [ fields := [[ tag := "STREET",    hide := FALSE, required := TRUE ]
                ,[ tag := "NR_DETAIL", hide := TRUE, required := FALSE ]
                ,[ tag := "ZIP",       hide := FALSE, required := FALSE ]
                ,[ tag := "CITY",      hide := FALSE, required := TRUE ]
                ,[ tag := "STATE",     hide := FALSE, required := FALSE ]
                ]
    , onlystreet := FALSE
    ], GetAddressFormat("af"), "*");

  TestEq("Hengelosestraat 296\n7521 AM  Enschede\nNetherlands", FormatAddress([ country := "NL", street := "Hengelosestraat", nr_detail := "296", city := "Enschede", zip := "7521AM" ], [ language := "EN"]));
  TestEq("Hengelosestraat 296\n7521 AM  Enschede\nNederland", FormatAddress([ country := "NL", street := "Hengelosestraat", nr_detail := "296", city := "Enschede", zip := "7521AM" ], [ language := "NL"]));
  TestEq("Hengelosestraat 296\n7521 AM  Enschede", FormatAddress([ country := "NL", street := "Hengelosestraat", nr_detail := "296", city := "Enschede", zip := "7521AM" ], [ language := "NL", showcountry := FALSE]));

  TestEq("Unit A, 6870 Sellers Av\nBurnaby BC V5J 4R3\nCanada", FormatAddress([ country := "CA", street := "Sellers Av", nr_detail := "Unit A, 6870", city := "Burnaby", state := "BC", zip := "V5J 4R3" ], [ language := "en" ]));
  TestEq("Unit A, 6870 Sellers Av\nBurnaby BC V5J 4R3\nCanada", FormatAddress([ country := "CA", street := "Unit A, 6870 Sellers Av", nr_detail := "", city := "Burnaby", state := "BC", zip := "V5J 4R3" ], [ language := "en" ]));

  TestEq("1600 Pennsylvania Ave NW\nWashington, DC 20500\nUnited States", FormatAddress([ country := "US", street := "Pennsylvania Ave NW", nr_detail := "1600", city := "Washington", state := "DC", zip := "20500" ], [ language := "en" ]));
  TestEq("1600 Pennsylvania Ave NW\nWashington, DC 20500\nUnited States", FormatAddress([ country := "US", street := "1600 Pennsylvania Ave NW", city := "Washington", state := "DC", zip := "20500" ], [ language := "en" ]));
}

OBJECT ASYNC FUNCTION TolliumFieldsEdit()
{
  OBJECT scr := GetTestController()->LoadScreen("webhare_testsuite:tests/anycomponent.anycomponent",
    [ component := "http://www.webhare.net/xmlns/wrd/components:addressfield"
    , settings := [ value := [ country := "NL", street := "Hengelosestraat", nr_Detail := "296", city := "Enschede", zip := "7521AM" ] ]
    ]);
  AWAIT ExpectScreenChange(+1, PTR scr->RunModal());
  TestEq("Hengelosestraat 296\n7521 AM  Enschede\nNetherlands", topscreen->comp->addressfield->value);

  AWAIT ExpectScreenChange(+1, PTR topscreen->comp->changeaction->TolliumClick());
  TestEq("NL", TT("country")->value);
  TestEq("Hengelosestraat", (SELECT AS OBJECT component FROM TT("countryfields")->currentfields WHERE tag="STREET")->value);

//ADDME perhaps the first step in this test should have normalized already?
  AWAIT ExpectScreenChange(-1, PTR topscreen->TolliumExecuteSubmit());
  TestEq("Hengelosestraat 296\n7521 AM  Enschede\nNetherlands", topscreen->comp->addressfield->value);
  AWAIT ExpectScreenChange(-1, PTR topscreen->TolliumExecuteSubmit());

  scr := GetTestController()->LoadScreen("webhare_testsuite:tests/anycomponent.anycomponent",
    [ component := "http://www.webhare.net/xmlns/wrd/components:addressfield"
    , settings := [ required := TRUE, value := [ country := "ES", street := "La Rambla", nr_Detail := "32", city := "Barcelona", zip := "08002" ] ]
    ]);

  // Test for all fields required when initializing with a country with non-required fields
  AWAIT ExpectScreenChange(+1, PTR scr->RunModal());
  AWAIT ExpectScreenChange(+1, PTR topscreen->comp->changeaction->TolliumClick());
  TestEq("La Rambla", (SELECT AS OBJECT component FROM TT("countryfields")->currentfields WHERE tag="STREET")->value);
  TestEq([ required := FALSE ], SELECT AS RECORD CELL[ component->required ] FROM TT("countryfields")->currentfields WHERE tag="STATE");
  AWAIT ExpectScreenChange(-1, PTR topscreen->TolliumExecuteSubmit());
  AWAIT ExpectScreenChange(-1, PTR topscreen->TolliumExecuteSubmit());

  testfw->BeginWork();
  SetupWRDAddressVerification(testfw->GetWRDSchema(), [ type := "pdok" ]);
  testfw->CommitWork();

  AWAIT ExpectScreenChange(+1, PTR TTLaunchApp("webhare_testsuite:anycomponent",
      [ params := [ "http://www.webhare.net/xmlns/wrd/components:addressfield", EncodeJSON([ verification := TRUE, force_verification := TRUE, wrdschema := "wrdschema:" || testfw->GetWRDSchema()->tag ])]
      ]));

  AWAIT ExpectScreenChange(+1, PTR TTClick(":Edit"));
  TT(":Country")->value := "NL";
  TT(":Street")->value := "Adres komt";
  AWAIT ExpectAndAnswerMessageBox("OK", PTR TTClick(":Ok"), [ messagemask := "*Not enough*" ]);
  TT(":Zip code")->value := "7521AM";
  TT(":Number")->value := "296";
  AWAIT ExpectScreenChange(+1, PTR TTClick(":Ok"));
  AWAIT ExpectScreenChange(-1, PTR TTClick(":Accept correction"));
  AWAIT ExpectScreenChange(-1, PTR TTClick(":Ok"));

  AWAIT ExpectScreenChange(+1, PTR TTClick(":Edit"));
  AWAIT ExpectScreenChange(-1, PTR TTClick(":Ok"));
  AWAIT ExpectScreenChange(-1, PTR TTEscape); //close anycomponent

  scr := GetTestController()->LoadScreen("webhare_testsuite:tests/anycomponent.anycomponent",
      [ component := "http://www.webhare.net/xmlns/wrd/components:addressfield"
      , settings := [ required := TRUE
                    , value := [ country := "NL", street := "", nr_detail := "296", city := "", zip := "7500 OO" ]
                    , verification := TRUE
                    , force_verification := TRUE
                    , wrdschema := "wrdschema:" || testfw->GetWRDSchema()->tag
                    ]
      ]);

  AWAIT ExpectScreenChange(+1, PTR scr->RunModal());
  AWAIT ExpectScreenChange(+1, PTR TT(":Edit")->TolliumClick());
  AWAIT ExpectAndAnswerMessageBox("OK", PTR TTClick(":Look up"), [ messagemask := "*Unknown zip-code*" ]);
  TestEq("PDOK (Publieke Dienstverlening Op de Kaart)", TT(":Street")->value);
  TestEq("DO NOT SHIP - NIET VERZENDEN", TT(":City")->value);

  AWAIT ExpectScreenChange(-1, PTR TTEscape);


  AWAIT ExpectScreenChange(-1, PTR topscreen->TolliumExecuteSubmit());

  scr := GetTestController()->LoadScreen("webhare_testsuite:tests/anycomponent.anycomponent",
      [ component := "http://www.webhare.net/xmlns/wrd/components:addressfield"
      , settings := [ required := TRUE
                    , value := [ country := "NL", street := "", nr_Detail := "296", city := "", zip := "7521 AM" ]
                    , verification := TRUE
                    , force_verification := TRUE
                    , wrdschema := "wrdschema:" || testfw->GetWRDSchema()->tag
                    ]
      ]);

  AWAIT ExpectScreenChange(+1, PTR scr->RunModal());
  AWAIT ExpectScreenChange(+1, PTR TT(":Edit")->TolliumClick());
  AWAIT ExpectNoScreenChange(PTR TT(":Verify address")->TolliumClick());
  AWAIT ExpectScreenChange(-1, PTR topscreen->TolliumExecuteSubmit());

  TestEq("Hengelosestraat 296\n7521 AM  Enschede\nNetherlands", topscreen->comp->addressfield->value);

  TestEq(TRUE, TT(":edit")->enabled);
  topscreen->comp->enabled := FALSE;
  TestEq(FALSE, TT(":edit")->enabled);

  topscreen->comp->enabled := TRUE;
  TestEq(FALSE, topscreen->comp->addressfield->readonly);
  topscreen->comp->readonly := TRUE;
  TestEq(FALSE, TT(":edit")->enabled);
  TestEq(TRUE, topscreen->comp->addressfield->readonly);

  AWAIT ExpectScreenChange(-1, PTR topscreen->TolliumExecuteSubmit());

  scr := GetTestController()->LoadScreen("webhare_testsuite:tests/anycomponent.anycomponent",
      [ component := "http://www.webhare.net/xmlns/wrd/components:addressfield"
      , settings := [ required := TRUE
                    , defaultcountry := "nl"
                    ]
      ]);
  AWAIT ExpectScreenChange(+1, PTR scr->RunModal());
  AWAIT ExpectScreenChange(+1, PTR TTClick(":Edit"));
  TestEq("NL", TT(":Country")->value);
  AWAIT ExpectScreenChange(-1, PTR TTEscape);
  AWAIT ExpectScreenChange(-1, PTR TTEscape);

  RETURN TRUE;
}

ASYNC MACRO TestTolliumFieldsAPI()
{
  STRING screenfile := `
<screens xmlns="http://www.webhare.net/xmlns/tollium/screens" xmlns:wrd="http://www.webhare.net/xmlns/wrd/components">
  <screen name="testaddressfield" implementation="none">
    <body>
      <wrd:addressfield name="addressfield" enabled="false" />
    </body>
  </screen>
</screens>`;

  OBJECT testscreen := GetTestController()->LoadScreen(`inline-base64::${EncodeBase64(screenfile)}#testaddressfield`, DEFAULT RECORD);
  TestEq(FALSE, testscreen->addressfield->enabled);
  TestEq(FALSE, testscreen->addressfield->^changeaction->enabled);

  screenfile := `
<screens xmlns="http://www.webhare.net/xmlns/tollium/screens" xmlns:wrd="http://www.webhare.net/xmlns/wrd/components">
  <screen name="testaddressfield" implementation="none">
    <body>
      <wrd:addressfield name="addressfield" defaultcountry="nl" />
    </body>
  </screen>
</screens>`;

  testscreen := GetTestController()->LoadScreen(`inline-base64::${EncodeBase64(screenfile)}#testaddressfield`, DEFAULT RECORD);
  TestEq(TRUE, testscreen->addressfield->enabled);

  AWAIT ExpectScreenChange(+1, PTR testscreen->RunModal());
  AWAIT ExpectScreenChange(+1, PTR TTClick(":Edit"));
  TestEq("NL", TT(":Country")->value);
  AWAIT ExpectScreenChange(-1, PTR TTEscape);
  AWAIT ExpectScreenChange(-1, PTR TTEscape);

}

MACRO TestSiteprofileAddressVerify()
{
  testfw->BeginWork();
  WriteRegistrykey("webhare_testsuite.tests.addressverification", DEFAULT RECORD);
  testfw->CommitWork();

  OBJECT api;

  api := GetAddressLookupAPI(GetApplyTesterForObject(OpenTestsuiteSite()->id),"nl");
  TestEq("PDOK (Publieke Dienstverlening Op de Kaart)", api->CheckAddress([ country := "NL", nr_detail := "296", zip := "7500oo" ]).looked_up.street);

  api := GetWRDAddressLookupAPI(testfw->GetWRDSchema(),"nl"); //directly ask wrd
  TestEq("PDOK (Publieke Dienstverlening Op de Kaart)", api->CheckAddress([ country := "NL", nr_detail := "296", zip := "7500 oo" ]).looked_up.street);

  api := GetAddressLookupAPI(GetApplyTesterForObject(OpenTestsuiteSite()->OpenBypath("testpages")->id),"nl"); //indirectly ask, /testschema/ is bound to wrd:testschema
  TestEq("PDOK (Publieke Dienstverlening Op de Kaart)", api->CheckAddress([ country := "NL", nr_detail := "296", zip := "7500oo" ]).looked_up.street);

  api := GetAddressLookupAPI(GetApplyTesterForObject(OpenTestsuiteSite()->OpenBypath("testpages/staticpage")->id),"nl"); //through registry key
  TestEq("PDOK (Publieke Dienstverlening Op de Kaart)", api->CheckAddress([ country := "NL", nr_detail := "296", zip := "7500 OO" ]).looked_up.street);

  //First set the WRD schema...
  testfw->BeginWork();
  SetupWRDAddressVerification(testfw->GetWRDSchema(),
      [ type := "postcodeapi.nu"
      , apikey := "loopback"
      ]);
  testfw->CommitWork();

  api := GetAddressLookupAPI(GetApplyTesterForObject(OpenTestsuiteSite()->id),"nl");
  TestEq("PDOK (Publieke Dienstverlening Op de Kaart)", api->CheckAddress([ country := "NL", nr_detail := "296", zip := "7500oo" ]).looked_up.street);

  api := GetWRDAddressLookupAPI(testfw->GetWRDSchema(),"nl"); //directly ask wrd
  TestEq("postcodeapi.nu", api->CheckAddress([ country := "NL", nr_detail := "296", zip := "7500 oo" ]).looked_up.street);

  api := GetAddressLookupAPI(GetApplyTesterForObject(OpenTestsuiteSite()->OpenBypath("testpages")->id),"nl"); //indirectly ask, /testschema/ is bound to wrd:testschema
  TestEq("postcodeapi.nu", api->CheckAddress([ country := "NL", nr_detail := "296", zip := "7500oo" ]).looked_up.street);

  api := GetAddressLookupAPI(GetApplyTesterForObject(OpenTestsuiteSite()->OpenBypath("testpages/staticpage")->id),"nl"); //through registry key
  TestEq("postcodeapi.nu", api->CheckAddress([ country := "NL", nr_detail := "296", zip := "7500 OO" ]).looked_up.street);

  //And now the regkey
  testfw->BeginWork();
  WriteRegistrykey("webhare_testsuite.tests.addressverification",
      [ type := "postcode.nl"
      , apikey := "loopback"
      , secret := "loopback"
      ]);
  testfw->CommitWork();

  api := GetAddressLookupAPI(GetApplyTesterForObject(OpenTestsuiteSite()->id),"nl");
  TestEq("PDOK (Publieke Dienstverlening Op de Kaart)", api->CheckAddress([ country := "NL", nr_detail := "296", zip := "7500oo" ]).looked_up.street);

  api := GetAddressLookupAPI(GetApplyTesterForObject(OpenTestsuiteSite()->OpenBypath("testpages")->id),"nl"); //indirectly ask, /testschema/ is bound to wrd:testschema
  TestEq("postcodeapi.nu", api->CheckAddress([ country := "NL", nr_detail := "296", zip := "7500oo" ]).looked_up.street);

  api := GetAddressLookupAPI(GetApplyTesterForObject(OpenTestsuiteSite()->OpenBypath("testpages/staticpage")->id),"nl"); //through registry key
  TestEq("postcode.nl", api->CheckAddress([ country := "NL", nr_detail := "296", zip := "7500 OO" ]).looked_up.street);

  testfw->BeginWork();
  WriteRegistrykey("webhare_testsuite.tests.addressverification", DEFAULT RECORD); //reset it just in case
  testfw->CommitWork();
}

MACRO TestFallbackAPI()
{
  testfw->BeginWork();
  SetupWRDAddressVerification(testfw->GetWRDSchema(), [ type := "pdok" ]);

  OBJECT api := GetWRDAddressLookupAPI(testfw->GetWRDSchema(),"be"); //not offered by PDOK

  RECORD checkres;

  checkres := api->CheckAddress([ zip := "2018", nr_detail := "27", country := "BE", street := "Koningin Astridplein", city := "Antwerpen"], [ alwaysverify := TRUE ]);
  TestEq('ok', checkres.status);
  TestEq([ city := "Antwerpen", country := "BE", nr_detail := "27", zip := "2018", street := "Koningin Astridplein"], checkres.data);

  //block invalid BE zips
  checkres := api->CheckAddress([ zip := "999", nr_detail := "27", country := "BE", street := "Koningin Astridplein", city := "Antwerpen"], [ alwaysverify := TRUE ]);
  TestEq('invalid_zip', checkres.status);

  checkres := api->CheckAddress([ zip := "2500", nr_detail := "Lier", country := "BE", street := "Baron Opsomerlaan", city := "1"], [ alwaysverify := TRUE ]);
  TestEq('invalid_city', checkres.status);

  //block invalid DE zips
  checkres := api->CheckAddress([ zip := "655520", nr_detail := "1", country := "DE", street := "Hauptstra\u00DFe", city := "Bad Camberg"], [ alwaysverify := TRUE ]);
  TestEq('invalid_zip', checkres.status);
  checkres := api->CheckAddress([ zip := "6520", nr_detail := "1", country := "DE", street := "Hauptstra\u00DFe", city := "Bad Camberg"], [ alwaysverify := TRUE ]);
  TestEq('invalid_zip', checkres.status);

  //block users typing their phone number into the nr_detail field
  checkres := api->CheckAddress([ zip := "2018", nr_detail := "0474000000", country := "BE", street := "Koningin Astridplein", city := "Antwerpen"], [ alwaysverify := TRUE ]);
  TestEq('invalid_nr_detail', checkres.status);

  testfw->RollbackWork();
}

MACRO TestCountryCallingCodes()
{
  TestEq([ [ code := "NL", title := "Netherlands", phone := "31" ] ], SELECT * FROM GetWRDCountryPhoneCodes("en") WHERE code = "NL");
  TestEq([ [ code := "SX", title := "Sint-Maarten", phone := "1" ] ], SELECT * FROM GetWRDCountryPhoneCodes("nl") WHERE code = "SX");
}


Runtestframework([ PTR AddressConversion
                 , PTR GenericCodes
                 , PTR NLCodes
                 , PTR TestAddressInfo
                 , PTR TolliumFieldsNew
                 , PTR TolliumFieldsEdit
                 , PTR TestTolliumFieldsAPI
                 , PTR TestSiteprofileAddressVerify
                 , PTR TestFallbackAPI
                 , PTR TestCountryCallingCodes
                 ], [ testusers := [[ login := "user" ]
                                   ]
                    ]);
