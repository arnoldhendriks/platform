<module xmlns="http://www.webhare.net/xmlns/system/moduledefinition">

  <meta>
    <description>Alpha and beta testing of WebHare, Tollium and add-on modules</description>
    <version>4.31.0</version>
    <validation options="nowarnings">
      <exclude mask="tests/wh/hscore/xml/generated/*" why="Don't validate generated code" />
      <!-- <exclude mask="tests/*" why="Tests (and their helper data) often violates validation rules" /> -->
      <exclude mask="tolliumapps/demo/*"
               why="the demos appear broken but not sure yet if we need to keep or remove them. some are used in tests" />
      <exclude mask="screens/tests/*" why="Many missing tids" />
      <exclude mask="tests/system/modules/data/brokenscreen.xml" why="Purposefully broken screen" />
      <exclude mask="tests/tollium/data/addons.xml" why="Purposefully broken screen" />
      <exclude mask="tests/publisher/siteprofile/data/test*.xml" why="Purposefully broken site profiles" />
      <exclude mask="tests/wh/hscore/xml/testinfo.xml" why="Refers to generated/testinfo.xml but this file does not exist until the XML tests have been generated" />

      <!-- <exclude mask="tests/*/data/*" why="don't validate test data, its often broke" /> -->
      <exclude mask="*testdata_loadlib_circ_?.whlib" why="These libraries are supposed to fail to compile" />

      <!-- tid tests need to generate tid errors, some tests explicitly have to trigger warnings -->
      <ignoremessage mask="*gettid*" regex="Invalid tid"/>
      <ignoremessage mask="*gettid*" regex="Missing tid"/>
      <ignoremessage mask="*hsengine/test*" regex="(WHERE clause partly not|redeclared|been declared|erroneously as a RECORD|has been deprecated)"/>
      <ignoremessage mask="*/tidtestscreen.xml" regex="Missing tid"/>
      <ignoremessage mask="*.whscr" regex="has been deprecated"/>
      <ignoremessage mask="*" regex="Missing tid"/>

      <eslint masks="nosuch.js *.ts *.tsx" />
      <format masks="*.ts *.tsx" />

    </validation>
  </meta>

  <servicemanager>
    <runonce script="scripts/whcommands/reset.whscr" tag="setuptestsite" when="poststart" ifenvironset="WEBHARE_CI" />
    <runonce script="tests/publisher/pwa/setuppwasite.whscr" tag="setuppwasite" when="poststart" ifenvironset="WEBHARE_CI" />
    <runonce script="scripts/runonce/typescript-runonce.ts" tag="typescript_runonce" when="poststart" />

    <task script="scripts/tasks/singleshottest.ts" tag="singleshottest" description="Test singleshot tasks">
      <arg>ARG1 SHOT</arg>
      <arg>arg2</arg>
    </task>
    <task runat="5 0 * * *" runtz="Europe/Amsterdam" script="scripts/whcommands/reset.whscr" tag="resettestsite" description="Periodically reset testsite and testdata" unlessenvironset="WEBHARE_CI" />

    <taskcluster harescriptworkers="2" tag="testcluster" />

    <managedtask type="aftercompile" objectname="lib/internal/tasks.whlib#aftercompiletask" />  <!-- used by assetmgr-brokentest -->
    <managedtask type="applyruletask" objectname="lib/internal/tasks.whlib#applyruletask" />  <!-- used by actions.whscr -->
    <managedtask type="ping" failreschedule="4000" objectname="lib/internal/tasks.whlib#pingtask" />
    <managedtask type="ping_js" failreschedule="4000" taskrunner="tests/system/queues/data/testtasks.ts#pingJS" />
    <managedtask type="pingretry2" maxfailures="1" objectname="lib/internal/tasks.whlib#pingtask" />
    <managedtask type="pingretry2_js" maxfailures="2" taskrunner="tests/system/queues/data/testtasks.ts#pingJS" />
    <managedtask type="cancellable" objectname="lib/internal/tasks.whlib#cancellabletask" />
    <managedtask type="cancellable_js" taskrunner="tests/system/queues/data/testtasks.ts#cancellabletaskJS" />
    <managedtask type="temporaryfailure" objectname="lib/internal/tasks.whlib#TemporaryFailingTask" />
    <managedtask type="temporaryfailure_js" taskrunner="tests/system/queues/data/testtasks.ts#failingTaskJS" />
    <managedtask type="timelimitedtask" objectname="lib/internal/tasks.whlib#TimelimitedTask" timeout="300" />
    <managedtask type="timelimitedtask_js" taskrunner="tests/system/queues/data/testtasks.ts#timelimitedtaskJS" timeout="300" />
    <managedtask type="usereditedtask" objectname="lib/internal/tasks.whlib#UserEditedTask" />
    <managedtask type="doublescheduletask" cluster="testcluster" objectname="lib/internal/tasks.whlib#DoubleScheduleTask" />
    <managedtask type="doublescheduletask_js"
                 cluster="testcluster"
                 taskrunner="tests/system/queues/data/testtasks.ts#doubleScheduleTaskJS" />

    <ephemeraltask type="ping" objectname="lib/internal/tasks.whlib#ephpingtask" />
  </servicemanager>

  <portal>
    <linkapp name="localtests" group="system:development" isdeveloperapp="true" link="/tests/" tid="localtests.apptitle">
      <accesscheck>
        <requireright right="system:sysop" />
      </accesscheck>
    </linkapp>

    <application name="runscreen" startmacro="lib/tollium.whlib#RunScreen">
      <accesscheck />
    </application>
    <application name="runyamlscreen" startmacro="lib/tollium.whlib#RunYamlScreenApp">
      <accesscheck />
    </application>
    <application name="testdocs" startmacro="tests/tollium/shell/data/testdocs.whlib#starttestdocs">
      <accesscheck />
    </application>
    <application name="uploadtest" screen="/tolliumapps/components/uploadtest.xml">
      <accesscheck />
    </application>
    <application name="anycomponent" startmacro="lib/screens/tests/anycomponent.whlib#RunAnyComponent">
      <accesscheck />
    </application>
    <application name="appstarttest" screen="/screens/tests/comm.xml#appstart">
      <accesscheck />
    </application>
    <application name="wasmstarttest" engine="wasm" screen="/screens/tests/comm.xml#appstart">
      <accesscheck />
    </application>
    <application name="human" screen="/screens/tests/human.xml#main">
      <accesscheck />
    </application>
    <application name="demo" screen="tolliumapps/demo/main.xml#main" title="Demo &amp; dev">
      <accesscheck />
    </application>

    <!-- temp, have to add a main.xml / tollium.xml which opens this? -->
    <application name="imagemap" screen="tolliumapps/demo/imagemap-editor/imagemap_editor.xml#main" title="Imagemap editor">
      <accesscheck />
    </application>
    <application name="wrddemo" screen="tolliumapps/demo/wrd/wrddemo.xml#main" supportedlanguages="en">
      <accesscheck />
    </application>
    <application name="mapdemo" screen="tolliumapps/demo/socialite/mapdemo.xml#maps">
      <accesscheck />
    </application>

    <notification name="eventservertest"
                  defaultenabled="true"
                  descriptiontid="module.eventservertest-desc"
                  tid="module.eventservertest" />
  </portal>

  <tollium>
    <components namespace="http://www.webhare.net/xmlns/webhare_testsuite/testcomponents" xmlschema="testcomponents.xsd" />
  </tollium>

  <services>
    <openapiservice name="testservice_novalidation"
                    inputvalidation="never"
                    outputvalidation="never"
                    spec="tests/wh/webserver/remoting/openapi/testservice.yaml" />

    <openapiclient name="testclient" spec="tests/wh/webserver/remoting/openapi/testservice.yaml" />

    <webservice name="test"
                library="lib/webservicetest.whlib"
                prefix="RPC_"
                primarytrans="auto"
                requirewhaccount="true"
                transports="jsonrpc whremoting">
      <accesscheck>
        <requireright right="system:sysop" />
      </accesscheck>
      <addheader header=" X-Test-Header :  * " />
    </webservice>
    <webservice name="testnoauth" library="lib/webservicetest.whlib" prefix="RPC_" transports="jsonrpc whremoting">
      <accesscheck />
    </webservice>
    <webservice name="testnoauthjs" service="js/jsonrpc/service.ts#TestNoAuthJS" transports="jsonrpc">
      <accesscheck />
    </webservice>
    <webservice name="sharedtests" library="lib/sharedtests.whlib" prefix="RPC_" transports="jsonrpc">
      <accesscheck />
    </webservice>

    <apprunnerconfig configfunction="lib/internal/support.whlib#getapprunnerconfig" />
  </services>

  <rights>
    <!-- Rights for tests -->
    <objecttype name="testobjecttype"
                describer="lib/internal/rights.whlib#betatreeobjecttypedescriber"
                parentfield="parent"
                table=".rights_tree"
                title="Test rights objecttype for table beta.rights_tree" />
    <objecttype name="testflatobjecttype"
                describer="lib/internal/rights.whlib#betatreeflatobjecttypedescriber"
                table=".rights_tree"
                title="Test rights objecttype for table beta.rights_tree, without parent field" />

    <right name="globalright" title="webhare_testsuite globalright">
      <impliedby right="system:sysop" />
    </right>
    <right name="objectright" objecttype="testobjecttype">
      <impliedby right="system:sysop" />
    </right>
    <right name="objectright2" objecttype="testobjecttype">
      <impliedby right="globalright" />
    </right>
    <right name="objectright2_impl" objecttype="testobjecttype">
      <impliedby right="objectright2" />
    </right>
    <right name="objectflatright" objecttype="testflatobjecttype">
      <impliedby right="globalright" />
    </right>
    <right name="betatesting" title="Allow acces to beta test apps">
      <impliedby right="system:sysop" />
    </right>
  </rights>

  <backend>
    <webrule path="root:/.webhare_testsuite/tests/"
             match="initial"
             allowallmethods="true"
             redirecttodir="web:/tests/"
             maxservertype="development" />
    <webrule path="root:/.webhare_testsuite/tests/js/"
             match="initial"
             allowallmethods="true"
             router="web/tests/js/testrouter.ts#handleJSRequest"
             maxservertype="development" />
    <webrule path="root:/.webhare_testsuite/tests/csp" match="initial">
      <csp policy="frame-ancestors [webhare];script-src 'self'" />
    </webrule>
    <webrule path="root:/.webhare_testsuite/tests/pages/captcha" match="initial">
      <csp policy="frame-ancestors [webhare];script-src 'self' https://www.google.com https://www.gstatic.com" />  <!-- needed for publisher.components.test-captcha -->
    </webrule>
    <webrule path="root:/.webhare_testsuite/tests/searchallcases/"
             match="initial"
             allowallmethods="true"
             redirecttodir="web:/tests/"
             searchallcases="true"
             maxservertype="development" />
    <webruleset name="testset" tid="module.webruleset">
      <webrule path="/xyz/" match="initial" redirecttoscript="urls/xyz.shtml" />
    </webruleset>
    <webruleset name="testframework" title="WebHare test framework: B-Lex testsuite testsite">
      <webrule path="/" match="initial" wrdschema="wrd:testschema" />
    </webruleset>
    <webrule path="root:/betatesting/testset/" match="initial" applyruleset="webhare_testsuite:testset" />
    <webruleset name="thirdpartytest" title="Testsuite: testwrdauthplugin thirdpartytest">
      <webrule path="/wrd/authtest/webhare_testsuite.shtml" match="exact" handlebyscript="web/tests/wrd/authdebug.shtml" />
      <webrule path="/wrd/authtest/accessruleaccounts/"
               match="initial"
               authmode="ipandaccess"
               authscript="webdesigns/basetest/lib/webhare-auth-portal1.whscr"
               handlebydir="web/tests/"
               requirewhaccount="true" />
    </webruleset>
    <webruleset name="securityheaders" title="Testsuite: security headers">
      <webrule path="/" match="initial">
        <addheader header="X-Frame-Options" value="SAMEORIGIN" />
        <addheader header="Strict-Transport-Security" value="max-age=15552000" />
        <addheader header="X-XSS-Protection" value="1; mode=block" />
        <csp policy="script-src 'self'" />
      </webrule>
    </webruleset>
    <restapi apispec="tests/wh/webserver/remoting/restapi/restapi.xml" path="root:/.webhare_testsuite/restapi/" />
    <openapi path="root:/.webhare_testsuite/openapi/testservice/" service="testservice" />
    <openapi path="root:/.webhare_testsuite/openapi/extendedservice/" service="extendedservice" />
    <openapi path="root:/.webhare_testsuite/openapi/authtests/" service="webhare_testsuite:authtests" />
    <webrule path="root:/.webhare_testsuite/datastorage/" match="initial">
      <tryfolder path="storage::webhare_testsuite/tmp/folderdirect/" />
      <tryfolder path="storage::webhare_testsuite/tmp/foldersha/" resolver="sha256b16" />
      <tryfolder path="storage::webhare_testsuite/tmp/foldersha_dir/" resolver="sha256b16_directory" />
      <tryfile path="storage::webhare_testsuite/tmp/file.shtml" />
      <tryfile path="mod::[modulename]/web/resources/tests/static.html" />
    </webrule>
    <usereditpolicy name="usereditpolicy"
                    title="webhare_testsuite test user edit policy"
                    usereditedmanagedtask="usereditedtask" />
  </backend>

  <databaseschema xmlns:d="http://www.webhare.net/xmlns/whdb/databaseschema">
    <d:table name="rights_tree" primarykey="id">
      <d:integer name="id" autonumberstart="1" />
      <d:varchar name="name" maxlength="256" nullable="0" />
      <d:integer name="parent" ondelete="cascade" references=".rights_tree" />
    </d:table>

    <d:table name="consilio_index" primarykey="id">
      <d:integer name="id" autonumberstart="1" />
      <d:varchar name="groupid" maxlength="1024" />
      <d:varchar name="objectid" maxlength="1024" />
      <d:varchar name="text" maxlength="1024" />
      <d:datetime name="adate" />
      <d:datetime name="grouprequiredindexdate" />  <!-- ignored when default -->
      <d:datetime name="objectrequiredindexdate" />  <!-- ignored when default -->
      <d:datetime name="indexdate" />
      <d:obsoletecolumn name="spiderfrom" />
      <d:varchar name="extradata" maxlength="4096" />
    </d:table>

    <d:obsoletetable name="hosted_blobs" />

    <d:table name="exporttest" primarykey="id">
      <d:integer name="id" autonumberstart="1" />
      <d:varchar name="text" maxlength="4096" />
      <d:blob name="datablob" />
    </d:table>
  </databaseschema>

  <logging>
    <log name="test" filename="betatest" rotates="4" timestamps="false" />
  </logging>

  <moduleregistry>
    <node name="tests">
      <string name="ingenicomode" />
      <string name="response" />
      <string name="alttestsitehost" />
      <string name="hashedpassword" />
      <string name="secondhareinterface" />
      <record name="addressverification" />
      <datetime name="lastaftercompile" />
      <boolean name="taskthrownow" />
      <string name="docroot" />
      <string name="runoncetest" />
    </node>

    <obsoletenode name="apis" />

    <node name="registrytests">
      <obsoletekey name="removekey" />
      <obsoletenode name="removenode" />
    </node>
  </moduleregistry>

  <publisher>
    <siteprofile path="data/webhare_testsuite.siteprl.xml" />
    <siteprofile path="data/wts.siteprl.yml" />
    <siteprofile path="tests/consilio/data/consiliotests.siteprl.xml" />

    <virtualfs name="beta_tests" objectname="lib/myvirtualfs.whlib#BetaFS">
      <accesscheck>
        <requireright right="system:sysop" />
      </accesscheck>
    </virtualfs>
    <virtualfs name="beta_tests2" objectname="lib/myvirtualfs.whlib#BetaModularFS">
      <accesscheck>
        <requireright right="system:sysop" />
      </accesscheck>
    </virtualfs>

    <worklisttype name="filteredlist" filterscreen="tests/publisher/tollium/data/worklistfilter.xml#FilteredListPanel" />

    <webdesign name="basetest"
               title="WH testsuite: selftest 'basetest'"
               hidden="true"
               siteprofile="webdesigns/basetest/basetest.siteprl.xml">
      <assetpack entrypoint="webdesigns/basetest/js/basetest" supportedlanguages="en nl ps" />
    </webdesign>
    <webdesign name="pwatest"
               title="WH testsuite: pwatest"
               hidden="true"
               siteprofile="webdesigns/pwatest/pwatest.siteprl.xml">
      <assetpack entrypoint="webdesigns/pwatest/pwatest" supportedlanguages="en" />  <!-- FIXME aftercompiletask="webharedev_pwa:rebuildpages" ?? -->
    </webdesign>
    <webdesign name="alttest"
               title="WH testsuite: selftest 'attest'"
               hidden="true"
               siteprofile="webdesigns/basetest/alttest.siteprl.xml" />
    <webdesign name="neverused_compiletest"
               title="WH testsuite: never used siteprl"
               hidden="true"
               siteprofile="webdesigns/basetest/neverused_compiletest.siteprl.xml" />

    <addtoassetpack assetpack="webhare_testsuite:basetest" entrypoint="webdesigns/basetest/js/addtopack" />

    <addtowebdesign siteprofile="webdesigns/basetest/siteprofiles/addtobasetest.siteprl.xml"
                    webdesign="webhare_testsuite:basetest" />

    <customnode name="test" namespace="urn:xyz" />
    <customnode name="blub" namespace="http://www.webhare.net/xmlns/webhare_testsuite/blub" />

    <webdesignplugin name="test"
                     hooksfeatures="preparemail"
                     namespace="http://www.webhare.net/xmlns/webhare_testsuite"
                     objectname="mod::webhare_testsuite/lib/internal/testplugin.whlib#TestPlugin" />

    <filemgrextension>
      <accesscheck>
        <requireright right="system:sysop" />
      </accesscheck>
      <addaction addtomenu="actions" screen="feedback.frompublisher" tid="module.testaction" />
    </filemgrextension>

    <indexfield member="textfield"
                namespace="http://www.webhare.net/xmlns/webhare_testsuite/publisher/publishersearchtest"
                tokenized="true" />
    <indexfield member="keywordfield"
                namespace="http://www.webhare.net/xmlns/webhare_testsuite/publisher/publishersearchtest" />
    <indexfield member="test_whfsrefarray"
                namespace="http://www.webhare.net/xmlns/webhare_testsuite/publisher/publishersearchtest" />
    <indexfield name="contenttypefield"
                member="testfield"
                namespace="http://www.webhare.net/xmlns/webhare_testsuite/publisher/publishersearchtest" />
    <indexfield name="libraryfield" fieldfunc="lib/publisher/publishersearch.whlib#gettestfield" />
    <indexfield name="arrayfield"
                member="testarray"
                namespace="http://www.webhare.net/xmlns/webhare_testsuite/publisher/publishersearchtest">
      <arrayfield member="arrayfield" />
      <arrayfield member="textfield" tokenized="true" />
      <arrayfield name="date_otherfield" member="otherfield" />
      <arrayfield name="libraryfield" fieldfunc="lib/publisher/publishersearch.whlib#getarrayfield" />
      <arrayfield member="deeper_whfsrefarray" />
    </indexfield>
    <indexfield name="purearray"
                member="testarray"
                namespace="http://www.webhare.net/xmlns/webhare_testsuite/publisher/publishersearchtest">
      <arrayfield name="libraryfield" fieldfunc="lib/publisher/publishersearch.whlib#getarrayfield" />
    </indexfield>

    <!-- test overwrites -->
    <indexfield member="keywordfield"
                namespace="http://www.webhare.net/xmlns/webhare_testsuite/publisher/publishersearchtest2" />

    <formcomponents namespace="http://www.webhare.net/xmlns/webhare_testsuite/testformcomponents"
                    xmlschema="tests/publisher/forms/data/testformcomponents.xsd" />

    <registerslot name="testsite"
                  type="site"
                  descriptiontid="module.testsite.description"
                  initialvalue="site::webhare_testsuite.testsite"
                  tid="module.testsite.title" />
    <registerslot name="testslot"
                  type="folder"
                  description="Test Description"
                  fallback="whfs::/webhare-tests/webhare_testsuite.testfolder/testslot-fallback"
                  initialvalue="whfs::/webhare-tests/webhare_testsuite.testfolder/testslot"
                  title="Test Slot" />

    <searchcontentprovider name="testprovider" objectname="lib/publisher/publishersearch.whlib#testprovider" version="1.0.0" />

    <shorturls domain="test2.bea.gl"
               urllisthook="tests/publisher/shorturl/shorturltests.whlib#HookURLList"
               urlpropsextension="tests/publisher/shorturl/shorturltests.xml#ExtendURLProps" />

    <webdesign name="basetestjs" title="BasetestJS" siteprofile="webdesigns/basetestjs/basetestjs.siteprl.xml" />

    <tagset name="testtags" />
  </publisher>

  <consilio>
    <!-- modern syntax -->
    <obsoletecatalog tag="obsoletecatalog" />
    <catalog tag="testsitecatalog"
             fieldgroups="pagelistfields consilio:sitecontent"
             ongetsources="lib/consilio/sources.whlib#GetTestSiteCatalogSources"
             priority="9">
      <customsource contentobject="lib/consilio/loremindex.whlib#IndexLibrary" tag="loremindex" />
      <customsource contentobject="lib/consilio/dbindex.whlib#DBContent" tag="dbindex" />
    </catalog>
    <fieldgroup tag="pagelistfields">
      <text name="myfield1" />
      <integer name="fieldgroup_integer" />
      <integer64 name="fieldgroup_integer64" />
      <float name="fieldgroup_float" />
      <boolean name="fieldgroup_boolean" />
      <datetime name="fieldgroup_datetime" />
    </fieldgroup>
    <fieldgroup tag="nested_records">
      <record name="record_field">
        <text name="firstname" />
        <text name="lastname" />
      </record>
      <nested name="nested_field">
        <text name="firstname" />
        <text name="lastname" />
      </nested>
      <nested name="documents">
        <text name="title" />
        <keyword name="type" />
        <nested name="pages" include_in_parent="true" include_in_root="true">
          <integer name="page" />
          <text name="content" />
        </nested>
      </nested>
    </fieldgroup>
    <catalog tag="testindex" fieldgroups="testindexfields" lang="nl" managed="false" />
    <catalog tag="testindex_suffixed" fieldgroups="testindexfields" managed="false" suffixed="true" />
    <fieldgroup tag="testindexfields">
      <datetime name="@timestamp" />
      <text name="title" />
      <keyword name="keyword" />
      <text name="body">
        <keyword name="body_keyword" ignore_above="256" />
      </text>
      <integer name="int_field" />
      <integer64 name="int64_field" />
      <float name="float_field" />
      <datetime name="date_field" />
      <boolean name="bool_field" />
      <record name="record_field">
        <text name="record_text_field" />
        <integer name="dn_*" />
      </record>
      <ipaddress name="ip_field" />
      <latlng name="latlng_field" />
      <addfieldgroup ref="testgroup" />
      <record name="documents">
        <!-- some modules will store record *arrays* into a field like this -->
        <text name="title" />
        <keyword name="slug" />
      </record>
    </fieldgroup>
    <fieldgroup tag="conflicttest_blue">
      <text name="c_text" />
      <keyword name="c_keyword" />
      <text name="c_indexoptions" index_options="positions" />  <!-- The default for OpenSearch -->
      <keyword name="c_ignoreabove" />
      <nested name="c_nested" include_in_parent="true" />
    </fieldgroup>
    <fieldgroup tag="conflicttest_red">
      <keyword name="c_text" />
      <text name="c_keyword" />
      <text name="c_indexoptions" />
      <keyword name="c_ignoreabove" ignore_above="256" />
      <nested name="c_nested" include_in_root="true" />
    </fieldgroup>
    <fieldgroup tag="testgroup">
      <integer name="group_int_field" />
      <record name="group_record_field">
        <text name="group_record_text_field" />
        <keyword name="ds_*" />
        <addfieldgroup ref="webhare_testsuite:testsubgroup" />
      </record>
    </fieldgroup>
    <fieldgroup tag="testsubgroup">
      <datetime name="when" />
      <latlng name="where" />
      <boolean name="db_*" />
    </fieldgroup>
    <catalog tag="module_reference" fieldgroups="consilio:test_reference_group" managed="false" />
    <catalog tag="emptycatalog" managed="false" />
    <fieldgroup tag="test_reference_group">
      <text name="testsuite" />
    </fieldgroup>
    <fieldgroup tag="circular_test_reference_group">
      <addfieldgroup ref="consilio:circular_test_reference_group" />
    </fieldgroup>
    <obsoletecatalog tag="duplicate_fieldnames" />  <!-- added and removed during 5.02 development -->
    <fieldgroup tag="fields_1">
      <text name="textfield" />
    </fieldgroup>
    <fieldgroup tag="fields_2">
      <text name="textfield" />
    </fieldgroup>
    <fieldgroup tag="circular">
      <addfieldgroup ref="circular" />
    </fieldgroup>
    <fieldgroup tag="circular_1">
      <record name="record_1">
        <addfieldgroup ref="circular_2" />
      </record>
    </fieldgroup>
    <fieldgroup tag="circular_2">
      <record name="record_2">
        <addfieldgroup ref="circular_1" />
      </record>
    </fieldgroup>
    <fieldgroup tag="nosuchfieldyet">
      <keyword name="nosuchfieldyet_extra" />
      <keyword name="nosuchfieldyet_extra2" />
      <record name="no">
        <record name="such">
          <record name="field">
            <integer name="yet" />
          </record>
        </record>
      </record>
    </fieldgroup>
    <fieldgroup tag="ignore_nosuchfieldyet">
      <keyword name="nosuchfieldyet_extra" />
      <record name="no">
        <record name="such">
          <ignore name="field" />
        </record>
      </record>
    </fieldgroup>
    <fieldgroup tag="storeonly">
      <keyword name="indexed" ignore_above="16" />
      <keyword name="stored" ignore_above="16" storeonly="true" />
    </fieldgroup>
    <fieldgroup tag="extrafield">
      <text name="extrafield" />
      <keyword name="pagekeywords" />
    </fieldgroup>
    <addtowhfsindex priority="aftersites" privatefolder="addtowhfsindex/aftersites" />
    <addtowhfsindex priority="beforerepository" privatefolder="addtowhfsindex/beforerepository" />
    <addtowhfsindex priority="beforesites" privatefolder="addtowhfsindex/beforesites" />
    <addtowhfsindex privatefolder="addtowhfsindex/anywhere" />

    <!-- legacy syntax -->
    <index tag="toolslog">
      <datetime name="@timestamp" />
      <record name="minfin">
        <keyword name="theme" />
        <keyword name="tool" />
        <keyword name="type" />
        <integer name="versionnr" />
      </record>
    </index>
  </consilio>

  <hooking>
    <target name="siteprofilehook" />
    <intercept name="publisher_whfsindex"
               interceptfunction="lib/internal/hooks.whlib#HookPublisherIndexPriorities"
               target="publisher:whfsindex_priority" />
  </hooking>

  <documentation>
    <embedded rooturlkey="webhare_testsuite.tests.docroot" subpath="docsubpath" />
  </documentation>

  <wrdschemas>
    <schema tag="testschema" definitionfile="mod::webhare_testsuite/tests/wrd/data/testschemadef.wrdschema.xml" title="WebHare testsuite test schema" maxservertype="development" />
  </wrdschemas>

</module>
