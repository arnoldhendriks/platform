﻿<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::javascript.whlib";

LOADLIB "mod::tollium/lib/screenbase.whlib";
LOADLIB "mod::tollium/lib/towl.whlib";

LOADLIB "mod::system/lib/database.whlib";


PUBLIC OBJECTTYPE AppStart EXTEND TolliumScreenBase
<
  INTEGER sentmsgs;
  INTEGER incomingmsgcount;
  BOOLEAN confirmclose;

  MACRO Init(RECORD data)
  {
    IF(Length(data.params) = 1 AND data.params[0] = "sleep")
      Sleep(500); //give focus steal test an unfair chance

    INTEGER my_targetid := RecordExists(this->tolliumcontroller->applicationtarget) ? this->tolliumcontroller->applicationtarget.test : 0;
    this->targetval->value := my_targetid;
    ^engine->value := IsWasm() ? "wasm" : "native";
    this->UpdateTitle();
    this->UpdateExpiryDate();
  }

  MACRO UpdateTitle()
  {
    this->frame->title := "app_" || (RecordExists(this->tolliumcontroller->applicationtarget) ? this->tolliumcontroller->applicationtarget.test : 0) || "_" || this->incomingmsgcount;
  }

  MACRO UpdateExpiryDate()
  {
    ^expirydate->value := FormatISO8601DateTime(this->contexts->controller->sessionexpires, "", "milliseconds", "Europe/Amsterdam", FALSE);
  }

  MACRO DoSetExpiryDateToNow()
  {
    // add a bit of delay to make sure setting the expiry doesn't crash other stuff
    this->contexts->controller->sessionexpires := AddTimeToDate(250, GetCurrentDateTime());
    this->UpdateExpiryDate();
  }

  MACRO DoStartApp()
  {
    INTEGER my_targetid := RecordExists(this->tolliumcontroller->applicationtarget) ? this->tolliumcontroller->applicationtarget.test : 0;
    this->sentmsgs := this->sentmsgs + 1;
    this->tolliumcontroller->SendApplicationMessage("webhare_testsuite:appstarttest", [ test := 1 ], [ id := my_targetid || ":" || this->sentmsgs ], TRUE);
  }

  MACRO OnMessage(RECORD data)
  {
    this->incomingmsgcount := this->incomingmsgcount + 1;
    this->messages->value := this->messages->value || "\n" || data.id;
    this->UpdateTitle();
  }

  MACRO DoAbortApp()
  {
    ABORT([[text := "DoAbortApp requested"],...RepeatElement([text := "Extra line"],40)]); //generate enough content to cause the errordialog to need scroll
  }

  MACRO DoOpenNoclose()
  {
    this->LoadScreen(".noclose")->RunModal();
  }
  MACRO DoOpenCloseable()
  {
    this->LoadScreen(".closeable")->RunModal();
  }
  MACRO DoToggleAllowClose()
  {
    this->confirmclose := TRUE;
    this->frame->allowclose := NOT this->frame->allowclose;
  }
  BOOLEAN FUNCTION Cancel()
  {
    RETURN this->confirmclose = FALSE OR this->RunMessageBox(".confirmclose")="yes";
  }
  MACRO DoRestart()
  {
    // target = 0 -> target=1+message=1 -> target=1+message="" -> target=0
    IF (this->targetval->value = 0)
      this->contexts->controller->RestartApplication(
          [ target :=   [ test := this->targetval->value + 1 ]
          , message :=  [ id := this->targetval->value + 1 ]
          ]);
    ELSE
    {
      IF (this->messages->value != "")
        this->contexts->controller->RestartApplication();
      ELSE
        this->contexts->controller->RestartApplication([ target := DEFAULT RECORD ]);
    }
  }
>;


PUBLIC OBJECTTYPE EventServer EXTEND TolliumScreenBase
<
  INTEGER msgcount;

  MACRO DoSendMessage()
  {
    this->msgcount := this->msgcount + 1;
    //ADDME would be best to send it directly to the user's session, but the tests run with an unaddressable anonymous user
    ShowTowlNotification("webhare_testsuite:eventservertest",
                             [ webhare_users := [ GetEffectiveUserId() ] ],
                             [ descriptionparams := [ ToString(this->msgcount )]
                             , icon := "tollium:filemgr/mydocuments"
                             , persistent := TRUE
                             , tag := this->msgcount > 1 ? "webhare_testsuite:towltag" : "" //start deduplicating after first
                             //, priority := TowlPriorityHigh
                             ]);
  }
>;
