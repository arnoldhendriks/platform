﻿<?wh

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/webserver.whlib";
LOADLIB "mod::system/lib/remoting/remotable.whlib";
LOADLIB "mod::system/lib/remoting/server.whlib";
LOADLIB "mod::system/lib/services.whlib";
LOADLIB "mod::system/lib/internal/webserver/config.whlib";
LOADLIB "mod::system/lib/internal/webserver/reloadconfig.whlib";

OBJECTTYPE TestObject EXTEND RemotableObject
< UPDATE PUBLIC STRING ARRAY FUNCTION __GetRemotableMethods() { RETURN [ "GetStr1", "CallTest", "Crash" ]; }

  PUBLIC STRING FUNCTION GetStr1()
  {
    RETURN "str1";
  }

  PUBLIC STRING FUNCTION CallTest(INTEGER a, STRING b)
  {
    IF (a = 10)
      THROW NEW Exception("Throwing b: " || b);
    RETURN a || " " || b;
  }

  PUBLIC OBJECT FUNCTION GetThis()
  {
    RETURN this;
  }

  PUBLIC MACRO Crash()
  {
    ABORT("crash requested");
  }
>;

PUBLIC RECORD FUNCTION RPC_TestRateLimit(RECORD options)
{
  options := ValidateOptions([ numhits := 1
                             , timeperiod := 1000
                             ], options);

  RECORD result := CheckRateLimit([ type := "checkratelimit" ], options.numhits, CELL[ options.timeperiod ]);
  IF(NOT result.accepted)
    THROW NEW RPCTooManyRequestsException((result.backoff + 999) / 1000);

  RETURN [accepted := TRUE];
}

/** @short Answer difficult questions by reflecting them to the questioneer */
PUBLIC STRING FUNCTION RPC_Echo(STRING indata)
{
  RETURN indata;
}

PUBLIC VARIANT FUNCTION RPC_EchoAny(VARIANT indata)
{
  RETURN indata;
}

PUBLIC OBJECT FUNCTION RPC_GetObject()
{
  RETURN NEW TestObject;
}

PUBLIC MACRO RPC_IReturnNothing()
{
}

PUBLIC OBJECT FUNCTION RPC_ObjectsRule(OBJECT inobj)
{
  RETURN inobj;
}

/** @short Test functions with deep data structures
    @param x record X
    @param y Second parameter, a record array with integer 'i' and recordarray 'ra'
    @return a record
    @cell(integer) x.i x.i
    @cell(record array) x.ra x.ra
    @cell(string) x.ra.s x.ra.s
    @cell(boolean) x.ra.f x.ra.f
    @cell(record array) y.ra y.ra
    @cell(integer) return.i return.i
    @cell(record array) return.ra return.ra
    @cell(string array) return.ra.sa return.ra.sa
*/
PUBLIC RECORD FUNCTION RPC_ComplexResults(RECORD x)
{
  RETURN x;
}

PUBLIC RECORD FUNCTION RPC_ComplexResultsSlow(RECORD x)
{
  Sleep(300);
  RETURN x;
}

PUBLIC RECORD FUNCTION RPC_CrashTest(STRING type)
{
  SWITCH (type)
  {
    CASE "abort" { ABORT("boem"); }
    CASE "throw" { THROW NEW Exception("boem"); }
    CASE "terminate" { TerminateScript(); }
  }
  ABORT("Unknown abort type '" || type || "'");
}


PUBLIC RECORD FUNCTION RPC_LoginTest()
{
  OBJECT rpcuser := GetEffectiveUser();
  RETURN [ user := ObjectExists(rpcuser) ? rpcuser->entityid : 0 ];
}

PUBLIC RECORD FUNCTION RPC_TestMinimalConfig()
{
  RECORD minimalconfig := CreateMinimalWebserverConfig();
  ReloadPreviousConfig(minimalconfig);
  RECORD fullconfig := DownloadWebserverConfig();
  ReloadPreviousConfig(fullconfig);

  RETURN CELL [ success := TRUE, login := GetEffectiveUser()->login ];
}

PUBLIC RECORD FUNCTION RPC_ManipWebserverSession(STRING action, STRING sessionid, STRING passwd, RECORD newdata)
{
  SWITCH (action)
  {
    CASE "get"    { RETURN GetWebSessionData(sessionid, passwd); }
    CASE "set"    { StoreWebSessionData(sessionid, passwd, newdata); RETURN DEFAULT RECORD;  }
    CASE "close"  { CloseWebSession(sessionid, passwd); RETURN DEFAULT RECORD;  }
  }
  RETURN DEFAULT RECORD;
}
