# DevKit
The devkit module is part of Webhare but not activated inside a docker container (WEBHARE_IN_DOCKER) unless WEBHARE_CI_MODULE or WEBHARE_ENABLE_DEVKIT is set

(this behavior is currently hardcoded based on the module's name)
