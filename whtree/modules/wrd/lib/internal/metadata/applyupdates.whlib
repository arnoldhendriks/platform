<?wh

LOADLIB "wh::util/algorithms.whlib";
LOADLIB "mod::system/lib/commonxml.whlib";
LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/logging.whlib";
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";
LOADLIB "mod::system/lib/resources.whlib";
LOADLIB "mod::wrd/lib/api.whlib";
LOADLIB "mod::wrd/lib/internal/metadata/moduledefs.whlib";
LOADLIB "mod::wrd/lib/internal/metadata/updateschema.whlib";

PUBLIC STRING ARRAY FUNCTION GetWRDSchemaDefCandidates(RECORD ARRAY schemarule)
{
  RECORD ARRAY currentwrdschemas := ListWRDSchemas();
  STRING ARRAY candidates;
  FOREVERY(RECORD rule FROM schemarule)
  {
    STRING ARRAY localcandidates := SELECT AS STRING ARRAY tag
                                      FROM currentwrdschemas
                                     WHERE ToUppercase(tag) LIKE ToUppercase(rule.tag);

    IF(Length(localcandidates) = 0 AND rule.autocreate)
      localcandidates := STRING[rule.tag];

    candidates := ArrayUnion(candidates, localcandidates);
  }
  RETURN candidates;
}

//update a schema.
PUBLIC BOOLEAN FUNCTION UpdateACandidateSchema(STRING candidate, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions(
      [ force :=    FALSE
      , reportupdates := FALSE
      , reportskips := FALSE
      ], options);

  GetPrimary()->BeginWork([ mutex := GetUpdateSchemaLockName(candidate) ]);
  TRY
  {
    OBJECT wrdschema := OpenWRDSchema(candidate);

    IF(ObjectExists(wrdschema))
      wrdschema->ApplySchemaDefinition(options);
    ELSE
    {
      IF(options.reportupdates)
        Print(`Creating schema '${candidate}'\n`);
      CreateWRDSchema(candidate, [ initialize := TRUE ]);
    }

    GetPrimary()->CommitWork();
    RETURN TRUE;
  }
  CATCH(OBJECT e)
  {
    LogHarescriptException(e);
    PrintTo(2, `Failed to apply metadata updates to WRD schema '${candidate}': ${e->what}\n`);
    IF(GetPrimary()->IsWorkOpen()) //if commit throws, no work is open!
      GetPrimary()->RollbackWork();
    RETURN FALSE;
  }
}

PUBLIC BOOLEAN FUNCTION UpdateAllModuleSchemas(RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions([ schemamasks := STRING[]
                             , reportupdates := FALSE
                             , reportskips := FALSE
                             , force := FALSE
                             ], options);

  RECORD ARRAY alldefs;
  FOREVERY(STRING module FROM GetInstalledModuleNames())
    alldefs := alldefs CONCAT GetModuleDefinedWRDSchemas(module);

  BOOLEAN allsuccess := TRUE, anymatch;
  RECORD ARRAY schemas := SELECT DISTINCT tag := ToLowercase(tag)
                              FROM ToRecordArray(GetWRDSchemaDefCandidates(alldefs), 'tag') CONCAT ListWRDSchemas();
  STRING ARRAY candidates := SELECT AS STRING ARRAY tag
                               FROM schemas
                           ORDER BY Tokenize(tag,':')[0] IN VAR whconstant_builtinmodules ? SearchElement(VAR whconstant_builtinmodules, Tokenize(tag,':')[0]) : 999;

  FOREVERY(STRING candidate FROM candidates)
  {
    IF(Length(options.schemamasks) > 0 AND NOT MatchCommonXMLWildcardMasks(candidate, options.schemamasks))
      CONTINUE;

    anymatch := TRUE;
    IF(NOT UpdateACandidateSchema(candidate, CELL[ options.force, options.reportupdates, options.reportskips ]))
      allsuccess := FALSE;
  }
  IF(NOT anymatch AND options.reportupdates)
    PrintTo(2, `No schemas matched the specified masks\n`);

  RETURN allsuccess;
}
