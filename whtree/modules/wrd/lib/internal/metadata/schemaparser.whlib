<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::money.whlib";
LOADLIB "wh::xml/dom.whlib";
LOADLIB "wh::xml/xsd.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/resources.whlib";
LOADLIB "mod::system/lib/internal/typecoder.whlib";
LOADLIB "mod::system/lib/validation.whlib";

LOADLIB "mod::wrd/lib/internal/support.whlib";


//LOADLIB "mod::wrd/lib/internal/schemaupdate.whlib";
STRING ns_schemadef := "http://www.webhare.net/xmlns/wrd/schemadefinition";

RECORD ARRAY FUNCTION GetXMLAttributes(OBJECT parent, STRING currentfile)
{
  RECORD ARRAY attrs;
  FOREVERY(OBJECT attrnode FROM parent->childnodes->GetCurrentElements())
  {
    RECORD attr := [ attributetype := 0
                   , attributetypename := ""
                   , tag := ToUppercase(attrnode->GetAttribute('tag'))
                   , title := attrnode->GetAttribute('title')
                   , description := attrnode->GetAttribute('description')
                   , isrequired := ParseXSBoolean(attrnode->GetAttribute('required'))
                   , isunique := ParseXSBoolean(attrnode->GetAttribute('unique'))
                   , isunsafetocopy := ParseXSBoolean(attrnode->GetAttribute('unsafetocopy'))
                   ];

    IF(attrnode->localname IN ["documentation"])
      CONTINUE;
    IF(attr.tag="")
      THROW NEW Exception("Attribute #" || #attrnode || " of type " || attrnode->localname || " has no tag");

    IF(attrnode->localname != "obsolete")
    {
      attr.attributetype := GetAttributeTypeIdByTypeName(attrnode->localname, FALSE);
      IF (attr.attributetype <= 0)
        THROW NEW Exception(`Unknown attribute type '${attrnode->localname}' for '${attr.tag}'`);
      attr.attributetypename := GetAttributeTypeNameByTypeId(attr.attributetype);

      RECORD descr := DescribeAttributeType(attr.attributetypename);

      IF("domain" IN descr.options)
      {
        INSERT CELL domaintag := attrnode->GetAttribute("domain") INTO attr;
        IF(attr.domaintag="")
          THROW NEW Exception(`Attribute '${attr.tag}' has no domain`);
      }

      INSERT CELL attrs := GetXMLAttributes(attrnode, currentfile) INTO attr;
      INSERT CELL multiline := ParseXSBoolean(attrnode->GetAttribute("multiline")) INTO attr;
      INSERT CELL allowedvalues := ParseXSList(attrnode->GetAttribute("allowedvalues")) INTO attr;

      STRING typedeclaration := attrnode->GetAttribute("typedeclaration");
      IF (typedeclaration LIKE "./*" OR  typedeclaration LIKE "../*")
        typedeclaration := MakeAbsoluteResourcePath(currentfile, typedeclaration);
      INSERT CELL typedeclaration := typedeclaration INTO attr;

      IF(attrnode->HasAttribute("checklinks"))
        INSERT CELL checklinks := ParseXSBoolean(attrnode->GetAttribute("checklinks")) INTO attr;
      ELSE
        INSERT CELL checklinks := ("checklinks" IN descr.defaults) INTO attr;
    }

    INSERT attr INTO attrs AT END;
  }
  RETURN attrs;
}

CONSTANT RECORD basetype :=
  [ tag := ""
  , type := ""
  , title := ""
  , parenttype_tag := ""
  , description := ""
  , deleteclosedafter := 0
  , keephistorydays := 0
  , haspersonaldata := FALSE
  , attrs := RECORD[]
  , allattrs := RECORD[]
  , hasvalues := FALSE
  , vals := RECORD[]
  , valslinenum := -1
  , linkfrom_tag := ""
  , linkto_tag   := ""
  , domvalsyncattr := ""
  , domvalsoverwritefields := STRING[]
  ];

STATIC OBJECTTYPE ParsedSchemaDef
<
  RECORD ARRAY types;
  RECORD ARRAY keyvaluestore;
  RECORD metadata;
  STRING currentfile;
  STRING ARRAY importedfiles;
  RECORD ARRAY migrations;
  RECORD ARRAY resources;

  MACRO AddKeyValue(OBJECT keyvaluenode)
  {
    STRING name := ToUppercase(keyvaluenode->GetAttribute("name"));
    IF (name = "")
      THROW NEW Exception("Keyvalue has no name");

    IF (CellExists(this->keyvaluestore, name))
      THROW NEW Exception("Duplicate keyvalue '" || name || "'");

    RECORD keyvalue := AnyTypeToBlobString(keyvaluenode->__INTERNAL_GetHSValue());
    INSERT CELL[ name, keyvalue ] INTO this->keyvaluestore AT END;
  }

  MACRO AddType(OBJECT typenode)
  {
    STRING which := typenode->localname;
    IF(which = "classification")
      which := "attachment";

    IF(which NOT IN ["object","attachment","link","domain","extend"])
      THROW NEW Exception("Unrecognized object type '" || which || "'");

    STRING typetag := ToUppercase(typenode->GetAttribute("tag"));
    IF(typetag="")
      THROW NEW Exception("Type has no tag");

    INTEGER existingtypepos := (SELECT AS INTEGER #types+1 FROM this->types WHERE ToUppercase(types.tag) = ToUppercase(VAR typetag))-1;
    RECORD type;
    IF(existingtypepos = -1) //ADDING
    {
      type :=
          CELL[ ...basetype
              , tag := ToUppercase(typenode->GetAttribute("tag"))
              , type := ToUppercase(which)
              , title := typenode->GetAttribute("title")
              , parenttype_tag := ToUppercase(typenode->GetAttribute("parent"))
              , description := typenode->GetAttribute("description")
              , deleteclosedafter := ParseXSInt(typenode->GetAttribute("deleteclosedafter"))
              , keephistorydays := ParseXSInt(typenode->GetAttribute("keephistorydays"))
              , haspersonaldata := ParseXSBoolean(typenode->GetAttribute("haspersonaldata"))
              , linkfrom_tag := which IN ["attachment","link"] ? ToUppercase(typenode->GetAttribute("linkfrom")) : ""
              , linkto_tag   := which = "link" ? ToUppercase(typenode->GetAttribute("linkto")) : ""
              ];
      // The WRD_PERSON type is marked as personal by default, unless explicitly set to false
      IF (type.tag = "WRD_PERSON" AND NOT typenode->HasAttribute("haspersonaldata"))
        type.haspersonaldata := TRUE;
    }
    ELSE
    {
      type := this->types[existingtypepos];

      IF(which != "extend" AND type.type != ToUppercase(which))
        THROW NEW Exception("Metatype '" || ToLowercase(this->types[existingtypepos].type) || "' cannot be changed to '" || which || "' at the second declaration of type '" || typetag || "'");

      IF(typenode->HasAttribute("parent"))
        type.parenttype_tag := typenode->GetAttribute("parent");

      IF(typenode->HasAttribute("title"))
        type.title := typenode->GetAttribute("title");

      IF(typenode->HasAttribute("description"))
        type.description := typenode->GetAttribute("description");

      IF(typenode->HasAttribute("haspersonaldata"))
        type.haspersonaldata := ParseXSBoolean(typenode->GetAttribute("haspersonaldata"));

      IF(typenode->HasAttribute("deleteclosedafter"))
        type.deleteclosedafter := ParseXSInt(typenode->GetAttribute("deleteclosedafter"));

      IF(typenode->HasAttribute("keephistorydays"))
        type.keephistorydays := ParseXSInt(typenode->GetAttribute("keephistorydays"));
    }

    OBJECT attributesnode := typenode->GetChildElementsByTagNameNS(ns_schemadef, "attributes")->Item(0);
    IF(ObjectExists(attributesnode))
    {
      type.attrs := type.attrs CONCAT GetXMLAttributes(attributesnode, this->currentfile);
      type.allattrs := type.attrs;
      //FIXME scan for dupes

      IF (type.parenttype_tag != "")
      {
        INTEGER parentpos := (SELECT AS INTEGER #types+1 FROM this->types WHERE ToUppercase(types.tag) = ToUppercase(type.parenttype_tag))-1;
        IF (parentpos != -1)
          type.allattrs := this->types[parentpos].allattrs CONCAT type.allattrs;
      }
    }

    OBJECT valuesnode := typenode->GetChildElementsByTagNameNS(ns_schemadef, "values")->Item(0);
    IF(ObjectExists(valuesnode))
    {
      IF(which NOT IN ["domain","object","attachment"])
        THROW NEW Exception("Unexpected <values> node in " || type.tag);
       IF(type.hasvalues)
         THROW NEW Exception("<values> can currently only be defined once for a type (reading a second <values> for '" || typetag || "')");

      type.hasvalues := TRUE;
      type.valslinenum := valuesnode->linenum;

      type.domvalsyncattr := ToUppercase(valuesnode->GetAttribute("matchattribute"));
      type.domvalsoverwritefields := ParseXSList(ToUppercase(valuesnode->GetAttribute("overwriteattributes")));

      // Overwrite WRD_TAG if it is the match attribute - fixes case sensitivity differences
      IF (type.domvalsyncattr = "WRD_TAG" AND "WRD_TAG" NOT IN type.domvalsoverwritefields)
        INSERT "WRD_TAG" INTO type.domvalsoverwritefields AT END;
      IF (which = "domain" AND "WRD_LEFTENTITY" NOT IN type.domvalsoverwritefields)
        INSERT "WRD_LEFTENTITY" INTO type.domvalsoverwritefields AT END;

      RECORD ARRAY vals;
      FOREVERY(OBJECT valnode FROM valuesnode->childnodes->GetCurrentElements())
      {
        IF(valnode->localname != "value")
          THROW NEW Exception("Expected <value> in <values> node");

        RECORD val := this->ParseEntityValue(valnode, ToString(#valnode), type, type.allattrs);
        INSERT val INTO vals AT END;
      }

      type.vals := vals;
    }

    IF(existingtypepos=-1)
      INSERT type INTO this->types AT END;
    ELSE
      this->types[existingtypepos] := type;
  }

  MACRO ReadMetadata(OBJECT rootnode)
  {
    IF (rootnode->HasAttribute("accounttype"))
      INSERT CELL accounttype := rootnode->GetAttribute("accounttype") INTO this->metadata;
    IF (rootnode->HasAttribute("accountloginfield"))
      INSERT CELL accountloginfield := rootnode->GetAttribute("accountloginfield") INTO this->metadata;
    IF (rootnode->HasAttribute("accountemailfield"))
      INSERT CELL accountemailfield := rootnode->GetAttribute("accountemailfield") INTO this->metadata;
    IF (rootnode->HasAttribute("accountpasswordfield"))
      INSERT CELL accountpasswordfield := rootnode->GetAttribute("accountpasswordfield") INTO this->metadata;
  }

  RECORD FUNCTION ParseEntityValue(OBJECT valnode, STRING nrvalnode, RECORD type, RECORD ARRAY attrs)
  {
    RECORD ARRAY subvalues;
    RECORD val;
    FOREVERY(OBJECT fieldnode FROM valnode->childnodes->GetCurrentElements())
    {
      IF (fieldnode->localname = "subvalues")
      {
        FOREVERY(OBJECT subvalnode FROM fieldnode->childnodes->GetCurrentElements())
        {
          IF(subvalnode->localname != "value")
            THROW NEW Exception("Expected <value> in <values> node");

          RECORD subval := this->ParseEntityValue(subvalnode, nrvalnode || "/" || #subvalnode, type, type.allattrs);
          INSERT subval INTO subvalues AT END;
        }
        CONTINUE;
      }
      ELSE IF (fieldnode->localname NOT IN [ "arrayfield", "field" ])
        THROW NEW Exception("Expected <field> or <arrayfield> in <value> node, got " || fieldnode->localname);

      STRING cellname := ToUppercase(fieldnode->GetAttribute("tag"));
      IF(CellExists(val, cellname))
        THROW NEW Exception("Duplicate field '" || cellname || "' in value #" || nrvalnode || " for type " || type.tag);

      RECORD attr := SELECT * FROM attrs WHERE ToUppercase(tag) = cellname; //FIXME delay parsing until we have all final attributes so we don't have to swtich on 'cellname'
      STRING celltype := RecordExists(attr) ? attr.attributetypename : "";


      IF ((fieldnode->localname = "arrayfield") != (celltype = "ARRAY"))
        THROW NEW Exception(`Wrong field type for '${ cellname }', expected got <${ fieldnode->localname }> for ${ celltype }`);

      IF(cellname IN ["WRD_TAG","WRD_TITLE","WRD_LEFTENTITY","WRD_GUID"] OR celltype IN ["FREE","DOMAIN","EMAIL","PASSWORD","TELEPHONE","ENUM"])
      {
        val := CellInsert(val, cellname, fieldnode->textcontent);
        IF (cellname = "WRD_TAG" AND (fieldnode->childrentext LIKE "* *"))
          THROW NEW Exception(`Found whitespace in the value for a WRD_TAG field for type ${ type.tag }: '${ fieldnode->childrentext }'`);
        IF (cellname = "WRD_TAG" AND (ToUppercase(fieldnode->childrentext) != fieldnode->childrentext))
          THROW NEW Exception(`Found a non-uppercase value for a WRD_TAG field for type ${ type.tag }: '${ fieldnode->childrentext }'`);
      }
      ELSE IF(cellname = "WRD_LIMITDATE")
      {
        IF(fieldnode->childrentext = "MAX_DATETIME")
          val := CellInsert(val, cellname, MAX_DATETIME);
        ELSE
          THROW NEW Exception(`Unsupported MAX_DATETIME value '${fieldnode->childrentext}'`);
      }
      ELSE IF(celltype IN ["INTEGER"] OR cellname IN ["WRD_ORDERING"])
      {
        val := CellInsert(val, cellname, ToInteger(fieldnode->textcontent,0));
      }
      ELSE IF(celltype IN ["INTEGER64"])
      {
        val := CellInsert(val, cellname, ToInteger64(fieldnode->textcontent,0));
      }
      ELSE IF(celltype IN ["BOOLEAN"])
      {
        val := CellInsert(val, cellname, ToUppercase(fieldnode->textcontent) IN ["TRUE","1"]);
      }
      ELSE IF(celltype IN ["MONEY"])
      {
        val := CellInsert(val, cellname, ToMoney(fieldnode->textcontent, 0m));
      }
      ELSE IF(celltype IN ["DOMAINARRAY","ENUMARRAY"])
      {
        val := CellInsert(val, cellname, Tokenize(fieldnode->textcontent, " "));
      }
      ELSE IF (celltype = "ARRAY")
      {
        RECORD ARRAY vals;
        FOREVERY(OBJECT subvalnode FROM fieldnode->childnodes->GetCurrentElements())
        {
          IF (subvalnode->localname != "element")
            THROW NEW Exception("Expected <element> in <arrayfield> node");

          INSERT this->ParseEntityValue(subvalnode, nrvalnode || "/" || #subvalnode, type, attr.attrs) INTO vals AT END;
        }
        val := CellInsert(val, cellname, vals);
      }
      ELSE IF(celltype="")
      {
        THROW NEW Exception("Undefined value cell '" || cellname || "'");
      }
      ELSE
      {
        THROW NEW Exception("Unimplemented value type '" || attr.attributetypename || "' for '" || cellname || "'");
      }
    }

    IF (LENGTH(subvalues) != 0)
      INSERT CELL __subvalues := subvalues INTO val;

    RETURN val;
  }

  PUBLIC MACRO ImportFile(STRING filetoimport)
  {
    IF (filetoimport IN this->importedfiles)
      RETURN;

    INSERT filetoimport INTO this->importedfiles AT END;

    RECORD rec := RetrieveWebHareResource(filetoimport);
    this->ReadSchemaDef(filetoimport, rec.data, rec.modified);
  }

  PUBLIC MACRO ReadSchemaDef(STRING filename, BLOB data, DATETIME modtime)
  {
    RECORD validateres := ValidateSingleFile(filename, [ overridedata := data ]);
    IF(Length(validateres.errors) > 0)
      THROW NEW Exception(`Cannot apply schemadefinition: ${FormatValidationError(validateres.errors[0])}`);

    INSERT CELL[ resourcename := filename, modified := modtime ] INTO this->resources AT END;
    STRING oldcurrent := this->currentfile;
    this->currentfile := filename;

    OBJECT el := MakeXMLDocument(data)->documentelement;
    FOREVERY(OBJECT node FROM el->childnodes->GetCurrentElements())
    {
      IF(node->localname="import")
      {
        // Import a file only once
        STRING filetoimport := node->GetAttribute("definitionfile");
        filetoimport := MakeAbsoluteResourcePath(this->currentfile, filetoimport);
        this->ImportFile(filetoimport);
      }
      ELSE IF(node->localname="migration")
      {
        IF(NOT IsNodeApplicableToThisWebHare(node))
          CONTINUE;

        INSERT CELL [ tag := node->GetAttribute("tag")
                    , updatefunction := MakeAbsoluteResourcePath(this->currentfile, node->GetAttribute("updatefunction"))
                    , stage := node->GetAttribute("stage") ?? "beforeTypes"
                    , revision := ParseXSInt(node->GetAttribute("revision"))
                    ] INTO this->migrations AT END;
      }
      ELSE IF(node->localname="keyvalues")
      {
        FOREVERY(OBJECT keyvalue FROM node->childnodes->GetCurrentElements())
          this->AddKeyValue(keyvalue);
      }
      ELSE
      {
        this->AddType(node);
      }
    }

    this->ReadMetadata(el);
    this->currentfile := oldcurrent;
  }
  PUBLIC RECORD FUNCTION GetResult()
  {
    RETURN
        CELL[ this->types
            , this->metadata
            , this->keyvaluestore
            , this->migrations
            , schemaresources := CELL[ resources := (SELECT * FROM this->resources ORDER BY resourcename)
                                     , version := current_schema_version
                                     ]
            ];
  }
>;

PUBLIC RECORD FUNCTION OpenWRDSchemaDefFile(STRING resourcepath, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions([ overridedata := DEFAULT BLOB
                             , addbaseschema := TRUE
                             ], options, [ optional := ["overridedata"]]);

  BLOB contents;
  DATETIME modtime;
  IF(CellExists(options, 'overridedata'))
  {
    contents := options.overridedata;
  }
  ELSE
  {
    RECORD rec := RetrieveWebHareResource(resourcepath);
    contents := rec.data;
    modtime := rec.modified;
  }

  OBJECT parser := NEW ParsedSchemaDef;
  IF(options.addbaseschema AND resourcepath != wrd_baseschemaresource)
    parser->ImportFile(wrd_baseschemaresource);
  parser->ReadSchemaDef(resourcepath, contents, modtime);

  RECORD res := parser->GetResult();
  //only associate with the schemaresource if it was actually used
  IF(NOT CellExists(options,'overridedata'))
    INSERT CELL schemaresource := resourcepath INTO res.schemaresources;
  RETURN res;
}
