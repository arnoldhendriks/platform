﻿<?wh

LOADLIB "wh::util/localization.whlib";

PUBLIC CONSTANT STRING ARRAY default_addressfield_options := ["nl-zip-suggest"];
PUBLIC CONSTANT STRING ARRAY valid_addressfield_options := ["nl-zip-suggest","nl-zip-force"];

PUBLIC CONSTANT STRING ARRAY stored_address_fields := ["country","street","nr_detail","zip","state","city"];

/** @short Parse a rawdata address
    @long The address is stored in java-encoded, linefeed separated, value=data pairs */
RECORD FUNCTION ParseAddressRawdata(STRING data)
{
  STRING ARRAY curaddresstoks:=Tokenize(data,'\n');
  RECORD retdata := CELL[];
  FOREVERY(STRING tok FROM curaddresstoks)
  {
    INTEGER equals := SearchSubstring(tok,'=');
    IF(equals<1)
      CONTINUE;

    STRING fieldname := Left(tok,equals);
    STRING fielddata := Substring(tok,equals+1,Length(tok));
    IF (NOT CellExists(retdata, fieldname))
      retdata := CellInsert(retdata,DecodeJava(fieldname),DecodeJava(fielddata));
  }
  RETURN retdata;
}

PUBLIC STRING FUNCTION MapToCountryName(STRING countrycode, STRING language)
{
  RETURN SELECT AS STRING GetCell(countries, language)
           FROM GetCountryList(language) AS countries
          WHERE code = VAR countrycode;
}

PUBLIC STRING FUNCTION EncodeWRDAddress(RECORD indata, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := CELL[ importmode := FALSE, ...options ];
  IF(NOT RecordExists(indata))
    RETURN "";
  IF((NOT CellExists(indata,'country') OR Length(indata.country) != 2) AND NOT options.importmode)
  {
    THROW NEW Exception(`The field 'country' is required in an address and must be a 2 character code`);
  }
  IF(CellExists(indata,'housenumber'))
    THROW NEW Exception(`Received address with 'houseNumber' field. This may be JS code attempting to talk directly with a HS API without renaming it to 'nr_detail' (and also not doing a recursive snake case conversion)`);
  IF(CellExists(indata,'house_number'))
    THROW NEW Exception(`Received address with 'house_number' field. This may be JS code attempting to talk directly with a HS API without renaming it to 'nr_detail'`);
  IF(CellExists(indata,'nrdetail'))
    THROW NEW Exception(`Received address with 'nrDetail' field. This may be JS code attempting to talk with a HS API without doing a recursive snake case conversion`);

  indata := PickCells(indata, stored_address_fields);
  indata.country := ToUppercase(indata.country);
  FOREVERY(STRING field FROM stored_address_fields)
    IF(CellExists(indata, field) AND GetCell(indata, field) = "")
      indata := CellDelete(indata, field);

  RETURN EncodeJSON(indata);
}

PUBLIC RECORD FUNCTION GenerateAddress(STRING rawvalue, STRING currentlanguage, INTEGER wrd_schema, BOOLEAN with_country_full)
{
  //The WH5.4 format
  RECORD finaladdress :=
    CELL[ country :=        ""
        , street :=         ""
        , locationdetail := ""   //we'll never set locationdetail anymore but we'll keep it for backwards compatibility
        , nr_detail :=      ""
        , zip :=            ""
        , city :=           ""
        , state :=          ""
        ];

  IF(rawvalue LIKE '{*') //It's JSON!
  {
    finaladdress := CELL[ ...finaladdress, ...PickCells(DecodeJSON(rawvalue),stored_address_fields) ];
  }
  ELSE //Convert pre WH5.4 address
  {
    RECORD originaladdress := ParseAddressRawdata(rawvalue);

    RECORD address := originaladdress;
    FOREVERY(RECORD field FROM UnpackRecord(address))
      IF(TrimWhitespace(field.value) IN ["","-"]) //invalid or dummy values
        address := CellDelete(address,field.name);

    FOREVERY(STRING straightcopy FROM ["country","street","nr_detail","zip","city"])
      IF(CellExists(address,straightcopy))
        finaladdress := CellUpdate(finaladdress, straightcopy, GetCell(address, straightcopy));

    IF(NOT CellExists(address,'country') OR Length(address.country) != 2)
      RETURN DEFAULT RECORD; //broken value

    IF(CellExists(address, "province"))
    {
      IF(finaladdress.state = "")
        finaladdress.state := address.province;
    }

    IF(CellExists(address, "district"))
    {
      IF(finaladdress.city = "")
        finaladdress.city := address.district;
      ELSE
        finaladdress.city := finaladdress.city || ", " || address.district;
    }

    DELETE CELL country,street,nr_detail,zip,city,country_full,province,state,district FROM address;

    FOREVERY(STRING prefix_to_street FROM ["colony","locationdetail2","locationdetail"])
      IF(CellExists(address,prefix_to_street))
      {
        finaladdress.street := TrimWhitespace(GetCell(address,prefix_to_street) || "\n" || finaladdress.street);
        address := CellDelete(address, prefix_to_street);
      }

    FOREVERY(RECORD remainingfield FROM UnpackRecord(address))
      {
        finaladdress.street := TrimWhitespace(remainingfield.value || "\n" || finaladdress.street);
        address := CellDelete(address, remainingfield.name);
      }
  }

  finaladdress.country := ToUppercase(finaladdress.country);

  IF (with_country_full) //only used by pre WRD2017 API
    finaladdress := CELL[...finaladdress, country_full := MapToCountryName(finaladdress.country, currentlanguage) ];

  RETURN finaladdress;
}
