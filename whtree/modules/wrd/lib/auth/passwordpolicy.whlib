<?wh

/** @short WRD authentication
    @topic wrdauth/api
*/

LOADLIB "wh::adhoccache.whlib";
LOADLIB "wh::crypto.whlib";
LOADLIB "wh::datetime.whlib";
LOADLIB "wh::files.whlib";
LOADLIB "wh::internet/webbrowser.whlib";
LOADLIB "wh::util/stringparser.whlib";

LOADLIB "mod::tollium/lib/gettid.whlib";

LOADLIB "mod::wrd/lib/internal/auth/support.whlib";
LOADLIB "mod::wrd/lib/internal/wrdsettings.whlib";


OBJECT pwnedbrowser;

/** Queries the haveibeenpwned (HIBP) service for the breach count of a password
    @param pwd Password to query
    @return Cacheable record
    @cell return.value Breach count
    @cell return.ttl Cache ttl
*/
RECORD FUNCTION GetCacheablePwnCount(STRING pwd)
{
  IF(NOT Objectexists(pwnedbrowser))
  {
    pwnedbrowser := NEW WebBrowser;
    pwnedbrowser->timeout := 3000; //3 secs is more than enough
  }

  STRING pwdhash := ToUppercase(EncodeBase16(GetSHA1Hash(pwd)));

  INTEGER numbreaches := -1;
  IF(pwnedbrowser->GotoWebPage(`https://api.pwnedpasswords.com/range/${Left(pwdhash,5)}`))
  {
    STRING data := BlobToString(pwnedbrowser->content);
    INTEGER hashpos := SearchSubstring(data, Substring(pwdhash,5));
    IF(hashpos = -1)
    {
      numbreaches := 0;
    }
    ELSE
    {
      INTEGER countstart := hashpos + 36; //35 remaining chars plus :
      INTEGER countend := SearchSubstring(data, '\n', countstart);
      numbreaches := ToInteger(Substring(data, countstart, countend - countstart - 1), -1);
    }
  }
  RETURN [ value := numbreaches
         , ttl := 30*60*1000
         ];
}

/** Queries the haveibeenpwned (HIBP) service for the breach count of a password
    @param pwd Password to query
    @return Breach count (cached for up to 30 minutes)
*/
PUBLIC INTEGER FUNCTION GetPasswordBreachCount(STRING pwd)
{
  RETURN GetAdhocCached(CELL[pwd], PTR GetCacheablePwnCount(pwd));
}

INTEGER FUNCTION GetCountOfSet(STRING text, STRING chars)
{
  OBJECT p := NEW StringParser(text);
  STRING matches;
  WHILE (NOT p->eof)
  {
    matches := matches || p->ParseWhileInSet(chars);
    p->ParseWhileNotInSet(chars);
  }
  RETURN LENGTH(matches);
}

/** @param checks Password validation checks. Space-separated list of checks. Possible checks:
    - hibp Check that the password isn't present in the "Have I Been Pwned" database
    - minlength:(amount) Make sure that password has at least (amount) characters
    - lowercase:(amount) Make sure that password has at least (amount) lowercase characters
    - uppercase:(amount) Make sure that password has at least (amount) uppercase characters
    - digits:(amount) Make sure that password has at least (amount) digits
    - symbols:(amount) Make sure that password has at least (amount) symbols
    @cell options.strict Throw on errors, defaults to FALSE
    @return Parsed list of checks
    @cell(string) return.check Token of the check ("hibp", "minlength", "lowercase", "uppercase", "digits", "symbols", "maxage", "noreuse")
    @cell(integer) return.value Amount (for checks that have an amount).
    @cell(string) return.duration Duration string (for checks that have an duration).
*/
PUBLIC RECORD ARRAY FUNCTION ParsePasswordChecks(STRING checks, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions(
      [ strict :=   FALSE
      ], options);

  RECORD ARRAY retval;

  FOREVERY (STRING token FROM Tokenize(checks, " "))
  {
    TRY
    {
      STRING ARRAY parts := Tokenize(token, ":");
      IF (LENGTH(parts) != (parts[0] IN [ "", "hibp", "require2fa" ] ? 1 : 2))
        THROW NEW Exception(`Password check '${token}' has a syntax error`);

      VARIANT value := LENGTH(parts) = 2 ? ToInteger(parts[1], -1) : 0;
      STRING duration;
      IF (parts[0] IN [ "maxage", "noreuse"])
      {
        TRY
          ParseDuration(parts[1]);
        CATCH
          THROW NEW Exception(`Password check '${token}' has an invalid duration`);
        value := 0;
        duration := parts[1];
      }
      ELSE IF (value < 0)
        THROW NEW Exception(`Password check '${token}' has an invalid count`);

      SWITCH (parts[0])
      {
        CASE ""       { CONTINUE; }
        CASE "hibp", "minlength", "lowercase", "uppercase", "digits", "symbols", "maxage", "noreuse", "require2fa"
        {
          INSERT CELL[ check := parts[0], value, duration ] INTO retval AT END;
        }
        DEFAULT
        {
          THROW NEW Exception(`No such password check '${parts[0]}'`);
        }
      }
    }
    CATCH
    {
      IF (options.strict)
        THROW;
      CONTINUE;
    }
  }

  // Return in fixed order
  RETURN
      SELECT *
        FROM retval
    ORDER BY SearchElement([ "hibp", "minlength", "lowercase", "uppercase", "digits", "symbols", "maxage", "noreuse", "require2fa" ], check);
}

STRING FUNCTION GetRequirementTid(RECORD check)
{
  SWITCH (check.check)
  {
    CASE "hibp"       { RETURN GetTid("wrd:site.forms.authpages.passwordcheck.requirements.hibp"); }
    CASE "minlength"  { RETURN GetTid("wrd:site.forms.authpages.passwordcheck.requirements.minlength", ToString(check.value)); }
    CASE "lowercase"  { RETURN GetTid("wrd:site.forms.authpages.passwordcheck.requirements.lowercase", ToString(check.value)); }
    CASE "uppercase"  { RETURN GetTid("wrd:site.forms.authpages.passwordcheck.requirements.uppercase", ToString(check.value)); }
    CASE "digits"     { RETURN GetTid("wrd:site.forms.authpages.passwordcheck.requirements.digits", ToString(check.value)); }
    CASE "symbols"    { RETURN GetTid("wrd:site.forms.authpages.passwordcheck.requirements.symbols", ToString(check.value)); }
    CASE "maxage"     { RETURN GetTid("wrd:site.forms.authpages.passwordcheck.requirements.maxage", GetDurationTitle(check.duration)); }
    CASE "noreuse"    { RETURN GetTid("wrd:site.forms.authpages.passwordcheck.requirements.noreuse", GetDurationTitle(check.duration)); }
    CASE "require2fa" { RETURN GetTid("wrd:site.forms.authpages.passwordcheck.requirements.require2fa"); }
  }
  THROW NEW Exception(`No tids for check ${check.check}`);
}

PUBLIC STRING FUNCTION DescribePasswordChecks(STRING checks)
{
  RECORD ARRAY parsedchecks := ParsePasswordChecks(checks);
  IF (IsDefaultValue(parsedchecks))
    RETURN "";

  STRING ARRAY lines;
  FOREVERY (RECORD check FROM parsedchecks)
    INSERT `- ${GetRequirementTid(check)}` INTO lines AT END;

  RETURN GetTid("wrd:site.forms.authpages.passwordcheck.requirements", Detokenize(lines, "\n"));
}

PUBLIC DATETIME FUNCTION GetPasswordMinValidFrom(STRING duration, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions(
      [ now := DEFAULT DATETIME
      ], options);

  RETURN SubtractUnpackedDurationToDate(ParseDuration(duration), options.now ?? GetCurrentDateTime());
}

/** Checks if a password complies with password checks
    @param checks String with password checks (eg "minlength:12 lowercase:8")
    @param newpassword (New) password to check
    @cell(record) options.authenticationsettings Current authentication settings, for checking re-use. Omit to
       disable check for re-use.
    @return Check result
    @cell(boolean) return.success TRUE if password complies with all checks
    @cell(string) return.message Set when message does not comply with the checks
    @cell(string array) return.failedchecks List of checks that failed. See [this](#ParsePasswordChecks.return.check) for the values.
*/
PUBLIC RECORD FUNCTION CheckPassword(STRING checks, STRING newpassword, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions(
      [ authenticationsettings :=   DEFAULT RECORD
      , isexisting :=               FALSE
      ], options);

  options.authenticationsettings := options.authenticationsettings ?? GetDefaultAuthenticationSettings();

  RECORD ARRAY failed;

  FOREVERY (RECORD check FROM ParsePasswordChecks(checks))
  {
    SWITCH (check.check)
    {
      CASE "hibp"
      {
        INTEGER breachcount := GetPasswordBreachCount(newpassword);
        IF (breachcount > 0) //may return -1 on HIBP connectivity error. best to let it pass and check it next time then
          INSERT check INTO failed AT END;
      }
      CASE "minlength"
      {
        IF (UCLength(newpassword) < check.value)
          INSERT check INTO failed AT END;
      }
      CASE "lowercase"
      {
        IF (GetCountOfSet(newpassword, "abcdefghijklmnopqrstuvwxyz") < check.value)
          INSERT check INTO failed AT END;
      }
      CASE "uppercase"
      {
        IF (GetCountOfSet(newpassword, "ABCDEFGHIJKLMNOPQRSTUVWXYZ") < check.value)
          INSERT check INTO failed AT END;
      }
      CASE "digits"
      {
        IF (GetCountOfSet(newpassword, "0123456789") < check.value)
          INSERT check INTO failed AT END;
      }
      CASE "symbols"
      {
        IF (UCLength(newpassword) - GetCountOfSet(newpassword, "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ") < check.value)
          INSERT check INTO failed AT END;
      }
      CASE "noreuse"
      {
        IF (RecordExists(options.authenticationsettings) AND NOT options.isexisting)
        {
          DATETIME cutoff := GetPasswordMinValidFrom(check.duration);
          FOR (INTEGER i := LENGTH(options.authenticationsettings.passwords) - 1; i >= 0; i := i - 1)
          {
            IF (VerifyWebHarePasswordHash(newpassword, options.authenticationsettings.passwords[i].passwordhash))
            {
              INSERT check INTO failed AT END;
              BREAK;
            }
            IF (options.authenticationsettings.passwords[i].validfrom <= cutoff)
              BREAK;
          }
        }
      }
      CASE "maxage"
      {
        IF (RecordExists(options.authenticationsettings) AND options.isexisting)
        {
          DATETIME cutoff := GetPasswordMinValidFrom(check.duration);
          IF (IsValueSet(options.authenticationsettings.passwords)
              AND options.authenticationsettings.passwords[END-1].passwordhash NOT IN [ "", "*" ]
              AND options.authenticationsettings.passwords[END-1].validfrom < cutoff)
          {
            INSERT check INTO failed AT END;
          }
        }
      }
      CASE "require2fa"
      {
        IF (RecordExists(options.authenticationsettings) AND options.isexisting AND NOT RecordExists(options.authenticationsettings.totp))
          INSERT check INTO failed AT END;
      }
      DEFAULT
      {
        THROW NEW Exception(`No such password check '${check.check}'`);
      }
    }
  }

  IF (RecordExists(failed))
  {
    STRING message;
    IF (RecordExists(SELECT FROM failed WHERE check != "hibp"))
    {
      STRING ARRAY lines;
      FOREVERY (RECORD check FROM failed)
        INSERT `- ${GetRequirementTid(check)}` INTO lines AT END;

      message := GetTid("wrd:site.forms.authpages.passwordcheck.failure", Detokenize(lines, "\n"));
    }
    ELSE
      message := GetTid("wrd:site.forms.authpages.passwordcheck.foundinhibp");

    RETURN CELL
        [ success :=        FALSE
        , message
        , failedchecks :=   (SELECT AS STRING ARRAY check FROM failed)
        ];
  }

  RETURN
      [ success :=        TRUE
      , message :=        ""
      , failedchecks :=   STRING[]
      ];
}

/** Checks if authentication settings complu with password checks
    @param checks String with password checks (eg "minlength:12 lowercase:8")
    @param authenticationsettings Current authentication settings
    @return Check result
    @cell(boolean) return.success TRUE if password complies with all checks
    @cell(string) return.message Set when message does not comply with the checks
    @cell(string array) return.failedchecks List of checks that failed. See [this](#ParsePasswordChecks.return.check) for the values.
*/
PUBLIC RECORD FUNCTION CheckAuthenticationSettings(STRING checks, RECORD authenticationsettings)
{
  authenticationsettings := authenticationsettings ?? GetDefaultAuthenticationSettings();

  RECORD ARRAY failed;
  FOREVERY (RECORD check FROM ParsePasswordChecks(checks))
  {
    SWITCH (check.check)
    {
      CASE "maxage"
      {
        DATETIME cutoff := GetPasswordMinValidFrom(check.duration);
        IF (IsValueSet(authenticationsettings.passwords)
            AND authenticationsettings.passwords[END-1].passwordhash NOT IN [ "", "*" ]
            AND authenticationsettings.passwords[END-1].validfrom < cutoff)
        {
          INSERT check INTO failed AT END;
        }
      }
      CASE "require2fa"
      {
        IF (RecordExists(authenticationsettings) AND NOT RecordExists(authenticationsettings.totp))
          INSERT check INTO failed AT END;
      }
    }
  }

  IF (RecordExists(failed))
  {
    STRING ARRAY lines;
    FOREVERY (RECORD check FROM failed)
      INSERT `- ${GetRequirementTid(check)}` INTO lines AT END;

    STRING message := GetTid("wrd:site.forms.authpages.passwordcheck.settingsfailure", Detokenize(lines, "\n"));

    RETURN CELL
        [ success :=        FALSE
        , message
        , failedchecks :=   (SELECT AS STRING ARRAY check FROM failed)
        ];
  }

  RETURN
      [ success :=        TRUE
      , message :=        ""
      , failedchecks :=   STRING[]
      ];
}

/** Returns the default WRD schema password policy
    @param(object %WRDSchema2017) wrdschema WRD schema
    @return Password policy
    @cell(string) return.passwordvalidationchecks Password validation checks
*/
PUBLIC RECORD FUNCTION GetDefaultWRDSchemaPasswordPolicy(OBJECT wrdschema)
{
  RECORD retval :=
      [ passwordvalidationchecks := ""
      ];

  IF (ObjectExists(wrdschema))
    retval.passwordvalidationchecks := GetWRDSetting(wrdschema, "password_validation_checks");

  RETURN retval;
}
