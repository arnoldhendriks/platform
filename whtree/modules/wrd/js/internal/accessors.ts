import { WRDBaseAttributeTypeId, WRDAttributeTypeId, type AllowedFilterConditions, type WRDAttrBase, WRDGender, type WRDInsertable, type GetResultType, type SimpleWRDAttributeType, baseAttrCells } from "./types";
import type { AttrRec, EntityPartialRec, EntitySettingsRec, EntitySettingsWHFSLinkRec } from "./db";
import { sql, type SelectQueryBuilder, type ExpressionBuilder, type RawBuilder, type Expression, type SqlBool, type Updateable } from "kysely";
import type { PlatformDB } from "@mod-platform/generated/whdb/platform";
import { compare, type ComparableType, recordLowerBound, recordUpperBound } from "@webhare/hscompat/algorithms";
import { isLike } from "@webhare/hscompat/strings";
import { Money, omit, isValidEmail, type AddressValue, isValidUrl, isDate, toCLocaleUppercase, regExpFromWildcards, stringify, parseTyped, isValidUUID } from "@webhare/std";
import { addMissingScanData, decodeScanData, ResourceDescriptor } from "@webhare/services/src/descriptor";
import { encodeHSON, decodeHSON, dateToParts, defaultDateTime, makeDateFromParts, maxDateTime, exportAsHareScriptRTD, buildRTDFromHareScriptRTD } from "@webhare/hscompat";
import type { IPCMarshallableData, IPCMarshallableRecord } from "@webhare/hscompat/hson";
import { maxDateTimeTotalMsecs } from "@webhare/hscompat/datetime";
import { isValidWRDTag } from "@webhare/wrd/src/wrdsupport";
import { uploadBlob } from "@webhare/whdb";
import { WebHareBlob, type RichTextDocument, IntExtLink } from "@webhare/services";
import { wrdSettingId } from "@webhare/services/src/symbols";
import { AuthenticationSettings } from "@webhare/wrd/src/auth";
import type { ValueQueryChecker } from "./checker";
import { getRTDFromWHFS, storeRTDinWHFS } from "@webhare/wrd/src/wrd-whfs";
import { isPromise } from "node:util/types";

/** Response type for addToQuery. Null to signal the added condition is always false
 * @typeParam O - Kysely selection map for wrd.entities (third parameter for `SelectQueryBuilder<PlatformDB, "wrd.entities", O>`)
 */
type AddToQueryResponse<O> = {
  needaftercheck: boolean;
  query: SelectQueryBuilder<PlatformDB, "wrd.entities", O>;
} | null;

/// Returns `null` if Required might be false
type NullIfNotRequired<Required extends boolean> = false extends Required ? null : never;

/** Allowed values for wrd.entity_settings_whfslink.linktype */
export const LinkTypes = { RTD: 0, Instance: 1, FSObject: 2 } as const;

/// Single settings record
export type EncodedSetting = Updateable<PlatformDB["wrd.entity_settings"]> & {
  id?: number;
  attribute: number;
  sub?: EncodedSetting[];
  ///If we also need to encode a link in the WHFS/WRD link table
  linktype?: typeof LinkTypes[keyof typeof LinkTypes];
  link?: number;
};

export type AwaitableEncodedSetting = Updateable<PlatformDB["wrd.entity_settings"]> & {
  id?: number;
  attribute: number;
  sub?: Array<AwaitableEncodedSetting | Promise<EncodedSetting[]>>;
  ///If we also need to encode a link in the WHFS/WRD link table
  linktype?: typeof LinkTypes[keyof typeof LinkTypes];
  link?: number;
};
/// All values needed for an field update
export type EncodedValue = {
  entity?: EntityPartialRec;
  settings?: EncodedSetting | EncodedSetting[];
};
export type AwaitableEncodedValue = {
  entity?: EntityPartialRec;
  settings?: AwaitableEncodedSetting | AwaitableEncodedSetting[] | Promise<EncodedSetting[]>;
};

export function encodeWRDGuid(guid: Buffer) {
  if (guid.length !== 16)
    throw new Error(`Input to encodeWRDGuid is not a raw guid`);

  const guidhex = guid.toString("hex");
  return `${guidhex.substring(0, 8)}-${guidhex.substring(8, 12)}-${guidhex.substring(12, 16)}-${guidhex.substring(16, 20)}-${guidhex.substring(20)}`;

}
export function decodeWRDGuid(wrdGuid: string) {
  if (!isValidUUID(wrdGuid))
    throw new Error("Invalid wrdGuid: " + wrdGuid);

  return Buffer.from(wrdGuid.replaceAll('-', ''), "hex");
}

/** Base for an attribute accessor
 * @typeParam In - Type for allowed values for insert and update
 * @typeParam Out - Type returned by queries
 * @typeParam Default - Output type plus default type (output may not include the default value for eg required domains, where `null` is the default)
 */
export abstract class WRDAttributeValueBase<In, Default, Out extends Default, C extends { condition: AllowedFilterConditions; value: unknown }> {
  attr: AttrRec;
  constructor(attr: AttrRec) {
    this.attr = attr;
  }

  /** Returns the default value for a value with no settings
   *  @returns Default value for this type
   */
  abstract getDefaultValue(): Default;

  /** Returns true if the value is not the default value
   * @param value - Value to check
   * @returns true if the value is not the default value
   */
  abstract isSet(value: Default): boolean;

  /** Checks if a filter (condition + value) is allowed for this attribute. Throws if not.
   * @param condition - Condition type
   * @param value - Condition value
   */
  abstract checkFilter({ condition, value }: C): void;

  /** Checks if a value matches a filter
   * @param value - Value to check
   * @param cv - Condition and value
   * @returns true if the value matches
   */
  abstract matchesValue(value: Default, cv: C): boolean;

  /** Try to add wheres to the database query on wrd.entities to filter out non-matches for this filter
   * @typeParam O - Output map for the database query
   * @param query - Database query
   * @param cv - Condition and value to compare with
   * @returns Whether after-filtering is necessary and updated query
   */
  addToQuery<O>(query: SelectQueryBuilder<PlatformDB, "wrd.entities", O>, cv: C): AddToQueryResponse<O> {
    return { needaftercheck: true, query };
  }

  /** Returns true all the values in a filter match the default value
   * @param cv - Condition+value to check
   * @returns true if all values match the default value
   */
  containsOnlyDefaultValues<CE extends C>(cv: CE): boolean {
    const defaultvalue = this.getDefaultValue();
    if (Array.isArray(cv.value)) {
      for (const value of cv.value) {
        const newcv = { condition: "=", value };
        if (!this.matchesValue(defaultvalue, newcv as C))
          return false;
      }
      return true;
    } else if (cv.condition === "=")
      return this.matchesValue(defaultvalue, cv);
    else
      throw new Error(`Cannot handle condition ${cv.condition} in containsOnlyDefaultValues`);
  }

  /** Given a list of entity settings, extract the return value for a field
   * @param entity_settings - List of entity settings
   * @param settings_start - Position where settings for this attribute start
   * @param settings_limit - Limit of setting for this attribute, is always greater than settings_start
   * @param links - Entity settings whfs links, sorted on id
   * @param cc - Creationdate unified cache validator
   */
  abstract getFromRecord(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number, links: EntitySettingsWHFSLinkRec[], cc: number): Out | Promise<Out>;

  /** Given a list of entity settings, extract the return value for a field
   * @param entity_settings - List of entity settings
   * @param settings_start - Position where settings for this attribute start
   * @param settings_limit - Limit of setting for this attribute, may be the same as settings_start
   * @param row - Entity record
   * @param links - Entity settings whfs links, sorted on id
   * @param cc - Creationdate unified cache validator
   * @returns The parsed value. The return type of this function is used to determine the selection output type for a attribute.
   */
  getValue(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number, row: EntityPartialRec, links: EntitySettingsWHFSLinkRec[], cc: number): Out | Promise<Out> {
    if (settings_limit <= settings_start)
      return this.getDefaultValue() as Out; // Cast is needed because for required fields, Out may not extend Default.
    else
      return this.getFromRecord(entity_settings, settings_start, settings_limit, links, cc);
  }

  /** Check the contents of a value used to insert or update a value
   * @param value - The value to check. The type of this value is used to determine which type is accepted in an insert or update.
   */
  abstract validateInput(value: In, checker: ValueQueryChecker, attrPath: string): void;

  /** Returns the list of attributes that need to be fetched */
  getAttrIds(): number | number[] {
    return this.attr.id || [];
  }

  getAttrBaseCells(): null | keyof EntityPartialRec | ReadonlyArray<keyof EntityPartialRec> {
    return null;
  }

  abstract encodeValue(value: In | null): AwaitableEncodedValue; //explicitly add | null so derived classes have to handle it

  protected decodeAsStringWithOverlow(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number): string {
    if (entity_settings[settings_start].rawdata)
      return entity_settings[settings_start].rawdata;
    const buf = entity_settings[settings_start].blobdata?.__getAsSyncUInt8Array();
    return buf ? Buffer.from(buf).toString() : "";
  }

  protected encodeAsStringWithOverlow(rawdata: string): AwaitableEncodedValue {
    if (!rawdata)
      return {};
    if (Buffer.byteLength(rawdata) <= 4096)
      return { settings: { rawdata, attribute: this.attr.id } };

    return {
      settings: (async (): Promise<EncodedSetting[]> => {
        const blobdata = WebHareBlob.from(rawdata);
        await uploadBlob(blobdata);
        return [{ blobdata, attribute: this.attr.id }];
      })()
    };
  }
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type AnyWRDAccessor = WRDAttributeValueBase<any, any, any, any>;

/** Compare values */
function cmp<T extends ComparableType>(a: T, condition: "=" | ">=" | ">" | "!=" | "<" | "<=", b: T) {
  const cmpres = compare(a, b);
  switch (condition) {
    case "=": return cmpres === 0;
    case ">=": return cmpres >= 0;
    case "<=": return cmpres <= 0;
    case "<": return cmpres < 0;
    case ">": return cmpres > 0;
    case "!=": return cmpres !== 0;
  }
}

type SettingsSelectBuilder = SelectQueryBuilder<PlatformDB, "wrd.entities" | "wrd.entity_settings", { id: number }>;
type SettingsExpressionBuilder = ExpressionBuilder<PlatformDB, "wrd.entities" | "wrd.entity_settings">;

/** Returns a subquery over wrd.entity_settings on a wrd.entities where, joined on the entity id
 * @param qb - Query over wrd.entities
 * @returns Subquery over wrd.entity_settings, with the column `id` already selected.
*/
function getSettingsSelect(qb: ExpressionBuilder<PlatformDB, "wrd.entities">, attr: number): SettingsSelectBuilder {
  return qb
    .selectFrom("wrd.entity_settings")
    .select(["wrd.entity_settings.id"])
    .whereRef("wrd.entity_settings.entity", "=", "wrd.entities.id")
    .where("wrd.entity_settings.attribute", "=", attr);
}

/** Adds query filters to a query for simple query matches
 * Given the query
 * ```
 * SELECT ...
 *   FROM wrd.entities
 * ```
 * this function adds the following condition
 *  WHERE (EXISTS (SELECT FROM wrd.entity_settings WHERE entity = entities.id AND (...part added by builder callback))
 *         OR NOT EXISTS (SELECT FROM entity_settings WHERE entity = entities.id) // only if defaultmatches === true
 * @param query - Query to extend
 * @param defaultmatches - If TRUE, entities that have no matching settings records (aka have a default value) should also be returned
 * @param builder - Function that add the relevant conditions on the first subquery to identify matching settings records
 * @returns Updated query
*/
function addQueryFilter2<O>(query: SelectQueryBuilder<PlatformDB, "wrd.entities", O>, attr: number, defaultmatches: boolean, builder?: (b: SettingsExpressionBuilder) => Expression<SqlBool>): SelectQueryBuilder<PlatformDB, "wrd.entities", O> {
  return query.where((oqb) => {
    let subquery = getSettingsSelect(oqb, attr);
    if (builder)
      subquery = subquery.where(sqb => builder(sqb));

    const valueTest = oqb.exists(subquery);
    if (defaultmatches) {
      return oqb.or([
        valueTest,
        oqb.not(oqb.exists(eqb => getSettingsSelect(eqb, attr)))
      ]);
    }
    return valueTest;
  });
}

function getAttrBaseCells<T extends keyof typeof baseAttrCells>(tag: string, allowedTypes: readonly T[]): typeof baseAttrCells[T] {
  if (!allowedTypes.includes(tag as T))
    throw new Error(`Unhandled base attribute ${JSON.stringify(tag)}`);
  return baseAttrCells[tag as T];
}

type WRDDBStringConditions = {
  condition: "=" | ">=" | ">" | "!=" | "<" | "<="; value: string; options?: { matchCase?: boolean };
} | {
  condition: "in"; value: readonly string[]; options?: { matchCase?: boolean };
} | {
  condition: "like"; value: string; options?: { matchCase?: boolean };
} | {
  condition: "mentions"; value: string; options?: { matchCase?: boolean };
} | {
  condition: "mentionsany"; value: readonly string[]; options?: { matchCase?: boolean };
};

class WRDDBStringValue extends WRDAttributeValueBase<string, string, string, WRDDBStringConditions> {
  getDefaultValue() { return ""; }
  isSet(value: string) { return Boolean(value); }
  checkFilter({ condition, value }: WRDDBStringConditions) {
    if (condition === "mentions" && !value)
      throw new Error(`Value may not be empty for condition type ${JSON.stringify(condition)}`);
  }
  matchesValue(value: string, cv: WRDDBStringConditions): boolean {
    const caseInsensitive = cv.options?.matchCase === false; //matchcase defauls to true
    if (caseInsensitive)
      value = toCLocaleUppercase(value);
    if (cv.condition === "in" || cv.condition === "mentionsany") {
      if (caseInsensitive) {
        return cv.value.some(v => value === toCLocaleUppercase(v));
      } else
        return cv.value.includes(value);
    }
    const cmpvalue = caseInsensitive ? toCLocaleUppercase(cv.value) : cv.value;
    if (cv.condition === "like") {
      return isLike(value, cmpvalue);
    }
    return cmp(value, cv.condition === "mentions" ? "=" : cv.condition, cmpvalue);
  }

  isCaseInsensitve(cv: WRDDBStringConditions) {
    return cv.options?.matchCase === false; //matchcase defaults to true;
  }

  addToQuery<O>(query: SelectQueryBuilder<PlatformDB, "wrd.entities", O>, cv: WRDDBStringConditions): AddToQueryResponse<O> {
    const defaultmatches = this.matchesValue(this.getDefaultValue(), cv);
    const caseInsensitive = this.isCaseInsensitve(cv);

    // Rewrite like query to PostgreSQL LIKE mask format
    let db_cv = { ...cv };
    if (db_cv.condition === "like") {
      db_cv.value = db_cv.value.replaceAll(/[\\%_]/g, "\\$&").replaceAll("*", "%").replaceAll("?", "_");
    }

    // rewrite mentions and mentionsany to supported conditions
    if (db_cv.condition === "mentions")
      db_cv = { ...db_cv, condition: "=" };
    else if (db_cv.condition === "mentionsany")
      db_cv = { ...db_cv, condition: "in" };

    if (caseInsensitive) {
      if (db_cv.condition === "in")
        db_cv.value = db_cv.value.map(v => toCLocaleUppercase(v));
      else
        db_cv.value = toCLocaleUppercase(db_cv.value);
    }

    if (db_cv.condition === "in" && !db_cv.value.length)
      return null; // no results!

    // copy to a new variable to satisfy TypeScript type inference
    const filtered_cv = db_cv;
    query = addQueryFilter2(query, this.attr.id, defaultmatches, b => {
      return caseInsensitive
        ? b(sql`upper("rawdata")`, filtered_cv.condition, filtered_cv.value)
        : b(`rawdata`, filtered_cv.condition, filtered_cv.value);
    });

    return {
      needaftercheck: false,
      query
    };
  }

  getFromRecord(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number): string {
    return entity_settings[settings_start].rawdata;
  }

  validateInput(value: string, checker: ValueQueryChecker, attrPath: string) {
    if (this.attr.required && !value && !checker.importMode && (!checker.temp || attrPath))
      throw new Error(`Provided default value for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
    if (value && this.attr.isunique)
      checker.addUniqueCheck(this.attr.fullTag, value, attrPath + this.attr.tag);
    if (Buffer.byteLength(value) > 4096)
      throw new Error(`Provided too large value (${Buffer.byteLength(value)} bytes) for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
  }

  encodeValue(value: string): EncodedValue {
    return value ? { settings: { rawdata: value, attribute: this.attr.id } } : {};
  }
}

class WRDDBEmailValue extends WRDDBStringValue {
  validateInput(value: string, checker: ValueQueryChecker, attrPath: string): void {
    super.validateInput(value, checker, attrPath);
    if (value && !isValidEmail(value) && !checker.importMode)
      throw new Error(`Invalid email address ${JSON.stringify(value)} for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
  }
  isCaseInsensitve(cv: WRDDBStringConditions) {
    return true;
  }
}

class WRDDBUrlValue extends WRDDBStringValue {
  validateInput(value: string, checker: ValueQueryChecker, attrPath: string): void {
    super.validateInput(value, checker, attrPath);
    if (value && !isValidUrl(value) && !checker.importMode)
      throw new Error(`Invalid URL ${JSON.stringify(value)} for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
  }
}

class WRDDBBaseStringValue extends WRDAttributeValueBase<string, string, string, WRDDBStringConditions> {
  getDefaultValue() { return ""; }
  isSet(value: string) { return Boolean(value); }
  checkFilter({ condition, value }: WRDDBStringConditions) {
    if (condition === "mentions" && !value)
      throw new Error(`Value may not be empty for condition type ${JSON.stringify(condition)}`);
  }
  matchesValue(value: string, cv: WRDDBStringConditions): boolean {
    const caseInsensitive = cv.options?.matchCase === false; //matchcase defauls to true
    if (caseInsensitive)
      value = toCLocaleUppercase(value);
    if (cv.condition === "in" || cv.condition === "mentionsany") {
      if (caseInsensitive) {
        return cv.value.some(v => value === toCLocaleUppercase(v));
      } else
        return cv.value.includes(value);
    }
    const cmpvalue = caseInsensitive ? toCLocaleUppercase(cv.value) : cv.value;
    if (cv.condition === "like") {
      return isLike(value, cmpvalue);
    }
    return cmp(value, cv.condition === "mentions" ? "=" : cv.condition, cmpvalue);
  }

  addToQuery<O>(query: SelectQueryBuilder<PlatformDB, "wrd.entities", O>, cv: WRDDBStringConditions): AddToQueryResponse<O> {
    const caseInsensitive = cv.options?.matchCase === false; //matchcase defauls to true
    // Rewrite like query to PostgreSQL LIKE mask format
    let db_cv = { ...cv };
    if (db_cv.condition === "like") {
      db_cv.value = db_cv.value.replaceAll(/[\\%_]/g, "\\$&").replaceAll("*", "%").replaceAll(".", "_");
    }

    // rewrite mentions and mentionsany to supported conditions
    if (db_cv.condition === "mentions")
      db_cv = { ...db_cv, condition: "=" };
    else if (db_cv.condition === "mentionsany")
      db_cv = { ...db_cv, condition: "in" };

    if (caseInsensitive) {
      if (db_cv.condition === "in")
        db_cv.value = db_cv.value.map(v => toCLocaleUppercase(v));
      else
        db_cv.value = toCLocaleUppercase(db_cv.value);
    }

    if (db_cv.condition === "in" && !db_cv.value.length)
      return null;

    // copy to a new variable to satisfy TypeScript type inference
    const filtered_cv = db_cv;

    let baseAttr: RawBuilder<unknown>;
    switch (this.attr.tag) {
      case "wrdTag": baseAttr = !caseInsensitive ? sql`tag` : sql`upper("tag")`; break;
      case "wrdInitials": baseAttr = !caseInsensitive ? sql`initials` : sql`upper("initials")`; break;
      case "wrdFirstName": baseAttr = !caseInsensitive ? sql`firstname` : sql`upper("firstname")`; break;
      case "wrdFirstNames": baseAttr = !caseInsensitive ? sql`firstnames` : sql`upper("firstnames")`; break;
      case "wrdInfix": baseAttr = !caseInsensitive ? sql`infix` : sql`upper("infix")`; break;
      case "wrdLastName": baseAttr = !caseInsensitive ? sql`lastname` : sql`upper("lastname")`; break;
      case "wrdTitles": baseAttr = !caseInsensitive ? sql`titles` : sql`upper("titles")`; break;
      case "wrdTitlesSuffix": baseAttr = !caseInsensitive ? sql`titles_suffix` : sql`upper("titles_suffix")`; break;
      default: throw new Error(`Unhandled base string attribute ${JSON.stringify(this.attr.tag)}`);
    }
    return {
      needaftercheck: false,
      query: query.where(baseAttr, filtered_cv.condition, filtered_cv.value)
    };
  }

  getValue(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number, entityRecord: EntityPartialRec): string {
    switch (this.attr.tag) {
      case "wrdTag": return entityRecord.tag || "";
      case "wrdInitials": return entityRecord.initials || "";
      case "wrdFirstName": return entityRecord.firstname || "";
      case "wrdFirstNames": return entityRecord.firstnames || "";
      case "wrdInfix": return entityRecord.infix || "";
      case "wrdLastName": return entityRecord.lastname || "";
      case "wrdTitles": return entityRecord.titles || "";
      case "wrdTitlesSuffix": return entityRecord.titles_suffix || "";
      default: throw new Error(`Unhandled base string attribute ${JSON.stringify(this.attr.tag)}`);
    }
  }

  getFromRecord(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number): string {
    throw new Error("Not implemented for base fields");
  }

  validateInput(value: string, checker: ValueQueryChecker, attrPath: string) {
    if (this.attr.required && !value && !checker.importMode)
      throw new Error(`Provided default value for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
    if (value.length > 256)
      throw new Error(`Value for attribute ${checker.typeTag}.${attrPath}${this.attr.tag} is too long (${value.length} characters, maximum is 256)`);
    if (value && this.attr.isunique)
      checker.addUniqueCheck(this.attr.fullTag, value, attrPath + this.attr.tag);
    if (this.attr.tag === "wrdTag" && !isValidWRDTag(value))
      throw new Error(`Invalid wrdTag '${value}' - must start with A-Z, may only contain A-Z, 0-9 and _, but must not end with a _. Maximum length is 64 characters`);
  }

  getAttrBaseCells(): keyof EntityPartialRec {
    return getAttrBaseCells(this.attr.tag, [
      "wrdTag",
      "wrdInitials",
      "wrdFirstName",
      "wrdFirstNames",
      "wrdInfix",
      "wrdLastName",
      "wrdTitles",
      "wrdTitlesSuffix"
    ]);
  }

  encodeValue(value: string): EncodedValue {
    const key = this.getAttrBaseCells();
    return { entity: { [key]: value } };
  }
}

type WRDDBGuidConditions = {
  condition: "=" | ">=" | ">" | "!=" | "<" | "<="; value: string;
} | {
  condition: "in"; value: readonly string[]; options?: { matchcase?: boolean };
};

class WRDDBBaseGuidValue extends WRDAttributeValueBase<string, string, string, WRDDBGuidConditions> {
  checkGuid(guid: string) {
    decodeWRDGuid(guid);
  }
  getDefaultValue() { return ""; }
  isSet(value: string) { return Boolean(value); }
  checkFilter(cv: WRDDBGuidConditions) {
    if (cv.condition === "in")
      cv.value.forEach(v => this.checkGuid(v));
    else
      this.checkGuid(cv.value);
  }
  matchesValue(value: string, cv: WRDDBGuidConditions): boolean {
    if (cv.condition === "in")
      return cv.value.includes(value);
    return cmp(value, cv.condition, cv.value);
  }
  addToQuery<O>(query: SelectQueryBuilder<PlatformDB, "wrd.entities", O>, cv: WRDDBGuidConditions): AddToQueryResponse<O> {
    // Rewrite like query to PostgreSQL LIKE mask format
    const db_cv = cv.condition === "in" ?
      { ...cv, value: cv.value.map(decodeWRDGuid) } :
      { ...cv, value: decodeWRDGuid(cv.value) };

    if (db_cv.condition === "in" && !db_cv.value.length)
      return null;

    return {
      needaftercheck: false,
      query: query.where("guid", db_cv.condition, db_cv.value)
    };
  }

  getValue(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number, entityRecord: EntityPartialRec): string {
    return encodeWRDGuid(entityRecord.guid!);
  }

  getFromRecord(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number): string {
    throw new Error("Not implemented for base fields");
  }

  validateInput(value: string, checker: ValueQueryChecker) {
    this.checkGuid(value);
    checker.addUniqueCheck(this.attr.fullTag, value, this.attr.tag);
  }

  getAttrBaseCells(): null | keyof EntityPartialRec | Array<keyof EntityPartialRec> {
    return getAttrBaseCells(this.attr.tag, ["wrdGuid"]);
  }

  encodeValue(value: string): EncodedValue {
    return { entity: { guid: decodeWRDGuid(value) } };
  }
}

type WRDDBaseGeneratedStringConditions = {
  condition: "=" | ">=" | ">" | "!=" | "<" | "<="; value: string;
} | {
  condition: "in"; value: readonly string[]; options?: { matchcase?: boolean };
};

class WRDDBBaseGeneratedStringValue extends WRDAttributeValueBase<never, string, string, WRDDBaseGeneratedStringConditions> {
  getDefaultValue() { return ""; }

  isSet(value: string) { return Boolean(value); }

  checkFilter({ condition, value }: WRDDBaseGeneratedStringConditions) {
    // type-check is enough (for now)
  }

  matchesValue(value: string, cv: WRDDBaseGeneratedStringConditions): boolean {
    if (cv.condition === "in")
      return cv.value.includes(value);
    return cmp(value, cv.condition, cv.value);
  }

  getValue(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number, entityRecord: EntityPartialRec): string {
    switch (this.attr.tag) {
      case "wrdSaluteFormal": {
        throw new Error(`wrdSaluteFormal is not implemented`);
      }
      case "wrdAddressFormal": {
        throw new Error(`wrdAddressFormal is not implemented`);
      }
      case "wrdFullName":
      case "wrdTitle": {
        if (!entityRecord.firstname && !entityRecord.firstnames && !entityRecord.lastname)
          return ""; //Not enough information to create a 'full name'

        let fullname = "";
        if (entityRecord.firstname !== "")
          fullname += entityRecord.firstname;
        else if (entityRecord.firstnames !== "")
          fullname += entityRecord.firstnames;
        else if (entityRecord.initials)
          fullname += entityRecord.initials;
        if (entityRecord.lastname)
          fullname += ` ${entityRecord.infix ? entityRecord.infix + " " : ""}${entityRecord.lastname}`;
        return fullname.trim();
      }
      default: throw new Error(`Unhandled base generated string attribute ${JSON.stringify(this.attr.tag)}`);
    }
  }

  getFromRecord(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number): string {
    throw new Error("Not implemented for base fields");
  }

  getAttrBaseCells(): null | keyof EntityPartialRec | ReadonlyArray<keyof EntityPartialRec> {
    return getAttrBaseCells(this.attr.tag, ["wrdSaluteFormal", "wrdAddressFormal", "wrdFullName", "wrdTitle"]);
  }

  validateInput(value: string, checker: ValueQueryChecker, attrPath: string): void {
    throw new Error(`Unable to update generated field ${checker.typeTag}.${attrPath}${this.attr.tag}`);
  }

  encodeValue(value: string | null): EncodedValue {
    throw new Error(`Unable to updated generated field ${JSON.stringify(this.attr.tag)}`);
  }
}

type WRDDBBooleanConditions = {
  condition: "<" | "<=" | "=" | "!=" | ">=" | ">"; value: boolean;
};

class WRDDBBooleanValue extends WRDAttributeValueBase<boolean, boolean, boolean, WRDDBBooleanConditions> {
  getDefaultValue() { return false; }
  isSet(value: boolean) { return value; }
  checkFilter({ condition, value }: WRDDBBooleanConditions) {
    // type-check is enough (for now)
  }
  matchesValue(value: boolean, cv: WRDDBBooleanConditions): boolean {
    return cmp(value, cv.condition, cv.value);
  }

  addToQuery<O>(query: SelectQueryBuilder<PlatformDB, "wrd.entities", O>, cv: WRDDBBooleanConditions): AddToQueryResponse<O> {
    const defaultmatches = this.matchesValue(this.getDefaultValue(), cv);

    query = addQueryFilter2(query, this.attr.id, defaultmatches, b => b(`rawdata`, cv.condition, cv.value ? "1" : ""));

    return {
      needaftercheck: false,
      query
    };
  }

  getFromRecord(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number): boolean {
    return entity_settings[settings_start].rawdata === "1";
  }

  validateInput(value: boolean, checker: ValueQueryChecker, attrPath: string) {
    if (this.attr.required && !value && !checker.importMode && (!checker.temp || attrPath))
      throw new Error(`Provided default value for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
  }

  encodeValue(value: boolean): EncodedValue {
    return value ? { settings: { rawdata: "1", attribute: this.attr.id } } : {};
  }
}

type WRDDBIntegerConditions = {
  condition: "<" | "<=" | "=" | "!=" | ">=" | ">"; value: number;
} | {
  condition: "in"; value: readonly number[];
} | {
  condition: "mentions"; value: number;
} | {
  condition: "mentionsany"; value: readonly number[];
};

class WRDDBIntegerValue extends WRDAttributeValueBase<number, number, number, WRDDBIntegerConditions> {
  getDefaultValue() { return 0; }
  isSet(value: number) { return Boolean(value); }
  checkFilter({ condition, value }: WRDDBIntegerConditions) {
    // type-check is enough (for now)
  }
  matchesValue(value: number, cv: WRDDBIntegerConditions): boolean {
    if (cv.condition === "in" || cv.condition === "mentionsany")
      return cv.value.includes(value);
    if (cv.condition === "mentions")
      cv = { condition: "=", value: cv.value };
    return cmp(value, cv.condition, cv.value);
  }

  addToQuery<O>(query: SelectQueryBuilder<PlatformDB, "wrd.entities", O>, cv: WRDDBIntegerConditions): AddToQueryResponse<O> {
    const defaultmatches = this.matchesValue(this.getDefaultValue(), cv);

    if (cv.condition === "mentions")
      cv = { condition: "=", value: cv.value };
    else if (cv.condition === "mentionsany")
      cv = { condition: "in", value: cv.value };
    if (cv.condition === "in" && !cv.value.length)
      return null;

    query = addQueryFilter2(query, this.attr.id, defaultmatches, b => b(sql<number>`rawdata::integer`, cv.condition, cv.value));

    return {
      needaftercheck: false,
      query
    };
  }

  getFromRecord(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number): number {
    return Number(entity_settings[settings_start].rawdata);
  }

  validateInput(value: number, checker: ValueQueryChecker, attrPath: string) {
    if (this.attr.required && !value && !checker.importMode && (!checker.temp || attrPath))
      throw new Error(`Provided default value for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
    if (value && this.attr.isunique)
      checker.addUniqueCheck(this.attr.fullTag, value, attrPath + this.attr.tag);
  }

  encodeValue(value: number): EncodedValue {
    return value ? { settings: { rawdata: String(value), attribute: this.attr.id } } : {};
  }
}


class WRDDBBaseIntegerValue extends WRDAttributeValueBase<number, number, number, WRDDBIntegerConditions> {
  getDefaultValue() { return 0; }
  isSet(value: number) { return Boolean(value); }
  checkFilter({ condition, value }: WRDDBIntegerConditions) {
    // type-check is enough (for now)
  }
  matchesValue(value: number, cv: WRDDBIntegerConditions): boolean {
    if (cv.condition === "in" || cv.condition === "mentionsany")
      return cv.value.includes(value);
    if (cv.condition === "mentions")
      cv = { condition: "=", value: cv.value };
    return cmp(value, cv.condition, cv.value);
  }

  addToQuery<O>(query: SelectQueryBuilder<PlatformDB, "wrd.entities", O>, cv: WRDDBIntegerConditions): AddToQueryResponse<O> {
    if (cv.condition === "mentions")
      cv = { condition: "=", value: cv.value };
    else if (cv.condition === "mentionsany")
      cv = { condition: "in", value: cv.value };

    if (cv.condition === "in" && !cv.value.length)
      return null;

    switch (this.attr.tag) {
      case "wrdId": query = query.where("id", cv.condition, cv.value); break;
      case "wrdType": query = query.where("type", cv.condition, cv.value); break;
      case "wrdOrdering": query = query.where("ordering", cv.condition, cv.value); break;
      default: throw new Error(`Unhandled base integer attribute ${JSON.stringify(this.attr.tag)}`);
    }

    return {
      needaftercheck: false,
      query
    };
  }

  getValue(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number, entityrec: EntityPartialRec): number {
    switch (this.attr.tag) {
      case "wrdId": return entityrec["id"] || 0;
      case "wrdType": return entityrec["type"] || 0;
      case "wrdOrdering": return entityrec["ordering"] || 0;
      default: throw new Error(`Unhandled base integer attribute ${JSON.stringify(this.attr.tag)}`);
    }
  }

  getFromRecord(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number): number {
    throw new Error(`Should not be called for base attributes`);
  }

  validateInput(value: number, checker: ValueQueryChecker, attrPath: string) {
    if (this.attr.required && !value && !checker.importMode && (!checker.temp || attrPath))
      throw new Error(`Provided default value for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
    if (value && this.attr.isunique)
      checker.addUniqueCheck(this.attr.fullTag, value, attrPath + this.attr.tag);
  }

  getAttrBaseCells(): keyof EntityPartialRec {
    return getAttrBaseCells(this.attr.tag, ["wrdId", "wrdType", "wrdOrdering"]);
  }

  encodeValue(value: number): EncodedValue {
    return { entity: { [this.getAttrBaseCells()]: value } };
  }
}

type WRDDBDomainConditions = {
  condition: "=" | "!="; value: number | null;
} | {
  condition: "in"; value: ReadonlyArray<number | null>;
} | {
  condition: "mentions"; value: number;
} | {
  condition: "mentionsany"; value: readonly number[];
};

class WRDDBDomainValue<Required extends boolean> extends WRDAttributeValueBase<
  (true extends Required ? number : number | null),
  (number | null),
  (true extends Required ? number : number | null),
  WRDDBDomainConditions
> {
  getDefaultValue(): number | null { return null; }
  isSet(value: number | null) { return Boolean(value); }
  checkFilter(cv: WRDDBDomainConditions) {
    if (cv.condition === "mentionsany") {
      if (cv.value.some(v => !v))
        throw new Error(`Not allowed to use 'null' or 0 for matchtype ${JSON.stringify(cv.condition)}`);
    } else if (cv.condition === "in") {
      if (cv.value.some(v => v === 0))
        throw new Error(`Not allowed to use 0 for matchtype ${JSON.stringify(cv.condition)}`);
    } else if (cv.condition === "mentions" && !cv.value)
      throw new Error(`Not allowed to use 'null' or 0 for matchtype ${JSON.stringify(cv.condition)}`);
    if (cv.value === 0)
      throw new Error(`Not allowed to use 0 for domain types`);
  }
  matchesValue(value: number | null, cv: WRDDBDomainConditions): boolean {
    switch (cv.condition) {
      case "=":
      case "mentions": {
        return value === (cv.value || null);
      }
      case "!=": {
        return value !== (cv.value || null);
      }
      case "in": {
        return Boolean(cv.value.includes(value));
      }
      case "mentionsany": {
        return Boolean(value && cv.value.includes(value));
      }
    }
  }

  addToQuery<O>(query: SelectQueryBuilder<PlatformDB, "wrd.entities", O>, cv: WRDDBDomainConditions): AddToQueryResponse<O> {
    const defaultmatches = this.matchesValue(this.getDefaultValue(), cv);

    // rewrite mentions and mentionsany to supported conditions
    let db_cv = { ...cv };
    if (db_cv.condition === "mentions")
      db_cv = { ...db_cv, condition: "=" };
    else if (db_cv.condition === "mentionsany")
      db_cv = { ...db_cv, condition: "in" };

    if (db_cv.condition === "in" && !db_cv.value.length)
      return null;

    // copy to a new variable to satisfy TypeScript type inference
    const fixed_db_cv = db_cv;
    query = db_cv.value === null && db_cv.condition === '!='
      ? addQueryFilter2(query, this.attr.id, defaultmatches)
      : addQueryFilter2(query, this.attr.id, defaultmatches, b => b(`setting`, fixed_db_cv.condition, fixed_db_cv.value));
    return {
      needaftercheck: false,
      query
    };
  }

  getFromRecord(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number): (true extends Required ? number : number | null) {
    return entity_settings[settings_start].setting as number; // for domains, always filled with valid reference
  }

  validateInput(value: true extends Required ? number : number | null, checker: ValueQueryChecker, attrPath: string) {
    if (this.attr.required && !value && !checker.importMode && (!checker.temp || attrPath))
      throw new Error(`Provided default value for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
    if (value && this.attr.domain) {
      checker.addRefCheck(this.attr.domain, value, attrPath + this.attr.tag);
      if (this.attr.isunique)
        checker.addUniqueCheck(this.attr.fullTag, value, attrPath + this.attr.tag);
    }
    if (value === 0)
      throw new Error(`Value may not be the number 0 for attribute ${checker.typeTag}.${attrPath}${this.attr.tag} - use \`null\` to encode an empty domain value`);
  }

  encodeValue(value: number): EncodedValue {
    if (value === null)
      return {};

    return { settings: { setting: value, attribute: this.attr.id } };
  }
}

class WRDDBBaseDomainValue<Required extends boolean> extends WRDAttributeValueBase<
  (true extends Required ? number : number | null),
  (number | null),
  (true extends Required ? number : number | null),
  WRDDBDomainConditions
> {
  getDefaultValue(): number | null { return null; }
  isSet(value: number | null) { return Boolean(value); }
  checkFilter(cv: WRDDBDomainConditions) {
    if (cv.condition === "mentionsany") {
      if (cv.value.some(v => !v))
        throw new Error(`The value 'null' (or 0) is not allowed for matchtype ${JSON.stringify(cv.condition)}`);
    } else if (cv.condition === "mentions" && !cv.value)
      throw new Error(`The value 'null' (or 0) is not allowed for matchtype ${JSON.stringify(cv.condition)}`);
  }
  matchesValue(value: number | null, cv: WRDDBDomainConditions): boolean {
    switch (cv.condition) {
      case "=":
      case "mentions": {
        return value === (cv.value || null);
      }
      case "!=": {
        return value !== (cv.value || null);
      }
      case "in":
      case "mentionsany": {
        return Boolean(value && cv.value.includes(value));
      }
    }
  }

  addToQuery<O>(query: SelectQueryBuilder<PlatformDB, "wrd.entities", O>, cv: WRDDBDomainConditions): AddToQueryResponse<O> {
    // rewrite mentions and mentionsany to supported conditions
    let db_cv = { ...cv };
    if (db_cv.condition === "mentions")
      db_cv = { ...db_cv, condition: "=" };
    else if (db_cv.condition === "mentionsany")
      db_cv = { ...db_cv, condition: "in" };

    if (db_cv.condition === "in" && !db_cv.value.length)
      return null; // no results!

    // copy to a new variable to satisfy TypeScript type inference
    const fixed_db_cv = db_cv;
    const fieldname = this.getAttrBaseCells();
    if (fixed_db_cv.condition === "=" && !fixed_db_cv.value)
      query = query.where(fieldname, "is", null);
    else if (fixed_db_cv.condition === "!=" && !fixed_db_cv.value)
      query = query.where(fieldname, "is not", null);
    else if (fixed_db_cv.condition === "in" && fixed_db_cv.value.some(v => !v)) {
      // convert `field in [ null, ...x ]` to `(field is null or field in [ ...x ])`
      const nonnull = fixed_db_cv.value.filter(v => v);
      if (nonnull.length)
        query = query.where(qb => qb.or([qb(fieldname, "in", nonnull), qb(fieldname, "is", null)]));
      else
        query = query.where(fieldname, "is", null);
    } else
      query = query.where(fieldname, fixed_db_cv.condition, fixed_db_cv.value);

    return {
      needaftercheck: false,
      query
    };
  }

  getValue(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number, entityrec: EntityPartialRec): (true extends Required ? number : number | null) {
    const retval = entityrec[this.getAttrBaseCells()] || null;
    return retval as (true extends Required ? number : number | null);
  }

  getFromRecord(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number): (true extends Required ? number : number | null) {
    throw new Error(`Should not be called for base attributes`);
  }

  validateInput(value: true extends Required ? number : number | null, checker: ValueQueryChecker, attrPath: string) {
    if (this.attr.required && !value && !checker.importMode && (!checker.temp || attrPath))
      throw new Error(`Provided default value for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);

    if (value && this.attr.domain && (this.attr.tag === "wrdLeftEntity" || this.attr.tag === "wrdRightEntity")) {
      if (value === checker.entityId)
        throw new Error(`Entity ${checker.entityId} may not reference itself in attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
      checker.addRefCheck(this.attr.domain, value, attrPath + this.attr.tag);
      if (value && this.attr.isunique)
        checker.addUniqueCheck(this.attr.fullTag, value, attrPath + this.attr.tag);
    }
  }

  getAttrBaseCells(): keyof EntityPartialRec {
    return getAttrBaseCells(this.attr.tag, ["wrdId", "wrdType", "wrdLeftEntity", "wrdRightEntity"]);
  }

  encodeValue(value: number): EncodedValue {
    return { entity: { [this.getAttrBaseCells()]: value } };
  }
}

type WRDDBDomainArrayConditions = {
  condition: "mentions" | "contains"; value: number;
} | {
  condition: "mentionsany" | "intersects"; value: readonly number[];
} | {
  condition: "=" | "!="; value: readonly number[];
};

class WRDDBDomainArrayValue extends WRDAttributeValueBase<number[], number[], number[], WRDDBDomainArrayConditions> {
  getDefaultValue(): number[] { return []; }
  isSet(value: number[]) { return Boolean(value?.length); }
  checkFilter({ condition, value }: WRDDBDomainArrayConditions) {
    if (Array.isArray(value)) {
      if (value.some(v => !v))
        throw new Error(`The value 'null' (or 0) is not allowed for matchtype ${JSON.stringify(condition)}`);
    } else if (!value)
      throw new Error(`The value 'null' (or 0) is not allowed for matchtype ${JSON.stringify(condition)}`);
  }
  matchesValue(value: number[], cv: WRDDBDomainArrayConditions): boolean {
    switch (cv.condition) {
      case "mentions":
      case "contains": {
        return value.includes(cv.value);
      }
      case "mentionsany":
      case "intersects": {
        for (const v of value)
          if (cv.value.includes(v))
            return true;
        return false;
      }
      case "=":
      case "!=": {
        for (const v of value)
          if (!cv.value.includes(v))
            return cv.condition === "!=";
        for (const v of cv.value)
          if (!value.includes(v))
            return cv.condition === "!=";
        return cv.condition !== "!=";
      }
    }
  }

  addToQuery<O>(query: SelectQueryBuilder<PlatformDB, "wrd.entities", O>, cv: WRDDBDomainArrayConditions): AddToQueryResponse<O> {
    const defaultmatches = this.matchesValue(this.getDefaultValue(), cv);

    type Conditions = {
      condition: "=" | "!="; value: number;
    } | {
      condition: "in" | "not in"; value: readonly number[];
    } | undefined;

    // For '=' and '!=',
    let db_cv: Conditions;
    switch (cv.condition) {
      case "mentions":
      case "contains": db_cv = { condition: "=", value: cv.value }; break;
      case "mentionsany":
      case "intersects": db_cv = { condition: "in", value: cv.value }; break;
      case "=": if (cv.value.length) db_cv = { condition: "in", value: cv.value }; break;
    }

    if (db_cv) {
      if (db_cv.condition === "in" && !db_cv.value.length)
        return null; // no results!

      // copy to a new variable to satisfy TypeScript type inference
      const fixed_db_cv = db_cv;

      query = addQueryFilter2(query, this.attr.id, defaultmatches, b => b(`setting`, fixed_db_cv.condition, fixed_db_cv.value));
    }

    return {
      needaftercheck: cv.condition === "=" || cv.condition === "!=",
      query
    };
  }

  getFromRecord(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number): number[] {
    const retval: number[] = [];
    for (let idx = settings_start; idx < settings_limit; ++idx) {
      const link = entity_settings[idx].setting;
      if (link)
        retval.push(link);
    }
    return retval;
  }

  validateInput(value: number[], checker: ValueQueryChecker, attrPath: string) {
    if (this.attr.required && !value.length && !checker.importMode && (!checker.temp || attrPath))
      throw new Error(`Provided default value for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
    if (value.includes(0))
      throw new Error(`Value may not include the number 0 for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
  }

  encodeValue(value: number[]): EncodedValue {
    return {
      settings: [...new Set(value)].toSorted((a, b) => a - b).map((setting, idx) => ({ setting, attribute: this.attr.id, ordering: idx + 1 }))
    };
  }
}

type WRDDBEnumConditions<Options extends { allowedValues: string }, Required extends boolean> = {
  condition: "=" | "!="; value: GetEnumAllowedValues<Options, Required> | null; options: { ignoreAllowedValues?: boolean };
} | {
  condition: "in"; value: ReadonlyArray<GetEnumAllowedValues<Options, Required> | null>; options: { ignoreAllowedValues?: boolean };
} | {
  condition: "like"; value: string;
} | {
  condition: "mentions"; value: GetEnumAllowedValues<Options, Required>; options: { ignoreAllowedValues?: boolean };
} | {
  condition: "mentionsany"; value: ReadonlyArray<GetEnumAllowedValues<Options, Required>>; options: { ignoreAllowedValues?: boolean };
};

// FIXME: add wildcard support
type GetEnumAllowedValues<Options extends { allowedValues: string }, Required extends boolean> = (Options extends { allowedValues: infer V } ? V : never) | (Required extends true ? never : null);

type AllowedValues = Array<string | RegExp>;

function prepAllowedValues(allowedValues: string): AllowedValues {
  return allowedValues?.split("\t").map(_ => _.includes("*") || _.includes('?') ? regExpFromWildcards(_) : _);
}

function isAllowed(allowed: AllowedValues, value: string) {
  return value.match(/^[-a-zA-Z0-9_:]+$/) && allowed.some(_ => _ === value || (_ instanceof RegExp && _.test(value)));
}


abstract class WRDDBEnumValueBase<
  Options extends { allowedValues: string },
  Required extends boolean> extends WRDAttributeValueBase<
    GetEnumAllowedValues<Options, Required>,
    GetEnumAllowedValues<Options, Required> | null,
    GetEnumAllowedValues<Options, Required>,
    WRDDBEnumConditions<Options, Required>
  > {
  getDefaultValue(): GetEnumAllowedValues<Options, Required> | null { return null; }
  isSet(value: string | null) { return Boolean(value); }
  checkFilter(cv: WRDDBEnumConditions<Options, Required>) {
    if ((cv.condition === "mentions" && !cv.value) || (cv.condition === "mentionsany" && cv.value.some(_ => !_)))
      throw new Error(`Value may not be empty for condition type ${JSON.stringify(cv.condition)}`);
    if (cv.value === "")
      throw new Error(`Use null instead of "" for enum compares`);

    if (cv.condition !== "like" && !cv.options?.ignoreAllowedValues) {
      if (cv.value && cv.condition === "=" && !isAllowed(prepAllowedValues(this.attr.allowedvalues), cv.value))
        throw new Error(`Invalid value ${JSON.stringify(cv.value)} for enum attribute ${this.attr.fullTag}`);
    }
  }
  //Note that even though the vlque
  matchesValue(value: GetEnumAllowedValues<Options, Required> | null, cv: WRDDBEnumConditions<Options, Required>): boolean {
    if (cv.condition === "in") {
      return cv.value.includes(value);
    }
    if (cv.condition === "mentionsany") {
      return Boolean(value && cv.value.includes(value));
    }
    if (cv.condition === "like") {
      return Boolean(value && isLike(value, cv.value));
    }
    return cmp(value, cv.condition === "mentions" ? "=" : cv.condition, cv.value);
  }

  addToQuery<O>(query: SelectQueryBuilder<PlatformDB, "wrd.entities", O>, cv: WRDDBEnumConditions<Options, Required>): AddToQueryResponse<O> {
    const defaultmatches = this.matchesValue(this.getDefaultValue(), cv);

    // Rewrite like query to PostgreSQL LIKE mask format
    let db_cv = { ...cv };
    if (db_cv.condition === "like") {
      db_cv.value = db_cv.value.replaceAll(/[\\%_]/g, "\\$&").replaceAll("*", "%").replaceAll(".", "_");
    }

    // rewrite mentions and mentionsany to supported conditions
    if (db_cv.condition === "mentions")
      db_cv = { ...db_cv, condition: "=" };
    else if (db_cv.condition === "mentionsany")
      db_cv = { ...db_cv, condition: "in" };

    if (db_cv.condition === "in" && !db_cv.value.length)
      return null; // no results!

    // copy to a new variable to satisfy TypeScript type inference
    const filtered_cv = db_cv;
    query = addQueryFilter2(query, this.attr.id, defaultmatches, b => b(sql`rawdata`, filtered_cv.condition, filtered_cv.value || ''));
    return {
      needaftercheck: false,
      query
    };
  }

  getFromRecord(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number): GetEnumAllowedValues<Options, Required> {
    return entity_settings[settings_start].rawdata as GetEnumAllowedValues<Options, Required>;
  }

  validateInput(value: GetEnumAllowedValues<Options, Required> | null, checker: ValueQueryChecker, attrPath: string) {
    if (this.attr.required && (!value || !value.length) && !checker.importMode && (!checker.temp || attrPath))
      throw new Error(`Provided default value for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
    if (value) {
      if (this.attr.isunique)
        checker.addUniqueCheck(this.attr.fullTag, value, attrPath + this.attr.tag);
      if (!checker.importMode && !isAllowed(prepAllowedValues(this.attr.allowedvalues), value))
        throw new Error(`Invalid value ${JSON.stringify(value)} for enum attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
    }
  }
}

class WRDDBEnumValue<Options extends { allowedValues: string }, Required extends boolean> extends WRDDBEnumValueBase<Options, Required> {
  encodeValue(value: GetEnumAllowedValues<Options, Required> | null) {
    return value ? { settings: { rawdata: value, attribute: this.attr.id } } : {};
  }
}

class WRDDBBaseGenderValue extends WRDDBEnumValueBase<{ allowedValues: WRDGender }, false> {
  addToQuery<O>(query: SelectQueryBuilder<PlatformDB, "wrd.entities", O>, cv: WRDDBEnumConditions<{ allowedValues: WRDGender }, false>): AddToQueryResponse<O> {
    if (this.attr.tag !== 'wrdGender')
      throw new Error(`Unhandled base gender attribute ${JSON.stringify(this.attr.tag)}`);

    //TODO implement (but low prio, searching by gender is rare)
    return { needaftercheck: true, query }; //avoid suoer() - WRDDBEnumValueBase does the wrong thing
  }

  getValue(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number, entityrec: EntityPartialRec): WRDGender | null {
    if (this.attr.tag !== 'wrdGender')
      throw new Error(`Unhandled base gender attribute ${JSON.stringify(this.attr.tag)}`);

    switch (entityrec["gender"]) {
      case 0: return null;
      case 1: return WRDGender.Male;
      case 2: return WRDGender.Female;
      case 3: return WRDGender.Other;
      default: throw new Error(`Unhandled base integer attribute ${JSON.stringify(this.attr.tag)}`);
    }
  }

  getFromRecord(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number): never {
    throw new Error(`Should not be called for base attributes`);

  }
  getAttrBaseCells(): keyof EntityPartialRec {
    return getAttrBaseCells(this.attr.tag, ["wrdGender"]);
  }

  validateInput(value: GetEnumAllowedValues<{ allowedValues: WRDGender }, false> | null, checker: ValueQueryChecker, attrPath: string) {
    if (this.attr.required && (!value || !value.length) && !checker.importMode && (!checker.temp || attrPath))
      throw new Error(`Provided default value for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
    if (value) {
      if (!["male", "female", "other"].includes(value))
        throw new Error(`Invalid value ${JSON.stringify(value)} for gender attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
    }
  }


  encodeValue(value: WRDGender) {
    const mapped = [null, WRDGender.Male, WRDGender.Female, WRDGender.Other].indexOf(value);
    if (mapped === -1)
      throw new Error(`Unknown gender value '${value}'`);
    return { entity: { [this.getAttrBaseCells()]: mapped } };
  }
}


type WRDDBEnumArrayConditions<Options extends { allowedValues: string }> = {
  condition: "=" | "!=" | "intersects"; value: ReadonlyArray<GetEnumAllowedValues<Options, true>>; options: { ignoreAllowedValues?: boolean };
} | {
  condition: "contains"; value: GetEnumAllowedValues<Options, true>;
};

class WRDDBEnumArrayValue<Options extends { allowedValues: string }> extends WRDAttributeValueBase<Array<GetEnumArrayAllowedValues<Options>>, Array<GetEnumArrayAllowedValues<Options>>, Array<GetEnumArrayAllowedValues<Options>>, WRDDBEnumArrayConditions<Options>> {
  getDefaultValue(): Array<GetEnumArrayAllowedValues<Options>> { return []; }
  isSet(value: string[]) { return Boolean(value?.length); }
  checkFilter(cv: WRDDBEnumArrayConditions<Options>) {
    const allowedvalues = prepAllowedValues(this.attr.allowedvalues);
    const testList = cv.condition === "contains" ? [cv.value] : cv.value;
    const firstBadValue = testList.find(_ => !_ || !isAllowed(allowedvalues, _));
    if (firstBadValue)
      throw new Error(`Invalid value ${JSON.stringify(firstBadValue)} for enum attribute ${this.attr.fullTag}`);
  }
  matchesValue(value: readonly string[], cv: WRDDBEnumArrayConditions<Options>): boolean {
    if (cv.condition === "contains") {
      return value.includes(cv.value);
    }
    throw new Error(`Condition ${cv.condition} not yet implemented for enumarray`);
  }

  getFromRecord(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number): Array<GetEnumArrayAllowedValues<Options>> {
    return entity_settings[settings_start].rawdata ? entity_settings[settings_start].rawdata.split("\t") as Array<GetEnumArrayAllowedValues<Options>> : [];
  }

  validateInput(value: Array<GetEnumArrayAllowedValues<Options>>, checker: ValueQueryChecker, attrPath: string) {
    if (this.attr.required && !value.length && !checker.importMode && (!checker.temp || attrPath))
      throw new Error(`Provided default value for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);

    for (const v of value)
      if (!v)
        throw new Error(`Provided default enum value for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);

    if (!checker.importMode) {
      const allowed = prepAllowedValues(this.attr.allowedvalues);
      for (const v of value)
        if (!isAllowed(allowed, v))
          throw new Error(`Invalid value ${JSON.stringify(v)} for enum attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
    }
    if (Buffer.byteLength(value.join("\t")) > 4096)
      throw new Error(`EnumArray value too long for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
  }

  encodeValue(value: Array<GetEnumArrayAllowedValues<Options>>): EncodedValue {
    return value.length ? {
      settings: { rawdata: value.toSorted().join("\t"), attribute: this.attr.id }
    } : {};
  }
}

/* Statusrecords are deprecated and TS support is allowed to be minimal. At most we may want to support WRD Syncing these
   fields for some compatibility with newsletter-using modules. No need to validate them any further */
type GetStatusRecordValues<Options extends { allowedValues: string; type: object }, Required extends boolean> = (Options extends { allowedValues: infer V; type: infer T extends object } ? { status: V } & T : never) | (Required extends true ? never : null);

class WRDDBStatusRecordValue<Options extends { allowedValues: string; type: object }, Required extends boolean> extends WRDAttributeValueBase<never, GetStatusRecordValues<Options, Required> | null, GetStatusRecordValues<Options, Required>, WRDDBEnumConditions<Options, Required>> {
  getDefaultValue(): GetStatusRecordValues<Options, Required> | null { return null; }
  isSet(value: object | null) { return Boolean(value); }
  checkFilter({ condition, value }: WRDDBEnumConditions<Options, Required>) {
  }
  matchesValue(value: { status: string } | null, cv: WRDDBEnumConditions<Options, Required>): boolean {
    if (cv.condition === "=" || cv.condition === "!=")
      return cmp(value?.status || "", cv.condition, cv.value || "");

    throw new Error(`Unsupported condition type '${cv.condition}' for statusrecords in TS`);
  }

  getFromRecord(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number): GetStatusRecordValues<Options, Required> {
    const rawdata = entity_settings[settings_start].rawdata;
    const tabPos = rawdata.indexOf("\t");
    if (tabPos < 0)
      return null as GetStatusRecordValues<Options, Required>;

    const status = rawdata.substring(0, tabPos);
    if (rawdata.length > tabPos + 1)
      return { status, ...(decodeHSON(rawdata.substring(tabPos + 1)) as object) } as GetStatusRecordValues<Options, Required>;
    const buf = entity_settings[settings_start].blobdata?.__getAsSyncUInt8Array();
    const bufData = buf ? decodeHSON(Buffer.from(buf).toString()) as object : {};
    return { status, ...bufData } as GetStatusRecordValues<Options, Required>;
  }

  validateInput(value: GetStatusRecordValues<Options, Required>, checker: ValueQueryChecker, attrPath: string) {
    if (this.attr.required && !value?.status.length && !checker.importMode && (!checker.temp || attrPath))
      throw new Error(`Provided default value for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
  }

  encodeValue(value: GetStatusRecordValues<Options, Required> | null): EncodedValue | AwaitableEncodedValue {
    if (!value)
      return {};

    const restData = encodeHSON(omit(value as { status: string }, ["status"]));
    const rawdata = value.status + "\t" + restData;
    if (Buffer.byteLength(rawdata) > 4096) {
      return { settings: { rawdata: rawdata, attribute: this.attr.id } };
    } else {
      return {
        settings: (async (): Promise<EncodedSetting[]> => {
          const blobdata = WebHareBlob.from(restData);
          await uploadBlob(blobdata);
          return [{ rawdata: value.status + "\t", blobdata, attribute: this.attr.id }];
        })()
      };
    }
  }
}

type WRDDBDateTimeConditions = {
  condition: "=" | "!="; value: Date | null;
} | {
  condition: ">=" | "<=" | "<" | ">"; value: Date;
} | {
  condition: "in"; value: ReadonlyArray<Date | null>;
};

class WRDDBDateValue<Required extends boolean> extends WRDAttributeValueBase<(true extends Required ? Date : Date | null), Date | null, (true extends Required ? Date : Date | null), WRDDBDateTimeConditions> {
  getDefaultValue(): Date | null { return null; }
  isSet(value: Date | null) { return Boolean(value); }
  checkFilter({ condition, value }: WRDDBDateTimeConditions) {
    /* always ok */
  }
  matchesValue(value: Date | null, cv: WRDDBDateTimeConditions): boolean {
    if (cv.condition === "in") {
      for (const v of cv.value)
        if (v?.getTime() === value?.getTime())
          return true;
      return false;
    }
    return cmp(value, cv.condition, cv.value);
  }

  getFromRecord(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number): (true extends Required ? Date : Date | null) {
    const parts = entity_settings[settings_start].rawdata.split(",");
    if (Number(parts[0]) >= 2147483647)
      return null as (true extends Required ? Date : Date | null);
    return makeDateFromParts(Number(parts[0]), 0);
  }

  validateInput(value: (true extends Required ? Date : Date | null), checker: ValueQueryChecker, attrPath: string) {
    if (value !== null && (!isDate(value) || isNaN(value.getTime())))
      throw new Error(`Invalid date value for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
    if (this.attr.required && !value && !checker.importMode && (!checker.temp || attrPath))
      throw new Error(`Provided default value for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
    if (value && (value.getTime() <= defaultDateTime.getTime() || value.getTime() > maxDateTimeTotalMsecs))
      throw new Error(`Not allowed use defaultDateTime of maxDateTime for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
  }

  encodeValue(value: (true extends Required ? Date : Date | null)): EncodedValue {
    if (!value)
      return {};

    const parts = dateToParts(value);
    return { settings: { rawdata: parts.days.toString(), attribute: this.attr.id } };
  }
}

class WRDDBBaseDateValue extends WRDAttributeValueBase<Date | null, Date | null, Date | null, WRDDBDateTimeConditions> {
  getDefaultValue(): Date | null { return null; }
  isSet(value: Date | null) { return Boolean(value); }
  validateFilterInput(value: Date | null) {
    if (value && (value.getTime() <= defaultDateTime.getTime() || value.getTime() > maxDateTimeTotalMsecs))
      throw new Error(`Not allowed to use defaultDatetime or maxDatetime, use null`);
  }
  checkFilter(cv: WRDDBDateTimeConditions) {
    if (cv.condition === "in")
      cv.value.forEach(v => this.validateFilterInput(v));
    else
      this.validateFilterInput(cv.value);
  }
  matchesValue(value: Date | null, cv: WRDDBDateTimeConditions): boolean {
    if (cv.condition === "in") {
      return cv.value.includes(value);
    }
    return cmp(value, cv.condition, cv.value);
  }

  addToQuery<O>(query: SelectQueryBuilder<PlatformDB, "wrd.entities", O>, cv: WRDDBDateTimeConditions): AddToQueryResponse<O> {
    let fieldname: "dateofbirth" | "dateofdeath";
    if (this.attr.tag === "wrdDateOfBirth")
      fieldname = "dateofbirth";
    else if (this.attr.tag === "wrdDateOfDeath")
      fieldname = "dateofdeath";
    else
      throw new Error(`Unhandled base string attribute ${JSON.stringify(this.attr.tag)}`);

    if (cv.condition === "in")
      cv.value = cv.value.map(v => v ?? defaultDateTime);
    else
      cv.value ??= defaultDateTime;

    if (cv.condition === "in" && !cv.value.length)
      return null; // no results!

    query = query.where(fieldname, cv.condition, cv.value);
    return { needaftercheck: false, query };
  }

  getFromRecord(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number): Date | null {
    throw new Error(`not used`);
  }

  getValue(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number, entityrec: EntityPartialRec): Date | null {
    let val: Date | undefined;
    if (this.attr.tag === "wrdDateOfBirth")
      val = entityrec.dateofbirth;
    else if (this.attr.tag === "wrdDateOfDeath")
      val = entityrec.dateofdeath;
    else
      throw new Error(`Unhandled base domain attribute ${JSON.stringify(this.attr.tag)}`);
    if (!val || val.getTime() <= defaultDateTime.getTime() || val.getTime() >= maxDateTimeTotalMsecs)
      return null;
    return val;
  }

  validateInput(value: Date | null, checker: ValueQueryChecker, attrPath: string) {
    if (value !== null && (!isDate(value) || isNaN(value.getTime())))
      throw new Error(`Invalid date value for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
    if (this.attr.required && !value && !checker.importMode && (!checker.temp || attrPath))
      throw new Error(`Provided default value for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
    if (value && (value.getTime() <= defaultDateTime.getTime() || value.getTime() > maxDateTimeTotalMsecs))
      throw new Error(`Not allowed to use defaultDatetime or maxDatetime, use null for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
    if (value && this.attr.tag === "wrdDateOfDeath" && value.getTime() > Date.now() && !checker.importMode)
      throw new Error(`Provided date of death in the future for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
  }

  getAttrBaseCells(): keyof EntityPartialRec {
    return getAttrBaseCells(this.attr.tag, ["wrdDateOfBirth", "wrdDateOfDeath"]);
  }

  encodeValue(value: Date | null): EncodedValue {
    return { entity: { [this.getAttrBaseCells()]: value || defaultDateTime } };
  }
}

class WRDDBDateTimeValue<Required extends boolean> extends WRDAttributeValueBase<(true extends Required ? Date : Date | null), Date | null, (true extends Required ? Date : Date | null), WRDDBDateTimeConditions> {
  getDefaultValue(): Date | null { return null; }
  isSet(value: Date | null) { return Boolean(value); }
  checkFilter({ condition, value }: WRDDBDateTimeConditions) {
    /* always ok */
  }
  matchesValue(value: Date | null, cv: WRDDBDateTimeConditions): boolean {
    if (cv.condition === "in") {
      for (const v of cv.value)
        if (v?.getTime() === value?.getTime())
          return true;
      return false;
    }
    return cmp(value, cv.condition, cv.value);
  }

  getFromRecord(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number): (true extends Required ? Date : Date | null) {
    const parts = entity_settings[settings_start].rawdata.split(",");
    if (Number(parts[0]) >= 2147483647)
      return null as (true extends Required ? Date : Date | null);
    return makeDateFromParts(Number(parts[0]), Number(parts[1]));
  }

  validateInput(value: (true extends Required ? Date : Date | null), checker: ValueQueryChecker, attrPath: string) {
    if (value !== null && (!isDate(value) || isNaN(value.getTime())))
      throw new Error(`Invalid date value for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
    if (this.attr.required && !value && !checker.importMode && (!checker.temp || attrPath))
      throw new Error(`Provided default value for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
  }

  encodeValue(value: (true extends Required ? Date : Date | null)): EncodedValue {
    if (!value)
      return {};

    const parts = dateToParts(value);
    return { settings: { rawdata: `${parts.days.toString()},${parts.msecs.toString()}`, attribute: this.attr.id } };
  }
}

type ArraySelectable<Members extends Record<string, SimpleWRDAttributeType | WRDAttrBase>> = {
  [K in keyof Members]: GetResultType<Members[K]>;
};

class WRDDBBaseCreationLimitDateValue extends WRDAttributeValueBase<Date | null, Date | null, Date | null, WRDDBDateTimeConditions> {
  getDefaultValue(): Date | null { return null; }

  isSet(value: Date | null) { return Boolean(value); }

  validateFilterInput(value: Date | null) {
    if (value && (value.getTime() <= defaultDateTime.getTime() || value.getTime() > maxDateTimeTotalMsecs))
      throw new Error(`Not allowed to use defaultDatetime or maxDatetime`);
  }

  checkFilter(cv: WRDDBDateTimeConditions) {
    if (cv.condition === "in")
      cv.value.forEach(v => this.validateFilterInput(v));
    else
      this.validateFilterInput(cv.value);
  }
  matchesValue(value: Date | null, cv: WRDDBDateTimeConditions): boolean {
    if (cv.condition === "in") {
      return cv.value.includes(value);
    }
    return cmp(value, cv.condition, cv.value);
  }

  addToQuery<O>(query: SelectQueryBuilder<PlatformDB, "wrd.entities", O>, cv: WRDDBDateTimeConditions): AddToQueryResponse<O> {
    const defaultMatches = this.matchesValue(this.getDefaultValue(), cv);

    let fieldname: "creationdate" | "limitdate";
    if (this.attr.tag === "wrdCreationDate")
      fieldname = "creationdate";
    else if (this.attr.tag === "wrdLimitDate")
      fieldname = "limitdate";
    else
      throw new Error(`Unhandled base string attribute ${JSON.stringify(this.attr.tag)}`);

    if (cv.condition === "in")
      cv.value = cv.value.map(v => v ?? defaultDateTime);
    else
      cv.value ??= defaultDateTime;

    if (cv.condition === "in" && !cv.value.length)
      return null; // no results!

    const maxDateTimeMatches = this.matchesValue(maxDateTime, cv);
    if (defaultMatches && !maxDateTimeMatches) {
      query = query.where(qb => qb.or([
        qb(fieldname, cv.condition, cv.value),
        qb(fieldname, "=", maxDateTime)
      ]));
    } else {
      query = query.where(fieldname, cv.condition, cv.value);
      if (maxDateTimeMatches && !defaultMatches)
        query = query.where(fieldname, "!=", maxDateTime);
    }
    return { needaftercheck: false, query };
  }

  getFromRecord(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number): Date | null {
    throw new Error(`not used`);
  }

  getValue(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number, entityrec: EntityPartialRec): Date | null {
    let val: Date | number | undefined;
    if (this.attr.tag === "wrdCreationDate")
      val = entityrec.creationdate as Date | number;
    else if (this.attr.tag === "wrdLimitDate")
      val = entityrec.limitdate as Date | number;
    else
      throw new Error(`Unhandled base domain attribute ${JSON.stringify(this.attr.tag)}`);
    if (typeof val === "number") // -Infinity and Infinity
      return null;
    if (!val || val.getTime() <= defaultDateTime.getTime() || val.getTime() >= maxDateTimeTotalMsecs)
      return null;
    return val;
  }

  validateInput(value: Date | null, checker: ValueQueryChecker, attrPath: string) {
    if (value !== null && (!isDate(value) || isNaN(value.getTime())))
      throw new Error(`Invalid date value for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
    // FIXME: check temp mode
    if (value && (value.getTime() <= defaultDateTime.getTime() || value.getTime() > maxDateTimeTotalMsecs))
      throw new Error(`Not allowed to use defaultDatetime or maxDatetime for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}, use null`);
    if (!value && this.attr.tag === "wrdCreationDate" && !checker.temp && !checker.importMode)
      throw new Error(`Not allowed to use \`null\` for attribute ${checker.typeTag}.${attrPath}${this.attr.tag} for non-temp entities`);
  }

  getAttrBaseCells(): keyof EntityPartialRec {
    return getAttrBaseCells(this.attr.tag, ["wrdCreationDate", "wrdLimitDate"]);
  }

  encodeValue(value: Date | null): EncodedValue {
    return { entity: { [this.getAttrBaseCells()]: value ?? maxDateTime } };
  }
}

class WRDDBBaseModificationDateValue extends WRDAttributeValueBase<Date, Date | null, Date, WRDDBDateTimeConditions> {
  getDefaultValue(): Date | null { return null; }

  isSet(value: Date | null) { return Boolean(value); }

  validateFilterInput(value: Date) {
    throw new Error(`Not allowed to use defaultDatetime or maxDatetime`);
  }

  checkFilter(cv: WRDDBDateTimeConditions) {
    if (cv.condition === "in") {
      for (const value of cv.value)
        if (!value)
          throw new Error(`Not allowed to use null in comparisions`);
        else
          this.validateFilterInput(value);
    } else if (!cv.value)
      throw new Error(`Not allowed to use null in comparisions`);
    else
      this.validateFilterInput(cv.value);
  }
  matchesValue(value: Date, cv: WRDDBDateTimeConditions): boolean {
    if (cv.condition === "in") {
      return cv.value.includes(value);
    }
    return cmp(value, cv.condition, cv.value);
  }

  addToQuery<O>(query: SelectQueryBuilder<PlatformDB, "wrd.entities", O>, cv: WRDDBDateTimeConditions): AddToQueryResponse<O> {
    if (cv.condition === "in")
      cv.value = cv.value.map(v => v ?? defaultDateTime);
    else
      cv.value ??= defaultDateTime;

    if (cv.condition === "in" && !cv.value.length)
      return null; // no results!

    query = query.where("modificationdate", cv.condition, cv.value);
    return { needaftercheck: false, query };
  }

  getFromRecord(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number): Date {
    throw new Error(`not used`);
  }

  getValue(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number, entityrec: EntityPartialRec): Date {
    if (!entityrec.modificationdate || entityrec.modificationdate.getTime() <= defaultDateTime.getTime() || entityrec.modificationdate.getTime() >= maxDateTimeTotalMsecs)
      return defaultDateTime;
    return entityrec.modificationdate;
  }

  validateInput(value: Date, checker: ValueQueryChecker, attrPath: string) {
    if (value !== null && (!isDate(value) || isNaN(value.getTime())))
      throw new Error(`Invalid date value for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
    if (!value)
      throw new Error(`Not allowed to use null for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
    if (value.getTime() <= defaultDateTime.getTime() || value.getTime() > maxDateTimeTotalMsecs)
      throw new Error(`Not allowed to use defaultDatetime or maxDatetime for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
  }

  getAttrBaseCells(): keyof EntityPartialRec {
    return getAttrBaseCells(this.attr.tag, ["wrdModificationDate"]);
  }

  encodeValue(value: Date | null): EncodedValue {
    return { entity: { [this.getAttrBaseCells()]: value } };
  }
}

class WRDDBArrayValue<Members extends Record<string, SimpleWRDAttributeType | WRDAttrBase>> extends WRDAttributeValueBase<
  Array<WRDInsertable<Members>>,
  Array<ArraySelectable<Members>>,
  Array<ArraySelectable<Members>>,
  never> {

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  fields = new Array<{ name: keyof Members; accessor: WRDAttributeValueBase<any, any, any, any> }>;

  constructor(attr: AttrRec, parentAttrMap: Map<number | null, AttrRec[]>) {
    super(attr);

    const childAttrs = parentAttrMap.get(attr.id);
    if (childAttrs) {
      for (const childAttr of childAttrs) {
        this.fields.push({
          name: childAttr.tag,
          accessor: getAccessor(childAttr, parentAttrMap)
        });
      }
    }
  }

  getDefaultValue(): Array<ArraySelectable<Members>> { return []; }

  isSet(value: unknown[]) { return Boolean(value?.length); }

  checkFilter({ condition, value }: never) {
    throw new Error(`Filters not allowed on arrays`);
  }

  matchesValue(value: Array<ArraySelectable<Members>>, cv: never): boolean {
    throw new Error(`Filters not allowed on arrays`);
  }

  /** Try to add wheres to the database query on wrd.entities to filter out non-matches for this filter
   * @typeParam O - Output map for the database query
   * @param query - Database query
   * @param cv - Condition and value to compare with
   * @returns Whether after-filtering is necessary and updated query
   */
  addToQuery<O>(query: SelectQueryBuilder<PlatformDB, "wrd.entities", O>, cv: never): AddToQueryResponse<O> {
    throw new Error(`Filters not allowed on arrays`);
  }

  /** Returns true all the values in a filter match the default value
   * @param cv - Condition+value to check
   * @returns true if all values match the default value
   */
  containsOnlyDefaultValues<CE extends never>(cv: CE): boolean {
    throw new Error(`Filters not allowed on arrays`);
  }

  /** Given a list of entity settings, extract the return value for a field
   * @param entity_settings - List of entity settings
   * @param settings_start - Position where settings for this attribute start
   * @param settings_limit - Limit of setting for this attribute, is always greater than settings_start
   */
  getFromRecord(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number, links: EntitySettingsWHFSLinkRec[]): Array<ArraySelectable<Members>> {
    throw new Error(`Not implemented yet`);
  }

  /** Given a list of entity settings, extract the return value for a field
   * @param entity_settings - List of entity settings
   * @param settings_start - Position where settings for this attribute start
   * @param settings_limit - Limit of setting for this attribute, may be the same as settings_start)
   * @returns The parsed value. The return type of this function is used to determine the selection output type for a attribute.
   */
  getValue(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number, row: EntityPartialRec, links: EntitySettingsWHFSLinkRec[], cc: number): Array<ArraySelectable<Members>> | Promise<Array<ArraySelectable<Members>>> {
    type RowType = ArraySelectable<Members>;

    if (settings_limit <= settings_start)
      return this.getDefaultValue() as RowType[]; // Cast is needed because for required fields, Out may not extend Default.

    const retval: RowType[] = [];
    const promises: Array<{ //We try to return promises only if really needed. Track which ones we want to resolve..
      row: RowType;
      member: keyof Members;
    }> = [];

    for (let idx = settings_start; idx < settings_limit; ++idx) {
      const settingid = entity_settings[idx].id;
      const rec = {} as ArraySelectable<Members> & { [wrdSettingId]: number };
      for (const field of this.fields) {
        const lb = recordLowerBound(entity_settings, { attribute: field.accessor.attr.id, parentsetting: settingid }, ["attribute", "parentsetting"]);
        const ub = recordUpperBound(entity_settings, { attribute: field.accessor.attr.id, parentsetting: settingid }, ["attribute", "parentsetting"]);
        rec[field.name] = field.accessor.getValue(entity_settings, lb.position, ub, row, links, cc);

        if (isPromise(rec[field.name])) {
          promises.push({ row: rec, member: field.name });
        }
      }
      rec[wrdSettingId] = settingid;
      retval.push(rec);
    }

    if (!promises.length)
      return retval;

    return (async function () {
      const results = await Promise.all(promises.map(rec => rec.row[rec.member]));
      for (const [idx, promise] of promises.entries()) {
        promise.row[promise.member] = results[idx];
      }
      return retval;
    })();
  }

  /** Check the contents of a value used to insert or update a value
   * @param value - The value to check. The type of this value is used to determine which type is accepted in an insert or update.
   */
  validateInput(value: Array<WRDInsertable<Members>>, checker: ValueQueryChecker, attrPath: string) {
    const eltBasePath = attrPath + this.attr.tag + "[";
    for (const [idx, row] of value.entries()) {
      const eltPath = eltBasePath + idx + '].';
      for (const field of this.fields) {
        if (field.name in row)
          field.accessor.validateInput(row[field.name as keyof typeof row], checker, eltPath);
        else if (field.accessor.attr.required && !checker.importMode)
          throw new Error(`Missing required field ${JSON.stringify(field.name)} for attribute ${checker.typeTag}.${eltBasePath}${field.name as string}`);
      }
    }
  }

  getAttrIds(): number | number[] {
    const retval = [this.attr.id];
    for (const field of this.fields) {
      const childIds = field.accessor.getAttrIds();
      if (typeof childIds === "number")
        retval.push(childIds);
      else
        retval.push(...childIds);
    }
    return retval;
  }

  encodeValue(value: Array<WRDInsertable<Members> & { [wrdSettingId]?: number }>): AwaitableEncodedValue {
    return {
      settings: value.map((row, idx): AwaitableEncodedSetting => {
        // if a setting id is present in an element (stored with wrdSettingId symbol), include it for re-use
        const retval: EncodedSetting = { attribute: this.attr.id, ordering: idx + 1, id: row[wrdSettingId] };
        const subs: NonNullable<AwaitableEncodedSetting["sub"]> = [];
        for (const field of this.fields) {
          if (field.name in row) {
            const subSettings = field.accessor.encodeValue(row[field.name as keyof typeof row]);
            if (subSettings.settings)
              if (Array.isArray(subSettings.settings))
                subs.push(...subSettings.settings);
              else
                subs.push(subSettings.settings);
          }
        }
        return { ...retval, sub: subs };
      })
    };
  }
}

export abstract class WRDAttributeUncomparableValueBase<In, Default, Out extends Default> extends WRDAttributeValueBase<In, Default, Out, never> {
  checkFilter(cv: never): void {
    throw new Error(`Cannot compare values of type ${WRDAttributeTypeId[this.attr.attributetype]}`);
  }

  matchesValue(value: unknown, cv: never): boolean {
    throw new Error(`Cannot compare values of type  ${WRDAttributeTypeId[this.attr.attributetype]}`);
  }

  addToQuery<O>(query: SelectQueryBuilder<PlatformDB, "wrd.entities", O>, cv_org: never): AddToQueryResponse<O> {
    throw new Error(`Cannot compare values of type  ${WRDAttributeTypeId[this.attr.attributetype]}`);
  }

  containsOnlyDefaultValues(cv: never): boolean {
    throw new Error(`Cannot compare values of type  ${WRDAttributeTypeId[this.attr.attributetype]}`);
  }
}

class WRDDBJSONValue<Required extends boolean, JSONType extends object> extends WRDAttributeUncomparableValueBase<JSONType | NullIfNotRequired<Required>, JSONType | null, JSONType | NullIfNotRequired<Required>> {
  /** Returns the default value for a value with no settings
      @returns Default value for this type
  */
  getDefaultValue(): JSONType | null {
    return null;
  }

  isSet(value: object | null) { return Boolean(value); }

  getFromRecord(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number): JSONType | NullIfNotRequired<Required> {
    const data = this.decodeAsStringWithOverlow(entity_settings, settings_start, settings_limit);
    return data ? parseTyped(data) : null;
  }

  validateInput(value: JSONType | NullIfNotRequired<Required>, checker: ValueQueryChecker, attrPath: string): void {
    if (!value && this.attr.required && !checker.importMode && (!checker.temp || attrPath))
      throw new Error(`Provided default value for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
  }

  encodeValue(value: JSONType | NullIfNotRequired<Required>): AwaitableEncodedValue {
    return this.encodeAsStringWithOverlow(value ? stringify(value, { typed: true }) : '');
  }
}

class WRDDBRecordValue extends WRDAttributeUncomparableValueBase<object | null, IPCMarshallableRecord | null, IPCMarshallableRecord | null> {
  /** Returns the default value for a value with no settings
      @returns Default value for this type
  */
  getDefaultValue(): IPCMarshallableRecord | null {
    return null;
  }

  isSet(value: object | null) { return Boolean(value); }

  getFromRecord(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number): IPCMarshallableRecord | null {
    const data = this.decodeAsStringWithOverlow(entity_settings, settings_start, settings_limit);
    return data ? decodeHSON(data) as IPCMarshallableRecord : null;
  }

  validateInput(value: object | null, checker: ValueQueryChecker, attrPath: string): void {
    if (!value && this.attr.required && !checker.importMode && (!checker.temp || attrPath))
      throw new Error(`Provided default value for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
  }

  encodeValue(value: object | null): AwaitableEncodedValue {
    return this.encodeAsStringWithOverlow(value ? encodeHSON(value as IPCMarshallableData) : '');
  }

}

//TODO {data: Buffer} is for 5.3 compatibility and we might have to just remove it
class WHDBResourceAttributeBase extends WRDAttributeUncomparableValueBase<ResourceDescriptor | null | { data: Buffer }, ResourceDescriptor | null, ResourceDescriptor | null> {
  /** Returns the default value for a value with no settings
      @returns Default value for this type
  */
  getDefaultValue(): ResourceDescriptor | null {
    return null;
  }

  isSet(value: ResourceDescriptor | null) { return Boolean(value); }

  getFromRecord(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number, links: EntitySettingsWHFSLinkRec[], cc: number): ResourceDescriptor | null {
    const val = entity_settings[settings_start];
    const lpos = recordLowerBound(links, val, ["id"]);
    const sourceFile = lpos.found ? links[lpos.position].fsobject : null;
    //See also GetWrappedObjectFromWRDSetting: rawdata is prefixed with WHFS: if we need to pick up a link
    const hasSourceFile = val.rawdata.startsWith("WHFS:");
    const meta = {
      ...decodeScanData(val.rawdata.substring(hasSourceFile ? 5 : 0)),
      sourceFile,
      dbLoc: { source: 3, id: val.id, cc }
    };

    const blob = val.blobdata;
    return new ResourceDescriptor(blob, meta);
  }

  validateInput(value: ResourceDescriptor | null | { data: Buffer }, checker: ValueQueryChecker, attrPath: string): void {
    if (!value && this.attr.required && !checker.importMode && (!checker.temp || attrPath))
      throw new Error(`Provided default value for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
  }

  encodeValue(value: ResourceDescriptor | null | { data: Buffer }): AwaitableEncodedValue {
    if (!value)
      return {};

    return {
      settings: (async (): Promise<EncodedSetting[]> => {
        if ("data" in value)
          value = await ResourceDescriptor.from(value.data);
        const rawdata = (value.sourceFile ? "WHFS:" : "") + await addMissingScanData(value);
        if (value.resource.size)
          await uploadBlob(value.resource);
        const setting: EncodedSetting = { rawdata, blobdata: value.resource.size ? value.resource : null, attribute: this.attr.id, id: value.dbLoc?.id };
        if (value.sourceFile) {
          setting.linktype = 2;
          setting.link = value.sourceFile;
        }
        return [setting];
      })()
    };
  }
}

class WRDDBFileValue extends WHDBResourceAttributeBase { }

class WRDDBImageValue extends WHDBResourceAttributeBase { }

class WRDDBRichDocumentValue extends WRDAttributeUncomparableValueBase<RichTextDocument | null, RichTextDocument | null, RichTextDocument | null> {
  getDefaultValue(): RichTextDocument | null {
    return null;
  }

  isSet(value: RichTextDocument | null) { return Boolean(value); }

  getFromRecord(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number, links: EntitySettingsWHFSLinkRec[], cc: number): null | Promise<RichTextDocument | null> {
    const val = entity_settings[settings_start];
    if (!val.blobdata) {
      const matchlink = links.find(_ => _.id === val.id);
      return matchlink ? getRTDFromWHFS(matchlink.fsobject) : null;
    }

    return buildRTDFromHareScriptRTD({ htmltext: val.blobdata, instances: [], embedded: [], links: [] });
  }

  validateInput(value: RichTextDocument | null | { data: Buffer }, checker: ValueQueryChecker, attrPath: string): void {
    if (!value && this.attr.required && !checker.importMode && (!checker.temp || attrPath))
      throw new Error(`Provided default value for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
  }

  encodeValue(value: RichTextDocument | null): AwaitableEncodedValue {
    if (!value || !value?.blocks.length)
      return {};

    return {
      settings: (async (): Promise<EncodedSetting[]> => {
        //FIXME: Encode links and instances (which are currently not yet supported by RichDocument anyway)
        //FIXME Reuse existing documents/settings

        const hsRTD = await exportAsHareScriptRTD(value);
        //do we need to use WHFS to store this document ?
        const inWHFS = hsRTD.links.length > 0 || hsRTD.instances.length > 0; //TODO what about embedded images ? can they have a source object?
        if (inWHFS) {
          //TODO avoid a full exportAsHareScriptRTD when just exporting to WHFS - we need a requiresWHFS or refersWHFS() in a RichDocument
          const whfsId = await storeRTDinWHFS(this.attr.schemaId, value);
          const setting: EncodedSetting = { rawdata: "WHFS", blobdata: null, attribute: this.attr.id, linktype: LinkTypes.RTD, link: whfsId };
          return [setting];
        }

        const rawdata = await addMissingScanData(await ResourceDescriptor.from(hsRTD.htmltext, { fileName: "rd1.html", getHash: true, mediaType: "text/html" }));
        await uploadBlob(hsRTD.htmltext);
        const setting: EncodedSetting = { rawdata, blobdata: hsRTD.htmltext, attribute: this.attr.id };
        return [setting];
      })()
    };
  }
}

class WRDDBWHFSIntextlinkValue extends WRDAttributeUncomparableValueBase<IntExtLink | null, IntExtLink | null, IntExtLink | null> {
  getDefaultValue(): IntExtLink | null {
    return null;
  }

  isSet(value: IntExtLink | null) { return Boolean(value); }

  getFromRecord(entity_settings: EntitySettingsRec[], settings_start: number, _settings_limit: number, links: EntitySettingsWHFSLinkRec[], _cc: number): IntExtLink | null {
    const setting = entity_settings[settings_start];
    if (!setting.rawdata)
      return null;

    let result: IntExtLink | null = null;
    if (setting.rawdata.startsWith("*")) // external link
      result = new IntExtLink(setting.rawdata.substring(1));
    else if (setting.rawdata === "WHFS" || setting.rawdata.startsWith("WHFS:")) {
      const target = links.filter(_ => _.id === setting.id)[0]?.fsobject;
      if (target)
        result = new IntExtLink(target, { append: setting.rawdata.substring(5) });
    } else
      throw new Error("Unrecognized whfs int/extlink format");
    return result;
  }

  validateInput(value: IntExtLink | null, checker: ValueQueryChecker, attrPath: string) {
    if (!value && this.attr.required && !checker.importMode && (!checker.temp || attrPath))
      throw new Error(`Provided default value for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
  }

  encodeValue(value: IntExtLink | null) {
    if (!value)
      return {};

    if (value.internalLink) {
      return { settings: { rawdata: "WHFS" + (value.append ? ":" + value.append : ""), attribute: this.attr.id, link: value.internalLink, linktype: LinkTypes.FSObject } };
    } else if (value.externalLink) {
      return { settings: { rawdata: "*" + value.externalLink, attribute: this.attr.id } };
    }
    throw new Error("Invalid whfs int/extlink");
  }
}

type WRDDBInteger64Conditions = {
  condition: "<" | "<=" | "=" | "!=" | ">=" | ">"; value: bigint | number;
} | {
  condition: "in"; value: readonly bigint[];
} | {
  condition: "mentions"; value: bigint | number;
} | {
  condition: "mentionsany"; value: readonly bigint[];
};
class WRDDBInteger64Value extends WRDAttributeValueBase<bigint | number, bigint, bigint, WRDDBInteger64Conditions> {
  getDefaultValue() { return 0n; }
  isSet(value: bigint) { return Boolean(value); }
  checkFilter({ condition, value }: WRDDBInteger64Conditions) {
    // type-check is enough (for now)
  }
  matchesValue(value: bigint, cv: WRDDBInteger64Conditions): boolean {
    if (cv.condition === "in" || cv.condition === "mentionsany")
      return cv.value.includes(value);
    if (cv.condition === "mentions")
      cv = { condition: "=", value: cv.value };
    return cmp(value, cv.condition, cv.value);
  }

  addToQuery<O>(query: SelectQueryBuilder<PlatformDB, "wrd.entities", O>, cv: WRDDBInteger64Conditions): AddToQueryResponse<O> {
    if (cv.condition === "mentions")
      cv = { condition: "=", value: cv.value };
    else if (cv.condition === "mentionsany")
      cv = { condition: "in", value: cv.value };

    const defaultmatches = this.matchesValue(this.getDefaultValue(), cv);
    if (cv.condition === "in" && !cv.value.length)
      return null;

    if (cv.condition === "in") {
      query = addQueryFilter2(query, this.attr.id, defaultmatches, b => b(sql<bigint>`rawdata::NUMERIC(1000)`, cv.condition, cv.value));
    } else {
      const value = typeof cv.value === "number" ? BigInt(cv.value) : cv.value;
      query = addQueryFilter2(query, this.attr.id, defaultmatches, b => b(sql<bigint>`rawdata::NUMERIC(1000)`, cv.condition, value));
    }

    return {
      needaftercheck: false,
      query
    };
  }

  getFromRecord(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number): bigint {
    return BigInt(entity_settings[settings_start].rawdata);
  }

  validateInput(value: bigint, checker: ValueQueryChecker, attrPath: string) {
    if (typeof value === "number")
      value = BigInt(value);
    if (this.attr.required && !value && !checker.importMode)
      throw new Error(`Provided default value for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
    if (value >= 2n ** 63n)
      throw new Error(`Integer64 out of range for ${checker.typeTag}.${attrPath}${this.attr.tag}: ${value} > 2^63-1`);
    if (value < -(2n ** 63n))
      throw new Error(`Integer64 out of range for ${checker.typeTag}.${attrPath}${this.attr.tag}: ${value} < -2^63`);
    if (value && this.attr.isunique)
      checker.addUniqueCheck(this.attr.fullTag, value, attrPath + this.attr.tag);
  }

  encodeValue(value: bigint): EncodedValue {
    return value ? { settings: { rawdata: String(value), attribute: this.attr.id } } : {};
  }
}

type WRDDBMoneyConditions = {
  condition: "<" | "<=" | "=" | "!=" | ">=" | ">"; value: Money;
} | {
  condition: "in"; value: readonly Money[];
} | {
  condition: "mentions"; value: Money;
} | {
  condition: "mentionsany"; value: readonly Money[];
};

class WRDDBMoneyValue extends WRDAttributeValueBase<Money, Money, Money, WRDDBMoneyConditions> {
  getDefaultValue() { return new Money("0"); }
  isSet(value: Money) { return Money.cmp(value, "0") !== 0; }
  checkFilter({ condition, value }: WRDDBMoneyConditions) {
    // type-check is enough (for now)
  }
  matchesValue(value: Money, cv: WRDDBMoneyConditions): boolean {
    if (cv.condition === "in" || cv.condition === "mentionsany")
      return cv.value.some(v => Money.cmp(value, v) === 0);
    const cmpres = Money.cmp(value, cv.value);
    switch (cv.condition) {
      case "=": return cmpres === 0;
      case "mentions": return cmpres === 0;
      case ">=": return cmpres >= 0;
      case "<=": return cmpres <= 0;
      case "<": return cmpres < 0;
      case ">": return cmpres > 0;
      case "!=": return cmpres !== 0;
    }
  }

  addToQuery<O>(query: SelectQueryBuilder<PlatformDB, "wrd.entities", O>, cv: WRDDBMoneyConditions): AddToQueryResponse<O> {
    if (cv.condition === "mentions")
      cv = { condition: "=", value: cv.value };
    else if (cv.condition === "mentionsany")
      cv = { condition: "in", value: cv.value };

    const defaultmatches = this.matchesValue(this.getDefaultValue(), cv);

    if (cv.condition === "in") {
      if (!cv.value.length)
        return null;
    }

    query = addQueryFilter2(query, this.attr.id, defaultmatches, b => b(sql<Money>`rawdata::NUMERIC(1000,5)`, cv.condition, cv.value));

    return {
      needaftercheck: false,
      query
    };
  }

  getFromRecord(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number): Money {
    return new Money(entity_settings[settings_start].rawdata);
  }

  validateInput(value: Money, checker: ValueQueryChecker, attrPath: string) {
    if (this.attr.required && (!value || !Money.cmp(value, "0")) && !checker.importMode)
      throw new Error(`Provided default value for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
  }

  encodeValue(value: Money): EncodedValue {
    return value ? { settings: { rawdata: String(value), attribute: this.attr.id } } : {};
  }
}

class WRDDBPasswordValue extends WRDAttributeUncomparableValueBase<AuthenticationSettings | null, AuthenticationSettings | null, AuthenticationSettings | null> {
  getDefaultValue(): null {
    return null;
  }

  isSet(value: AuthenticationSettings | null): boolean {
    return Boolean(value);
  }

  getFromRecord(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number): AuthenticationSettings | null {
    return AuthenticationSettings.fromPasswordHash(entity_settings[settings_start].rawdata);
  }

  validateInput(value: AuthenticationSettings | null): void {
    /* always valid */
  }

  encodeValue(value: AuthenticationSettings | null): AwaitableEncodedValue {
    // For now this may also save us from having to port the Password->AuthenticationSetting migration from HS to TS. Would be nice to leave that all behind in HS
    throw new Error(`Writing password values is not supported in the TypeScript API - the schema needs to switch to AuthenticationSettings`);
  }
}

class WRDDBAddressValue<Required extends boolean> extends WRDAttributeUncomparableValueBase<AddressValue | NullIfNotRequired<Required>, AddressValue | null, AddressValue | NullIfNotRequired<Required>> {
  getDefaultValue(): AddressValue | null {
    return null;
  }

  isSet(value: AddressValue | null): boolean {
    return Boolean(value);
  }

  getFromRecord(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number, links: EntitySettingsWHFSLinkRec[], cc: number): AddressValue | NullIfNotRequired<Required> {
    const data = this.decodeAsStringWithOverlow(entity_settings, settings_start, settings_limit);
    const parsed = JSON.parse(data) as AddressValue & { nr_detail: string };
    return { ...omit(parsed, ["nr_detail"]), houseNumber: parsed.nr_detail };
  }

  validateInput(value: AddressValue | NullIfNotRequired<Required>, checker: ValueQueryChecker, attrPath: string): void {
    if (!value) {
      if (this.attr.required && !checker.importMode)
        throw new Error(`Provided default value for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
      return;
    }
    if ("nr_detail" in value)
      throw new Error(`AddressValue should not contain nr_detail for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}, use houseNumber instead`);
    if ("housenumber" in value)
      throw new Error(`AddressValue should not contain housenumber for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}, use houseNumber instead (did you route the address value through HareScript?)`);
    if (value.country?.length !== 2)
      throw new Error(`The field 'country' is required in an address for attribute ${checker.typeTag}.${attrPath}${this.attr.tag} and must be a 2 character code`);
    if (value.country !== toCLocaleUppercase(value.country))
      throw new Error(`The field 'country' must be uppercase for attribute ${checker.typeTag}.${attrPath}${this.attr.tag}`);
  }

  encodeValue(value: AddressValue | NullIfNotRequired<Required>): AwaitableEncodedValue {
    return this.encodeAsStringWithOverlow(value ? JSON.stringify({ ...omit(value, ["houseNumber"]), nr_detail: value.houseNumber }) : '');
  }
}

class WRDDBAuthenticationSettingsValue extends WRDAttributeUncomparableValueBase<AuthenticationSettings | null, AuthenticationSettings | null, AuthenticationSettings | null> {
  getDefaultValue(): null {
    return null;
  }

  isSet(value: AuthenticationSettings | null): boolean {
    return Boolean(value);
  }

  getFromRecord(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number): AuthenticationSettings | null {
    const data = this.decodeAsStringWithOverlow(entity_settings, settings_start, settings_limit);
    return AuthenticationSettings.fromHSON(data);
  }

  validateInput(value: AuthenticationSettings | null): void {
    /* always valid */
  }

  encodeValue(value: AuthenticationSettings | null): AwaitableEncodedValue {
    return this.encodeAsStringWithOverlow(value ? value.toHSON() : '');
  }
}

export class WRDAttributeUnImplementedValueBase<In, Default, Out extends Default, C extends { condition: AllowedFilterConditions; value: unknown } = { condition: AllowedFilterConditions; value: unknown }> extends WRDAttributeValueBase<In, Default, Out, C> {
  throwError(): never {
    throw new Error(`Unimplemented accessor for type ${WRDAttributeTypeId[this.attr.attributetype] ?? WRDBaseAttributeTypeId[this.attr.attributetype]} (tag: ${JSON.stringify(this.attr.tag)})`);
  }

  /** Returns the default value for a value with no settings
      @returns Default value for this type
  */
  getDefaultValue(): Default {
    this.throwError();
  }

  isSet(value: Default): boolean {
    this.throwError();
  }

  checkFilter(cv: C): void {
    this.throwError();
  }

  matchesValue(value: unknown, cv: C): boolean {
    this.throwError();
  }

  addToQuery<O>(query: SelectQueryBuilder<PlatformDB, "wrd.entities", O>, cv_org: C): AddToQueryResponse<O> {
    this.throwError();
  }

  containsOnlyDefaultValues<CE extends C>(cv: CE): boolean {
    this.throwError();
  }

  getFromRecord(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number): Out {
    this.throwError();
  }

  getValue(entity_settings: EntitySettingsRec[], settings_start: number, settings_limit: number, row: EntityPartialRec): Out {
    this.throwError();
  }

  validateInput(value: In): void {
    this.throwError();
  }

  encodeValue(value: In): EncodedValue {
    this.throwError();
  }
}

type GetEnumArrayAllowedValues<Options extends { allowedValues: string }> = Options extends { allowedValues: infer V } ? V : never;

/// The following accessors are not implemented yet
class WRDDBWHFSInstanceValue extends WRDAttributeUnImplementedValueBase<unknown, unknown, unknown> { }
class WRDDBPaymentProviderValue extends WRDAttributeUnImplementedValueBase<unknown, unknown, unknown> { }
class WRDDBPaymentValue extends WRDAttributeUnImplementedValueBase<unknown, unknown, unknown> { }
class WRDDBWHFSLinkValue extends WRDAttributeUnImplementedValueBase<unknown, unknown, unknown> { }

/// Map for all attribute types that have no options
type SimpleTypeMap<Required extends boolean> = {
  [WRDBaseAttributeTypeId.Base_Integer]: WRDDBIntegerValue;
  [WRDBaseAttributeTypeId.Base_Guid]: WRDDBBaseGuidValue;
  [WRDBaseAttributeTypeId.Base_Tag]: WRDDBBaseStringValue;
  [WRDBaseAttributeTypeId.Base_CreationLimitDate]: WRDDBBaseCreationLimitDateValue;
  [WRDBaseAttributeTypeId.Base_ModificationDate]: WRDDBBaseModificationDateValue;
  [WRDBaseAttributeTypeId.Base_Date]: WRDDBDateValue<false>;
  [WRDBaseAttributeTypeId.Base_GeneratedString]: WRDDBStringValue;
  [WRDBaseAttributeTypeId.Base_NameString]: WRDDBBaseStringValue;
  [WRDBaseAttributeTypeId.Base_Domain]: WRDDBBaseDomainValue<Required>;
  [WRDBaseAttributeTypeId.Base_Gender]: WRDDBBaseGenderValue;
  [WRDBaseAttributeTypeId.Base_FixedDomain]: WRDDBBaseDomainValue<true>;

  [WRDAttributeTypeId.String]: WRDDBStringValue;
  [WRDAttributeTypeId.Email]: WRDDBEmailValue;
  [WRDAttributeTypeId.Telephone]: WRDDBStringValue;
  [WRDAttributeTypeId.URL]: WRDDBUrlValue;
  [WRDAttributeTypeId.Boolean]: WRDDBBooleanValue;
  [WRDAttributeTypeId.Integer]: WRDDBIntegerValue;
  [WRDAttributeTypeId.Date]: WRDDBDateValue<Required>;
  [WRDAttributeTypeId.DateTime]: WRDDBDateTimeValue<Required>;
  [WRDAttributeTypeId.Domain]: WRDDBDomainValue<Required>;
  [WRDAttributeTypeId.DomainArray]: WRDDBDomainArrayValue;
  [WRDAttributeTypeId.Address]: WRDDBAddressValue<Required>;
  [WRDAttributeTypeId.Password]: WRDDBPasswordValue;
  [WRDAttributeTypeId.Image]: WRDDBImageValue;
  [WRDAttributeTypeId.File]: WRDDBFileValue;
  [WRDAttributeTypeId.Money]: WRDDBMoneyValue;
  [WRDAttributeTypeId.RichDocument]: WRDDBRichDocumentValue;
  [WRDAttributeTypeId.Integer64]: WRDDBInteger64Value;
  [WRDAttributeTypeId.WHFSInstance]: WRDDBWHFSInstanceValue;
  [WRDAttributeTypeId.WHFSIntExtLink]: WRDDBWHFSIntextlinkValue;
  [WRDAttributeTypeId.HSON]: WRDDBRecordValue;
  [WRDAttributeTypeId.PaymentProvider]: WRDDBPaymentProviderValue;
  [WRDAttributeTypeId.Payment]: WRDDBPaymentValue;
  [WRDAttributeTypeId.AuthenticationSettings]: WRDDBAuthenticationSettingsValue;
  [WRDAttributeTypeId.WHFSRef]: WRDDBWHFSLinkValue;
  [WRDAttributeTypeId.Time]: WRDDBIntegerValue;
};

/** Returns the accessor for a WRDAttr record
 * @typeParam T - WRDAttr type
 * @returns Accessor (extends WRDAttributeValueBase)
 */
export type AccessorType<T extends WRDAttrBase> = T["__attrtype"] extends keyof SimpleTypeMap<T["__required"]>
  ? SimpleTypeMap<T["__required"]>[T["__attrtype"]]
  : (T extends { __attrtype: WRDAttributeTypeId.Enum }
    ? WRDDBEnumValue<T["__options"], T["__required"]>
    : (T extends { __attrtype: WRDAttributeTypeId.EnumArray }
      ? WRDDBEnumArrayValue<T["__options"]>
      : (T extends { __attrtype: WRDAttributeTypeId.DeprecatedStatusRecord }
        ? WRDDBStatusRecordValue<T["__options"], T["__required"]>
        : (T extends { __attrtype: WRDAttributeTypeId.Array }
          ? WRDDBArrayValue<T["__options"]["members"]>
          : (T extends { __attrtype: WRDAttributeTypeId.JSON }
            ? WRDDBJSONValue<T["__required"], T["__options"]["type"]>
            : never)))));


export function getAccessor<T extends WRDAttrBase>(
  attrinfo: AttrRec & { attributetype: T["__attrtype"]; required: T["__required"] },
  parentAttrMap: Map<number | null, AttrRec[]>,
): AccessorType<T> {
  switch (attrinfo.attributetype) {
    case WRDBaseAttributeTypeId.Base_Integer: return new WRDDBBaseIntegerValue(attrinfo) as AccessorType<T>;
    case WRDBaseAttributeTypeId.Base_Guid: return new WRDDBBaseGuidValue(attrinfo) as AccessorType<T>;
    case WRDBaseAttributeTypeId.Base_Tag: return new WRDDBBaseStringValue(attrinfo) as AccessorType<T>;
    case WRDBaseAttributeTypeId.Base_CreationLimitDate: return new WRDDBBaseCreationLimitDateValue(attrinfo) as AccessorType<T>;
    case WRDBaseAttributeTypeId.Base_ModificationDate: return new WRDDBBaseModificationDateValue(attrinfo) as AccessorType<T>;
    case WRDBaseAttributeTypeId.Base_Date: return new WRDDBBaseDateValue(attrinfo) as AccessorType<T>;
    case WRDBaseAttributeTypeId.Base_GeneratedString: return new WRDDBBaseGeneratedStringValue(attrinfo) as AccessorType<T>;
    case WRDBaseAttributeTypeId.Base_NameString: return new WRDDBBaseStringValue(attrinfo) as AccessorType<T>;
    case WRDBaseAttributeTypeId.Base_Domain: return new WRDDBBaseDomainValue<T["__required"]>(attrinfo) as AccessorType<T>;
    case WRDBaseAttributeTypeId.Base_Gender: return new WRDDBBaseGenderValue(attrinfo) as AccessorType<T>; // WRDDBBaseGenderValue
    case WRDBaseAttributeTypeId.Base_FixedDomain: return new WRDDBBaseDomainValue<true>(attrinfo) as AccessorType<T>;

    case WRDAttributeTypeId.String: return new WRDDBStringValue(attrinfo) as AccessorType<T>;
    case WRDAttributeTypeId.Email: return new WRDDBEmailValue(attrinfo) as AccessorType<T>;
    case WRDAttributeTypeId.Telephone: return new WRDDBStringValue(attrinfo) as AccessorType<T>;
    case WRDAttributeTypeId.URL: return new WRDDBUrlValue(attrinfo) as AccessorType<T>;
    case WRDAttributeTypeId.Boolean: return new WRDDBBooleanValue(attrinfo) as AccessorType<T>;
    case WRDAttributeTypeId.Integer:
    case WRDAttributeTypeId.Time:
      return new WRDDBIntegerValue(attrinfo) as AccessorType<T>;
    case WRDAttributeTypeId.Date: return new WRDDBDateValue<T["__required"]>(attrinfo) as AccessorType<T>;
    case WRDAttributeTypeId.DateTime: return new WRDDBDateTimeValue<T["__required"]>(attrinfo) as AccessorType<T>;
    case WRDAttributeTypeId.Domain: return new WRDDBDomainValue<T["__required"]>(attrinfo) as AccessorType<T>;
    case WRDAttributeTypeId.DomainArray: return new WRDDBDomainArrayValue(attrinfo) as AccessorType<T>;
    case WRDAttributeTypeId.Address: return new WRDDBAddressValue(attrinfo) as AccessorType<T>;
    case WRDAttributeTypeId.Password: return new WRDDBPasswordValue(attrinfo) as AccessorType<T>;
    case WRDAttributeTypeId.Image: return new WRDDBImageValue(attrinfo) as AccessorType<T>;
    case WRDAttributeTypeId.File: return new WRDDBFileValue(attrinfo) as AccessorType<T>;
    case WRDAttributeTypeId.Money: return new WRDDBMoneyValue(attrinfo) as AccessorType<T>;
    case WRDAttributeTypeId.RichDocument: return new WRDDBRichDocumentValue(attrinfo) as AccessorType<T>;
    case WRDAttributeTypeId.Integer64: return new WRDDBInteger64Value(attrinfo) as AccessorType<T>;
    case WRDAttributeTypeId.WHFSInstance: return new WRDDBWHFSInstanceValue(attrinfo) as AccessorType<T>;
    case WRDAttributeTypeId.WHFSIntExtLink: return new WRDDBWHFSIntextlinkValue(attrinfo) as AccessorType<T>;
    case WRDAttributeTypeId.HSON: return new WRDDBRecordValue(attrinfo) as AccessorType<T>;
    case WRDAttributeTypeId.PaymentProvider: return new WRDDBPaymentProviderValue(attrinfo) as AccessorType<T>;
    case WRDAttributeTypeId.Payment: return new WRDDBPaymentValue(attrinfo) as AccessorType<T>;
    case WRDAttributeTypeId.AuthenticationSettings: return new WRDDBAuthenticationSettingsValue(attrinfo) as AccessorType<T>;
    case WRDAttributeTypeId.WHFSRef: return new WRDDBWHFSLinkValue(attrinfo) as AccessorType<T>;

    case WRDAttributeTypeId.Enum: return new WRDDBEnumValue<{ allowedValues: (T["__options"] & { allowedValues: string })["allowedValues"] }, T["__required"]>(attrinfo) as AccessorType<T>;
    case WRDAttributeTypeId.EnumArray: return new WRDDBEnumArrayValue<{ allowedValues: (T["__options"] & { allowedValues: string })["allowedValues"] }>(attrinfo) as AccessorType<T>;
    case WRDAttributeTypeId.Array: return new WRDDBArrayValue<(T["__options"] & { members: Record<string, SimpleWRDAttributeType | WRDAttrBase> })["members"]>(attrinfo, parentAttrMap) as AccessorType<T>;
    case WRDAttributeTypeId.JSON: return new WRDDBJSONValue<T["__required"], (T["__options"] & { type: object })["type"]>(attrinfo) as AccessorType<T>;
    case WRDAttributeTypeId.DeprecatedStatusRecord: return new WRDDBStatusRecordValue<T["__options"] & { allowedValues: string; type: object }, T["__required"]>(attrinfo) as AccessorType<T>;
  }
  throw new Error(`Unhandled attribute type ${(attrinfo.attributetype < 0 ? WRDBaseAttributeTypeId[attrinfo.attributetype] : WRDAttributeTypeId[attrinfo.attributetype]) ?? attrinfo.attributetype}`);
}
