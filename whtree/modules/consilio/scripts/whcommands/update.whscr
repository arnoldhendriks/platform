<?wh
// syntax: [mask]
// short: manually update catalogs and mappings

LOADLIB "wh::os.whlib";

LOADLIB "mod::consilio/lib/internal/updateindices.whlib";

LOADLIB "mod::system/lib/database.whlib";

MACRO Main()
{
  RECORD ARRAY options := [ [ name := "zapbroken", type := "switch" ]
                          , [ name := "zapanymismatch", type := "switch" ]
                          , [ name := "catalogmask", type := "param" ]
                          ];

  RECORD cmdargs := ParseArguments(GetConsoleArguments(), options);
  IF(NOT RecordExists(cmdargs))
  {
    Print("Syntax: wh consilio:updatecatalogs [options] [mask]\n");
    Print("  --zapbroken:          Zap any index to which we can't reapply configuration\n");
    Print("  --zapanymismatch:     Zap any index with field mismatches, even if it looks usable\n");
    Print("  catalogmask:          Wildcard mask of catalogs to update\n");
    TerminateScriptWithError("");
  }

  FixConsilioIndices(CELL[ catalogmask := cmdargs.catalogmask ?? "*"
                         , cmdargs.zapbroken
                         , cmdargs.zapanymismatch
                         ]);
}

OpenPrimary();
Main();
