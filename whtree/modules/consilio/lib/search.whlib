﻿<?wh
/** @short Searching interface to the Search module
    @long This library provides a low-level api to the Consilio search engine
    @private Searching should be done through api.whlib (cacheable and without the option for string-merge built queries)
*/

LOADLIB "mod::consilio/lib/database.whlib";
LOADLIB "mod::consilio/lib/internal/indexmanager.whlib"
    EXPORT SearchOk, SearchError, SearchIndexNotFound, SearchNoAccess
         , SearchConnectError, SearchSendError, SearchHTTPError
         , SearchNoTotalError, SearchUnavailable, SearchTimeOutError;
LOADLIB "mod::consilio/lib/internal/support.whlib"
    EXPORT DateTimeToString, StringToDateTime;

LOADLIB "mod::system/lib/services.whlib";

LOADLIB "mod::tollium/lib/gettid.whlib";


// we cannot loadlib api.whlib, it loadlibs us
FUNCTION PTR CQParseUserQuery := MakeFunctionPtr("mod::consilio/lib/api.whlib#CQParseUserQuery");
FUNCTION PTR __GetIndexManagerOptions := MakeFunctionPtr("mod::consilio/lib/api.whlib#__GetIndexManagerOptions");

PUBLIC OBJECTTYPE SearchException EXTEND Exception
< STRING code;
  PUBLIC PROPERTY errorcode(code, -);

  MACRO NEW(STRING errorcode, STRING what)
  : Exception(what)
  {
    this->code := errorcode;
  }
>;

/** @short Get a description for an error code
    @long Use this function to get a short description for an error code
          received from one of the search functions.
    @param errorcode The error code received
    @param lang Language code to display the description in (e.g. "en" for English
                or "nl" for Dutch)
    @return The error description
    @see SearchFor, SearchForAll, GetIndexList */
PUBLIC STRING FUNCTION GetErrorMessage(STRING errorcode, STRING lang DEFAULTSTO "")
{
  STRING error;
  IF (errorcode = SearchOk)
    error := GetTIDForLanguage(lang, "consilio:searcherrors.ok");
  ELSE IF (errorcode = SearchIndexNotFound)
    error := GetTIDForLanguage(lang, "consilio:searcherrors.indexnotfound");
  ELSE IF (errorcode = SearchNoAccess)
    error := GetTIDForLanguage(lang, "consilio:searcherrors.noaccess");
  ELSE IF (errorcode = SearchConnectError)
    error := GetTIDForLanguage(lang, "consilio:searcherrors.connecterror");
  ELSE IF (errorcode = SearchSendError)
    error := GetTIDForLanguage(lang, "consilio:searcherrors.senderror");
  ELSE IF (errorcode = SearchHTTPError)
    error := GetTIDForLanguage(lang, "consilio:searcherrors.httperror");
  ELSE IF (errorcode = SearchNoTotalError)
    error := GetTIDForLanguage(lang, "consilio:searcherrors.nototalerror");
  ELSE IF (errorcode = SearchUnavailable)
    error := GetTIDForLanguage(lang, "consilio:searcherrors.unavailable");
  ELSE IF (errorcode = SearchTimeOutError)
    error := GetTIDForLanguage(lang, "consilio:searcherrors.timeouterror");
  ELSE IF (errorcode = SearchInvalidArgument)
    error := GetTIDForLanguage(lang, "consilio:searcherrors.invalidargument");
  ELSE
    error := GetTIDForLanguage(lang, "consilio:searcherrors.error");

  RETURN error;

  //STRING curlang := GetGettextLanguage();
  //SetGettextLanguage(lang);
  //STRING error := GetText/*ignore*/("consilio", 2000, errorcode);
  //SetGettextLanguage(curlang);
  //RETURN error;
}




STATIC OBJECTTYPE SearchObject
< //////////////////////////////////////////////////////////////////////////////
  // Variables
  //

  // The catalog we're searching
  INTEGER catalog_id;

  // Searching through whole index
  BOOLEAN catalog_all;

  // Current search session
  RECORD session;

  STRING catalog_name;

  RECORD pvt_empty_result_record;
  BOOLEAN have_empty_result_record;


  //////////////////////////////////////////////////////////////////////////////
  // Properties
  //

  PUBLIC BOOLEAN debug;
  PUBLIC BOOLEAN explain;
  PUBLIC INTEGER totaltotrack;

  /** @short The language used for searching
  */
  PUBLIC STRING language;

  /** @short When searching in a specific catalog, the id of the catalog, 0 otherwise
  */
  PUBLIC PROPERTY catalogid(GetCatalogId, -);

  /** @short Skeleton search result record
      @long The fields in this record will be returned in the search results. Fields in the result record are updated with
            the values returned by Consilio. If the empty result record is a default record, all known fields are returned.
  */
  PUBLIC PROPERTY empty_result_record(pvt_empty_result_record, SetEmptyResultRecord);

  /** @short Skeleton aggregation result record
      @long Like the `empty_result_record`, but only used for aggregation queries to return the aggregation results separately
            from the query results
  */
  PUBLIC RECORD aggregation_mapping;

  /** @short For site searches: Only results with a URL starting with this restrict_url are returned
  */
  PUBLIC STRING restrict_url;

  /** @short For site searches: Results with a URL starting with one of these exclude_urls are not returned
  */
  PUBLIC STRING ARRAY exclude_urls;

  /** @short The length of the summary to create, in characters (approximately)
      @long If this is set to 0, or if summaries are discarded for a content source, no summary will be created. If this is
            set to -1, a summary with a default length of 200 characters is created.
  */
  PUBLIC INTEGER summary_length;

  /** @short The fields in which to highlight found query words
  */
  PUBLIC STRING ARRAY highlightfields;

  /** @short Do not highlight query matches within search results
  */
  PUBLIC PROPERTY donthighlight(GetDontHighlight, SetDontHighlight);

  /** @short Save query statistics
      @long If set to true, Consilio stores queries and the number of returned results and browsed result pages.
  */
  PUBLIC PROPERTY save_searches(GetSaveSearches, SetSaveSearches);

  /** @short The saved search session id
  */
  PUBLIC PROPERTY session_id(GetSessionID, SetSessionID);

  /** @short The saved search session tag
      @long This tag can be used to filter stored queries.
  */
  PUBLIC PROPERTY session_tag(GetSessionTag, SetSessionTag);

  /** @short The site for which searches will be saved
      @long Set this propery to force searches to be saved with this site instead of letting Consilio try to determine the
            relevant site.
  */
  PUBLIC PROPERTY session_site(GetSessionSite, SetSessionSite);

  //Internal property used by __SessionedSearchFor
  PUBLIC PROPERTY __session(session, SetSession);

  /** @short Set to TRUE to require all terms in the query to be present in the results
  */
  PUBLIC BOOLEAN and_search;


  //////////////////////////////////////////////////////////////////////////////
  // Initialization
  //

  /** @short Create a new search object
      @param language The language to use when searching (for parsing and stemming)
      @param save_searches If queries should be stored
      @param initdata Initialization data
      @cell initdata.type "catalog"
      @cell initdata.catalog [type = "catalog"] The catalog to search
  */
  MACRO NEW(STRING language, BOOLEAN save_searches, RECORD initdata)
  {
    this->language := language;
    this->summary_length := -1; // Default summary length
    this->totaltotrack := default_track_total_hits;
    this->save_searches := save_searches;
    this->highlightfields := [ "body" ]; // For summary generation

    IF (initdata.type = "catalog")
      this->InitCatalog(initdata);
    ELSE IF (initdata.type = "index")
      this->InitIndex();
    ELSE IF (initdata.type = "module")
      THROW NEW SearchException(SearchIndexNotFound, "Module content sources are no longer supported");

  }

  // Initialize the search object for catalog searches
  MACRO InitCatalog(RECORD initdata)
  {
    INTEGER indexid :=
        SELECT AS INTEGER id
          FROM consilio.catalogs
          WHERE ToUppercase(name) = ToUppercase(initdata.catalog);
    IF (indexid <= 0)
      THROW NEW SearchException(SearchIndexNotFound, "Could not find catalog '" || initdata.catalog || "'");

    this->catalog_id := indexid;
    this->catalog_name := initdata.catalog;
  }

  // Initialize the search object for whole index searches
  MACRO InitIndex()
  {
    this->catalog_id := 0;
    this->catalog_all := TRUE;
  }


  //////////////////////////////////////////////////////////////////////////////
  // Public API
  //

  /** @short Perform a search query
      @param query The query to search for
      @param first The first result to return (0-based)
      @param count The number of results to return
      @return The search result
      @cell return.totalcount The total number of matches in Consilio
      @cell return.totalexact The total number of matches is an exact value (if FALSE, it's a lower bound)
      @cell return.results The actual search results (records as defined by empty_result_record)
  */
  PUBLIC RECORD FUNCTION Search(STRING query, INTEGER first, INTEGER count)
  {
    // No results if query is empty, but we'll start a session
    IF (query = "")
      RETURN [ totalcount := 0
             , totalexact := TRUE
             , results := DEFAULT RECORD ARRAY
             ];

    RECORD parsed_query := CQParseUserQuery(query, [ querymode := this->and_search ? "AND" : "OR" ]);

    // If 'summary' is requested, request it through '_summary'
    BOOLEAN want_summary := CellExists(this->empty_result_record, "summary") AND NOT CellExists(this->empty_result_record, "_summary");
    IF (want_summary)
    {
      INSERT CELL _summary := "summary" INTO this->empty_result_record;
      DELETE CELL summary FROM this->empty_result_record;
    }
    ELSE
      want_summary := NOT RecordExists(this->empty_result_record);
    // If 'score' is requested, request it through '_score'
    BOOLEAN want_score := CellExists(this->empty_result_record, "score") AND NOT CellExists(this->empty_result_record, "_score");
    IF (want_score)
    {
      INSERT CELL _score := "score" INTO this->empty_result_record;
      DELETE CELL score FROM this->empty_result_record;
    }

    RECORD result := this->SearchQuery(parsed_query, first, count);

    // If 'summary' was requested, reset the summary mapping
    IF (want_summary AND RecordExists(this->empty_result_record))
    {
      INSERT CELL summary := "" INTO this->empty_result_record;
      DELETE CELL _summary FROM this->empty_result_record;
    }
    // If 'score' was requested, reset the score mapping
    IF (want_score AND RecordExists(this->empty_result_record))
    {
      INSERT CELL score := this->empty_result_record._score INTO this->empty_result_record;
      DELETE CELL _score FROM this->empty_result_record;
    }

    RETURN result;
  }

  PUBLIC RECORD FUNCTION SearchQuery(RECORD query, INTEGER first, INTEGER count)
  {
    RETURN this->__SearchQuery(query, first, count, FALSE);
  }

  PUBLIC RECORD FUNCTION __SearchQuery(RECORD query, INTEGER first, INTEGER count, BOOLEAN addscore)
  {
    IF (this->catalog_id = 0 AND NOT this->catalog_all)
      THROW NEW SearchException(SearchError, "Search object not initialized");

    // No results if query is empty
    IF (NOT RecordExists(query))
    {
      IF(this->debug)
        Print("Query is empty, skipping search\n");
      RETURN [ totalcount := 0
             , totalexact := TRUE
             , results := DEFAULT RECORD ARRAY
             ];
    }

    RECORD indexoptions := __GetIndexManagerOptions(this->catalog_id);

    STRING ARRAY highlightfields := this->highlightfields;
    // IF (this->summary_length != 0 AND "body" NOT IN highlightfields)
    //   INSERT "body" INTO highlightfields AT 0;
    // Get search results
    RECORD options := CELL
        [ first
        , count
        , summary := this->summary_length
        , highlightfields
        , lang := this->language
        , restrict_to := this->restrict_url
        , this->exclude_urls
        , this->session
        , this->debug
        , this->explain
        , this->totaltotrack
        , this->aggregation_mapping
        ];
    IF (this->have_empty_result_record)
      INSERT CELL mapping := this->empty_result_record INTO options;
    ELSE
      INSERT CELL mapping := indexoptions.defaultmapping INTO options;
    IF (addscore)
      INSERT CELL _score := "_score" INTO options.mapping;

    RECORD result := SearchIndexManager(this->catalog_id, query, options, indexoptions);
    IF (result.status != SearchOk) //though actually all SearchIndexManager non-ok paths should be capable of throwing (no-attached-indices might not yet?)
    {
      THROW NEW SearchException(result.status, "Search error: " || result.status);
    }

    // Update our session
    IF (RecordExists(this->session))
      this->session := result.session;

    // Return results
    RETURN CELL[ ...result, DELETE status, DELETE session ];
  }

  PUBLIC MACRO ResetEmptyResultRecord()
  {
    this->empty_result_record := DEFAULT RECORD;
    this->have_empty_result_record := FALSE;
  }


  //////////////////////////////////////////////////////////////////////////////
  // Property getters and setters
  //

  INTEGER FUNCTION GetCatalogId()
  {
    RETURN this->catalog_id > 0 ? this->catalog_id : 0;
  }

  BOOLEAN FUNCTION GetDontHighlight()
  {
    RETURN Length(this->highlightfields) = 0;
  }

  MACRO SetDontHighlight(BOOLEAN donthighlight)
  {
    this->highlightfields := donthighlight AND this->summary_length = 0 ? STRING[] : [ "body" ];
  }

  BOOLEAN FUNCTION GetSaveSearches()
  {
    RETURN RecordExists(this->session);
  }
  MACRO SetSaveSearches(BOOLEAN save_searches)
  {
    IF (this->save_searches != save_searches)
    {
      IF (save_searches)
        this->session := [ sessid := ""
                         , tag := ""
                         , siteid := 0
                         ];
      ELSE
        this->session := DEFAULT RECORD;
    }
  }

  STRING FUNCTION GetSessionID()
  {
    RETURN RecordExists(this->session) ? this->session.sessid: "";
  }
  MACRO SetSessionID(STRING sessid)
  {
    this->save_searches := TRUE; // Will create an empty session record, if it does not exist yet
    this->session.sessid := sessid;
  }

  STRING FUNCTION GetSessionTag()
  {
    RETURN RecordExists(this->session) ? this->session.tag : "";
  }
  MACRO SetSessionTag(STRING tag)
  {
    IF (RecordExists(this->session))
      this->session.tag := tag;
  }

  INTEGER FUNCTION GetSessionSite()
  {
    RETURN RecordExists(this->session) ? this->session.siteid : 0;
  }
  MACRO SetSessionSite(INTEGER siteid)
  {
    IF (RecordExists(this->session))
      this->session.siteid := siteid;
  }

  MACRO SetSession(RECORD sess)
  {
    this->save_searches := TRUE; // Will create an empty session record, if it does not exist yet
    this->session := ValidateOptions(this->session, sess);
  }

  MACRO SetEmptyResultRecord(RECORD empty_result_record)
  {
    this->pvt_empty_result_record := empty_result_record;
    this->have_empty_result_record := TRUE;
  }
>;

/** @short Create a search object for catalog searches
    @param language The language to use for searching (used for parsing and stemming)
    @param catalog The catalog to search
*/
PUBLIC OBJECT FUNCTION OpenSearchObject(STRING language, STRING catalog, /*unused*/STRING password DEFAULTSTO "")
{
  RETURN NEW SearchObject(language, TRUE, [ type := "catalog", catalog := catalog ]);
}

PUBLIC OBJECT FUNCTION __OpenIndexSearchObject(STRING language)
{
  RETURN NEW SearchObject(language, FALSE, [ type := "index" ]);
}



OBJECTTYPE SuggestObject
< //////////////////////////////////////////////////////////////////////////////
  // Variables
  //

  // The current document count method ("", "fast", "active", or "search")
  STRING doccount;

  // The catalog we're returning suggestions for
  INTEGER catalog_id;

  // Custom suggestion prefix
  STRING prefix;

  STRING catalog_name;

  PUBLIC BOOLEAN debug;


  //////////////////////////////////////////////////////////////////////////////
  // Properties
  //

  /** @short Counting method for matching documents
      @long The method for counting the number of documents for suggestions. One of "fast" (will also return deleted
            documents), "active" (will only return non-deleted documents, but might be slower) or "search" (will search for
            each suggested term and only return found documents, is slower still). Set to empty to omit document counts.
            Default value is "search" for site suggestions and "active" for module suggestions.
  */
  PUBLIC PROPERTY document_count(doccount, SetDocCount);

  /** @short For site suggestions: Only suggestions for documents with a URL starting with this restrict_url are returned
      @long If this is set, only documents matching the restrict URL will be returned, which means "search" will be used as
            document_count value. However, document counts will only be returned if document_count was already set.
  */
  PUBLIC STRING restrict_url;

  PUBLIC STRING ARRAY exclude_urls;

  /** @short Set to TRUE to require all terms in the query to be present in the results
  */
  PUBLIC BOOLEAN and_search;


  //////////////////////////////////////////////////////////////////////////////
  // Initialization
  //

  /** @short Create a new suggest object
      @cell catalog The catalog to get suggestions for
  */
  MACRO NEW(RECORD initdata)
  {
    IF (initdata.type = "catalog")
      this->InitCatalog(initdata);
    ELSE IF (initdata.type = "module")
      THROW NEW SearchException(SearchIndexNotFound, "Module content sources are no longer supported");
  }

  // Initialize the suggest object for catalog suggestions
  MACRO InitCatalog(RECORD initdata)
  {
    INTEGER indexid :=
        SELECT AS INTEGER id
          FROM consilio.catalogs
          WHERE ToUppercase(name) = ToUppercase(initdata.catalog);

    IF (indexid = 0)
      THROW NEW SearchException(SearchIndexNotFound, "Could not find catalog '" || initdata.catalog || "'");

    this->catalog_id := indexid;
    this->catalog_name := initdata.catalog;
    this->document_count := "search";
  }


  //////////////////////////////////////////////////////////////////////////////
  // Public API
  //

  /** @short Get suggestions for the given text
      @param text The text to get suggestions for
      @param count The maximum number of suggestions to return (0 for all matches, -1 for default number of 10)
      @return The suggestions
      @cell return.text The suggestion text
      @cell return.count The number of documents which match the suggestion
  */
  PUBLIC RECORD ARRAY FUNCTION GetSuggestions(STRING text, INTEGER count)
  {
    IF (this->catalog_id = 0)
      THROW NEW SearchException(SearchError, "Search object not initialized");

    // No results if query is empty and no prefix is given
    IF (text = "" AND this->prefix = "")
      RETURN DEFAULT RECORD ARRAY;

    // Get search results
    RECORD callresult := SuggestIndexManager(this->catalog_id, text,
        [ count := count
        , doccount := this->doccount
        , prefix := this->prefix
        , restrict_to := this->restrict_url
        , exclude_urls := this->exclude_urls
        , and_search := this->and_search
        , debug := this->debug
        ]);
    IF (callresult.status != SearchOk)
      THROW NEW SearchException(callresult.status, "Search weblet error: " || callresult.status);

    RETURN callresult.suggestions;
  }


  //////////////////////////////////////////////////////////////////////////////
  // Property getters and setters
  //

  MACRO SetDocCount(STRING doccount)
  {
    doccount := ToLowercase(doccount);
    IF (doccount != this->doccount AND doccount IN [ "", "fast", "active", "search" ])
      this->doccount := doccount;
  }
>;

/** @short Create a suggest object for catalog suggestions
    @param catalog The catalog to search
*/
PUBLIC OBJECT FUNCTION OpenSuggestObject(STRING catalog, /*unused*/STRING password DEFAULTSTO "")
{
  IF(Length(catalog) > 40) //likely an encrypted catalog ref. support it in this API to ease upgrade of 'old' users - at least allow them to properly encode their catalog names..
  {
    RECORD tok := DecryptForThisServer("consilio:suggest", catalog, [ fallback := CELL[ t := catalog, ag := TRUE ]]);
    IF(NOT tok.ag)
      THROW NEW Exception(`Autosuggest is not enabled for this token`);
    catalog := tok.t;
  }

  RETURN NEW SuggestObject([ type := "catalog", catalog := catalog ]);
}
