<?wh
/** @topic consilio/api
*/

LOADLIB "wh::adhoccache.whlib";
LOADLIB "wh::datetime.whlib";
LOADLIB "wh::util/algorithms.whlib";

//The exports from catalogs.whlib are for legacy users... caller should switch to the catalogs.whlib and as soon as they're all >5.02+ we will remove the exports
LOADLIB "mod::consilio/lib/catalogs.whlib" EXPORT CreateConsilioCatalog, ListConsilioCatalogs, OpenConsilioCatalog, OpenConsilioCatalogById;
LOADLIB "mod::consilio/lib/database.whlib";
LOADLIB "mod::consilio/lib/internal/indexmanager.whlib";
LOADLIB "mod::consilio/lib/internal/support.whlib" EXPORT fetcher_trusted_ip;
LOADLIB "mod::consilio/lib/internal/opensearch.whlib" EXPORT SearchUnavailableException;
LOADLIB "mod::system/lib/services.whlib";
LOADLIB "mod::system/lib/internal/whfs/support.whlib";
LOADLIB "mod::consilio/lib/internal/indexmanager_state.whlib";

LOADLIB "mod::tollium/lib/gettid.whlib";


PUBLIC CONSTANT INTEGER require_allowed, require_required, require_prohibited;

MACRO ValidateFieldName(STRING field)
{
  IF (field = "")
    THROW NEW Exception("Field name is required");
  IF (field LIKE "?*@*") //block @ fields, or at least until transition is complete
    THROW NEW Exception(`Field name '${field}' is illegal, use mod_<modname>.<field>...`);
}

/** @short Create a match query
    @long This query can be used to match a field with a given value. The matching behaviour depends on the matchtype and the
        type of the value. For INTEGER and STRING values, matchtype "=" means a literal match and matchtype "CONTAINS" means
        a value match. For INTEGER ARRAY and STRING ARRAY values, matchtype "IN" means that the fields must contain at least
        one of the values and matchtype "CONTAINS" means that the field must contain all of the values. For DATETIME values,
        the matchtype can be one of "<", "<=", "=", "=>" or ">" to search for date ranges.
    @param field The field to search
    @param matchtype How to match (possible values: 'IN', 'CONTAINS', '<', '<=', '=', '>=', '>')
    @param value The value to match
    @cell(float) options.boost Factor to increase or decrease the score for this query (defaults to 1f)
    @return The match query
*/
PUBLIC RECORD FUNCTION CQMatch(STRING field, STRING matchtype, VARIANT value, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions([ boost := 1f ], options);

  ValidateFieldName(field);

  SWITCH (TypeID(value))
  {
    CASE TypeID(STRING ARRAY)
       , TypeID(INTEGER ARRAY)
       , TypeID(INTEGER64 ARRAY)
       , TypeID(MONEY ARRAY)
       , TypeID(FLOAT ARRAY)
       , TypeID(DATETIME ARRAY)
       , TypeID(BOOLEAN ARRAY)
    {
      IF (matchtype NOT IN [ "IN", "CONTAINS" ])
        THROW NEW Exception(`Invalid match type '${matchtype}' for ${GetTypeName(TypeID(value))} match query`);
    }
    CASE TypeID(STRING)
       , TypeID(INTEGER)
       , TypeID(INTEGER64)
       , TypeID(MONEY)
       , TypeID(FLOAT)
       , TypeID(DATETIME)
    {
      IF (matchtype NOT IN [ "CONTAINS", "<", "<=", "=", ">=", ">" ])
        THROW NEW Exception(`Invalid match type '${matchtype}' for ${GetTypeName(TypeID(value))} match query`);
      IF (matchtype = "CONTAINS" AND TypeID(value) != TypeID(STRING))
        matchtype := "="; // Match the literal query value within an indexed array
    }
    CASE TypeID(BOOLEAN)
    {
      IF (matchtype NOT IN [ "=" ])
        THROW NEW Exception(`Invalid match type '${matchtype}' for ${GetTypeName(TypeID(value))} match query`);
    }
    DEFAULT
    {
      THROW NEW Exception(`Invalid value type '${GetTypeName(TypeID(value))}' for match query`);
    }
  }

  IF (IsTypeIDArray(TypeID(value)))
  {
    IF (Length(value) = 0)
      RETURN CQNothing();
    // For 'IN' queries, we can use a terms query, which supports up to 65536 terms, but can only do an 'OR' search, so for
    // 'CONTAINS' queries we have to use a boolean query, which only supports up to 1024 terms (clauses).
    IF (matchtype = "IN")
      RETURN CELL
          [ _type := "terms"
          , field
          , terms := value
          , options.boost
          ];
    RETURN CELL
        [ _type := "boolean"
        , subqueries :=
            (SELECT query := CELL
                        [ _type := "term"
                        , field
                        , term
                        , boost := 1f
                        ]
                  , required := TRUE
                  , prohibited := FALSE
               FROM ToRecordArray(value, "term"))
        , options.boost
        ];
  }
  ELSE IF (matchtype IN [ "<", "<=", ">=", ">" ])
  {
    RETURN CELL
        [ _type := "range"
        , field
        , matchtype
        , term := value
        , options.boost
        ];
  }

  RETURN CELL
      [ _type := matchtype = "=" ? "literal" : "term"
      , field
      , term := value
      , options.boost
      ];
}

/** Returns a query record that will match results in which a field values lies between the specified range
    @param field Field name
    @param lowerterm Lower range value
    @param includelower Whether to include exact matches of the lower range value
    @param upperterm Upper range value
    @param includeupper Whether to include exact matches of the upper range value
    @cell(float) options.boost Factor to increase or decrease the score for this query (defaults to 1f)
    @return Consilio query record
*/
PUBLIC RECORD FUNCTION CQRange(STRING field, VARIANT lowerterm, BOOLEAN includelower, VARIANT upperterm, BOOLEAN includeupper, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions([ boost := 1f ], options);

  ValidateFieldName(field);

  IF (TypeID(lowerterm) != TypeID(upperterm))
    THROW NEW Exception(`Range query lower term type '${GetTypeName(TypeID(lowerterm))}' not equal to upper term type '${GetTypeName(TypeID(upperterm))}'`);

  VARIANT lowersearchterm, uppersearchterm;
  IF (TypeID(lowerterm) = TypeID(INTEGER))
  {
    lowersearchterm := ToString(lowerterm);
    uppersearchterm := ToString(upperterm);
  }
  ELSE IF (TypeID(lowerterm) = TypeID(STRING))
  {
    lowersearchterm := STRING(lowerterm);
    uppersearchterm := STRING(upperterm);
  }
  ELSE IF (TypeID(lowerterm) = TypeID(DATETIME))
  {
    lowersearchterm := lowerterm;
    uppersearchterm := upperterm;
  }
  ELSE
    THROW NEW Exception(`Invalid term type '${GetTypeName(TypeID(lowerterm))}' for range query`);

  RETURN CELL
      [ _type := "range"
      , field
      , matchtype := (includelower ? "[" : "{") || (includeupper ? "]" : "}")
      , lowerterm := lowersearchterm
      , options.boost
      , upperterm := uppersearchterm
      ];
}

/** Returns a query record that will never match anything
    @long This exists so query operators can optimize themselves into CQNothing.
    @return Consilio query record
*/
PUBLIC RECORD FUNCTION CQNothing()
{
  RETURN
      [ _type := "nothing"
      , boost := 0f
      ];
}

/** @short Returns a query record that matches all results that have a value for the specified field
    @param field Field to look at
    @cell(float) options.boost Factor to increase or decrease the score for this query (defaults to 1f)
    @return Consilio query record
*/
PUBLIC RECORD FUNCTION CQHas(STRING field, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions(CELL[ boost := 1f ], options);
  ValidateFieldName(field);

  RETURN CELL
      [ _type := "exists"
      , field := field
      , options.boost
      ];
}

/** @short Returns a query record that matches all results
    @cell(float) options.boost Factor to increase or decrease the score for this query (defaults to 1f)
    @return Consilio query record
*/
PUBLIC RECORD FUNCTION CQAll(RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions(CELL[ boost := 1f ], options);
  RETURN CELL
      [ _type := "all"
      , options.boost
      ];
}

/** @short Combine queries using AND
    @param inputqueries Consilio queries to combine
    @cell(float) options.boost Factor to increase or decrease the score for this query (defaults to 1f)
    @return Consilio query record
*/
PUBLIC RECORD FUNCTION CQAnd(RECORD ARRAY inputqueries, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions(CELL[ boost := 1f ], options);
  RETURN CombineSubqueries(inputqueries, TRUE, options.boost);
}

/** @short Combine queries using OR
    @param inputqueries Consilio queries to combine
    @cell(float) options.boost Factor to increase or decrease the score for this query (defaults to 1f)
    @return Consilio query record
*/
PUBLIC RECORD FUNCTION CQOr(RECORD ARRAY inputqueries, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions(CELL[ boost := 1f ], options);
  RETURN CombineSubqueries(inputqueries, FALSE, options.boost);
}

RECORD FUNCTION CombineSubqueries(RECORD ARRAY subqueries, BOOLEAN required, FLOAT boost)
{
  IF(Length(subqueries) = 0)
    RETURN CQNothing();

  RECORD resultquery := CELL
      [ _type := "boolean"
      , subqueries := RECORD[]
      , boost
      ];
  FOREVERY (RECORD query FROM subqueries)
  {
    // If the first input query is a boolean query with the requested requirement, just add the other input queries
    IF (query._type = "boolean")
    {
      // If this subquery is a boolean query with the requested requirement, just add its subqueries directly
      IF (NOT RecordExists(SELECT FROM query.subqueries WHERE COLUMN required != VAR required OR prohibited))
      {
        // If this is the first query, replace the query
        IF (#query = 0)
          resultquery := query;
        ELSE
          resultquery.subqueries := resultquery.subqueries CONCAT query.subqueries;
      }
      ELSE IF (RecordExists(query.subqueries))
      {
        // Add the boolean query to the subqueries
        INSERT CELL[ query, required, prohibited := FALSE ] INTO resultquery.subqueries AT END;
      }
    }
    ELSE IF(query._type = "nothing")
    {
      IF(required)  //In a serie of ANDs, nothing matches a poison.
        RETURN CQNothing();

      //but for OR queries, just ignore it
    }
    ELSE
    {
      INSERT CELL[ query, required, prohibited := FALSE ] INTO resultquery.subqueries AT END;
    }
  }
  RETURN resultquery;
}

/** @short Negate query
    @param inputquery Consilio query to negate
    @cell(float) options.boost Factor to increase or decrease the score for this query (defaults to 1f)
    @return Consilio query record
*/
PUBLIC RECORD FUNCTION CQNot(RECORD inputquery, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions(CELL[ boost := 1f ], options);
  IF(inputquery._type = "nothing")
    RETURN CQAll(options);
  ELSE IF(inputquery._type = "all")
    RETURN CQNothing();
  RETURN CELL
      [ _type := "boolean"
      , subqueries :=
          [ [ query := inputquery
            , required := FALSE
            , prohibited := TRUE
            ]
          ]
      , options.boost
      ];
}

/** @short Parse a user query
    @param userquery User query string
    @cell(string) options.querymode How to combine the toplevel terms, either 'AND' or 'OR' (defaults to 'AND')
    @cell(record array) options.defaultfields Fields to search for terms without explicit field specification
    @cell(string) options.defaultfields.field Field name
    @cell(float) options.defaultfields.boost Factor to increase or decrease the score for this field (defaults to 1f)
    @cell(boolean) options.defaultfields.stemmed_field Set to true to also use stemming when searching for terms in this field
    @cell(boolean) options.synonyms If the catalog synonyms list should be used (defaults to TRUE, Opensearch only)
    @cell(float) options.boost Factor to increase or decrease the score for this query (defaults to 1f)
    @return Consilio query record
*/
PUBLIC RECORD FUNCTION CQParseUserQuery(STRING userquery, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions(
      [ querymode := "AND"
      , defaultfields := DEFAULT RECORD ARRAY
      , synonyms := TRUE
      , boost := 1f
      ],
      options,
      [ enums := [ querymode := [ "AND", "OR" ] ]
      ]);

  userquery := TrimWhitespace(userquery);
  IF(userquery = "") //FIXME validate for other weird stuff that should require us to just discard this
    RETURN CQNothing();

  RETURN CELL
      [ _type := "user"
      , userquery
      , options.querymode
      , options.defaultfields
      , options.synonyms
      , options.boost
      ];
}

/** @short Builds an aggregation query
    @param field Field to aggregate over
    @param type Aggregation type (`"count"`, `"min"`, `"max"`, `"terms"`, `"top_hits"`, `"cardinality"`)
    @cell(string) options.name Name of the query, to refer to this query (defaults to the used field)
    @cell(integer) options.size Nr of items to return (only for aggregation types `"terms"` and `"top_hits"`, defaults to -1
        for `"terms"` to return all terms, must be a positive non-zero number for `"top_hits"`)
    @cell(string) options.countfield The name of the field to return the number of documents containing the term in (only for
        aggregation type `"terms"`)
    @cell(string) options.orderby Field to order over (only for aggregation type `"top_hits"`, `"terms"` are always sorted by
        the aggregation field value)
    @cell(boolean) options.orderdesc Whether to reverse the ordering (only for aggregation types `"terms"` and `"top_hits"`,
        defaults to FALSE for `"terms"` and to TRUE for `"top_hits"`)
    @cell(record) options.query Query to run the aggregation over (defaults to matching all documents in the index)
    @cell(record array) options.aggregations Subaggregations (returned by #CQAggregate)
    @return Consilio aggregation query record
*/
PUBLIC RECORD FUNCTION CQAggregate(STRING field, STRING type, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions(
      [ name := ""
      , size := -1
      , countfield := ""
      , orderby := ""
      , orderdesc := FALSE
      , query := DEFAULT RECORD
      , aggregations := DEFAULT RECORD ARRAY
      ], options, [ optional := [ "orderdesc" ] ]);

  // Make sure either field or name is given
  IF (type != "count")
    ValidateFieldName(field);
  ELSE IF (type = "count" AND options.name = "")
    THROW NEW Exception("Name option is required for aggregation type 'count'");

  // For top_hits, a size must be given
  IF (type IN [ "top_hits" ] AND options.size <= 0)
    THROW NEW Exception("A non-zero size option is required for aggregation type 'top_hits'");

  RECORD aggregation :=
      CELL[ _type := type
          , name := options.name ?? field
          , field
          , options.aggregations
          ];
  IF (type IN [ "min", "max", "count", "cardinality" ])
  {
  }
  ELSE IF (type = "terms")
  {
    BOOLEAN orderdesc := CellExists(options, "orderdesc") AND options.orderdesc;
    aggregation := CELL[ ...aggregation, options.size, options.countfield, orderdesc ];
  }
  ELSE IF (type = "top_hits")
  {
    BOOLEAN orderdesc := NOT CellExists(options, "orderdesc") OR options.orderdesc;
    aggregation := CELL[ ...aggregation, options.size, options.orderby, orderdesc ];
  }
  ELSE
    THROW NEW Exception(`Invalid aggregation type '${type}'`);

  RECORD res := CELL[ aggregation ];
  IF (RecordExists(options.query))
    INSERT CELL query := options.query INTO res;
  RETURN res;
}

/** @short Create a query to search through nested fields
    @param field The nested field to query
    @param query The query to run
    @cell(integer) options.first The first result to return (0-based, defaults to 0)
    @cell(integer) options.count The number of results to return (-1 for all results, defaults to 10)
    @cell(boolean) options.returnnonmatching Set to TRUE to have all the nested subrecords returned (by default only the
        matching subrecords are returned)
    @cell(string) options.summaryfield The name of the field to return the summary for the nested result in
    @cell(string) options.scorefield The name of the field to return the nested result's score in
    @cell(string) options.totalcountfield The name of the field to return the total number of nested results in
    @cell(string) options.orderby Field to order over (defaults to `""`, which orders descending by score, can be set to the
        value of the `scorefield` option, doesn't have to be a field in the `mapping` option)
    @cell(boolean) options.orderdesc Whether to reverse the ordering (defaults to `TRUE` when ordering by score or `FALSE`
        when ordering by another field)
    @cell(float) options.boost Factor to increase or decrease the score for this query (defaults to 1f)
*/
PUBLIC RECORD FUNCTION CQNested(STRING field, RECORD query, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions(
      [ first := 0
      , count := 10
      , returnnonmatching := FALSE
      , summaryfield := ""
      , scorefield := ""
      , totalcountfield := ""
      , orderby := ""
      , orderdesc := FALSE
      , boost := 1f
      ], options, [ optional := [ "orderdesc" ] ]);

  RETURN CELL
      [ _type := "nested"
      , field
      , subquery := query
      , ...options
      ];
}

/** @short Add a raw OpenSearch query
    @param query The raw query, eg `[ match_bool_prefix := [ "title" := "rises wi" ] ]`
    @return Consilio wrapped raw Opensearch query record
*/
PUBLIC RECORD FUNCTION CQOpenSearch(RECORD query)
{
  RETURN CELL[ _type := "opensearch", rawquery := query ];
}

RECORD ARRAY FUNCTION Highlight(RECORD ARRAY results, STRING ARRAY fields, STRING class)
{
  FOREVERY(RECORD res FROM results)
  {
    FOREVERY(RECORD cellrec FROM UnpackRecord(res))
    {
      SWITCH (TypeID(cellrec.value))
      {
        CASE TypeID(RECORD ARRAY)
        {
          // Recurse into nested subresults
          STRING ARRAY subfields := SELECT AS STRING ARRAY Substring(field, Length(cellrec.name) + 1) FROM ToRecordArray(fields, "field") WHERE field LIKE cellrec.name || ".*";
          res := CellUpdate(res, cellrec.name, Highlight(cellrec.value, subfields, class));
        }
        CASE TypeID(STRING)
        {
          IF (cellrec.name IN fields)
            res := CellUpdate(res, cellrec.name, AddHighlightClass(cellrec.value, class));
        }
        CASE TypeID(STRING ARRAY)
        {
          IF (cellrec.name IN fields)
            res := CellUpdate(res, cellrec.name, SELECT AS STRING ARRAY AddHighlightClass(value, class) FROM ToRecordArray(cellrec.value, "value"));
        }
      }
    }
    results[#res] := res;
  }
  RETURN results;
}

// The value of a highlighted field is always HTML-encoded
STRING FUNCTION AddHighlightClass(STRING value, STRING class)
{
  INTEGER firstclose := SearchSubstring(value, "\x1C");
  INTEGER lastopen := SearchLastSubstring(value, "\x1D");
  // If there are no markers, just return the (encoded) value
  IF (firstclose < 0 AND lastopen < 0)
    RETURN EncodeHTML(value);

  // If the highlight class name is empty, remove the markers
  IF (class = "")
  {
    value := Substitute(value, "\x1D", "");
    value := Substitute(value, "\x1C", "");
    RETURN EncodeHTML(value);
  }

  // OpenSearch seems to not add the pre tag when the highlight starts at the start of the string or add the post tag
  // when the highlight ends at the end of the string
  IF (firstclose >= 0 AND (SearchSubstring(value, "\x1D") < 0 OR SearchSubstring(value, "\x1D") > firstclose))
    value := "\x1D" || value;
  IF (lastopen >= 0 AND SearchLastSubstring(value, "\x1C") < lastopen)
    value := value || "\x1C";

  // Translate the markers to something that will survive EncodeHTML
  value := Substitute(value, "\x1D", "\uF8F0");
  value := Substitute(value, "\x1C", "\uF8F1");
  value := EncodeHTML(value);
  value := Substitute(value, "&#63728;", `<span class="${EncodeValue(class)}">`);
  value := Substitute(value, "&#63729;", "</span>");

  // Remove legacy hyperlink markers (won't be added anymore by parser_html, but might still be present in already indexed documents)
  value := Substitute(value, "\x1F", "");
  RETURN value;
}

/** @short Get a token to access a consilio search engine's autosuggest API
    @param catalogtag Catalog tag
    @cell(boolean) options.autosuggest Allow this token to be used for the autosuggest APIs
    @cell(string) options.restrict_url Restrict suggest matches to this URL
*/
PUBLIC STRING FUNCTION GetConsilioRPCToken(STRING catalogtag, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions(CELL[ autosuggest := FALSE
                                 , restrict_url := ""
                                 ], options);

  RECORD tok := CELL [ c := GetCurrentDatetime(), t := catalogtag, ag := options.autosuggest ];
  IF(options.restrict_url != "")
    INSERT CELL r := options.restrict_url INTO tok;
  RETURN EncryptForThisServer("consilio:suggest", tok);
}

/** @short Look up the 'best' catalog for a folder/site
    @long Lookup the catalog to use. Prefers catalog that more specifically target this folder and catalogs that contain less other contentsources
    @param folder Folder it to look up. If you're not specifically targetting folders we recommend specifying the site id
    @return Catalog name (as suitable for RunConsilioSearch). Empty if no catalog found */
PUBLIC STRING FUNCTION GetConsilioPublisherCatalog(INTEGER folder)
{
  RECORD ARRAY candidates := GetContentSourceMatches([folder]);
  IF(Length(candidates) = 0)
    candidates := GetContentSourceMatches(GetWHFSTree(folder,0));

  IF(Length(candidates) = 0)
    RETURN "";

  IF(Length(candidates) > 1) //more than 1? eliminate all but the closest match
    DELETE FROM candidates AS c WHERE fsobject != candidates[0].fsobject;

  RECORD ARRAY catalogs := SELECT id, name
                             FROM consilio.catalogs
                            WHERE id IN (SELECT AS INTEGER ARRAY DISTINCT catalogid FROM candidates);

  IF(Length(catalogs) = 0)
    RETURN "";

  IF(Length(catalogs) =  1)
    RETURN catalogs[0].name;

  //Prefer the catalog which most closely matches what we need. resolve ties by taking the lowest catalogid (we need a stable result..)
  INTEGER smallestcatalog := SELECT AS INTEGER catalogid
                               FROM consilio.contentsources
                              WHERE catalogid IN (SELECT AS INTEGER ARRAY id FROM catalogs)
                           GROUP BY catalogid
                           ORDER BY COUNT(*), catalogid;

  RETURN SELECT AS STRING name FROM catalogs WHERE id = smallestcatalog;
}

RECORD FUNCTION GenerateDefaultMapping(RECORD ARRAY inprops)
{
  //TODO shouldn't we be returning/allowing deep/structured results too ?  or can you optin to that?  not sure, this is just a default code

  RECORD defaultmapping;
  FOREVERY(RECORD row FROM inprops)
  {
    IF(row.type = "ignore")
      CONTINUE;

    IF(row.type IN [ "record", "nested" ])
      defaultmapping := CellInsert(defaultmapping, row.name, GenerateDefaultMapping(row.properties));
    ELSE
      defaultmapping := CellInsert(defaultmapping, row.name, row.defaultvalue);
  }
  RETURN defaultmapping;
}

RECORD FUNCTION GetCacheableIndexConfiguration(INTEGER indexid)
{
  OBJECT catalog := OpenConsilioCatalogById(indexid);
  IF (NOT ObjectExists(catalog))
    THROW NEW Exception(`Cannot find catalog #${indexid}`);

  RECORD ARRAY expectedmapping := catalog->GetExpectedMapping();
  RECORD defaultmapping := GenerateDefaultMapping(expectedmapping);
  defaultmapping := CELL[ ...defaultmapping
                        //for some reason? these weren't part of the defaultvalue although they're always in the mapping... probably too many sources of fields
                        , DELETE _indexed
                        , DELETE _contentsource
                        , DELETE body
                        ];

  RECORD ARRAY indexmanagers := GetIndexManagersForIndex(indexid);

  RETURN [ value := CELL [ defaultmapping, indexmanagers, expectedmapping, cat := catalog->tag ]
         , ttl := 15 * 60 * 1000
         , eventmasks := [ "consilio:contentsourceschanged"
                         , "consilio:indexfields"
                         ]
         ];
}

PUBLIC RECORD FUNCTION __GetIndexManagerOptions(INTEGER indexid)
{
  RETURN GetAdhocCached( [ indexoptions := indexid ], PTR GetCacheableIndexConfiguration(indexid));
}

/** @short Search an index
    @param indextag Index to search (module:tag)
    @param query Search query
    @cell(boolean) options.refresh Refresh the catalog before searching, making sure very recent changes are picked up
    @cell(string) options.language The language to use (legacy Consilio indices only, defaults to the language returned by
        %GetTidLanguage)
    @cell(integer) options.first The first result to return (0-based, defaults to 0)
    @cell(integer) options.count The number of results to return (-1 for all results, defaults to 100)
    @cell(boolean) options.save_searches If the search query should be saved for search reports (defaults to `FALSE`)
    @cell(integer) options.save_searches_site Set an explicit site id to save the query for (by default, Consilio tries to
        determine the site id by checking if the index contains only one publisher content source or by using
        %LookupPublisherUrl on the `restrict_url`)
    @cell(integer) options.summary_length The length of the summary to generate for each result (-1 for the default length of
        200, 0 to prevent summary generation, defaults to -1)
    @cell(string) options.highlightclass The CSS class used to highlight found query words in the `highlightfields` (set to
        empty string to disable match highlighting)
    @cell(string array) options.highlightfields The fields in which found query words should be highlighted (note that all
        fields mentioned in highlightfields are HTML-encoded)
    @cell(string array) options.highlightparts The fields from which only a relevant highlighted part (with max length
        `"summary_length"`) will be returned in the summaryfield, defaults to `[ "body" ]` (note that all fields mentioned in
        highlightparts are HTML-encoded)
    @cell(string) options.highlighttype Which highlighting algorithm to use, either `unified` (the default) or `plain` (see
        https://opensearch.org/docs/latest/search-plugins/searching-data/highlight/#highlighter-types for more information)
    @cell(string) options.summaryfield The name of the field to return the summary in (set to empty string to not return the
        summary, defaults to `"summary"`)
    @cell(string) options.scorefield The name of the field to return the result score in (set to empty string to not return
        scores, defaults to `""`)
    @cell(record) options.mapping The fields in this record will be returned in the search results and updated with the
        values returned by Consilio (defaults to `DEFAULT RECORD`, which returns all known fields)
    @cell(record) options.aggregation_mapping For aggregation queries, only the aggregation results will be returned, mapped
        using the `mapping` option, not the actual matching documents. By supplying a separate aggregation mapping, the
        aggregation results are returned separately in an `"aggregation"` result field
    @cell(string) options.orderby Field to order over (defaults to `""`, which orders descending by score, can be set to the
        value of the `scorefield` option, doesn't have to be a field in the `mapping` option)
    @cell(boolean) options.orderdesc Whether to reverse the ordering (defaults to `TRUE` when ordering by score or `FALSE`
        when ordering by another field)
    @cell(record array) options.ordering Advanced results ordering, overrides the `orderby` and `orderdesc` options
    @cell(string) options.ordering.orderby Field to order over (required, can be set to the value of the `scorefield` option,
        doesn't have to be a field in the `mapping` option, but it should be an indexed field)
    @cell(boolean) options.ordering.orderdesc Whether to reverse the ordering (defaults to `TRUE` when ordering by score or
        `FALSE` when ordering by another field)
    @cell(string) options.ordering.ordermode Sort mode to use (one of `"min"`, `"max"`, `"sum"`, `"avg"` or `"median"`,
        defaults to `"min"` when sorting ascending or `"max"` when sorting descending)
    @cell(string) options.restrict_url For site searches: Only results with a URL starting with this restrict_url are
        returned
    @cell(string array) options.exclude_urls For site searches: Results with a URL starting with one of these exclude_urls
        are not returned
    @cell(integer) options.totaltotrack By default, only up to a certain results are counted with the `totalexact` return
        value being set to FALSE if there are more than that number of results. Set this option to the number of results that
        should be always be counted or to -1 to always return the exact number of results (a higher setting comes with a
        performance cost). Defaults to 10,000.
    @cell(boolean) options.validatemapping Validate the specified mapping. Defaults to TRUE
    @return The search result
    @cell(integer) return.totalcount The total number of results (not necessarily the number of returned results, if `count`
        is set to a value >= 0)
    @cell(boolean) return.totalexact If the total number of results is exact, if FALSE it's a lower bound for the actual
        number of results
    @cell(record array) return.results The actual results
    @cell(record array) return.aggregation The aggregation results for an aggregation query with a separate aggregation
        mapping (see the `aggregation_mapping` option)
    @cell(string array) return.eventmasks Event masks to listen to for changes to this query
*/
PUBLIC RECORD FUNCTION RunConsilioSearch(STRING indextag, RECORD query, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions(
      [ language := GetTidLanguage()
      , first := 0
      , count := default_consilio_search_count
      , save_searches := FALSE
      , save_searches_site := 0
      , summary_length := -1
      , highlightclass := "consilio--highlight"
      , highlightfields := STRING[]
      , highlightparts := [ "body" ]
      , highlighttype := "unified"
      , summaryfield := "summary"
      , scorefield := ""
      , mapping := DEFAULT RECORD
      , aggregation_mapping := DEFAULT RECORD
      , orderby := ""
      , orderdesc := FALSE
      , ordering := DEFAULT RECORD ARRAY
      , restrict_url := ""
      , exclude_urls := STRING[]
      , totaltotrack := default_track_total_hits
      , debug := FALSE
      , explain := FALSE
      , refresh := FALSE
      , validatemapping := TRUE
      , timeout := opensearch_read_timeout
      ], options,
      [ optional := [ "orderdesc" ]
      , enums :=
          [ highlighttype := [ "unified", "plain" ]
          ]
      ]);

  IF(NOT RecordExists(query)
      OR (CellExists(query, "query") AND RecordExists(query.query) AND query.query._type = "nothing")
      OR (NOT CellExists(query, "query") AND NOT CellExists(query, "aggregation") AND query._type = "nothing")) //will not return anything
    RETURN [ totalcount := 0
           , results := RECORD[]
           , eventmasks := STRING[]
           ];

  OBJECT catalog := OpenConsilioCatalog(indextag);
  IF (NOT ObjectExists(catalog))
    THROW NEW Exception(`Cannot open catalog '${indextag}'`);

  FOREVERY (RECORD ordering FROM options.ordering)
    options.ordering[#ordering] := ValidateOptions(
        [ orderby := ""
        , orderdesc := FALSE
        , ordermode := ""
        ],
        ordering,
        [ title := "ordering"
        , required := [ "orderby" ]
        , optional := [ "orderdesc", "ordermode" ]
        , enums :=
            [ ordermode := [ "min", "max", "sum", "avg", "median" ]
            ]
        ]);

  IF (RecordExists(options.mapping))
  {
    IF(options.validatemapping)
    {
      FOREVERY(RECORD cellrec FROM UnpackRecord(options.mapping))
      {
        ValidateFieldName(cellrec.name);
        IF(cellrec.name LIKE "_*")
          THROW NEW Exception("Internal fields may not be requested through mapping"); //TODO perhaps we should allow this? but how to deal with various conflicting settings between this and summaryfield/scorefield ?
      }
    }
  }

  IF (RecordExists(options.ordering))
    query := CELL[ ...query, options.ordering ];
  ELSE IF (options.orderby != "")
  {
    RECORD ordering := CELL[ options.orderby ];
    IF (CellExists(options, "orderdesc"))
      ordering := CELL[ ...ordering, options.orderdesc ];
    query := CELL[ ...query, ordering := [ ordering ] ];
  }
  IF (CellExists(query, "ordering") AND RecordExists(query.ordering))
  {
    // If orderinging on scorefield, ordering on "_score", otherwise if not already orderinging on "_score", add it for consistency
    INTEGER scoreordering :=
        (SELECT AS INTEGER #ordering + 1
           FROM query.ordering
          WHERE orderby = (options.scorefield ?? "_score")) - 1;
    IF (scoreordering >= 0)
      query.ordering[scoreordering].orderby := "_score";
    ELSE
      INSERT [ orderby := "_score" ] INTO query.ordering AT END;
  }

  RECORD session;
  IF (options.save_searches)
    session := [ sessid := "", tag := "", siteid := options.save_searches_site ];

  RECORD indexoptions := __GetIndexManagerOptions(catalog->id);
  RECORD searchoptions := CELL
      [ options.first
      , options.count
      , summary := options.summary_length
      , options.highlightfields
      , options.highlightparts
      , options.highlighttype
      , lang := options.language
      , restrict_to := options.restrict_url
      , options.exclude_urls
      , session
      , options.debug
      , options.explain
      , options.totaltotrack
      , options.timeout
      ];
  IF (RecordExists(options.mapping))
    INSERT CELL mapping := options.mapping INTO searchoptions;
  ELSE
    INSERT CELL mapping := indexoptions.defaultmapping INTO searchoptions;
  IF (options.scorefield != "")
    INSERT CELL _score := options.scorefield INTO searchoptions.mapping;
  INSERT CELL _summary := options.summaryfield INTO searchoptions.mapping;
  RECORD nestedfields := GetNestedFields(query);
  searchoptions.mapping := CELL[ ...searchoptions.mapping, ...nestedfields.mapping ];

  IF (CellExists(query, "aggregation"))
    INSERT CELL aggregation_mapping := options.aggregation_mapping INTO searchoptions;

  IF (options.refresh)
    catalog->Refresh();

  RECORD result := SearchIndexManager(catalog->id, query, searchoptions, __GetIndexManagerOptions(catalog->id));
  IF (result.status != SearchOk) //though actually all SearchIndexManager non-ok paths should be capable of throwing (no-attached-indices might not yet?)
    THROW NEW Exception("Search error: " || result.status);
  DELETE CELL status, session FROM result;

  IF(CellExists(result,'results'))
  {
    //Translate results to the requested format
    //(should probablyhave returned proper results in the first place, but wait until we're ready to deprecate older apis)
    //cant fix it using the mapping, as it's unset before __searchquery
    IF(options.summaryfield = "")
      result.results := SELECT *, DELETE _summary FROM result.results;

    IF(options.scorefield = "")
      result.results := SELECT *, DELETE _score FROM result.results;

    STRING ARRAY highlightfields :=
        SELECT AS STRING ARRAY ToUppercase(field)
          FROM ToRecordArray(GetSortedSet(STRING[ ...options.highlightfields, ...options.highlightparts, options.summaryfield, ...nestedfields.highlightfields ]), "field")
         WHERE field != "";
    IF(Length(highlightfields) != 0)
      result.results := Highlight(result.results, highlightfields, options.highlightclass);
  }

  INSERT CELL eventmasks := [ "consilio:index." || catalog->id ] INTO result;
  RETURN result;
}

RECORD FUNCTION GetNestedFields(RECORD query)
{
  RECORD fields := [ mapping := DEFAULT RECORD, highlightfields := STRING[] ];
  query := CellExists(query, "query") ? query.query : query;
  DELETE CELL aggregation, ordering FROM query;
  IF (NOT RecordExists(UnpackRecord(query)))
    RETURN fields;
  RETURN GetNestedFieldsRecursive(fields, query);
}

RECORD FUNCTION GetNestedFieldsRecursive(RECORD fields, RECORD query)
{
  SWITCH (query._type)
  {
    CASE "boolean"
    {
      FOREVERY (RECORD subquery FROM query.subqueries)
        fields := GetNestedFieldsRecursive(fields, subquery.query);
    }
    CASE "nested"
    {
      fields := GetNestedFieldsRecursive(fields, query.subquery);
      IF (query.scorefield != "")
        fields.mapping := CellInsert(fields.mapping, "_score." || query.field, query.scorefield);
      IF (query.summaryfield != "")
      {
        fields.mapping := CellInsert(fields.mapping, "_summary." || query.field, query.summaryfield);
        INSERT query.field || "." || query.summaryfield INTO fields.highlightfields AT END;
      }
      IF (query.totalcountfield != "")
        fields.mapping := CellInsert(fields.mapping, "_totalcount." || query.field, query.totalcountfield);
    }
  }
  RETURN fields;
}

/** Get completion suggestions for a text
    @param indextag Index to search (module:tag)
    @param searchtext The text to get suggestions for
    @cell options.count maximum number of suggestions to return (0 for all matches, defaults to 10)
    @cell(string) options.restrict_url For site searches: Only results with a URL starting with this restrict_url are
        returned
    @cell(string array) options.exclude_urls For site searches: Results with a URL starting with one of these exclude_urls
        are not returned
    @return List of suggestions
    @cell(string) return.text The suggestion text
*/
PUBLIC RECORD ARRAY FUNCTION GetConsilioSuggestions(STRING indextag, STRING searchtext, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions([ count := 10
                             , restrict_url := ""
                             , exclude_urls := STRING[]
                             , debug := FALSE
                             ], options);

  // No results if query is empty and no prefix is given
  IF (searchtext = "")
    RETURN DEFAULT RECORD ARRAY;

  OBJECT catalog := OpenConsilioCatalog(indextag);
  IF (NOT ObjectExists(catalog))
    THROW NEW Exception(`Cannot open catalog '${indextag}'`);

  // Get search results
  RECORD result := SuggestIndexManager(catalog->id, searchtext, CELL
      [ options.count
      , restrict_to := options.restrict_url
      , exclude_urls := options.exclude_urls
      , options.debug
      ]);
  IF (result.status != SearchOk)
    THROW NEW Exception("Suggest error: " || result.status);

  // Return all terms starting with "bla"
  RETURN SELECT num := suggestions.count //rename, 'count' is an annoying column to select, it clashes with the global AND count(*)
              , text
           FROM result.suggestions;
}
