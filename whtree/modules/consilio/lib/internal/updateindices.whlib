﻿<?wh

LOADLIB "wh::ipc.whlib";

LOADLIB "mod::consilio/lib/api.whlib";
LOADLIB "mod::consilio/lib/catalogs.whlib";
LOADLIB "mod::consilio/lib/internal/catalogdefparser.whlib";
LOADLIB "mod::consilio/lib/internal/dbschema.whlib";
LOADLIB "mod::consilio/lib/internal/opensearch/mapping.whlib";
LOADLIB "mod::consilio/lib/internal/opensearch.whlib";
LOADLIB "mod::consilio/lib/internal/finishhandler.whlib";

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/logging.whlib";
LOADLIB "mod::system/lib/resources.whlib";
LOADLIB "mod::system/lib/services.whlib";
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";

MACRO ObsoleteCatalogs(RECORD options)
{
  STRING ARRAY obsoletecatalogs;
  FOREVERY(RECORD catalognode FROM GetCustomModuleSettings("http://www.webhare.net/xmlns/system/moduledefinition", "obsoletecatalog"))
  {
    STRING catalogname := ToLowercase(`${catalognode.module}:${catalognode.node->GetAttribute("tag")}`);
    INSERT ToUppercase(catalogname) INTO obsoletecatalogs AT END;
  }

  RECORD ARRAY tocleanup := SELECT id, name
                              FROM consilio.catalogs
                              WHERE ToUppercase(name) LIKE ToUppercase(options.catalogmask)
                                    AND ToUppercase(name) IN obsoletecatalogs;

  IF(Length(tocleanup) > 0)
  {
    GetPrimary()->BeginWork();
    DELETE FROM consilio.catalogs WHERE id IN (SELECT AS INTEGER ARRAY id FROM tocleanup);
    GetConsilioFinishHandler()->ScheduleCleanup();
    GetPrimary()->CommitWork();
  }
}

MACRO UpdateManagedCatalog(OBJECT catalog, RECORD want, RECORD options)
{
  RECORD ARRAY currentsources := catalog->ListContentSources([__withdata := TRUE]);
  FOREVERY(RECORD source FROM want.contentsources)
  {
    INTEGER matchpos := (SELECT AS INTEGER #currentsources + 1
                           FROM currentsources
                          WHERE contentprovider = (source.contentobject != "" ? "consilio:custom" : VAR whconstant_consilio_contentprovider_site)
                                AND fsobject = source.fsobject
                                AND ToUppercase(tag) = ToUppercase(source.tag)) - 1;

    IF(matchpos >= 0)
    {
      STRING expect_library, expect_contentobject;
      IF(source.contentobject != "")
      {
        expect_library := Tokenize(source.contentobject,'#')[0];
        expect_contentobject := Tokenize(source.contentobject,'#')[1];
      }

      OBJECT contentsource := catalog->OpenContentSourceById(currentsources[matchpos].id);
      BOOLEAN mismatch;
      IF(currentsources[matchpos].definedby != source.definedby OR currentsources[matchpos].contentobject != source.contentobject)
        mismatch := TRUE;

      IF(expect_library != "")
      {
        IF(currentsources[matchpos].__data = "")
          mismatch := TRUE;
        ELSE
        {
          RECORD cursettings := EnforceStructure(CELL[library := "", contentobject := ""], DecodeHSON(currentsources[matchpos].__data));
          IF(cursettings.library != expect_library OR cursettings.contentobject != expect_contentobject)
            mismatch := TRUE;
        }
      }

      IF(mismatch)
      {
        contentsource->UpdateContentSource(CELL
            [ source.definedby
            , settings := CELL[library := expect_library, contentobject := expect_contentobject]
            , tag := source.tag
            ]);
      }

      IF(currentsources[matchpos].isorphan)
        contentsource->__ReactivateContentSource();

      DELETE FROM currentsources AT matchpos; //remove from cleanup list
      CONTINUE;
    }

    TRY
    {
      IF(source.contentobject = "")
        catalog->AddFolderToCatalog(source.fsobject, CELL[ source.definedby ]);
      ELSE
        catalog->AddCustomContentSource(source.tag, source.contentobject, CELL[ source.definedby ]);
    }
    CATCH(OBJECT e)
    {
      LogHarescriptException(e);
      PrintTo(2,`Failed to add catalog from ${source.definedby}: ${e->what}\n`);
      CONTINUE;
    }
  }

  //mark unreferred catalogs as orphan
  FOREVERY(RECORD makeorphansource FROM SELECT * FROM currentsources WHERE NOT isorphan)
    catalog->OpenContentSourceById(makeorphansource.id)->__MarkAsOrphan();
}

BOOLEAN FUNCTION AttemptZap(OBJECT catalog)
{
  Print(`Deleting attached indices for catalog '${catalog->tag}'\n`);
  FOREVERY(RECORD indexrec FROM catalog->ListAttachedIndices())
    IF(NOT indexrec.readonly)
      SendRawJSONToOpenSearch(indexrec.indexmanager, "DELETE", indexrec.indexname || catalog->suffixmask, DEFAULT RECORD);

  TRY
  {
    catalog->ApplyConfiguration();
  }
  CATCH(OBJECT e)
  {
    PrintTo(2, `Failed to reconfigure recreated indices for catalog '${catalog->tag}': ${e->what}\n`);
  }

  RETURN TRUE;
}

//TODO we assume we won't be inside a long running process. UpdateIncides should only run in CI and in a task..
RECORD ARRAY indexmanagers;

MACRO EnsureIndexManagerUp(INTEGER id)
{
  INTEGER idx := (SELECT AS INTEGER #indexmanagers + 1 FROM indexmanagers WHERE indexmanagers.id = VAR id) - 1;
  IF(idx < 0)
    THROW NEW Exception(`Unknown indexmanager #${id}`);
  WHILE(NOT indexmanagers[idx].isup)
  {
    TRY
    {
      RECORD ARRAY indices := ListOpenSearchIndices(indexmanagers[idx].id);
      /* +RECORD ARRAY
           +RECORD
            +DOCS: 0
            +HEALTH: 'red'
            +INDEXNAME: 'c_a41aadb1f49583fa2708e5c325ef19b3'
            +SHARDS: 1
            +SIZE: 0i64
            +STATUS: 'close'
            +UUID: 'ES4GCGCZSn2DNpKbMm_ngA'

        TODO do we als need to be able to wait for specific indices to be open? */
      indexmanagers[idx].isup := TRUE;
    }
    CATCH(OBJECT<SearchUnavailableException> e)
    {
      //Wait for it to come online TODO: we should probably log this after some wait time but I'm not sure what a reasonable give up timeout is
      Sleep(200);
    }
  }
}

MACRO UpdateModuleCatalogs(RECORD options)
{
  indexmanagers := SELECT *, isup := FALSE FROM ListIndexManagers() ORDER BY isdefault DESC;

  OBJECT trans := GetPrimary();

  RECORD ARRAY wantcatalogs := SELECT *
                                    , catalog := DEFAULT OBJECT
                                 FROM GetRequiredCatalogs(FALSE, options.catalogmask);

  BOOLEAN havedefaultindexmanager := Length(indexmanagers) >= 1 AND indexmanagers[0].isdefault;
  trans->BeginWork();
  FOREVERY(RECORD want FROM wantcatalogs)
  {
    OBJECT catalog := OpenConsilioCatalog(want.tag);
    IF(NOT ObjectExists(catalog))
    {
      IF(want.managed)
      {
        IF(NOT havedefaultindexmanager)
          CONTINUE; //we cannot create catalogs if the defaultindexmanager doesn't exist (yet))

        EnsureIndexManagerUp(indexmanagers[0].id); //create will need the indexmanager
      }
      catalog := CreateConsilioCatalog(want.tag, CELL[ want.managed, want.suffixed ]);
    }

    wantcatalogs[#want].catalog := catalog;
    IF(catalog->managed != want.managed)
    {
      PrintTo(2, `Catalog '${want.tag}' is incorrectly ${catalog->managed ? "" : "not "}managed, it probably needs to be recreated\n`);
      CONTINUE;
    }

    //Setup contentsources first so UpdateCatalog can immediately apply the proper mapping
    IF(want.managed)
      UpdateManagedCatalog(catalog, want, options);

    TRY
    {
      //UpdateCatalog will not do unneeded changes but *will* reverify and update the mapping based on fieldgroups and contentsources
      catalog->UpdateCatalog( CELL[ want.definedby, want.suffixed ]);
    }
    CATCH(OBJECT e)
    {
      LogHarescriptException(e);
      PrintTo(2, `Failed to update catalog '${want.tag}': ${e->what}\n`);
      CONTINUE;
    }
  }

  UndoScheduleUpdate();   //either UpdateManagedcatalog or UpdateCatalog may schedule one.. but we *are* updateindices so undo that
  trans->CommitWork();

  STRING ARRAY wantedtags := SELECT AS STRING ARRAY tag FROM wantcatalogs;
  RECORD ARRAY othermanagedcatalogs := SELECT catalog := OpenConsilioCatalogById(id)
                                            , managed
                                            , tag
                                         FROM ListConsilioCatalogs()
                                        WHERE ToUppercase(tag) LIKE ToUppercase(options.catalogmask)
                                              AND tag NOT IN wantedtags;

  FOREVERY(RECORD want FROM wantcatalogs CONCAT othermanagedcatalogs) //we also need to manage manually created managed catalogs
  {
    IF(NOT ObjectExists(want.catalog))
     CONTINUE;

    RECORD ARRAY indices := want.catalog->ListAttachedIndices();
    FOREVERY(RECORD indic FROM indices)
      EnsureIndexManagerUp(indic.indexmanager);

    IF(want.managed AND want.catalog->managed) //If we are supposed to manage the catalog, we'll restore indices if needed
    {
      //only add indices for managed catalogs that have something to index. cleanupindices will cleanup catalogs that lost all sources
      IF(Length(indices) = 0 AND Length(want.catalog->ListContentSources()) > 0)
      {
        Print(`Managed catalog '${want.tag}' has no attached index yet, adding one\n`);
        trans->BeginWork();
        want.catalog->AttachIndex(0);
        want.catalog->ReindexCatalog();
        trans->CommitWork();
      }
    }

    TRY
    {
      // zapbroken can autodestroy 'fix' anything broken, but shouldn't we be first comparing whether the index needs changes before blindly applying mappings everywhere ? (and then get rid of this double-try?)
      want.catalog->ApplyConfiguration(); //ensure any config is fresh/uptodate
    }
    CATCH(OBJECT e)
    {
      LogHarescriptException(e);
      PrintTo(2, `Failed to reconfigure catalog '${want.tag}': ${e->what}\n`);

      IF(options.zapbroken)
        AttemptZap(want.catalog);
    }

    /* Verify field mappings and update where needed. We do this for all registered indices
       (at some point during development we left unmanaged indices alone during consilio:update, but not during ApplyConfiguration. now
        that ApplyConfiguration is always async and relies on us to update, we shouldn't make that exception anymore (and probably should never have) */
    FOREVERY(RECORD opensearch FROM SELECT * FROM want.catalog->ListAttachedIndices() WHERE NOT readonly)
    {
      EnsureIndexManagerUp(opensearch.indexmanager);
      FOREVERY(STRING suffix FROM want.catalog->__ListSuffixes())
      {
        STRING fullname := opensearch.indexname || (suffix != "" ? "-" || suffix : "");

        RECORD res;
        TRY
        {
          res := GetOpenSearchCurrentMapping(opensearch.indexmanager, fullname);
        }
        CATCH(OBJECT e)
        {
          Print(`Catalog ${want.catalog->tag} index ${fullname}: GetOpenSearchCurrentMapping failed: ${e->what}\n`);
          CONTINUE;
        }

        RECORD ARRAY exp := want.catalog->GetExpectedMapping();
        RECORD ARRAY issues := CompareMappings(res,exp,"");
        IF (IsDefaultValue(issues))
          CONTINUE;

        IF(options.zapanymismatch AND AttemptZap(want.catalog))
          BREAK;

        FOREVERY(RECORD issue FROM issues)
        {
          //Not sure about printing to 2... once we have a way to fix/snooze/perma--ignore such issues ?
          Print(`Catalog ${want.catalog->tag} index ${fullname}: field '${issue.field}': ${issue.message} (${issue.definedby})\n`);
        }
      }
    }
  } //FOREVERY wantcatalogs
}

PUBLIC MACRO FixConsilioIndices(RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions(
      [ reportmissing := FALSE
      , catalogmask := "*"
      , zapbroken := FALSE
      , zapanymismatch := FALSE
      ], options);

  IF (NOT GetPrimary()->ColumnExists("consilio", "indexmanagers", "id"))
    RETURN; //consilio is not initialized yet!

  OBJECT lock := OpenLockManager()->LockMutex("consilio:updateindices");
  TRY
  {
    ObsoleteCatalogs(options);
    UpdateModuleCatalogs(options);
  }
  FINALLY
  {
    lock->Release();
  }

  // Broadcast indexfields event to reset cached module index fields (publisher search)
  BroadcastEvent("consilio:indexfields", DEFAULT RECORD);//TODO is this still needed? but UpdateConsilioIndices, now removed, broadcasted it
}
