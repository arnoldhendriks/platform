<?wh

LOADLIB "wh::promise.whlib";

LOADLIB "mod::consilio/lib/database.whlib";
LOADLIB "mod::consilio/lib/internal/opensearch.whlib";
LOADLIB "mod::consilio/lib/internal/support.whlib";


PUBLIC RECORD ARRAY FUNCTION RunConsilioChecks()
{
  RECORD ARRAY messages;

  OBJECT ARRAY promises;
  RECORD ARRAY indexmanagers := SELECT id
                                     , config := ParseIndexmanagerConfiguration(configuration)
                                  FROM consilio.indexmanagers;

  FOREVERY (RECORD indexmgr FROM indexmanagers)
    INSERT CreatePromise(PTR CheckIndexManager(indexmgr, #1, #2)) INTO promises AT END;
  OBJECT indexmanagerpromise := CreatePromiseAll(promises);

  RECORD ARRAY brokencontentsources := SELECT contentsources.id
                                            , catalogs.name
                                         FROM consilio.contentsources, consilio.catalogs
                                        WHERE contentsources.catalogid = catalogs.id
                                              AND contentsources.status = 4
                                              AND contentsources.tag NOT LIKE "$consilio$deleted$*";

  FOREVERY (RECORD source FROM brokencontentsources)
  {
    INSERT
        [ msg := `:Content source #${source.id} (${source.name}) has errors`
        , jumpto := [ app := "consilio:config", source := source, action := "errors" ]
        ] INTO messages AT END;
  }

  FOREVERY(OBJECT promise FROM promises)
    messages := messages CONCAT WaitForPromise(promise);

  RETURN messages;
}

MACRO CheckIndexManager(RECORD indexmgr, MACRO PTR onresolve, MACRO PTR onreject)
{
  RECORD ARRAY messages;
  RECORD status;
  TRY
  {
    status := GetOpensearchStatus(indexmgr.id);
  }
  CATCH (OBJECT e)
  {
    //not logging this as a HareScriptException. we're prepared for the indexmanager to be offline in this codepath and it will only trigger noticelog errors by intervalchecks
    INSERT CELL [ msg := `:Could not connect to index manager #${indexmgr.id}: ${e->what}` ] INTO messages AT END;
  }

  IF(RecordExists(status))
  {
    // If an OpenSearch manager has a disk watermark, it returns indexstatus 0
    IF (status.indexstatus = 0)
    {
      // Find the watermark
      STRING watermark := status.disk_watermark;
      // We don't have to show "false" (not threshold active) or "ok" (threshold not triggered)
      IF (watermark NOT IN [ "", "false", "ok" ])
      {
        // Watermark has the form <low|high|flood>=<value>, where value is percentage full, or byte value free
        STRING ARRAY parts := Tokenize(watermark, "=");
        INSERT CELL [ msg := `:Index manager #${indexmgr.id} has disk watermark ${parts[0]} (${parts[1] LIKE "*%" ? `> ${parts[1]} in use` : `< ${parts[1]} free`})`
                    , messagetag := [ type := "consilio:indexmanager.diskusage", indexmanager := indexmgr.id ]
                    ] INTO messages AT END;
      }
    }

    IF(indexmgr.config.warnheapusage != 0 AND status.jvm_heap_used_percent >= indexmgr.config.warnheapusage) //FIXME should be configurable in the runtime settings
      INSERT CELL [ msg := `:Index manager #${indexmgr.id} has heap memory usage ${status.jvm_heap_used_percent}% `
                  , messagetag := [ type := "consilio:indexmanager.memoryusage", indexmanager := indexmgr.id ]
                  ] INTO messages AT END;
  }

  onresolve(messages);
}
