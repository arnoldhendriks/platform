<?wh

LOADLIB "wh::dbase/transaction.whlib";

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/tasks.whlib";


STATIC OBJECTTYPE ConsilioFinishHandler EXTEND TransactionFinishHandlerBase
<
  BOOLEAN mustcleanupindices;
  BOOLEAN mustreconfigurecatalogs;
  MACRO PTR ARRAY execoncommit;

  UPDATE PUBLIC MACRO OnPreCommit(OBJECT transaction)
  {
    IF(this->mustcleanupindices)
      ScheduleTimedTask("consilio:cleanupindices"); //remove stale indices ASAP
    IF(this->mustreconfigurecatalogs)
      ScheduleTimedTask("consilio:update");
  }

  UPDATE PUBLIC MACRO OnCommit()
  {
    FOREVERY(MACRO PTR todo FROM this->execoncommit)
      todo();
  }

  // ---------------------------------------------------------------------------
  //
  // Public API
  //

  /// Schedule task consilio:cleanupindices
  PUBLIC MACRO ScheduleCleanup()
  {
    this->mustcleanupindices := TRUE;
  }

  /// Schedule task consilio:update
  PUBLIC MACRO ScheduleUpdate()
  {
    this->mustreconfigurecatalogs := TRUE;
  }

  PUBLIC MACRO UndoScheduleUpdate()
  {
    this->mustreconfigurecatalogs := FALSE;
  }

  /** Request reindexing a content source
      @param(object %ContentSource) contentsource Contentsource to reindex
  */
  PUBLIC MACRO RequestReindexContentSource(OBJECT contentsource)
  {
    INSERT PTR contentsource->ReindexContentSource() INTO this->execoncommit AT END;
  }
>;

PUBLIC OBJECT FUNCTION GetConsilioFinishHandler(BOOLEAN create DEFAULTSTO TRUE)
{
  OBJECT handler := GetPrimary()->GetFinishHandler("consilio:finish");
  IF (NOT ObjectExists(handler) AND create)
  {
    handler := NEW ConsilioFinishHandler;
    GetPrimary()->SetFinishHandler("consilio:finish", handler);
  }
  RETURN handler;
}

PUBLIC MACRO UndoScheduleUpdate()
{
  OBJECT handler := GetConsilioFinishHandler(FALSE);
  IF(ObjectExists(handler))
    handler->UndoScheduleUpdate();
}
