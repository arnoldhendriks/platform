<?wh

LOADLIB "wh::adhoccache.whlib";
LOADLIB "wh::datetime.whlib";
LOADLIB "wh::crypto.whlib";
LOADLIB "wh::devsupport.whlib";
LOADLIB "wh::files.whlib";
LOADLIB "wh::ipc.whlib";
LOADLIB "wh::util/algorithms.whlib";
LOADLIB "wh::xml/dom.whlib";

LOADLIB "mod::publisher/lib/siteprofiles.whlib";
LOADLIB "mod::publisher/lib/search/searchfilters.whlib";
LOADLIB "mod::publisher/lib/search/searchproviders.whlib";
LOADLIB "mod::publisher/lib/webtools/internal/formcomponents.whlib";
LOADLIB "mod::publisher/lib/internal/forms/results.whlib";
LOADLIB "mod::publisher/lib/internal/forms/support.whlib";
LOADLIB "mod::publisher/lib/internal/siteprofiles/cache.whlib";
LOADLIB "mod::publisher/lib/internal/siteprofiles/lookups.whlib";

LOADLIB "mod::tollium/lib/gettid.whlib";

LOADLIB "mod::consilio/lib/api.whlib";
LOADLIB "mod::consilio/lib/contentproviders/customcontent.whlib";
LOADLIB "mod::consilio/lib/parsers/parser.whlib";
LOADLIB "mod::consilio/lib/internal/whfscatalog/indexfields.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/logging.whlib";
LOADLIB "mod::system/lib/resources.whlib";
LOADLIB "mod::system/lib/whfs.whlib";
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";
LOADLIB "mod::system/lib/internal/whfs/drafts.whlib";
LOADLIB "mod::system/lib/internal/whfs/support.whlib";

PUBLIC RECORD FUNCTION HookRunner(STRING name, RECORD data)
{
  OpenPrimary();
  data := RunModuleHookTarget("publisher:whfsindex_priority", data);

  // Listen to all loaded libraries, non-builtin only
  STRING ARRAY eventmasks := GetResourceEventMasks(
      SELECT AS STRING ARRAY liburi
        FROM GetAllUsedHarescriptLibraries()
       WHERE liburi NOT LIKE "wh::*"
         AND (liburi LIKE "mod::*/*" ? SubString(Tokenize(liburi, "/")[0], 5) NOT IN whconstant_builtinmodules : TRUE));

  RETURN CELL[ data, eventmasks ];
}

RECORD FUNCTION GetCacheableModuleStartingPoints()
{
  RECORD ARRAY startingpoints;
  RECORD ARRAY added := GetCustomModuleSettings("http://www.webhare.net/xmlns/system/moduledefinition", "addtowhfsindex");
  FOREVERY(RECORD toadd FROM added)
  {
    STRING basepath := `/webhare-private/${toadd.module}/`;
    STRING targetpath := MergePath(basepath, toadd.node->GetAttribute("privatefolder")) || "/";
    IF(targetpath NOT LIKE basepath || "*") //scope escape?
      CONTINUE;

    INSERT [ folder := -1 //needs lookup
           , whfspath := targetpath
           , priority := toadd.node->GetAttribute("priority")
           , boost := ""
           ] INTO startingpoints AT END;
  }

  RECORD res := CallFunctionFromJob(Resolve("#HookRunner"), "publisher:whfsindex_priorities", CELL[ startingpoints ]);

  startingpoints := EnforceStructure(
    [ [ folder :=     -1
      , whfspath :=   ""
      , priority :=   ""
      , boost :=      ""
      ]
    ], res.data.startingpoints);

  STRING ARRAY eventmasks := res.eventmasks;

  startingpoints :=
      SELECT TEMPORARY priority_pos := SearchElement(["beforerepository","beforesites","aftersites"], priority)
           , folder
           , whfspath :=        folder > 0 ? OpenWHFSObject(folder)->whfspath : whfspath
           , priority :=        [5,1,3,5][priority_pos+1]
           , boost
        FROM startingpoints;

  RETURN [ ttl := 15 * 60 * 1000
         , value := startingpoints
         , eventmasks :=      [ "system:modulesupdate", ...res.eventmasks ]
         ];
}

PUBLIC RECORD ARRAY FUNCTION GetWHFSIndexModuleStartingPoints()
{
  RETURN GetAdhocCached([tyoe := "GetWHFSIndexModuleStartingPoints"], PTR GetCacheableModuleStartingPoints);
}

RECORD FUNCTION GetTypeSearchProviderUncached(INTEGER type, BOOLEAN isfolder)
{
  STRING ARRAY eventmasks := [ "system:whfs.types" ]; // broadcast after siteprofile recompile (searchproviders may have changed)
  RECORD typeinfo := DescribeContentTypeById(type, CELL[ isfolder, mockifmissing := TRUE ]);


  /* We need to regenerate whenever for files of this type
     any of the following changes:
     - filetype indexversion
     - searchcontentprovider
     - searchcontentprovider version */

  RECORD retval := [ typehash :=       EncodeBase16(GetSHA1Hash("whfstype:" || typeinfo.namespace))
                   , versionhash :=    ""
                   , searchprovider := ""
                   ];

  IF(typeinfo.searchcontentprovider != "")
  {
    RECORD scp := GetSearchContentProvider(typeinfo.searchcontentprovider);
    retval.versionhash := EncodeBase16(GetSHA1Hash(retval.typehash || "\t" || typeinfo.searchcontentprovider || "\t" || typeinfo.indexversion || "\t" || scp.version));
    retval.searchprovider := scp.objectname;
  }

  RETURN CELL
      [ value := retval
      , eventmasks
      ];
}

PUBLIC RECORD FUNCTION GetSearchSubfiles(RECORD fsobj)
{
  RECORD scp := GetAdhocCached(CELL[ fsobj.type, fsobj.isfolder ], PTR GetTypeSearchProviderUncached(fsobj.type, fsobj.isfolder));

  //TODO Cache searchprovider objects, they shouldn't need reconstruction (unless out of date but then *we* need to fully restart)
  OBJECT provider := scp.searchprovider != "" ? MakeObject(scp.searchprovider) : NEW SearchProviderBase;

  RECORD ARRAY subfiles;
  STRING ARRAY indextypes, indexversions;
  FOREVERY(RECORD subfile FROM provider->GetSearchContents(fsobj.id))
  {
    RECORD parsed_page := ParsePage(subfile.data, subfile.mimetype);
    IF (RecordExists(parsed_page) AND parsed_page.success)
    {
      INSERT WrapBlob(StringToBlob(parsed_page.text), "subresult" || #subfile || ".txt") INTO subfiles AT END;
      IF(#subfile = 0 AND CellExists(subfile,'indextypes'))
      {
        indextypes := subfile.indextypes;
        indexversions := subfile.indexversions;
      }
    }
    ELSE
      INSERT DEFAULT RECORD INTO subfiles AT END;
  }

  //Gather versions of any embedded widgets
  INTEGER ARRAY instancetypes :=
     SELECT AS INTEGER ARRAY DISTINCT fs_settings.instancetype
       FROM system.fs_settings
          , system.fs_instances
      WHERE fs_instances.fs_object = fsobj.id
            AND instancetype != 0
            AND fs_settings.fs_instance = fs_instances.id;

  IF(Length(instancetypes) > 0)
  {
    RECORD csp := GetCachedSiteProfiles();
    FOREVERY(RECORD widget FROM SELECT * FROM csp.contenttypes WHERE id IN instancetypes AND type = "widgettype")
    {
      STRING indextype := EncodeBase16(GetSHA1Hash(`widget:${widget.namespace}`));
      INSERT indextype INTO indextypes AT END;
      INSERT EncodeBase16(GetSHA1Hash(`${indextype} ${widget.filetype.indexversion}`)) INTO indexversions AT END;
    }
  }

  IF(scp.typehash != "")
    INSERT scp.typehash INTO indextypes AT END;
  IF(scp.versionhash != "")
    INSERT scp.versionhash INTO indexversions AT END;

  RETURN CELL[ fsobj.id
             , subfiles
             //TODO support indextypes/versions on more than one page? or should we just require that they're all supplied in one big block?
             , indextypes
             , indexversions
             ];
}

RECORD ARRAY FUNCTION Cleanup(RECORD ARRAY inmapping)
{
  RETURN SELECT *
              , DELETE _indexfield
              , properties := Cleanup(properties)
           FROM inmapping;
}

PUBLIC OBJECTTYPE WHFSObjectContentSource EXTEND CustomContentBase
<
  OBJECT trans;

  UPDATE PUBLIC RECORD ARRAY FUNCTION GetMapping()
  {
    RECORD ARRAY mapping :=
        [ [ name := "name", value := "" ]
        , [ name := "title", value := "" ]
        , [ name := "description", value := "" ]
        , [ name := "keywords", value := "" ]
        , [ name := "body", value := "" ]
        , [ name := "url", value := "", tokenized := FALSE ]
        , [ name := "whfsid", value := 0 ]
        , [ name := "whfsparent", value := 0 ]
        , [ name := "whfsobject", value := "", tokenized := FALSE ]
        , [ name := "whfstype", value := 0 ]
        , [ name := "whfspath", value := "" ]
        , [ name := "whfstree", value := INTEGER[] ]
        , [ name := "whfssite", value := 0 ]
        , [ name := "creationdate", value := DEFAULT DATETIME ]
        , [ name := "modificationdate", value := DEFAULT DATETIME ]
        ];

    RETURN mapping CONCAT Cleanup(GetIndexFields().mapping);
  }

  UPDATE PUBLIC MACRO PrepareForIndexing()
  {
    this->trans := OpenPrimary();
  }

  PUBLIC RECORD ARRAY FUNCTION GetWHFSStartingPoints()
  {
    RECORD ARRAY spoints := SELECT folder := id
                                 , priority := id = whconstant_whfsid_repository ? 2 : 4
                                 , boost := ""
                              FROM system.sites
                             WHERE id != whconstant_whfsid_webharebackend
                          ORDER BY id = whconstant_whfsid_repository DESC
                                 , outputweb != 0 DESC       // anything not being published can't be important ?
                                 , locked = FALSE DESC       // don't rush to index locked sites, that indicates them are likely being unmaintained and probaly less important
                                 , Length(outputfolder);     // prefer 'homepages' over deeper sites


    FOREVERY(RECORD modulepath FROM GetWHFSIndexModuleStartingPoints())
    {
      INTEGER startfolder := modulepath.folder > 0 ? modulepath.folder : LookupWHFSObject(0, modulepath.whfspath);
      IF(startfolder <= 0) //not found, or ... root?!
      {
        PRINT(`${modulepath.whfspath} not found\n`);
        CONTINUE;
      }

      INSERT CELL[ folder := startfolder, modulepath.priority, modulepath.boost ] INTO spoints AT END;
    }

    spoints := SELECT * FROM spoints ORDER BY priority, boost DESC;

    RETURN spoints;
  }

  UPDATE PUBLIC RECORD FUNCTION ListGroups(DATETIME indexdate)
  {
    RECORD ARRAY groups;

    FOREVERY(RECORD startingpoint FROM this->GetWHFSStartingPoints())
    {
      INTEGER ARRAY sitefolders := INTEGER[startingpoint.folder] CONCAT GetWHFSDescendantIds(startingpoint.folder, TRUE, TRUE, 32, FALSE); //folders: true, files: TRUE, maxdepth: 32, onlypublished: false
      groups := groups CONCAT
          SELECT id := "fsobj_" || id
            FROM ToRecordArray(sitefolders,"id");
    }

    // the starting points can be nested, so do an afterfilter to get rid of double
    // group ids. Keep the first one (the startingpoints were already ordered)
    groups := SELECT id FROM groups GROUP BY id ORDER BY Any(#groups);

    RETURN [ status := "result"
           , groups := groups
           ];
  }

  UPDATE PUBLIC RECORD FUNCTION ListObjects3(DATETIME indexdate, STRING groupid, RECORD ARRAY current_objects, RECORD options)
  {
    DATETIME start := GetCurrentDatetime();
    STRING ARRAY parts := Tokenize(groupid, "_");
    INTEGER fsobjid := Length(parts) >= 2 AND parts[0] = "fsobj" ? ToInteger(parts[1], 0) : 0;
    OBJECT fsobj := OpenWHFSObject(fsobjid);
    IF(NOT ObjectExists(fsobj))
    {
      //Most likely: object already deleted since the commit that queued us
      RETURN [ status := "result"
             , objects := RECORD[]
             ];
    }

    //If an object is not active (outside 'normal' whfs parts) verify if it's one of the explicitly indexed starting points
    IF(NOT fsobj->isactive AND NOT RecordExists(SELECT FROM GetWHFSIndexModuleStartingPoints() AS spoint WHERE ToUppercase(fsobj->whfspath) LIKE ToUppercase(spoint.whfspath) || "*"))
    {
      RETURN [ status := "result"
             , objects := RECORD[]
             ];
    }

    RECORD ARRAY objects;
    IF (ObjectExists(fsobj))
    {
      STRING ARRAY whfstree;
      INTEGER parent := fsobj->id;
      WHILE (parent > 0)
      {
        INSERT ToString(parent) INTO whfstree AT 0;
        parent := SELECT AS INTEGER COLUMN parent
                    FROM system.fs_objects
                   WHERE id = VAR parent;
      }

      RECORD basefields :=
          [ name := fsobj->name
          , title := fsobj->title
          , description := fsobj->description
          , keywords := fsobj->keywords
          , url := fsobj->indexurl
          , whfsid := fsobj->id
          , whfsparent := fsobj->parent
          , whfsobject := fsobj->isfolder ? "folder" : "file"
          , whfstype := fsobj->type
          , whfspath := fsobj->whfspath
          , whfstree := whfstree
          , whfssite := fsobj->parentsite
          , creationdate := fsobj->creationdate
          , modificationdate := fsobj->modificationdate
          ];

      TRY
      {
        RECORD searchres := GetSearchSubfiles(CELL
            [ fsobj->id
            , fsobj->type
            , fsobj->isfolder
            , fsobj->parent
            ]);

        RECORD firstdocfields := CELL[ ...basefields
                                     , ...GetParsedIndexFields(fsobj, 0)
                                     ];
        firstdocfields.mod_consilio := CELL[ ...firstdocfields.mod_consilio
                                           , searchres.indexversions
                                           , searchres.indextypes
                                           ];

        INSERT
            [ id := "fsobj_" || fsobj->id
            , data := CELL
                [ status := "result"
                , document_body := Length(searchres.subfiles) > 0 AND RecordExists(searchres.subfiles[0]) ? BlobToString(searchres.subfiles[0].data) : ""
                , document_fields := firstdocfields
                ]
            ] INTO objects AT END;

        FOREVERY (RECORD subfile FROM searchres.subfiles)
        {
          IF (#subfile = 0)
            CONTINUE;
          INSERT
              [ id := "fsobj_" || fsobj->id || "_" || #subfile
              , data := CELL
                  [ status := "result"
                  , document_body := RecordExists(subfile) ? BlobToString(subfile.data) : ""
                  , document_fields := CELL[...basefields, ...GetParsedIndexFields(fsobj, #subfile) ]
                  ]
              ] INTO objects AT END;
        }
      }
      CATCH (OBJECT e)
      {
        LogHarescriptException(e);

        IF(IsDebugTagEnabled("consilio:whfscatalog"))
        {
          LogDebug("consilio:whfscatalog", "ListObjects3", CELL [ runtime_ms := GetMsecsDifference(start, GetCurrentDatetime())
                                                                , fsobj->id
                                                                , fsobj->whfspath
                                                                , fsobj->type
                                                                , fsobj->parentsite
                                                                , error := e->what
                                                                ]);
        }

        RETURN [ status := e EXTENDSFROM LibraryVersionConflictException ? "outofdate" : "error"
               , error := e->what
               ];
      }
    }

    //TODO stats like this should be enable-able for all contentsources, not just us.. but we still need information such as whfspath of the actual objects, not ids. return some context ?
    IF(IsDebugTagEnabled("consilio:whfscatalog"))
    {
      LogDebug("consilio:whfscatalog", "ListObjects3", CELL [ runtime_ms := GetMsecsDifference(start, GetCurrentDatetime())
                                                            , numobjects := Length(objects)
                                                            , fsobj->id
                                                            , fsobj->whfspath
                                                            , fsobj->type
                                                            , fsobj->parentsite
                                                            ]);
    }
    RETURN [ status := "result"
           , objects := objects
           ];
  }
>;

PUBLIC VARIANT FUNCTION GetItemField(STRING fieldname, INTEGER objectid)
{
  SWITCH (fieldname)
  {
    CASE "hasdraft"
    {
      RECORD ARRAY drafts := objectid != 0 ? GetDrafts(objectid) : RECORD[];
      RETURN RecordExists(SELECT FROM drafts WHERE ispublic) ? "1" : "";
    }

    CASE "formfields"
    {
      RECORD instancedata := OpenWHFSType("http://www.webhare.net/xmlns/publisher/formwebtool")->Enrich([ [ id := objectid ] ], "id", [ "data" ]);

      RECORD ARRAY allcomps;
      IF (RecordExists(instancedata.data))
      {
        RECORD ARRAY allcomponents := GetSearchableFormComponents();
        OBJECT xmldoc := MakeXMLDocument(instancedata.data.text, "utf-8", TRUE);
        OBJECT ARRAY containernodes :=
            xmldoc->GetElementsByTagNameNS(xmlns_forms, "page")->GetCurrentElements()
            CONCAT
            xmldoc->GetElementsByTagNameNS(xmlns_forms, "group")->GetCurrentElements()
            CONCAT
            xmldoc->GetElementsByTagNameNS(xmlns_forms, "handlers")->GetCurrentElements()
            CONCAT
            xmldoc->GetElementsByTagNameNS(xmlns_forms, "trash")->GetCurrentElements();
        FOREVERY (OBJECT containernode FROM containernodes)
          FOREVERY (OBJECT compnode FROM containernode->childnodes->GetCurrentElements())
            IF (compnode->namespaceuri != xmlns_forms OR compnode->localname != "group")
            {
              RECORD ARRAY matching_components := SELECT * FROM allcomponents WHERE namespace = compnode->namespaceuri AND name = compnode->localname;
              RECORD component;
              FOREVERY (RECORD match FROM matching_components)
              {
                IF (match.is_handler OR ComponentMatches(compnode, match))
                {
                  component := match;
                  BREAK;
                }
              }
              STRING qname := RecordExists(component) ? component.qname : compnode->namespaceuri || "#" || compnode->localname;
              RECORD pos := RecordLowerBound(allcomps, CELL[ qname ], [ "qname" ]);
              IF (NOT pos.found)
                INSERT CELL[ qname ] INTO allcomps AT pos.position;
            }
      }
      RETURN SELECT AS STRING ARRAY qname FROM allcomps;
    }

    CASE "storeresults"
    {
      //FIXME we invoke this for every file, even though most aren't forms. Fix that!
      RECORD instancedata := OpenWHFSType("http://www.webhare.net/xmlns/publisher/formwebtool")->Enrich([ [ id := objectid ] ], "id", [ "data", "storeresults" ]);
      IF (NOT RecordExists(instancedata.data))
        RETURN "";

      INTEGER storeresults := GetFormResultsExpirationDaysByInstance(GetApplyTesterForObject(objectid), instancedata).storeresults;
      RETURN Right("0000000" || storeresults, 8);
    }
  }
  THROW NEW Exception(`Unknown item field '${fieldname}'`);
}

PUBLIC OBJECTTYPE HasDraftFilter EXTEND BaseSearchFilter
<
  UPDATE PUBLIC RECORD FUNCTION GetFields()
  {
    RETURN
        [ valuequery :=  [ [ rowkey := "1", title := GetTid("consilio:tolliumapps.whfsindex.filtertypes.file-hasdraft") ] ]
        , valuequerytype := "checkbox"
        ];
  }

  UPDATE PUBLIC RECORD FUNCTION GetQuery(RECORD value)
  {
    IF (Length(value.valuequery) = 0) // valuequery is either STRING[] OR [ "1" ]
      RETURN DEFAULT RECORD;

    RETURN CQMatch("mod_publisher.hasdraft", "IN", value.valuequery);
  }
>;

PUBLIC STATIC OBJECTTYPE FormFieldsFilter EXTEND BaseSearchFilter
<
  RECORD ARRAY allcomponents;

  MACRO NEW()
  {
  }

  UPDATE PUBLIC RECORD FUNCTION GetFields()
  {
    RETURN
        [ matchlabel := GetTid("consilio:tolliumapps.whfsindex.filtertypes.form-contains")
        , selectquery := DEFAULT RECORD
        ];
  }

  UPDATE PUBLIC RECORD FUNCTION SelectQuery(RECORD value)
  {
    IF(Length(this->allcomponents) = 0)
      this->allcomponents := GetSearchableFormComponents();

    STRING qname := this->context->filterscreen->RunScreen("mod::publisher/tolliumapps/formedit/formcomponent.xml#selectcomponent",
        [ components := this->allcomponents
        , type := "pubsearch"
        , value := RecordExists(value.selectquery) ? value.selectquery.component : ""
        ]);
    IF (qname = "")
      RETURN DEFAULT RECORD;
    RECORD comptype := SELECT * FROM this->allcomponents WHERE COLUMN qname = VAR qname;
    RETURN
        CELL[ ...value
            , selectquery :=
                [ component := qname
                , showvalue := RecordExists(comptype) ? comptype.title : qname
                ]
            ];
  }

  UPDATE PUBLIC RECORD FUNCTION GetQuery(RECORD value)
  {
    IF (RecordExists(value.selectquery))
      RETURN CQMatch("mod_publisher.formfields", "=", value.selectquery.component);
    RETURN DEFAULT RECORD;
  }
>;

RECORD ARRAY FUNCTION GetSearchableFormComponents()
{
  RECORD ARRAY allcomponents :=
      (SELECT *
            , title := GetTid(title)
            , tid := title
            , description := GetTid(description)
            , qname := FormatQName(components)
            , builtin := namespace = xmlns_forms
            , is_question := TRUE
            , is_handler := FALSE
         FROM GetAllFormComponents() AS components)
      CONCAT
      (SELECT *
            , title := GetTid(title)
            , description := GetTid(description)
            , qname := FormatQName(components)
            , builtin := namespace = xmlns_forms
            , is_question := FALSE
            , is_handler := TRUE
         FROM GetAllFormHandlers() AS components);
  DELETE FROM allcomponents
   WHERE namespace = xmlns_forms AND name IN [ "page", "group" ];
  RETURN allcomponents;
}

PUBLIC OBJECTTYPE StoreResultsFilter EXTEND BaseSearchFilter
<
  UPDATE PUBLIC RECORD FUNCTION GetFields()
  {
    RETURN
          // Range queries are not fully supported by Consilio (at least not in OR combinations), so disable the matchtype
          // relying on range queries
        [ matchtype :=
            (SELECT *
               FROM matchtypevalue
              WHERE rowkey NOT IN [ "less", "lessorequal", "greater", "greaterorequal" ])
        , textquery := ""
        ];
  }

  UPDATE PUBLIC RECORD FUNCTION GetQuery(RECORD value)
  {
    INTEGER intvalue := ToInteger(value.textquery, -2);
    // The value -1 is only used to check if the default value is (not) used
    IF (intvalue = -1)
    {
      IF (value.matchtype = "equal")
        RETURN CQMatch("mod_publisher.storeresults", "=", "-1");
      ELSE IF (value.matchtype = "notequal")
        RETURN CQNot(CQMatch("mod_publisher.storeresults", "=", "-1"));
    }
    ELSE IF (intvalue >= 0)
    {
      INTEGER defaultvalue := ReadRegistryKey("publisher.forms.storeresults");
      STRING matchvalue := Right("0000000" || intvalue, 8);
      RECORD query;
      SWITCH (value.matchtype)
      {
        CASE "equal"
        {
          query := CQMatch("mod_publisher.storeresults", "=", matchvalue);
          IF (intvalue = defaultvalue)
            query := CQOr([ query, CQMatch("mod_publisher.storeresults", "=", "-1") ]);
        }
        CASE "notequal"
        {
          query := CQNot(CQMatch("mod_publisher.storeresults", "=", matchvalue));
          IF (intvalue = defaultvalue)
            query := CQAnd([ query, CQNot(CQMatch("mod_publisher.storeresults", "=", "-1")) ]);
        }
        CASE "less"
        {
          query := CQMatch("mod_publisher.storeresults", "<", matchvalue);
          IF (defaultvalue < intvalue) // Forms with default value match
            query := CQOr([ query, CQMatch("mod_publisher.storeresults", "=", "-1") ]);
          ELSE // Forms with default value don't match
            query := CQAnd([ query, CQNot(CQMatch("mod_publisher.storeresults", "=", "-1")) ]);
        }
        CASE "lessorequal"
        {
          query := CQMatch("mod_publisher.storeresults", "<=", matchvalue);
          IF (defaultvalue <= intvalue) // Forms with default value match
            query := CQOr([ query, CQMatch("mod_publisher.storeresults", "=", "-1") ]);
          ELSE // Forms with default value don't match
            query := CQAnd([ query, CQNot(CQMatch("mod_publisher.storeresults", "=", "-1")) ]);
        }
        CASE "greater"
        {
          query := CQMatch("mod_publisher.storeresults", ">", matchvalue);
          IF (defaultvalue > intvalue) // Forms with default value match
            query := CQOr([ query, CQMatch("mod_publisher.storeresults", "=", "-1") ]);
          ELSE // Forms with default value don't match
            query := CQAnd([ query, CQNot(CQMatch("mod_publisher.storeresults", "=", "-1")) ]);
        }
        CASE "greaterorequal"
        {
          query := CQMatch("mod_publisher.storeresults", ">=", matchvalue);
          IF (defaultvalue >= intvalue) // Forms with default value match
            query := CQOr([ query, CQMatch("mod_publisher.storeresults", "=", "-1") ]);
          ELSE // Forms with default value don't match
            query := CQAnd([ query, CQNot(CQMatch("mod_publisher.storeresults", "=", "-1")) ]);
        }
      }
      RETURN query;
    }
    RETURN DEFAULT RECORD;
  }
>;
