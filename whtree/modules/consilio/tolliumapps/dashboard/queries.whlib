<?wh

LOADLIB "mod::consilio/lib/internal/dbschema.whlib";
LOADLIB "mod::consilio/lib/internal/opensearch.whlib";

LOADLIB "mod::system/lib/screenbase.whlib";
LOADLIB "mod::system/lib/internal/modules/json.whlib";

LOADLIB "mod::tollium/lib/dialogs.whlib";
LOADLIB "mod::tollium/lib/screenbase.whlib";

PUBLIC STATIC OBJECTTYPE Queries EXTEND DashboardPanelBase
<
  RECORD ARRAY seenqueries;

  MACRO NEW()
  {

  }
  UPDATE PUBLIC MACRO EnableRefresh()
  {
    ^consiliolistener->masks := ["consilio:executedquery"];
  }
  UPDATE PUBLIC MACRO DisableRefresh()
  {
    ^consiliolistener->masks := DEFAULT STRING ARRAY;
  }

  UPDATE PUBLIC MACRO OnConsilioEvents(RECORD ARRAY events)
  {
    FOREVERY(RECORD evt FROM events)
    {
      IF(evt.data.isstart)
      {
        INSERT evt.data INTO this->seenqueries AT END;
      }
      ELSE
      {
        //ADDME: if evt.isstart = false, we can update numresults and the time the query took
      }
    }

    IF(Length(this->seenqueries) > 100)
      this->seenqueries := ArraySlice(this->seenqueries, Length(this->seenqueries)-100, 100);

    this->RefreshEvents();
  }
  PUBLIC MACRO DoClearQueries()
  {
    this->seenqueries := RECORD[];
    this->RefreshEvents();
  }
  UPDATE PUBLIC MACRO RefreshDashboardPanel() //called whenever a panel should refresh itself (either manually invoked by user, or automatically)
  {
    this->RefreshEvents();
  }

  MACRO RefreshEvents()
  {
    ^queries->rows := SELECT rowkey := uuid
                           , query := CellExists(seenqueries,'query') ? query : EncodeJSON(searchquery) //ES gives us searchquery
                           , searchquery := CellExists(seenqueries,'searchquery') ? searchquery : DEFAULT RECORD
                           , catalog
                           , when := now
                        FROM this->seenqueries;
}

  UPDATE PUBLIC INTEGER FUNCTION GetSuggestedRefreshFrequency()
  {
    RETURN 0;
  }

  MACRO DoOpenQuery()
  {
    RECORD query := ^queries->selection;
    this->RunScreen(Resolve("queries.xml#inspectquery"),
        [ indexmanager := query.catalog
        , query := query.searchquery
        ]);
  }
>;

PUBLIC STATIC OBJECTTYPE InspectQuery EXTEND TolliumScreenBase
<
  MACRO Init(RECORD data)
  {
    ^indexmanager->options := SELECT rowkey := id, title := name, address
                                FROM consilio.indexmanagers
                            ORDER BY ToUppercase(name);
    ^indexmanager->SetValueIfValid(data.indexmanager);
    ^query->value := PrettifyJSON(EncodeJSON(data.query));
  }
  MACRO DoRunQuery()
  {
    TRY
    {
      RECORD query := DecodeJSON(^query->value);
      IF(NOT RecordExists(query))
        THROW NEW Exception("Invalid JSON format");

      RECORD res := SendRawJSONToOpenSearch(^indexmanager->value, query.method, query.path, query.body);
      ^result->value := PrettifyJSON(EncodeJSON(res.result));
    }
    CATCH(OBJECT e)
    {
      RunExceptionReportDialog(this, e);
    }
  }
>;
