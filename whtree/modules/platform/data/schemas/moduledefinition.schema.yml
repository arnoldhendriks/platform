---
# yaml-language-server: $schema=http://json-schema.org/draft-07/schema#
# To update derived the TypeScript interfaces: wh apply dev
"$schema": http://json-schema.org/draft-07/schema#
title: Module definition
description: A module definition tells WebHare which features and integrations are offered by this module
type: object
additionalProperties: false
properties:
  backendHooks:
    $ref: "#/$defs/BackendHooks"
  managedServices:
    description: Additional services
    $ref: "#/$defs/ManagedServices"
  backendServices:
    description: Additional services
    $ref: "#/$defs/BackendServices"
  openApiServices:
    description: OpenAPI services
    $ref: "#/$defs/OpenApiServices"
  assetPacks:
    description: JavaScript/asset bundles
    $ref: "#/$defs/AssetPacks"
  webFeatures:
    description: "Web features (apply additional siteprofiles)"
    $ref: "#/$defs/WebFeatures"
  selfChecks:
    description: Adds automatic selfchecks
    $ref: "#/$defs/SelfChecks"
  moduleFileTypes:
    description: Register file types used to build modules
    $ref: "#/$defs/ModuleFileTypes"
  pxlEvents:
    description: Declare pxl event types
    $ref: "#/$defs/PxlEvents"

$defs:
  # Server DTAP stages
  DtapStage:
    type: string
    enum:
      - development
      - test
      - acceptance
      - production

  # IfWebHare rules restrict configuration rules to specific installations or versions
  IfWebHare:
    type: object
    additionalProperties: false
    properties:
      whVersion:
        type: string
        description: "WebHare version to apply the rule to"
      dtapStages:
        description: "DTAP stages to which the rule applies"
        type: array
        items:
          $ref: "#/$defs/DtapStage"
      serverNames:
        description: "Restrict to specific servers (by server name)"
        type: array
        items:
          type: string
      envVars:
        description: "Only apply if the environment variable is set"
        type: array
        items:
          type: string
      ifModules:
        description: "Only apply if the specific modules are installed"
        type: array
        items:
          type: string
      not:
        description: "Specifies as set of restrictions of which neither should be met"
        type: array
        items:
          $ref: "#/$defs/IfWebHare"

  # managedServices are additional services to have the servicemanager start
  ManagedServices:
    type: object
    additionalProperties: false
    patternProperties:
      "^[a-z_][-a-z0-9_]*$": # Service name
        type: object
        additionalProperties: false
        properties:
          script:
            description: "Script which implements the service (must have whscr, js or ts extesion)"
            type: string
          run:
            description: "Whether to always run the service or only ondemand"
            type: string
            enum: # "once" is not exposed, you should use tasks instead
              - always
              - on-demand
          arguments:
            description: "Command line arguments for script"
            type: array
            items:
              type: string
          engine:
            description: "HareScript engine to use"
            type: string
            enum:
              - native
              - wasm
          ifWebHare:
            description: "Restrict service to specific installations or versions"
            $ref: "#/$defs/IfWebHare"
          minRunTime:
            description: "Minimum amount of time the service must have been running before being considered stable and not require a throttle during restart. Defaults to 60_000 (1 minute)"
            type: integer
            minimum: 0
          maxThrottleMsecs:
            description: "Maximum time to throttle the service restart if it exits unexpectedly. Defaults to 60_000 (1 minute)"
            type: integer
            minimum: 0
        required:
          - script
          - run

  # backendHooks are a generic way to extend the WebHare backend (user interface)
  # NOTE In WH5.3 this was an object but we want to enforce unique names per hook so we can do things like 'before/after' (ordering) in hte future
  BackendHooks:
    description: WebHare backend (user interface) integrations
    type: object
    additionalProperties: false
    patternProperties:
      "^[a-z_][-a-z0-9_]*$": # Service name
        type: object
        additionalProperties: false
        required:
          - screen
        properties:
          screen:
            type: string
          when:
            $ref: "#/$defs/ContextFilter"
          tabsExtension:
            type: string
          addActions:
            type: array
            items:
              type: object
              additionalProperties: false
              required: [category, title, onExecute]
              properties:
                title:
                  type: string
                  description: "Title for the action"
                category:
                  type: string
                  description: "Action category (possible values defined by the screen)"
                onExecute:
                  type: string
                  description: "Function to invoke"

  BackendServices:
    description: backendServices implement the actual services for openBackendService
    type: object
    additionalProperties: false
    patternProperties:
      "^[a-z_][-a-z0-9_]*$": # Service name
        type: object
        additionalProperties: false
        properties:
          clientFactory:
            description: "Function to create a client for this service. Must satisfy ServiceClientFactoryFunction"
            type: string
          controllerFactory:
            description: "Function to create a controller for this service which will create the clients. Must satisfy ServiceControllerFactoryFunction"
            type: string
          coreService:
            description: "Core services start up during WebHare bootup (and may find database and other configuration to still be incomplete!)"
            type: boolean

  OpenApiServices:
    description: Specifies an OpenAPI service
    type: object
    additionalProperties: false
    patternProperties:
      "^[a-z_][-a-z0-9_]*$": # Service name
        type: object
        additionalProperties: false
        required:
          - spec
        properties:
          spec:
            description: "Path to the OpenAPI V3 specification"
            type: string
          initHook:
            description: "Callback to do runtime modifications to the OpenAPI configuration"
            type: string
          handlerInitHook:
            description: "Callback to do runtime modifications to the OpenAPI call handler"
            type: string
          merge:
            type: array
            description: "Optional overrides for the specification"
            items:
              type: string
              description: "Path to an override file"
          # TODO inputValidation & outputValidation? or should that be server configuration ?
          crossDomainOrigins:
            description: "Allow sites on these domains direct access"
            type: array
            items:
              type: string
              description: "Host to permit ('*' is supported as a wildcard)"

  # Filter where backend hooks are applied
  ContextFilter:
    type: object
    additionalProperties: false
    properties:
      wrdSchema:
        type: string
        # TODO? support masks in the future, but how to properly combine that with support for optionally module-scoped schemas?
        description: "WRD schema to apply the hook to"

  # assetPacks configure the JS bundler
  AssetPacks:
    type: object
    additionalProperties: false
    patternProperties:
      "^[a-z_][-a-z0-9_]*$": # Assetpack name
        type: object
        additionalProperties: false
        properties:
          entryPoint:
            description: "Initial script for the asset pack"
            type: string
          supportedLanguages:
            description: "Languages to compile into the asset pack"
            type: array
            items:
              # We don't suport the full range of language codes yet - this is what we tested with
              type: string
              description: "ISO-639 language codes, eg 'en-US' or 'nl'"
              pattern: "[a-z]{2}(-[A-Z]{2})?"
          compatibility:
            description: "The compatibility settings for generated code. es2016-es2021 or esnext. See also https://esbuild.github.io/content-types/#javascript"
            type: string
          whPolyfills:
            description: "Whether to include WebHare polyfills (defaults to true)"
            type: boolean
          module:
            description: "Output an ES module. This setting no longer applies to WH5.5 and up which will always generate and expect modules"
            type: boolean
          afterCompileTask:
            description: "Task to run after compiling the asset pack"
            type: string
          esBuildPlugins:
            description: "Custom plugins to add"
            type: array
            items:
              type: object
              additionalProperties: false
              required: [plugin]
              properties:
                plugin:
                  description: "Path to the plugin"
                  type: string
                pluginOptions:
                  description: "Options to pass to the plugin function"
                  type: array

        required:
          - entryPoint

  SelfChecks:
    description: Add checks to run as part of the automatic checks
    type: array
    items:
      type: object
      additionalProperties: false
      properties:
        checker:
          description: Path to function to execute. Must satify CheckFunction
          type: string
      required:
        - checker

  ModuleFileTypes:
    description: Register YAML filetypes and their schema definitions
    type: array
    items:
      type: object
      additionalProperties: false
      properties:
        resourceMask:
          description: RegExp to match the resource name
          type: string
        schema:
          description: Path to schema.org schema to validate against. Allows both YAML and JSON schemas
          type: string
        contentValidator:
          description: Path to function to execute for further validation checks. Must satisfy ContentValidationFunction
          type: string
        tsType:
          type: string
          description: "If set, compile the JSON schema to a TypeScript definition with this name."
      required:
        - resourceMask
        - schema

  WebFeatures:
    type: object
    additionalProperties: false
    patternProperties:
      "^[a-z_][-a-z0-9_]*$": # Webfeature name
        type: object
        additionalProperties: false
        properties:
          siteProfile:
            type: string
            description: "Site profile to apply"
          title:
            type: string
          hidden:
            type: boolean
          webDesignMasks:
            type: array
            items:
              type: string
            description: "Limit webdesigns to which this feature can apply"

  PxlEvents:
    type: object
    additionalProperties: false
    patternProperties:
      "^[a-z_][-a-z0-9_]*$": # Event name
        type: object
        additionalProperties: false
        properties:
          fields:
            type: object
            description: "Fields sent with event"
            additionalProperties: false
            patternProperties:
              "^[a-z_][-a-z0-9_]*$": # Field name
                type: string
                enum:
                  - boolean
                  - keyword
                  - integer
          includeFields:
            type: "string"
            description: "Include fields from another event"
