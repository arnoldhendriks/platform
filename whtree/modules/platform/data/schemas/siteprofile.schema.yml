---
# yaml-language-server: $schema=http://json-schema.org/draft-07/schema#
# (To update derived the TypeScript interfaces: wh apply dev)
# TODO we leave some parts open now to allow for custom elements... but we should be able to compile to a complete schema allowing only supported elements
"$schema": http://json-schema.org/draft-07/schema#
title: Site profile
type: object
additionalProperties: false
properties:
  gid:
    type: string
  typeGroup:
    type: string
  types:
    type: object
    additionalProperties: false
    patternProperties:
      "^[a-z][a-zA-Z0-9]*$": # Explicitly not permitting underscores, expecting camelcase. TODO allow URIs too
        $ref: "#/$defs/Type"
        type: object
  apply:
    type: array
    items:
      $ref: "#/$defs/Apply"

$defs:
  Type:
    type: object
    additionalProperties: false
    properties:
      namespace:
        type: string
        description: "The namespace assigned to the type. If not set, it will be `x-webhare-scopedtype:module.type_group.type` (snake-cased)"
      gid:
        type: string
      title:
        type: string
      tid:
        type: string
      members:
        $ref: "#/$defs/TypeMembers"

  TypeMembers:
    type: object
    additionalProperties: false
    patternProperties:
      "^[a-z][a-zA-Z0-9]*$": # Explicitly not permitting underscores, expecting camelcase
        # TODO we could consider mapping underscores to double-underscores when snake-casing and then even allow them in camelcaps names?
        $ref: "#/$defs/TypeMember"
        type: object

  TypeMember:
    type: object
    additionalProperties: false
    required:
      - "type"
    properties:
      type:
        type: string
        enum:
          - string
          - integer
          - datetime
          - date
          - file
          - image
          - boolean
          - float
          - money
          - whfsref
          - array
          - whfsrefarray
          - stringarray
          - richdocument
          - intextlink
          - instance
          - url
          - composeddocument
          - hson
          - record
          # - formcondition  FIXME is this really declarable?
      tid:
        type: string
      title:
        type: string
      comment:
        type: string
        description: "A developer's comment on this type. Not shown to non-prviileged WebHare users."
      members: # TODO limit to arrays only
        $ref: "#/$defs/TypeMembers"
      constraints:
        $ref: "#/$defs/ValueConstraints"
      component:
        $ref: "#/$defs/Component"
      line:
        $ref: "#/$defs/ComponentLine"
      lines:
        $ref: "#/$defs/ComponentLines"
      layout:
        $ref: "#/$defs/FieldLayout"

  FieldLayout:
    type: string
    description: "Force fields to be presented as a block or separate section"
    enum:
      - section
      - block

  ValueConstraints: # TODO we need to share this with Tollium components
    type: object
    description: Record constraints for a Tollium component value
    additionalProperties: false
    properties:
      valueType:
        description: Type of value used
        type: string
        enum:
          - string
          - integer
          - float
          - money
          - boolean
          - dateTime
          - date
          - resourceDescriptor
          - imageDescriptor
          - fsObjectId
          - array
          - richDocument
      itemType:
        description: Type of value used in an array
        type: string
        enum:
          - string
          - integer
          - float
          - money
          - boolean
          - dateTime
          - resourceDescriptor
          - fsObjectId
      validation:
        description: Validation checks to apply on the input
        type: array
        items:
          type: string
          enum:
            - "email"
            - "email-maysendfrom"
            - "email-maysendto"
            - "emails"
            - "emails-maysendto"
            - "url"
            - "url-plus-relative"
            - "http-url"
            - "secure-url"
            - "insecure-url"
            - "hostname"
            - "whfsname"
      minValue:
        description: Minimum value
        type: integer #TODO float?
      maxValue:
        description: Maximum value
        type: integer #TODO float?
      maxBytes:
        description: Maximum data length in (UTF-8) bytes
        type: integer
      maxLength:
        description: Maximum data length in UTF-16 'code units' (ie JavaScript String.length)
        type: integer
      required:
        description: Require a value to be set
        type: boolean
      precision:
        description: Date/time precision
        type: string
        enum:
          - hour
          - minute
          - second
          - millisecond

  Apply:
    type: object
    additionalProperties: false
    required:
      - to
    properties:
      to:
        $ref: "#/$defs/ApplyTo"
      baseProps:
        $ref: "#/$defs/ApplyBaseProps"
      editProps:
        $ref: "#/$defs/ApplyEditProps"
      userData:
        $ref: "#/$defs/ApplyUserData"

  ApplyTo:
    oneOf:
      - type: object
        additionalProperties: false
        required: ["and"]
        properties:
          and:
            type: array
            items:
              $ref: "#/$defs/ApplyTo"
      - type: object
        additionalProperties: false
        required: ["or"]
        properties:
          or:
            type: array
            items:
              $ref: "#/$defs/ApplyTo"
      - type: object
        additionalProperties: false
        required: ["not"]
        properties:
          not:
            $ref: "#/$defs/ApplyTo"
      - type: object
        additionalProperties: false
        properties:
          type: # should map to scopedType and be resolved
            type: string
          fileType: #'legacy' types - note that these in HS matching also support wildcards
            type: string
          folderType: #'legacy' types - note that these in HS matching also support wildcards
            type: string
          pathMatch:
            type: string
          parentPath:
            type: string
          parentType:
            type: string
          testSetting:
            $ref: "#/$defs/ApplyToTestSetting"
      - type: string
        enum:
          - "all"
          - "isFile"
          - "isFolder"
          - "isIndex"

  ApplyToTestSetting:
    type: object
    additionalProperties: false
    required: [target, type, member, value]
    properties:
      target:
        type: string
        enum:
          - "self"
      type:
        type: string
      member:
        type: string
      value: {}

  ApplyBaseProps:
    type: array
    items:
      type: string
      enum:
        - description
        - keywords
        - seotitle

  ApplyEditProps:
    type: array
    items:
      type: object
      additionalProperties: false
      required: [type]
      properties:
        type:
          type: string
        layout:
          oneOf:
            - type: string
              enum:
                - all
            - type: array
              items:
                type: string
        override:
          type: object
          additionalProperties:
            $ref: "#/$defs/ApplyEditMember"

  ApplyEditMember:
    type: object
    additionalProperties: false
    properties:
      component:
        $ref: "#/$defs/Component"
      line:
        $ref: "#/$defs/ComponentLine"
      lines:
        $ref: "#/$defs/ComponentLines"
      title:
        type: string
      constraints:
        $ref: "#/$defs/ValueConstraints"
      props:
        type: object
      layout:
        $ref: "#/$defs/FieldLayout"

  ApplyUserData:
    type: object
    additionalProperties:
      type: string # TODO A future version of YAML may open up this type (eg allow all JS primitves) but for HS compatibility we'll limit this to strings for now

  Component:
    type: object
    not: # forbid 'line' or 'lines' as a component
      required:
        - line
      properties:
        line:
          $ref: "#/$defs/ComponentLine"

  ComponentLine:
    type: array
    items:
      $ref: "#/$defs/Component" # TODO We should also permit line: but

  ComponentLines:
    type: array
    items:
      oneOf:
        - type: object
          required: [line]
          properties:
            line:
              $ref: "#/$defs/ComponentLine"
        - $ref: "#/$defs/Component"
