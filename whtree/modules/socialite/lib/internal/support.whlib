﻿<?wh
LOADLIB "mod::socialite/lib/database.whlib";
LOADLIB "mod::system/lib/internal/rightsmgmt.whlib";

PUBLIC OBJECTTYPE SocialiteException EXTEND Exception
<
  STRING code;

  /** @param code INVALIDARG: Invalid call (local contract violation)
                  TRANSPORT (I/O error or network said something odd)
                  EXPIRED (Session gone)
  */
  MACRO NEW(STRING code, STRING what)
  : Exception(what)
  {
    this->code := code;
  }
>;

// networks
PUBLIC RECORD ARRAY socialnetworks :=
  [ [ tag := "FACEBOOKV2"
    , title := "Facebook v2 (removed)"
    , setup := ["PUBKEY","PVTKEY","CALLBACKHOST"]
    ]
  , [ tag := "FACEBOOKV3"
    , title := "Facebook v3"
    , setup := ["PUBKEY","PVTKEY","CALLBACKHOST"]
    ]
  , [ tag := "LINKEDIN"
    , title := "LinkedIn V1 (closed may 2019)"
    , setup := ["PUBKEY","PVTKEY","CALLBACKHOST"]
    ]
  , [ tag := "LINKEDINV2"
    , title := "LinkedIn V2"
    , setup := ["PUBKEY","PVTKEY","CALLBACKHOST"]
    ]
  , [ tag := "TWITTER"
    , title := "Twitter (removed)"
    , setup := ["PUBKEY","PVTKEY","CALLBACKHOST"]
    ]
  , [ tag := "YAMMER"
    , title := "Yammer (removed)"
    , setup := ["PUBKEY","PVTKEY","CALLBACKHOST"]
    ]
  , [ tag := "FOURSQUARE"
    , title := "Foursquare (removed)"
    , setup := ["PUBKEY","PVTKEY","CALLBACKHOST"]
    ]
  , [ tag := "YOUTUBE"
    , title := "Youtube"
    , setup := ["PUBKEY","PVTKEY","CALLBACKHOST","DEVELOPERKEY"]
    ]
  , [ tag := "VIMEO"
    , title := "Vimeo"
    , setup := ["PUBKEY","PVTKEY","CALLBACKHOST"]
    ]
  , [ tag := "INSTAGRAM"
    , title := "Instagram"
    , setup := ["PUBKEY","PVTKEY","CALLBACKHOST"]
    ]
  , [ tag := "PINTEREST"
    , title := "Pinterest (removed)"
    , setup := ["PUBKEY","PVTKEY","CALLBACKHOST"]
    ]
  , [ tag := "GOOGLE"
    , title := "Google (removed)"
    , setup := ["PUBKEY","PVTKEY","CALLBACKHOST"]
    ]
  , [ tag := "MONEYBIRD"
    , title := "Moneybird (removed)"
    , setup := ["PUBKEY","PVTKEY","CALLBACKHOST"]
    ]
  ];

PUBLIC RECORD ARRAY globalpermissionmap :=
  [ [ id := 1, userapi := "GetOwnRecentStatuses", networks := [ "FACEBOOK", "TWITTER"] ]
  , [ id := 10, userapi := "PostMessageToFriend", networks := ["FACEBOOK" ] ]
  , [ id := 11, userapi := "PostMessageWithAttachmentToFriend", networks := ["FACEBOOK" ] ]
  , [ id := 12, userapi := "Events_Create", networks := ["FACEBOOK" ] ]
  , [ id := 13, userapi := "Events_RSVP", networks := ["FACEBOOK" ] ]
  , [ id := 14, userapi := "UpdateTwitterStatus", networks := ["TWITTER" ] ]
  //, [ id := 15, userapi := "FQL_User_Photos", networks := ["FACEBOOK" ] ]
  , [ id := 17, userapi := "GetFriendStatuses", networks := ["FACEBOOK" ] ]
  ];


PUBLIC STRING FUNCTION MYEncodeURLBin64(STRING data)
{
  data := EncodeBase64(data);
  data := Substitute(data,'+','-');
  data := Substitute(data,'/','_');
  data := Substitute(data,'=','');
  RETURN data;
}

PUBLIC STRING FUNCTION MYDecodeURLBin64(STRING data)
{
  data := Substitute(data,'-','+');
  data := Substitute(data,'_','/');
  WHILE( (Length(data) % 4) = 0 )
    data := data || "=";
  RETURN DecodeBase64(data);
}


//FIXME ->childrentext ?!
PUBLIC STRING FUNCTION GetNodeText(OBJECT query, STRING nodename, OBJECT root DEFAULTSTO DEFAULT OBJECT, BOOLEAN recursive DEFAULTSTO TRUE)
{
  STRING result;
  FOREVERY (OBJECT node FROM GetNodes(query, nodename, root, recursive))
    result := result || node->childrentext;
  RETURN result;
}


//fixme - get(child)elementsbytagname(ns) ?   die doet echter geen paths..
PUBLIC OBJECT ARRAY FUNCTION GetNodes(OBJECT query, STRING nodename, OBJECT root DEFAULTSTO DEFAULT OBJECT, BOOLEAN recursive DEFAULTSTO TRUE)
{
  STRING xpath := (ObjectExists(root) ?".":"") || (recursive ? "//" : "/") || nodename;
  OBJECT nodelist := query->ExecuteQuery(xpath, root);
  RETURN nodelist->GetCurrentElements();
}

/// Cast a VARIANT ARRAY to a RECORD ARRAY
PUBLIC RECORD ARRAY FUNCTION CastVAToRA(VARIANT xs)
{
  IF (TypeID(xs) = TypeID(RECORD ARRAY))
    RETURN xs;
  RECORD ARRAY res;
  FOREVERY (VARIANT x FROM xs)
    INSERT RECORD(x) INTO res AT END;
  RETURN res;
}

PUBLIC RECORD FUNCTION SplitSecureToken(STRING intoken)
{
  STRING encryptedhandle_plus_header := MyDecodeURLBin64(intoken);
  IF(encryptedhandle_plus_header="") //legacy support, REMOVE
    encryptedhandle_plus_header := DecodeBase64(intoken);

  RECORD packetheader := DecodePacket("version:C,appid:L", Left(encryptedhandle_plus_header,5));
  IF(NOT RecordExists(packetheader) OR packetheader.version!=1)
    RETURN DEFAULT RECORD;

  RETURN [ version := packetheader.version
         , appid := packetheader.appid
         , encryptedhandle := Substring(encryptedhandle_plus_header,5)
         ];
}

PUBLIC OBJECTTYPE AccountsDescriber EXTEND ObjectTypeDescriber
< PUBLIC UPDATE RECORD FUNCTION DescribeObject(INTEGER objectid)
  {
    RETURN
      SELECT id
           , name     := title
           , icon     := "tollium:folders/shared"
        FROM socialite.accounts
       WHERE id = objectid;
  }
>;
