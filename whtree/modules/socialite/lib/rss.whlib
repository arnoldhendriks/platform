<?wh
LOADLIB "wh::adhoccache.whlib";
LOADLIB "wh::crypto.whlib";
LOADLIB "wh::datetime.whlib";
LOADLIB "wh::files.whlib";
LOADLIB "wh::filetypes/rss.whlib";
LOADLIB "wh::internet/fetch.whlib";
LOADLIB "wh::xml/dom.whlib";
LOADLIB "wh::filetypes/html.whlib";
LOADLIB "mod::system/lib/logging.whlib";

RECORD FUNCTION GetRSSItems(STRING url)
{
  RECORD ARRAY items;
  OBJECT feed := FetchSync(url);
  IF(NOT feed->ok)
    THROW NEW Exception("Unable to retrieve " || url);

  //some rss feeds give wrong content-type like 'text/html' and in that case html/body are wrapped around rss in document object
  // so parse raw content to prefent content-type confusion
  RECORD result := ParseRSSFeedFromDocument(url, MakeXMLDocument(feed->AsBlob()));
  RETURN [ format := "rss"
         , entries := (SELECT created := date
                           , messageid := EncodeUFS(GetMD5Hash(GetDayCount(date) || ":" || GetMsecondCount(date) || ":" || title))
                           , data := [ richtext := description //FIXME Sanitize!
                                     , title := title
                                     , url := items.url
                                     ]
                        FROM result.items)
         ];
}

STRING FUNCTION GetSimpleTextFromHTML(OBJECT indoc)
{
  STRING output;
  FOR(OBJECT node := indoc->firstchild;ObjectExists(node);node := node->nextsibling)
  {
    IF(indoc->nodetype = 1 OR indoc->nodetype = 9)
      IF(ToUppercase(indoc->nodename)="BR")
        output := output || "\n";
      ELSE
        output := output || GetSimpleTextFromHTML(node);
    ELSE IF(indoc->nodetype=3)//text
      output := output || indoc->nodevalue;
  }
  RETURN output;
}

PUBLIC RECORD ARRAY FUNCTION DecodeFeedItems(STRING format, RECORD ARRAY initems)
{
  OBJECT rewriter := NEW HTMLRewriter;
  FOREVERY(RECORD row FROM initems)
  {
    OBJECT indoc := MakeXMLDocumentFromHTML(StringToBlob(row.data.richtext), "UTF-8");
    STRING rawtext := GetSimpleTextFromHTML(indoc);

    //FIXME cleanup the data
    rewriter->parsemode:="block";
    rewriter->ParseXmlDocument(indoc);

    INSERT CELL text := rawtext INTO row;
    INSERT CELL richtext := rewriter->parsed_text INTO row;
    INSERT CELL title := row.data.title INTO row;
    INSERT CELL url := row.data.url INTO row;

    initems[#row] := row;
  }
  RETURN initems;
}
RECORD FUNCTION GrabFeed(STRING query, INTEGER maxnum)
{
   DATETIME now := GetCurrentDatetime();
  now := GetCurrentDatetime();

  //Get a feed update.
  STRING error;
  RECORD baseinfo;
  RECORD feedresult;

  TRY
  {
    //FIXME ensure timeouts
    feedresult := GetRSSItems(query);

    UPDATE feedresult.entries
        SET messageid := EncodeUFS(GetSHA1Hash(messageid))
      WHERE Length(messageid) > 64; //ensure all message ids fit

    //ADDME: are there cases where we want to merge with existing entries, ie to get 'old forgotten' twitter stuff?

    IF(CellExists(feedresult,'baseinfo'))
      baseinfo := feedresult.baseinfo;
  }
  CATCH(OBJECT e)
  {
    LogError("socialite:feeds", "Error querying feed for '" || query || "': " || e->what, [ error := "FEEDQUERYERROR", query := query]);
    error := e->what; //FIXME Log it, mark the feed as failing
  }

  RECORD ARRAY existing_entries;

  INTEGER feedid;

  //Throw new entries into the database (but only if there were no errors getting the feed)
  STRING format;
  DATETIME lastupdate;


  // Decode the entries
  RECORD ARRAY entries;
  IF (RecordExists(feedresult))
    entries := SELECT * FROM DecodeFeedItems(format, feedresult.entries) ORDER BY created DESC LIMIT maxnum;

  RETURN [ ttl := 15 * 60 * 1000
          , value := [ entries :=    entries
                    , baseinfo :=   baseinfo
                    ]
          , eventmasks := [ "socialite:feedsupdate" ]
          ];
}

PUBLIC RECORD FUNCTION GetRSSFeed(STRING url, INTEGER maxnum)
{
  //The adhoc cache also prevents multiple requests for the same feed to be issued, causing database duplicates
  RETURN GetAdhocCached([ query := url, maxnum := maxnum ], PTR GrabFeed(url,maxnum));
}
