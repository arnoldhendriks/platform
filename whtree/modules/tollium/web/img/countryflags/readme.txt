Images in this namespace are automatically mapped to the flag data from https://www.npmjs.com/package/flag-icons. Specify
the two-letter country code as the image name.

Examples:

tollium:countryflags/nl      The Netherlands
tollium:countryflags/gb      Great Britain
tollium:countryflags/gb-eng  England
