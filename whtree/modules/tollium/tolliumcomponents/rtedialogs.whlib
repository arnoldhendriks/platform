<?wh

LOADLIB "wh::files.whlib";

LOADLIB "mod::tollium/lib/screenbase.whlib";
LOADLIB "mod::publisher/lib/components/internallinks.whlib";
LOADLIB "mod::publisher/lib/internal/rtd/support.whlib";
LOADLIB "mod::publisher/lib/publisher.whlib";
LOADLIB "mod::publisher/lib/internal/rtd/previewcontext.whlib";
LOADLIB "mod::publisher/lib/internal/siteprofiles/widgets.whlib";

RECORD FUNCTION GetHyperlinkResult(OBJECT rte, OBJECT selectbox, BOOLEAN newwindow)
{
  RECORD link := selectbox->value;
  IF(NOT RecordExists(link))
    RETURN DEFAULT RECORD;

  RETURN CELL[ link := link.internallink != 0 ? rte->CreateLinkRefHyperlink(link.internallink) || link.append
                                              : link.externallink
             , target := newwindow ? "_blank" : ""
             ];
}

PUBLIC STATIC OBJECTTYPE ImageProperties EXTEND TolliumScreenBase
<
  OBJECT rte;
  BOOLEAN suspendonchange;
  RECORD original;
  FLOAT origaspect;

  MACRO Init(RECORD data)
  {
    ^align->value := data.data.align;
    ^alttext->value := data.data.alttext;

    this->rte := data.rte;
    this->original := data.originalimg;

    IF(data.data.height > 0 AND data.data.width > 0) //if a height is set, we're overriding
    {
      ^overridedimensions->value := TRUE;
      ^height->value := data.data.height;
      ^width->value := data.data.width;
    }
    ELSE
    {
      ^height->value := data.originalimg.height;
      ^width->value := data.originalimg.width;
    }

    this->origaspect := data.originalimg.height > 0 ? FLOAT(data.originalimg.width) / FLOAT(data.originalimg.height) : 0f;
    SetupHyperlinkOptions(this, this->rte, data.data.link, ^link, ^openinnewwindow);
  }

  RECORD FUNCTION Submit()
  {
    IF(NOT this->BeginFeedback()->Finish())
      RETURN DEFAULT RECORD;
    ELSE
      RETURN CELL[ height := ^overridedimensions->value ? ^height->value : 0
                 , width := ^overridedimensions->value ? ^width->value : 0
                 , link := GetHyperlinkResult(this->rte, ^link, ^openinnewwindow->value)
                 , alttext := ^alttext->value
                 , align := ^align->value
                 ];
  }

  MACRO OnWidthChange()
  {
    IF(this->suspendonchange OR this->origaspect = 0)
      RETURN;

    this->suspendonchange := TRUE;
    ^height->value := INTEGER(^width->value / this->origaspect + 0.5);
    this->suspendonchange := FALSE;
  }
  MACRO OnHeightchange()
  {
    IF(this->suspendonchange OR this->origaspect = 0)
      RETURN;

    this->suspendonchange := TRUE;
    ^width->value := INTEGER(^height->value * this->origaspect + 0.5);
    this->suspendonchange := FALSE;
  }

  MACRO DoimgInline(RECORD clickinfo)
  {
    ^align->value := "";
  }
  MACRO DoimgLeft(RECORD clickinfo)
  {
    ^align->value := "left";
  }
  MACRO DoimgRight(RECORD clickinfo)
  {
    ^align->value := "right";
  }

  MACRO DoDownload(OBJECT imgreceiver)
  {
    RECORD tosend := this->original;
    //Restore the extension if needed so downloaders have a usable file
    IF(tosend.extension != "" AND ToUppercase(tosend.filename) NOT LIKE ToUppercase("*" || tosend.extension))
      tosend.filename := tosend.filename || tosend.extension;
    imgreceiver->SendWrappedFile(tosend);
  }
>;

MACRO SetupHyperlinkOptions(OBJECT screen, OBJECT rte, RECORD linkdata, OBJECT comp_linktype, OBJECT comp_openinnewwindow)
{
  OBJECT ARRAY handlers := rte->linkhandlers;
  comp_linktype->internallinks := RecordExists(SELECT FROM ToRecordArray(handlers,"handler") WHERE handler EXTENDSFROM InternalLinks);
  comp_linktype->internallinkroots := rte->internallinkroots;
  comp_linktype->linkhandlers := SELECT AS OBJECT ARRAY handler FROM ToRecordArray(handlers,"handler") WHERE handler NOT EXTENDSFROM InternalLinks;

  RECORD linkvalue;
  IF(RecordExists(linkdata))
  {
    IF(linkdata.link LIKE "x-richdoclink-e:*")
    {
      RECORD split := SplitIntLink(linkdata.link);
      INTEGER targetid := rte->GetLinkRefHyperlink(split.link);
      IF(targetid != 0)
        linkvalue := MakeIntExtInternalLink(targetid, split.append);
    }
    ELSE
    {
      linkvalue := MakeIntExtExternalLink(linkdata.link);
    }
  }
  comp_linktype->value := linkvalue;

  comp_openinnewwindow->visible := NOT RecordExists(rte->rtdtypeinfo) OR rte->rtdtypeinfo.allownewwindowlinks;
  comp_openinnewwindow->value := RecordExists(linkdata) AND linkdata.target = "_blank";
}

//ADDME call destroycomponents too..
PUBLIC STATIC OBJECTTYPE Hyperlink EXTEND TolliumScreenBase
<
  OBJECT rte;
  OBJECT extlink;
  BOOLEAN isremove;

  MACRO Init(RECORD data)
  {
    this->rte := data.rte;
    ^removehyperlinkbutton->visible := RecordExists(data.data);

    SetupHyperlinkOptions(this, this->rte, data.data, ^link, ^openinnewwindow);

    IF(NOT RecordExists(data.data)) //move focus to the external link field..
      ^link->FocusExternalLinkEntry();
  }
  BOOLEAN FUNCTION Submit()
  {
    RETURN this->BeginFeedback()->Finish();
  }

  PUBLIC RECORD FUNCTION GetData()
  {
    RETURN this->isremove ? [destroy:=TRUE] : GetHyperlinkResult(this->rte, ^link, ^openinnewwindow->value);
  }

  MACRO DoRemoveHyperlink()
  {
    this->isremove := TRUE;
    this->tolliumresult := "ok";
  }
>;

PUBLIC STATIC OBJECTTYPE TableProperties EXTEND TolliumScreenBase
< /// RTE object
  OBJECT rte;

  MACRO Init(RECORD data)
  {
    this->rte := data.rte;
    IF(NOT RecordExists(this->rte->rtdtypeinfo))
    {
      this->tolliumresult := "cancel";
      RETURN;
    }

    ^headers->value := STRING[ ...(data.data.datacell.row != 0 ? ["topheader"] : STRING[])
                             , ...(data.data.datacell.col != 0 ? ["leftheader"] : STRING[])
                             ];

    ^tablecaption->value := data.data.tablecaption;

    ^tablestyle->options := SELECT rowkey := ToLowercase(tag)
                                 , title := title ?? ToLowercase(tag)
                                 , tolliumselected := ToLowercase(tag) = data.data.tablestyletag
                             FROM this->rte->rtdtypeinfo.structure.blockstyles
                            WHERE type = "table"
                         ORDER BY ToUppercase(title);

    IF(data.data.type = "cell")
    {
      ^cellstyle->options := SELECT rowkey := tag
                                  , title := tag = "" ? this->GetTid(".normalcell") : (title ?? tag)
                                  , tolliumselected := tag = data.data.cellstyletag
                               FROM this->rte->rtdtypeinfo.structure.cellstyles
                           ORDER BY tag="" DESC
                                  , ToUppercase(title ?? tag);
    }
    ELSE
    {
      ^cellstyle->visible := FALSE;
      ^cellproperties->visible := FALSE;
    }
  }

  /** Returns action to be executed by RTE when tolliumresult = "ok"
  */
  PUBLIC RECORD FUNCTION GetValue()
  {
    RECORD val := CELL[ datacell :=     [ row := "topheader" IN ^headers->value ? 1 : 0
                                        , col := "leftheader" IN ^headers->value ? 1 : 0
                                        ]
                      , tablecaption := ^tablecaption->value
                      , tablestyletag := ^tablestyle->value
                      ];

    IF(^cellstyle->visible) //editing a cell
    {
      val := CELL[ ...val
                 , cellstyletag := ^cellstyle->value
                 ];
    }
    RETURN val;
  }

  MACRO DoRemoveTable()
  {
    IF (this->RunSimpleScreen("confirm", this->GetTid(".sureremovetable")) = "yes")
      this->tolliumresult := "remove";
  }
>;

PUBLIC STATIC OBJECTTYPE InspectWidget EXTEND TolliumScreenBase
<
  RECORD instance;
  OBJECT rtd;

  MACRO Init(RECORD data)
  {
    this->instance := data.instance;
    ^value->value := data.instance;
    this->rtd := data.rte;

    //FIXME run through async job so we can handle HS errors
    OBJECT context := NEW PreviewContext(DEFAULT OBJECT, this->contexts->applytester, this->rtd->rtdtypeinfo, FALSE);
    RECORD widgetinfo := GetEmbeddedObjectRenderer(DEFAULT OBJECT, this->contexts->applytester, this->rtd->rtdtypeinfo.namespace, data.instance, context);
    OBJECT renderer := widgetinfo.widget; //GetEmbeddedObjectRenderer never returns a default object
    ^searchpreview->value := `<html><body>${BlobToString(GetPrintedAsBlob(PTR renderer->RenderSearchPreview()))}</body><html>`;
  }
>;
