﻿<?wh

LOADLIB "wh::ipc.whlib";
LOADLIB "wh::internal/debug.whlib";

LOADLIB "mod::system/lib/logging.whlib";
LOADLIB "mod::system/lib/internal/backend/users.whlib";
LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/userrights.whlib";

LOADLIB "mod::tollium/lib/gettid.whlib";
LOADLIB "mod::tollium/lib/todd/internal/controller.whlib";
LOADLIB "mod::tollium/lib/users.whlib";
LOADLIB "mod::wrd/lib/auth.whlib";


OBJECT parentlink;
BOOLEAN sent_initial_message;

// Callback for initial message (will be sent with response of app start service)
MACRO SendInitialMessage(RECORD data)
{
  parentlink->SendMessage([ type := "appstart", data := data ]);
  sent_initial_message := TRUE;
}

MACRO GotUserActivity()
{
  IF (sent_initial_message)
    parentlink->SendMessage([ type := "useractivity" ]);
}

MACRO GotSessionExpiresChange(DATETIME sessionexpires)
{
  parentlink->SendMessage(CELL[ type := "sessionexpireschange", sessionexpires ]);
}

MACRO GotSessionExpired()
{
  parentlink->DoRequest(CELL[ type := "sessionexpired" ]);
}

parentlink := GetIPCLinkToParent();
IF(NOT ObjectExists(parentlink))
  ABORT("The application host cannot be directly invoked, it must run as a job");

OpenPrimary();
// Receive configuration
RECORD firstmsg := parentlink->ReceiveMessage(DEFAULT DATETIME);

/* Startup message
   @cell(string) frontendid
   @cell(record) appparams
   @cell(string) app
   @cell parameters
   @cell target
*/
RECORD appinfo := firstmsg.msg;
__webdebugsettings := appinfo.webdebugsettings;

OBJECT wrdauthplugin, userapi;

DATETIME sessionexpires := appinfo.launchuserinfo.loggedin_expires;
TRY
{
  OBJECT currentuser;
  IF(appinfo.launchuserinfo.forceprimaryuserapi)
  {
    wrdauthplugin := GetPrimaryWebhareAuthPlugin();
  }
  ELSE
  {
    wrdauthplugin := GetWRDAuthPlugin(appinfo.url, [ clientwebserver := appinfo.requestdata.webserver ]);
  }

  IF(wrdauthplugin->HasFailed())
  {
    IF(appinfo.launchuserinfo.loggedin != -1)
      ABORT("WRDAuthPlugin is broken: " || wrdauthplugin->GetFailReason());
    currentuser := GetMagicSysopObject("<overrideuser>");
  }
  ELSE
  {
    userapi := wrdauthplugin->userapi;

    IF(RecordExists(appinfo.launchuserinfo))
    {
      RECORD res := userapi->GetTolliumUserByIdAndImpersonation(appinfo.launchuserinfo.loggedin, appinfo.launchuserinfo.openas);
      currentuser := ObjectExists(res.userobject) ? res.userobject : userapi->GetAnonymousUser();
      sessionexpires := appinfo.launchuserinfo.loggedin_expires;
      IF (ObjectExists(res.impersonatinguser))
      {
        UpdateAuditContext(
            [ impersonator_entityid := res.impersonatinguser->entityid
            , impersonator_login :=    res.impersonatinguser->login
            ]);
      }
    }
    ELSE
    {
      currentuser := userapi->GetAnonymousUser();
    }
  }

  __SetEffectiveUser(currentuser);

  UpdateAuditContext(CELL
      [ appinfo.requestdata.remoteip
      , appinfo.requestdata.browsertriplet
      ]);

  LogAuditEvent(
            "tollium:applicationstart",
            [ user :=         currentuser->GetUserEventIdentifier()
            , app :=          appinfo.app
            , appparams :=    [ calltype := "direct"
                              , params :=   appinfo.parameters
                              , target :=   appinfo.target
                              , message :=  appinfo.message
                              ]
            ]);
}
CATCH (OBJECT e)
{
  LogHarescriptException(e);
  IF(appinfo.launchuserinfo.loggedin = -1)
    __SetEffectiveUser(GetMagicSysopObject("<overrideuser>"));
  ELSE //We don't really need the plugin, so let it go..
    __SetEffectiveUser(NEW TolliumUser(DEFAULT OBJECT, DEFAULT OBJECT, 0, "<anonymous>"));
}

OBJECT webcontroller := NEW TolliumWebController(appinfo, userapi, wrdauthplugin);
SetTidLanguage(appinfo.lang);

//IF(NOT launcher->SetupForUser(appinfo.app))
 // ABORT("No longer authorized"); //FIXME what to do, really?

webcontroller->pvt_sendinitialmessage := PTR SendInitialMessage;
webcontroller->onuseractivity := PTR GotUserActivity;
webcontroller->onsessionexpireschange := PTR GotSessionExpiresChange;
webcontroller->onsessionexpired := PTR GotSessionExpired;
webcontroller->ActivateWebController();
webcontroller->pvt_inapppath := appinfo.inapppath;

webcontroller->LaunchApplication(appinfo.app, [ calltype := "direct"
                                              , params := appinfo.parameters
                                              , target := appinfo.target
                                              , message := appinfo.message
                                              , webvars := appinfo.webvars
                                              , goparam := appinfo.goparam
                                              ]);
