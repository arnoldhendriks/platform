<?wh

LOADLIB "wh::internet/smtp.whlib";
LOADLIB "wh::internet/mime.whlib";
LOADLIB "wh::internet/urls.whlib";
LOADLIB "mod::system/lib/internal/whcore_interface.whlib";
LOADLIB "mod::system/lib/internal/mail/routemgr.whlib";
LOADLIB "mod::tollium/lib/gettid.whlib";

/* This library contains the validator for input fields
*/

BOOLEAN FUNCTION CheckURL(STRING input)
{
  RECORD url := UnpackURL(input);
  IF(url.scheme="" OR url.schemespecificpart ="" OR (url.scheme IN ["http","https","ftp"] AND url.schemespecificpart NOT LIKE "//*"))
    RETURN FALSE;
  IF(url.schemespecificpart LIKE "/*" AND url.schemespecificpart NOT LIKE "//*" AND url.scheme != "file")
    RETURN FALSE;
  RETURN TRUE;
}

PUBLIC CONSTANT STRING ARRAY url_validationchecks := [ "url", "url-plus-relative", "http-url", "secure-url", "insecure-url"];

/** Generalize a shared link - remove redirect layers and credentials

    this list necessarily relies on user reporting, there's no "official" list.

    this canonicalization should be done in the WebHare UI where it makes sense, but not so deep in the code that we can never implement an 'off' switch in the future for that specific UI spot */
PUBLIC STRING FUNCTION GeneralizeSharedLink(STRING inlink)
{
  IF(Length(inlink) > 2048)
    RETURN inlink; //ignore unreasonably long links

  //TODO on disk YML list for easier maintenace

  RECORD unp := UnpackURL(inlink);
  IF(unp.origin LIKE "*.safelinks.protection.outlook.com") //Office safe links
  {
    STRING url := GetVariableFromURL(inlink,"url");
    IF(urL != "")
      RETURN url;
  }

  IF(unp.origin = "https://www.google.com" AND Left(unp.urlpath,4) = "url?") //Google calendar redirects
  {
    STRING url := GetVariableFromURL(inlink,"q");
    IF(url != "")
      RETURN url;
  }

  IF(unp.origin = "https://drive.google.com" AND Left(unp.urlpath,8) = "drive/u/") //Google drive links copied from the URL when logged in to multiple acounts
  {
    //TODO regex would be more robust and not potentially end up with hundreds of tokens
    STRING ARRAY toks := Tokenize(unp.urlpath,'/');
    IF(ToInteger(toks[2],-1) >= 0) //it's indeed a user number
      RETURN unp.origin || "/drive/" || Detokenize(ArraySlice(toks,3), "/");
  }

  RETURN inlink;
}


/** Run an input value through a range of checks
    @param input Input value to check
    @param fieldtitle Title of field that received the input value (used for feedback)
    @param checks List of checks to perform.
       Currently allowed checks:
          'email'         Accepts a valid e-mail address
          'emails'        Accepts a list of valid e-mail addresses (including
          'http-url'      Accepts http:// and https:// urls
          'secure-url'    Accepts only secure urls (currently https and imaps)
          'insecure-url'  Accepts only insecure urls
    @return Validation error in current gettid language, empty string when every checks passed.
*/
PUBLIC STRING FUNCTION ValidateInput(STRING input, STRING fieldtitle, STRING ARRAY checks)
{
  FOREVERY (STRING check FROM checks)
  {
    BOOLEAN isnameemail;
    IF(check LIKE "name-email*")
    {
      isnameemail := TRUE;
      check := Substring(check,5);
    }

    SWITCH (check)
    {
      CASE "email","email-maysendfrom","email-maysendto"
      {
        IF (isnameemail ? NOT IsValidEmailAddress(input) : NOT IsValidModernEmailAddress(input))
          RETURN GetTid("tollium:validation.errors.invalid_email", fieldtitle);

        IF(check = "email-maysendfrom")
        {
          STRING mail := SplitEmailName(input).email;
          IF(IsAllowedEmailSender(mail) = 0)
            RETURN GetTid("tollium:validation.errors.sender_not_whitelisted", fieldtitle, mail);
        }

        IF(check = "email-maysendto")
        {
          STRING mail := SplitEmailName(input).email;
          IF(NOT IsAllowedEmailReceiver(mail))
            RETURN GetTid("tollium:validation.errors.receiver_not_whitelisted", fieldtitle, mail);
        }
      }

      CASE "emails","emails-maysendto"
      {
        IF(isnameemail)
        {
          RECORD ARRAY data := TokenizeEmailAddressList(input, FALSE);
          FOREVERY (RECORD rec FROM data)
          {
            IF(NOT IsValidEmailAddress(rec.original))
              RETURN GetTid("tollium:validation.errors.invalid_emails", fieldtitle, rec.original);
            IF(check = "emails-maysendto" AND NOT IsAllowedEmailReceiver(rec.email))
              RETURN GetTid("tollium:validation.errors.receiver_not_whitelisted", fieldtitle, rec.email);
          }
        }
        ELSE
        {
          STRING ARRAY data := Tokenize(Substitute(input, ";", ","), ",");
          FOREVERY (STRING rec FROM data)
          {
            rec := TrimWhitespace(rec);

            IF(NOT IsValidModernEmailAddress(rec))
              RETURN GetTid("tollium:validation.errors.invalid_emails", fieldtitle, rec);
            IF(check = "emails-maysendto" AND NOT IsAllowedEmailReceiver(rec))
              RETURN GetTid("tollium:validation.errors.receiver_not_whitelisted", fieldtitle, rec);
          }
        }
      }

      CASE "url"
      {
        RECORD url := UnpackURL(input);
        IF(NOT CheckUrl(input))
          RETURN GetTid("tollium:validation.errors.invalid_url", fieldtitle);
      }

      CASE "url-plus-relative"
      {
        IF(input LIKE "#*" OR input LIKE "./*" OR input LIKE "../*" OR (input LIKE "/*" AND input NOT LIKE "//*"))
          RETURN "";

        RECORD url := UnpackURL(input);
        IF(NOT CheckUrl(input))
          RETURN GetTid("tollium:validation.errors.invalid_url", fieldtitle);
      }

      CASE "http-url"
      {
        IF (NOT IsValidPlainHTTPURL(input))
          RETURN GetTid("tollium:validation.errors.invalid_http_url", fieldtitle);
      }

      CASE "secure-url"
      {
        RECORD url := UnpackURL(input);
        IF(url.schemespecificpart="" OR NOT url.secure)
          RETURN GetTid("tollium:validation.errors.invalid_secure_url", fieldtitle);
      }

      CASE "insecure-url"
      {
        RECORD url := UnpackURL(input);
        IF(url.schemespecificpart="" OR url.secure)
          RETURN GetTid("tollium:validation.errors.invalid_insecure_url", fieldtitle);
      }

      CASE "hostname"
      {
        IF (NOT IsValidHostName(input))
          RETURN GetTid("tollium:validation.errors.invalid_hostname", fieldtitle);
      }

      CASE "whfsname"
      {
        IF(Length(input) >= 256 OR NOT IsValidWHFSName(input, FALSE))
          RETURN GetTid("tollium:validation.errors.invalid_whfsname", fieldtitle);
      }

      DEFAULT
      {
        THROW NEW Exception("Unrecognized validation check '" || check || "'");
      }
    }

    IF (check LIKE "*url*" AND LENGTH(input) > 2000)
      RETURN GetTid("tollium:validation.errors.too_long_url", fieldtitle);
  }
  RETURN "";
}
