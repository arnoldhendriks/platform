<?wh
// Tollium's embdded doc system

LOADLIB "wh::files.whlib";
LOADLIB "wh::internet/urls.whlib";
LOADLIB "wh::internet/webbrowser.whlib";

LOADLIB "mod::tollium/lib/gettid.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/logging.whlib";
LOADLIB "mod::system/lib/resources.whlib";
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";

RECORD checkedmodules;

CONSTANT INTEGER docinfoversion := 1; //may need to change this if ondisk format changes

CONSTANT RECORD docinfostructure :=
  [ languages :=
    [[ code := ""
     , topics := [[ topic := "", link := "" ]]
    ]]
  , editfallback := ""
  ];

RECORD FUNCTION GetModuleDefEmbedNode(STRING modulename)
{
  OBJECT node := GetModuleDefinitionXML(modulename);
  IF(ObjectExists(node))
    node := PickFirst(node->documentelement->ListChildren(whconstant_xmlns_moduledef, "documentation"));
  IF(ObjectExists(node))
    node := PickFirst(node->ListChildren(whconstant_xmlns_moduledef, "embedded"));
  IF(NOT ObjectExists(node))
    RETURN DEFAULT RECORD;

  STRING rooturlkey := node->GetAttribute("rooturlkey");
  STRING rooturl;
  IF(rooturlkey != "")
    rooturl := ReadRegistryKey(rooturlkey);
  IF(rooturl = "")
    rooturl := node->GetAttribute("rooturl");
  IF(rooturl = "")
    RETURN DEFAULT RECORD;

  RETURN CELL[ rooturl
             , subpath := node->GetAttribute("subpath")
             ];
}

RECORD FUNCTION DownloadDocInfo(RECORD embedinfo)
{
  OBJECT browser;
  TRY
  {
    browser := NEW Webbrowser;
    //ensure url ends with a '/', even after appending a subpath
    STRING rooturl := embedinfo.rooturl;
    IF(rooturl NOT LIKE "*/")
      rooturl := rooturl || "/";
    IF(embedinfo.subpath != "")
      rooturl := rooturl || (embedinfo.subpath LIKE "/*" ? Substring(embedinfo.subpath,1) : embedinfo.subpath);
    IF(rooturl NOT LIKE "*/")
      rooturl := rooturl || "/";

    rooturl := rooturl || "whdocs-v1.json";

    IF(NOT browser->GotoWebPage(rooturl))
    {
      PrintTo(2, `Error '${browser->GetHTTPStatusCode()}' downloading documentation from ${rooturl}\n`);
      RETURN DEFAULT RECORD;
    }

    RECORD docinfo := DecodeJSONBlob(browser->content);
    IF(NOT RecordExists(docinfo))
    {
      PrintTo(2, `Unable to parse JSON document at ${rooturl}\n`);
      RETURN DEFAULT RECORD;
    }

    docinfo := CELL[ ...EnforceStructure(docinfostructure, docinfo)
                   , base := rooturl
                   , version := docinfoversion
                   ];
    RETURN docinfo;
  }
  FINALLY
  {
    browser->Close();
  }
}

RECORD FUNCTION GetModuleDocInfo(STRING modulename)
{
  //We currently only lookup by module name, but the XML syntax (an <embedded> nodes inside <documentation>) wouldn't preclude muiltiple embedded nodes mathing the doclink by prefix
  IF(CellExists(checkedmodules, modulename))
    RETURN Getcell(checkedmodules, modulename);

  RECORD moduleinfo;
  BLOB cachedmoduleinfo := GetDiskResource(MergePath(GetModuleStorageRoot("tollium"), `embeddeddocs/${modulename}.whdocs.hson`), [ allowmissing := TRUE ]);
  IF(Length(cachedmoduleinfo) != 0)
  {
    RECORD rawdata;

    //Try to deecode. Log but ignore if corrupt
    TRY
    {
      rawdata := DecodeHSONBlob(cachedmoduleinfo);
      IF(rawdata.version != docinfoversion)
        THROW NEW Exception(`Invalid docinfo version #${rawdata.version} for module '${modulename}'`);

      moduleinfo := rawdata;
    }
    CATCH(OBJECT e)
    {
      LogHarescriptException(e);
    }
  }
  checkedmodules := CellInsert(checkedmodules, modulename, moduleinfo);

  RETURN moduleinfo;
}

PUBLIC RECORD FUNCTION GetRemoteEmbeddedDocumentation(OBJECT contexts, STRING doclink)
{
  STRING modulename := Left(doclink, SearchSubstring(doclink, ':'));
  STRING sublink := Substring(doclink, Length(modulename) + 1);
  RECORD modinfo := GetModuleDocInfo(modulename);

  IF(NOT RecordExists(modinfo))
    RETURN DEFAULT RECORD;

  RECORD mylang := SELECT * FROM modinfo.languages WHERE code = GetTidLanguage();
  IF(RecordExists(mylang))
  {
    RECORD mytopic := SELECT * FROM mylang.topics WHERE topic = sublink;
    IF(RecordExists(mytopic) AND mytopic.link != "")
    {
      RETURN [ editinfo := DEFAULT RECORD
             , url := ResolveToAbsoluteURL(modinfo.base, mytopic.link)
             , missing := FALSE
             ];
    }
  }

  IF(modinfo.editfallback != "" AND contexts->user->ShowDeveloperOptions()) //there may be some edit info there...
  {
    RETURN [ editinfo := DEFAULT RECORD
           , url := UpdateURLVariables(ResolveToAbsoluteURL(modinfo.base, modinfo.editfallback)
                                      , CELL[ topic := sublink
                                            , lang := GetTidLanguage()
                                            , module := modulename
                                            , referrer := contexts->controller->baseurl
                                            ])
           , missing := TRUE
           ];
  }

  RETURN DEFAULT RECORD;
}

PUBLIC BOOLEAN FUNCTION RefreshEmbeddedDocumentation(STRING modulemask)
{
  //TODO for easier cleanup of obsolete module data we should join any future centrale 'module cache' folder in storage/ - but no such folder is there yet.
  STRING outdir := MergePath(GetModuleStorageRoot("tollium"), "embeddeddocs");
  CreateDiskDirectoryRecursive(outdir, TRUE);
  BOOLEAN anyerrors;

  FOREVERY(STRING mod FROM GetInstalledModuleNames())
  {
    IF(ToUppercase(mod) LIKE ToUppercase(modulemask))
    {
      STRING outpath := MergePath(outdir, mod) || ".whdocs.hson";
      RECORD embedinfo := GetModuleDefEmbedNode(mod), moduleinfo;
      IF(NOT RecordExists(embedinfo))
      {
        DeleteDiskFile(outpath); //delete any module file if present, as apparently its docs are gone
        CONTINUE;
      }

      moduleinfo := DownloadDocInfo(embedinfo);
      IF(NOT RecordExists(moduleinfo))
      {
        anyerrors := TRUE;
        CONTINUE;
      }

      StoreDiskFile(outpath, EncodeHSONBlob(moduleinfo), [ overwrite := TRUE ]);
    }
  }
  RETURN anyerrors;
}
