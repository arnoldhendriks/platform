<?wh
LOADLIB "mod::system/lib/resources.whlib";

LOADLIB "mod::system/lib/internal/validation/support.whlib";
LOADLIB "mod::tollium/lib/internal/controllerbase.whlib";
LOADLIB "mod::tollium/lib/internal/screenparser.whlib";
LOADLIB "mod::tollium/lib/internal/support.whlib";

PUBLIC STATIC OBJECTTYPE ScreensFileValidator EXTEND XMLValidatorBase
<
  UPDATE PUBLIC MACRO ValidateDocument()
  {
    // try to load each <screen> in the screens document to catch all errors
    // which can't be picked up by validating using the XSD.

    // We use the document to know which screens are availabe,
    // but make the controller/resourcemanager load the screen.
    FOREVERY(OBJECT topnode FROM this->xml->documentelement->childnodes->GetCurrentElements())
    {
      IF(topnode->localname IN ["screen","fragment","propertyeditor","tabsextension"]) //FIXME validate namespaces. note that propertyeditor is a hack and is from a different namespace!
      {
        RECORD savescreenbuildinfo := __screenbuildinfo;
        TRY
        {
          RECORD parsedelement := ParseFoundXMLPage( this->respath
                                                   , topnode->localname = "propertyeditor" ? "tabsextension" : topnode->localname
                                                   , topnode
                                                   , this->xml->documentelement->GetAttribute("gid")
                                                   , TRUE
                                                   );
          FOREVERY(RECORD iconref FROM parsedelement.iconreferences)
            this->ValidateIcon(iconref.scope, iconref.icon, "b,w");

          this->warnings := this->warnings CONCAT parsedelement.warnings;
          this->hints := this->hints CONCAT parsedelement.hints;

          INTEGER elline := parsedelement.rootnode.tolliumscope.line;
          IF(NOT RecordExists(this->VerifyObjectExistence(elline, parsedelement.objtype)))
            CONTINUE;

          //any errors would throw a RetrieveResourceException

          //FIXME can all objects be processed by screenbuilder?
          //FIXME simplify and share core with pvt_loadscreen
          IF(topnode->localname = "screen" AND NOT this->onlytids) //only screen is complete enough for a sensible internal consistency valiadtion
          {
            __screenbuildinfo := [ options := [ contexts := DEFAULT RECORD ]
                                 , parent := DEFAULT OBJECT
                                 , controller := DEFAULT OBJECT
                                 , privateptr := DEFAULT OBJECT
                                 , initdata := DEFAULT RECORD
                                 , contextparent := DEFAULT OBJECT
                                 ];
            OBJECT builder := NEW __ScreenBuilder(DEFAULT OBJECT, parsedelement, this);
            OBJECT screen := __screenbuildinfo.privateptr;
            RECORD ARRAY allcomponents := screen->DoLoadComponents(parsedelement.formobjects, builder);
            allcomponents := screen->tolliumscreenmanager->ScopeFormObjects(allcomponents);

            FOREVERY(RECORD objinfo FROM allcomponents)
            {
              TRY
              {
                objinfo.data := builder->ReprocessData(DEFAULT OBJECT, objinfo.data, objinfo.data.__xml_fields, objinfo.data.tolliumscope);
              }
              CATCH(OBJECT e)
              {
                builder->AddError(objinfo.data.tolliumscope, e->what);
              }
            }
            this->errors := this->errors CONCAT builder->GetErrors();
          }
        }
        CATCH(OBJECT< RetrieveResourceException > e)
        {
          IF(Length(e->xmlerrors) > 0)
            this->errors := this->errors CONCAT e->xmlerrors;
          ELSE
            this->errors := this->errors CONCAT [[ line         := 0
                                 , col          := 0
                                 , message      := e->mainerror
                                 , resourcename := e->resourcename
                                ]];
        }
        CATCH(OBJECT< HarescriptErrorException > e)
        {
          this->errors := this->errors CONCAT SELECT resourcename := this->respath, line, col, message FROM e->errors;
        }
        CATCH(OBJECT e)
        {
          this->errors := this->errors CONCAT [[ line    := 0
                               , col     := 0
                               , message := e->what
                               , resourcename := this->respath
                              ]];
        }
        FINALLY
        {
          __screenbuildinfo := savescreenbuildinfo;
        }
      }
    }
  }
>;
