<?wh

LOADLIB "wh::crypto.whlib";
LOADLIB "wh::files.whlib";
LOADLIB "wh::promise.whlib";
LOADLIB "wh::filetypes/html.whlib";
LOADLIB "wh::internet/mime.whlib";
LOADLIB "wh::xml/dom.whlib";

LOADLIB "mod::publisher/lib/publisher.whlib";

LOADLIB "mod::system/lib/configure.whlib";

LOADLIB "mod::tollium/lib/componentbase.whlib";
LOADLIB "mod::tollium/lib/internal/components.whlib";

BOOLEAN did_calljs_warning;

PUBLIC OBJECTTYPE TolliumIframe EXTEND TolliumComponentBase
< // ---------------------------------------------------------------------------
  //
  // Variables
  //

  /// Current selected urlfragment
  STRING urlfragment;

  /// Link to main page content in downloadkeeper
  STRING mainpagelink;

  /// Source url
  STRING pvt_src;

  /// List of additional components
  OBJECT ARRAY pvt_additionalcomponents;

  /// Function ptr for clicklink callback
  FUNCTION PTR pvt_onclicklink;

  /// Function ptr for js callback
  FUNCTION PTR pvt_oncallback;

  /// Function ptr for data changed callback
  FUNCTION PTR pvt_ondata;

  /// Submit value
  RECORD pvt_data;

  /// PostMessage receiver
  PUBLIC MACRO PTR onmessage;

  /// host.post receiver
  PUBLIC MACRO PTR onclientmessage;

  /// Pending requests
  RECORD ARRAY pendingrequests;

  /// Viewport width/height. if empty, full screen iframe
  RECORD _viewport;

  /// <iframe> sandbox settings
  STRING _sandbox;

  /// HTML value set
  BLOB _setvalue;

  /// Embedded data for SetEmail
  RECORD ARRAY embedded;

  // ---------------------------------------------------------------------------
  //
  // Public variables & properties
  //

  /** Set the current HTML contents of the iframe (string).
  */
  PUBLIC PROPERTY value(GetHTML, SetHTML);

  /** Set the current HTML contents of the iframe (blob).
  */
  PUBLIC PROPERTY blobvalue(_setvalue, SetHTMLBlob);

  /** The source URL for the content
  */
  PUBLIC PROPERTY src(pvt_src, SetSource);

  /// Triggered when a link within the iframe is clicked. Signature: MACRO func(STRING href)
  PUBLIC PROPERTY onclicklink(pvt_onclicklink, SetOnClickLink);

  /// Trigged when javascripts calls DoCallback. Signature: MACRO func(RECORD data)
  PUBLIC PROPERTY oncallback(pvt_oncallback, SetOnCallback);

  /// Trigged when the data changed. Signature: MACRO func()
  PUBLIC PROPERTY ondata(pvt_ondata, SetOnData);

  /// Called when enabledon is executed. Signature: BOOLEAN func(INTEGER min, INTEGER max, STRING ARRAY flags, STRING selectionmatch)
  PUBLIC FUNCTION PTR onenabledon;

  /// List of additional components
  PUBLIC PROPERTY additionalcomponents(GetAdditionalComponents, SetAdditionalComponents);

  /// Submit value
  PUBLIC PROPERTY data(pvt_data, SetData);

  /// Todd iframe JavaScript url
  PUBLIC PROPERTY toddiframeurl(GetToddIframeUrl, -);

  PUBLIC PROPERTY sandbox(_sandbox, SetSandbox);

  // ---------------------------------------------------------------------------
  //
  // Constructor & tollium init
  //

  MACRO NEW()
  {
    this->pvt_blockelement := TRUE;
    EXTEND this BY TolliumDownloadKeeper;
    this->componenttype := "iframe";
    this->formfieldtype := "record";
    this->sandbox := "none";
  }

  UPDATE PUBLIC MACRO StaticInit(RECORD def)
  {
    TolliumComponentBase::StaticInit(def);
    this->pvt_onclicklink := def.onclicklink;
    this->pvt_oncallback := def.oncallback;
    this->pvt_ondata := def.ondata;
    this->onmessage := def.onmessage;
    this->onclientmessage := def.onclientmessage;
    this->sandbox := def.sandbox;
  }

  UPDATE RECORD FUNCTION GetExtraDirtyFlags()
  {
    RETURN
        [ content := FALSE
        , eventmask := FALSE
        , addcomps := FALSE
        , data := FALSE
        , viewport := FALSE
        , sandbox := FALSE
        ];
  }

  PUBLIC STRING FUNCTION __GetContentURL()
  {
    RETURN this->pvt_src ?? this->mainpagelink;
  }

  UPDATE PUBLIC MACRO TolliumWebRender()
  {
    /* FIXME: Security issues with tolliumwebrender: embedded javascript or encoding errors can compromise
              todd's framework, allowing attacks on other todd application with higher privileges */

    STRING uri := this->__GetContentURL();

    IF (this->dirtyflags.fully)
    {
      RECORD comp := [ unmasked_events := GetUnmask(this,["clicklink","callback","data"])
                     , fragment :=  this->urlfragment
                     , mainuri :=   uri
                     , addcomps :=  GetComponentNames(this->pvt_additionalcomponents)
                     , data :=      this->pvt_data
                     , sandbox :=   this->_sandbox
                     , viewport := this->_viewport
                     ];
      this->owner->tolliumcontroller->SendComponent(this, comp);
    }
    ELSE
    {
      IF (this->dirtyflags.sandbox)
        this->ToddUpdate([ type:= "sandbox", sandbox := this->_sandbox ]);

      IF(this->dirtyflags.content)
        this->ToddUpdate(
            [ type:="content"
            , mainuri := uri
            , fragment := this->urlfragment
            ]);

      IF (this->dirtyflags.eventmask)
        this->ToddUpdate([ type:="eventmask", unmasked_events := GetUnmask(this,["clicklink", "callback", "data"]) ]);

      IF (this->dirtyflags.viewport)
        this->ToddUpdate([ type:="viewport", viewport := this->_viewport]);

      IF (this->dirtyflags.addcomps)
        this->ToddUpdate([ type:="addcomps", addcomps := GetComponentNames(this->pvt_additionalcomponents) ]);

      IF (this->dirtyflags.data)
        this->ToddUpdate([ type:= "data", data := this->pvt_data ]);
    }

    TolliumComponentBase::TolliumWebRender();
  }

  PUBLIC MACRO TolliumWeb_FormUpdate(RECORD data)
  {
    this->pvt_data := data;
  }

  // ---------------------------------------------------------------------------
  //
  // Getters & setters
  //

  UPDATE PUBLIC STRING FUNCTION GetDefaultWidth()
  {
    RETURN "1pr";
  }
  UPDATE PUBLIC STRING FUNCTION GetDefaultHeight()
  {
    RETURN "1pr";
  }

  MACRO SetSandbox(STRING newsandbox)
  {
    IF(this->_sandbox = newsandbox)
      RETURN;

    this->_sandbox := newsandbox;
    this->ExtUpdatedSandbox();
  }

  STRING FUNCTION GetHTML()
  {
    RETURN BlobToString(this->_setvalue);
  }

  MACRO SetHTML(STRING data)
  {
    this->SetHTMLBlob(StringToBlob(data));
  }

  MACRO SetHTMLBlob(BLOB data)
  {
    this->pvt_src := "";
    this->_setvalue := data;
    this->mainpagelink := this->AddInlineDownload(data, "text/html; charset=utf-8", "download.html");
    this->ExtUpdatedContent();
  }

  STRING FUNCTION RewriteEmbeddedLink(STRING inlink)
  {
    IF(inlink NOT LIKE "cid:*")
      RETURN inlink;

    STRING lookfor :=  '<' || Substring(inlink,4) || '>';
    RECORD match := SELECT * FROM this->embedded WHERE contentid = lookfor;
    IF(RecordExists(match) AND match.link = "")
    {
      match.link := this->AddInlineDownload(match.data, GetMIMEHeaderParameter(match.mimetype,""), GetMIMEHeaderParameter(match.mimetype,"name"));
      UPDATE this->embedded SET link := match.link WHERE contentid = lookfor;
    }
    RETURN RecordExists(match) ? match.link : inlink;
  }

  /** Fill the iframe's contents with an email
      @param mailtoppart Top MIME part of the email
      @cell(blob) return.source text/html or text/plain source */
  PUBLIC RECORD FUNCTION SetEmail(RECORD mailtoppart)
  {
    this->ResetContent();

    RECORD relatedpart := GetEmailPrimaryMIMEPart(mailtoppart, "multipart/related");
    RECORD htmlpart := GetEmailPrimaryMIMEPart(mailtoppart, "text/html");
    IF(NOT RecordExists(htmlpart))
    { //Then try as text
      RECORD textpart := GetEmailPrimaryMIMEPart(mailtoppart, "text/plain");
      IF (RecordExists(textpart))
      {
        this->SetHTML(EncodeHTML(BlobToString(textpart.data)));
        RETURN CELL[ source := textpart.data ];
      }
      RETURN DEFAULT RECORD;
    }

    IF (RecordExists(relatedpart))
      this->embedded := SELECT *, link := "" FROM relatedpart.subparts;

    OBJECT doc  := MakeXMLDocumentFromHTML(htmlpart.data);
    FOREVERY(OBJECT link FROM doc->GetElements("*[href]"))
      link->SetAttribute("target", "_blank");

    NEW HTMLRewriterContext->RewriteEmbeddedLinks(doc, PTR this->RewriteEmbeddedLink);
    this->SetHTML(doc->innerhtml);

    RETURN CELL[ source := htmlpart.data ];
  }

  PUBLIC MACRO SetFile(RECORD wrappedfile)
  {
    IF(NOT RecordExists(wrappedfile))
    {
      this->SetSource("about:blank");
      RETURN;
    }
    this->pvt_src := "";
    this->_setvalue := wrappedfile.data;
    this->mainpagelink := this->AddInlineDownload(wrappedfile.data, wrappedfile.mimetype, wrappedfile.filename);
    this->ExtUpdatedContent();
  }

  MACRO SetSource(STRING href)
  {
    IF(this->pvt_src = href)
      RETURN;

    this->pvt_src := href;
    this->_setvalue := DEFAULT BLOB;
    this->ExtUpdatedContent();
  }

  OBJECT ARRAY FUNCTION GetAdditionalComponents()
  {
    RETURN this->pvt_additionalcomponents;
  }

  MACRO SetAdditionalComponents(OBJECT ARRAY comps)
  {
    this->pvt_additionalcomponents := comps;
    this->ExtUpdatedAdditionalComponents();
  }

  MACRO SetOnClickLink(FUNCTION PTR onclicklink)
  {
    this->pvt_onclicklink := onclicklink;
    this->ExtUpdatedEventMask();
  }

  MACRO SetOnCallback(FUNCTION PTR oncallback)
  {
    this->pvt_oncallback := oncallback;
    this->ExtUpdatedEventMask();
  }

  MACRO SetOnData(FUNCTION PTR ondata)
  {
    this->pvt_ondata := ondata;
    this->ExtUpdatedEventMask();
  }

  MACRO SetData(RECORD newdata)
  {
    // Simple check if data actually changed
    RECORD ARRAY newfields := UnpackRecord(newdata);
    IF (Length(newfields) = Length(UnpackRecord(this->pvt_data)))
    {
      BOOLEAN changed;
      FOREVERY (RECORD field FROM newfields)
      {
        IF (NOT CellExists(this->pvt_data, field.name)
            OR TypeID(GetCell(this->pvt_data, field.name)) != TypeID(field.value)
            OR NOT IsTypeidComparable(TypeID(field.value))
            OR GetCell(this->pvt_data, field.name) != field.value)
        {
          changed := TRUE;
          BREAK;
        }
      }
      IF (NOT changed)
        RETURN;
    }

    this->pvt_data := newdata;
    this->ExtUpdatedData();
  }

  STRING FUNCTION GetToddIframeUrl()
  {
    RETURN "/.webhare/direct/tollium/toddiframe.js";
  }

  // ---------------------------------------------------------------------------
  //
  // Callbacks
  //

  // Called when component is unloaded
  UPDATE PUBLIC MACRO OnUnloadComponent()
  {
    this->ResetDownloads();
  }

  // ---------------------------------------------------------------------------
  //
  // Tollium overrides
  //

  UPDATE PUBLIC BOOLEAN FUNCTION EnabledOn(INTEGER min, INTEGER max, STRING ARRAY flags, STRING selectionmatch)
  {
    IF (this->onenabledon != DEFAULT FUNCTION PTR)
      RETURN this->onenabledon(min, max, flags, selectionmatch);

    RETURN FALSE;
  }

  UPDATE PUBLIC MACRO ProcessInboundMessage(STRING type, RECORD msgdata)
  {
    SWITCH (type)
    {
      CASE "clicklink"
      {
        IF(this->pvt_onclicklink != DEFAULT FUNCTION PTR)
          this->pvt_onclicklink(msgdata.href);
      }
      CASE "callback"
      {
        IF (this->pvt_oncallback != DEFAULT FUNCTION PTR)
          this->pvt_oncallback(msgdata.parameters);
      }
      CASE "data"
      {
        this->pvt_data := msgdata.data;
        IF (this->pvt_ondata != DEFAULT FUNCTION PTR)
          this->pvt_ondata();
      }
      CASE "post" // { tollium_iframe: "post"; type: string; data: unknown };
      {
        IF(this->onclientmessage != DEFAULT FUNCTION PTR)
          this->onclientmessage(msgdata.msg.type, msgdata.msg.data);
      }
      CASE "postmessage"
      {
        IF(CellExists(msgdata.data,"__REQUESTTOKEN")) //it's a response
        {
          INTEGER matchingrequest := (SELECT AS INTEGER #pendingrequests + 1 FROM this->pendingrequests WHERE pendingrequests.requesttoken = msgdata.data.__requesttoken) - 1;
          IF(matchingrequest < 0)
            RETURN;

          DELETE CELL __requesttoken FROM msgdata.data;
          this->pendingrequests[matchingrequest].defer.resolve(msgdata.data);
          DELETE FROM this->pendingrequests AT matchingrequest;
        }
        ELSE IF(this->onmessage != DEFAULT FUNCTION PTR)
        {
          this->onmessage(msgdata.data, msgdata.origin);
        }
      }
      DEFAULT
      {
        TolliumComponentBase::ProcessInboundMessage(type, msgdata);
      }
    }
  }

  // ---------------------------------------------------------------------------
  //
  // ExtXXX functions
  //

  MACRO ExtUpdatedContent()
  {
    this->EnsureFilledDirtyFlags();
    this->dirtyflags.content := TRUE;
  }

  MACRO ExtUpdatedAdditionalComponents()
  {
    this->EnsureFilledDirtyFlags();
    this->dirtyflags.addcomps := TRUE;
  }

  MACRO ExtUpdatedEventMask()
  {
    this->EnsureFilledDirtyFlags();
    this->dirtyflags.eventmask := TRUE;
  }

  MACRO ExtUpdatedData()
  {
    this->EnsureFilledDirtyFlags();
    this->dirtyflags.data := TRUE;
  }

  MACRO ExtUpdatedViewport()
  {
    this->EnsureFilledDirtyFlags();
    this->dirtyflags.viewport := TRUE;
  }

  MACRO ExtUpdatedSandbox()
  {
    this->EnsureFilledDirtyFlags();
    this->dirtyflags.sandbox := TRUE;
  }

  // ---------------------------------------------------------------------------
  //
  // Public API
  //

  /** Set a viewport from the iframe (usually used to simulate devices)
      @param width Width in pixels
      @param height Height in pixels */
  PUBLIC MACRO SetViewport(INTEGER width, INTEGER height)
  {
    this->_viewport := CELL[ width, height ];
    this->ExtUpdatedViewport();
  }

  /** Undo setting a viewport */
  PUBLIC MACRO ResetViewport()
  {
    this->_viewport := DEFAULT RECORD;
    this->ExtUpdatedViewport();
  }

  /// Reset content (iframe page & contents)
  PUBLIC MACRO ResetContent()
  {
    //this->pvt_src := "about:blank";
    this->embedded := RECORD[];
    this->mainpagelink := "";
    this->_setvalue := DEFAULT BLOB;
    this->ResetDownloads();
  }

  /// Scroll to a fragment (only works when content is updated in the same update round)
  PUBLIC MACRO ScrollTo(STRING fragment)
  {
    this->urlfragment := fragment;
    this->ExtUpdatedComponent();
  }

  /** Execute a javascript function within the iframe
      @param funcname Name of the function to call
      @param args Arguments
  */
  PUBLIC MACRO CallJavascript(STRING funcname, VARIANT ARRAY args) __ATTRIBUTES__(VARARG)
  {
    IF(GetDtapStage() = "development")
    {
      IF(NOT did_calljs_warning)
      {
        DumpValue(GetStackTrace(),'boxed');
        this->owner->RunSimpleScreen("info", "Future versions of WebHare will remove CallJavascript as it is not compatible with our Content-Security-Policy. Use PostMessage");
        did_calljs_warning := TRUE;
      }
    }
    this->QueueOutboundMessage("JS", [ funcname := funcname, args := args ]);
  }

/** @short Do a callback from todd to the iframe with the given data
    @param data The data to send with the callback
*/
  PUBLIC MACRO DoCallback(RECORD data)
  {
    this->QueueOutboundMessage("Callback", data);
  }

  /** Post a message using the tollium-iframe-api protocol */
  PUBLIC MACRO Post(STRING type, VARIANT ARRAY args) __ATTRIBUTES__(VARARG) {
    this->QueueOutboundMessage("PostToGuest", CELL[ type, args ]);
  }

  PUBLIC MACRO PostMessage(VARIANT message, STRING targetorigin)
  {
    this->QueueOutboundMessage("PostMessage", CELL[ message, targetorigin ]);
  }

  PUBLIC OBJECT FUNCTION PostRequest(RECORD message, STRING targetorigin)
  {
    //FIXME reject pendingrequest promises on unload

    STRING requesttoken := GenerateUFS128BitId();
    RECORD defer := CreateDeferredPromise();
    INSERT CELL __requesttoken := requesttoken INTO message;
    INSERT CELL [ requesttoken, defer ] INTO this->pendingrequests AT END;
    this->QueueOutboundMessage("PostMessage", CELL[ message, targetorigin ]);
    RETURN defer.promise;
  }

  /// Trigger print
  PUBLIC MACRO Print()
  {
    this->QueueOutboundMessage("Print", DEFAULT RECORD);
  }

  /** @short Add an embedded object (to which you can refer in your HTML source code)
      @param data Data to export
      @param mimetype Mimetype for this object
      @return The hyperlink to use in your HTML code to refer to this object
  */
  PUBLIC STRING FUNCTION AddEmbeddedObject(BLOB data, STRING mimetype)
  {
    RETURN this->AddInlineDownload(data, mimetype, "");
  }

  /** @short Delete an embedded object
      @param hyperlink Link to the object */
  PUBLIC MACRO DeleteEmbeddedObject(STRING dellink)
  {
    this->RemoveDownload(dellink);
  }

  /** Initialize the value with an assetpack */
  PUBLIC MACRO InitializeWithAssetpack(STRING assetpack, RECORD initdata DEFAULTSTO DEFAULT RECORD) {
    this->QueueOutboundMessage("InitializeWithAssetpack", CELL[ assetpack, initdata, devmode := IsModuleInstalled("dev") ]);
  }

  /** Update the initial data without reloading the assetpack */
  PUBLIC MACRO UpdateInitData(RECORD initdata) {
    this->QueueOutboundMessage("UpdateInitData", CELL[ initdata ]);
  }
>;
