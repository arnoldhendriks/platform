﻿<?wh
/** @short The tollium gettid API
    @long Pre-tollium applications can use this API to be able to switch to the GetTID function.
          Tollium applications should use screenbase.whlib for GetTID
    @topic localization/gettid
*/

LOADLIB "wh::adhoccache.whlib";
LOADLIB "wh::datetime.whlib";
LOADLIB "wh::ipc.whlib";
LOADLIB "wh::util/algorithms.whlib";
LOADLIB "wh::xml/dom.whlib";
LOADLIB "wh::witty.whlib";

LOADLIB "mod::system/lib/logging.whlib";
LOADLIB "mod::system/lib/resources.whlib";

LOADLIB "mod::tollium/lib/internal/gettid.whlib" EXPORT ParseXMLTIdPtr; //ParseXMLTIdPtr for external use is deprecated, but formsapi loader needs it

MACRO PTR missingtidcallback;

PUBLIC BOOLEAN __shortunknowntids;

/** Returns the current language
    @return The language code for the current language (lowercase, eg 'nl')
*/
PUBLIC STRING FUNCTION GetTIDLanguage()
{
  RETURN tidlang;
}

/** Sets the current language
    @param language The language code for the new current language
*/
PUBLIC MACRO SetTIDLanguage(STRING language)
{
  tidlang := ToLowercase(language);
  __SETWITTYGETTIDFALLBACK(PTR GetTid, PTR GetHTMLTid);
}

/** Get the current missing tid callback
    @return Current callback
*/
PUBLIC MACRO PTR FUNCTION GetMissingTidCallback()
{
  RETURN missingtidcallback;
}

/** Sets the callback called when a missing tid is encountered
    @param callback Callback, signature: MACRO missingtidcallback(data)
*/
PUBLIC MACRO SetMissingTidCallback(MACRO PTR callback)
{
  missingtidcallback := callback;
}

RECORD FUNCTION ParseIfParam(OBJECT fragment)
{
  RECORD retval :=
      CELL[ t := "ifparam"
          , p := ToInteger(fragment->GetAttribute("p"),0)
          , value := fragment->GetAttribute("value")
          , subs := DEFAULT VARIANT ARRAY
          , subselse := DEFAULT VARIANT ARRAY
          ];

  BOOLEAN inelse;
  FOR(OBJECT part := fragment->firstchild; ObjectExists(part); part := part->nextsibling)
  {
    IF(part->nodetype = 1 AND part->localname="else")
      inelse := TRUE;
    ELSE IF(inelse)
      INSERT ParseTextNode(part) INTO retval.subselse AT END;
    ELSE
      INSERT ParseTextNode(part) INTO retval.subs AT END;
  }

  RETURN retval;
}

VARIANT FUNCTION ParseTextNode(OBJECT part)
{
  INTEGER nodetype := part->nodetype;

  IF(nodetype = 3) //XmlTextNode
    RETURN part->textcontent;

  IF(nodetype != 1)
    THROW NEW Exception("Unexpected node " || part->nodename || " on line " || part->linenum);

  STRING localname := part->localname;
  IF(part->namespaceuri  = "http://www.webhare.net/xmlns/tollium/screens")
  {
    SWITCH(localname)
    {
      CASE "br"
      {
        RETURN "\n";
      }
      CASE "param"
      {
        INTEGER p := ToInteger(part->GetAttribute("p"),0);
        IF(p<1 OR p>4)
          THROW NEW Exception("Unexpected parameter on line " || part->linenum);

        RETURN p;
      }
      CASE "ifparam"
      {
        RETURN ParseIfParam(part);
      }
      DEFAULT
      {
        THROW NEW Exception("Unexpected screens node " || localname || " on line " || part->linenum);
      }
    }
  }
  ELSE IF(part->namespaceuri  = "http://www.w3.org/1999/xhtml")
  {
    SWITCH(localname)
    {
      CASE "b", "i", "u"
      {
        RETURN CELL[ t := "tag"
                   , tag := localname
                   , subs := DecodeLanguageText(part)
                   ];
      }
      CASE "br"
      {
        RETURN "\n";
      }
      CASE "a"
      {
        RETURN CELL[ t := "a"
                   , link := part->GetAttribute("href")
                   , linkparam := ToInteger(part->GetAttribute("data-href-param"),0)
                   , target := part->GetAttribute("target")
                   , subs := DecodeLanguageText(part)
                   ];
      }
      DEFAULT
      {
        THROW NEW Exception("Unexpected HTML node " || localname || " on line " || part->linenum);
      }
    }
  }
  ELSE
  {
    THROW NEW Exception("Unexpected node {" || part->namespaceuri || "}" || localname|| " line " || part->linenum);
  }
}

VARIANT FUNCTION DecodeLanguageText(OBJECT fragment)
{
  VARIANT ARRAY outparts;
  FOR(OBJECT part := fragment->firstchild; ObjectExists(part); part := part->nextsibling)
    INSERT ParseTextNode(part) INTO outparts AT END;

  IF(Length(outparts) = 0)
    RETURN "";
  IF(Length(outparts) = 1 AND TYPEID(outparts[0]) = TYPEID(STRING))
    RETURN outparts[0];
  RETURN outparts;
}

RECORD ARRAY FUNCTION ReadLanguageTexts(OBJECT element, STRING pathsofar)
{
  RECORD ARRAY texts;
  FOR(OBJECT node := element->firstchild;ObjectExists(node);node:=node->nextsibling)
  {
    IF(node->nodetype != 1) //XmlElementNode
      CONTINUE;

    IF(node->localname = "textgroup")
    {
      STRING name := ToLowercase(node->GetAttribute("gid"));
      texts := texts CONCAT ReadLanguageTexts(node, pathsofar || name || ".");
    }
    ELSE IF(node->localname = "text")
    {
      STRING name := ToLowercase(node->GetAttribute("tid"));
      INSERT [ tid := pathsofar || name, text := DecodeLanguageText(node) ] INTO texts AT END;
    }
  }
  RETURN texts;
}

RECORD FUNCTION CompileLanguageFile(STRING resname)
{
  BLOB input := GetWebHareResource(resname);
  OBJECT doc := MakeXMLDocument(input);
  IF(NOT ObjectExists(doc->documentelement))
    THROW NEW Exception(`Invalid XML document '${resname}`);
  RECORD result := [ fallbacklanguage := ToLowercase(doc->documentelement->GetAttribute("fallbacklanguage"))
                   , langcode := ToLowercase(doc->documentelement->GetAttribute("xml:lang"))
                   , texts := (SELECT * FROM ReadLanguageTexts(doc->documentelement, "") ORDER BY ToLowercase(tid))
                   ];
  RETURN result;
}

STRING FUNCTION ExecuteCompiledTidText(VARIANT text, STRING ARRAY params, BOOLEAN rich)
{
  IF(TYPEID(text) = TYPEID(STRING))
    RETURN rich ? EncodeHTML(text) : text;

  STRING output;
  FOREVERY(VARIANT tok FROM VARIANT ARRAY(text))
  {
    IF(TYPEID(tok) = TYPEID(STRING))
    {
      output := output || (rich ? EncodeHTML(tok) : tok);
    }
    ELSE IF(TYPEID(tok) = TYPEID(INTEGER))
    {
      IF(tok >= 1)
      {
        STRING get_param;
        IF(tok <= Length(params))
          get_param := params[tok - 1];
        output := output || (rich ? EncodeHTML(get_param) : get_param);
      }
    }
    ELSE IF(TYPEID(tok) = TYPEID(RECORD))
    {
      IF(tok.t = "tag")
      {
        STRING sub := ExecuteCompiledTidText(tok.subs, params, rich);
        output := output || (rich ? `<${tok.tag}>${sub}</${tok.tag}>` : sub);
      }
      ELSE IF(tok.t = "ifparam")
      {
        STRING get_param;
        IF(tok.p <= Length(params))
          get_param := params[tok.p - 1];

        output := output || ExecuteCompiledTidText(ToUppercase(get_param) = ToUppercase(tok.value) ? tok.subs : tok.subselse, params, rich);
      }
      ELSE IF(tok.t = "a")
      {
        STRING sub := ExecuteCompiledTidText(tok.subs, params, rich);
        IF(rich)
        {
          STRING link := tok.link;
          IF(tok.linkparam > 0 AND tok.linkparam <= Length(params))
            link := params[tok.linkparam - 1] ?? link;

          IF(link != "")
            output := output || `<a href="${EncodeValue(link)}">${sub}</a>`;
          ELSE
            output := output || sub;
        }
        ELSE
        {
          output := output || sub;
        }
      }
    }
  }
  RETURN output;
}

RECORD langfilecache; //TODO allow invalidate on garbage collection ? need adhoccache to help with setting that up generically

RECORD FUNCTION GetCompiledLanguageFile(STRING module, STRING langcode)
{
  STRING resname := `mod::${module}/language/${langcode}.xml`;
  BLOB resdata := GetWebhareResource(resname, [ allowmissing := TRUE ]);
  IF(Length(resdata) = 0)
  {
    STRING fallbackresname := `mod::${module}/language/default.xml`;
    resdata := GetWebhareResource(fallbackresname, [ allowmissing := TRUE ]);

    IF(Length(resdata) = 0)
      THROW NEW RetrieveResourceException(resname, `Cannot find '${resname}' or the fallback file '${fallbackresname}'`, RECORD[]);

    resname := fallbackresname;
  }
  RETURN [ value := CELL[ resource := resname, ...CompileLanguageFile(resname) ]
         , ttl := 60 * 60 * 1000
         , eventmasks := GetResourceEventMasks([resname])
         ];
}

BOOLEAN invalidation_enabled;

MACRO ClearLocalTidCache(STRING event, RECORD ARRAY data)
{
  langfilecache := DEFAULT RECORD;
}

/// Enable auto-invalidation of the local tid cache when the language files change
PUBLIC MACRO __EnableTidCacheInvalidation()
{
  IF (NOT invalidation_enabled)
  {
    RegisterMultiEventCallback("system:modulesupdate", PTR ClearLocalTidCache);
    RegisterMultiEventCallback("system:modulefolder.mod::*/language/", PTR ClearLocalTidCache);
    langfilecache := DEFAULT RECORD;
    invalidation_enabled := TRUE;
  }
}

RECORD FUNCTION GetLanguageFile(STRING module, STRING langcode)
{
  STRING langkey := module || ":" || langcode;
  IF(NOT CellExists(langfilecache, langkey))
    langfilecache := CellInsert(langfilecache, langkey, GetAdhocCached(CELL[module,langcode], PTR GetCompiledLanguageFile(module, langcode)));

  RETURN GetCell(langfilecache, langkey);
}

STRING ARRAY FUNCTION GroupParams(STRING p1, STRING p2, STRING p3, STRING p4)
{
  STRING ARRAY params;
  IF(p4 != "")
    params := [ p1, p2, p3, p4 ];
  ELSE IF(p3 != "")
    params := [ p1, p2, p3 ];
  ELSE IF(p2 != "")
    params := [ p1, p2 ];
  ELSE IF(p1 != "")
    params := [ p1 ];
  RETURN params;
}

STRING FUNCTION WrapForMode(STRING text, BOOLEAN rich)
{
  RETURN rich ? EncodeHTML(text) : text;
}

STRING FUNCTION MissingTid(STRING module, STRING langcode, STRING tid, STRING ARRAY params, BOOLEAN rich)
{
  IF(missingtidcallback != DEFAULT MACRO PTR)
    missingtidcallback(CELL[ module := module, langcode := langcode, tid := tid, params, stacktrace := ArraySlice(GetStackTrace(), 3) ]);

  IF(__shortunknowntids)
    RETURN WrapForMode(Substring(tid,SearchLastSubstring(tid,'.')), rich);
  ELSE
    RETURN WrapForMode(`(cannot find text: ${tid})`, rich);
}

STRING FUNCTION CalcTIDForLanguage2(STRING langcode, STRING tid, STRING ARRAY params, BOOLEAN rich)
{
  tid := GetCanonicalTid(tid);
  IF(tid = "tollium:tilde.locale.datetimestrings")
    RETURN WrapForMode(GetLanguageDatetimeStrings(langcode), rich);

  INTEGER modsep := SearchSubstring(tid,':');
  IF(modsep = -1)
    RETURN WrapForMode(`(missing module name in tid: ${tid})`, rich);

  STRING module := Left(tid, modsep);
  IF(tid LIKE "* *") //spaces? map to underscore, but NOT the modulename.
    tid := module || ":" || Substitute(Substring(tid, modsep + 1),' ','_'); //rewrite the tid so the error shows what we were exactly looking for

  IF(langcode = "debug")
  {
    IF(__shortunknowntids)
      tid := Substring(tid,SearchLastSubstring(tid,'.'));

    //Use {, as that mixes more safely with HTML than <s, which is useful with rich text...
    STRING debugtid := "{" || Detokenize(STRING[tid,...params], "|") || "}";
    RETURN WrapForMode(debugtid, rich);
  }

  TRY
  {
    RECORD compiled := GetLanguageFile(module, langcode);
    RECORD lookup := CELL[ tid := ToLowercase(Substring(tid, modsep+1)) ];

    RECORD match := RecordLowerBound(compiled.texts, lookup, ["tid"]);
    IF(NOT match.found AND compiled.fallbacklanguage != "")
    { //we do just one fallback. in the future we could either recurse (but guard against loops) or allow the user to set a list of fallback languages
      compiled := GetLanguageFile(module, compiled.fallbacklanguage);
      match := RecordLowerBound(compiled.texts, lookup, ["tid"]);
    }

    IF(NOT match.found)
      RETURN MissingTid(module, langcode, tid, params, rich);

    RETURN ExecuteCompiledTidText(compiled.texts[match.position].text, params, rich);
  }
  CATCH(OBJECT<RetrieveResourceException> e)
  {
    LogHarescriptException(e);
    RETURN MissingTid(module, langcode, tid, params, rich);
  }
}

/** Get the text for a tid in the current language
    @param tid Tid to get
    @param p1 Parameter 1
    @param p2 Parameter 2
    @param p3 Parameter 3
    @param p4 Parameter 4
    @return Language text
    @loadlib mod::tollium/lib/gettid.whlib
*/
PUBLIC STRING FUNCTION GetTid(STRING tid, STRING p1 DEFAULTSTO "", STRING p2 DEFAULTSTO "", STRING p3 DEFAULTSTO "", STRING p4 DEFAULTSTO "")
{
  //Doing these fast returns inside Get*Tid reduced their hovi call times from 17ms to 14ms, mostly because GroupParams took 3ms of that 17
  IF(tid = "")
    RETURN "";

  IF(tid LIKE ":*")
    RETURN Substring(tid,1);
  ELSE IF(tid LIKE "<>*")
    RETURN DecodeHTML(Substring(tid,2));

  RETURN CalcTIDForLanguage2(tidlang, tid, GroupParams(p1, p2, p3, p4), FALSE);
}

/** Get the HTML text for a tid in the current language
    @param tid Tid to get
    @param p1 Parameter 1
    @param p2 Parameter 2
    @param p3 Parameter 3
    @param p4 Parameter 4
    @return Language HTML text
*/
PUBLIC STRING FUNCTION GetHTMLTid(STRING tid, STRING p1 DEFAULTSTO "", STRING p2 DEFAULTSTO "", STRING p3 DEFAULTSTO "", STRING p4 DEFAULTSTO "")
{
  IF(tid = "")
    RETURN "";

  IF(tid LIKE ":*")
    RETURN EncodeHTML(Substring(tid,1));
  ELSE IF(tid LIKE "<>*")
    RETURN Substring(tid,2);

  RETURN CalcTIDForLanguage2(tidlang, tid, GroupParams(p1, p2, p3, p4), TRUE);
}

/** Get the text for a tid for a specific language
    @param languagecode Language code
    @param tid Tid to get
    @param p1 Parameter 1
    @param p2 Parameter 2
    @param p3 Parameter 3
    @param p4 Parameter 4
    @return Language text
*/
PUBLIC STRING FUNCTION GetTidForLanguage(STRING languagecode, STRING tid, STRING p1 DEFAULTSTO "", STRING p2 DEFAULTSTO "", STRING p3 DEFAULTSTO "", STRING p4 DEFAULTSTO "")
{
  IF(tid = "")
    RETURN "";

  IF(tid LIKE ":*")
    RETURN Substring(tid,1);
  ELSE IF(tid LIKE "<>*")
    RETURN DecodeHTML(Substring(tid,2));

  RETURN CalcTIDForLanguage2(languagecode, tid, GroupParams(p1, p2, p3, p4), FALSE);
}

/** Get the HTML text for a tid in the current language
    @param languagecode Language code
    @param tid Tid to get
    @param p1 Parameter 1
    @param p2 Parameter 2
    @param p3 Parameter 3
    @param p4 Parameter 4
    @return Language HTML text
*/
PUBLIC STRING FUNCTION GetHTMLTidForLanguage(STRING languagecode, STRING tid, STRING p1 DEFAULTSTO "", STRING p2 DEFAULTSTO "", STRING p3 DEFAULTSTO "", STRING p4 DEFAULTSTO "")
{
  IF(tid = "")
    RETURN "";

  IF(tid LIKE ":*")
    RETURN EncodeHTML(Substring(tid,1));
  ELSE IF(tid LIKE "<>*")
    RETURN Substring(tid,2);

  RETURN CalcTIDForLanguage2(languagecode, tid, GroupParams(p1, p2, p3, p4), TRUE);
}


/** Returns the list of tids defined in a group in the current language
    @param gid Language text group
    @return List of tids in that group
*/
PUBLIC STRING ARRAY FUNCTION GetTIDList(STRING gid)
{
  RETURN GetTIDListForLanguage(tidlang, gid);
}

/** Returns the list of tids defined in a group
    @param langcode Language code
    @param gid Language text group
    @return List of tids in that group
*/
PUBLIC STRING ARRAY FUNCTION GetTIDListForLanguage(STRING langcode, STRING gid)
{
  INTEGER colon := SearchLastSubstring(gid,':');
  IF (colon = -1)
    THROW NEW Exception("Missing module name in call for gid '" || gid || "'");

  STRING wantmodule := Left(gid,colon);
  STRING gidpath := Substring(gid,colon+1,Length(gid));

  RECORD compiled := GetLanguageFile(wantmodule, langcode);
  RETURN SELECT AS STRING ARRAY wantmodule || ":" || tid
           FROM compiled.texts
          WHERE tid LIKE VAR gidpath || ".*"
                AND tid NOT LIKE VAR gidpath || ".*.*";
}

/** Returns the canonical tid for a tid (eg tollium:tilde.xxx for ~xxx)
    @param tid Tid to rewrite
    @return Canonical tid
*/
PUBLIC STRING FUNCTION GetCanonicalTid(STRING tid)
{
  IF(tid LIKE "tollium:common.buttons.*")
    tid := "tollium:tilde." || Substring(tid,23);
  ELSE IF(tid LIKE "tollium:common.actions.*")
    tid := "tollium:tilde." || Substring(tid,23);
  ELSE IF(tid LIKE "tollium:common.labels.*")
    tid := "tollium:tilde." || Substring(tid,22);
  ELSE IF(tid LIKE "~*")
    tid := "tollium:tilde." || Substring(tid,1);

  RETURN tid;
}

PUBLIC RECORD FUNCTION __GetLanguageFile(STRING module, STRING langcode)
{
  RETURN GetLanguageFile(module, langcode);
}
