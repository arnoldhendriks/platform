<?wh
LOADLIB "wh::internet/urls.whlib";

LOADLIB "mod::tollium/lib/componentbase.whlib";
LOADLIB "mod::tollium/lib/screenbase.whlib";
LOADLIB "mod::tollium/lib/internal/hsvalue_introspection.whlib";


/////////////////////////////////////////////////////////////////////
// The HSValue component

PUBLIC OBJECTTYPE HSValue EXTEND TolliumFragmentBase
<
  RECORD valueholder;

  PUBLIC PROPERTY value(this->valueholder.val, SetValue);

  PUBLIC PROPERTY showprivate(this->dumper->showprivate, this->dumper->showprivate);
  PUBLIC PROPERTY forceshowraw(this->dumper->forceshowraw, this->dumper->forceshowraw);
  PUBLIC PROPERTY expandlevel(this->dumper->expandlevel, this->dumper->expandlevel);

  PUBLIC PROPERTY iframe(this->valuedisplay, -);

  OBJECT serializer;
  OBJECT dumper;

  MACRO NEW()
  {
    EXTEND this BY TolliumIsComposable;
    this->serializer := NEW HSValueDescriber();
    this->dumper := NEW HSValueDumper();
    this->valueholder := [ val := DEFAULT RECORD ];
  }

  UPDATE PUBLIC MACRO StaticInit(RECORD data)
  {
    TolliumFragmentBase::StaticInit(data);
    ^valuedisplay->width := data.width ?? ^valuedisplay->width;
    ^valuedisplay->height := data.height ?? ^valuedisplay->height;
    ^valuedisplay->minwidth := data.minwidth ?? ^valuedisplay->minwidth;
    ^valuedisplay->minheight := data.minheight ?? ^valuedisplay->minheight;
  }

  PUBLIC MACRO SetValue(VARIANT newval)
  {
    this->valueholder := [ val := newval ];
    //this->valuedisplay->blobvalue := StringToBlob(AnyToString(this->valueholder.val,'htmltree:4'));

    RECORD descriptionrec;
    IF (this->dumper->IsDescribedValue(newval))
      descriptionrec := newval;
    ELSE
      descriptionrec := this->serializer->Describe(this->valueholder.val);

    this->valuedisplay->blobvalue := this->dumper->StyledAnyToHTMLBlob(descriptionrec, this->valuedisplay);
  }

  PUBLIC BOOLEAN FUNCTION DoHandleLink(STRING url)
  {
    RECORD urlrec := UnpackURL(url);
    IF (urlrec.scheme != "webhare")
    {
      this->owner->frame->OpenBrowserWindow(url, "_blank");
      RETURN FALSE;
    }

    IF (urlrec.host = "wrd")
    {
      STRING ARRAY urlparts := Tokenize(urlrec.urlpath, ",");
      IF (Length(urlparts) = 1)
      {
         this->tolliumcontroller->SendApplicationMessage("wrd:browser", [ schemaname := urlparts[0] ], DEFAULT RECORD, FALSE);
      }
    }
    ELSE IF (urlrec.host = "whfs")
    {
      // Open the publisher by WHFS ID, URL or WHFSPATH
      this->tolliumcontroller->SendApplicationMessage("publisher:app", [ selectobject := urlrec.urlpath ], DEFAULT RECORD, FALSE);
    }
    RETURN TRUE;
  }
>;
