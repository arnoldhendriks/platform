export * from "./video";

import { warnESFile } from "@mod-system/js/internal/es-warning";
warnESFile("@mod-publisher/js/richcontent/video.es");
