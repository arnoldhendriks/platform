export * from "./datetime";

import { warnESFile } from "@mod-system/js/internal/es-warning";
warnESFile("@mod-publisher/js/form/fields/datetime.es");
