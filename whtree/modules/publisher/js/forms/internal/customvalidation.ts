import * as dompack from '@webhare/dompack';
import type { FieldErrorOptions, FormFrontendMessage } from '../formbase';
import { debugFlags } from '@webhare/env';
import { getFieldDisplayName, getFieldNativeError } from '@webhare/forms/src/domsupport';

///Fired at nodes to apply error
export type SetFieldErrorData = {
  error: FormFrontendMessage;
  reportimmediately: boolean;
  serverside: boolean;
  metadata: unknown;
};

export type FieldValidator<E extends HTMLElement = HTMLElement> = (node: E) => Promise<FormFrontendMessage | undefined> | FormFrontendMessage | undefined;

function setupServerErrorClear(field: HTMLElement) {
  const group = field.closest<HTMLElement>('.wh-form__fieldgroup') || field;
  field.propWhCleanupFunction = () => {
    group.removeEventListener("change", field.propWhCleanupFunction!, true);
    group.removeEventListener("input", field.propWhCleanupFunction!, true);
    group.removeEventListener("blur", field.propWhCleanupFunction!, true);
    setFieldError(field, '', { serverside: true });
    field.propWhCleanupFunction = undefined;
  };

  // to be rightly paranoid (plugins and JS directly editing other fields) we'll blur when anything anywhere seems to change
  // eg wrd.testwrdauth-emailchange would fail on Chrome without this if the browser window was not currently focused
  group.addEventListener("change", field.propWhCleanupFunction, true);
  group.addEventListener("input", field.propWhCleanupFunction, true);
  if (!field.closest('form[novalidate]')) //if we're doing html5 validation, errors will block submit, so let's already clear on blur
    group.addEventListener("blur", field.propWhCleanupFunction, true);
}

class ComponentValidationState {
  el: HTMLElement;

  /** Explicitly set error */
  explicit: {
    error: FormFrontendMessage;
    serverside: boolean;
    reportimmediately: boolean;
    metadata?: unknown;

  } | null = null;

  /** Dynamic errors (generated by the validation steps, refreshable instead of waiting for explciit errors) */
  dynamicError: FormFrontendMessage | null = null;

  validators: FieldValidator[] = [];

  constructor(el: HTMLElement) {
    this.el = el;
  }

  getState(): { error: FormFrontendMessage; suggested?: never } | { error?: never; suggested: FormFrontendMessage } | null {
    const err = this.getError();
    if (err)
      return { error: err };

    if (this.el.propWhValidationSuggestion)
      return { suggested: this.el.propWhValidationSuggestion };

    return null;
  }

  getError(): FormFrontendMessage | null {
    return this.explicit?.error || this.dynamicError || getFieldNativeError(this.el) || null;
  }

  hasError(): boolean {
    return Boolean(this.getError());
  }
}

const validations = new WeakMap<HTMLElement, ComponentValidationState>();

export function getValidationState(field: HTMLElement) {

  const state = validations.get(field);
  if (state)
    return state;

  const newstate = new ComponentValidationState(field);
  validations.set(field, newstate);
  return newstate;
}

/* TODO Are we sure we should expose this API? A form-level setupValidator might be better as we can then re-validate at the proper point in time
   instead of relying on an external user to enable and shutdown the error. We're mixing two completely different systems with setupValidator (which
   works nicely with our focusout hadling) and setFieldError (which is extremely stateful and relies on the error-setter managing all the edge cases)

   Oh and serverside errors float somewhere halfway that spectrum.. as we setup explicit clearing callbacks to remove the state we created ourselves.
*/
export function setFieldError(field: Element, error: FormFrontendMessage, options?: Partial<FieldErrorOptions>) {
  if (!dompack.isHTMLElement(field))
    throw new Error(`Field is not a valid target for handling errors`);

  const state = getValidationState(field);
  if (!error) {  //clearing
    if (!state.explicit)
      return; //no change

    state.explicit = null;
    if (debugFlags.fhv)
      console.log(`[fhv] Clearing error state for ${getFieldDisplayName(field)}`, field, error, options);
  } else { //changing ?
    if (debugFlags.fhv)
      console.log(`[fhv] ${state.explicit ? 'Updating' : 'Setting'} error state for ${getFieldDisplayName(field)}`, field, error, options);

    state.explicit = {
      error,
      serverside: options?.serverside || false,
      reportimmediately: options?.reportimmediately || false,
      metadata: options?.metadata
    };

    if (options?.serverside)
      setupServerErrorClear(field);
  }

  updateFieldError(field);
}

export function updateFieldError(field: HTMLElement) {
  const state = getValidationState(field);

  dompack.dispatchCustomEvent(field, 'wh:form-setfielderror', //this is where parsley hooks in and cancels to handle the rendering of faults itself
    {
      bubbles: true,
      cancelable: true,
      detail: {
        error: state.explicit?.error || getFieldNativeError(field) || '',
        reportimmediately: state.explicit?.reportimmediately || false,
        serverside: state.explicit?.serverside || false,
        metadata: state.explicit?.metadata
      }
    });
}

/** Set up a custom validator
 * @param node - Form element to validate
 * @param checker - Sync or async function that returns a string with an error message or undefined if the field is valid.
*/
export function setupValidator<NodeType extends HTMLElement>(node: NodeType, checker: FieldValidator<NodeType>): void {
  getValidationState(node).validators.push(checker as FieldValidator);

  //if the checker as a name, we'll add it. otherwise we'll ensure that data-wh-form-custom-validator is set so we can scan for it (and it's probably nice to see it in the dom)
  node.dataset.whFormCustomValidator = ((node.dataset.whFormCustomValidator || "") + " " + (checker.name || "?")).trim();
}
