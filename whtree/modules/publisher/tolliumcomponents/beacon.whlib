<?wh

LOADLIB "mod::publisher/lib/components/fsobjectoverviewbase.whlib";

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/whfs.whlib";

LOADLIB "mod::tollium/lib/componentbase.whlib";


PUBLIC OBJECTTYPE DCBeacon EXTEND TolliumFragmentBase
< // ---------------------------------------------------------------------------
  //
  // Variables
  //

  OBJECT ARRAY searchroots;
  STRING rowkeyfield;


  // ---------------------------------------------------------------------------
  //
  // Public properties
  //

  PUBLIC PROPERTY value(^beacon->value, SetValue);
  UPDATE PUBLIC PROPERTY title(^beacon->title, ^beacon->title);
  UPDATE PUBLIC PROPERTY required(^beacon->required, ^beacon->required);
  PUBLIC PROPERTY empty(^beacon->empty, ^beacon->empty);
  UPDATE PUBLIC PROPERTY errorlabel(^beacon->errorlabel, ^beacon->errorlabel);
  PUBLIC PROPERTY dirtylistener(^beacon->dirtylistener, ^beacon->dirtylistener);
  PUBLIC PROPERTY onchange(^beacon->onchange, ^beacon->onchange);
  PUBLIC PROPERTY options(GetOptions, -);
  PUBLIC PROPERTY selection(GetSelection, -);


  // ---------------------------------------------------------------------------
  //
  // Initialization
  //

  MACRO NEW()
  {
    EXTEND this BY TolliumIsComposable;
  }

  UPDATE PUBLIC MACRO StaticInit(RECORD def)
  {
    TolliumFragmentBase::StaticInit(def);

    this->title := def.title;
    this->required := def.required;
    this->empty := def.empty;
    this->errorlabel := def.errorlabel;
    this->dirtylistener := def.dirtylistener;

    this->searchroots := this->GetSearchRoots();
    this->rowkeyfield := def.rowkeyfield;
  }

  UPDATE OBJECT ARRAY FUNCTION GetSearchRoots()
  {
    OBJECT applytester := this->contexts->applytester;
    IF(NOT ObjectExists(applytester))
      RETURN OBJECT[];

    OBJECT ARRAY searchroots;
    FOREVERY (INTEGER libid FROM applytester->GetLibrary("publisher:beacons"))
    {
      OBJECT libobject := OpenWHFSObject(libid);
      IF (ObjectExists(libobject))
        INSERT libobject INTO searchroots AT END;
    }
    RETURN searchroots;
  }

  UPDATE MACRO PreinitComponent()
  {
    this->RefreshBeacons();

    STRING ARRAY masks;
    FOREVERY(OBJECT searchroot FROM this->searchroots)
      masks := masks CONCAT searchroot->GetEventMasks();
    ^eventlistener->masks := masks;
  }


  // ---------------------------------------------------------------------------
  //
  // Property getters/setters
  //

  RECORD ARRAY FUNCTION GetOptions()
  {
    //not showing more fields than needed, so we can easily change implementation
    RETURN SELECT rowkey, title FROM ^beacon->options;
  }

  RECORD FUNCTION GetSelection()
  {
    RECORD sel := ^beacon->selection;
    //not showing more fields than needed, so we can easily change implementation
    RETURN sel.rowkey != 0 ? CELL[sel.rowkey, sel.title] : DEFAULT RECORD;
  }

  MACRO SetValue(VARIANT beacon)
  {
    DELETE FROM ^beacon->options WHERE NOT enabled;
    IF (IsValueSet(beacon) AND NOT RecordExists(SELECT FROM ^beacon->options WHERE rowkey = beacon))
      INSERT [ rowkey := beacon, title := GetTid("publisher:tolliumcomponents.beacon.deletedbeacon"), enabled := FALSE ] INTO ^beacon->options AT 0;
    ^beacon->value := beacon;
  }


  // ---------------------------------------------------------------------------
  //
  // Callbacks
  //

  MACRO OnEvents(RECORD ARRAY events)
  {
    this->RefreshBeacons();
  }


  // ---------------------------------------------------------------------------
  //
  // Helper functions
  //

  MACRO RefreshBeacons()
  {
    VARIANT defaultrowkey;
    IF (this->rowkeyfield = "id")
      defaultrowkey := 0;
    ELSE
      defaultrowkey := "";

    RECORD ARRAY foundbeacons;
    FOREVERY(OBJECT searchroot FROM this->searchroots)
      foundbeacons := foundbeacons CONCAT SELECT id
                                               , name
                                               , title := title ?? name
                                            FROM system.fs_objects
                                           WHERE parent = searchroot->id
                                                 AND type = OpenWHFSType("http://www.webhare.net/xmlns/publisher/contentlibraries/beacon")->id;

    ^beacon->options := [ [ rowkey := defaultrowkey, title := "", invalidselection := TRUE ] ] CONCAT
        SELECT rowkey := GetCell(foundbeacons, this->rowkeyfield)
             , title
          FROM foundbeacons
      ORDER BY ToUppercase(title);
  }
>;
