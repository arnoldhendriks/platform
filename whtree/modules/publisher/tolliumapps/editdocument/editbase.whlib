<?wh

LOADLIB "wh::javascript.whlib";

LOADLIB "mod::publisher/lib/internal/siteprofiles/reader.whlib";

LOADLIB "mod::system/lib/internal/whfs/base.whlib";
LOADLIB "mod::system/lib/database.whlib";

LOADLIB "mod::system/lib/whfs.whlib";

LOADLIB "mod::tollium/lib/screenbase.whlib";
LOADLIB "mod::tollium/lib/yamlcomponents.whlib";

RECORD FUNCTION GetRecycledObjectInfo(OBJECT whuser, INTEGER objectid)
{
  RECORD res;

  RECORD historyinfo := SELECT * FROM system.fs_history WHERE fs_history.type = 0 AND fs_history.fs_object = objectid ORDER BY when DESC;
  IF(NOT RecordExists(historyinfo))
    RETURN DEFAULT RECORD;

   RETURN [ deletionuser := historyinfo.user
          , deletiondate := historyinfo.when
          , origlocation := historyinfo.currentparent
          , originalpath := (SELECT AS STRING whfspath FROM system.fs_objects WHERE id=historyinfo.currentparent)
          , origname :=     historyinfo.currentname
          ];
}

PUBLIC STATIC OBJECTTYPE FieldScope EXTEND YamlScope
<
  OBJECT editor;
  RECORD controller;

  MACRO NEW(OBJECT editor, RECORD controller)
  {
    this->editor := editor;
    this->controller := controller;
  }
  UPDATE PUBLIC OBJECT FUNCTION ResolveComponentRef(OBJECT forcomp, STRING name)
  {
    IF(name LIKE "$type.*")
    {
      STRING ARRAY toks := Tokenize(name, '.');
      OBJECT match := this->controller.fsinstance->GetComponent(ToSnakeCase(toks[1]));
      IF(NOT ObjectExists(match))
      {
        STRING ARRAY availablerefs := SELECT AS STRING ARRAY ToCamelCase(compname) FROM ToRecordArray(this->controller.fsinstance->GetComponents(),'compname');
        THROW NEW TolliumException(forcomp, `No such field '${toks[1]}' available in type '${this->controller.type->namespace}'. Available: ${Detokenize(availablerefs, ', ')}`);
      }
      IF(Length(toks) > 2)
        THROW NEW TolliumException(forcomp, `No subfield support yet`);
      RETURN match;
    }
    RETURN YamlsCope::ResolveComponentRef(forcomp, name);
  }
>;

// Base class for a property editor. Preparing to share common code with objectprops
PUBLIC STATIC OBJECTTYPE PublisherEditorBase EXTEND TolliumScreenBase
<
  /// Id of edited object, 0 for new or duplicate objects
  INTEGER itemid;

  // Metatabs config as received from describeMetaTabsForHS
  RECORD metatabsconfig;
  // Controllers for the metadata. For now we'll rely on compositions
  PUBLIC RECORD ARRAY metacontrollers;
  /// Whether to assume the user has write access (often used by editdocument into otherwise locked/hidden folders)
  BOOLEAN assumewriteaccess;
  /// Whether the edited object is in the recycle bin, and if so, what the original name etc was
  RECORD recyclebininfo;
  /// If the user only has read-only access to this file (fs_browse)
  BOOLEAN readonly;
  /// Whether the edited (or new) object is a folder
  BOOLEAN isfolder;
  /// Whether the object is a new object. It may already live in the user's autosave folder! (TODO EditProps may need to be careful when *actually* requesting props in the /webhare-private/ folder? or just not activate meta apply rules there)
  BOOLEAN isnew;
  /// Parent folder of the new/edited object
  OBJECT parentfolder;

  RECORD basesettings;

  BOOLEAN FUNCTION IsReadOnly(OBJECT fsobj)
  {
    RETURN (ObjectExists(fsobj) AND IsReadonlyWHFSSpace(fsobj->whfspath))
           OR (this->itemid != 0 AND NOT this->assumewriteaccess AND NOT this->tolliumuser->HasRightOn("system:fs_fullaccess", this->itemid));

  }

  MACRO RefreshReadonly(OBJECT fsobj)
  {
    BOOLEAN setreadonly := this->IsReadOnly(fsobj);
    this->readonly := setreadonly;
    this->OnReadonlyChange();
  }

  BOOLEAN FUNCTION CanWriteCurrentItem()
  {
    RETURN this->isnew OR (NOT RecordExists(this->recyclebininfo) AND this->tolliumuser->HasRightOn("system:fs_fullaccess", this->itemid));
  }

  MACRO OnReadonlyChange()
  {
    this->RefreshMetaTabsReadonly();
  }

  MACRO SetupBaseSettings()
  {
    this->basesettings := GetBaseProperties(this->contexts->applytester);
    IF(this->basesettings.seotabrequireright != "" AND NOT this->contexts->applytester->HasRequiredRight(this->contexts->user, this->basesettings.seotabrequireright))
    {
      this->basesettings.seotitle := FALSE;
      this->basesettings.seotab := FALSE;
    }

    ^title->visible := this->basesettings.title;
    ^description->visible := this->basesettings.description;
    ^keywords->visible := this->basesettings.keywords;

    //FIXME restore seotabrequireright. see objectprops.whlib
    BOOLEAN needstemplate := this->isfolder ? FALSE : this->contexts->applytester->IsTypeNeedsTemplate();
    //As these are published, only files that go through a webdesign can show something useful
    ^seotitle->visible := needstemplate AND this->basesettings.seotitle;

    //TODO generalize further so metadata.ts also managed SEO settings
    INSERT CELL[ type := ^seosettings->type
               , fsinstance := ^seosettings
               ] INTO this->metacontrollers AT END;
  }

  RECORD FUNCTION UpdatePinned()
  {
    //Do not honor 'pinned' for new objects... then it should apply only AFTER we finalize the dialog. Also ensures valid-whfsname validation still happens
    BOOLEAN pinned := (^pinobject->value OR RecordExists(this->recyclebininfo)) AND NOT this->isnew;
    BOOLEAN parentfullaccess := this->tolliumuser->HasRightOn("system:fs_fullaccess", this->parentfolder->id);

    ^name->enabled := NOT pinned AND parentfullaccess AND NOT this->readonly;

    RETURN CELL [pinned, parentfullaccess];
  }

  MACRO SetupEditBaseForExistingObject(INTEGER objectid)
  {
    this->__SetupEditBase(objectid, 01, FALSE, -1);
  }
  MACRO SetupEditBaseForNewObject(INTEGER parent, BOOLEAN isfolder, INTEGER type)
  {
    this->__SetupEditBase(0, parent, isfolder, type);
  }

  MACRO __SetupEditBase(INTEGER objectid, INTEGER parent, BOOLEAN isfolder, INTEGER type)
  {
    this->metatabsconfig := CallJS("@mod-publisher/lib/internal/siteprofiles/metatabs.ts#describeMetaTabsForHS", CELL[objectid, parent, isfolder, type] );
    this->contexts->applytester := RecordExists(this->metatabsconfig) ? GetApplyTesterForHSInfo(this->metatabsconfig.__hsinfo) : GetApplyTesterForObject(objectid); //fallback. test-whfs-history-v4 triggers it
    IF(NOT ObjectExists(this->contexts->applytester))
      THROW NEW TolliumException(this, "No applytester found for this object");

    this->parentfolder := this->contexts->applytester->objparent = 0 ? OpenWHFSRootObject() : OpenWHFSObject(this->contexts->applytester->objparent);
    this->itemid := objectid;
    this->SetupBaseSettings();
    this->isnew := objectid = 0 OR (RecordExists(this->metatabsconfig) AND this->metatabsconfig.is_new);
  }

  MACRO RefreshMetaTabsReadonly()
  {
    FOREVERY(RECORD controller FROM this->metacontrollers)
      controller.fsinstance->enabled := NOT this->readonly;

    BOOLEAN needstemplate := this->isfolder ? FALSE : this->contexts->applytester->IsTypeNeedsTemplate();
    //As these are published, only files that go through a webdesign can show something useful
    ^seotitle->visible := needstemplate AND this->basesettings.seotitle;
    ^title->enabled := NOT this->readonly AND ^title->visible;
    ^description->enabled := NOT this->readonly AND ^description->visible;
    ^keywords->enabled := NOT this->readonly AND ^keywords->visible;
    this->UpdatePinned();
  }

  MACRO LoadBaseData(OBJECT sourceobject)
  {
    this->recyclebininfo := IsRecycleBinWHFSPath(sourceobject->whfspath) ? GetRecycledObjectInfo(this->contexts->user, sourceobject->id) : DEFAULT RECORD;
    ^pinobject->value := sourceobject->ispinned AND NOT this->isnew;

    IF(this->isnew)
      ^name->value := "";
    ELSE IF(RecordExists(this->recyclebininfo))
      ^name->value := this->recyclebininfo.origname;
    ELSE
      ^name->value := sourceobject->name;

    FOREVERY (STRING field FROM ["title", "description", "keywords" ])
    {
      OBJECT fieldobj := GetMember(this, "^" || field);
      fieldobj->value := GetMember(sourceobject, field);
    }
  }

  MACRO LoadMetadata(OBJECT sourceobject)
  {
    FOREVERY(RECORD controller FROM this->metacontrollers)
    {
      controller.fsinstance->value := controller.type->GetInstanceData(sourceobject->id);
    }
  }

  MACRO SaveMetadata(INTEGER targetobject)
  {
    //FIXME ensure any specific validation rule *and* all valueConstraints are honored
    FOREVERY(RECORD controller FROM this->metacontrollers)
    {
      //broken fields (eg due to incompatible data/vaueConstraints) should be able to opt out, IsUpdateValue ?
      controller.type->SetInstanceData(targetobject, controller.fsinstance->GetUpdateValue());
    }

    RECORD updates;
    IF(^title->enabled AND ^title->visible)
      updates := CELL[...updates, title := ^title->value ];
    IF(^description->enabled AND ^description->visible)
      updates := CELL[...updates, description := ^description->value ];

    IF(RecordExists(updates))
      OpenWHFSObject(targetobject)->UpdateMetadata(updates);
  }

  BOOLEAN FUNCTION AddTypesTabs()
  {
    INTEGER insertpos := 1;
    IF(RecordExists(this->metatabsconfig))
      FOREVERY(RECORD type FROM this->metatabsconfig.types)
    {
      OBJECT fsinstance := this->CreateTolliumComponent("record");
      RECORD controller := CELL [ fsinstance, type := OpenWHFSType(type.namespace) ];
      fsinstance->yamlscope := NEW FieldScope(fsinstance, controller);
      INSERT controller INTO this->metacontrollers AT END;

      FOREVERY(RECORD section FROM type.sections)
      {
        //TODO restore tab merging - or have metatabsconfig calculate this for us?
        OBJECT newtab := ^maintabs->InsertTabAt(insertpos, GetTid(section.title));
        insertpos := insertpos + 1;
        FOREVERY(RECORD field FROM section.fields)
        {
          STRING title := GetTid(field.title);
          STRING layout := CellExists(field, 'layout') ? field.layout : "inline";
          IF(title != "" AND layout = "block") //put it in the H1 above.
          {
            OBJECT heading := this->CreateTolliumComponent("heading");
            heading->title := title;
            newtab->InsertComponentAfter(heading, DEFAULT OBJECT, TRUE);
          }

          OBJECT newcomp := CreateYamlComponent(this, field.component, [ composition := fsinstance
                                                                       , cellname := field.name
                                                                       , dirtylistener := ^dirtylistener
                                                                       , yamlscope := fsinstance->yamlscope
                                                                       ]);

          newcomp->title := layout = "inline" ? title : "";
          newcomp->errorlabel := title;
          newtab->layout := layout = "inline" ? "form" : "left";  //update tab's layout as that's what new lines will use
          newtab->InsertComponentAfter(newcomp, DEFAULT OBJECT, TRUE);
        }
      }
    }

    IF(ObjectExists(this->contexts->editdocumentapi)) //TODO it looks like *we* should manage the list of editcontentttypes
      FOREVERY(RECORD controller FROM this->metacontrollers)
        INSERT controller.type->namespace INTO this->contexts->editdocumentapi->editcontenttypes AT END;

    this->RefreshMetaTabsReadonly();
    RETURN RecordExists(this->metatabsconfig) AND Length(this->metatabsconfig.types) > 0;
  }

>;
