<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::regex.whlib";
LOADLIB "wh::internet/urls.whlib";
LOADLIB "wh::xml/xsd.whlib";

LOADLIB "mod::publisher/lib/shorturl.whlib";
LOADLIB "mod::publisher/lib/internal/tollium-helpers.whlib";

LOADLIB "mod::system/lib/resources.whlib";
LOADLIB "mod::system/lib/userrights.whlib";
LOADLIB "mod::system/lib/whfs.whlib";

LOADLIB "mod::tollium/lib/dialogs.whlib";
LOADLIB "mod::tollium/lib/screenbase.whlib";


RECORD FUNCTION GetShortURLAppSettings(STRING domain)
{
  RECORD settings := CELL[ urllisthook := ""
                         , urlpropsextension := ""
                         ];
  FOREVERY(RECORD setting FROM GetCustomModuleSettings("http://www.webhare.net/xmlns/system/moduledefinition", "shorturls"))
    IF(setting.node->GetAttribute("domain") = domain)
    {
      IF(setting.node->HasAttribute("urllisthook"))
        settings.urllisthook := MakeAbsoluteResourcePath(setting.resource, setting.node->GetAttribute("urllisthook"));
      IF(setting.node->HasAttribute("urlpropsextension"))
        settings.urlpropsextension := MakeAbsoluteResourcePath(setting.resource, setting.node->GetAttribute("urlpropsextension"));
    }

  RETURN settings;
}

PUBLIC STATIC OBJECTTYPE Main EXTEND TolliumScreenBase
<
  OBJECT rightsapi;
  STRING originaltitle;
  RECORD appsettings;

  MACRO NEW()
  {
    this->rightsapi := GetPrimaryWebhareUserApi();
  }

  MACRO Init(RECORD data)
  {
    this->originaltitle := this->frame->title;

    INTEGER opendomain := this->RunScreen("domains.xml#selectdomain", [ appjustopened := TRUE ]);
    this->OpenDomain(opendomain);
  }

  MACRO DoSelectDomain()
  {
    INTEGER opendomain := this->RunScreen("domains.xml#selectdomain", [ appjustopened := FALSE ]);
    IF(opendomain != 0)
      this->OpenDomain(opendomain);
    ELSE IF(NOT ObjectExists(OpenShortURLDomainById(this->contexts->^shortdomain->id))) //open domain deleted?
      this->tolliumresult := "cancel";
  }

  MACRO OpenDomain(INTEGER domain)
  {
    this->contexts->^shortdomain := OpenShortURLDomainById(domain);
    IF(NOT ObjectExists(this->contexts->^shortdomain) OR NOT this->contexts->user->HasRightOn("publisher:manageshorturls", domain))
    {
      this->tolliumresult := "cancel";
      RETURN;
    }

    this->frame->title := this->contexts->^shortdomain->domain || " - " || this->originaltitle;
    ^shorturls->eventmasks := ["system:whfs.folder." || this->contexts->^shortdomain->id];
    ^shorturls->Invalidate(); //ensure it's reloaded as #selectdomain above can trigger an initial(empty) load

    this->appsettings := GetShortURLAppSettings(this->contexts->^shortdomain->domain);
    IF(this->appsettings.urllisthook != "")
      MakeFunctionPtr(this->appsettings.urllisthook)(^shorturls);
  }

  MACRO OnView(OBJECT browser)
  {
    browser->SendURL(^shorturls->selection.shorturl);
  }

  MACRO DoSearch()
  {
    ^shorturls->Invalidate();
  }

  MACRO DoClear()
  {
    ^searchfor->value := "";
    this->DoSearch();
  }

  MACRO OnUrlSelect()
  {
    IF(Length(^shorturls->selection) = 1)
    {
      ^previewbrowser->SetPreviewURL(this->contexts->^shortdomain->baseurl || ^shorturls->selection[0].shortname);
    }
    ELSE
    {
      ^previewbrowser->SetPreviewURL("");
    }
  }

  RECORD ARRAY FUNCTION OnGetURLs()
  {
    IF(NOT ObjectExists(this->contexts->^shortdomain))
      RETURN RECORD[];

    RECORD ARRAY rows := SELECT *, rowkey := id FROM this->contexts->^shortdomain->ListShortURLs();
    IF( ^searchfor->value != "" )
    {
      STRING searchterm := "*" || ToUpperCase(^searchfor->value) || "*";
      rows := SELECT * FROM rows WHERE ToUpperCase(shorturl) LIKE searchterm OR ToUpperCase(targeturl) LIKE searchterm;
    }

    RETURN (SELECT *, modifiedby := modifiedby != 0 ? this->rightsapi->GetUserDisplayName(modifiedby) : "" FROM rows);
  }

  MACRO DoDeleteUrl()
  {
    IF(this->RunSimpleScreen("confirm", this->GetTid(".suredeleteurls", ToString(Length(^shorturls->selection)))) != "yes")
      RETURN;

    OBJECT work := this->BeginUnvalidatedWork();
    FOREVERY( RECORD item FROM ^shorturls->selection )
      OpenWHFSObject(item.rowkey)->RecycleSelf();
    work->Finish();
  }

  MACRO DoEditUrl()
  {
    this->RunScreen("#urlprops", CELL[ id := ^shorturls->value[0]
                                     , this->appsettings.urlpropsextension
                                     ]);
  }

  MACRO DoAddUrl()
  {
    INTEGER newurl := this->RunScreen("#urlprops",
      CELL[ id := 0
          , this->appsettings.urlpropsextension
          ]);

    IF(newurl != 0)
    {
      this->frame->focused := ^shorturls;
      ^shorturls->value := [newurl];
    }
  }

  MACRO DoExportAll()
  {
    this->RunExport(^shorturls->rows);
  }
  MACRO DoExportSelection()
  {
    this->RunExport(^shorturls->selection);
  }

  MACRO RunExport(RECORD ARRAY rows)
  {
    RECORD ARRAY columns :=
      [ [ title := this->GetTid(".shorturl"), name := "shorturl", type := "text" ]
      , [ title := this->GetTid(".targeturl"), name := "targeturl", type := "text" ]
      ];

    RunColumnFileExportDialog(this, CELL[ columns
                                        , rows := (SELECT * FROM rows ORDER BY targeturl)
                                        , exporttitle := this->GetTid(".shorturlexport", this->contexts->^shortdomain->domain)
                                        ]);
  }

  MACRO DoBulkcreate()
  {
    INTEGER ARRAY toselect := this->RunScreen("#bulkcreate");
    IF(Length(toselect) > 0)
      ^shorturls->value := toselect;
  }
>;

PUBLIC STATIC OBJECTTYPE URLProps EXTEND TolliumScreenBase
<
  INTEGER urlid;

  MACRO Init(RECORD data)
  {
    ^shortname->prefix := this->contexts->^shortdomain->baseurl;
    ^tabs->extendcomponents := [ [ name := "fsobject", component := ^fsobject ] ];
    ^tabs->insertpoints := [ [ name := "extensionpoint", component := ^extensionpoint ] ];
    IF(data.urlpropsextension != "")
      ^tabs->LoadTabsExtension(data.urlpropsextension);

    ^fsobject->Load(data.id);

    IF(data.id != 0)
    {
      RECORD file := this->contexts->^shortdomain->GetShortUrl(data.id);
      this->urlid := data.id;
      ^target->value := file.target;
      ^shortname->value := file.shortname;
      ^shortname->required := TRUE;
      ^description->value := file.description;
      ^expirationdate->value := file.expirationdate;

      SetWHFSObjectLastModifiedField(^modified, OpenWHFSObject(data.id));
    }
    ELSE
    {
      //we only show the placeholder if we're not going to require the field
      ^shortname->placeholder := this->GetTid(".shortname-placeholder");
    }

    ^tabs->RunExtensionsPostInit();

    IF(^tabs->GetNumVisiblePages() <= 1)
      ^tabs->type := "server";
  }

  INTEGER FUNCTION Submit()
  {
    OBJECT fb := this->BeginFeedback();
    IF(^shortname->value != "" AND NOT IsValidWHFSName(^shortname->value,FALSE))
      fb->AddErrorFor(^shortname, this->GetTid(".invalidshortname"));
    IF(^expirationdate->value != DEFAULT DATETIME AND ^expirationdate->value <= GetCurrentDatetime() )
      fb->AddErrorFor(^expirationdate, this->GetTid(".invaliddate"));

    STRING baddomain := this->contexts->^shortdomain->__GetBadDomain(^target->value);
    IF(baddomain != "")
      fb->AddErrorFor(^target, this->GetTid(".unacceptabledomain", baddomain));

    IF(NOT fb->Finish())
      RETURN 0; //some basic constraint failed

    IF(this->urlid = 0) //creating a NEW link
    {
      RECORD existing := this->contexts->^shortdomain->LookupByTarget(^target->value);
      IF(RecordExists(existing))
      {
        IF(this->RunSimpleScreen("confirm", this->GetTid(".warnexistingshortlink", existing.shorturl)) != "yes")
          RETURN 0;
      }
    }

    OBJECT work := this->BeginWork();
    IF(^shortname->value != "")
    {
      RECORD existing := this->contexts->^shortdomain->GetShortURLByName(^shortname->value);
      IF(RecordExists(existing) AND existing.id != this->urlid)
        work->AddErrorFor(^shortname, this->GetTid(".shortnamealreadyexists"));
    }

    RECORD newurl;
    IF(NOT work->HasFailed())
    {
      RECORD setmetadata := CELL[ shortname := ^shortname->value
                                , description := ^description->value
                                , expirationdate := ^expirationdate->value
                                ];

      IF(this->urlid = 0)
      {
        newurl := this->contexts->^shortdomain->CreateShortUrl(
          ^target->value, CELL[ ...setmetadata, forcenew := TRUE ]);
      }
      ELSE
      {
        this->contexts->^shortdomain->UpdateShortUrl(this->urlid,
           CELL[ ...setmetadata, target := ^target->value ]);
      }

      ^fsobject->Store(work, [ fsobjectid := RecordExists(newurl) ? newurl.id : this->urlid ]);
    }

    IF(NOT work->HasFailed())
      ^tabs->SubmitExtensions(work);

    RETURN work->Finish() ? this->urlid != 0 ? this->urlid : newurl.id : 0;
  }
>;

PUBLIC STATIC OBJECTTYPE BulkCreate EXTEND TolliumScreenBase
<
  RECORD ARRAY FUNCTION ParseUrls()
  {
    //Cleanup and convert tabs/newlines ea to single spaces and create url list
    STRING importstr := TrimWhiteSpace(NEW RegEx("[,;\t\n\r]+","g")->Replace(^importstr->value," "));
    IF( importstr = "" )
      RETURN DEFAULT RECORD ARRAY;

    STRING ARRAY urls := Tokenize(importstr," ");

    //Validate urls and keep import order
    RETURN SELECT target, isvalid := IsValidURL(target) FROM ToRecordArray( urls, "target") GROUP BY target ORDER BY SearchElement(urls,target);
  }

  MACRO DoImport()
  {
    OBJECT work := this->BeginWork();

    INTEGER ARRAY newurls;
    FOREVERY(STRING url FROM ParseXSList(^importstr->value))
    {
      IF(url = "")
        CONTINUE;

      TRY
      {
        INSERT this->contexts->^shortdomain->CreateExternalShortUrl(url).id INTO newurls AT END;
      }
      CATCH(OBJECT e) //TODO translation
      {
        work->AddError(`${url} - ${e->what}`);
      }
    }

    IF(Length(newurls) = 0 AND NOT work->HasFailed())
      work->AddErrorFor(^importstr, this->GetTid(".nourlstoimport"));

    IF(work->Finish())
      this->__SetScreenResult(newurls);
  }
>;
