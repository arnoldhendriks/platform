<?wh

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/logging.whlib";
LOADLIB "mod::system/lib/whfs.whlib";

LOADLIB "mod::tollium/lib/dialogs.whlib";
LOADLIB "mod::tollium/lib/screenbase.whlib";
LOADLIB "mod::tollium/lib/internal/support.whlib";

BOOLEAN FUNCTION IsInSlotsFolder(INTEGER fsobj)
{
  INTEGER ARRAY foldertree := OpenWHFSObject(fsobj)->GetWHFSTree();
  RETURN RecordExists(SELECT FROM system.fs_objects WHERE id IN foldertree AND type = OpenWHFSType("http://www.webhare.net/xmlns/publisher/contentlibraries/slots")->id);
}

PUBLIC STATIC OBJECTTYPE Editor EXTEND TolliumScreenBase
<
  STRING whfstype;
  OBJECT editextension;

  PUBLIC OBJECT component; //TODO can we remove this too ?

  MACRO Init(RECORD data)
  {
    ^instance->basefsobject := data.fsbaseobject;
    this->whfstype := data.whfstype;

    ^instance->type := OpenWHFSType(data.whfstype);

    this->frame->title := GetTid(data.typedef.title);
    ^filetitle->value := data.currenttitle;

    ^extensiontabs->extendcomponents := [ [ name := "contentdata", component := ^instance ]
                                        , [ name := "filetitle", component := ^filetitle ]
                                        , [ name := "widgetsettings", component := ^widgetsettings ] //TODO better name? isn't the widget data also the widget settings ?
                                        ];

    // We want to prevent crashing an application due to errors in one of the extensions because:
    // - A user may lose data upon a crash, so showing the error and then cancelling the editting is a much better user experience
    // - When developing extensions it lowers the amount of times you have to restart an application due to an error
    TRY
    {
      RECORD result := ^extensiontabs->LoadTabsExtension(data.typedef.editor.extension);
      this->editextension := result.fragment;
      IF (this->editextension NOT EXTENDSFROM TolliumTabsExtensionBase)
        THROW NEW TolliumException(result.components[0], "Widget editextension does not extend TolliumTabsExtensionBase");
    }
    CATCH(OBJECT err)
    {
      LogHarescriptException(err);
      RunExceptionReportDialog(this, err);

      // Don't offer an incomplete editor, just cancel this screen
      this->tolliumresult := "cancel";
      RETURN;
    }

    ^body->spacers := DEFAULT RECORD;
    ^extensiontabs->visible := TRUE;

    IF(data.isfilewidget AND ^extensiontabs->GetNumVisiblePages() >= 1)
    {
      //TODO this is a hack, and theoretically the first tab might not even be visible due to visibleon... should we offer a <insert> alternative for widgets? or even do title insertion ONLY for widgts working that way?
      ^filetitle->visible := TRUE;
      ^filetitle->required := ^filetitle->value != "" OR NOT RecordExists(data.value); //Require if set *or* new widget (don't require setting a title just to edit an existing widget)
      ^filetitle->RemoveFromParent();
      ^extensiontabs->pages[0]->InsertComponentBefore(^filetitle, DEFAULT OBJECT, TRUE);

      //TODO another hack. insert Conditions tab if we're inside a slots folder. not sure yet how to express this in site profile rules
      IF(IsInSlotsFolder(this->contexts->applytester->objparent))
        ^extensiontabs->LoadTabsExtension("mod::publisher/tolliumapps/filemanager/adaptivecontent.xml#slotwidgetextensions");
    }

    ^extensiontabs->type := ^extensiontabs->GetNumVisiblePages() > 1 ? "regular" : "server";
    IF(Length(^extensiontabs->pages) > 0 AND ^extensiontabs->pages[0]->title = "")
      ^extensiontabs->pages[0]->title := this->GetTid(".content");

    IF(^extensiontabs->IsResizeSuggested())
    {
      this->frame->savestatebasekey := `${this->frame->savestatebasekey}(${data.typedef.editor.extension})`;
      this->frame->allowresize := TRUE;
      this->frame->savestate := ["size"];
    }

    this->component := ^instance;

    ^instance->value := data.value;
    ^widgetsettings->value := data.widgetsettings;
    ^extensiontabs->RunExtensionsPostInit();
  }

  RECORD FUNCTION Submit()
  {
    OBJECT feedback := this->BeginFeedback(); //implies validation on ^extensiontabs
    ^extensiontabs->SubmitExtensions(feedback);
    IF(NOT feedback->Finish())
      RETURN DEFAULT RECORD;

    RETURN CELL[ title := ^filetitle->value
               , widgetsettings := ^widgetsettings->value
               , instance := CELL[ ...^instance->value
                                 , whfstype := this->whfstype
                                 ]
               ];
  }
>;
