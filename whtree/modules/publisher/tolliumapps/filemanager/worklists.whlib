<?wh

LOADLIB "wh::util/algorithms.whlib";

LOADLIB "mod::publisher/lib/hooks.whlib";
LOADLIB "mod::publisher/lib/worklists.whlib";
LOADLIB "mod::publisher/lib/internal/dbschema.whlib";

LOADLIB "mod::system/lib/dialogs-userrights.whlib";
LOADLIB "mod::system/lib/services.whlib";

LOADLIB "mod::tollium/lib/screenbase.whlib";


RECORD FUNCTION EnrichWorklistObject(RECORD inrec)
{
  RECORD descr;

  IF(inrec.type != "")
  { //try to get the type, but don't crash the interface if we can't derive it
    TRY descr := DescribeWorklistType(inrec.type);
    CATCH(OBJECT e) LogHarescriptException(e);
  }
  RETURN CELL[ ...inrec
             , filterscreen := RecordExists(descr) ? descr.filterscreen : ""
             , filtersettings := [ worklist := inrec.custom_listid ]
             ];
}

PUBLIC STATIC OBJECTTYPE WorkListsContents EXTEND ContentsHandlerBase
< //----------------------------------------------------------------------------
  //
  // Variables
  //

  OBJECT api;
  RECORD ARRAY worklistcache;


  //----------------------------------------------------------------------------
  //
  // Initialization
  //

  MACRO NEW()
  {
    this->api := NEW WorkListAPI(this->tolliumuser);
    this->SyncWorkLists();
  }


  //----------------------------------------------------------------------------
  //
  // Updates functions
  //

  UPDATE PUBLIC BOOLEAN FUNCTION IsVisible()
  {
    // We need to resync or as sysop we might miss the removal of lists (probably need a smarter cache stragegy, this change might have made it net-worse than no caching at all)
    this->SyncWorkLists();

    // GetAllWorkLists returns all accessible work lists
    RETURN this->tolliumuser->HasRight("system:sysop") OR RecordExists(SELECT FROM this->worklistcache);
  }

  UPDATE PUBLIC RECORD ARRAY FUNCTION GetAddedColumns()
  {
    RETURN
        [ [ name := "custom_comments"
          , title := GetTid("publisher:filemanager.worklists.columns.custom_comments")
          , type := "text"
          ]
        , [ name := "custom_resolveddate"
          , sortkeyname := "custom_resolveddate_sort"
          , title := GetTid("publisher:filemanager.worklists.columns.custom_resolveddate")
          , type := "text"
          , align := "right"
          ]
        , [ name := "custom_statusicons"
          , title := GetTid("publisher:filemanager.worklists.columns.custom_statusicons")
          , type := "icons"
          , align := "right"
          ]
        ];
  }

  UPDATE PUBLIC RECORD ARRAY FUNCTION GetOneRowLayout()
  {
    RETURN
        [ [ name := "name",                width := "1pr" ]
        , [ name := "custom_comments",     width := "1pr" ]
        , [ name := "custom_resolveddate", width := "13x" ]
        , [ name := "custom_statusicons",  width := "2x" ]
        ];
  }

  UPDATE PUBLIC RECORD FUNCTION GetMultiRowLayout()
  {
    RETURN
        [ headers :=
            [ [ name := "icon",                combinewithnext := TRUE,  width := "" ]
            , [ name := "name",                combinewithnext := FALSE, width := "" ]
            , [ name := "custom_resolveddate", combinewithnext := FALSE, width := "13x" ]
            ]
        , rows :=
            [ [ cells :=
                [ [ name := "icon", rowspan := 2 ]
                , [ name := "name_noicon" ]
                , [ name := "custom_resolveddate" ]
                ]
              ]
            , [ cells :=
                [ [ name := "custom_comments"]
                , [ name := "custom_statusicons"]
                ]
              ]
            ]
        ];
  }

  UPDATE PUBLIC STRING ARRAY FUNCTION GetEventMasks()
  {
    // A mask for events emitted when editing work lists
    RETURN [ "publisher:worklist.*" ];
  }

  UPDATE PUBLIC STRING ARRAY FUNCTION GetAddedFlags()
  {
    // Flags to control availability of our actions
    RETURN [ "custom_canmanageusers", "custom_canresolve", "custom_cancleanup" ];
  }

  UPDATE PUBLIC INTEGER ARRAY FUNCTION OnGetPath(INTEGER childid)
  {
    // If the root id is requested, return just that
    IF (childid = this->rootid)
      RETURN INTEGER[ this->rootid ];

    // Check if one of the work list id's was requested
    //ADDME: This may not return the desired work list though, as list ids (which are requested here) are independent from
    //       work list ids
    this->SyncWorkLists();
    IF (RecordExists(SELECT FROM this->worklistcache WHERE id = childid))
      RETURN INTEGER[ this->rootid, childid ];

    // Not the root or a known work list (work list items have WHFS ids and will be shown within the publisher root)
    RETURN INTEGER[];
  }

  UPDATE PUBLIC BOOLEAN FUNCTION HasChildren(INTEGER parentid, RECORD options)
  {
    // Only the root can have children
    IF (parentid != this->rootid)
      RETURN FALSE;

    // Return if there are any (accessible) work lists
    this->SyncWorkLists();
    RETURN RecordExists(this->worklistcache);
  }

  UPDATE PUBLIC RECORD ARRAY FUNCTION OnGetChildren(INTEGER parentid, RECORD options)
  {
    // Check if the children for a folder tree item are requested (or for the file list)
    IF (options.tree)
    {
      // Only the root can have children
      IF (parentid != this->rootid)
        RETURN RECORD[];

      // Return the (accessible) work lists
      this->SyncWorkLists();

      RECORD ARRAY worklists := SELECT AS RECORD ARRAY EnrichWorklistObject(worklist) FROM this->worklistcache AS worklist;
      RETURN worklists;
    }
    ELSE
    {
      // If the parent is the root, return the work lists
      IF (parentid = this->rootid)
      {
        this->SyncWorkLists();
        RETURN this->worklistcache;
      }

      INTEGER ARRAY objectids;
      BOOLEAN skipaccesscheck;
      IF(RecordExists(options.filterresults))
      {
        //Then we can just show those items  TODO save the custom_itemids !
        objectids := SELECT AS INTEGER ARRAY id FROM options.filterresults.showresults;
        IF(CellExists(options.filterresults,'skipaccesscheck'))
          skipaccesscheck := options.filterresults.skipaccesscheck;
      }
      ELSE
      {
        /* Return the selected work list's items that the user has write access to (this used to be fs_browse but you can't
           really fix anything with just read. if we ever need readble too, it should probably be a worklist level option) */
        objectids :=
            SELECT AS INTEGER ARRAY objectid
              FROM this->api->GetWorkListItems(this->GetWorkListId(parentid));
      }

      //TODO is there a way to move responsibility for this to the search result filtering too ?
      IF(NOT skipaccesscheck)
        objectids := this->tolliumuser->HasRightOnMultiple("system:fs_fullaccess", objectids);

      RETURN ToRecordArray(objectids,"id");
    }
  }

  UPDATE PUBLIC RECORD ARRAY FUNCTION OnMapItems(INTEGER parentid, RECORD ARRAY items)
  {
    this->SyncWorkLists();

    RECORD ARRAY enrichments;

    IF (parentid = this->rootid)
    {
      // These are the work lists themselves
      enrichments :=
          SELECT id
               , custom_listid
               , custom_itemid := 0
               , custom_comments := ""
               , custom_resolveddate := ""
               , custom_resolveddate_sort := DEFAULT DATETIME
               , custom_statusicons := INTEGER[]
               , custom_canmanageusers := iseditor
               , custom_canresolve := FALSE
               , custom_cancleanup := iseditor
            FROM this->worklistcache;
    }
    ELSE
    {
      // These are work list items
      INTEGER listid := this->GetWorkListId(parentid);
      enrichments :=
          SELECT id := objectid
               , custom_listid := listid
               , custom_itemid := id
               , custom_comments := comments
               , custom_resolveddate := this->tolliumuser->FormatRecentDateTime(resolveddate, TRUE)
               , custom_resolveddate_sort := resolveddate
               , custom_statusicons := resolveddate != DEFAULT DATETIME ? INTEGER[ this->GetListIcon("tollium:status/positive") ] : INTEGER[]
               , custom_canmanageusers := FALSE
               , custom_canresolve := resolveddate = DEFAULT DATETIME
               , custom_cancleanup := FALSE
            FROM this->api->GetWorkListItems(listid);
    }

    // Enrich the incoming items
    items := JoinArrays(items, "id", enrichments,
        [ custom_listid := 0
        , custom_itemid := 0
        , custom_comments := ""
        , custom_resolveddate := ""
        , custom_resolveddate_sort := DEFAULT DATETIME
        , custom_statusicons := INTEGER[]
        , custom_canmanageusers := FALSE
        , custom_canresolve := FALSE
        , custom_cancleanup := FALSE
        ], [ rightouterjoin := TRUE ]);

    RETURN items;
  }

  UPDATE PUBLIC RECORD ARRAY FUNCTION GetActions()
  {
    RETURN
        [ [ title            := GetTid("publisher:filemanager.worklists.actions.manageworklist")
          , executehandler   := PTR this->AssignWorkListUsers
          , addtomenu        := "file"
          , addtocontextmenu := TRUE
          , allowmultiple    := FALSE
          , onlyfolders      := TRUE
          , checkflags       := [ "custom_canmanageusers" ]
          ]
        , [ title            := GetTid("publisher:filemanager.worklists.actions.resolveitem")
          , executehandler   := PTR this->ResolveWorklistItem
          , addtomenu        := "file"
          , addtocontextmenu := TRUE
          , allowmultiple    := TRUE
          , onlyfiles        := FALSE
          , checkflags       := [ "custom_canresolve" ]
          ]
        , [ title            := GetTid("publisher:filemanager.worklists.actions.cleanupresolved")
          , executehandler   := PTR this->CleanupResolvedItems
          , addtomenu        := "file"
          , addtocontextmenu := TRUE
          , allowmultiple    := FALSE
          , onlyfolders      := TRUE
          , checkflags       := [ "custom_cancleanup" ]
          ]
        ];
  }

  UPDATE PUBLIC BOOLEAN FUNCTION RunDeleteDialog(OBJECT parentscreen, RECORD ARRAY selection)
  {
    IF(parentscreen->RunSimpleScreen("confirm", GetTid("publisher:filemanager.worklists.messages.confirmdeleteworklists")) != "yes")
      RETURN FALSE;

    OBJECT work := parentscreen->BeginUnvalidatedWork();
    FOREVERY(RECORD list FROM selection)
      this->api->DeleteWorkList(list.custom_listid);

    RETURN work->Finish();
  }

  //----------------------------------------------------------------------------
  //
  // Action handlers
  //

  MACRO AssignWorkListUsers(OBJECT screen, RECORD data)
  {
    screen->RunScreen(Resolve("worklists.xml#worklistusers"), CELL[ worklistid := this->GetWorkListId(data.objectid) ]);
  }

  MACRO ResolveWorklistItem(OBJECT screen, RECORD data)
  {
    IF (screen->RunSimpleScreen("confirm", GetTid("publisher:filemanager.worklists.messages.confirmresolveitems", ToString(Length(data.objectids)))) = "yes")
    {
      OBJECT work := screen->BeginWork();
      this->api->ResolveItems(this->GetWorkListId(data.parentid), data.objectids);
      work->Finish();
    }
  }

  MACRO CleanupResolvedItems(OBJECT screen, RECORD data)
  {
    IF (screen->RunSimpleScreen("confirm", GetTid("publisher:filemanager.worklists.messages.confirmcleanupresolved")) = "yes")
    {
      OBJECT work := screen->BeginWork();
      this->api->PurgeWorklistItems(this->GetWorkListId(data.objectid));
      work->Finish();
    }
  }


  //----------------------------------------------------------------------------
  //
  // Helper functions
  //

  // The synced work list maintains the object id <-> work list id mapping
  MACRO SyncWorkLists()
  {
    RECORD ARRAY newworklistcache;
    FOREVERY (RECORD worklist FROM this->api->GetAllWorkLists())
    {
      RECORD item :=
          [ id := (SELECT AS INTEGER worklistcache.id FROM this->worklistcache WHERE custom_listid = worklist.id) ?? this->GetNextId()
          , custom_listid := worklist.id
          , contentmode := "searchresults" //enabled "open in publisher"
          , name := worklist.name
          , title := ""
          , isfolder := TRUE
          , modificationdate := worklist.creationdate
          , creationdate := worklist.creationdate
          , iseditor := worklist.iseditor
          , canwrite := FALSE
          , candelete := worklist.iseditor
          , custom_canresolve := FALSE
          , icon := "tollium:actions/checklist"
          , type := worklist.type
          ];
      INSERT item INTO newworklistcache AT END;
    }
    this->worklistcache := newworklistcache;
  }

  // Get the work list id from an object id
  INTEGER FUNCTION GetWorkListId(INTEGER objectid)
  {
    RETURN SELECT AS INTEGER COLUMN custom_listid FROM this->worklistcache WHERE id = objectid;
  }
>;

PUBLIC STATIC OBJECTTYPE WorkListUsers EXTEND TolliumScreenBase
< //----------------------------------------------------------------------------
  //
  // Variables
  //

  OBJECT api;
  INTEGER worklistid;


  //----------------------------------------------------------------------------
  //
  // Initialization
  //

  MACRO Init(RECORD data)
  {
    this->api := NEW WorkListAPI(this->tolliumuser);
    this->worklistid := data.worklistid;
    RECORD worklistinfo := SELECT * FROM publisher.worklists WHERE id = this->worklistid;

    ^users->value :=
        SELECT id := user->authobjectid
             , name := user->type = 1 ? (user->realname ?? user->login) : user->GetUncachedUserRightsName()
             , email := user->type = 1 ? user->emailaddress : user->GetUncachedUserRightsName()
             , obj := user
          FROM ToRecordArray(this->api->GetWorkListUsers(this->worklistid), "user");
    ^name->value := worklistinfo.name;
  }


  //----------------------------------------------------------------------------
  //
  // Callbacks
  //

  // This function is only called to add users
  RECORD ARRAY FUNCTION OnEditUser(RECORD _unused_row)
  {
    RECORD ARRAY result := RunUserRoleSelectDialog(this, [ selectmultiple := TRUE ]);
    //TODO we need a user+role enrichtment component, or perhaps we just need to be able to use a userrolelist ?
    RETURN
          SELECT TEMPORARY obj := type = "user" ? this->contexts->userapi->GetUser(entityid) : this->contexts->userapi->GetRole(entityid)
               , id := obj->authobjectid
               , name := type = "user" ? (obj->realname ?? obj->login) :  obj->GetUncachedUserRightsName()
               , email := type = "user" ? obj->emailaddress : ""
               , obj := obj
            FROM result;
  }

  BOOLEAN FUNCTION Submit()
  {
    OBJECT work := this->BeginWork();

    //TODO should there be an API for this? which also implies broadcastoncommit (but for now we just rely on 'addworklistusers' to do it..)
    UPDATE publisher.worklists SET name := ^name->value WHERE id = this->worklistid;

    RECORD ARRAY curusers :=
        SELECT id := user->authobjectid
             , obj := user
          FROM ToRecordArray(this->api->GetWorkListUsers(this->worklistid), "user");

    OBJECT ARRAY addusers;
    FOREVERY (RECORD user FROM ^users->value)
    {
      INTEGER idx := (SELECT AS INTEGER #curusers + 1 FROM curusers WHERE id = user.id) - 1;
      IF (idx < 0)
        INSERT user.obj INTO addusers AT END;
      ELSE
        DELETE FROM curusers AT idx;
    }

    this->api->AddWorkListUsers(this->worklistid, addusers);
    OBJECT ARRAY delusers := SELECT AS OBJECT ARRAY obj FROM curusers;
    this->api->RemoveWorkListUsers(this->worklistid, delusers);

    RETURN work->Finish();
  }
>;
