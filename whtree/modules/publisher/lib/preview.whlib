<?wh
/** @short Publisher (browser) previews
    @topic sitedev/whfs
*/

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::internet/urls.whlib";

LOADLIB "mod::publisher/lib/control.whlib";
LOADLIB "mod::publisher/lib/internal/feedback/feedbacksupport.whlib";

LOADLIB "mod::system/lib/internal/whconfig.whlib";
LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/services.whlib";
LOADLIB "mod::system/lib/webserver.whlib";
LOADLIB "mod::system/lib/webserver/configure.whlib";


/** @short Get a preview link for a page
    @param fsobject FS object to show (live version)
    @cell options.validuntil Validity of the link. Defaults to 1 day
    @cell options.password Password to protect the preview with
    @cell options.version Exact version to view (eg a draft).
    @return Preview url. An empty URL if this object cannot be previewed (the site has no output URL) */
PUBLIC STRING FUNCTION GetPreviewLink(INTEGER fsobject, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions( [ validuntil := DEFAULT DATETIME
                              , password := ""
                              , version := 0
                              ], options);

  IF(options.validuntil = DEFAULT DATETIME)
    options.validuntil := AddDaysToDate(1, GetCurrentDatetime());

  INTEGER showversion := options.version ?? fsobject;

  STRING baseurl := SELECT AS STRING objecturl FROM system.fs_objects WHERE id = fsobject;
  IF(baseurl = "")
    RETURN "";

  RECORD viewdata := [ id := showversion, c := (SELECT AS DATETIME creationdate FROM system.fs_objects WHERE id = showversion), v := options.validuntil, p := options.password ];
  STRING urldata := EncryptForThisServer("publisher:preview", viewdata);
  RETURN ResolveToAbsoluteURL(baseurl, "/.publisher/preview/" || urldata || "/");
}

/** @short Get a link that will show a specific A/B test variant
    @param navigationobject ID of the fsobject implementing the A/B test
    @param targetobject ID Of the target to show. If not a current target, returns a 404.
    @return Link that will select the requested target. Empty link if the navigationobject is not being published */
PUBLIC STRING FUNCTION GetABTestVariantLink(INTEGER navigationobject, INTEGER targetobject)
{
  STRING baselink := SELECT AS STRING link FROM system.fs_objects WHERE id = navigationobject;
  IF(baselink = "")
    RETURN "";

  RETURN UpdateURLVariables(baselink, [ "wh.variant" := EncryptForThisServer("publisher:abvariant", [ t := targetobject])]);
}

/** Describe the current preview request (for <preview browserurl> pages)
    @long Aborts if the current request is not a valid preview request
    @return A record describing this preview
    @cell(integer) return.targetobject ID of the (published) object we need to show
    @cell(integer) return.contentobject ID of the object from which we should get content (ie drafts and content links)
*/
PUBLIC RECORD FUNCTION DescribePreviewRequest()
{
  RECORD info := DecryptForThisServer("publisher:previewbrowser", GetWebVariable("preview"), [fallback := DEFAULT RECORD]);
  IF(NOT RecordExists(info))
    AbortWithHTTPError(400);

  //On development, this URL will be valid for 8 hours, but on live for only 15 seconds (TODO: we could signal our parent "hey, refresh us!" if we discover that the link is expired)
  INTEGER timeout := GetDtapStage() = "development" ? 8*60*60*1000 : 15000;
  IF(info.when < AddTimeToDate(-timeout, GetCurrentDatetime()))
    AbortWithHTTPError(403);

  RETURN CELL[ targetobject := info.source
             , contentobject := info.content
             , languagecode := info.lang
             ];
}

PUBLIC STRING FUNCTION GetViewURLWithFeedback(OBJECT file, OBJECT tolliumuser)
{
  STRING url := file->link ?? file->url;
  /* If a page has no design and doesn't end with a SHTML extension, assume a location.href-redirect done by view.shtml is unsafe (ie it will trigger a download and then view.shtml wouldn't complete. So don't try to preview.
     The previewbrowser and this API should probably use the same checks to determine whether to attempt a preview. And maybe even the preview button should just disable itself?
  */
  IF(NOT file->isfolder AND NOT TestFlagFromPublished(file->published, PublishedFlag_HasWebDesign) AND file->url NOT LIKE "*/" AND ToUppercase(file->url) NOT LIKE "*.SHTML")
    RETURN url;

  STRING feedbacktoken := GetFeedbackWebToken(tolliumuser);
  IF(feedbacktoken = "") //eg anonymous/override user
    RETURN url;

  //Do we host this URL ?
  IF(NOT RecordExists(GetWebserverForURL(url, [ allowfailure := TRUE ])))
    RETURN url;

  RETURN ResolveToAbsoluteURL(url, "/.wh/common/feedback/view.shtml?target=" || EncodeURL(UnpackURL(url).urlpath) || "&feedbacktoken=" || EncodeURL(feedbacktoken));
}

PUBLIC RECORD FUNCTION ParsePreviewURL(STRING url)
{
  STRING baseurl := Tokenize(Tokenize(url, '/')[5],'?')[0];
  RECORD data := DecryptForthisServer("publisher:preview",baseurl);
  BOOLEAN renderwidgetpreview := CellExists(data,'t') AND data.t = "w";
  RETURN [ id := data.id
         , creationdate := data.c
         , validuntil := data.v
         , password := data.p
         , renderwidgetpreview := renderwidgetpreview
         , instanceid := renderwidgetpreview AND CellExists(data,'i') ? data.i : 0
         ];
}
