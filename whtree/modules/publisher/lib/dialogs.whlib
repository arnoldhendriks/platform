<?wh
/** @topic publisher-apps/tollium
*/

LOADLIB "wh::datetime.whlib";

LOADLIB "mod::publisher/lib/internal/tollium-helpers.whlib";
LOADLIB "mod::publisher/lib/internal/siteprofiles/reader.whlib";
LOADLIB "mod::publisher/lib/internal/siteprofiles/widgets.whlib";
LOADLIB "mod::publisher/lib/internal/files.whlib";
LOADLIB "mod::publisher/tolliumapps/filemanager/objecteditor.whlib";
LOADLIB "mod::publisher/lib/internal/forms/results.whlib";

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/services.whlib";
LOADLIB "mod::system/lib/whfs.whlib";
LOADLIB "mod::system/lib/internal/whfs/base.whlib";
LOADLIB "mod::system/lib/internal/resourcemanager.whlib";

LOADLIB "mod::tollium/lib/dialogs.whlib";
LOADLIB "mod::tollium/lib/gettid.whlib";


/** Run the tag manager
    @param parentscreen Screen invoking us
    @param tagfolder Tag folder
    @param options Options
*/
PUBLIC MACRO RunManageTagsDialog(OBJECT parentscreen, INTEGER tagfolder, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions(CELL[], options);
  parentscreen->RunScreen("mod::publisher/tolliumapps/tagmanager/tagmanager.xml#tagmanager", [ tagfolder := tagfolder ]);
}

/** Run URL history dialog
    @param parentscreen Screen invoking us
    @param objectid Object for which we want to manage the history
    @param options Options
    @cell(boolean) options.skipaccesscheck Do not verify whether the user has write access to the location
*/
PUBLIC MACRO RunFSObjectHistoryDialog(OBJECT parentscreen, INTEGER objectid, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions(CELL[ skipaccesscheck := FALSE ], options);
  parentscreen->RunScreen("mod::publisher/screens/commondialogs/urlhistory.xml#urlhistory", CELL[ id := objectid, options.skipaccesscheck ]);
}


/** Run the 'browse for object' dialog, returning a single object
    @param parentscreen Screen invoking us
    @cell(integer array) options.roots Root folders to show. If empty, shows all root folders
    @cell(boolean) options.acceptfiles Accept files. (defaults to TRUE)
    @cell(boolean) options.acceptfolders Accept folders. (defaults to TRUE)
    @cell(boolean) options.acceptforeignfolders Accept selection of foreign folders (defaults to FALSE)
    @cell(boolean) options.hideviewtype Remove the view type (list/thumbnails selector) control
    @cell(string array) options.accepttypes List of acceptable WHFS types for selection
    @cell(integer) options.recyclebinid ID for the recyclebin. If not set, no recyclebin will be available in the browse dialog
    @cell(integer) options.value Currently selected item
    @cell(integer) options.folder Folder to preselect, if no value is given. If 0, preselect based on last selected folder. If -1, select the root
    @return Selected object, or 0 if the dialog was cancelled */
PUBLIC INTEGER FUNCTION RunBrowseForFSObjectDialog(OBJECT parentscreen, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  OBJECT dialog := parentscreen->LoadScreen('mod::publisher/tolliumapps/dialogs/browseforobject.xml#browseforobject', CELL[...options, __selectmode := "single"]);
  IF (dialog->RunModal() = "ok")
    RETURN dialog->value;

  RETURN 0;
}

/** Run the 'browse for object' dialog, returning multiple objects
    @param parentscreen Screen invoking us
    @param options @includecelldef %RunBrowseForFSObjectDialog.options
    @cell(integer array) options.value Currently selected items
    @return Selected objects or an empty array if the dialog was cancelled */
PUBLIC INTEGER ARRAY FUNCTION RunBrowseForFSObjectsDialog(OBJECT parentscreen, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  OBJECT dialog := parentscreen->LoadScreen('mod::publisher/tolliumapps/dialogs/browseforobject.xml#browseforobject', CELL[...options, __selectmode := "multiple"]);
  IF (dialog->RunModal() = "ok")
    RETURN dialog->value;

  RETURN DEFAULT INTEGER ARRAY;
}

/** Run the 'select filetype/foldertype/template dialog'
    @long This function invokes the standard select type for new file/folder dialog, but doesn't run the actual creation steps
    @param parentscreen Screen invoking us
    @param parentfolder Folder which will receive the new object
    @cell(boolean) options.isfolder If true, create a folder
    @cell(integer array) options.limittypes If set, allow only templates and types with one of these IDs to be added
    @return A record describing the creation request, or a DEFAULT RECORD if the dialog was cancelled
    @cell(integer) return.createtype FS type ID to create
    @cell(integer) return.template FS object ID to duplicate
    ID of the newly generated fsobject, 0 if creation was cancelled
*/
PUBLIC RECORD FUNCTION RunSelectNewFSObjectTypeDialog(OBJECT parentscreen, INTEGER parentfolder, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  //We will require 'isfolder' for now, but may drop that in the future if we're not going to require either
  options := ValidateOptions([ isfolder := FALSE
                             , limittypes := INTEGER[]
                             , __v2 := FALSE
                             ], options, [ required := [ "isfolder" ]
                                         , optional := [ "limittypes" ]
                                         ]);

  IF(CellExists(options,'limittypes') AND Length(options.limittypes) = 0)
    RETURN DEFAULT RECORD;

  IF(options.__v2)
    RETURN parentscreen->RunScreen("mod::publisher/tolliumapps/filemanager/types.xml#selecttype", CELL[ ...options, parentfolder, DELETE isfolder, DELETE __v2 ]);

  OBJECT typesdialog := parentscreen->LoadScreen("publisher:filemgrdialogs.selecttype",
      [ parent := parentfolder
      , foldertypes := options.isfolder
      , limittypes := CellExists(options, 'limittypes') ? options.limittypes : INTEGER[]
      ]);

  IF (typesdialog->RunModal() != "ok")
    RETURN DEFAULT RECORD;

  RETURN [ createtype := typesdialog->selectedtype
         , template := typesdialog->selectedtemplate
         ];
}

/** Run the 'create new object' flow
    @long This function invokes the standard 'new object' flow (which currently starts with a type selection and then runs the objectproperties dialog)
    @param parentscreen Screen invoking us
    @param parentfolder Folder which will receive the new object
    @param options @includecelldef %RunSelectNewFSObjectTypeDialog.options
    @cell(integer) options.template If not 0, duplicate this object. (isfolder and type options are ignored)
    @cell(boolean) options.issite If true, create a site
    @cell(integer) options.type Type to create. If not set, this flow may launch a 'select type' dialog
    @return ID of the newly generated fsobject, 0 if creation was cancelled
*/
PUBLIC INTEGER FUNCTION RunNewFSObjectDialog(OBJECT parentscreen, INTEGER parentfolder, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  //TODO should we fail or ignore if 'isfolder' is inconsistent with 'type' ?

  //We will require 'isfolder' for now, but may drop that in the future if we're not going to require either
  options := ValidateOptions([ isfolder := FALSE
                             , issite := FALSE
                             , type := 0
                             , template := 0
                             , __v2 := FALSE
                             ], options, [ optional := [ "type" ], required := [ "isfolder" ] ]);

  IF(options.issite AND NOT options.isfolder)
    THROW NEW Exception("A site MUST be a folder");
  IF(options.issite AND options.template != 0) //are you duplicating a site, cloning a non-site folder as a site, or... ? need use case
    THROW NEW Exception("Site duplication is not supported");

  RECORD createres;

  IF(options.template != 0)
  {
    createres := [ template := options.template
                 , createtype := SELECT AS INTEGER type FROM system.fs_objects WHERE id = options.template
                 ];
  }
  ELSE IF(CellExists(options,'type'))
  {
    createres := [ template := 0
                 , createtype := options.type
                 ];
  }
  ELSE IF(NOT options.issite)
  {
    createres := RunSelectNewFSObjectTypeDialog(parentscreen, parentfolder, CELL[ options.__v2
                                                                                , options.isfolder
                                                                                ]);
    IF(NOT RecordExists(createres))
      RETURN 0;
  }

  IF(NOT options.issite AND createres.template = 0 AND createres.createtype != 0 AND options.isfolder = FALSE)
  {
    OBJECT applytester := GetApplyTesterForFakeObject(parentfolder,TRUE, createres.createtype);
    RECORD editorinfo := applytester->GetLastValueForCell("SETOBJECTEDITOR"); //TODO this is replicating the editor selection logic from RunFSObjectEditApplication, cleaner?
    IF(NOT RecordExists(editorinfo)) //if no custom editor was set...
    {
      STRING typens := SELECT AS STRING namespace FROM system.fs_types WHERE id = createres.createtype;
      RECORD typedef := LookupContentType(typens);
      IF(RecordExists(typedef) AND typedef.isembeddedobjecttype AND RecordExists(typedef.editor) AND typedef.editor.type = "extension")
      {
        //NEW widget... so go straight to the widget editor
        RECORD embobj := DoEditWidget(parentscreen, DEFAULT OBJECT, typens, DEFAULT RECORD, DEFAULT OBJECT, applytester, FALSE, TRUE, "", DEFAULT RECORD);
        IF(NOT RecordExists(embobj)) // was the data editted? ('Cancel' returns a DEFAULT RECORD)
          RETURN 0;

        GetPrimary()->BeginWork();
        OBJECt parentobj := OpenWHFSObject(parentfolder);
        OBJECT newfile := parentobj->CreateFile(
          [ name := parentobj->GenerateUniqueName(embobj.title)
          , title := embobj.title
          , typens := embobj.instance.whfstype
          ]);

        //FIXME: verify that file is still available, writeable atc - although we actually -should-
        //       be given such notifications in __editEmbeddedObject's SUBMIT handler.
        newfile->SetInstanceData(embobj.instance.whfstype, embobj.instance);
        newfile->SetInstanceData("http://www.webhare.net/xmlns/publisher/contentlibraries/slottedwidget", embobj.widgetsettings);

        GetPrimary()->CommitWork();
        RETURN newfile->id;
      }
    }
  }

  //only path where createres can be a DEFAULT RECORD is where options.issite = TRUE
  RETURN __RunHistoryPropsDialog(parentscreen,
      [ why := options.issite ? "newsite" : createres.template != 0 ? "duplicate" : options.isfolder ? "newfolder" : "newfile"
      , parentfolder := parentfolder
      , type := options.issite ? 0 : createres.createtype
      , fsobj := options.issite ? 0 : createres.template
      ], options.issite ? "site"
                        : "objectprops");
}

/** Run the 'delete object' flow
    @param parentscreen Screen invoking us
    @param fsobjects List of fsobjects to delete
    @cell options.checkpins Prevent deletion of pinned objects. Defaults to TRUE
    @cell options.confirm Request confirmation. Defaults to TRUE
    @return True if the deletion succeeded
*/
PUBLIC BOOLEAN FUNCTION RunDeleteFSObjectsDialog(OBJECT parentscreen, INTEGER ARRAY fsobjects, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions([ confirm := TRUE
                             , checkpins := TRUE
                             ], options);

  // Filter out all already deleted objects
  RECORD ARRAY todelete := SELECT id
                                , name
                                , isfolder
                                , inrecyclebin := IsRecycleBinWHFSPath(whfspath)
                                , parentsite
                                , published
                                , ispinned
                             FROM system.fs_objects
                            WHERE id IN fsobjects;

  IF (Length(todelete) = 0)
    RETURN FALSE;

  IF (options.checkpins AND RecordExists(SELECT FROM todelete WHERE ispinned))
  {
    STRING msg := Length(todelete) = 1 ? GetTid("publisher:filemanager.main.messageboxes.cannotdeletepinned")
                                       : GetTid("publisher:filemanager.main.messageboxes.cannotdeletesomepinned");
    parentscreen->RunSimpleScreen("error", msg);
    RETURN FALSE;
  }

  // First, check if we are deleting any sites (we have to do this first, because we cannot ask for user confirmation
  // while having a work open
  RECORD ARRAY analysis := AnalyzeContents(SELECT AS INTEGER ARRAY ID FROM todelete);
  IF(RecordExists(SELECT FROM analysis WHERE errorcode = "CONTAINSSITEOUTPUT"))
  {
    //The deletion cannot be carried out. Currently, this implies that any of the folders contain a site
    STRING  sitenames := Detokenize((SELECT AS STRING ARRAY DISTINCT errordata
                                            FROM analysis
                                           WHERE errorcode="CONTAINSSITEOUTPUT"
                                                 AND errordata != ""), "\n- ");
    IF(Length(sitenames)>0)
    {
      parentscreen->RunSimpleScreen("error", GetTid("publisher:commondialogs.messageboxes.failrecyclefortheseoutputsites", "- " || sitenames || "\n"));
    }
    ELSE
    {
      parentscreen->RunSimpleScreen("error", GetTid("publisher:commondialogs.messageboxes.failrecycleforotheroutputsites"));
    }
    RETURN FALSE;
  }

  IF(options.confirm)
  {
    IF(RecordExists(SELECT FROM analysis WHERE errorcode = "CONTAINSSITEROOT"))
    {
      STRING deletesitenames := Detokenize((SELECT AS STRING ARRAY DISTINCT errordata
                                              FROM analysis
                                             WHERE errorcode="CONTAINSSITEROOT"
                                                   AND errordata != ""), "\n- ");
      STRING res;
      IF(parentscreen->RunSimpleScreen("confirm", GetTid("publisher:commondialogs.messageboxes.recyclesitesconfirm", "- " || deletesitenames || "\n")) != "yes")
        RETURN FALSE;
    }
    ELSE
    {
      STRING filename;
      STRING foldername;
      BOOLEAN anyactive := RecordExists(SELECT FROM todelete WHERE NOT inrecyclebin);
      IF(Length(todelete)=1)
      {
        filename := todelete[0].isfolder ? "" : todelete[0].name;
        foldername := todelete[0].isfolder ? todelete[0].name : "";
      }

      IF(anyactive)
      {
        IF (parentscreen->RunSimpleScreen("confirm", GetTid("publisher:commondialogs.messageboxes.movetotrashconfirm", ToString(Length(todelete)), filename, foldername)) != "yes")
          RETURN FALSE;
      }
      ELSE
      {
        IF (parentscreen->RunSimpleScreen("confirm", GetTid("publisher:commondialogs.messageboxes.deleteconfirm", ToString(Length(todelete)), filename, foldername)) != "yes")
          RETURN FALSE;
      }
    }
  }

  OBJECT work := parentscreen->BeginUnvalidatedWork();

  INTEGER ARRAY totrash := SELECT AS INTEGER ARRAY id FROM todelete WHERE NOT inrecyclebin;
  INTEGER ARRAY tokill := SELECT AS INTEGER ARRAY id FROM todelete WHERE inrecyclebin;

  IF(Length(totrash)>0)
    RunAnyDelete(totrash, "publisher:app");

  IF(Length(tokill)>0)
    DELETE FROM system.fs_objects WHERE id IN tokill;

  RETURN work->Finish();

}

/** Run the 'inspect object' dialog
    @param parentscreen Screen invoking us
    @param objectid Object to inpsect */
PUBLIC MACRO RunFSObjectInspectDialog(OBJECT parentscreen, INTEGER objectid, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions(CELL[], options);

  IF(NOT parentscreen->contexts->user->HasRight("system:supervisor"))
    THROW NEW Exception("Non-supervisors may not access the Inspect dialog");

  parentscreen->RunScreen("mod::publisher/tolliumapps/filemanager/inspect.xml#inspect", [ fsobj := objectid ]);
}


/** Run the 'edit object properties' dialog
    @param parentscreen Screen invoking us
    @param objectid Object to edit
    @cell(boolean) options.assumereadaccess Don't check for read access, just assume the user has it
    @cell(boolean) options.siteproperties Open to access site properties
*/
PUBLIC MACRO RunFSObjectPropertiesDialog(OBJECT parentscreen, INTEGER objectid, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions(CELL[ assumereadaccess := FALSE
                                 , siteproperties := FALSE
                                 ], options);

  IF (NOT options.assumereadaccess AND NOT parentscreen->contexts->user->HasRightOn("system:fs_browse", objectid))
  {
    STRING targetpath := SELECT AS STRING whfspath FROM system.fs_objects WHERE id = objectid;
    IF(NOT IsRecycleBinWHFSPath(targetpath) OR NOT IsDeletedObjectAccessible(objectid))
    {
      parentscreen->RunSimpleScreen("error", GetTid("publisher:commondialogs.messageboxes.norightstoviewfile"));
      RETURN;
    }
  }

  TRY
  {
    __RunHistoryPropsDialog(parentscreen, [ why := "edit", fsobj := objectid ], options.siteproperties ? "site" : "objectprops");
  }
  CATCH(OBJECT err)
  {
    // don't crash on any exception, we can just choose to not open the properties dialog
    // which is better than to crash the whole Publisher
    RunExceptionReportDialog(parentscreen, err);
  }
}

/** @short Launch the filemanager opening the specified file or folder
    @param parentscreen Screen invoking us
    @param objectid ID of object to focus in foldertree or filelist
    @cell(boolean) options.infilelist Prefer the selected object to be in the filelist
*/
PUBLIC MACRO RunPublisherFileManager(OBJECT parentscreen, INTEGER objectid, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions([ infilelist := FALSE ], options);

  RECORD objinfo := SELECT isfolder FROM system.fs_objects WHERE id = objectid;
  RECORD target;
  IF(RecordExists(objinfo))
  {
    target := objinfo.isfolder ? [ folderid := objectid ] : [ fileid := objectid ];
    IF(options.infilelist)
      INSERT CELL infilelist := TRUE INTO target;
  }

  parentscreen->contexts->controller->SendApplicationMessage("publisher:app", target, DEFAULT RECORD, FALSE);
}


/** @short Launch an editor for the specified file
           This can be an:
          - objecteditor (application screen
          - hardcoded inbuild publisher dialogs (for .tpl, forums, archives, int/ext/content-links and contentlistings)
          - embeddedobjecttype editfragment

    @param parentscreen Screen invoking us
    @param objectid Object to edit
    @cell(boolean) options.forceintegrated Do not launch a separate application
    @cell(boolean) options.assumereadaccess Assume the user has read access
    @cell(boolean) options.assumewriteaccess Assume the user has write access
    @cell(integer array) options.internallinkroots Limit RTE internal links to these subfolders
*/
PUBLIC MACRO RunFSObjectEditApplication(OBJECT parentscreen, INTEGER objectid, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions([ forceintegrated := FALSE
                             , assumereadaccess := FALSE
                             , assumewriteaccess := FALSE
                             , internallinkroots := INTEGER[]
                             ], options);

  OBJECT fileobj := OpenWHFSObject(objectid);
  IF(NOT ObjectExists(fileobj) OR fileobj->isfolder)
    RETURN;

  IF(options.assumewriteaccess)
    options.assumereadaccess := TRUE; //implied

  //INTEGER siteid := SELECT AS INTEGER parentsite FROM system.fs_objects WHERE id=objectid;
  OBJECT applytester := GetApplyTesterForObject(objectid);
  RECORD editorinfo := applytester->GetLastValueForCell("SETOBJECTEDITOR");
  RECORD objeditor;
  IF(RecordExists(editorinfo) AND editorinfo.name != "")
    objeditor := GetObjectEditor(editorinfo.name);

  BOOLEAN haswriteaccess;
  IF(NOT options.assumewriteaccess AND (NOT RecordExists(objeditor) OR NOT objeditor.supportsreadonly))
  {
    IF (NOT parentscreen->contexts->user->HasRightOn("system:fs_fullaccess", objectid))
    {
      parentscreen->RunMessageBox("publisher:commondialogs.norightstoeditfile");
      RETURN;
    }
    haswriteaccess := TRUE;
  }

  IF(NOT haswriteaccess AND NOT options.assumereadaccess AND (NOT RecordExists(objeditor) OR NOT objeditor.supportsnoaccess))
  {
    IF (NOT options.assumereadaccess AND NOT parentscreen->contexts->user->HasRightOn("system:fs_browse", objectid))
    {
      parentscreen->RunSimpleScreen("error", GetTid("publisher:commondialogs.messageboxes.norightstoviewfile"));
      RETURN;
    }
  }

  IF(options.forceintegrated = FALSE
     AND ((RecordExists(editorinfo) AND editorinfo.separateapp)
          OR (RecordExists(objeditor) AND objeditor.documenteditor != "")))
  {
    /* restart us as a separate application. note that applicationmessages currently go through the client so they are NOT
       a safe messaging channel.*/

    STRING appoptions;
    IF(options.assumereadaccess OR options.assumewriteaccess OR Length(options.internallinkroots) > 0)
    {
      appoptions := EncryptForThisServer("publisher:runfsobjecteditapplication",
                                          CELL[ //verification
                                                now := GetCurrentDatetime() //allow us to limit lifetime to help prevent replay attacks
                                              , objectid
                                              , creationdate := fileobj->creationdate
                                                // the optinos
                                              , options.assumereadaccess
                                              , options.assumewriteaccess
                                              , options.internallinkroots
                                              ]);
    }

    parentscreen->contexts->controller->SendApplicationMessage("publisher:edit", CELL[ id := objectid, appoptions ], DEFAULT RECORD, TRUE);
    RETURN;
  }

  IF(RecordExists(editorinfo))
  {
    IF(editorinfo.screen != "")
    {
      StartObjectEditor(parentscreen, [ calltype := "embedded", target := [ id := objectid ]]);
      RETURN;
    }

    //Look up the editor
    IF(RecordExists(objeditor))
    {
      //Launchable, go for it!
      IF (parentscreen->contexts->controller->__DoIntegratedAppLaunch(parentscreen, objeditor, CELL[ ...options
                                                                                       , calltype := "objecteditor"
                                                                                       , id := objectid
                                                                                       , objeditor
                                                                                       ]))
        RETURN;
    }

    //FIXME If developer, warn we did a fallback
    IF(parentscreen->tolliumuser->HasRight("system:sysop"))
    {
      parentscreen->RunMessageBox("publisher:commondialogs.cannotfindobjecteditor", editorinfo.name);
      RETURN;
    }
  }

  IF(fileobj->type=29)
  {
    OBJECT editprofilescreen := parentscreen->LoadScreen("publisher:conversionprofile.editor", [fileid:=objectid]);
    editprofilescreen->RunModal();
    RETURN;
  }

  IF(fileobj->type=32)
  {
    //Ook hier. Integreren met algemene property editor (forum voegt dan tabbladden toe) of aparte dialogs of... ?
    OBJECT editforumscreen := parentscreen->LoadScreen("publisher:forum.editforum", [forumid:=objectid]);
    editforumscreen->RunModal();
    RETURN;
  }
  IF(fileobj->type = 14 OR fileobj->typens = "http://www.webhare.net/xmlns/publisher/webharearchivefile") //targz/zip
  {
    OBJECT extractdialog := parentscreen->LoadScreen('publisher:fd-archive.extracthere', [fileid:=objectid]);
    extractdialog->RunModal();
    RETURN;
  }
  IF(fileobj->type IN [18,19,20])
  {
    parentscreen->RunMessageBox("publisher:commondialogs.cannotopenlink");
    RETURN;
  }
  IF(fileobj->type = 24)
  {
    parentscreen->RunMessageBox("publisher:commondialogs.cannotopencontentlisting");
    RETURN;
  }

  //Get the siteprofile information about this type
  RECORD typedef := LookupContentType(fileobj->typens);
  IF(RecordExists(typedef) AND typedef.isembeddedobjecttype)
  {
    RECORD data := fileobj->GetInstanceData(fileobj->typens);
    RECORD widgetsettings := fileobj->GetInstanceData("http://www.webhare.net/xmlns/publisher/contentlibraries/slottedwidget");
    RECORD embobj := DoEditWidget(parentscreen, DEFAULT OBJECT, fileobj->typens, data, fileobj, applytester, TRUE, TRUE, fileobj->title, widgetsettings);
    IF(RecordExists(embobj)) // was the data editted? ('Cancel' returns a DEFAULT RECORD)
    {
      GetPrimary()->BeginWork();
      //FIXME: verify that file is still available, writeable atc - although we actually -should-
      //       be given such notifications in __editEmbeddedObject's SUBMIT handler.
      fileobj->SetInstanceData(fileobj->typens, embobj.instance);
      fileobj->SetInstanceData("http://www.webhare.net/xmlns/publisher/contentlibraries/slottedwidget", embobj.widgetsettings);
      fileobj->UpdateMetadata([ title := embobj.title ]);
      RunEditFileHooks(fileobj->id);
      GetPrimary()->CommitWork();
    }
    RETURN;
  }
  RunFSObjectPropertiesDialog(parentscreen, objectid, [assumereadaccess := TRUE]); //we already check readaccess, don't redo it
}


/** Show a single form result
    @param parentscreen Screen invoking us
    @param formresults Form results
    @cell(string) options.uselanguage The language to show the results in, defaults to the form's language, falls back to the
        current user's language */
PUBLIC MACRO RunFormResultsDialog(OBJECT parentscreen, OBJECT formresults, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions([ uselanguage := "" ], options);
  IF (options.uselanguage = "")
  {
    OBJECT applytester := GetApplyTesterForObject(formresults->GetStorageObject()->id);
    IF (ObjectExists(applytester))
      options.uselanguage := applytester->GetSiteLanguage();
  }
  IF (options.uselanguage = "")
    options.uselanguage := parentscreen->contexts->user->language;

  parentscreen->RunScreen("mod::publisher/tolliumapps/formedit/results.xml#results", CELL
      [ fsobjid := formresults->GetStorageObject()->id
      , formobj := formresults->__formdef
      , formresults
      , options.uselanguage
      , rerunnablehandlers := RECORD[]
      , testablehandlers := RECORD[]
      , showpending := FALSE
      ]);
}


/** Show a single form result
    @param parentscreen Screen invoking us
    @param formresults Form results
    @param resultguid Result guid
    @cell(string array) options.hidefields List of base fields to hide (ipaddress, ipcountrycode, url, useragent, when)
    @return False if the requested results could not be found */
PUBLIC BOOLEAN FUNCTION RunSingleFormResultDialog(OBJECT parentscreen, OBJECT formresults, STRING resultguid, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions(CELL[ hidefields := STRING[] ], options);

  INTEGER existingresult := GetFinalResultId(formresults->GetStorageObject()->id, "webtoolform", resultguid);
  IF(existingresult = 0)
    RETURN FALSE;

  OBJECT exporter := formresults->CreateExporter( [ language := GetTidLanguage()
                                                  , isfullexport := TRUE
                                                  , showpendingresults := TRUE
                                                  , showexpiredresults := TRUE
                                                  , inlineattachments := TRUE
                                                  ]);
  RECORD data := exporter->ExportAllResults([ resultids := INTEGER[existingresult]]);
  IF(NOT RecordExists(data))
    RETURN FALSE;

  RECORD ARRAY cols := exporter->columns;
  DELETE FROM cols WHERE name IN options.hidefields;
  IF(NOT parentscreen->contexts->user->HasRight("system:supervisor")) //hide sensitive data - but even for supervisors it should be optional to export these
    DELETE FROM cols WHERE name IN ["ipaddress","useragent"];

  parentscreen->RunScreen("mod::publisher/tolliumapps/formedit/results.xml#singleresult",
      CELL[ row := data
          , cols
          , resultid := existingresult
          ]);
  RETURN TRUE;
}

PUBLIC MACRO RunFeedbackDetailsDialog(OBJECT contexts, INTEGER feedbackid)
{
  contexts->controller->SendApplicationMessage("publisher:feedback", CELL[ feedback := feedbackid ], DEFAULT RECORD, TRUE);
}
