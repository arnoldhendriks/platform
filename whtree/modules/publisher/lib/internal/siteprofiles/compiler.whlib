﻿<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::files.whlib";
LOADLIB "wh::internal/interface.whlib";
LOADLIB "wh::util/algorithms.whlib";

LOADLIB "mod::publisher/lib/siteprofiles.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/logging.whlib";
LOADLIB "mod::system/lib/services.whlib";
LOADLIB "mod::system/lib/internal/dbschema.whlib";
LOADLIB "mod::publisher/lib/internal/siteprofiles/cache.whlib";
LOADLIB "mod::publisher/lib/internal/siteprofiles/parser.whlib";
LOADLIB "mod::publisher/lib/internal/siteprofiles/reader.whlib";
LOADLIB "mod::system/lib/internal/whfs/contenttypes.whlib";
LOADLIB "mod::system/lib/internal/whfs/events.whlib";
LOADLIB "mod::system/lib/internal/modules/defreader.whlib";

CONSTANT RECORD sp_translations := //JSON key spelling. Must match siteprofiles.ts CachedSiteProfiles and any used types
  CELL[ //valueConstraints
        "minValue","maxValue","maxBytes","valueType","itemType"
      ];

PUBLIC RECORD ARRAY FUNCTION GetCurrentFSTypes()
{
  RETURN SELECT * FROM system.fs_types;
}

PUBLIC STATIC OBJECTTYPE RecompileContext
<
  RECORD ARRAY current_fs_types;
  RECORD ARRAY current_fs_members;

  INTEGER ARRAY seen_members;

  OBJECT parameters;
  PUBLIC BOOLEAN debug;
  PUBLIC BOOLEAN reportmissing;
  RECORD result;
  RECORD ARRAY grouptypes;

  MACRO NEW()
  {
    this->current_fs_types := SELECT *
                                FROM system_internal.fs_types
                            ORDER BY namespace;
    this->current_fs_members := SELECT *
                                  FROM system_internal.fs_members
                              ORDER BY fs_type;

    this->result := [ errors               := RECORD[]
                    , warnings             := RECORD[]
                    , hints                := RECORD[]
                    , contenttypes         := RECORD[]
                    , grouptypes           := RECORD[]
                    , siteprofiles         := STRING[]
                    , profilerefs          := RECORD[]
                    , success              := FALSE
                    , applies              := RECORD[]
                    , webrules             := RECORD[]
                    , addtocatalogs        := RECORD[]
                    , hiddenfoldertypes    := INTEGER[]
                    ];
  }

  PUBLIC RECORD ARRAY FUNCTION GetErrors()
  {
    RETURN this->result.errors;
  }

  MACRO LogSPError(STRING filename, STRING error, INTEGER line)
  {
    INSERT INTO this->result.errors(resourcename, message, line, col) VALUES(filename, error, line, 0) AT END;
  }

  PUBLIC RECORD ARRAY FUNCTION GetMembersFor(INTEGER gettype)
  {
    RETURN RecordRange(this->current_fs_members, [ fs_type := gettype ], [ "FS_TYPE" ]);
  }

  MACRO ProcessGroupMemberships()
  {
    this->grouptypes := SELECT * FROM this->grouptypes ORDER BY namespace;
    STRING ARRAY grouptypenamespaces := SELECT AS STRING ARRAY namespace FROM this->grouptypes;

    FOREVERY (RECORD fs_type FROM this->result.contenttypes)
    {
      RECORD ARRAY valid_memberships;
      RECORD ARRAY addtogroups;

      // First filter on valid groups, then add to groups as members. Keeps groupmembership lists valid everywhere.
      FOREVERY (RECORD rec FROM fs_type.groupmemberships)
      {
        RECORD pos := RecordLowerBound(this->grouptypes, [ namespace := rec.grouptype ], [ "NAMESPACE" ]);
        IF (NOT pos.found)
        {
          this->LogSPError(fs_type.siteprofile, "Contenttype '" || fs_type.namespace || "' is member of non-existing group '" || rec.grouptype || "'", rec.line);
        }
        ELSE
        {
          INSERT rec INTO valid_memberships AT END;
          INSERT pos INTO addtogroups AT END;
        }
      }

      fs_type.groupmemberships := valid_memberships;
      this->result.contenttypes[#fs_type] := fs_type;

      FOREVERY (RECORD pos FROM addtogroups)
      {
        RECORD pos2 := RecordLowerBound(this->grouptypes[pos.position].members, fs_type, [ "NAMESPACE" ]);
        IF (NOT pos2.found)
          INSERT fs_type INTO this->grouptypes[pos.position].members AT pos2.position;
      }
    }
  }

  RECORD ARRAY siteprofiles;
  RECORD ARRAY contenttypes;

  MACRO LoadSiteProfileRecursively(STRING name, INTEGER ARRAY siteprofileids, STRING ARRAY loadpath)
  {
    IF(this->debug)
      Print("Load " || name || " from " || Detokenize(loadpath," >") || "\n");

    IF(name IN loadpath)
    {
      this->LogSPError(name,"Recursive loadlib detected, path: " || Detokenize(loadpath," >"),0);
      RETURN;
    }
    INSERT name INTO loadpath AT END;

    //Do we have the siteprofile already
    INTEGER existingpos := (SELECT AS INTEGER #siteprofiles + 1 FROM this->siteprofiles WHERE resourcename = name)-1;
    RECORD loaded;
    IF(existingpos=-1)
    {
      existingpos := Length(this->siteprofiles);

      loaded := GetParsedSiteProfile(name);
      this->result.errors := this->result.errors CONCAT RECORD ARRAY(loaded.errors);
      this->result.warnings := this->result.warnings CONCAT RECORD ARRAY(loaded.warnings);
      this->contenttypes := this->contenttypes CONCAT RECORD ARRAY(loaded.contenttypes);
      this->grouptypes := this->grouptypes CONCAT RECORD ARRAY(loaded.grouptypes);

      INSERT [ resourcename := name
             , siteprofileids := siteprofileids
             , siteprofile := loaded
             ] INTO this->siteprofiles AT END;
    }
    ELSE
    {
      loaded := this->siteprofiles[existingpos].siteprofile;

      //if both we and the siteprofile already have limited application, combine them
      IF(Length(this->siteprofiles[existingpos].siteprofileids) > 0)
      { //note that global siteprofiles load before anything specific, so we KNOW Length(siteprofileids) to be > 0
        this->siteprofiles[existingpos].siteprofileids := ArrayUnion(this->siteprofiles[existingpos].siteprofileids, siteprofileids);
      }
    }

    FOREVERY(STRING apply FROM loaded.applysiteprofiles) //note that if the siteprofile is already loaded, this just 'spreads' our siteprofileids recursively
      this->LoadSiteProfileRecursively(apply, this->siteprofiles[existingpos].siteprofileids, loadpath);

    IF(this->debug)
      Print("Done " || name || "\n");
  }

  PUBLIC RECORD FUNCTION GatherSiteProfiles()
  {
    //Gather unique list of siteprofiles
    RECORD ARRAY webdesigns  := SELECT * FROM GetAvailableWebDesigns(FALSE) WHERE Length(siteprofiles) > 0;
    RECORD ARRAY webfeatures := SELECT * FROM GetAvailableWebFeatures() WHERE siteprofile != "";
    STRING ARRAY allsiteprls := SELECT AS STRING ARRAY siteprofile FROM webfeatures;
    STRING ARRAY basesiteprofiles;

    FOREVERY(RECORD mod FROM GetWebHareModules())
      basesiteprofiles := basesiteprofiles CONCAT mod.basesiteprofiles;

    FOREVERY(RECORD webdesign FROM webdesigns)
      allsiteprls := allsiteprls CONCAT webdesign.siteprofiles;

    allsiteprls := GetSortedSet(allsiteprls CONCAT basesiteprofiles);

    //Rebuild a simple reference -> resourceid list
    RECORD ARRAY profilerefs := SELECT isfeature := TRUE
                                     , name := rowkey
                                     , siteprofileids := INTEGER[BinaryFind(allsiteprls, siteprofile) + 1 ]
                                  FROM webfeatures;

    FOREVERY(RECORD webdesign FROM webdesigns)
    {
      INTEGER ARRAY siteprofileids;
      FOREVERY(STRING siteprof FROM webdesign.siteprofiles)
        INSERT BinaryFind(allsiteprls, siteprof) + 1 INTO siteprofileids AT END;

      INSERT CELL[ isfeature := FALSE
                 , name := webdesign.rowkey
                 , siteprofileids
                 ] INTO profilerefs AT END;
    }

    INTEGER ARRAY baseprofiles := SELECT AS INTEGER ARRAY BinaryFind(allsiteprls, siteprl) + 1 FROM ToRecordArray(basesiteprofiles, "siteprl");

    RETURN CELL[ siteprofiles := allsiteprls
               , profilerefs
               , baseprofiles
               ];
  }

  PUBLIC RECORD ARRAY FUNCTION PreprocessContentTypes(RECORD ARRAY suppliedcontenttypes, RECORD ARRAY currentcontenttypes) //public for test_compiler
  {
    currentcontenttypes := SELECT * FROM currentcontenttypes ORDER BY namespace;
    suppliedcontenttypes := SELECT * FROM suppliedcontenttypes ORDER BY namespace;

    RECORD ARRAY finalcontenttypes;
    FOREVERY(RECORD ctype FROM suppliedcontenttypes)
    {
      // Second time we see this type? If so, ignore this definition and say something
      IF(#ctype > 0)
      {
        RECORD prevctype := suppliedcontenttypes[#ctype-1];
        IF(prevctype.namespace = ctype.namespace)
        {
          this->LogSPError(prevctype.siteprofile, "Location of other definition of '" || ctype.namespace || "'", prevctype.line);
          this->LogSPError(ctype.siteprofile, "Second definition for type '" || ctype.namespace || "' found", ctype.line);
          CONTINUE;
        }
      }

      // These two types are both #0 and will never be inserted into the database, so just put them on the finallist and be done with it
      IF(ctype.namespace IN [ "http://www.webhare.net/xmlns/publisher/unknownfile", "http://www.webhare.net/xmlns/publisher/normalfolder" ])
      {
        INSERT ctype INTO finalcontenttypes AT END;
        CONTINUE;
      }

      /* We originally had GetFSTypeBase which was simply a list of fields that went to the database, triggering a lot of
         field duplication:

      PUBLIC RECORD FUNCTION GetFSTypeBase()
      {
        RETURN CELL [ cloneoncopy := TRUE, isfiletype := FALSE, isfoldertype := FALSE, orphan := FALSE
                    , isacceptableindex := FALSE
                    , needstemplate := FALSE, ispublishable := FALSE, ispublishedassubdir := FALSE
                    , generatepreview := FALSE
                    , isdynamicexecution := FALSE, capturesubpaths := FALSE
                    ];
      }

      I think a lot of these fields don't need to be in the database anymore. But to limit this refactor we'll simply rebuild this this structure

      Look at MakeBasicTypeInfo, GetBaseFolderTypeRecord and GetBaseFiletypeRecord to find out where these fields are in suppliedcontenttypes
      */
      RECORD fstype := CELL[ ctype.cloneoncopy
                           , ctype.scopedtype
                           , isfiletype := RecordExists(ctype.filetype)
                           , isfoldertype := RecordExists(ctype.foldertype)
                           , isdynamicexecution := RecordExists(ctype.dynamicexecution)
                           , orphan := FALSE //TODO isn't this always false? can the parser/compiler return orphans ?
                           , isacceptableindex := RecordExists(ctype.filetype) AND ctype.filetype.isacceptableindex
                           , needstemplate := RecordExists(ctype.filetype) AND ctype.filetype.needstemplate
                           , ispublishable := RecordExists(ctype.filetype) AND ctype.filetype.ispublishable
                           , ispublishedassubdir := RecordExists(ctype.filetype) AND ctype.filetype.ispublishedassubdir
                           , generatepreview := RecordExists(ctype.filetype) AND ctype.filetype.generatepreview
                           , capturesubpaths := RecordExists(ctype.filetype) AND ctype.filetype.capturesubpaths
                           ];

      RECORD existing := RecordLowerBound(currentcontenttypes, ctype, ["NAMESPACE"]);
      RECORD toupdate;

      IF(existing.found)
      {
        RECORD match := currentcontenttypes[existing.position];
        IF( (match.isfoldertype AND RecordExists(ctype.filetype)) OR (match.isfiletype AND RecordExists(ctype.foldertype)))
        {
          this->LogSPError(ctype.siteprofile, "Attempting to redefine file/foldertype", ctype.line);
          CONTINUE;
        }

        //FIXME PREVENT loss of isfoldertype if folders still exist

        ctype.id := match.id;
        FOREVERY(RECORD fld FROM UnpackRecord(fstype))
          IF(fld.value != GetCell(match, fld.name))
            toupdate := CellInsert(toupdate, fld.name, fld.value);

        INSERT CELL toupdate := toupdate INTO ctype;
      }
      ELSE
      {
        ctype.id := -1;
        INSERT CELL toupdate := fstype INTO ctype;
      }
      INSERT ctype INTO finalcontenttypes AT END;
    }
    RETURN finalcontenttypes;
  }

  RECORD ARRAY FUNCTION ApplyContentTypes(RECORD ARRAY contenttypes)
  {
    RECORD ARRAY outtypes;
    FOREVERY(RECORD ctype FROM contenttypes)
    {
      IF(ctype.id != 0) //this type lives in the database
      {
        IF(ctype.id = -1) //desires creation
        {
          ctype.id := MakeAutonumber(system.fs_types,'id');
          INSERT CELL[ ...ctype.toupdate
                     , ctype.id
                     , ctype.namespace
                     , ctype.scopedtype
                     ] INTO system.fs_types;
        }
        ELSE IF(RecordExists(ctype.toupdate))
        {
          UPDATE system.fs_types SET RECORD ctype.toupdate WHERE id = ctype.id;
        }

        RECORD updres := ApplyCTMembersRecurse(ctype.members, ctype.id, 0, FALSE, this->GetMembersFor(ctype.id));
        this->seen_members := this->seen_members CONCAT updres.seenmembers;
      }
      INSERT ctype INTO outtypes AT END;
    }
    RETURN outtypes;
  }

  PUBLIC RECORD FUNCTION RunSPCompiler()
  {
    RECORD gathersp := this->GatherSiteProfiles();
    RECORD compileresult := this->CompileSiteprofiles(gathersp);
    compileresult := this->ApplyStandardPostProcessing(compileresult);

    //this->result.siteprofiles := SELECT name := resourcename
    //                                  , applysiteprofiles := siteprofile.applysiteprofiles
    //                                  , sites := siteids
    //                               FROM this->siteprofiles;
    this->result.contenttypes := SELECT *, DELETE toupdate FROM this->ApplyContentTypes(compileresult.allcontenttypes);
    this->result.grouptypes := this->grouptypes;
    this->result.hiddenfoldertypes := compileresult.hiddenfoldertypes;
    this->result.profilerefs := gathersp.profilerefs;
    this->result.siteprofiles := gathersp.siteprofiles;

    SaveCompiledSiteProfiles(this->result);
    INSERT CELL fullcspdata := this->result INTO compileresult;

    compileresult := CELL[ ...compileresult
                         , this->result.errors
                         , this->result.warnings
                         , this->result.hints
                         ];

    // Just assume we'll make changes
    GetWHFSCommitHandler()->FSTypesChanged();

    this->UpdateOrphanStatus();

    GetPrimary()->BroadcastOnCommit("publisher:internal.siteprofiles.recompiled", DEFAULT RECORD);
    GetPrimary()->BroadcastOnCommit("publisher:internal.siteprofiles.memberschanged", DEFAULT RECORD);

    CollectGarbage();

    RETURN compileresult;
  }

  MACRO UpdateOrphanStatus()
  {
    // Only hit the DB for records we need to change
    INTEGER ARRAY seen_content_type_ids := SELECT AS INTEGER ARRAY id FROM this->result.contenttypes ORDER BY id;
    INTEGER ARRAY set_orphan_types :=
        SELECT AS INTEGER ARRAY id
          FROM this->current_fs_types
         WHERE (orphan = FALSE OR orphansince = DEFAULT DATETIME) AND NOT LowerBound(seen_content_type_ids, id).found;

    INTEGER ARRAY clear_orphan_types :=
        SELECT AS INTEGER ARRAY id
          FROM this->current_fs_types
         WHERE orphan = TRUE AND LowerBound(seen_content_type_ids, id).found;

    INTEGER ARRAY seen_member_ids := GetSortedSet(this->seen_members);
    INTEGER ARRAY set_orphan_members :=
        SELECT AS INTEGER ARRAY id
          FROM this->current_fs_members
         WHERE (orphan = FALSE OR orphansince = DEFAULT DATETIME) AND NOT LowerBound(seen_member_ids, id).found;
    INTEGER ARRAY clear_orphan_members :=
        SELECT AS INTEGER ARRAY id
          FROM this->current_fs_members
         WHERE orphan = TRUE AND LowerBound(seen_member_ids, id).found;

    DATETIME now := GetCurrentDateTime();
    IF(Length(set_orphan_types) > 0)
    {
      UPDATE system_internal.fs_types
         SET orphan := TRUE
           , orphansince := now
       WHERE id IN set_orphan_types;
    }

    IF(Length(clear_orphan_types) > 0)
    {
      UPDATE system_internal.fs_types
          SET orphan := FALSE
            , orphansince := DEFAULT DATETIME
        WHERE id IN clear_orphan_types;
    }

    IF(Length(set_orphan_members) > 0)
    {
      UPDATE system_internal.fs_members
         SET orphan := TRUE
           , orphansince := now
       WHERE id IN set_orphan_members;
    }

    IF(Length(clear_orphan_members) > 0)
    {
      UPDATE system_internal.fs_members
         SET orphan := FALSE
           , orphansince := DEFAULT DATETIME
       WHERE id IN clear_orphan_members;
    }
  }

  PUBLIC RECORD FUNCTION ApplyStandardPostProcessing(RECORD compileresult) //public for test_compiler
  {
    compileresult.allcontenttypes := this->PreprocessContentTypes(compileresult.allcontenttypes, GetCurrentFSTypes());
    INSERT CELL hiddenfoldertypes := (SELECT AS INTEGER ARRAY id
                                           FROM compileresult.allcontenttypes
                                          WHERE RecordExists(foldertype) AND foldertype.ishidden) INTO compileresult;
    RETURN compileresult;
  }

  PUBLIC RECORD FUNCTION CompileSiteprofiles(RECORD gathersp)
  {
    RECORD ARRAY errors;

    DATETIME now := GetCurrentDateTime();
    this->result.errors := errors;

    RECORD ARRAY compiledsiteprofiles;

    // First load all base profiles (these always apply)
    FOREVERY(INTEGER baseprofile FROM gathersp.baseprofiles)
      this->LoadSiteProfileRecursively(gathersp.siteprofiles[baseprofile - 1], INTEGER[], STRING[]);

    // Then run through the siteprofiles that can be activated by a resource id
    FOREVERY(STRING siteprofile FROM gathersp.siteprofiles)
      IF(#siteprofile + 1 NOT IN gathersp.baseprofiles)
        this->LoadSiteProfileRecursively(siteprofile, INTEGER[#siteprofile + 1], STRING[]);

    this->ProcessGroupMemberships();

     // Copy siteprofileids from siteprofiles to the individual rules
    FOREVERY(RECORD siteprof FROM this->siteprofiles)
    {
      FOREVERY(RECORD rule FROM siteprof.siteprofile.rules)
      {
        IF(rule.ruletype = "apply")
        {
          RECORD addrule;
          IF(rule.applynodetype = "apply") //explicit <apply> blocks are still scoped to their site(s)
            addrule := CELL[...rule, DELETE ruletype, siteprofileids := siteprof.siteprofileids ];
          ELSE //<filetype> and <foldertype> are not scoped
            addrule := CELL[...rule, DELETE ruletype, siteprofileids := INTEGER[] ];

          INSERT addrule INTO this->result.applies AT END;
        }
        ELSE IF(rule.ruletype = "sitesetting")
        {
          IF(Length(siteprof.siteprofileids) = 0)
          {
            this->LogSPError(siteprof.resourcename, "Sitesettings may not be defined in siteprofiles that affect all sites", rule.line);
            CONTINUE;
          }

          this->result.webrules      := this->result.webrules      CONCAT SELECT *, siteprofileids := siteprof.siteprofileids, sitefilter := rule.sitefilter FROM rule.webrules;
          this->result.addtocatalogs := this->result.addtocatalogs CONCAT SELECT *, siteprofileids := siteprof.siteprofileids, sitefilter := rule.sitefilter FROM rule.addtocatalogs;
        }
      }
    }
    //Sort apply rules. Note that sorts on record arrays are stable.
    this->result.applies := SELECT * FROM this->result.applies ORDER BY priority;

    RETURN [ result := this->result  //FIXME state leak
           , compiledsiteprofiles := compiledsiteprofiles
           , allcontenttypes := this->contenttypes
           ];
  }
>;

MACRO SaveCompiledSiteProfiles(RECORD result)
{
  /* We stored this in WHFS to be able to share it in a potential cluster, but we're not there yet and JS stores it on disk anyway... so sort it out later
     If CSP in WHFS we need a working WHFS to read/write is ... but CreateFile/UpdateMetadata may use SetInstanceData and thus rely on CSP to be there */

  INTEGER str := CreateStream();
  __HS_MarshalWriteTo(str, result);
  BLOB blobdata := MakeBlobFromStream(str);

  //TODO This is not an atomic replace. consider combining siteprofiles and siteprofilerefs.json and have the 'fast path' simply update part of the refs file
  STRING configpath := GetModuleStorageRoot("system") || "config";
  UpdateSiteProfileRefs(result); //will also generate the needed directories
  CreateDiskDirectoryRecursive(configpath, TRUE);
  StoreDiskFile(configpath || "/siteprofiles.json", EncodeJSONBlob(result, sp_translations), [ overwrite := TRUE ]);
  StoreDiskFile(configpath || "/siteprofiles.bin", blobdata, [ overwrite := TRUE ]);
}

PUBLIC RECORD FUNCTION __DoRecompileSiteprofiles(BOOLEAN locked, BOOLEAN debug, BOOLEAN reportmissing)
{
  IF(NOT HavePrimaryTransaction()) //--twice already gives us one
    OpenPrimary();
  IF(locked)
    GetPrimary()->BeginLockedWork("publisher:compilesiteprofiles");
  ELSE
    GetPrimary()->BeginWork();

  RECORD response;
  TRY
  {
    OBJECT recompiler := NEW RecompileContext;
    recompiler->debug := debug;
    recompiler->reportmissing := reportmissing;
    response := recompiler->RunSPCompiler();
  }
  CATCH(OBJECT e)
  {
    GetPrimary()->RollbackWork();
    THROW e;
  }
  GetPrimary()->CommitWork();

  __SetCSPCacheFromSourceData(response.fullcspdata);

  IF(Length(response.errors)>0)
    LogError("publisher:siteprofiles", "siteprofilecompilation-error", [ errors := response.errors ]);

  //async update webserver check/indices
  GetPrimary()->BeginWork();
  GetWHFSCommitHandler()->TriggerSiteSettingsCheckOnCommit();
  GetPrimary()->CommitWork();

  RETURN [ errors := response.errors
         , warnings := response.warnings
         , hints := response.hints
         ];
}

PUBLIC RECORD FUNCTION RecompileSiteProfiles()
{
  RETURN WaitForPromise(AsyncRecompileSiteProfiles());
}

PUBLIC ASYNC FUNCTION AsyncRecompileSiteProfiles()
{
  OBJECT service  := AWAIT OpenWebHareService("system:spcompiler");
  TRY
  {
    RECORD response := AWAIT service->RequestRecompile();
    RETURN response;
  }
  FINALLY
  {
    service->CloseService();
  }
}
