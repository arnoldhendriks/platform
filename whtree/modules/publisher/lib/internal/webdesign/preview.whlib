<?wh

LOADLIB "wh::internet/tcpip.whlib";

LOADLIB "mod::system/lib/webserver.whlib";

/** Formats a X-WH-Proxy header
    @param source
    @param binding
    @param proto
    @param forip
    @param localip
    @param localport
    @return Formatted X-WH-Proxy header value
*/
PUBLIC STRING FUNCTION FormatXWHProxyHeader(RECORD data)
{
  data := ValidateOptions(
      [ source :=     ""
      , binding :=    0
      , proto :=      ""
      , forip :=      ""
      , localip :=    ""
      , localport :=  0
      ],
      data,
      [ required := [ "source", "proto", "forip", "localip", "localport" ]
      ]);

  FOREVERY (RECORD param FROM UnpackRecord(CELL[ data.source, data.proto, data.forip, data.localip ]))
    IF (param.value LIKE "*;*" OR param.value LIKE "*\n*")
      THROW NEW Exception(`Illegal proxy header parameter '${param.name}': ${EncodeJava(param.value)}`);

  STRING header := `source=${EncodeJava(data.source)}`;
  IF (data.binding != 0)
    header := header || `;binding=${data.binding}`;
  header := header || `;proto=${EncodeJava(data.proto)};for=${EncodeJava(data.forip)};local=${FormatSocketAddress(EncodeJava(data.localip), data.localport)}`;
  RETURN header;
}

/** Returns the X-WH-Proxy header that is needed for a forwarded request
*/
PUBLIC STRING FUNCTION CalculateXWHProxyHeaderForRequestForwarding(STRING source)
{
  STRING proto := GetClientRequestURL() LIKE "https:*" ? "https" : "http";
  RETURN FormatXWHProxyHeader(CELL[ source
                                  , binding :=    GetClientBinding()
                                  , proto
                                  , forip :=      GetClientRemoteIp()
                                  , localip :=    GetClientLocalIp()
                                  , localport :=  GetClientLocalPort()
                                  ]);
}
