<?wh
/** @short Sites published to the local filesystem
*/

LOADLIB "wh::filetypes/archiving.whlib";

LOADLIB "mod::publisher/lib/internal/outputmedia/base.whlib";
LOADLIB "mod::publisher/lib/internal/support.whlib";
LOADLIB "mod::publisher/lib/internal/outputmedia/localfs-actions.whlib";

LOADLIB "mod::system/lib/internal/webhareconstants.whlib";

//-----------------------------------------------------------------------------
//
// Declarations
//

PUBLIC OBJECTTYPE LocalFSOutputMedia EXTEND OutputMediaBase
<
  UPDATE PUBLIC OBJECT FUNCTION CreatePublicationOutput(STRING _filename,
                                                        BOOLEAN all_subdir,
                                                        BOOLEAN is_index,
                                                        BOOLEAN capturesubpaths,
                                                        STRING _outputroot,
                                                        BOOLEAN _stripextension)
  {
    RETURN NEW LocalSiteWriter(_filename, all_subdir, is_index, capturesubpaths, _outputroot, _stripextension);
  }
>;

STATIC OBJECTTYPE LocalSiteWriter EXTEND OutputPublicationBase
< ///The directory in which all publishing takes place
  PUBLIC STRING outputroot;

  ///The name of the file we are publishing
  PUBLIC STRING outputname;

  ///List of files we created
  PUBLIC STRING ARRAY createdfiles;

  STRING newversion_dir;
  PUBLIC STRING newversion_file;

  PUBLIC STRING newversion_filegz;

  ///Filename of the first published file
  PUBLIC STRING final_firstfilename;

  PUBLIC BOOLEAN createddir;

  ///Publish as a subdirectory
  PUBLIC BOOLEAN publish_as_subdir;
  ///Publish first file as folder index?
  PUBLIC BOOLEAN publish_as_index;

  /// Whether to enable capturing subpaths (so emit ^^useindex markers)
  PUBLIC BOOLEAN capturesubpaths;

  /** List of current outputs */
  PUBLIC INTEGER ARRAY outfiles;

  // Current total size
  PUBLIC INTEGER totalsize;

  PUBLIC RECORD current_error;

  PUBLIC BOOLEAN stripextension;

  PUBLIC FUNCTION PTR error_notify;

  /** The files we created, relative to this->outputroot
      @cell path The local path
      @cell firstfile If this is the first file
      @cell temppath Path to the written temporary
  */
  PUBLIC RECORD ARRAY generatedfiles;

  PUBLIC MACRO NEW(
      STRING _filename,
      BOOLEAN all_subdir,
      BOOLEAN is_index,
      BOOLEAN capturesubpaths,
      STRING _outputroot,
      BOOLEAN _stripextension)
  {
    this->outputroot := _outputroot;
    this->outputname := _filename;
    this->createddir := FALSE;
    this->publish_as_subdir := all_subdir;
    this->publish_as_index := is_index;
    this->capturesubpaths := capturesubpaths;
    this->stripextension := _stripextension;

    if (NOT IsPathAbsolute(this->outputroot))
    {
      //Store the error code and message.
      this->ErrorNotify(1001, "Will not publish to a non-absolute path: " || this->outputroot);
      RETURN;
    }

    /* Find any existing Temp directories - don't consider removal failure to be fatal to work around NT locking bugs.. */
    FOREVERY (RECORD entry FROM ReadDiskDirectory(this->outputroot, "$$$??" || this->outputname || "$*"))
    {
      STRING todelete := MergePath(this->outputroot, entry.name);
      BOOLEAN success;
      IF(entry.type = 1)
        success := DeleteLocalFSDirectory("Delete already existing temporaries", todelete);
      ELSE
        success := DeleteLocalFSFile("Delete already existing temporaries", todelete);

      IF(NOT success)
        DEBUGPRINT("Cannot remove existing temp file/directory - duplicate publishing process? (" || GetLastOSError() || ")");
    }

    //Create the output directory if it doesn't exist yet
    RECORD dirprops := GetDiskFileProperties(this->outputroot);
    IF (NOT RecordExists(dirprops) OR dirprops.type != 1) //then
    {
      STRING createerror;
      IF(NOT CreateLocalFSDirectory("Creating output root", this->outputroot))
      {
        STRING error := "Cannot create site output root directory " || this->outputroot || " (" || GetLastOSError() || ")";
        //Check if we really failed, the error might be a race caused by another publishing engine
        dirprops := GetDiskFileProperties(this->outputroot);
        IF (NOT RecordExists(dirprops))
        {
          this->ErrorNotify(1001,error || " - it still doesn't exist");
          RETURN;
        }
        IF (dirprops.type != 1)
        {
          this->ErrorNotify(1001,error || " - it has type #" || dirprops.type);
          RETURN;
        }
      }
    }

    //Determine output directory, and name of the first output file
    this->newversion_dir := GenerateTemporaryPathnameFromBasepath(MergePath(this->outputroot,"$$$pd" || this->outputname || "$"));
    IF(this->newversion_dir = "")
      ABORT("Did not receive a temp path name");
  }


  //-----------------------------------------------------------------------------
  //
  // Helpers
  //
  PRIVATE MACRO ErrorNotify(INTEGER code, STRING msg)
  {
    this->current_error := [ code := code, msg := msg ];
    IF (this->error_notify != DEFAULT FUNCTION PTR)
      this->error_notify(code, msg);
  }

  PRIVATE STRING FUNCTION PrepareOpen(STRING _filename)
  {

    STRING filename := _filename;

    BOOLEAN firstfile := false;

    IF (this->publish_as_index OR this->publish_as_subdir)
    {
      //This is the first file if it appears in the list of default pages
      firstfile := ToLowercase(_filename) IN whconstant_webserver_indexpages;
    }
    ELSE
    {
      IF (ToUppercase(filename) = ToUppercase(this->outputname))
          firstfile := TRUE;
    }

    IF (NOT this->createddir AND (this->publish_as_subdir OR NOT firstfile) AND this->newversion_dir="")
      ABORT("Newversion dir is empty!");

    IF (firstfile)
    {
      if (this->final_firstfilename != "")
      {
        DEBUGPRINT("Duplicate first file " || this->final_firstfilename);
        RETURN "";
      }
      this->final_firstfilename := _filename;
    }

    //ADDME: These checks are not 'local' specific, so they should be moved to a higher level
    IF (SearchSubString(filename, '/') != -1
        OR SearchSubString(filename, '\\') != -1
        OR SearchSubString(filename, ':') != -1)
    {
      DEBUGPRINT("Illegal filename " || _filename);
      RETURN "";
    }

    //Add to our list, and check against duplicates
    IF (SearchElement(this->createdfiles, ToUppercase(filename)) != -1)
    {
      DEBUGPRINT("File " || filename  || " has already been created");
      RETURN "";
    }
    INSERT ToUppercase(filename) INTO this->createdfiles AT END;

    //Should we create a directory?
    IF (NOT this->createddir AND (this->publish_as_subdir OR NOT firstfile))
    {
      //Well, either everything goes into a dir, or we're now working
      //on the second file
      IF (NOT CreateLocalFSDirectory("Creating new version directory", this->newversion_dir))
      {
        this->ErrorNotify(1001,"Cannot create output directory '" || this->newversion_dir || "' (" || GetLastOSError() || ")");
        RETURN "";
      }
      this->createddir := true;
    }

    //this->outputroot = full path on disk to the folder containing this file

    //We treat the current file different from other files
    RECORD outputnames := this->GetOutputNames();
    IF (firstfile)
    {
      if (this->publish_as_subdir)
      {
        this->newversion_file := this->newversion_dir || "/" || this->final_firstfilename;
        INSERT [ path := outputnames.localsubdir || "/" || this->final_firstfilename
               , firstfile := firstfile
               , temppath := this->newversion_file
               ] INTO this->generatedfiles AT END;
      }
      ELSE
      {
        this->newversion_file := GenerateTemporaryPathnameFromBasepath(MergePath(this->outputroot,"$$$pf" || this->outputname || "$"));
        IF (RecordExists(GetDiskFileProperties(this->newversion_file)))
        {
          IF(NOT DeleteLocalFSFile("Deleting existing newversion", this->newversion_file))
          {
            this->ErrorNotify(1001,"Cannot remove file " || this->newversion_file || " (" || GetLastOSError() || ")");
            RETURN "";
          }
        }
        INSERT [ path := this->final_firstfilename
               , firstfile := firstfile
               , temppath := this->newversion_file
               ] INTO this->generatedfiles AT END;
      }

      RETURN this->newversion_file;
    }

    STRING temppath := MergePath(this->newversion_dir, filename);
    INSERT [ path := outputnames.localsubdir || "/" || filename
           , firstfile := firstfile
           , temppath := temppath
           ] INTO this->generatedfiles AT END;
    RETURN temppath;
  }

  PRIVATE RECORD FUNCTION GetOutputNames()
  {
    STRING basedir := this->outputroot;
    STRING firstfilename := this->final_firstfilename;
    STRING file, dir, destroydir;

    BOOLEAN firstfile_outside_subdir := NOT this->publish_as_subdir OR this->publish_as_index;
    IF (firstfile_outside_subdir)
      file := MergePath(basedir, firstfilename);

    STRING localsubdir; //the subdir we'll use inside this->outputroot

    //if we're always a subdir, AND our name does not conflict with the folder..
    IF (this->publish_as_subdir AND this->outputname != firstfilename)
    {
      STRING name := this->outputname;

      IF(this->stripextension)
      {
        destroydir := MergePath(basedir, name); //any old directory with the long name should be removed
        name := GetBasenameFromPath(this->outputname);
      }

      IF(firstfile_outside_subdir AND ToLowercase(name) = ToLowercase(whconstant_webserver_indexpages[0]))
        localsubdir :=  "^" || name;
      ELSE
        localsubdir :=  name;
    }
    ELSE
    {
      localsubdir :=  "^" || this->outputname;
    }
    dir := MergePath(basedir, localsubdir);

    RETURN [ file := file, dir := dir, localsubdir := localsubdir, destroydir := destroydir ];
  }

  PUBLIC BOOLEAN FUNCTION AnyError()
  {
    RETURN RecordExists(this->current_error);
  }

  //-----------------------------------------------------------------------------
  //
  // The functions
  //
  PUBLIC MACRO HaveException(INTEGER exceptioncode, STRING exceptiondata)
  {
    this->ErrorNotify(exceptioncode, exceptiondata);
  }

  PUBLIC INTEGER FUNCTION OpenOutput(STRING filename)  // from OpenRawOutput
  // from OpenRawOutput
  {
    // MyRawOutputPtr newoutput(new MyRawOutput);

    STRING path := this->PrepareOpen(filename);
    IF (this->AnyError() OR path = "")
      RETURN 0; //illegal or dupe filename. FIXME: Check valid filename

    INTEGER handle := CreateLocalFSDiskFile("Create output file " || filename, path, FALSE, TRUE);
    IF (handle = 0)
    {
      this->ErrorNotify(1001,"Cannot create output file " || path || " (" || GetLastOSError() || ")");
      RETURN 0;
    }

    //newoutput->bufferedstream.reset(new Blex::BufferedStream(*newoutput->filestream));

    INSERT handle INTO this->outfiles AT END;
    RETURN handle;
  }

  PUBLIC BOOLEAN FUNCTION CloseOutput(INTEGER outputid)    // from CloseRawOutput
  // from CloseRawOutput
  {
    INTEGER pos := SearchElement(this->outfiles, outputid);
    IF (pos = -1)
      RETURN FALSE;

    this->totalsize := this->totalsize + INTEGER(GetFilelength(outputid));

    CloseDiskFile(outputid);
    DELETE FROM this->outfiles AT pos;
    RETURN TRUE;
  }

  MACRO GenerateCompressedFiles()
  {
    FOREVERY (RECORD outfile FROM this->generatedfiles)
    {
      IF(outfile.path LIKE "*.shtml")
        CONTINUE; //pointless to compress

      // Get the type of file to detect the filetype, to see if this file must be compressed
      BLOB original_file := GetDiskResource(outfile.temppath);
      RECORD detect := WrapBlob(original_file, outfile.path);
      IF (detect.mimetype IN compressible_filetypes)
      {
        // Compress the file with max compression
        BLOB compressed := MakeZlibCompressedFile(detect.data, "GZIP", 9);

        // Don't write the compressed file if it's bigger than the original data
        IF (LENGTH64(compressed) < LENGTH64(detect.data))
        {
          STRING path := outfile.temppath || ".gz";
          StoreLocalFSDiskFile("Store compressed versin", path, compressed, [overwrite := TRUE]);

          IF (outfile.firstfile)
            this->newversion_filegz := path;
        }
      }
    }
  }

  MACRO ForceReplaced(STRING src, STRING target)
  {
    RECORD targetinfo := GetDiskFileProperties(target);
    //Eliminate target if not a file, or if we're not going to place a file there anyway
    IF(RecordExists(targetinfo) AND (targetinfo.type != 0 OR src = ""))
    {
      IF(targetinfo.type = 1)
        DeleteLocalFSDirectory("Remove target directory that shouldn't be there", target);
      ELSE
        DeleteLocalFSFile("Remove target file, we don't need it", target);
    }

    IF(src != "")
      MoveLocalFSDiskPath("Replace target with newly published version", src, target);
  }

  //  , MACRO WriteData(STRING data)
  PUBLIC MACRO Finish()
  {
    // Close outfiles if still open. Forget failing, it hasn't been checked for ages, so there will be templates out there which fail on this...
    IF (LENGTH(this->outfiles) != 0)
    {
      FOREVERY (INTEGER handle FROM this->outfiles)
        CloseDiskFile(handle);
      this->outfiles := DEFAULT INTEGER ARRAY;
    }

    // Do some preflight checks before we figure out what we want to move around
    STRING destination_dir, destination_file, destination_filegz;
    RECORD outputnames := this->GetOutputNames();
    destination_file := outputnames.file;
    destination_dir := outputnames.dir;
    destination_filegz := (destination_file ?? destination_dir) || ".gz";

    IF (destination_file = destination_dir) //Internal check
    {
      this->ErrorNotify(1001,"Publication is set up to create both a file and a directory named " || destination_file);
      RETURN;
    }

    IF (this->newversion_file = "") //no first file ?!
    {
      this->ErrorNotify(106,"");
      RETURN;
    }

    this->GenerateCompressedFiles();

    // Add an empty ^^useindex when capturing subpaths.
    IF (this->capturesubpaths)
    {
      IF(this->publish_as_index) //if we're the index, create ^^useindex to claim all subroots too
      {
        StoreLocalFSDiskFile("Add capturesubpaths file to parent", MergePath(this->outputroot,"^^useindex"), DEFAULT BLOB, [ overwrite := TRUE ]);
      }
      ELSE IF(this->publish_as_subdir) //we're just a subdir.. put the useindex there
      {
        IF(this->newversion_dir = "")
          THROW NEW Exception("newversion_dir should already exist");
        ELSE
          StoreLocalFSDiskFile("Add capturesubpaths file to newversion", MergePath(this->newversion_dir, "^^useindex"), DEFAULT BLOB, [ overwrite := TRUE ]);
      }
      ELSE
      {
        THROW NEW Exception("capturesubpaths is TRUE but no valid place for ^^useindex");
      }
    }
    ELSE IF(this->publish_as_index)
    {
      DeleteLocalFSFile("Remove useindex", MergePath(this->outputroot, "^^useindex")); //delete any ^^useindex if we're the index file but didn't want it...  FIXME outputanalyzer should also clear this up
    }

    /* Move the new version in place of the old version. We don't care too much more about the corner case "I/O error"
       because things getting locked etc was a Win32 thing. And besides, halfway recovery is usually impossible too
    */
    IF(this->publish_as_index OR NOT this->publish_as_subdir) //should the first file exist outside our subdir?
    {
      // Move our files over the destination, or if they don't exist, get rid of the destination
      this->ForceReplaced(this->newversion_filegz, destination_filegz);
      this->ForceReplaced(this->newversion_file, destination_file);
    }

    //Move subfiles into place, ie  newversion_dir => destination_dir, and then clear out destination_dir
    IF(this->createddir) //if set, we need a Destination Directory
    {
      //ADDME we shouldn't have to ReadDiskDirectory but should know if the dir is empty (maybe not create it in the first place)
      RECORD ARRAY tomove := ReadDiskDirectory(this->newversion_dir,"*");
      RECORD target := GetDiskFileProperties(destination_dir);
      RECORD ARRAY tocleanup;
      IF(RecordExists(target) AND target.type = 1) //it's not a directory now...
      {
        tocleanup := ReadDiskDirectory(destination_dir,"*");
      }
      ELSE
      {
        IF(RecordExists(target))
          DeleteLocalFSFile("Remove whatever's blocking our target directory", destination_dir);
        IF(Length(tomove) > 0)
          CreateLocalFSDirectory("Create target directory", destination_dir);
      }

      FOREVERY(RECORD move FROM tomove) //Move the files into place
      {
        RECORD ARRAY cleanup_casemismatches := SELECT * FROM tocleanup WHERE ToUppercase(name) = ToUppercase(move.name) AND name != move.name;
        FOREVERY(RECORD mismatch FROM cleanup_casemismatches) //case mismatches are dangerous, will moving the new file in place remove them or not? depends on target fs. just kill it beforehand
          IF(mismatch.type = 1)
            DeleteLocalFSDirectory("Delete case mismatch", mismatch.path);
          ELSE
            DeleteLocalFSFile("Delete case mismatch", mismatch.path);

        DELETE FROM tocleanup WHERE ToUppercase(name) = ToUppercase(move.name); //delete it from our list either way
        IF(NOT MoveLocalFSDiskPath("Move new file into place", move.path, MergePath(destination_dir, move.name)))
        {
          this->ErrorNotify(1001,"Publication failure replacing output file");
          RETURN;
        }
      }

      //Remove obsolete files/folders/ in the target
      FOREVERY(RECORD cleanup FROM tocleanup)
        IF(cleanup.type = 1)
          DeleteLocalFSDirectory("Delete obsolete folder in target directory", cleanup.path);
        ELSE
          DeleteLocalFSFile("Delete obsolete file in target directory", cleanup.path);

      DeleteLocalFSDirectory("Remove original 'tomove' source", this->newversion_dir);
    }

    IF(outputnames.destroydir != "")
      DeleteLocalFSDirectory("Delete 'destroydir'", outputnames.destroydir);

    //Remove any other existing default pages, if this was an index
    IF (this->publish_as_index)
    {
      FOREVERY (STRING page FROM whconstant_webserver_indexpages)
        IF (ToUppercase(page) != ToUppercase(this->final_firstfilename))
          DeleteLocalFSFile("Delete index page", MergePath(this->outputroot, page));
    }
  }

  UPDATE PUBLIC MACRO SetCaptureSubPaths(BOOLEAN capturesubpaths)
  {
    this->capturesubpaths := capturesubpaths;
  }

  PUBLIC MACRO Cancel()
  {
    FOREVERY (INTEGER handle FROM this->outfiles)
      CloseDiskFile(handle);
    this->outfiles := DEFAULT INTEGER ARRAY;

    IF (this->newversion_dir != "")
      DeleteLocalFSDirectory("Cancel, clean newversion_dir", this->newversion_dir);
    IF (this->newversion_file != "")
      DeleteLocalFSDirectory("Cancel, clean newversion_file", this->newversion_file);
  }
>;


//-----------------------------------------------------------------------------
//
// Debug stuff
//

BOOLEAN debug_prints;

MACRO DEBUGPRINT(STRING x)
{
  IF (debug_prints)
    PrintTo(2,x || "\n");
}

PUBLIC MACRO __EnableSiteWriterDebugPrints()
{
  debug_prints := TRUE;
}
