<?wh

LOADLIB "wh::crypto.whlib";
LOADLIB "wh::datetime.whlib";
LOADLIB "wh::files.whlib";

LOADLIB "mod::publisher/lib/database.whlib";
LOADLIB "mod::publisher/lib/forms/api.whlib";
LOADLIB "mod::publisher/lib/forms/base.whlib";
LOADLIB "mod::publisher/lib/webtools/internal/buildformhandler.whlib";
LOADLIB "mod::publisher/lib/internal/forms/opener.whlib";
LOADLIB "mod::publisher/lib/internal/siteprofiles/reader.whlib";

LOADLIB "mod::system/lib/services.whlib";
LOADLIB "mod::system/lib/tasks.whlib";
LOADLIB "mod::system/lib/whfs.whlib";
LOADLIB "mod::system/lib/internal/typecoder.whlib";
LOADLIB "mod::system/lib/internal/mail/mergefields.whlib";
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";


PUBLIC STATIC OBJECTTYPE WebtoolFormBase EXTEND FormBase
<
  RECORD ARRAY formhandlers;
  OBJECT pvt_results;
  STRING pvt_editguid;
  STRING pvt_resultguid;
  STRING pvt_idfieldvalue;
  BOOLEAN pvt_retrievedguid;
  OBJECT captchaquestion;
  RECORD captchasettings;
  BOOLEAN __didpreparehandlers;

  PUBLIC PROPERTY formresults(GetFormResults, -);

  PUBLIC PROPERTY editingguid(pvt_editguid, -);
  PUBLIC PROPERTY resultguid(GetResultGuid, -);

  ///If set, the form is unavailable and this text should be shown
  PUBLIC RECORD unavailabletext;

  MACRO NEW()
  {
    this->formvariables := CELL[...this->formvariables, formsubmittype := "new" ];

    // TODO Optimize/cache ?  We could've gotten this information when loading the form I think... or we should just do the formload here ?
    IF(ObjectExists(this->formcontext))
    {
      RECORD formsettings := OpenWHFSType("http://www.webhare.net/xmlns/publisher/formwebtool")->GetInstanceData(this->__form.webtoolformid);
      IF(NOT formsettings.disablecaptcha)
      {
        OBJECT formintegrationplugin := this->formcontext->GetPlugin("http://www.webhare.net/xmlns/publisher/siteprofile", "formintegration");
        IF(ObjectExists(formintegrationplugin) AND formintegrationplugin->usecaptcha)
          this->captchaquestion := this->AppendFormField(DEFAULT OBJECT, "http://www.webhare.net/xmlns/publisher/forms#captcha", "__webtoolform__captcha");

      }
      IF(formsettings.unavailable)
        this->unavailabletext := formsettings.unavailabletext;
    }

    IF(RecordExists(this->unavailabletext))
      RETURN;

    // Initialize the form handlers
    FOREVERY (RECORD handler FROM this->__form.handlers)
      INSERT CELL[...handler
                 , handlerobject := InstantiateFormHandler(this, handler, this->__form.webtoolformid)
                 ] INTO this->formhandlers AT END;

    IF(this->__form.objref != "")
      INSERT CELL "wh-form-objref" := this->__form.objref INTO this->formdataset;

    OBJECT targetformapplytester := GetApplyTesterForObject(this->__form.webtoolformid); //TODO we could cache endresult of this lookup while parsing formdef ?
    IF(ObjectExists(targetformapplytester))
    {
      RECORD targetformsettings := targetformapplytester->GetPluginConfiguration("http://www.webhare.net/xmlns/publisher/siteprofile","formintegration");
      IF(RecordExists(targetformsettings) AND targetformsettings.webtoolformhooks != "")
      {
        RECORD save__passthrough := __passthrough;
        TRY
        {
          __passthrough := [ form := this ];
          this->__directhookobject := MakeObject(targetformsettings.webtoolformhooks);

          IF(NOT (this->__directhookobject EXTENDSFROM WebtoolFormHooks))
            THROW NEW Exception(`Object '${targetformsettings.webtoolformhooks}' should derive from WebtoolFormHooks`);
        }
        FINALLY
        {
          __passthrough := save__passthrough;
        }
      }
    }
  }

  ///Returns true if the form is normally available
  PUBLIC BOOLEAN FUNCTION IsAvailable()
  {
    RETURN NOT RecordExists(this->unavailabletext);
  }

  //Prepare form for use in the frontend (either rendering or for result data)
  UPDATE PUBLIC MACRO PrepareForFrontend()
  {
     FormBase::PrepareForFrontend();

    // All handlers get a chance to run so they can still do prefill/data corrections which may affect storage/activation
    FOREVERY(RECORD handler FROM this->formhandlers)
      handler.handlerobject->PrepareForFrontend();

    OBJECT hookobject := this->__directhookobject;
    IF (ObjectExists(hookobject) AND hookobject EXTENDSFROM WebtoolFormHooks) //not a 'newstyle page base'
      hookobject->PrepareForFrontend();
  }

  // Let form handlers prefill the form fields before returning the witty date to print
  UPDATE PUBLIC RECORD FUNCTION GetWittyData()
  {
    IF(NOT this->__didpreparehandlers) //run them only once
    {
      FOREVERY (RECORD handler FROM this->formhandlers)
        IF(ObjectExists(handler.handlerobject))
          handler.handlerobject->PrepareRendering();

      OBJECT hookobject := this->__directhookobject;
      IF (ObjectExists(hookobject) AND hookobject EXTENDSFROM WebtoolFormHooks) //not a 'newstyle page base'
        hookobject->PrepareRendering();

      this->__didpreparehandlers := TRUE;
    }

    RETURN FormBase::GetWittyData();
  }

  // Request a form handler object
  PUBLIC OBJECT ARRAY FUNCTION GetHandlers(STRING handlertype)
  {
    IF(handlertype NOT LIKE "*#*")
      handlertype := "http://www.webhare.net/xmlns/publisher/forms#" || handlertype;

    OBJECT ARRAY results;
    FOREVERY(RECORD handler FROM this->formhandlers)
      IF(handler.handlertype = handlertype)
        INSERT handler.handlerobject INTO results AT END;

    RETURN results;
  }

  // Are we editing?
  UPDATE PUBLIC BOOLEAN FUNCTION IsEditing()
  {
    RETURN this->pvt_editguid != "";
  }

  // Let form handlers validate the form after the default validation is already done
  UPDATE PUBLIC MACRO __FormValidateHooks(OBJECT workobj)
  {
    FOREVERY (RECORD handler FROM this->formhandlers)
      handler.handlerobject->ValidateForm(workobj);

    OBJECT hookobject := this->__directhookobject;
    IF (ObjectExists(hookobject))
      hookobject->Validate(workobj);

    IF(ObjectExists(this->captchaquestion) AND NOT workobj->HasFailed())
      this->captchaquestion->ValidateCaptcha(workobj);
  }

  //Retrieve GUID but add some boobytraps against incorrect use
  PUBLIC STRING FUNCTION GetResultGuid()
  {
    IF(this->pvt_resultguid="")
      THROW NEW Exception("The result guid isn't available until submission has started");
    this->pvt_retrievedguid := TRUE;
    RETURN this->pvt_resultguid;
  }

  /** Set up the ID field and deduplicate existing submisisons if needed
      @param idvalue ID value to use (eg an email address or WRD GUID)
      @cell options.overwriteexisting If true, this submission replaces an earlier submission by this id (if EditExistingResult was not already invoked) */
  PUBLIC MACRO SetIDField(STRING idvalue, RECORD options DEFAULTSTO DEFAULT RECORD)
  {
    options := ValidateOptions( [ overwriteexisting := FALSE ], options);
    IF(this->pvt_retrievedguid AND options.overwriteexisting)
      THROW NEW Exception("Overwrite mode cannot be set once the result guid has been retrieved");

    this->pvt_idfieldvalue := idvalue;

    IF(this->pvt_editguid = "" AND options.overwriteexisting AND idvalue != "")
    {
      //Have we got an earlier submission?
      RECORD existingresult := SELECT guid
                                 FROM publisher.formresults
                                WHERE formresults.form_fsobject = this->__form.webtoolformid
                                      AND formresults.idfield = VAR idvalue
                                      AND formresults.status = VAR whconstant_formstatus_final;

      IF(RecordExists(existingresult))
      {
        this->pvt_editguid := existingresult.guid;
        this->pvt_resultguid := existingresult.guid;
      }
    }
  }

  /** List the current form handlers */
  PUBLIC RECORD ARRAY FUNCTION ListHandlers()
  {
    RETURN SELECT guid, handlertype, handlerobject, __handlertask := handlertask, __condition := condition FROM this->formhandlers;
  }

  /** Delete a handler by GUID. This will delete it for the current submission
      (if run in the submit handler) but not from the actual form file */
  PUBLIC MACRO DeleteHandlerByGUID(STRING guid)
  {
    INTEGER tokill := SELECT AS INTEGER #formhandlers + 1 FROM this->formhandlers WHERE formhandlers.guid = VAR guid;
    IF(tokill = 0)
      THROW NEW Exception(`No handler with guid '${guid}'`);
    DELETE FROM this->formhandlers WHERE formhandlers.guid = VAR guid;
  }

  PUBLIC MACRO __RenderThankYouFields(RECORD ARRAY richvalues)
  {
    FOREVERY (RECORD richvalue FROM richvalues)
    {
      RECORD field := SELECT * FROM this->__formfields WHERE ToUppercase(name) = ToUppercase(richvalue.field);
      IF (RecordExists(field))
        field.obj->__renderedmergefields := richvalue.value;
    }
  }

  PUBLIC MACRO __StraightToThankyouPage()
  {
    FOREVERY(RECORD page FROM this->__formpages)
    {
      IF(page.role = "thankyou") //first non normal page
        BREAK;
      //hide everything up to the first thankyou page.
      page.obj->visible := FALSE;
    }
  }

  PUBLIC MACRO RunServerSideConfirmation(STRING resultsguid, STRING formsubmittype) //confirm without a client present
  {
    this->formsubmittype := formsubmittype;
    this->pvt_resultguid := resultsguid;

    RECORD results := this->formresults->GetSingleResult(resultsguid, [__form := this, allowpending := TRUE, allowcancelled := TRUE ]);
    IF(NOT this->__FinalizeResult(resultsguid, formsubmittype))
      RETURN;

    //NOTE this GetSingleResult also fills our local fields
    this->__RunPostStorageHandlers(this->__GetActiveHandlers(), results.extradata);
  }

  BOOLEAN FUNCTION __FinalizeResult(STRING confirmguid, STRING formsubmittype)
  {
    IF(formsubmittype NOT IN ["cancel","confirm"])
      THROW NEW Exception(`Invalid formsubmittype '${formsubmittype}' for confirmation`);

    RECORD match :=
        SELECT id
             , status
             , results := ReadAnyFromDatabase(results, blobresults)
          FROM publisher.formresults
         WHERE form_fsobject = this->__form.webtoolformid
               AND COLUMN guid = confirmguid
               AND form_name = "webtoolform";

    IF(NOT RecordExists(match))
      THROW NEW Exception(`Cannot find entry '${confirmguid}' to confirm`);
    IF(formsubmittype = "cancel" AND match.status != whconstant_formstatus_pendingconfirmation) //can only cancel something that was pending (5)
    {
      IF(match.status = whconstant_formstatus_final) //going back from approved to failed? (eg. payment resubmission but now to 'failed')
        RETURN FALSE; //TODO escalate to form owner or sysop that a payment has suddenly gone to failed. nothing we can really do
      THROW NEW Exception(`Entry #${match.id} ('${confirmguid}') is not pending but in status ${match.status}`);
    }
    IF(formsubmittype = "confirm" AND match.status NOT IN [whconstant_formstatus_cancelled, whconstant_formstatus_pendingconfirmation]) //but we *do* allow you to confirm anyway even after cancelling first. eg payment flows can do that
      THROW NEW Exception(`Entry #${match.id} ('${confirmguid}') is not pending but in status ${match.status}`);

    // Update the result's submit type
    IF (CellExists(match.results, "__formmeta")
        AND CellExists(match.results.__formmeta, "submittype")
        AND match.results.__formmeta.submittype != formsubmittype)
    {
      match.results.__formmeta.submittype := formsubmittype;
      RECORD storeresults := PrepareAnyForDatabase(match.results);
      UPDATE publisher.formresults
         SET results := storeresults.stringpart
           , blobresults := storeresults.blobpart
       WHERE id = match.id;
    }

    IF(formsubmittype = "confirm")
    {
      UPDATE publisher.formresults SET confirmed := GetCurrentDatetime()
                                     , status := VAR whconstant_formstatus_final
                                 WHERE id = match.id;
    }
    ELSE
    {
      UPDATE publisher.formresults SET modified := GetCurrentDatetime()
                                     , status := VAR whconstant_formstatus_cancelled
                                 WHERE id = match.id;
    }
    RETURN TRUE;
  }

  RECORD ARRAY FUNCTION __GetActiveHandlers()
  {
    RETURN SELECT * FROM this->formhandlers WHERE this->MatchFormCondition(condition, DEFAULT RECORD).success;
  }

  MACRO __RunPostStorageHandlers(RECORD ARRAY activehandlers, RECORD extradata)
  {
    FOREVERY (RECORD handler FROM activehandlers)
    {
      IF(handler.handlerobject->IsConfirmationHandler())
        CONTINUE;
      IF(this->formsubmittype = "cancel" AND NOT handler.handlerobject->IsCancelHandler())
        CONTINUE;
      IF(this->formsubmittype != "cancel" AND NOT handler.handlerobject->IsSubmitHandler())
        CONTINUE;
      IF(handler.handlertask = "")
        CONTINUE;

      ScheduleManagedTask(handler.handlertask, CELL[ formid := this->__form.webtoolformid
                                                   , formname := this->__form.name
                                                   , formhandler := handler.guid
                                                   , resultsguid := this->pvt_editguid ?? this->pvt_resultguid
                                                   , submittype := this->formsubmittype
                                                   ],
                                                   [ auxdata := CELL[ extradata ] ]);
    }
  }

  // Let the form handlers process the form results after submission
  RECORD FUNCTION Submit(RECORD extradata)
  {
    IF(NOT this->IsAvailable())
      THROW NEW Exception("This form is currently unavailable");

    // All handlers get a chance to run so they can still do prefill/data corrections which may affect storage/activation
    FOREVERY(RECORD handler FROM this->formhandlers)
      handler.handlerobject->PrepareResultData();

    STRING mutexname := "publisher:form-" || this->__form.webtoolformid; //we use the lock to dedupe submissions
    OBJECT work := this->BeginWork( [ mutex := mutexname ] );

    this->pvt_resultguid := this->pvt_editguid ?? GenerateUFS128BitId();

    /* If the validation hasn't failed, run the WebtoolFormPage submit handler. This is done before confirmationhandler
       by convention (it might have theoretically made more sense to run this after confirmation handlers, but we decided
       not to and users rely on that) */
    RECORD result;
    IF (NOT work->HasFailed())
    {
      OBJECT hookobject := this->__directhookobject;
      IF (ObjectExists(hookobject))
        result := hookobject->Submit(work, extradata);
    }

    RECORD ARRAY activehandlers := this->__GetActiveHandlers();
    FOREVERY(RECORD handler FROM activehandlers)
      IF (NOT work->HasFailed())
        handler.handlerobject->PrepareSubmit(work);

    // Run the e-mail confirmation task first before we actually start submitting things
    RECORD confirmationhandler := SELECT * FROM activehandlers WHERE handlerobject->IsConfirmationHandler();
    IF (RecordExists(confirmationhandler) AND NOT work->HasFailed())
    {
      IF (this->formsubmittype = "new")
      {
        this->formsubmittype := "pending";

        IF (NOT work->HasFailed())
        {
          IF(confirmationhandler.handlertask != "")
            ScheduleManagedTask(confirmationhandler.handlertask,
                                CELL[ formid := this->__form.webtoolformid
                                    , formname := this->__form.name
                                    , formhandler := confirmationhandler.guid
                                    , resultsguid := this->pvt_resultguid
                                    , submittype := this->formsubmittype
                                    ],
                                [ auxdata := CELL[ extradata ] ]);
        }
      }
      ELSE IF (this->formsubmittype IN [ "confirm", "duplicate" ])
      {
        //We're submitting from __MaybeConfirmResult
        this->__StraightToThankyouPage();
      }
    }

    // If the WebtoolFormPage submit handler hasn't failed and this isn't a duplicate submission, run the store results handler
    IF (NOT work->HasFailed() AND this->formsubmittype = "confirm")
    {
      this->__FinalizeResult(this->pvt_editguid, this->formsubmittype);
    }
    ELSE IF (NOT work->HasFailed() AND this->formsubmittype != "duplicate")
    {
      // The store handler is processed inline
      this->StoreFormValue(CELL[ idfieldvalue := this->pvt_idfieldvalue
                               , guid := this->pvt_resultguid
                               , isedit := this->pvt_editguid != ""
                               , ispending := this->formsubmittype = "pending"
                               , extradata
                               , __formsubmittype := this->formsubmittype
                               ]);
    }

    IF (this->formsubmittype NOT IN [ "pending", "duplicate" ] // don't run handlers for pending or duplicate submissions
        AND NOT work->HasFailed())
    {
      // If the store handler hasn't failed, schedule a task for each handler
      this->__RunPostStorageHandlers(activehandlers, extradata);
    }

    // Submission was successful if there are no errors
    BOOLEAN success := work->Finish();
    IF (NOT success)
      RETURN DEFAULT RECORD;

    result := CELL[ ...result
                  , resultsguid := this->pvt_resultguid
                  , submittype := this->formsubmittype
                  ];

    IF (this->formsubmittype NOT IN [ "pending", "duplicate" ])
    {
      //NOW aftersubmit handlers can do their thing
      FOREVERY (RECORD handler FROM activehandlers)
        result := handler.handlerobject->UpdateResultAfterSubmit(result);
    }
    ELSE IF(this->formsubmittype = "pending")
    {
      IF (RecordExists(confirmationhandler))
        result := confirmationhandler.handlerobject->UpdateResultPendingSubmit(result);
    }

    IF (RecordExists(confirmationhandler) AND this->formsubmittype IN ["pending","confirm","duplicate"])
    {
      RECORD results := this->formresults->GetSingleResult(this->pvt_resultguid, [ __form := this, allowpending := TRUE ]);
      confirmationhandler.handlerobject->RunConfirmationUpdates(results);
    }

    // Merge result fields into result page RTD's
    INSERT CELL richvalues := this->MergeThankYouPageRTDFields(extradata) INTO result;

    // If this is a confirmation or a duplicate submission, the form is opened on the thankyou page, without being submitted,
    // so we'll pre-render the RTD contents, filling the merge fields before rendering the form
    IF (this->formsubmittype IN [ "confirm", "duplicate" ])
    {
      this->__RenderThankYouFields(result.richvalues);
    }

    RETURN result;
  }

  PUBLIC MACRO PTR FUNCTION GetFormBody()
  {
    RETURN PTR this->RenderForm();
  }

  // Cancel an earlier submission
  PUBLIC MACRO CancelExistingResult(STRING resultsguid)
  {
    this->pvt_resultguid := resultsguid;
    this->formresults->CancelResult(resultsguid);
    this->formvariables := CELL[...this->formvariables, formsubmittype := "cancel" ];
    this->formsubmittype := "cancel";

    RECORD results := this->formresults->GetSingleResult(resultsguid, [__form := this, allowcancelled := TRUE]);
    this->__RunPostStorageHandlers(this->__GetActiveHandlers(), results.extradata);
    this->__StraightToThankyouPage();
  }

  PUBLIC BOOLEAN FUNCTION EditExistingResult(STRING resultsguid)
  {
    IF(NOT this->PrefillWithResults(resultsguid))
      RETURN FALSE;

    this->formvariables := CELL[...this->formvariables, formsubmittype := "change" ];
    this->pvt_editguid := resultsguid;
    this->formsubmittype := "change";
    RETURN TRUE;
  }

  PUBLIC BOOLEAN FUNCTION PrefillWithResults(STRING resultsguid)
  {
    RECORD results := this->formresults->GetSingleResult(resultsguid, [__form := this ]);
    IF (NOT RecordExists(results))
      RETURN FALSE;

//    this->PrefillWithSingleResult(results); //FIXME the above GetSingleResult already loaded into us, so I don't think we need this anymore...
    RETURN TRUE;
  }

  PUBLIC BOOLEAN FUNCTION __MaybeConfirmResult()
  {
    STRING confirm := GetFormWebVariable("confirm");
    IF (confirm = "")
      RETURN TRUE;

    RECORD confirmdata;
    TRY
      confirmdata := ValidateOptions(
          [ r := "" // result guid
          , h := "" // handler guid
          ], DecodeHSON(DecryptForThisServer("publisher:confirmation", confirm)));
    CATCH;
    IF (NOT RecordExists(confirmdata))
      RETURN FALSE;

    STRING resultguid := confirmdata.r;
    STRING handlerguid := confirmdata.h;

    RECORD confirmationhandler := SELECT * FROM this->formhandlers WHERE guid = VAR handlerguid;
    IF (NOT RecordExists(confirmationhandler) OR NOT confirmationhandler.handlerobject->IsConfirmationHandler())
      RETURN TRUE;

    RECORD results := this->formresults->GetSingleResult(resultguid, [ __form := this, allowpending := TRUE, allowcancelled := TRUE ]);
    IF (NOT RecordExists(results) OR results.status NOT IN [ "pending", "final", "cancelled"])
      RETURN FALSE;

    this->pvt_editguid := VAR resultguid;

    IF (results.submittype = "new") //not yet in pending..
    {
      this->formsubmittype := confirmationhandler.handlerobject->DetermineNewSubmissionStatus(results, this->formresults);
    }
    ELSE
    {
      this->formsubmittype := results.status = "cancelled" ? "cancel" : "confirm";
    }

    this->formvariables := CELL[...this->formvariables, formsubmittype := this->formsubmittype ];

    // The captcha field is not required when the confirmation link is used
    this->captchaquestion := DEFAULT OBJECT;

    IF(results.status = "pending" AND this->formsubmittype != "pending")
    {
      this->FormExecuteSubmit();
    }
    ELSE //don't resubmit, but still go straight to 'thank you'
    {
      this->__StraightToThankyouPage();
      confirmationhandler.handlerobject->RunConfirmationUpdates(results);
      this->__RenderThankYouFields(this->MergeThankYouPageRTDFields());
    }

    RETURN TRUE;
  }

  OBJECT FUNCTION GetFormResults()
  {
    IF (NOT ObjectExists(this->pvt_results))
    {
      //RECORD ARRAY fields := this->ListFields();
      OBJECT whfsobject := OpenWHFSObject(this->__form.webtoolformid);
      this->pvt_results := OpenFormFileResults(whfsobject);
    }
    RETURN this->pvt_results;
  }

  RECORD ARRAY FUNCTION MergeThankYouPageRTDFields(RECORD extradata DEFAULTSTO DEFAULT RECORD)
  {
    RECORD ARRAY richvalues;
    IF (MemberExists(this->formcontext, "__mergefields"))
    {
      OBJECT formdefinition := OpenFormFileDefinition(OpenWHFSObject(this->__form.webtoolformid), [ formname := this->__form.name ]);
      RECORD mergedata := this->GetWittyResultData();
      OBJECT mergefields := NEW MergeFieldsContext([ type := "formdef", formdef := formdefinition, data := mergedata, applytester := this->formcontext->targetapplytester ], this->formlanguagecode);
      mergefields->extradata := extradata;

      OBJECT oldmergefields := this->formcontext->__mergefields;
      this->formcontext->__mergefields := mergefields;

      OBJECT webdesign;
      FOREVERY (RECORD page FROM SELECT * FROM this->__form.pages WHERE role = "thankyou")
      {
        FOREVERY (RECORD field FROM SELECT * FROM page.fields WHERE qname = "http://www.webhare.net/xmlns/publisher/forms#richtext" AND mayhavemergefields)
        {
          OBJECT fieldobj := GetMember(this, "^" || field.name);
          IF (fieldobj->IsNowVisible() AND RecordExists(field.richvalue))
          {
            IF(NOT ObjectExists(webdesign)) //TODO: avoid opening full webdesign if possible. perhaps if there are no witty fields or no embedded objects ?
            {
              RECORD webdesigninfo := this->formcontext->targetapplytester->GetWebDesignObjinfo();
              webdesign := InstantiateWebDesign(this->formcontext->targetapplytester, webdesigninfo, 0, DEFAULT OBJECT, DEFAULT OBJECT, FALSE);
              webdesign->__mergefields := this->formcontext->__mergefields;
            }

            BLOB richcontent := GetPrintedAsBlob(PTR webdesign->CallWithScope(PTR webdesign->RenderRTD(field.richvalue.data)));
            INSERT [ field := field.name, value := BlobToString(richcontent) ] INTO richvalues AT END;
          }
        }
      }
      this->formcontext->__mergefields := oldmergefields;
    }
    RETURN richvalues;
  }
>;
