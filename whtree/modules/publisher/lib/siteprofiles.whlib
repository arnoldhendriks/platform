﻿<?wh
/** @topic siteprofiles/api */

LOADLIB "wh::internet/urls.whlib";

LOADLIB "mod::publisher/lib/internal/siteprofiles/cache.whlib" EXPORT DescribeContentTypeById;
LOADLIB "mod::publisher/lib/internal/siteprofiles/reader.whlib" EXPORT GetApplyTesterForObject, GetApplyTesterForFakeObject;
LOADLIB "mod::publisher/lib/internal/webdesign/webcontext.whlib";
LOADLIB "mod::publisher/lib/publisher.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/internal/modules/defreader.whlib";
LOADLIB "mod::system/lib/resources.whlib";

/** @private Only two callers left and its pretty low level, so we should just consider just deprecating it (at least no more new callers)
    @short Open site profile by name
    @long This is the preferred API, as it supports all siteprofile names including those originating from modules
*/
PUBLIC OBJECT FUNCTION OpenSiteProfile(STRING name)
{
  RETURN NEW SiteProfileObject(name);
}

/** @short Open and prepare a webdesign
    @param objectid Target object for which we're opening the webdesign
    @return The webdesign, or a DEFAULT OBJECT if no webdesign applies to the specified object id */
PUBLIC OBJECT FUNCTION GetWebDesign(INTEGER objectid)
{
  OBJECT applytester := GetApplyTesterForObject(objectid);
  IF (NOT ObjectExists(applytester))
    RETURN DEFAULT OBJECT;

  RECORD webdesigninfo := applytester->GetWebDesignObjinfo();
  IF(NOT RecordExists(webdesigninfo))
    RETURN DEFAULT OBJECT;

  RETURN InstantiateWebDesign(applytester, webdesigninfo, 0, DEFAULT OBJECT, DEFAULT OBJECT, FALSE);
}

/** @short Get only the plugin loader for a specific url */
PUBLIC OBJECT FUNCTION GetWebContextForURL(STRING url, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions([ clientwebserver := -1 ], options);
  RECORD urlinfo := LookupPublisherURL(url, options);
  IF(urlinfo.folder = 0) //Not sure how to handle this, but we have no way to figure out your plugins. Give up
    THROW NEW Exception(`Cannot resolve the URL '${url}' to a published folder`);

  OBJECT wdpr := NEW WebContext;
  wdpr->baseurl := url;

  INTEGER objid := urlinfo.file ?? urlinfo.folder;
  OBJECT applytester := GetApplyTesterForObject(objid);
  wdpr->__SetObjectId(objid, applytester);
  wdpr->__SetAvailablePlugins(applytester->GetWebDesignPlugins());
  RETURN wdpr;
}

/// Get the webdesign for a URL
PUBLIC OBJECT FUNCTION GetWebDesignForURL(STRING url)
{
  RECORD urlinfo := LookupPublisherURL(url);
  IF(urlinfo.folder = 0) //Not sure how to handle this, but we have no way to figure out your plugins. Give up
    THROW NEW Exception(`Cannot resolve the URL '${url}' to a published folder`);

  STRING resolvedurlpath;
  INTEGER objid := urlinfo.file ?? urlinfo.folder;
  IF (objid != 0)
  {
    STRING resolvedurl := SELECT AS STRING COLUMN objecturl FROM system.fs_objects WHERE id = objid;
    resolvedurlpath := resolvedurl = "" ? "" : UnpackURL(resolvedurl).urlpath;
  }

  RECORD savepageinfo := __pageinfo;
  __pageinfo := __SplitBaseAndSubUrl(url, resolvedurlpath);

  TRY
  {
    OBJECT webdesign := GetWebDesign(urlinfo.file ?? urlinfo.folder);
    RETURN webdesign;
  }
  FINALLY
  {
    __pageinfo := savepageinfo;
  }
}

/** @short Get only the plugin loader for a webdesign */
PUBLIC OBJECT FUNCTION __GetWebContextForApplyTester(OBJECT applytester, INTEGER objid)
{
  OBJECT wdpr := NEW WebContext;

  wdpr->__SetObjectId(objid, applytester);
  wdpr->__SetAvailablePlugins(applytester->GetWebDesignPlugins());
  IF(objid != 0)
    wdpr->baseurl := SELECT AS STRING objecturl FROM system.fs_objects WHERE id=objid;
  RETURN wdpr;
}

/** @short Get only the plugin loader for a webdesign */
PUBLIC OBJECT FUNCTION GetWebContext(INTEGER objid)
{
  OBJECT applytester := GetApplyTesterForObject(objid);
  IF(NOT ObjectExists(applytester))
    THROW NEW Exception("Unable to locate the webdesign for #" || objid);

  RETURN __GetWebContextForApplyTester(applytester, objid);
}

/** @short Get the language code for the specified objet, as specified by <sitelanguage> in the site profile */
PUBLIC STRING FUNCTION GetSiteLanguage(INTEGER objectid)
{
  OBJECT applytester := GetApplyTesterForObject(objectid);
  IF(NOT ObjectExists(applytester))
    THROW NEW Exception("Unable to locate the webdesign for #" || objectid);

  RETURN applytester->GetSiteLanguage();
}

/** @short Gather custom siteprofile settings for an object */
PUBLIC RECORD ARRAY FUNCTION GetCustomSiteProfileSettings(STRING ns, STRING localname, INTEGER fsobjectid)
{
  OBJECT applytester := GetApplyTesterForObject(fsobjectid);
  IF(NOT ObjectExists(applytester))
    RETURN DEFAULT RECORD ARRAY;
  RETURN applytester->GetCustomSettings(ns,localname);
}

PUBLIC RECORD ARRAY FUNCTION GetAvailableWebDesigns(BOOLEAN gettemplates)
{
  RECORD ARRAY sitedesigns;
  RECORD ARRAY mods := GetWebHareModules();

  FOREVERY(RECORD mod FROM mods)
    FOREVERY(RECORD design FROM mod.webdesigns)
    {
      IF(design.istemplate != gettemplates)
        CONTINUE;

      INSERT [ title := design.title ?? `:${mod.name}:${design.name}` //our callers expected a tid, so prefix with :
             , rowkey := mod.name || ":" || design.name
             , siteprofiles := design.siteprofile != "" ? STRING[design.siteprofile] : STRING[]
             , designroot := design.designroot
             , hidden := design.hidden
             ] INTO sitedesigns AT END;
    }

  FOREVERY(RECORD mod FROM mods)
    FOREVERY(RECORD adddesign FROM mod.addtowebdesigns)
    {
      INTEGER match := (SELECT AS INTEGER #sitedesigns + 1 FROM sitedesigns WHERE rowkey = adddesign.webdesign) - 1;
      IF(match >= 0)
        sitedesigns[match].siteprofiles := sitedesigns[match].siteprofiles CONCAT STRING[adddesign.siteprofile];
    }

  RETURN sitedesigns;
}

PUBLIC RECORD ARRAY FUNCTION GetAvailableWebFeatures()
{
  RECORD ARRAY webfeatures := __GetExtractedConfig("webdesigns").webfeatures;
  RETURN SELECT title := title ?? `:${name}` //our callers expected a tid, so prefix with :
              , rowkey := name
              , siteprofile
              , webdesignmasks := webdesignmasks ?? ["*"]
              , hidden
           FROM webfeatures;
}

PUBLIC MACRO __ValidateWebdesign(OBJECT applytester)
{
  STRING ARRAY webdesigntoks := Tokenize(applytester->webdesign,':');
  IF(NOT IsModuleInstalled(webdesigntoks[0]))
    THROW NEW Exception(`The module for webdesign '${webdesigntoks[0]}' is not installed`);

}
