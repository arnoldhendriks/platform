﻿<?wh

LOADLIB "wh::adhoccache.whlib";
LOADLIB "wh::util/algorithms.whlib";

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/whfs.whlib";
LOADLIB "mod::system/lib/internal/support.whlib";
LOADLIB "mod::system/lib/internal/whfs/support.whlib";

LOADLIB "mod::publisher/lib/internal/files.whlib";
LOADLIB "mod::publisher/lib/internal/tollium-helpers.whlib";
LOADLIB "mod::publisher/lib/internal/siteprofiles/cache.whlib";
LOADLIB "mod::publisher/lib/internal/siteprofiles/reader.whlib";

LOADLIB "mod::tollium/lib/componentbase.whlib" EXPORT TolliumComponentBase;
LOADLIB "mod::tollium/lib/screenbase.whlib" EXPORT TolliumFragmentBase;


OBJECT hooks;

//Cache only the data needed for fast fsobject overiews
RECORD FUNCTION GetCacheableFSObjectOverviewInfo()
{
  RECORD csp := GetCachedSiteProfiles();

  INTEGER ARRAY hiddenfoldertypes := csp.hiddenfoldertypes;
  RECORD ARRAY typemap := SELECT id
                               , foldertype := RecordExists(foldertype)
                               , tolliumicon
                               , candownload := RecordExists(filetype) AND filetype.blobiscontent
                               , ispublishable := RecordExists(filetype) AND filetype.ispublishable
                            FROM csp.contenttypes
                           WHERE RecordExists(filetype) OR RecordExists(foldertype)
                        ORDER BY id, RecordExists(foldertype);

  RETURN [ value := CELL[ hiddenfoldertypes, typemap ]
         , ttl :=          60 * 60 * 1000 // 1 hour
         , eventmasks :=   [ "publisher:internal.siteprofiles.recompiled" ]
         ];
}
RECORD FUNCTION GetFSObjectOverviewInfo()
{
  RETURN GetAdhocCached(CELL[type := "fsobjectoverviewinfo"], PTR GetCacheableFSObjectOverviewInfo);
}

PUBLIC RECORD ARRAY FUNCTION EnrichObjectListing(OBJECT tolliumuser, RECORD ARRAY objects, BOOLEAN hidden_too, STRING contentmode)
{
  // ADDME: is it possible to (cheaply) determine if the parentfolder is 12 (webhare-modules) to properly hide them?
/*
  objects := SELECT objects.*
                  , fs_objects.parent
               FROM objects, system.fs_objects
              WHERE objects.id=fs_objects.id
                    AND fs_objects.id IN (SELECT AS INTEGER ARRAY id FROM objects);
/*
  INTEGER ARRAY uniqueparents := SELECT AS INTEGER ARRAY DISTINCT parent FROM objects ;
  INTEGER ARRAY writableparents;
  FOREVERY(INTEGER id FROM uniqueparents)
    IF(tolliumuser->HasRightOn("system:fs_fullaccess", id))
      INSERT id INTO writableparents AT END;

/*  objects := SELECT objects.*
                  , parent
               FROM objects, system.fs_objects

  RECORD ARRAY parentinfo := SELECT id
                                  , writable := tolliumuser->HasRightOn("system:fs_fullaccess", id)

                  //, pvt_parentwriteall := (fs_objects.parent != parentfolder.id ? tolliumuser->HasRightOn("system:fs_fullaccess", parent) : parentwriteaccess)
*/
  IF(contentmode = "recyclebin")
  {
    RECORD ARRAY org_objects := objects;

    RECORD ARRAY recycleinfo := SELECT fs_object, currentname, currentparent
                                  FROM system.fs_history
                                 WHERE fs_object IN (SELECT AS INTEGER ARRAY id FROM objects)
                                       AND type = 0;

    BOOLEAN canrestoreall := tolliumuser->HasRightOn("system:fs_fullaccess", 0);
    INTEGER ARRAY canrestoreparents;
    IF(NOT canrestoreall)
    {
      RECORD ARRAY uniqueparents := SELECT DISTINCT currentparent FROM recycleinfo;
      canrestoreparents := SELECT AS INTEGER ARRAY currentparent FROM uniqueparents WHERE tolliumuser->HasRightOn("system:fs_fullaccess", currentparent);
    }

    objects := SELECT objects.*
                    , name := recycleinfo.currentname
                    , canrestore := VAR canrestoreall OR recycleinfo.currentparent IN canrestoreparents
                 FROM objects, recycleinfo
                WHERE objects.id = recycleinfo.fs_object;


  }
  ELSE
  {
    objects := SELECT *
                    , canrestore := FALSE
                 FROM objects;
  }

  INTEGER ARRAY sitecandidates := SELECT AS INTEGER ARRAY id FROM objects WHERE isfolder;
  RECORD ARRAY sitelist := SELECT id, locked FROM system.sites WHERE sites.id IN sitecandidates ORDER BY id;

  //note: lastpublishdate is only needed by filemgr 'fake' publish statuses
  RECORD ARRAY fsdata := SELECT id, parent, parentsite, published, isactive, ordering, ispinned, lastpublishdate
                           FROM system.fs_objects
                          WHERE id IN (SELECT AS INTEGER ARRAY id FROM objects)
                       ORDER BY id;

  // Add tollium-specific extra fields (e.g. icons)
  RECORD overinfo := GetFSObjectOverviewInfo();

  // Add access fields and filter out hidden objects
  objects := SELECT TEMPORARY fsdatapos := RecordLowerBound(fsdata, objects, [ "ID"])
                  , *
                  , __fsdata := fsdatapos.found ? fsdata[fsdatapos.position] : DEFAULT RECORD
               FROM objects
              WHERE hidden_too OR (type NOT IN overinfo.hiddenfoldertypes);

  objects := SELECT TEMPORARY sitepos := RecordLowerBound(sitelist, objects, [ "ID" ])
                  , TEMPORARY islockedsite := sitepos.found AND sitelist[sitepos.position].locked
                  , id
                  , name
                  , title
                  , filterscreen
                  , filtersettings
                  , canwrite
                  , canadd :=       isfolder AND type != 1 AND canwrite AND NOT islockedsite
                  , candelete :=    RecordExists(__fsdata) ? __fsdata.parent != 18 AND candelete : candelete // can't delete from versioning archive
                  , issite :=       sitepos.found
                  , islockedsite := islockedsite
                  , iswithinsite := (RecordExists(__fsdata) AND __fsdata.parentsite != 0)
                  , published :=    RecordExists(__fsdata) ? __fsdata.published : 0
                  , parentsite :=   RecordExists(__fsdata) ? __fsdata.parentsite : CellExists(objects, 'parentsite') ? objects.parentsite : 0
                  , isactive :=     RecordExists(__fsdata) ? __fsdata.isactive : CellExists(objects, "ISACTIVE") ? isactive : TRUE
                  , ordering :=     RecordExists(__fsdata) ? __fsdata.ordering : 0
                  , isfolder
                  , ispinned :=     RecordExists(__fsdata) ? __fsdata.ispinned : TRUE
                  , lastpublishdate := RecordExists(__fsdata) ? __fsdata.lastpublishdate : DEFAULT DATETIME
                  , type
                  , modificationdate
                  , creationdate
                  , canrestore
                  , objects.contentmode
                  , isvirtual :=    id <= 0
                  , pvt_iconname
                  , modulekey
                  , contentshandler
               FROM objects;

  objects := SELECT TEMPORARY posinfo := RecordLowerBound(overinfo.typemap, CELL[ id := type, foldertype := isfolder ], ["ID", "FOLDERTYPE"])
                  , TEMPORARY typeinfo := posinfo.found ? overinfo.typemap[posinfo.position] : DEFAULT RECORD
                  , *
                  , candownload := RecordExists(typeinfo) AND typeinfo.candownload
                  , ispublishable := RecordExists(typeinfo) AND typeinfo.ispublishable
                  , pvt_iconname := pvt_iconname ?? GetFSObjectTolliumIcon(objects, typeinfo)
               FROM objects;

  // Add drag info
  RETURN
      SELECT *
           , draginfo := id > 0
                ? [ type :=     isfolder ? "system:whfsfolder" : "system:whfsfile"
                  , data :=
                        [ filename :=     name
                        , mimetype :=     "application/octet-stream"
                        , id :=           id
                        , canmove :=      candelete //ADDME OR canrestore  - reenable tihs as soon as we've got time to test it.
                        , candownload :=  candownload
                        , isactive :=     isactive
                        ]
                  ]
                : DEFAULT RECORD
        FROM objects;
}

PUBLIC RECORD ARRAY FUNCTION GetInnerObjectListing(OBJECT tolliumuser, INTEGER parentfolder, BOOLEAN showfolders, BOOLEAN showexternal, BOOLEAN showfiles, BOOLEAN listspecificobjects, INTEGER ARRAY getobjects, STRING contentmode)
{
  IF(NOT showfolders AND NOT showfiles)
    RETURN DEFAULT RECORD ARRAY;

  BOOLEAN parentfullaccess;
  BOOLEAN showlimitedroots;

  IF(NOT listspecificobjects AND parentfolder = 0 AND NOT tolliumuser->HasRightOn("system:fs_browse",0)) //limited publisher view
  {
    // parentfolder = 0 and user doesn't have browse rights on root. Show the roots the user has rights on.
    listspecificobjects := TRUE;
    getobjects := SELECT AS INTEGER ARRAY id
                    FROM tolliumuser->GetObjectsChildren("system:fs_objects", 0, FALSE);
    showlimitedroots := TRUE; //if we're showing a rightslimited list, never show recyclebin etc
  }

  INTEGER ARRAY tohide := GetWHFSObjectsToHide(tolliumuser);
  RECORD ARRAY objects := SELECT id
                               , parent
                               , name
                               , title
                               , ispinned
                               , isfolder
                               , type
                               , modificationdate
                               , creationdate
                               , filterscreen := ""
                               , filtersettings := DEFAULT RECORD
                               , pvt_iconname := ""
                               , modulekey := ""
                               , contentshandler := 0
                            FROM system.fs_objects
                           WHERE (listspecificobjects ? id IN getobjects : parent = parentfolder)
                                 AND (showfolders = TRUE AND showfiles = FALSE ? isfolder = TRUE : TRUE)
                                 AND (showfolders = FALSE AND showfiles = TRUE ? isfolder = FALSE : TRUE)
                                 AND (showexternal = FALSE ? type != 1 : TRUE )
                                 AND (showlimitedroots ? isactive = TRUE : TRUE)
                                 AND id NOT IN tohide;

  IF(showlimitedroots)
    objects := GetUniqueNamesForRoots(objects);

  //Now figure out canwrite & candelete.
  INTEGER ARRAY writableparents;
  IF(contentmode != "recyclebin")
    writableparents := SELECT AS INTEGER ARRAY parent FROM (SELECT DISTINCT parent FROM objects) WHERE tolliumuser->HasRightOn("system:fs_fullaccess", parent);

  objects := SELECT *
                  , canwrite := parent IN writableparents
                  , candelete := parent IN writableparents AND NOT ispinned
                  , DELETE ispinned
               FROM objects;

  IF(contentmode != "recyclebin")
  {
    INTEGER ARRAY unwriteableobjects := SELECT AS INTEGER ARRAY id FROM objects WHERE NOT canwrite;
    IF(Length(unwriteableobjects) > 0)
    {
      //After parent-level checks, some objects are still readonly. But we might have had direct access to them, so query that...
      INTEGER ARRAY writableobjects := tolliumuser->HasRightOnMultiple("system:fs_fullaccess", unwriteableobjects);
      IF(Length(writableobjects) > 0)
        UPDATE objects SET canwrite := TRUE WHERE id IN writableobjects;
    }
  }

  IF (listspecificobjects)
    objects := SELECT * FROM objects ORDER BY SearchElement(getobjects, id);

  RETURN objects;
}

PUBLIC OBJECTTYPE FSObjectOverviewBase EXTEND TolliumFragmentBase
<
  STRING ARRAY defaultflags; // Default flags, which are always available
  RECORD ARRAY contentshandlercache;

  MACRO NEW()
  {
    //this->invisibletitle := TRUE;
    this->defaultflags := [ "isactive", "isroot", "islockedsite", "issite", "isfolder"
                          , "candownload", "canwrite", "candelete", "canadd", "canrestore"
                          , "selectable", "isvirtual"
                          ];
  }

  MACRO ReloadOverview()
  {
  }

  PUBLIC MACRO ShowFolder(INTEGER folderid)
  {
    //FIXME set proper folder GetFolderSettings
    OBJECT applytester := folderid > 0 ? GetApplyTesterForObject(folderid) : DEFAULT OBJECT;
    this->__SetFolder(folderid, ObjectExists(applytester) ? applytester->GetFolderSettings() : GetDefaultFolderSettings());
  }

  RECORD ARRAY FUNCTION GetObjectListing(INTEGER parentfolderid, BOOLEAN showfolders, BOOLEAN showexternal, BOOLEAN showfiles, BOOLEAN hidden_too, STRING contentmode, BOOLEAN specificobjects, INTEGER ARRAY specificobjectids)
  {
    IF(parentfolderid < 0 )
      THROW NEW Exception("GetObjectListing does not support internal folder #" || parentfolderid);

    RECORD ARRAY baselist := SELECT *
                                  , extension := DEFAULT OBJECT
                                  , contentmode := ""
                                  , tag := ""
                                  , filterscreen := ""
                                  , filtersettings := DEFAULT RECORD
                               FROM GetInnerObjectListing(this->owner->tolliumuser, parentfolderid, showfolders, showexternal, showfiles, specificobjects, specificobjectids, contentmode);

    RETURN EnrichObjectListing(this->owner->tolliumuser, baselist, hidden_too, contentmode);
  }

  OBJECT FUNCTION GetCustomContentsHandler(INTEGER folderid)
  {
    IF (NOT ObjectExists(this->owner->contexts->^__contentshandlercontext))
      RETURN DEFAULT OBJECT;
    RETURN this->owner->contexts->^__contentshandlercontext->GetContentsHandler(folderid);
  }

  BOOLEAN FUNCTION CustomObjectHasChildren(INTEGER parentid, BOOLEAN forfoldertree, BOOLEAN showfolders, BOOLEAN showfiles, BOOLEAN hidden_too)
  {
    OBJECT contentshandler := this->GetCustomContentsHandler(parentid);
    IF (ObjectExists(contentshandler))
    {
      RECORD options := CELL
          [ showhidden := hidden_too
          ];
      RETURN contentshandler->HasChildren(parentid, options);
    }
    RETURN FALSE;
  }

  RECORD ARRAY FUNCTION GetCustomObjectListing(INTEGER parentid, BOOLEAN forfoldertree, BOOLEAN showfolders, BOOLEAN showfiles, BOOLEAN hidden_too, RECORD filterresults)
  {
    OBJECT contentshandler := this->GetCustomContentsHandler(parentid);
    IF (NOT ObjectExists(contentshandler))
      RETURN RECORD[];

    RECORD options := CELL
        [ tree := forfoldertree
        , showfolders
        , showfiles
        , showhidden := hidden_too
        , filterresults
        ];

    RECORD ARRAY objects := contentshandler->OnGetChildren(parentid, options);
    INTEGER ARRAY hideids := GetWHFSObjectsToHide(this->owner->tolliumuser);
    INTEGER ARRAY fsobjectids :=
        SELECT AS INTEGER ARRAY id
          FROM objects
         WHERE id > 0
               AND id NOT IN hideids;
    objects :=
        (SELECT id
              , name
              , title
              , isfolder
              , modificationdate
              , creationdate
              , canwrite
              , candelete
              , pvt_iconname := icon

              , parent := parentid
              , type := 0
              , filterscreen := Cellexists(objects,"filterscreen") ? objects.filterscreen : ""
              , filtersettings := Cellexists(objects,"filtersettings") ? objects.filtersettings : DEFAULT RECORD
              , modulekey := ""
              , extension := DEFAULT OBJECT
              , contentmode := Cellexists(objects,"contentmode") ? objects.contentmode : ""
              , tag := ""
              , contentshandler := contentshandler->rootid
           FROM objects
          WHERE id < 0)
        CONCAT
        (SELECT id
               , name
               , title
               , isfolder
               , type
               , modificationdate
               , creationdate

               , canwrite := FALSE
               , candelete := FALSE

               , parent := parentid
               , filterscreen := ""
               , filtersettings := DEFAULT RECORD
               , pvt_iconname := ""
               , modulekey := ""
               , extension := DEFAULT OBJECT
               , contentmode := ""
               , tag := ""
               , contentshandler := contentshandler->rootid
            FROM system.fs_objects
           WHERE id IN fsobjectids);

    RETURN EnrichObjectListing(this->owner->tolliumuser, objects, hidden_too, "");
  }

  INTEGER ARRAY FUNCTION GetCustomObjectPath(INTEGER itemid)
  {
    OBJECT contentshandler := this->GetCustomContentsHandler(itemid);
    IF (NOT ObjectExists(contentshandler))
      RETURN INTEGER[];

    RETURN contentshandler->OnGetPath(itemid);
  }

  PUBLIC BOOLEAN FUNCTION OnCellEdit(RECORD row, STRING columnname, STRING newvalue)
  {
    newvalue := TrimWhitespace(newvalue);
    IF(GetCell(row,columnname) = newvalue)
      RETURN FALSE;

    OBJECT work := this->owner->BeginWork();
    RECORD oldfile := SELECT * FROM system.fs_objects WHERE id=row.rowkey;
    IF(NOT RecordExists(oldfile) OR NOT oldfile.isactive OR NOT this->owner->tolliumuser->HasRightOn("system:fs_fullaccess", row.rowkey))
      RETURN FALSE;

    RECORD upd;
    IF(columnname = "name")
    {
      IF(oldfile.ispinned)
      {
        work->AddError(GetTid("publisher:components.errors.renamepinned"));
      }
      ELSE IF(NOT IsValidWHFSName(newvalue, FALSE))
      {
        work->AddError(GetTid("publisher:common.errors.illegalfilename", newvalue));
      }
      ELSE
      {
        INTEGER conflictid := SELECT AS INTEGER id FROM system.fs_objects WHERE parent = oldfile.parent AND id != row.rowkey AND ToUppercase(name) = ToUppercase(newvalue);
        IF(conflictid != 0)
        {
          STRING parentname := SELECT AS STRING name FROM system.fs_objects WHERE id = oldfile.parent;
          BOOLEAN hiddenforme := IsObjectHiddenForMe(this->owner->tolliumuser, conflictid);
          IF(hiddenforme)
            work->AddError(GetTid("publisher:common.errors.hiddenfolderexists", newvalue, parentname));
          ELSE
            work->AddError(GetTid("publisher:common.errors.fileorfolderexists", newvalue, parentname));
        }
      }
      INSERT CELL name := newvalue INTO upd;
    }
    ELSE IF(columnname="title")
    {
      IF(Length(newvalue)>=256)
      {
        work->AddError(GetTid("tollium:common.errors.too_long", GetTid("~title")));
      }
      INSERT CELL title := newvalue INTO upd;
    }

    IF(NOT work->HasFailed())
    {
      OpenWHFSObject(row.rowkey)->UpdateMetadata(upd);
      RunEditFileHooks(oldfile.id);
    }
    IF(NOT work->Finish())
      RETURN FALSE;

    this->ReloadOverview();
    RETURN TRUE;
  }
>;

/// List an arbitrary list of FSObjects
PUBLIC OBJECTTYPE FSContentsOverviewBase EXTEND FSObjectOverviewBase
<
  /// Properties that sould always be available:
  // groupfolders
  // hideexternal
  // hidefiles
  // hidefolders
  // rows - readonly
  //

  /// The foldertree to whose selection we are listening
  OBJECT pvt_foldertree;

  PUBLIC PROPERTY foldertree(pvt_foldertree, SetFoldertree);

  PUBLIC UPDATE MACRO StaticInit(RECORD def)
  {
    FSObjectOverviewBase::StaticInit(def);
    IF(ObjectExists(def.foldertree))
      this->foldertree := def.foldertree;
  }

  UPDATE MACRO PostInitComponent()
  {
    FSObjectOverviewBase::PostInitComponent();
  }

  /** @short Should empty the contents of the listing.
  */
  PUBLIC MACRO ClearContents()
  {
    this->ShowFSObjects(DEFAULT INTEGER ARRAY);
  }

  /** @short Should reload the contents of the listing.
  */
  UPDATE PUBLIC MACRO ReloadOverview()
  {
    THROW NEW Exception("ReloadOverview must be overwritten");
  }

  /** @short Apply a filtered set of results to the list.
      @param(RECORD ARRAY) The output of the filter callback.
  */
  PUBLIC MACRO _SetFilterResult(RECORD filterresult)
  {
    THROW NEW Exception("_SetFilterResult must be overwritten"); //this function is only PUBLIC so filemanager can jump to recycled file
  }

  /** @short Sets the FolderTree object that should be followed, if any.
      @param(OBJECT) newfollow The new FolderTree object to follow (DEFAULT OBJECT for none).
  */
  PUBLIC MACRO SetFoldertree(OBJECT newfollow)
  {
    IF(this->pvt_foldertree = newfollow)
      RETURN;

    IF(this->pvt_foldertree != DEFAULT OBJECT)
    {
      this->pvt_foldertree->__RemoveFilelistListener(this);
      this->pvt_foldertree := DEFAULT OBJECT;
    }
    IF(newfollow != DEFAULT OBJECT)
    {
      IF(NOT newfollow EXTENDSFROM FSObjectOverviewBase)/// Was in fact FolderTree, but that objecttype is undefined within this scope, and would cause recursive loadlibs
        THROW NEW Exception("The <filelist> " || this->name || " only supports following a <foldertree>");
      newfollow->__AddFilelistListener(this);
      this->pvt_foldertree := newfollow;
      this->__FolderTreeChanged();
    }
  }

  /** @short Shows an arbitrary listing of WHFSObjects based on the array of IDs given.
      @param(INTEGER ARRAY) fileids The WHFSIDs of the objects to show
  */
  PUBLIC MACRO ShowFSObjects(INTEGER ARRAY fileids)
  {
    THROW NEW Exception("ShowFSObjects must be overwritten");
  }

  /** @short Should update the list when the FolderTree selection has changed; used in conjunction with SetFoldertree.
  */
  PUBLIC MACRO __FolderTreeChanged()
  {
    RECORD sel := ObjectExists(this->foldertree) ? this->foldertree->selection : DEFAULT RECORD;
    IF(NOT RecordExists(this->foldertree->selection))
    {
      this->ClearContents();
      RETURN;
    }
    this->__SetFolder(sel.id, this->foldertree->GetFolderSettings());
  }

  /** @short Sets the list contents to the given folder and applies the given extension, if any.
      @param(INTEGER) folderid The WHFSID of the folder to list the contents for
      @param(OBJECT) extension The extension object to apply for this folder, if any (DEFAULT OBJECT for none).
  */
  MACRO __SetFolder(INTEGER folderid, RECORD foldersettings)
  {
    THROW NEW Exception("__SetFolder must be overwritten");
  }
>;
