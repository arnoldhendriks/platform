# WebHare frontend

`@webhare/frontend` implements various site integrations for the WebHare CMS

You would generally import this package this way:

```javascript
import "@webhare/frontend";
import "@webhare/frontend/styling/reset.css";


```
