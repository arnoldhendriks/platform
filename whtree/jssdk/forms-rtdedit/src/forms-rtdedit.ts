export { RTDEditElement } from "@mod-publisher/js/forms/fields/rtd";

// This gets TypeScript to refer to us by our @webhare/... name in auto imports:
declare module "@webhare/forms-rtdedit" {
}
