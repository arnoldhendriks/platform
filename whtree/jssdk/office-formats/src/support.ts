import type { Money, stdTypeOf } from "@webhare/std";

interface ColumnTypeDef {
  validDataTypes: Array<ReturnType<typeof stdTypeOf>>;
}

export const ColumnTypes = {
  "string": { validDataTypes: ["string"] },
  "number": { validDataTypes: ["number"] },
  "date": { validDataTypes: ["Date", "null"] },
  "boolean": { validDataTypes: ["boolean"] },
  "money": { validDataTypes: ["Money"] },
  "time": { validDataTypes: ["number"] },
  "dateTime": { validDataTypes: ["Date", "null"] },
} as const satisfies Record<string, ColumnTypeDef>;

export type SpreadsheetColumn = {
  name: string;
  title: string;
  type: Exclude<keyof typeof ColumnTypes, "dateTime">;
} | {
  name: string;
  title: string;
  type: "dateTime";
  storeUTC: boolean;
};

export type SpreadsheetRow = Record<string, number | string | Date | boolean | null | Money>;

export type GenerateSpreadsheetOptions = {
  rows: SpreadsheetRow[];
  columns: SpreadsheetColumn[];
  title?: string;
  timeZone?: string;
};

export type GenerateWorkbookProperties = {
  sheets: GenerateSpreadsheetOptions[];
  title?: string;
  timeZone?: string;
};

export function isValidSheetName(sheetname: string): boolean {
  /*  https://support.microsoft.com/en-us/office/rename-a-worksheet-3f1f7148-ee83-404d-8ef0-9ff99fbad1f9
      Important:  Worksheet names cannot:
      - Be blank .
      - Contain more than 31 characters.
      - Contain any of the following characters: / \ ? * : [ ]
      - Begin or end with an apostrophe ('), but they can be used in between text or numbers in a name.
      - Be named "History". This is a reserved word Excel uses internally.

    WE'll also reject
    - anything starting or ending with a space
    - anything outside the printable range
*/
  // eslint-disable-next-line no-control-regex
  return /^[^:/\\?\\*\\[\]\x00-\x1F]{1,31}$/.test(sheetname) && sheetname.toLowerCase() !== "history" && !/^[' ]|[ ']$/.test(sheetname);
}

export function validateRowsColumns(options: GenerateSpreadsheetOptions) {
  if (options.columns.length === 0) {
    throw new Error("No columns defined");
  }

  for (const column of options.columns) {
    if (column.type === "dateTime") {
      if (typeof column.storeUTC !== "boolean")
        throw new Error(`Column ${column.name} is of type dateTime but storeUTC is not a boolean`);
      if (column.storeUTC && !options.timeZone)
        throw new Error(`Column ${column.name} is of type dateTime and storeUTC is true but no timeZone is set`);
    } else if (!(column.type in ColumnTypes)) {
      throw new Error(`Column ${column.name} has an invalid type: ${column.type}`);
    }
  }
}
