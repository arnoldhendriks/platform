#ifndef blex_parsers_office_word_word_debug
#define blex_parsers_office_word_word_debug

#include "word_base.h"

namespace Parsers {
namespace Office {
namespace Word {

void DumpAbstractNumbering(ListData const &listdata);

} // End of namespace Word
} // End of namespace Office
} // End of namespace Parsers

#endif
